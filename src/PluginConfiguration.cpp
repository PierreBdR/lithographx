/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "PluginConfiguration.hpp"

#include <Information.hpp>

#include <algorithm>

#include <QAbstractButton>
#include <QBuffer>
#include <QDataStream>
#include <QEvent>
#include <QFontMetrics>
#include <QItemSelectionModel>
#include <QMimeData>
#include <QModelIndex>

namespace lgx {
namespace gui {

using process::StackProcess;
using process::MeshProcess;
using process::GlobalProcess;
using process::Plugin;
using process::pluginManager;

PluginListModel::PluginListModel(QObject *parent)
  : QAbstractItemModel(parent)
{
  manager = process::pluginManager();
  pluginList = manager->pluginsLoadingOrder();
  savedPluginList = pluginList;
}

void PluginListModel::updateProcesses()
{
  beginResetModel();
  pluginManager()->refreshAll();
  savedPluginList = pluginList;
  endResetModel();
}

void PluginListModel::resetProcesses()
{
  beginResetModel();
  pluginManager()->setPluginsLoadingOrder(savedPluginList);
  pluginList = savedPluginList;
  endResetModel();
}

void PluginListModel::cleanPlugins()
{
  beginResetModel();
  auto manager = pluginManager();
  manager->cleanPluginList();
  pluginList = manager->pluginsLoadingOrder();
  savedPluginList = pluginList;
  endResetModel();
}

int PluginListModel::columnCount(const QModelIndex &parent) const
{
  return 2;
}

QVariant PluginListModel::headerData(int section, Qt::Orientation orientation, int role) const
{
  if(orientation == Qt::Vertical)
    return QVariant();
  switch(role) {
    case Qt::DisplayRole:
      if(section == 0)
        return "Name";
      return "Type";
    default:
      return QVariant();
  }
}

int PluginListModel::rowCount(const QModelIndex &parent) const
{
  if(parent.isValid()) return 0;
  return pluginList.size();
}

QVariant PluginListModel::data(const QModelIndex &index, int role) const
{
  auto fullName = pluginList.at(index.row());
  auto plugin = manager->plugin(fullName);
  switch(role) {
    case Qt::DisplayRole:
      if(index.column() == 0)
        return plugin->name();
      return plugin->type();
    case Qt::ForegroundRole:
      if(not plugin->loaded())
        return QPalette().color(QPalette::Disabled, QPalette::WindowText);
      break;
    case Qt::BackgroundRole:
      if(not plugin->valid())
        return QColor(255, 128, 128); // light-red
      break;
    case Qt::CheckStateRole:
      if(index.column() == 0)
        return (plugin->used() ? Qt::Checked : Qt::Unchecked);
      break;
    case Qt::ToolTipRole:
      {
        if(plugin->loaded())
          return fullName;
        if(not plugin->loaded())
          return QString("Plugin empty or not loaded.");
        return QString("Error: %1").arg(plugin->errorMessage());
      }
    case Qt::WhatsThisRole:
      if(index.column() == 0)
        return "Plugin name";
      else
        return "Plugin type";
    default:
      break;
  }
  return QVariant();
}

std::shared_ptr<process::Plugin> PluginListModel::plugin(const QModelIndex& idx)
{
  return manager->plugin(pluginList.at(idx.row()));
}

bool PluginListModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
  if(index.column() == 0 and role == Qt::CheckStateRole) {
    auto plug = plugin(index);
    plug->use(value.toInt() == Qt::Checked);
    emit usedChange(plug.get());
    emit dataChanged(index, index, {Qt::CheckStateRole});
  }
  return false;
}

Qt::DropActions PluginListModel::supportedDropActions() const
{
  return Qt::MoveAction;
}

Qt::DropActions PluginListModel::supportedDragActions() const
{
  return Qt::MoveAction;
}

Qt::ItemFlags PluginListModel::flags(const QModelIndex &index) const
{
  if(not index.isValid())
    return Qt::ItemIsDropEnabled;
  int r = index.row();
  auto plugin = manager->plugin(pluginList.at(r));
  Qt::ItemFlags flags = Qt::ItemNeverHasChildren | Qt::ItemIsSelectable | Qt::ItemIsEnabled;
  if(not plugin->isSystem()) {
    flags |= Qt::ItemIsDragEnabled;
    if(plugin->loaded())
      flags |= Qt::ItemIsUserCheckable;
  }
  return flags;
}

QModelIndex PluginListModel::index(int row, int column, const QModelIndex &parent) const
{
  return createIndex(row, column, row);
}

QModelIndex PluginListModel::parent(const QModelIndex &index) const
{
  return QModelIndex();
}

bool PluginListModel::moveRows(const QModelIndex &sourceParent, int sourceRow, int count,
                               const QModelIndex &destinationParent, int destinationChild)
{
  if(sourceParent.isValid() or destinationParent.isValid() or count != 1 or
     sourceRow == destinationChild or sourceRow == destinationChild-1)
    return false;
  if(DEBUG)
    Information::out << "Moving rows from " << sourceRow << " to " << destinationChild << endl;
  beginMoveRows(sourceParent, sourceRow, sourceRow, destinationParent, destinationChild);
  auto manager = process::pluginManager();
  manager->movePluginLoadingOrder(pluginList.at(sourceRow), destinationChild);
  pluginList = manager->pluginsLoadingOrder();
  endMoveRows();
  return false;
}

bool PluginListModel::dropMimeData(const QMimeData *data, Qt::DropAction action, int row, int column,
                                   const QModelIndex &parent)
{
  if(data->hasFormat("application/x-lgx-pluginorderindex")) {
    QSet<int> ls;
    {
      const QByteArray& ba = data->data("application/x-lgx-pluginorderindex");
      QDataStream ds(ba);
      ds >> ls;
    }
    if(ls.size() != 1)
      return false;
    int idx = *ls.begin();
    moveRows(QModelIndex(), idx, 1, QModelIndex(), row);
    return true;
  }
  return false;
}

QStringList PluginListModel::mimeTypes() const
{
  return {"application/x-lgx-pluginorderindex"};
}

QMimeData* PluginListModel::mimeData(const QModelIndexList &indexes) const
{
  auto fmt = mimeTypes().at(0);
  QSet<int> ls;
  for(auto& idx: indexes)
    ls.insert(idx.row());
  QByteArray arr;
  {
    QBuffer buf(&arr);
    buf.open(QIODevice::WriteOnly);
    QDataStream ds(&buf);
    ds << ls;
  }
  auto mime = new QMimeData();
  mime->setData("application/x-lgx-pluginorderindex", arr);
  return mime;
}

namespace {
enum class IndexType : quintptr {
  Title            = 0,
  Item             = 1,
  ProcessAttribute = 2,
  ProcessParameter = 3
};

enum class ItemType : quintptr {
  Macro  = 0,
  Stack  = 1,
  Mesh   = 2,
  Global = 3,
};

enum class ItemAttribute : int {
  RegistrationName = 0,  ///< Name of the registration
  Used,                  ///< Yes, No or the name of the conflicting plugin
  Folder,                ///< Folder the process appears in
  Parameters,            ///< List of parameters
};


IndexType indexType(quintptr value) {
  return IndexType(value & 0x3);
}

ItemType itemType(quintptr value) {
  return ItemType((value >> 2) & 0x3);
}

quintptr indexId(quintptr value) {
  return value >> 4;
}

quintptr makeIndexValue(IndexType index,
                        ItemType item,
                        quintptr id)
{
  //Information::out << "makeIndexValue(" << quintptr(index) << ", " << quintptr(item) << ", " << id << ")\n";
  return (id << 4) | (quintptr(item) << 2) | quintptr(index);
}

template <typename P>
void sortProcesses(QStringList& list, Plugin* plugin) {
  QStringList names;
  std::vector<int> values(list.size());
  for(const auto& p : list) {
    values[names.size()] = names.size();
    names << plugin->processDefinition<P>(p).name;
  }

  std::sort(values.begin(), values.end(),
            [&names](int idx1, int idx2) -> bool {
              return QString::compare(names[idx1], names[idx2], Qt::CaseInsensitive) < 0;
            });
  QStringList newList;
  for(int i : values)
    newList << list.at(i);
  list = std::move(newList);
}

} // namespace

PluginModel::PluginModel(std::shared_ptr<process::Plugin> p, QObject *parent)
  : QAbstractItemModel(parent)
  , _plugin(p)
{
  errorMessage = _plugin->errorMessage();
  errorMode = not _plugin->loaded();
  if(errorMode) return;

  macroLanguages = _plugin->macroLanguages();
  macroLanguages.sort(Qt::CaseInsensitive);

  stackProcesses = _plugin->processes<StackProcess>();
  meshProcesses = _plugin->processes<MeshProcess>();
  globalProcesses = _plugin->processes<GlobalProcess>();

  sortProcesses<StackProcess>(stackProcesses, _plugin.get());
  sortProcesses<MeshProcess>(meshProcesses, _plugin.get());
  sortProcesses<GlobalProcess>(globalProcesses, _plugin.get());
}

int PluginModel::columnCount(const QModelIndex &parent) const
{
  return (errorMode ? 1 : 2);
}

QVariant PluginModel::headerData(int section, Qt::Orientation orientation, int role) const
{
  if(role == Qt::DisplayRole and orientation == Qt::Horizontal) {
    if(errorMode)
      return "Error Message";
    switch(section) {
      case 0:
        return "Property";
      case 1:
        return "Value";
    }
  }
  return QVariant();
}

int PluginModel::rowCount(const QModelIndex &parent) const
{
  if(errorMode)
    return (parent.isValid() ? 0 : 1);
  if(parent.isValid()) {
    auto value = parent.internalId();
    switch(indexType(value)) {
      case IndexType::Title:
        switch(itemType(value)) {
          case ItemType::Macro:
            if(parent.row() == 0)
              return 3;
            return macroLanguages.size();
          case ItemType::Stack:
            return stackProcesses.size();
          case ItemType::Mesh:
            return meshProcesses.size();
          case ItemType::Global:
            return globalProcesses.size();
        }
      case IndexType::Item:
        switch(itemType(value)) {
          case ItemType::Macro:
            return 0;
          case ItemType::Stack:
          case ItemType::Mesh:
          case ItemType::Global:
            return 4;
        }
      case IndexType::ProcessAttribute:
        {
          const process::BaseProcessDefinition* def = nullptr;
          if(parent.row() == int(ItemAttribute::Parameters)) {
            switch(itemType(value)) {
              case ItemType::Macro:
                return 0; // Should never happen anyway
              case ItemType::Stack:
                def = &_plugin->processDefinition<StackProcess>(stackProcesses.at(indexId(value)));
                break;
              case ItemType::Mesh:
                def = &_plugin->processDefinition<MeshProcess>(meshProcesses.at(indexId(value)));
                break;
              case ItemType::Global:
                def = &_plugin->processDefinition<GlobalProcess>(globalProcesses.at(indexId(value)));
                break;
            }
            return def->parmNames.size();
          }
          return 0;
        }
      case IndexType::ProcessParameter:
        return 0;
    }
  }
  return (macroLanguages.isEmpty() ? 1 : 2);
}

namespace {

template <typename P>
QString conflictingProcess(QString name, Plugin* plugin)
{
  auto manager = pluginManager();
  if(manager->hasProcess<P>(name)) {
    const auto& def = plugin->processDefinition<P>(name);
    const auto& mdef = pluginManager()->processDefinition<P>(def.name);
    if(mdef.plugin != def.plugin)
      return mdef.plugin;
  }
  return QString();
}

template <typename P>
QVariant processAttribute(int attr, QString name, Plugin* plugin)
{
  const auto& def = plugin->processDefinition<P>(name);
  switch(ItemAttribute(attr)) {
    case ItemAttribute::RegistrationName:
      return name;
    case ItemAttribute::Used: {
      if(plugin->used()) {
        auto conflict = conflictingProcess<P>(name, plugin);
        if(conflict.isEmpty())
          return "Yes";
        return QString("Conflict with plugin %1").arg(conflict);
      }
      return "No";
    }
    case ItemAttribute::Folder:
      return def.folder;
    case ItemAttribute::Parameters:
      return QVariant(); // Nothing
  }
}

template <typename P>
QVariant processParameterName(int param, QString name, Plugin* plugin)
{
  const auto& def = plugin->processDefinition<P>(name);
  return def.parmNames.at(param);
}

template <typename P>
QVariant processParameterValue(int param, QString name, Plugin* plugin)
{
  const auto& def = plugin->processDefinition<P>(name);
  return def.parms.at(param);
}

} // namespace

QVariant PluginModel::data(const QModelIndex &index, int role) const
{
  if(errorMode) {
    switch(role) {
      case Qt::DisplayRole:
        return (errorMessage.isEmpty() ? QString("Plugin empty or not loaded") : errorMessage);
      case Qt::BackgroundRole:
        if(not errorMessage.isEmpty())
          return QBrush(QColor(255, 128, 128));
        break;
    }
    return QVariant();
  }
  auto value = index.internalId();
  switch(indexType(value)) {
    case IndexType::Title:
      switch(role) {
        case Qt::DisplayRole:
          if(index.column() == 0) {
            switch(itemType(value)) {
              case ItemType::Macro:
                if(index.row() == 0) return "Processes";
                return "Macro Languages";
              case ItemType::Stack:
                return "Stack";
              case ItemType::Mesh:
                return "Mesh";
              case ItemType::Global:
                return "Global";
            }
          }
          break;
        case Qt::FontRole:
          {
            QFont font;
            font.setBold(true);
            return font;
          }
      }
      break;
    case IndexType::Item:
      switch(role) {
        case Qt::DisplayRole:
          if(index.column() == 0) {
            switch(itemType(value)) {
              case ItemType::Macro:
                return macroLanguages.at(index.row());
              case ItemType::Stack:
                return _plugin->processDefinition<StackProcess>(stackProcesses.at(index.row())).name;
              case ItemType::Mesh:
                return _plugin->processDefinition<MeshProcess>(meshProcesses.at(index.row())).name;
              case ItemType::Global:
                return _plugin->processDefinition<GlobalProcess>(globalProcesses.at(index.row())).name;
            }
          }
          break;
        case Qt::DecorationRole:
          if(index.column() == 0) {
            switch(itemType(value)) {
              case ItemType::Macro:
                return QString();
              case ItemType::Stack:
                return _plugin->processDefinition<StackProcess>(stackProcesses.at(index.row())).icon;
              case ItemType::Mesh:
                return _plugin->processDefinition<MeshProcess>(meshProcesses.at(index.row())).icon;
              case ItemType::Global:
                return _plugin->processDefinition<GlobalProcess>(globalProcesses.at(index.row())).icon;
            }
          }
          break;
        case Qt::BackgroundRole:
          switch(itemType(value)) {
            case ItemType::Stack:
              if(not conflictingProcess<StackProcess>(stackProcesses.at(index.row()), _plugin.get()).isEmpty())
                return QBrush(QColor(255, 128, 128));
              break;
            case ItemType::Mesh:
              if(not conflictingProcess<MeshProcess>(meshProcesses.at(index.row()), _plugin.get()).isEmpty())
                return QBrush(QColor(255, 128, 128));
              break;
            case ItemType::Global:
              if(not conflictingProcess<GlobalProcess>(globalProcesses.at(index.row()), _plugin.get()).isEmpty())
                return QBrush(QColor(255, 128, 128));
              break;
            case ItemType::Macro:
              break;
          }
          break;
      }
      break;
    case IndexType::ProcessAttribute:
      if(role == Qt::DisplayRole) {
        if(index.column() == 0) {
          switch(ItemAttribute(index.row())) {
            case ItemAttribute::RegistrationName:
              return "Registration name";
            case ItemAttribute::Used:
              return "Used";
            case ItemAttribute::Folder:
              return "Folder";
            case ItemAttribute::Parameters:
              return "Parameters";
          }
        } else {
          switch(itemType(value)) {
            case ItemType::Stack:
              return processAttribute<StackProcess>(index.row(), stackProcesses.at(indexId(value)), _plugin.get());
            case ItemType::Mesh:
              return processAttribute<MeshProcess>(index.row(), meshProcesses.at(indexId(value)), _plugin.get());
            case ItemType::Global:
              return processAttribute<GlobalProcess>(index.row(), globalProcesses.at(indexId(value)), _plugin.get());
            case ItemType::Macro:
              return QVariant(); // shouldn't happen
          }
        }
      }
      if(role == Qt::BackgroundRole and index.column() == 1 and index.row() == int(ItemAttribute::Used)) {
        switch(itemType(value)) {
          case ItemType::Stack:
            if(not conflictingProcess<StackProcess>(stackProcesses.at(indexId(value)), _plugin.get()).isEmpty())
              return QBrush(QColor(255, 128, 128));
            break;
          case ItemType::Mesh:
            if(not conflictingProcess<MeshProcess>(meshProcesses.at(indexId(value)), _plugin.get()).isEmpty())
              return QBrush(QColor(255, 128, 128));
            break;
          case ItemType::Global:
            if(not conflictingProcess<GlobalProcess>(globalProcesses.at(indexId(value)), _plugin.get()).isEmpty())
              return QBrush(QColor(255, 128, 128));
            break;
          case ItemType::Macro:
            break;
        }
      }
      break;
    case IndexType::ProcessParameter:
      if(role == Qt::DisplayRole) {
        if(index.column() == 0) {
          switch(itemType(value)) {
            case ItemType::Stack:
              return processParameterName<StackProcess>(index.row(), stackProcesses.at(indexId(value)), _plugin.get());
            case ItemType::Mesh:
              return processParameterName<MeshProcess>(index.row(), meshProcesses.at(indexId(value)), _plugin.get());
            case ItemType::Global:
              return processParameterName<GlobalProcess>(index.row(), globalProcesses.at(indexId(value)), _plugin.get());
            case ItemType::Macro:
              return QVariant(); // shouldn't happen
          }
        } else {
          switch(itemType(value)) {
            case ItemType::Stack:
              return processParameterValue<StackProcess>(index.row(), stackProcesses.at(indexId(value)), _plugin.get());
            case ItemType::Mesh:
              return processParameterValue<MeshProcess>(index.row(), meshProcesses.at(indexId(value)), _plugin.get());
            case ItemType::Global:
              return processParameterValue<GlobalProcess>(index.row(), globalProcesses.at(indexId(value)), _plugin.get());
            case ItemType::Macro:
              return QVariant(); // shouldn't happen
          }
        }
      }
      break;
  }
  return QVariant();
}

void PluginModel::usedChanged()
{
  if(errorMode) return;
  for(int i = 0 ; i < stackProcesses.size() ; ++i) {
    auto itemStart = createIndex(i, 0, makeIndexValue(IndexType::Item, ItemType::Stack, i));
    auto itemEnd = createIndex(i, 1, makeIndexValue(IndexType::Item, ItemType::Stack, i));
    emit dataChanged(itemStart, itemEnd);
    auto idx = createIndex(int(ItemAttribute::Used), 1, makeIndexValue(IndexType::ProcessAttribute, ItemType::Stack, i));
    emit dataChanged(idx, idx);
  }
  for(int i = 0 ; i < meshProcesses.size() ; ++i) {
    auto itemStart = createIndex(i, 0, makeIndexValue(IndexType::Item, ItemType::Mesh, i));
    auto itemEnd = createIndex(i, 1, makeIndexValue(IndexType::Item, ItemType::Mesh, i));
    emit dataChanged(itemStart, itemEnd);
    auto idx = createIndex(int(ItemAttribute::Used), 1, makeIndexValue(IndexType::ProcessAttribute, ItemType::Mesh, i));
    emit dataChanged(idx, idx);
  }
  for(int i = 0 ; i < globalProcesses.size() ; ++i) {
    auto itemStart = createIndex(i, 0, makeIndexValue(IndexType::Item, ItemType::Global, i));
    auto itemEnd = createIndex(i, 1, makeIndexValue(IndexType::Item, ItemType::Global, i));
    emit dataChanged(itemStart, itemEnd);
    auto idx = createIndex(int(ItemAttribute::Used), 1, makeIndexValue(IndexType::ProcessAttribute, ItemType::Global, i));
    emit dataChanged(idx, idx);
  }
}

/*
 * Structure of the index:
 *
 *  [Id|ProcessType|Type]
 *
 * IndexType (2 bit) :
 *  00 = Title,
 *  01 = Item (e.g. process or macro language)
 *  10 = ProcessAttribute
 *  11 = ProcessParameter
 *
 * ItemType: (2 bits)
 *  00 = Macro / Title
 *  01 = Stack
 *  10 = Mesh
 *  11 = Global
 *
 * Id: Position in the macro languages / process list
 */
QModelIndex PluginModel::index(int row, int column, const QModelIndex &parent) const
{
  if(parent.isValid()) {
    auto value = parent.internalId();
    switch(indexType(value)) {
      case IndexType::Title:
        if(itemType(value) == ItemType::Macro) {
          if(parent.row() == 0)
            return createIndex(row, column, makeIndexValue(IndexType::Title, ItemType(row+1), 0));
          return createIndex(row, column, makeIndexValue(IndexType::Item, ItemType::Macro, row));
        }
        return createIndex(row, column, makeIndexValue(IndexType::Item, itemType(value), row));
      case IndexType::Item:
        if(itemType(value) == ItemType::Macro)
          return QModelIndex(); // should never happen
        return createIndex(row, column,
                           makeIndexValue(IndexType::ProcessAttribute, itemType(value), indexId(value)));
      case IndexType::ProcessAttribute:
        if(parent.row() != int(ItemAttribute::Parameters))
          return QModelIndex(); // Should never happen
        return createIndex(row, column,
                           makeIndexValue(IndexType::ProcessParameter, itemType(value), indexId(value)));
      case IndexType::ProcessParameter:
        return QModelIndex(); // Should never happen
    }
  }
  return createIndex(row, column, makeIndexValue(IndexType::Title, ItemType::Macro, 0));
}

QModelIndex PluginModel::parent(const QModelIndex &index) const
{
  auto value = index.internalId();
  switch(indexType(value)) {
    case IndexType::Title:
      {
        if(itemType(value) == ItemType::Macro)
          return QModelIndex(); // top-level title
        auto parentValue = makeIndexValue(IndexType::Title, ItemType::Macro, 0);
        return createIndex(0, 0, parentValue);
      }
    case IndexType::Item:
      {
        switch(itemType(value)) {
          case ItemType::Macro:
            return createIndex(1, 0, makeIndexValue(IndexType::Title, ItemType::Macro, 0));
          case ItemType::Stack:
            return createIndex(0, 0, makeIndexValue(IndexType::Title, ItemType::Stack, 0));
          case ItemType::Mesh:
            return createIndex(1, 0, makeIndexValue(IndexType::Title, ItemType::Mesh, 0));
          case ItemType::Global:
            return createIndex(2, 0, makeIndexValue(IndexType::Title, ItemType::Global, 0));
        }
      }
    case IndexType::ProcessAttribute:
      {
        auto id = indexId(value);
        return createIndex(id, 0, makeIndexValue(IndexType::Item, itemType(value), id));
      }
    case IndexType::ProcessParameter:
      return createIndex(int(ItemAttribute::Parameters), 0,
                         makeIndexValue(IndexType::ProcessAttribute, itemType(value), indexId(value)));
  }
}

PluginConfiguration::PluginConfiguration(QWidget* parent)
  : QDialog(parent)
{
  ui.setupUi(this);

  ui.splitter->setStretchFactor(0, 2);
  ui.splitter->setStretchFactor(1, 3);

  listModel = new PluginListModel(this);
  ui.pluginsList->setModel(listModel);
  pluginsList_currentChanged(listModel->index(0, 0, QModelIndex()), QModelIndex());

  auto selModel = ui.pluginsList->selectionModel();

  ui.buttonBox->addButton("Clean Plugins", QDialogButtonBox::ActionRole);

  connect(listModel.data(), &PluginListModel::usedChange,
          this, &PluginConfiguration::pluginsList_usedChange);
  connect(selModel, &QItemSelectionModel::currentChanged,
          this, &PluginConfiguration::pluginsList_currentChanged);
}

bool PluginConfiguration::event(QEvent* e)
{
  if(e->type() == QEvent::PolishRequest) {
    ui.pluginsList->resizeColumnToContents(0);
    ui.pluginsList->resizeColumnToContents(1);
    int w = ui.pluginsList->width() - ui.pluginsList->columnWidth(1);
    ui.pluginsList->setColumnWidth(0, w);
    setPluginColumnWidth();
  }
  return QDialog::event(e);
}

void PluginConfiguration::setPluginColumnWidth()
{
  if(pluginModel) {
    ui.pluginView->expandAll();
    ui.pluginView->resizeColumnToContents(0);
    ui.pluginView->collapseAll();
    ui.pluginView->expand(pluginModel->index(0, 0, QModelIndex()));
  }
}

void PluginConfiguration::pluginsList_currentChanged(const QModelIndex& index, const QModelIndex& previous)
{
  auto plugin = listModel->plugin(index);
  if(pluginModel) pluginModel->deleteLater();
  pluginModel = new PluginModel(plugin, this);
  ui.pluginView->setModel(pluginModel);
  ui.pluginView->setExpanded(pluginModel->index(0, 0, QModelIndex()), true);
  setPluginColumnWidth();
}

void PluginConfiguration::pluginsList_usedChange(process::Plugin* plugin)
{
  pluginModel->usedChanged();
}

void PluginConfiguration::on_buttonBox_clicked(QAbstractButton* btn)
{
  switch(ui.buttonBox->buttonRole(btn)) {
    case QDialogButtonBox::ApplyRole:
    case QDialogButtonBox::AcceptRole:
      listModel->updateProcesses();
      pluginModel->usedChanged();
      break;
    case QDialogButtonBox::ResetRole:
    case QDialogButtonBox::RejectRole:
      listModel->resetProcesses();
      break;
    case QDialogButtonBox::ActionRole:
      if(btn->text() == "Clean Plugins") {
        listModel->cleanPlugins();
      }
    default:
      break;
  }
}

} // namespace gui
} // namespace lgx
