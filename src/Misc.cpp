/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "Misc.hpp"

#include "Dir.hpp"
#include "Information.hpp"

#include <iostream>
#include <limits>

#include <QDir>
#include <QFileInfo>
#include <QRegExp>
#include <QRegularExpression>
#include <QStandardPaths>
#include <QString>
#include <QTextStream>
#include <QUrl>
//#include <QNetworkRequest>
//#include <QNetworkManager>

#ifndef WIN32
#  include <unistd.h>
#  include <errno.h>
#endif

using namespace std;

namespace lgx {

const QString UM(QString::fromWCharArray(L"\xb5m"));             // um
const QString UM2(QString::fromWCharArray(L"\xb5m\xb2"));        // um^2
const QString UM3(QString::fromWCharArray(L"\xb5m\xb3"));        // um^3
const QString UM_1(QString::fromWCharArray(L"\xb5m\x207B\xb9")); // um^-1
const QString UM_2(QString::fromWCharArray(L"\xb5m\x207B\xb2")); // um^-2

namespace util {

namespace {
QRegularExpression defaultSeps("\\s*[[:space:],;:]\\s*");
}

QStringList splitFields(const QString& s)
{
  return s.split(defaultSeps);
}

Point3i stringToPoint3i(QString s, bool *glob_ok)
{
  const auto& fields = splitFields(s);
  bool ok;
  if(fields.size() == 1) {
    int val = fields[0].toInt(&ok);
    if(glob_ok)
      *glob_ok = ok;
    return Point3i(val);
  } else if(fields.size() == 3) {
    int vx, vy, vz;
    vx = fields[0].toInt(&ok);
    if(ok)
      vy = fields[1].toInt(&ok);
    if(ok)
      vz = fields[2].toInt(&ok);
    if(glob_ok)
      *glob_ok = ok;
    return Point3i(vx, vy, vz);
  }
  if(glob_ok)
    *glob_ok = false;
  return Point3i();
}

Point3u stringToPoint3u(QString s, bool *glob_ok)
{
  const auto& fields = splitFields(s);
  bool ok;
  if(fields.size() == 1) {
    uint val = fields[0].toUInt(&ok);
    if(glob_ok)
      *glob_ok = ok;
    return Point3u(val);
  } else if(fields.size() == 3) {
    uint vx, vy, vz;
    vx = fields[0].toUInt(&ok);
    if(ok)
      vy = fields[1].toUInt(&ok);
    if(ok)
      vz = fields[2].toUInt(&ok);
    if(glob_ok)
      *glob_ok = ok;
    return Point3u(vx, vy, vz);
  }
  if(glob_ok)
    *glob_ok = false;
  return Point3u();
}

Point3f stringToPoint3f(QString s, bool* glob_ok)
{
  bool ok;
  const auto& fields = splitFields(s);
  if(fields.size() == 1) {
    float val = fields[0].toFloat(&ok);
    if(ok) {
      if(glob_ok) *glob_ok = true;
      return Point3f(val);
    }
  } else if(fields.size() == 3) {
    float vx, vy, vz;
    vx = fields[0].toFloat(&ok);
    if(ok)
      vy = fields[1].toFloat(&ok);
    if(ok)
      vz = fields[2].toFloat(&ok);
    if(ok) {
      if(glob_ok) *glob_ok = true;
      return Point3f(vx, vy, vz);
    }
  }
  if(glob_ok)
    *glob_ok = false;
  return Point3f(std::numeric_limits<float>::quiet_NaN());
}

Point2f stringToPoint2f(QString s, bool *glob_ok)
{
  const auto& fields = splitFields(s);
  bool ok;
  if(fields.size() == 1) {
    float val = fields[0].toFloat(&ok);
    if(ok) {
      if(glob_ok) *glob_ok = true;
      return Point2f(val);
    }
  } else if(fields.size() == 2) {
    float vx, vy;
    vx = fields[0].toFloat(&ok);
    if(ok)
      vy = fields[1].toFloat(&ok);
    if(ok) {
      if(glob_ok) *glob_ok = true;
      return Point2f(vx, vy);
    }
  }
  if(glob_ok) *glob_ok = false;
  return Point2f(std::numeric_limits<float>::quiet_NaN());
}

QString toPercentEncoding(QString text)
{
  return QString::fromUtf8(QUrl::toPercentEncoding(text));
}

QString fromPercentEncoding(QString text)
{
  return QUrl::fromPercentEncoding(text.toUtf8());
}

} // namespace util
} // namespace lgx
