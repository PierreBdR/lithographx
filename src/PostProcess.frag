uniform sampler2D texId;
uniform ivec2 texSize;
uniform bool unsharp;
uniform float brightness;
uniform float contrast;
uniform float amount;
uniform float kernel[9];
// 0 1 2
// 3 4 5
// 6 7 8

smooth centroid in vec2 texPos;

void main()
{
  vec4 col = texture(texId, texPos);
  if(unsharp)
  {
    vec2 dt = vec2(1.0/texSize.x, 1.0/texSize.y);
    vec4 mask_col = kernel[4] * col;
    mask_col += kernel[3] * texture(texId, vec2(texPos.x-dt.x, texPos.y));
    mask_col += kernel[5] * texture(texId, vec2(texPos.x+dt.x, texPos.y));
    mask_col += kernel[1] * texture(texId, vec2(texPos.x, texPos.y+dt.y));
    mask_col += kernel[7] * texture(texId, vec2(texPos.x, texPos.y-dt.y));
    mask_col += kernel[2] * texture(texId, texPos + dt);
    mask_col += kernel[6] * texture(texId, texPos - dt);
    mask_col += kernel[0] * texture(texId, texPos + vec2(-dt.x, dt.y));
    mask_col += kernel[8] * texture(texId, texPos + vec2(dt.x, -dt.y));

    mask_col = col - mask_col;

    col += amount * mask_col;
  }
  col.rgb = (col.rgb - 0.5) * contrast + 0.5 + brightness;
  gl_FragColor = col;
}

