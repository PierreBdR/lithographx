/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

/*
 * This file is part of LithoGraphX
 *
 * LithoGraphX is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LithoGraphX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.
 */
#include "MeshProcessGrowth.hpp"

#include "Progress.hpp"
#include "Dir.hpp"
#include "SetVector.hpp"

#include <unordered_map>
#include <unordered_set>

// NB: we keep the name std::map otherwise it is ambiguous (map defined in vector.hpp)

namespace lgx {
namespace process {

using namespace util;

typedef std::unordered_map<int, std::pair<std::vector<vertex>, std::vector<vertex> > > IntPairVectVertices;
typedef std::unordered_map<int, set_vector<int> > IntLabelsMap;
typedef std::unordered_map<vertex, set_vector<int> > VertLabelsMap;
typedef std::unordered_map<int, vertex> VMap;
typedef std::pair<int, vertex> VMPair;

using std::swap;

namespace {

enum GrowthMeasure { GM_STRAIN, GM_ENGINEERING_STRAIN, GM_STRETCH_RATIO };

GrowthMeasure readGrowthMeasure(QString value, bool* ok = 0)
{
  value = value.toLower().trimmed();
  if(ok)
    *ok = true;
  if(value == "strain")
    return GM_STRAIN;
  if(value == "engineering strain")
    return GM_ENGINEERING_STRAIN;
  if(value == "stretch ratio")
    return GM_STRETCH_RATIO;
  if(ok)
    *ok = false;
  return GM_STRAIN;
}

QString toString(GrowthMeasure growth)
{
  switch(growth) {
  case GM_STRAIN:
    return "Strain";
  case GM_ENGINEERING_STRAIN:
    return "Engineering Strain";
  case GM_STRETCH_RATIO:
    return "Stretch ratio";
  }
  return "Strain";   // Should never reach here!
}

enum Ordering { ORDER_ABSOLUTE, ORDER_RELATIVE };

Ordering readOrdering(QString value, bool* ok = 0)
{
  value = value.toLower().trimmed();
  if(ok)
    *ok = true;
  if(value == "relative")
    return ORDER_RELATIVE;
  if(value == "absolute")
    return ORDER_ABSOLUTE;
  if(ok)
    *ok = false;
  return ORDER_ABSOLUTE;
}

QString toString(Ordering order)
{
  switch(order) {
  case ORDER_ABSOLUTE:
    return "Absolute";
  case ORDER_RELATIVE:
    return "Relative";
  }
  return "Absolute";   // Should never reach here!
}

enum AnisotropyType { ONE_MINUS_KMIN_OVER_KMAX, KMAX_OVER_KMIN, DIFFERENCE_OVER_SUM };

AnisotropyType readAnisotropy(QString value, bool* ok = 0)
{
  value = value.toLower().trimmed();
  if(ok)
    *ok = true;
  if(value == "1-kmin/kmax")
    return ONE_MINUS_KMIN_OVER_KMAX;
  if(value == "kmax/kmin")
    return KMAX_OVER_KMIN;
  if(value == "|(kmax-kmin)/(kmax+kmin)|")
    return DIFFERENCE_OVER_SUM;
  if(ok)
    *ok = false;
  return ONE_MINUS_KMIN_OVER_KMAX;
}

QString toString(AnisotropyType type)
{
  switch(type) {
  case ONE_MINUS_KMIN_OVER_KMAX:
    return "1-kmin/kmax";
  case KMAX_OVER_KMIN:
    return "kmax/kmin";
  case DIFFERENCE_OVER_SUM:
    return "|(kmax-kmin)/(kmax+kmin)|";
  }
  return "";
}

struct CorrespondanceMeshes {
  CorrespondanceMeshes(Mesh* mesh) {
    setOtherMesh(mesh);
  }
  void clear()
  {
    first_vid = last_vid = 0;
    graph_size = 0;
    useParent2 = false;
    J1J2.clear();
    J1J2forC1.clear();
    J1J2forC2.clear();
    centerNeighborsMapT1.clear();
    centerNeighborsMapT2.clear();
  }

  void setOtherMesh(Mesh* mesh)
  {
    graph_size = mesh->size();
    first_vid = mesh->graph()[0].id();
    last_vid = mesh->graph()[graph_size - 1].id();
  }

  bool checkMesh(Mesh* mesh)
  {
    if(mesh->size() != graph_size)
      return false;
    if(mesh->graph()[0].id() != first_vid)
      return false;
    if(mesh->graph()[graph_size - 1].id() != last_vid)
      return false;
    return true;
  }

  // Stored to check if this is the same graph
  graph::vertex_identity_t first_vid, last_vid;
  size_t graph_size;

  // To store the correspondance between junctions:
  // for the entire meshes:
  VertexVertexMap J1J2;
  // for each cell in mesh1:
  IntPairVectVertices J1J2forC1;
  IntPairVectVertices J1J2forC2;
  // Needed to check the connections between neighbors:
  IntLabelsMap centerNeighborsMapT1;
  IntLabelsMap centerNeighborsMapT2;
  // Store if mesh 2 uses parent labels
  bool useParent2;
};

enum GrowthHeatMap { GROWTH_NONE, GROWTH_PDG_MAX, GROWTH_PDG_MIN, GROWTH_AREA, GROWTH_ANISOTROPY };

GrowthHeatMap growthHeatMap(QString str)
{
  str = str.toLower();
  if(str == "kmax")
    return GROWTH_PDG_MAX;
  if(str == "kmin")
    return GROWTH_PDG_MIN;
  if(str == "anisotropy")
    return GROWTH_ANISOTROPY;
  if(str == "karea")
    return GROWTH_AREA;
  return GROWTH_NONE;
}

QString toString(GrowthHeatMap type)
{
  switch(type) {
  case GROWTH_NONE:
    return "None";
  case GROWTH_PDG_MAX:
    return "kmax";
  case GROWTH_PDG_MIN:
    return "kmin";
  case GROWTH_AREA:
    return "karea";
  case GROWTH_ANISOTROPY:
    return "Anisotropy";
  }
  return "None";
}
} // namespace

// take the correspondance between junctions for the whole mesh, compute the one for each cell
bool J1J2forCell(const vvgraph& T1, const IntIntMap& labelsT1, const IntLabelsMap& parentDaughtersMap,
                 set_vector<vertex>& problemJunctionT1, CorrespondanceMeshes& cm)
{
  forall(vertex c1, T1) {
    std::vector<vertex> J1forC1;
    std::vector<vertex> J2forC2;

    // c1 has to be a center, connected to  at least 3 vertices
    {
      IntIntMap::const_iterator it = labelsT1.find(c1->label);
      if(c1->type != 'c' or T1.valence(c1) < 3 or it == labelsT1.end() or it->second == 0)
        continue;
    }

    IntLabelsMap::const_iterator it = parentDaughtersMap.find(c1->label);
    if(it == parentDaughtersMap.end())
      continue;
    const set_vector<int>& lDaughters = it->second;
    if(lDaughters.count(0) > 0)
      continue;

    // pick a vertex in neihborhood of c1 to start searching for correspondance.
    vertex v1 = T1.anyIn(c1);
    vertex start1 = v1;
    do {
      vertex n = T1.nextTo(c1, v1);
      v1 = n;
    } while(v1->labcount < 3 and v1 != start1);
    // if couldn't find any vertex connected to 3 neigbors, go to next cell in T1
    if(v1->labcount < 3) {
      Information::out << "could not find a 3 way junction in cell: " << c1->label << " in T1" << endl;
      break;
    }

    vertex j1 = v1;     // starting vertex for the correpondance
    // check if there is a corresponding vertex in S2
    do {
      if(j1->type == 'j') {
        VertexVertexMap::iterator it = cm.J1J2.find(j1);
        if(it != cm.J1J2.end()) {
          vertex j2 = it->second;
          J1forC1.push_back(j1);
          J2forC2.push_back(j2);
        } else {
          problemJunctionT1.insert(j1);
        }
      }
      j1 = T1.nextTo(c1, j1);
    } while(j1 != v1);

    // if the correspondence was OK for the whole cell, store it into  J1J2forC1
    {
      IntLabelsMap::iterator it = cm.centerNeighborsMapT1.find(c1->label);
      if(it != cm.centerNeighborsMapT1.end() and it->second.size() != 0 and it->second.size() == J2forC2.size()) {
        cm.J1J2forC1[c1->label].first = J1forC1;
        cm.J1J2forC1[c1->label].second = J2forC2;
      } else
        Information::out << "for cell: " << c1->label << " in T1, found only " << J2forC2.size()
                         << " corresponding vertices on " << T1.valence(c1) << endl;
    }
  }
  return true;
}

bool CorrespondanceJunctions::operator()(Mesh* mesh1, Mesh* mesh2, bool eraseMarginCells, bool ShowVVCorrespondance)
{
  // clean up the previous correspondance
  mesh1->showLabel();
  mesh2->showLabel();

  // Create new graph containing only junctions
  MakeCellMesh mcm(*this);
  if(not mcm(mesh1, -1, mesh1->graph(), eraseMarginCells))
    return false;

  if(not mcm(mesh2, -1, mesh2->graph(), eraseMarginCells))
    return false;

  CorrespondanceMeshes* pcm = mesh1->attachment<CorrespondanceMeshes>("CorrespondanceMeshes");
  if(not pcm) {
    pcm = new CorrespondanceMeshes(mesh2);
    mesh1->attachObject("CorrespondanceMeshes", pcm);
  } else {
    pcm->clear();
    pcm->setOtherMesh(mesh2);
  }
  CorrespondanceMeshes& cm = *pcm;

  const vvgraph T1 = mesh1->graph();
  const vvgraph T2 = mesh2->graph();

  // Create a map of labels in both meshes, to check parent labeling or create it
  // we will later add 0 values in the label map for cells which don't have daughters (in T1) or parent (in T2)
  IntIntMap labelsT1, labelsT2;
  forall(vertex c, T1)
    if(c->type == 'c')
      labelsT1[c->label] = c->label;
  forall(vertex c, T2)
    if(c->type == 'c')
      labelsT2[c->label] = c->label;

  // Copy the "parentLabel" map into daughterParentMap: key = label in T2, value = parent in T1.
  // If cell does not have parent in T1, key = label in T2, value = 0
  IntIntMap daughterParentMapOrig = mesh2->parentLabelMap();
  IntIntMap daughterParentMap;
  if(daughterParentMapOrig.size() != 0) {
    Information::out << "Use parent labeling to check correspondence bewteen junctions." << endl;
    // Check the parentLabelMap for cells which are not in the mesh1 or mesh2
    for(IntIntMap::const_iterator it = daughterParentMapOrig.begin(); it != daughterParentMapOrig.end(); it++) {
      int lParent = it->second;
      int lDaughter = it->first;
      if(labelsT1.count(lParent) == 0 or labelsT2.count(lDaughter) == 0)
        return setErrorMessage(QString("Problems with parent label: %1, daughter label: %2."
                                       " First run Correct Parents, then Save and Load parents.")
                               .arg(lParent)
                               .arg(lDaughter));
      daughterParentMap[lDaughter] = lParent;
    }
    cm.useParent2 = true;
  } else {
    // If there is no parent map stored in mesh, the meshes should have the same labels
    // create a parent map based on the same cells in both meshes
    Information::out << "No parent labels loaded. Use labels to check correspondence bewteen junctions." << endl;
    for(IntIntMap::const_iterator l = labelsT1.begin(); l != labelsT1.end(); l++) {
      int label = l->first;
      if(labelsT2.count(label) != 0)
        daughterParentMap[label] = label;
    }
  }

  // Look at the daughters (in T2) for each cell in T1
  // parentDaughtersMap: key = parent in T1, value = labels of daughters in T2
  // If there is no parent, key = 0, values = labels of orphans (in T2)
  IntLabelsMap parentDaughtersMap;
  for(IntIntMap::const_iterator l = labelsT2.begin(); l != labelsT2.end(); l++) {
    int labelDaughter = l->first;
    int labelParent;
    set_vector<int> LDaughters;
    // add to daughterParentMap 0 values for the cells that do not have parent in T1.
    // map the label of cells in T2 without parent with 0
    {
      IntIntMap::iterator it = daughterParentMap.find(labelDaughter);
      if(it == daughterParentMap.end() or it->second == 0 or labelDaughter == 0) {
        labelsT2[labelDaughter] = 0;
        daughterParentMap[labelDaughter] = 0;
        labelParent = 0;
      } else {
        labelParent = it->second;
      }
    }
    IntLabelsMap::iterator it = parentDaughtersMap.find(labelParent);
    if(it != parentDaughtersMap.end())
      LDaughters = it->second;
    LDaughters.insert(labelDaughter);
    parentDaughtersMap[labelParent] = LDaughters;
  }
  // add 0 values in parentDaughtersMap for cells in T1 that do not have daughters
  // map the label of cells in T1 without daughters with 0
  set_vector<int> noDaughter;
  noDaughter.insert(0);
  for(IntIntMap::const_iterator l = labelsT1.begin(); l != labelsT1.end(); l++) {
    int labelParent = l->first;
    if(parentDaughtersMap.count(labelParent) == 0) {
      parentDaughtersMap[labelParent] = noDaughter;
      labelsT1[labelParent] = 0;
    }
  }

  // Fill in a map of the labels associated with each junction in T1 and T2.
  // The list of labels is sorted (it is a set).
  // If there are only 2 neighbors, then the edge is at the mesh border and we say it is in contact with the outside
  // (label 0);
  // Labels of cells of T1 that do not have daughters are stored as 0.
  // This will be used to identify junctions at each time points.
  VertLabelsMap junctionLabelsT1;
  forall(vertex j, T1) {
    set_vector<int> labels;
    if(j->type == 'j') {
      // list the labels around junction
      forall(vertex n, T1.neighbors(j))
        if(n->label > 0) {
          IntIntMap::iterator it = labelsT1.find(n->label);
          if(it != labelsT1.end())
            labels.insert(it->second);
        }
      if(labels.size() == 2)
        labels.insert(0);
      junctionLabelsT1[j] = labels;
    }
  }
  VertLabelsMap junctionLabelsT2;
  forall(vertex j, T2) {
    set_vector<int> labels;
    if(j->type == 'j') {
      // list the labels around junction
      forall(vertex n, T2.neighbors(j))
        if(n->label > 0) {
          IntIntMap::iterator it = labelsT2.find(n->label);
          if(it != labelsT2.end())
            labels.insert(it->second);
        }
      if(labels.size() == 2)
        labels.insert(0);
      junctionLabelsT2[j] = labels;
    }
  }

  // Look for the correspondence between junctions in T1 and in T2
  forall(vertex j2, T2) {
    if(junctionLabelsT2.count(j2) > 0) {
      set_vector<int> labelsDaughters;
      VertLabelsMap::iterator it = junctionLabelsT2.find(j2);
      if(it != junctionLabelsT2.end())
        labelsDaughters = it->second;
      set_vector<int> labelsParents;
      forall(int lDaughter, labelsDaughters) {
        IntIntMap::iterator it = daughterParentMap.find(lDaughter);
        int lParent = 0;
        if(it != daughterParentMap.end())
          lParent = it->second;
        labelsParents.insert(lParent);
      }
      // here compare the labels of parents of junction in T2 with junction in T1.
      // we considere only 3 ways junctions.
      if(labelsParents.size() == 3)
        for(VertLabelsMap::const_iterator it = junctionLabelsT1.begin(); it != junctionLabelsT1.end(); it++) {
          const set_vector<int>& labelsJunction = it->second;
          if(labelsJunction == labelsParents)
            cm.J1J2[it->first] = j2;
        }
    }
  }

  // Show the correspondence between vertices.
  IntMatrix3x3fMap& J1J2vectors = mesh1->vvCorrespondance();
  J1J2vectors.clear();
  mesh1->showVVCorrespondance() = ShowVVCorrespondance;
  int i = 0;
  for(VertexVertexMap::const_iterator it = cm.J1J2.begin(); it != cm.J1J2.end(); it++, i++) {
    vertex j1 = it->first;
    vertex j2 = it->second;
    J1J2vectors[i][0] = j1->pos;
    J1J2vectors[i][1] = j2->pos;
  }

  // Create a map of neighbors (labels) for each cell in T1 and T2.
  // in T1, labels of neighbors without daughters are stored as 0
  forall(vertex c, T1) {
    set_vector<int> labels;
    if(c->type == 'c' and c->label > 0) {
      // list the labels at junctions around the center
      forall(vertex j, T1.neighbors(c)) {
        if(junctionLabelsT1.count(j) != 0) {
          VertLabelsMap::iterator it = junctionLabelsT1.find(j);
          if(it != junctionLabelsT1.end())
            labels.insert(it->second.begin(), it->second.end());
        }
      }
      labels.erase(c->label);
      cm.centerNeighborsMapT1[c->label] = labels;
    }
  }
  // in T2, labels of neighbors without parents are stored as 0
  forall(vertex c, T2) {
    set_vector<int> labels;
    if(c->type == 'c' and c->label > 0) {
      // list the labels at junctions around the center
      forall(vertex j, T2.neighbors(c)) {
        if(junctionLabelsT2.count(j) != 0) {
          VertLabelsMap::iterator it = junctionLabelsT2.find(j);
          if(it != junctionLabelsT2.end())
            labels.insert(it->second.begin(), it->second.end());
        }
      }
      labels.erase(c->label);
      cm.centerNeighborsMapT2[c->label] = labels;
    }
  }

  // Select the vertices in T1 which cause trouble
  set_vector<vertex> problemJunctionT1;
  Information::out << "\n~~~~ Check problems with correspondence between junctions: ~~~~\n";
  J1J2forCell(T1, labelsT1, parentDaughtersMap, problemJunctionT1, cm);
  Information::out << problemJunctionT1.size() << " problem junctions found.\n" << endl;
  forall(vertex v, T1) {
    v->selected = false;
    if(problemJunctionT1.count(v) != 0)
      v->selected = true;
  }

  // Check that the neighbors around each cell are consistent in T1 and T2
  IntFloatMap heatMapDiffNeighborsT1;
  IntFloatMap heatMapDiffNeighborsT2;
  for(IntLabelsMap::const_iterator it = parentDaughtersMap.begin(); it != parentDaughtersMap.end(); ++it) {
    IntIntMap::iterator ip = labelsT1.find(it->first);
    if(ip == labelsT1.end())
      continue;
    int labelParent = ip->second;
    if(labelParent == 0)
      continue;

    set_vector<int> LDaughters = it->second;
    // case of cells without daughters (giant cell + periphery)
    if(LDaughters.size() == 1 and LDaughters.count(0) == 1) {
      IntLabelsMap::iterator ip = parentDaughtersMap.find(0);
      if(ip != parentDaughtersMap.end())
        LDaughters = ip->second;
    }

    set_vector<int> neighborsT1;
    set_vector<int> neighborsT2;
    set_vector<int> parentsNeighborsT2;

    // list the neighbors around parent cell in T1
    {
      IntLabelsMap::iterator ip = cm.centerNeighborsMapT1.find(labelParent);
      if(ip != cm.centerNeighborsMapT1.end())
        neighborsT1 = ip->second;
      else
        neighborsT1.insert(0);
    }

    // list the neighbors around daughter cells in T2
    forall(int labelD, LDaughters) {
      if(cm.centerNeighborsMapT2.count(labelD) != 0) {
        IntLabelsMap::iterator ip = cm.centerNeighborsMapT2.find(labelD);
        if(ip != cm.centerNeighborsMapT2.end())
          neighborsT2.insert(ip->second.begin(), ip->second.end());
      }
    }
    // take out daughter cells from neighborsT2
    set_vector<int> neighborsT2temp;
    forall(int neighbor, neighborsT2)
      if(LDaughters.count(neighbor) == 0)
        neighborsT2temp.insert(neighbor);
    neighborsT2 = neighborsT2temp;

    // look at the parents of neighbors in T2
    forall(int neighbor, neighborsT2) {
      IntIntMap::iterator ip = daughterParentMap.find(neighbor);
      if(ip != daughterParentMap.end())
        parentsNeighborsT2.insert(ip->second);
      else
        parentsNeighborsT2.insert(0);
    }

    // compare the neighbors around parent and daughter cells, fill in heat map with differences
    heatMapDiffNeighborsT1[labelParent] = float(parentsNeighborsT2 != neighborsT1);
    forall(int labelD, LDaughters)
      if(labelD != 0)
        heatMapDiffNeighborsT2[labelD] = float(parentsNeighborsT2 != neighborsT1);

    if(parentsNeighborsT2.size() != neighborsT1.size())
      Information::out << "problem with cell: " << labelParent << ", the number of neighbors changes. "
                       << parentsNeighborsT2.size() << " parents of neighbors in T2 vs. " << neighborsT1.size()
                       << " neighbors in T1." << endl;
  }

  // Display the difference in neighbor number in a heatmap
  mesh1->clearHeatmap();
  mesh2->clearHeatmap();
  float min1 = FLT_MAX, max1 = -FLT_MAX;
  forall(const IntFloatPair& p, heatMapDiffNeighborsT1) {
    float val = p.second;
    if(val < min1)
      min1 = val;
    if(val > max1)
      max1 = val;
    mesh1->labelHeat()[p.first] = val;
  }
  float min2 = FLT_MAX, max2 = -FLT_MAX;
  forall(const IntFloatPair& p, heatMapDiffNeighborsT2) {
    float val = p.second;
    if(val < min2)
      min2 = val;
    if(val > max2)
      max2 = val;
    mesh2->labelHeat()[p.first] = val;
  }

  mesh1->heatMapDesc() = Description("Neighborhood difference");
  if(min1 > max1) {
    min1 = 0.f;
    max1 = 1.f;
  } else if(min1 == max1)
    max1 += 1.f;
  mesh1->heatMapBounds() = Point2f(min1, max1);
  mesh1->showHeat();
  mesh1->updateTriangles();
  mesh1->updateSelection();

  mesh2->heatMapDesc() = Description("Neighborhood difference");
  if(min2 > max2) {
    min2 = 0.f;
    max2 = 1.f;
  } else if(min2 == max2)
    max2 += 1.f;
  mesh2->heatMapBounds() = Point2f(min2, max2);
  mesh2->showHeat();
  mesh2->updateTriangles();
  // set transfert function to jet, so that the 0 values look blue and 1 values look red
  mesh1->setHeatFct(TransferFunction::viridis());
  mesh2->setHeatFct(TransferFunction::viridis());

  //// for debugging
  //{
  //  Information::out << endl;
  //  Information::out << "///////////daughterParentMap: " << endl;
  //  IntIntMap::iterator it;
  //  for (it = daughterParentMap.begin(); it != daughterParentMap.end(); ++it)
  //    Information::out << "labelDaughter: " << it->first << ", labelParent: " << it->second << endl;
  //}
  //
  //{
  //  Information::out << endl;
  //  Information::out << "///////////parentDaughtersMap: " << endl;
  //  IntLabelsMap::iterator it;
  //  for (it = parentDaughtersMap.begin(); it != parentDaughtersMap.end(); ++it)
  //  {
  //    Information::out << "labelParent: " << it->first << ", labelDaughters: " ;
  //    set<int> LDaughters = it->second;
  //    forall(int labelD, LDaughters)
  //      Information::out << labelD << ", " ;
  //    Information::out << endl;
  //  }
  //}
  // Information::out << "///////////problems junctions size: " << problemJunctionT1.size() << endl;
  // Information::out << "///////////cm.J1J2forC1 size: " << cm.J1J2forC1.size() << endl;

  SETSTATUS("Cells with change in number of neighbors appear in red, vertices which could not be identified are "
            "selected on the first mesh.");
  return true;
}
REGISTER_MESH_PROCESS(CorrespondanceJunctions);

/////////////////// Stretch computation ///////////////////////////////////////

namespace {
// compute polygon center:
Point3d cell_center(const std::vector<Point3d>& polygon)
{
  Point3d center = Point3d(0, 0, 0);
  int nb_sides = polygon.size();
  for(int i = 0; i < nb_sides; i++)
    center += polygon[i];

  center /= nb_sides;
  return center;
}

// compute polygon normal:
Point3d cell_normal(const std::vector<Point3d>& polygon, const Point3d& center_polygon)
{
  Point3d normal_polygon = Point3d(0, 0, 0);
  int nb_sides = polygon.size();
  int prev = nb_sides - 1;
  for(int i = 0; i < nb_sides; i++) {
    normal_polygon += (polygon[prev] - center_polygon) % (polygon[i] - center_polygon);     // cross product
    prev = i;
  }
  normal_polygon.normalize();
  return normal_polygon;
}

// project Point3d on plane:
// P=Point to project (Point3d), Q=Point of the plane(Point3d),N=Normal to the plane(point3d), norm(N)=1
Point3d projectPointOnPlane(const Point3d& P, const Point3d& Q, const Point3d& N) {
  return P - ((P - Q) * N) * N;
}

// multiply vector of point3d by matrix:
std::vector<Point3d> matrixMultiply(const std::vector<Point3d>& P, const Matrix3d& M)
{
  int nb_rows = P.size();
  std::vector<Point3d> result(nb_rows);
  for(int i = 0; i < nb_rows; ++i)
    result[i] = P[i] * M;
  return result;
}

void inplaceMatrixMultiply(std::vector<Point3d>& P, const Matrix3d& M)
{
  int nb_rows = P.size();
  for(int i = 0; i < nb_rows; ++i)
    P[i] = P[i] * M;
}

// for two matrices P and M of size 2xnb_rows, multiply: transpose(P) * M == the dot produc
Matrix2d dotProduct(const std::vector<Point2d>& P, const std::vector<Point2d>& M)
{
  Matrix2d prod;
  for(size_t i = 0; i < P.size(); ++i) {
    prod(0, 0) += P[i][0] * M[i][0];
    prod(0, 1) += P[i][0] * M[i][1];
    prod(1, 0) += P[i][1] * M[i][0];
    prod(1, 1) += P[i][1] * M[i][1];
  }
  return prod;
}

inline double square(double x) {
  return x * x;
}

// SVD decomposition of a 2D matrix M in: rotation(-theta)*[maxS,0; 0, minS]*rotation(psi)
void SVD_matrix2x2(Matrix2d M, double& theta, double& psi, double& maxS, double& minS)
{
  double c1, c2, c3, c4;
  c1 = sqrt(square(M(0, 0) + M(1, 1)) + square(M(0, 1) - M(1, 0)));
  c2 = sqrt(square(M(0, 0) - M(1, 1)) + square(M(0, 1) + M(1, 0)));
  c3 = atan2(M(0, 1) + M(1, 0), M(0, 0) - M(1, 1));
  c4 = atan2(M(0, 1) - M(1, 0), M(0, 0) + M(1, 1));

  maxS = (c1 + c2) / 2;
  minS = (c1 - c2) / 2;
  theta = (c3 - c4) / 2;
  psi = (c3 + c4) / 2;
}

// compute Principal Directions of Strain in 2D
void computeGrowth(const std::vector<vertex>& J1forC1, const std::vector<vertex>& J2forC2,
                   SymmetricTensor& cellGrowth_1, SymmetricTensor& cellGrowth_2, GrowthMeasure growth,
                   Ordering ordering, float dt)
{
  int nb_sides = J1forC1.size();
  std::vector<Point3d> polygon1(nb_sides), polygon2(nb_sides);

  for(int i = 0; i < nb_sides; i++) {
    polygon1[i] = J1forC1[i]->pos;
    polygon2[i] = J2forC2[i]->pos;
  }

  // Compute transformation matrix for the two polygons
  // Compute polygon center (3D)
  const Point3d& center_polygon1 = cell_center(polygon1);
  const Point3d& center_polygon2 = cell_center(polygon2);

  // Compute polygon normal (3D)
  const Point3d& normal_polygon1 = cell_normal(polygon1, center_polygon1);
  const Point3d& normal_polygon2 = cell_normal(polygon2, center_polygon2);

  // Project polygon 3d points on the cell plane and center the polygon on (0,0,0)
  std::vector<Point3d> polygon_proj1(nb_sides), polygon_proj2(nb_sides);

  for(int i = 0; i < nb_sides; i++) {
    // pos=Point to project (Point3d), p=Point of the plane(Point3d),n=Normal to the plane(point3d)
    polygon_proj1[i] = projectPointOnPlane(polygon1[i] - center_polygon1, Point3d(), normal_polygon1);
    polygon_proj2[i] = projectPointOnPlane(polygon2[i] - center_polygon2, Point3d(), normal_polygon2);
  }

  // Rotate polygon plane to align it with (x,y) plane
  Point3d z_axis(0, 0, 1);
  double cos_angle1, sin_angle1, angle_rot_plane1;
  Point3d rot_axis_plane1 = normal_polygon1 % z_axis;   // cross product,  ||normalxZaxis||=sin(angle rotation)
  sin_angle1 = norm(rot_axis_plane1);
  cos_angle1 = normal_polygon1 * z_axis;   // dot product,  normal.Zaxis=cos(angle rotation)
  angle_rot_plane1 = atan2(sin_angle1, cos_angle1);

  // which means: if rot_axis_plane is perfectly aligned with Z axis
  if(fabs(sin_angle1) < 1e-8)
    rot_axis_plane1 = Point3d(1, 0, 0);
  else
    rot_axis_plane1.normalize();

  // with angle= angle between normal and z axis
  Matrix3d inv_rot_polygon_plane1 = Matrix3d::rotation(rot_axis_plane1, -angle_rot_plane1);
  inplaceMatrixMultiply(polygon_proj1, inv_rot_polygon_plane1);

  // same for the second polygon
  float cos_angle2, sin_angle2, angle_rot_plane2;
  Point3d rot_axis_plane2 = normal_polygon2 % z_axis;   // cross product,  ||normalxZaxis||=sin(angle rotation)
  sin_angle2 = norm(rot_axis_plane2);
  cos_angle2 = normal_polygon2 * z_axis;   // dot product,  normal.Zaxis=cos(angle rotation)
  angle_rot_plane2 = atan2(sin_angle2, cos_angle2);

  // which means: if rot_axis_plane is perfectly aligned with Z axis
  if(fabs(sin_angle2) < 1e-8)
    rot_axis_plane2 = Point3d(1, 0, 0);
  else
    rot_axis_plane2.normalize();

  // with angle= angle between normal and z axis
  Matrix3d inv_rot_polygon_plane2 = Matrix3d::rotation(rot_axis_plane2, -angle_rot_plane2);
  inplaceMatrixMultiply(polygon_proj2, inv_rot_polygon_plane2);

  // Take out 3rd dimension from projected polygons:
  std::vector<Point2d> polygon1_2D(nb_sides), polygon2_2D(nb_sides);
  for(int i = 0; i < nb_sides; ++i) {
    polygon1_2D[i] = Point2d(polygon_proj1[i].x(), polygon_proj1[i].y());
    polygon2_2D[i] = Point2d(polygon_proj2[i].x(), polygon_proj2[i].y());
  }

  // Compute transformation matrix between c1 and c2(in 2D, on projected polygons):
  Matrix2d XTX = dotProduct(polygon1_2D, polygon1_2D);
  Matrix2d XTY = dotProduct(polygon1_2D, polygon2_2D);
  Matrix2d transfM = inverse(XTX) * XTY;

  // Decompose transformation matrix into rotations and growth:
  double theta, psi, maxStretchRatio, minStretchRatio;
  SVD_matrix2x2(transfM, theta, psi, maxStretchRatio, minStretchRatio);

  switch(growth) {
  case GM_STRAIN:
    maxStretchRatio = log(maxStretchRatio);
    minStretchRatio = log(minStretchRatio);
    break;
  case GM_ENGINEERING_STRAIN:
    maxStretchRatio -= 1.;
    minStretchRatio -= 1.;
    break;
  case GM_STRETCH_RATIO:
    break;     // Nothing to do, as this is what we compute
  }
  if(dt > 0) {
    maxStretchRatio /= dt;
    minStretchRatio /= dt;
  }

  if(ordering == ORDER_ABSOLUTE) {
    if(fabs(minStretchRatio) > fabs(maxStretchRatio)) {
      swap(minStretchRatio, maxStretchRatio);
      theta += M_PI / 2;
      if(theta > M_PI / 2)
        theta -= M_PI;
      psi += M_PI / 2;
      if(psi > M_PI / 2)
        psi -= M_PI;
    }
  }

  // Convert transformation matrix from 2D to 3D:
  Matrix3d transfM_3D;
  for(int i = 0; i < 2; ++i)
    for(int j = 0; j < 2; ++j)
      transfM_3D(i, j) = transfM(i, j);

  Matrix3d rot_polygon_plane2 = Matrix3d::rotation(rot_axis_plane2, angle_rot_plane2);
  Matrix3d rot_polygon_plane1 = Matrix3d::rotation(rot_axis_plane1, angle_rot_plane1);
  transfM_3D = inv_rot_polygon_plane1 * transfM_3D * rot_polygon_plane2;

  // Apply 3D transformation to polygon1 (cell before growth), in order to compute residuals
  // (difference between actual and computed transformation):
  std::vector<Point3d> polygon1_centered, polygon1_transf;
  std::vector<double> residuals;
  polygon1_centered.resize(nb_sides);
  residuals.resize(nb_sides);

  for(int i = 0; i < nb_sides; ++i)
    polygon1_centered[i] = polygon1[i] - center_polygon1;
  polygon1_transf = matrixMultiply(polygon1_centered, transfM_3D);

  for(int i = 0; i < nb_sides; ++i) {
    polygon1_transf[i] = polygon1_transf[i] + center_polygon1;
    residuals[i] = norm(polygon1_transf[i] - polygon2[i]);
  }

  // Return the Principal Directions for polygon2 (deformed configuration):
  cellGrowth_2.ev1() = Point3f(Point3d(cos(psi), sin(psi), 0) * rot_polygon_plane2);    // unit vector PDG max
  cellGrowth_2.ev2() = Point3f(Point3d(-sin(psi), cos(psi), 0) * rot_polygon_plane2);   // unit vector PDG min
  cellGrowth_2.evals() = Point3f(maxStretchRatio, minStretchRatio, 0);

  // Return the Principal Directions for polygon1 (original configuration):
  cellGrowth_1.ev1() = Point3f(Point3d(cos(theta), sin(theta), 0) * rot_polygon_plane1);
  cellGrowth_1.ev2() = Point3f(Point3d(-sin(theta), cos(theta), 0) * rot_polygon_plane1);
  cellGrowth_1.evals() = Point3f(maxStretchRatio, minStretchRatio, 0);
}
} // namespace

bool ComputeGrowth2D::operator()(Mesh* mesh1, Mesh* mesh2, QString growthStr, QString orderingStr, float dt)
{
  if(!mesh1->cells() or !mesh2->cells()) {
    setErrorMessage(QString("Use 'Check correspondance' first"));
    return false;
  }

  CorrespondanceMeshes* pcm = mesh1->attachment<CorrespondanceMeshes>("CorrespondanceMeshes");
  if(not pcm or not pcm->checkMesh(mesh2))
    return setErrorMessage(QString("Error, no correspondance mesh attached to Mesh %1").arg(mesh1->userId()));

  bool ok;
  GrowthMeasure growth = readGrowthMeasure(growthStr, &ok);
  if(not ok)
    return setErrorMessage(QString("Error, unknown growth measure '%1'").arg(growthStr));
  growthStr = toString(growth);

  if(growth == GM_STRETCH_RATIO and dt > 0)
    return setErrorMessage(QString("Error, the stretch ratio cannot be used with a nontime-step"));
  if(dt < 0)
    return setErrorMessage(QString("Error, the time-step must be positive."));

  Ordering ordering = readOrdering(orderingStr, &ok);
  if(not ok)
    return setErrorMessage(QString("Error, unknown ordering '%1'").arg(orderingStr));
  orderingStr = toString(ordering);

  mesh1->clearCellAxis();
  mesh2->clearCellAxis();
  // Copy meshes
  vvgraph S1 = mesh1->graph();
  vvgraph S2 = mesh2->graph();

  mesh1->updateCentersNormals();
  mesh2->updateCentersNormals();

  // use the method CellAxis to modify a map associating labels with a vector of 3 Point3f (the cell axis)
  IntSymTensorMap& cellGrowth_2 = mesh2->cellAxis();
  IntSymTensorMap& cellGrowth_1 = mesh1->cellAxis();
  cellGrowth_2.clear();
  cellGrowth_1.clear();

  forall(const vertex& c1, S1) {
    if(c1->type != 'c' or S1.valence(c1) < 3 or c1->label == 0)
      continue;

    // if the correspondance between vertices exist for the whole cell, compute Stretch
    if(pcm->J1J2forC1.count(c1->label) > 0) {
      computeGrowth(pcm->J1J2forC1[c1->label].first, pcm->J1J2forC1[c1->label].second, cellGrowth_1[c1->label],
                    cellGrowth_2[c1->label], growth, ordering, dt);
    }
  }

  if(pcm->useParent2)
    mesh2->setParentAxis();
  else
    mesh2->setCellAxis();
  mesh1->setCellAxis();

  Description axisType = Description("2D Growth")
    << std::make_pair("Measure", growthStr) << std::make_pair("Axis ordering", orderingStr)
    << std::make_pair("Time Step", QString::number(dt)) << std::make_pair("Time", "")
    << std::make_pair("Use Parent Labelling", "");
  mesh1->cellAxisDesc() = axisType;
  mesh1->cellAxisDesc()["Time"] = "Start";
  mesh1->cellAxisDesc()["Use Parent Labelling"] = mesh1->isParentAxis() ? "Yes" : "No";
  mesh2->cellAxisDesc() = axisType;
  mesh2->cellAxisDesc()["Time"] = "End";
  mesh2->cellAxisDesc()["Use Parent Labelling"] = mesh2->isParentAxis() ? "Yes" : "No";
  if(dt > 0) {
    mesh1->cellAxisUnit() = QString("1/h");
    mesh2->cellAxisUnit() = QString("1/h");
  } else {
    mesh1->cellAxisUnit() = QString("");
    mesh2->cellAxisUnit() = QString("");
  }

  Information::out << "Size of Cell growth map in mesh1: " << cellGrowth_1.size() << endl;
  Information::out << "Size of Cell growth map in mesh2: " << cellGrowth_2.size() << endl;

  return true;
}

void ComputeGrowth2D::showResult(Mesh* mesh1, Mesh* mesh2)
{
  // Run PDG display on both meshes, with the parameters from the GUI
  ParmList parms;
  if(not getDefaultParms("Mesh", "Display 2D Growth", parms))
    return;
  DisplayGrowth2D dpdg(*this);
  bool ok;
  float anisotropyThreshold = parms[5].toFloat(&ok);
  if(!ok)
    return;
  float axisLineScale = parms[6].toFloat(&ok);
  if(!ok)
    return;
  float scaleAxisLength = parms[7].toFloat(&ok);
  if(!ok)
    return;
  float axisOffset = parms[8].toFloat(&ok);
  if(!ok)
    return;
  dpdg(mesh1, parms[1].toString(), QColor(parms[3].toString()), QColor(parms[4].toString()),
       axisLineScale, scaleAxisLength, axisOffset, parms[0].toString(),
       parms[2].toString(), anisotropyThreshold);
  dpdg(mesh2, parms[1].toString(), QColor(parms[3].toString()), QColor(parms[4].toString()),
       axisLineScale, scaleAxisLength, axisOffset, parms[0].toString(),
       parms[2].toString(), anisotropyThreshold);
}

REGISTER_MESH_PROCESS(ComputeGrowth2D);

namespace {
bool aboveOne(float a) {
  return a >= 1;
}
} // namespace

// Display Stretch (separatly from each other) and heatmaps of anisotropy etc.
bool DisplayGrowth2D::operator()(Mesh* mesh, QString anisotropyStr, const QColor& ColorExpansion,
                                 const QColor& ColorShrinkage, float AxisLineWidth, float ScaleAxisLength,
                                 float AxisOffset, const QString DisplayHeatMap, const QString DisplayGrowth,
                                 float AnisotropyThreshold)
{
  // if there is no Stretch stored, display error message
  const IntSymTensorMap& cellAxis = mesh->cellAxis();
  if(cellAxis.size() == 0 or mesh->cellAxisDesc().type() != "2D Growth") {
    setErrorMessage(QString("No Stretch stored in active mesh!"));
    return false;
  }

  bool ok;
  AnisotropyType anisotropyType = readAnisotropy(anisotropyStr, &ok);
  if(not ok)
    return setErrorMessage(QString("Error, unknown anisotropy type: '%1'").arg(anisotropyStr));

  // Update the normals and center of the cell for visualisation.
  bool useParents = mesh->isParentAxis();
  if(mesh->labelCenterVis().empty() or (useParents and mesh->parentCenterVis().empty()))
    mesh->updateCentersNormals();

  // Scale Stretch for visualization
  mesh->cellAxisWidth() = AxisLineWidth;
  mesh->cellAxisOffset() = AxisOffset;
  Colorf colorExpansion = ColorExpansion;
  Colorf colorShrinkage = ColorShrinkage;

  bool showMajorAxis = (DisplayGrowth == "kmax")or (DisplayGrowth == "both");
  bool showMinorAxis = (DisplayGrowth == "kmin")or (DisplayGrowth == "both");
  bool computeStretch = mesh->cellAxisDesc().field("Measure", "") == toString(GM_STRETCH_RATIO);
  mesh->prepareAxisDrawing(ScaleAxisLength, (computeStretch ? aboveOne : isPositive), colorExpansion, colorShrinkage,
                           Point3b(showMajorAxis, showMinorAxis, false));

  IntFloatMap anisotropy;

  forall(const IntSymTensorPair& p, mesh->cellAxis()) {
    int label = p.first;
    const SymmetricTensor& tensor = p.second;
    float a;
    switch(anisotropyType) {
    case ONE_MINUS_KMIN_OVER_KMAX:
      a = 1 - tensor.evals()[1] / tensor.evals()[0];
      break;
    case KMAX_OVER_KMIN:
      a = tensor.evals()[0] / tensor.evals()[1];
      break;
    case DIFFERENCE_OVER_SUM:
      a = fabs((tensor.evals()[0] - tensor.evals()[1]) / (tensor.evals()[0] + tensor.evals()[1]));
      break;
    }
    anisotropy[label] = a;
  }

  forall(const IntSymTensorPair& p, mesh->cellAxis()) {
    int cell = p.first;
    if(anisotropy[cell] < AnisotropyThreshold)
      mesh->cellAxisScaled()[cell].zero();       // reset the axis
  }

  GrowthHeatMap stretch_map = growthHeatMap(DisplayHeatMap);

  if(stretch_map != GROWTH_NONE) {
    IntFloatMap labelHeatMap;
    float maxValue = -FLT_MAX, minValue = FLT_MAX;
    forall(const IntSymTensorPair& p, cellAxis) {
      int cell = p.first;
      const SymmetricTensor& tensor = p.second;
      float value = 0;
      switch(stretch_map) {
      case GROWTH_PDG_MAX:
        value = tensor.evals()[0];
        break;
      case GROWTH_PDG_MIN:
        value = tensor.evals()[1];
        break;
      case GROWTH_ANISOTROPY:
        value = anisotropy[cell];
        break;
      case GROWTH_AREA:
        value = tensor.evals()[0] + tensor.evals()[1];
      case GROWTH_NONE:       // That cannot happen anyway
        break;
      }
      labelHeatMap[cell] = value;

      if(value > maxValue)
        maxValue = value;
      if(value < minValue)
        minValue = value;
    }

    // Round the bounds values of heat map, to make sure we have the same bound in case of loading Stretch on both
    // meshes.
    minValue = floor(minValue * 1000) / 1000;
    maxValue = ceil(maxValue * 1000) / 1000;

    using std::swap;

    // Adjust heat map if run on parents
    if(mesh->isParentAxis()) {
      mesh->clearHeatmap();
      IntIntMap& parentMap = mesh->parentLabelMap();
      IntFloatMap& labelHeat = mesh->labelHeat();
      forall(const IntIntPair& p, parentMap) {
        int mother = p.second;
        int daughter = p.first;
        if(daughter <= 0)
          continue;
        IntFloatMap::iterator found = labelHeatMap.find(mother);
        if(found != labelHeatMap.end())
          labelHeat[daughter] = found->second;
      }
    } else
      swap(labelHeatMap, mesh->labelHeat());

    if(stretch_map == GROWTH_ANISOTROPY) {
      if(anisotropyType == ONE_MINUS_KMIN_OVER_KMAX) {
        minValue = 0;
        maxValue = 2;
      }
      mesh->heatMapUnit() = "";
    } else
      mesh->heatMapUnit() = mesh->cellAxisUnit();
    mesh->heatMapDesc() = Description("2D Growth") << std::make_pair("Measure", toString(stretch_map))
                                                   << std::make_pair("Anisotropy", toString(anisotropyType));

    mesh->heatMapBounds() = Point2f(minValue, maxValue);
    mesh->showHeat();
    mesh->updateTriangles();
  }

  return true;
}
REGISTER_MESH_PROCESS(DisplayGrowth2D);

bool LoadGrowth2D::operator()(Mesh* mesh, const QString& filename)
{
  QRegularExpression k1Name("(Max Stretch Ratio|Max Strain|Max Strain Rate \\(1/h\\))");
  QRegularExpression k2Name("(Min Stretch Ratio|Min Strain|Min Strain Rate \\(1/h\\))");
  QRegularExpression k3Name;
  auto header = readFile(mesh, "2D Growth", filename, k1Name, k2Name, k3Name);
  if(header) {
    const Description& desc = mesh->cellAxisDesc();
    bool useParent = stringToBool(desc.field("Use Parent Labelling", "No"));
    if(useParent)
      mesh->setParentAxis();
    else
      mesh->setCellAxis();
    if(header.k1Match.captured(1) == "Max Strain Rate (1/h)")
      mesh->cellAxisUnit() = "1/h";
    else
      mesh->cellAxisUnit() = "";
  } else {
    if(not header.wrong_type)
      return false;
    // Try to read it as an old file
    QString k1, k2, k3;
    GrowthMeasure measure;
    Ordering order = ORDER_RELATIVE;
    float dt = 0.f;
    if(k1 == "stretchRatioMax" or k1 == "k1")
      measure = GM_STRETCH_RATIO;
    else if(k1 == "k1 (1/h)") {
      measure = GM_STRAIN;
      order = ORDER_ABSOLUTE;
      dt = 1.;
    } else {
      measure = GM_STRETCH_RATIO;
      order = ORDER_ABSOLUTE;
    }
    if(not readOldFile(mesh, "2D Growth", filename, k1, k2, k3))
      return false;
    Description axisType = Description("2D Growth") << std::make_pair("Measure", toString(measure))
                                                    << std::make_pair("Axis ordering", toString(order))
                                                    << std::make_pair("Time Step", QString::number(dt))
                                                    << std::make_pair("Time", "")
                                                    << std::make_pair("Use Parent Labelling", "");
    if(mesh->isParentAxis()) {
      mesh->cellAxisDesc() = axisType;
      mesh->cellAxisDesc()["Time"] = "End";
      mesh->cellAxisDesc()["Use Parent Labelling"] = "Yes";
    } else {
      mesh->cellAxisDesc() = axisType;
      mesh->cellAxisDesc()["Time"] = "Start";
      mesh->cellAxisDesc()["Use Parent Labelling"] = "No";
    }
  }
  return true;
}

REGISTER_MESH_PROCESS(LoadGrowth2D);

bool SaveGrowth2D::operator()(Mesh* mesh, const QString& filename)
{
  const IntSymTensorMap& cellAxis = mesh->cellAxis();
  if(cellAxis.size() == 0 or mesh->cellAxisDesc().type() != "2D Growth") {
    setErrorMessage(QString("No Stretch stored in active mesh!"));
    return false;
  }
  const Description& desc = mesh->cellAxisDesc();
  QString k1Name;
  QString k2Name;
  GrowthMeasure measure = readGrowthMeasure(desc.field("Measure"));
  float dt = desc.field("Time Step", "0.").toFloat();
  switch(measure) {
  case GM_STRETCH_RATIO:
    k1Name = "Max Stretch Ratio";
    k2Name = "Min Stretch Ratio";
    break;
  case GM_STRAIN:
  case GM_ENGINEERING_STRAIN:
    if(dt > 0) {
      k1Name = "Max Strain Rate (1/h)";
      k2Name = "Min Strain Rate (1/h)";
    } else {
      k1Name = "Max Strain";
      k2Name = "Min Strain";
    }
    break;
  }
  if(not this->saveFile(mesh, filename, k1Name, k2Name))
    return false;
  return true;
}

REGISTER_MESH_PROCESS(SaveGrowth2D);
} // namespace process
} // namespace lgx
