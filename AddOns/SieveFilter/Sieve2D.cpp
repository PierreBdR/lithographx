#include "Sieve2D.hpp"

#include <Algorithms.hpp>

#include <QFile>
#include <QTextStream>
#include <QString>

#include <unordered_set>

namespace lgx {
namespace process {
namespace sieve {

Sieve2D::Sieve2D(const HVecUS& data,
                 const Point2u& size,
                 FILTER_TYPE first_filter,
                 NUM_DIRS ndir,
                 SieveProgress progress)
  : Sieve(data)
  , _ndir(ndir)
  , _N(size[1])
  , _M(size[0])
  , _borders{Point2i(0, -1), Point2i(_M-1, -1)  , Point2i(-1  , 0), Point2i(-1  , _N-1),
             Point2i(0, 0) , Point2i(0   , _N-1), Point2i(_M-1, 0), Point2i(_M-1, _N-1)}
{
  if(error()) return;

  if(_N < 0 or _M < 0) {
    _errorString = "Error, the image is too large to be processed.";
    return;
  }
  if(data.size() == 0) {
    _errorString = "Error, the image is empty.";
    return;
  }
  if(ndir != NDIR_4 and ndir != NDIR_8) {
    _errorString = "Error, in 2D there must be either 4 or 8 directions";
    return;
  }

  if(not progress.advance(0)) {
    _errorString = "Progress abort";
    return;
  }

  _offsets = {-1, +1, -_M, +_M, -1-_M, -1+_M, +1-_M, +1+_M};

  // Allocate memory for the pel graph
  if(not prepareMesh(first_filter, progress)) {
    _errorString = "Failed to prepare the mesh.";
    return;
  }
  if(not findConnectedSets(progress)) {
    _errorString = "Failed to find connected sets.";
    return;
  }
  if(not findAdjacencyAndExtrema(progress)) {
    _errorString = "Failed to find adjacency and extrema.";
    return;
  }

  //saveFiles("init_");
}

bool Sieve2D::writeTo(HVecUS& output) const
{
  if(output.size() != _M*_N) {
    _errorString = "Error, output doesn't have the same size as the current image";
    return false;
  }

#pragma omp parallel for
  for(intptr_t y = 0 ; y < _N ; ++y) {
    intptr_t pos = _M*y;
    for(intptr_t x = 0 ; x < _M ; ++x, ++pos) {
      auto root = _G[pos].root_pel;
      if(root < 0) root = pos;
      output[pos] = _image[root];
    }
  }

  return true;
}

void Sieve2D::saveFiles(const QString& prefix)
{
  std::vector<int8_t> extrems(_G.size(), 0);

  for(auto pos: _minima)
    extrems[pos] = 1;

  for(auto pos: _maxima)
    extrems[pos] = 2;

  QFile image(prefix + "image.csv");
  QFile zones(prefix + "zones.csv");
  QFile adj(prefix + "adj.csv");
  QFile flags(prefix + "flags.csv");
  QFile roots(prefix + "roots.csv");
  QFile extrem(prefix + "extrema.csv");

  image.open(QIODevice::WriteOnly);
  zones.open(QIODevice::WriteOnly);
  adj.open(QIODevice::WriteOnly);
  flags.open(QIODevice::WriteOnly);
  roots.open(QIODevice::WriteOnly);
  extrem.open(QIODevice::WriteOnly);

  QTextStream ts_image(&image);
  QTextStream ts_zones(&zones);
  QTextStream ts_adj(&adj);
  QTextStream ts_flags(&flags);
  QTextStream ts_roots(&roots);
  QTextStream ts_extrem(&extrem);

  for(int32_t x = 0 ; x < _M ; ++x) {
    ts_image << ",";
    ts_zones << ",";
    ts_adj << ",";
    ts_flags << ",";
    ts_roots << ",";
    ts_extrem << ",";

    ts_image << x;
    ts_zones << x;
    ts_adj << x;
    ts_flags << x;
    ts_roots << x;
    ts_extrem << x;
  }
  ts_image << "\r\n";
  ts_zones << "\r\n";
  ts_adj << "\r\n";
  ts_flags << "\r\n";
  ts_roots << "\r\n";
  ts_extrem << "\r\n";

  int32_t pos = 0;
  for(int32_t y = 0 ; y < _N ; ++y) {
    ts_image << y;
    ts_zones << y;
    ts_adj << y;
    ts_flags << y;
    ts_roots << y;
    ts_extrem << y;
    for(int32_t x = 0 ; x < _M ; ++x, ++pos) {
      auto& cur = _G[pos];
      ts_image << ",";
      ts_zones << ",";
      ts_adj << ",";
      ts_flags << ",";
      ts_roots << ",";
      ts_extrem << ",";

      ts_image << _image[pos];
      ts_zones << cur.next_pel;
      ts_adj << cur.next_adj;
      ts_flags << _adj_flags[pos];
      ts_roots << cur.root_pel;
      ts_extrem << extrems[pos];
    }
    ts_image << "\r\n";
    ts_zones << "\r\n";
    ts_adj << "\r\n";
    ts_flags << "\r\n";
    ts_roots << "\r\n";
    ts_extrem << "\r\n";
  }
}

bool Sieve2D::prepareMesh(FILTER_TYPE first_filter, SieveProgress& progress)
{
  auto progress_step = _N / 33;
  if(progress_step < 1)
    progress_step = 1;
  double progress_ratio = 33. / _N;

  for(intptr_t y = 0 ; y < _N ; ++y) {
    if(y % progress_step == 0)
      if(not progress.advance(y * progress_ratio))
        return false;

    intptr_t pos = _M*y;
    for(intptr_t x = 0 ; x < _M ; ++x, ++pos) {
      auto val = _image[pos];
      auto val_min = val, val_max = val;
      ushort lt_val = 0, gt_val = std::numeric_limits<ushort>::max();
      for(uint8_t dir = 0 ; dir < _ndir ; ++dir) {
        if(_borders[dir].x() != x and _borders[dir].y() != y) {
          auto nval = _image[pos + _offsets[dir]];
          if(nval < val_min) val_min = nval;
          if(nval > val_max) val_max = nval;
          if(nval <= val and nval > lt_val) lt_val = nval;
          if(nval >= val and nval < gt_val) gt_val = nval;
        }
      }
      if(first_filter == FT_OPENING) {
        if(val_max == val)
          val = lt_val;
      } else if(first_filter == FT_CLOSING) {
        if(val_min == val)
          val = gt_val;
      } else if(val_min == val)
        val = gt_val;
      else if(val_max == val)
        val = lt_val;
      _image[pos] = val;
    }
  }
  return true;
}

bool Sieve2D::findConnectedSets(SieveProgress& progress)
{
  // In this function:
  //  - root_pel will point to the root, that may be the pel itself
  //  - next_adj is 1 if the pel has been processed, 0 otherwise
  //  - next_pel is set to the index of the next pel of same intensity in the neighborhood
  //  - adj_flags contains first the direction in which the search should be
  //    continued and, after processing, the direction where different value
  //    pels are found.
  uint8_t dir_left, dir_right, dir_top, dir_bottom;
  uint8_t dir_top_left = 0, dir_top_right = 0, dir_bottom_left = 0, dir_bottom_right = 0;
  if(_ndir == NDIR_8) {
    dir_left = LEFT | TOP_LEFT | BOTTOM_LEFT;
    dir_right = RIGHT | TOP_RIGHT | BOTTOM_RIGHT;
    dir_top = TOP_LEFT | TOP | TOP_RIGHT;
    dir_bottom = BOTTOM_LEFT | BOTTOM | BOTTOM_RIGHT;
    dir_top_left = LEFT | TOP | TOP_LEFT | BOTTOM_LEFT | TOP_RIGHT;
    dir_top_right = RIGHT | TOP | TOP_RIGHT | BOTTOM_RIGHT | TOP_LEFT;
    dir_bottom_left = LEFT | BOTTOM | BOTTOM_LEFT | TOP_LEFT | BOTTOM_RIGHT;
    dir_bottom_right = RIGHT | BOTTOM | BOTTOM_RIGHT | TOP_RIGHT | BOTTOM_LEFT;
  } else {
    dir_left = LEFT | TOP | BOTTOM;
    dir_right = RIGHT | TOP | BOTTOM;
    dir_top = LEFT | TOP | RIGHT;
    dir_bottom = LEFT | BOTTOM | RIGHT;
  }

  intptr_t processed = 0;
  intptr_t pos = 0;
  auto progress_step = _G.size() / 33;
  if(progress_step < 1)
    progress_step = 1;
  double progress_ratio = 33. / _G.size();

  for(int32_t y = 0 ; y < _N ; ++y) {
    for(int32_t x = 0 ; x < _M ; ++x, ++pos) {

      auto& cur = _G[pos];
      auto val = _image[pos];

      // Test whether the current position has already been processed
      if(cur.next_adj == 0) {
        cur.root_pel = pos;
        cur.next_adj = 1;
        uint8_t other_regions = 0;
        auto last_pel = pos;

        // Try 4 valid directions to move from root to pel
        if(x != _M-1) {
          if(val == _image[pos+1]) {
            _G[last_pel].next_pel = pos+1;
            last_pel = pos+1;
            _adj_flags[last_pel] = RIGHT;
            _G[last_pel].next_adj = 1;
          } else
            other_regions |= RIGHT;
        }

        if(y != _N-1) {
          if(val == _image[pos+_M]) {
            _G[last_pel].next_pel = pos+_M;
            last_pel = pos+_M;
            _adj_flags[last_pel] = BOTTOM;
            _G[last_pel].next_adj = 1;
          } else
            other_regions |= BOTTOM;
        }

        if(_ndir == NDIR_8) {
          if(x != 0 and y != _N-1) {
            if(val == _image[pos-1+_M]) {
              _G[last_pel].next_pel = pos-1+_M;
              last_pel = pos-1+_M;
              _adj_flags[last_pel] = BOTTOM_LEFT;
              _G[last_pel].next_adj = 1;
            } else
              other_regions |= BOTTOM_LEFT;
          }
          if(x != _M-1 and y != _N-1) {
            if(val == _image[pos+1+_M]) {
              _G[last_pel].next_pel = pos+1+_M;
              last_pel = pos+1+_M;
              _adj_flags[last_pel] = BOTTOM_RIGHT;
              _G[last_pel].next_adj = 1;
            } else
              other_regions |= BOTTOM_RIGHT;
          }
        }

        // geck other directions for other regions
        if(x != 0 and _image[pos-1] != val)
          other_regions |= LEFT;
        if(y != 0 and _image[pos-_M] != val)
          other_regions |= TOP;
        if(_ndir == NDIR_8) {
          if(x != 0 and y != 0 and _image[pos-1-_M] != val)
            other_regions |= TOP_LEFT;
          if(x != _M-1 and y != 0 and _image[pos+1-_M] != val)
            other_regions |= TOP_RIGHT;
        }

        // Store the direction to other regions
        _adj_flags[pos] = other_regions;

        // End the linked list of pels
        _G[last_pel].next_pel = -1;

        if(++processed % progress_step == 0)
          if(not progress.advance(33 + processed * progress_ratio)) return false;

        // Loop over all pels added to the region to expand it to the whole area of common value
        for(auto next_pel = cur.next_pel ; next_pel >= 0 ;
            next_pel = _G[next_pel].next_pel) {
          auto& node = _G[next_pel];
          auto val = _image[next_pel];

          // Set the root of the region
          node.root_pel = pos;

          // Get the previous direction
          auto last_dirn = _adj_flags[next_pel];
          auto x0 = offX(next_pel);
          auto y0 = offY(next_pel);

          other_regions = 0;

          // Consider all possible directions from which we need
          // to expand, but do not try to come back on our tracks
          if((last_dirn & dir_left) and x0 != 0) {
            if(val == _image[next_pel-1]) {
              if(_G[next_pel-1].next_adj == 0) {
                _G[last_pel].next_pel = next_pel-1;
                last_pel = next_pel-1;
                _adj_flags[last_pel] = LEFT;
                _G[last_pel].next_adj = 1;
              }
            } else
              other_regions |= LEFT;
          }

          if((last_dirn & dir_right) and x0 != _M-1) {
            if(val == _image[next_pel+1]) {
              if(_G[next_pel+1].next_adj == 0) {
                _G[last_pel].next_pel = next_pel+1;
                last_pel = next_pel+1;
                _adj_flags[last_pel] = RIGHT;
                _G[last_pel].next_adj = 1;
              }
            } else
              other_regions |= RIGHT;
          }

          if((last_dirn & dir_top) and y0 != 0) {
            if(val == _image[next_pel-_M]) {
              if(_G[next_pel-_M].next_adj == 0) {
                _G[last_pel].next_pel = next_pel-_M;
                last_pel = next_pel-_M;
                _adj_flags[last_pel] = TOP;
                _G[last_pel].next_adj = 1;
              }
            } else
              other_regions |= TOP;
          }

          if((last_dirn & dir_bottom) and y0 != _N-1) {
            if(val == _image[next_pel+_M]) {
              if(_G[next_pel+_M].next_adj == 0) {
                _G[last_pel].next_pel = next_pel+_M;
                last_pel = next_pel+_M;
                _adj_flags[last_pel] = BOTTOM;
                _G[last_pel].next_adj = 1;
              }
            } else
              other_regions |= BOTTOM;
          }

          if(_ndir == NDIR_8) {
            if((last_dirn & dir_top_left) and x0 != 0 and y0 != 0) {
              if(val == _image[next_pel-1-_M]) {
                if(_G[next_pel-1-_M].next_adj == 0) {
                  _G[last_pel].next_pel = next_pel-1-_M;
                  last_pel = next_pel-1-_M;
                  _adj_flags[last_pel] = TOP_LEFT;
                  _G[last_pel].next_adj = 1;
                }
              } else
                other_regions |= TOP_LEFT;
            }

            if((last_dirn & dir_bottom_left) and x0 != 0 and y0 != _N-1) {
              if(val == _image[next_pel-1+_M]) {
                if(_G[next_pel-1+_M].next_adj == 0) {
                  _G[last_pel].next_pel = next_pel-1+_M;
                  last_pel = next_pel-1+_M;
                  _adj_flags[last_pel] = BOTTOM_LEFT;
                  _G[last_pel].next_adj = 1;
                }
              } else
                other_regions |= BOTTOM_LEFT;
            }

            if((last_dirn & dir_top_right) and x0 != _M-1 and y0 != 0) {
              if(val == _image[next_pel+1-_M]) {
                if(_G[next_pel+1-_M].next_adj == 0) {
                  _G[last_pel].next_pel = next_pel+1-_M;
                  last_pel = next_pel+1-_M;
                  _adj_flags[last_pel] = TOP_RIGHT;
                  _G[last_pel].next_adj = 1;
                }
              } else
                other_regions |= TOP_RIGHT;
            }

            if((last_dirn & dir_bottom_right) and x0 != _M-1 and y0 != _N-1) {
              if(val == _image[next_pel+1+_M]) {
                if(_G[next_pel+1+_M].next_adj == 0) {
                  _G[last_pel].next_pel = next_pel+1+_M;
                  last_pel = next_pel+1+_M;
                  _adj_flags[last_pel] = BOTTOM_RIGHT;
                  _G[last_pel].next_adj = 1;
                }
              } else
                other_regions |= BOTTOM_RIGHT;
            }
          }

          // Store directions to other regions
          _adj_flags[next_pel] = other_regions;

          // End the linked list of pels
          _G[last_pel].next_pel = -1;

          if(++processed % progress_step == 0)
            if(not progress.advance(33 + processed * progress_ratio)) return false;
        }
      }
    }
  }
  return true;
}

bool Sieve2D::findAdjacencyAndExtrema(SieveProgress& progress)
{
  // At that point, a root node has itself as root_pel and next_adj is 1 for all node
  std::vector<uint8_t> extrema(_G.size(), 0);

  auto process = [&extrema, this](intptr_t pos) -> bool {
    std::unordered_set<intptr_t> known_neighbors;
    known_neighbors.reserve(25);
    auto& root = _G[pos];

    // Is the current node a root node?
    if(root.root_pel == pos) {
      intptr_t area = 0;
      intptr_t prev_adj = -1;
      int8_t sign = 0;
      auto val = _image[pos];

      known_neighbors.clear();

      // For the whole area from this root
      for(intptr_t pel = pos ; pel >= 0 ; pel = _G[pel].next_pel) {
        ++area;

        // Find new regions adjacent to the current pel
        uint8_t flags = 0;
        uint8_t bit = 1;
        uint8_t off = 0;

        _G[pel].next_adj = -1;

        for(uint8_t dirns = _adj_flags[pel] ; dirns != 0 ;
            dirns >>= 1, bit <<= 1, ++off) {
          if(dirns & 1) {
            auto adj = pel + _offsets[off];
            auto adj_root = (_G[adj].root_pel >= 0) ? _G[adj].root_pel : adj;
            // If the root wasn't known yet
            if(known_neighbors.insert(adj_root).second) {
              flags |= bit;
              sign |= (_image[adj_root] > val ? 2 : 1);
            }
          }
        }

        // Set node data
        if(prev_adj == -1)
          prev_adj = pel;
        else if(flags) {
          _G[prev_adj].next_adj = pel;
          prev_adj = pel;
        }

        _adj_flags[pel] = flags;
      }

      root.root_pel = -area;

      // At that point, sign is 1 if the area is a max, 2 if it's a min and 3 if neither
      extrema[pos] = sign;
    }
    return true;
  };

  auto size = intptr_t(_G.size());

  double progress_ratio = 33. / size;
  SieveProgress newProgress([&progress, progress_ratio](int value) -> bool {
                                return progress.advance(66 + int(value * progress_ratio)); });

  bool result = util::apply_kernel_it(process, util::Counter(0), util::Counter(size), newProgress);
  if(newProgress.canceled())
    Process::userCancel();

  if(not result)
    return false;

  for(intptr_t pos = 0 ; pos < size ; ++pos) {
    if(extrema[pos] == 2) {
      _minima.push_back(pos);
      auto area = -_G[pos].root_pel;
      if(_min_min_area > area)
        _min_min_area = area;
    } else if(extrema[pos] == 1) {
      _maxima.push_back(pos);
      auto area = -_G[pos].root_pel;
      if(_max_min_area > area)
        _max_min_area = area;
    }
  }

  return true;
}

} // namespace sieve
} // namespace process
} // namespace lgx
