/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef PROCESSTHREAD_HPP
#define PROCESSTHREAD_HPP

#include <Process.hpp>

#include <QEvent>
#include <QMutex>
#include <QThread>

namespace lgx {
struct LGX_EXPORT ProcessUpdateViewEvent : public QEvent {
  ProcessUpdateViewEvent()
    : QEvent(QEvent::User)
  {
  }
};

struct LGX_EXPORT ProcessSystemCommand : public QEvent {
  ProcessSystemCommand(process::Process* p, process::SystemCommand cmd, const process::ParmList& prms)
    : QEvent(QEvent::User)
    , process(p)
    , command(cmd)
    , parms(prms)
  {
  }

  process::Process* process;
  process::SystemCommand command;
  const process::ParmList& parms;
};

class LGX_EXPORT ProcessThread : public QThread {
  Q_OBJECT
public:
  ProcessThread(process::Process* proc, const process::ParmList& prms, QObject* parent)
    : QThread(parent)
    , process(proc)
    , parms(prms)
  {
  }

  virtual ~ProcessThread() {
  }

  void run();

  bool exitStatus() const {
    return _status;
  }
  QString errorMessage() const {
    return _error;
  }
  QString warningMessage() const {
    return _warning;
  }

protected:
  process::Process* process;
  process::ParmList parms;
  bool _status;
  QString _error, _warning;
};
} // namespace lgx
#endif
