/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "About.hpp"
#include "LGXVersion.hpp"
#include <QThread>
#include <QString>

QString aboutText()
{
  return QString("<h2>LithoGraphX " VERSION "</h2>"
                 "<p><a href='http://lithographx.readthedocs.io/en/latest/changelog.html'>"
                 "<b>What's new in version " VERSION "</b></a></p>"
                 "<p>A tool for 2.5 & 3D cell segmentation, image processing, and more!</p>"
                 "<p>&#169; 2015 Barbier de Reuille, Pierre</p>"
                 "<p>Contact: pierre _at_ barbierdereuille _dot_ net</p>"
                 /*
                  *"Please visit "
                  *"<html><style type='text/css'></style><a href='http://www.LithoGraphX.org'>LithoGraphX.org</a></html>"
                  *" for more information.<br/><br/>"
                  */
                 "<p>LithoGraphX is a fork of the MorphoGraphX project and is based on the version 1.0 rev. 1256<br/>"
                 "If you use LithoGraphX in your research, please "
                 "<a href='http://dx.doi.org/10.7554/eLife.05864'>reference</a>"
                 " us, including the LithoGraphX website: <a href='http://www.lithographx.com'>http://www.lithographx.com</a>.</p>"
                 "<p>LithoGraphX is licensed under the "
                 "<a href='http://www.gnu.org/licenses/gpl-3.0.html'>GNU GPLv3.</a></p>"
                 "<ul>"
                 "<li>Documentation and tutorials available at <a href='http://lithographx.readthedocs.io'>"
                 "http://lithographx.readthedocs.io</a></li>"
                 "<li>For help and support use the <a href='https://bitbucket.org/PierreBdR/lithographx/issues?status=new&status=open'>"
                 "Issue tracker</a></li>"
                 "<li>List of contributors: <a href='http://lithographx.readthedocs.io/en/latest/Copyright.html'>"
                 "http://lithographx.readthedocs.io/en/latest/Copyright.html</a></li>"
"                 </ul>"
                 "Funding support:<dl>"
                 "<ul>"
                 "<li> SystemsX.ch & the Swiss National Science Foundation</li>"
                 "<li> University of Bern, Switzerland </li>"
                 "</ul>");
}

QString introText()
{
  return QString("LithoGraphX version: %2\n   created in thread: 0x%1")
         .arg(quintptr(QThread::currentThread()), -16).arg(lgxVersion.toString());
}
