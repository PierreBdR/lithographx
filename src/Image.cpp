/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "Image.hpp"

#include <Defer.hpp>
#include <Information.hpp>
#include <Misc.hpp>
#include <Parms.hpp>
#include <Process.hpp>
#include <Progress.hpp>

#include <CImg.h>
#include <cmath>
#include <limits>
#include <QDomDocument>
#include <QDomElement>
#include <QImageReader>
#include <QImageWriter>
#include <QProcess>
#include <string.h>
#include <tuple>

extern "C" {
#include <tiffio.h>
#include <tiff.h>
}

#ifdef _OPENMP
#  include <omp.h>
#endif

namespace lgx {
using namespace cimg_library;
typedef CImg<ushort> CImgUS;

QStringList supportedImageReadFormats()
{
  static QStringList formats;
  if(formats.empty()) {
    formats << "dlm"
            << "tif"
            << "cimg"
            << "Auto";
    formats.sort();
  }
  return formats;
}

QStringList supportedImageWriteFormats()
{
  static QStringList formats;
  if(formats.empty()) {
    formats << "dlm"
            << "tif"
            << "cimg"
            << "CImg Auto";
    formats.sort();
  }
  return formats;
}

namespace {
void save_dlm(const QString& fn, const CImgUS& img) { img.save_dlm(fn.toLocal8Bit()); }

void save_cimg(const QString& fn, const CImgUS& img) { img.save_cimg(fn.toLocal8Bit()); }

void save_tiff(const QString& fn, const CImgUS& img) { img.save_tiff(fn.toLocal8Bit()); }

void save_auto(const QString& fn, const CImgUS& img) { img.save(fn.toLocal8Bit()); }
} // namespace

void saveImage(QString filename, const Image5D& data, QString type, unsigned int nb_digits)
{
  if(type.isEmpty()) {
    if(filename.contains("."))
      type = filename.mid(filename.indexOf('.') + 1);
    else
      throw ImageError("saveImage: Either the type is set, or the filename must specify an extension.");
  }

  if(data.nbTimePoints() > 1)
    throw ImageError("saveImage: Error, cannot save images with more than one time point.");

  if(data.nbChannels() != 1 and data.nbChannels() != 3 and data.nbChannels() != 4)
    throw ImageError("saveImage: Error, can only save images with 1, 3 or 4 channels.");

  const Point5u& size = data.size;

  unsigned int min_digits = (unsigned int)ceil(log10((float)size.z()));

  if(nb_digits > 0 and nb_digits < min_digits)
    throw ImageError(QString("saveImage: Error, you specified %1 digits for %2 images. This is not enough.")
                                .arg(nb_digits).arg(size.z()));

  if(type != "CImg Auto") {
    if(filename.contains(".")) {
      QString ext = filename.mid(filename.lastIndexOf(".") + 1);
      if(ext.toLower() != type)
        filename += "." + type;
    } else {
      filename += "." + type;
    }
  } else if(nb_digits == 0)
    nb_digits = min_digits;

  void (*save)(const QString& fn, const CImgUS& img) = 0;
  if(type == "dlm")
    save = &save_dlm;
  else if(type == "cimg")
    save = &save_cimg;
  else if(type == "tif")
    save = &save_tiff;
  else
    save = &save_auto;
  if(nb_digits == 0) {
    CImgUS image(data.data(), size.x(), size.y(), size.z(), 1, true);
    save(filename, image);
  } else {
    int index_dot = filename.lastIndexOf(".");
    QString prefix = filename.left(index_dot);
    QString suffix = filename.mid(index_dot);
    unsigned int shift = size.x() * size.y();
    for(unsigned int z = 0; z < size.z(); ++z) {
      CImgUS image(&data[z * shift], size.x(), size.y(), 1, 1, true);
      QString fn = QString("%1%2%3").arg(prefix).arg(z, nb_digits, 10, QLatin1Char('0')).arg(suffix);
      save(fn, image);
    }
  }
}

HVecUS resize(const HVecUS& data, const Point3u& before, const Point3u& after, bool center)
{
  HVecUS result;
  size_t tot_before = size_t(before.x()) * before.y() * before.z();
  size_t tot_after = size_t(after.x()) * after.y() * after.z();
  if(tot_before != data.size())
    return result;
  result.resize(tot_after, 0);
  uint length_x = std::min(before.x(), after.x());
  uint length_y = std::min(before.y(), after.y());
  uint length_z = std::min(before.z(), after.z());
  uint before_shift_x = 0;
  uint before_shift_y = 0;
  uint before_shift_z = 0;
  uint after_shift_x = 0;
  uint after_shift_y = 0;
  uint after_shift_z = 0;
  uint before_inter_y = before.x() * (before.y() - length_y);
  uint after_inter_y = after.x() * (after.y() - length_y);
  if(center) {
    if(length_x == before.x())
      after_shift_x = (after.x() - length_x) / 2;
    else
      before_shift_x = (before.x() - length_x) / 2;
    if(length_y == before.y())
      after_shift_y = (after.y() - length_y) / 2;
    else
      before_shift_y = (before.y() - length_y) / 2;
    if(length_z == before.z())
      after_shift_z = (after.z() - length_z) / 2;
    else
      before_shift_z = (before.z() - length_z) / 2;
  }
  const ushort* b = &data[(before_shift_z * before.y() + before_shift_y) * before.x() + before_shift_x];
  ushort* a = &result[(after_shift_z * after.y() + after_shift_y) * after.x() + after_shift_x];
  for(uint z = 0; z < length_z; ++z) {
    for(uint y = 0; y < length_y; ++y) {
      memcpy(a, b, sizeof(ushort) * length_x);
      b += before.x();
      a += after.x();
    }
    b += before_inter_y;
    a += after_inter_y;
  }
  return result;
}

Image5D::Image5D()
  : ImageInfo()
{
}

Image5D::Image5D(const QString& filename)
  : ImageInfo(filename)
{ }

Image5D::Image5D(const QString& filename, ushort* data, const Point3u& size, const Point3f& step)
  : ImageInfo(filename, size, step)
  , _data(data)
  , strides((size.x() > 1 ? 1 : 0),
            (size.y() > 1 ? size.x() : 0),
            (size.z() > 1 ? size.x()*size.y() : 0),
            0,
            0)
{
}

Image5D::Image5D(const Image5D& other)
  : ImageInfo(other)
  , strides(other.strides)
  , minc(other.minc)
  , maxc(other.minc)
  , brightness(other.brightness)
  , compression_level(other.compression_level)
  , _data(other._data)
  , allocated(false)
{
}

Image5D& Image5D::operator=(const Image5D& other)
{
  ImageInfo::operator=(other);
  if(allocated)
    delete [] _data;

  strides = other.strides;
  minc = other.minc;
  maxc = other.minc;
  brightness = other.brightness;
  compression_level = other.compression_level;
  _data = other._data;
  allocated = false;
  return *this;
}

void Image5D::updateStrides()
{
  Point5s orderedSizes, orderedStrides;
  for(int i = 0 ; i < 5 ; ++i)
    orderedSizes[i] = size[axisOrder[i]];

  size_t acc = 1;
  for(size_t i = 0 ; i < 5 ; ++i) {
    orderedStrides[i] = (orderedSizes[i] > 1 ? acc : 0);
    acc *= orderedSizes[i];
  }

  for(int i = 0 ; i < 5 ; ++i)
    strides[axisOrder[i]] = orderedStrides[i];
}

void Image5D::free()
{
  if(allocated)
    delete [] _data;
  _data = nullptr;
}

void Image5D::allocate(const Point3u& s)
{
  size = Point5u(s.x(), s.y(), s.z(), 1, 1);
  allocate();
}

void Image5D::allocate(const Point5u& s)
{
  size = s;
  allocate();
}

void Image5D::allocate()
{
  minc = 0;
  maxc = 0;
  depth = 16;
  size_t s = size_t(size[0])*size[1]*size[2]*size[3]*size[4];
  if(s > 0) {
    if(_data and allocated)
      delete [] _data;
    allocated = true;
    _data = new ushort[s];
  } else {
    if(allocated)
      delete [] _data;
    allocated = false;
    _data = 0;
  }
  updateStrides();
}

namespace {
QStringList viewNames = { "column", "line", "plane", "channel", "timepoint" };
}

template <size_t N>
Image5D Image5D::view(uint n)
{
  if(n >= size[N])
    throw ImageError(QString("Error, cannot extract %1 %2 out of %3.")
                     .arg(viewNames[N])
                     .arg(n)
                     .arg(size[N]));
  if(not _data)
    throw ImageError("Cannot take a view of an image without data.");
  auto result = *this;
  result._data += strides[N] * n;
  result.strides[N] = 0;
  result.size[N] = 1;
  return result;
}

template Image5D Image5D::view<0>(uint);
template Image5D Image5D::view<1>(uint);
template Image5D Image5D::view<2>(uint);
template Image5D Image5D::view<3>(uint);
template Image5D Image5D::view<4>(uint);

Image5D::~Image5D()
{
  if(_data and allocated)
    delete [] _data;
  allocated = false;
  _data = 0;
}

namespace {
void tiffErrorHandler(const char* module, const char* fmt, va_list ap)
{
  SETSTATUS("TIFF Error in module " << module);
  vfprintf(stderr, fmt, ap);
  fprintf(stderr, "\n");
  fflush(stderr);
}

struct StripDataIncrement {
  int start_row;
  int start_column;
  int shift_row;
  int shift_column;
  int line_end;
};

struct TileDataIncrement {
  int start_row;
  int start_column;
  int tile_width;
  int tile_height;
  int line_width;
};

template <typename T>
struct ImageBuffer {
  ImageBuffer(T* buf)
      : buffer(buf)
  {
  }
  ImageBuffer(const ImageBuffer&) = default;
  ImageBuffer(ImageBuffer&&) = default;

  ImageBuffer& operator=(const ImageBuffer&) = default;
  ImageBuffer& operator=(ImageBuffer&&) = default;

  T* buffer;
  ImageBuffer& operator++()
  {
    ++buffer;
    return *this;
  }
  ImageBuffer& operator+=(int n)
  {
    buffer += n;
    return *this;
  }
  ImageBuffer& operator-=(int n)
  {
    buffer -= n;
    return *this;
  }
  uint operator*() const;
};

template <> uint ImageBuffer<signed char>::operator*() const
{
  int val = *buffer;
  if(val < 0)
    return 0;
  return uint(val << 8);
}

template <> uint ImageBuffer<unsigned char>::operator*() const { return uint(*buffer) << 8; }

template <> uint ImageBuffer<short>::operator*() const
{
  int val = *buffer;
  if(val < 0)
    return 0;
  return uint(val);
}

template <> uint ImageBuffer<ushort>::operator*() const { return uint(*buffer); }

template <> uint ImageBuffer<int>::operator*() const
{
  int val = *buffer;
  if(val < 0)
    return 0;
  return uint(val >> 16);
}

template <> uint ImageBuffer<uint>::operator*() const
{
  int val = *buffer;
  return (val >> 16);
}

template <> uint ImageBuffer<float>::operator*() const
{
  float val = *buffer;
  if(val < 0)
    return 0;
  return uint(floor(val));
}

template <typename T> ImageBuffer<T> imageBuffer(T* buf) { return ImageBuffer<T>(buf); }

template <typename T>
void readStrip(Image5D& data, ushort*& p, const StripDataIncrement& incr, ImageBuffer<T> buf, size_t nb_pixels,
               uint16_t spp, size_t sel_channel)
{
  int col_count = 0;
  buf += sel_channel;
  for(size_t i = 0; i < nb_pixels; ++i, buf += spp) {
    uint val = *buf;
    val *= data.brightness;
    if(val > 0xFFFF)
      *p = 0xFFFF;
    else
      *p = ushort(val);
    p += incr.shift_column;
    if(++col_count >= incr.line_end) {
      col_count = 0;
      p += incr.shift_row;
    }
    if(data.minc > val)
      data.minc = val;
    if(data.maxc < val)
      data.maxc = val;
  }
}

template <typename T>
void readTile(Image5D& data, ushort* p, const TileDataIncrement& incr,
              ImageBuffer<T> buf, size_t nb_rows, size_t nb_cols)
{
  int col_count = 0;
  for(size_t y = 0 ; y < nb_rows ; ++y) {
    for(size_t x = 0 ; x < nb_cols ; ++x, ++buf) {
      uint val = *buf;
      val *= data.brightness;
      if(val > 0xFFFF)
        *p = 0xFFFF;
      else
        *p = ushort(val);
      p++;
      if(data.minc > val)
        data.minc = val;
      if(data.maxc < val)
        data.maxc = val;
    }
    buf += incr.tile_width - nb_cols;
    p -= nb_cols + incr.line_width;
  }
}

struct TIFFHandler {
  typedef TIFF* pointer;
  TIFFHandler(TIFF* t)
      : tif(t)
  {
  }

  TIFFHandler(TIFFHandler&& other)
    : tif(other.tif)
  { other.tif = nullptr; }

  TIFFHandler& operator=(TIFFHandler&& other)
  {
    free();
    tif = other.tif;
    other.tif = nullptr;
    return *this;
  }

  ~TIFFHandler()
  {
    free();
  }

  operator bool() const { return bool(tif); }

  operator pointer() { return tif; }

private:
  void free() {
    if(tif)
      TIFFClose(tif);
    tif = 0;
  }

  TIFF* tif;
};

struct TIFFAllocated {
  TIFFAllocated(tdata_t t)
      : tif(t)
  {
  }

  ~TIFFAllocated()
  {
    if(tif)
      _TIFFfree(tif);
    tif = 0;
  }

  operator bool() const { return bool(tif); }

  operator tdata_t() { return tif; }

  tdata_t& pointer() { return tif; }

  tdata_t tif;
};

#define NB_UNITS 23

const QString units[NB_UNITS] = {
  "ym",  // yocto - 10^-24
  "zm",  // zepto - 10^-21
  "am",  // atto  - 10^-18
  "fm",  // femto - 10^-15
  "pm",  // pico  - 10^-12
  "nm",  // nano  - 10^-9
  "um",  // micro - 10^-6
  "micron",  // micro - 10^-6
  QString::fromWCharArray(L"\xb5m"),    // micro - 10^-6
  "mm",  // milli - 10^-3
  "cm",  // centi - 10^-2
  "dm",  // deci  - 10^-1
  "m",   // unit  - 10^0
  "dam", // deca  - 10^1
  "hm",  // hecto - 10^2
  "km",  // kilo  - 10^3
  "Mm",  // mega  - 10^6
  "Gm",  // giga  - 10^9
  "Tm",  // tera  - 10^12
  "Pm",  // peta  - 10^15
  "Em",  // exa   - 10^18
  "Zm",  // zetta - 10^21
  "Ym"   // yota  - 10^24
};

const float unit_value[NB_UNITS] = {
  1e-24f, 1e-21f, 1e-18f, 1e-15f, 1e-12f, 1e-9f, 1e-6f, 1e-6f, 1e-6f, 1e-3f, 1e-2f,
  1e-1f, 1.f, 1e1f, 1e2f, 1e3f, 1e6f, 1e9f, 1e12f, 1e15f, 1e18f, 1e21f, 1e24f
};

std::array<ImageInfo::Axis, 5> readOMETIFFDimensionOrder(const QDomElement& pixels)
{
  QString val = pixels.attribute("DimensionOrder", "XYZCT");
  if(val.startsWith("XY") and val.contains('Z') and val.contains('C') and val.contains('T')) {
    std::array<ImageInfo::Axis, 5> order;
    order[0] = ImageInfo::X;
    order[1] = ImageInfo::Y;
    auto idx = val.indexOf('Z');
    order[idx] = ImageInfo::Z;
    idx = val.indexOf('C');
    order[idx] = ImageInfo::C;
    idx = val.indexOf('T');
    order[idx] = ImageInfo::T;
    return order;
  }
  SETSTATUS("Error, the DimensionOrder attribute of the OME TIFF must contain X, Y, Z, C and T and start with 'XY'.");
  auto bad = ImageInfo::T;
  return { bad, bad, bad, bad, bad };
}

float readOMETIFFresolution(const QDomElement& pixels, QString axis)
{
  bool ok;
  float unit = 0;
  QString val;
  val = pixels.attribute(QString("PhysicalSize%1Unit").arg(axis));
  if(!val.isEmpty()) {
    for(size_t i = 0; i < NB_UNITS; ++i) {
      if(units[i] == val) {
        unit = unit_value[i];
        break;
      }
    }
  }
  if(unit == 0)
    unit = 1e-6f;
  val = pixels.attribute(QString("PhysicalSize%1").arg(axis));
  if(!val.isEmpty()) {
    float res = val.toDouble(&ok);
    if(not ok) {
      SETSTATUS(QString("Invalid OME TIFF property PhysicalSize%1").arg(axis));
      return false;
    }
    return res * unit;
  }
  return -1;
}

int readOMETIFFSize(const QDomElement& pixels, QString axis)
{
  bool ok;
  QString val = pixels.attribute(QString("Size%1").arg(axis));
  if(!val.isEmpty()) {
    size_t size = val.toUInt(&ok);
    if(not ok) {
      SETSTATUS(QString("Invalid OME TIFF property Size%1").arg(axis));
      return false;
    }
    return size;
  }
  return -1;
}

bool getTIFFInfo(TIFF* tif, ImageInfo& info)
{
  // First, check if this is ImageJ-like
  float xres = info.step.x(), yres = info.step.y(), zres = info.step.z();
  float unit = 1e-6; // um by default
  uint16_t resunit, spp;
  char* desc;
  uint16_t planar_config;

  // Check number images in directory
  size_t nb_slices = TIFFNumberOfDirectories(tif);

  Information::out << "nb slices = " << nb_slices << endl;

  // Read standard tags
  if(TIFFGetFieldDefaulted(tif, TIFFTAG_IMAGEWIDTH, &info.size.x()) != 1) {
    SETSTATUS("Error, no width");
    return false;
  }
  if(TIFFGetFieldDefaulted(tif, TIFFTAG_IMAGELENGTH, &info.size.y()) != 1) {
    SETSTATUS("Error, no height");
    return false;
  }
  bool has_xres = TIFFGetField(tif, TIFFTAG_XRESOLUTION, &xres);
  bool has_yres = TIFFGetField(tif, TIFFTAG_YRESOLUTION, &yres);
  bool has_zres = false;
  bool has_desc = TIFFGetField(tif, TIFFTAG_IMAGEDESCRIPTION, &desc);
  bool has_resunit = TIFFGetFieldDefaulted(tif, TIFFTAG_RESOLUTIONUNIT, &resunit);
  // TODO: remember what needs to be done with this variable
  bool has_planar = TIFFGetFieldDefaulted(tif, TIFFTAG_PLANARCONFIG, &planar_config);
  if(TIFFGetFieldDefaulted(tif, TIFFTAG_SAMPLESPERPIXEL, &spp) != 1) {
    SETSTATUS("Error, no samples per pixel");
    return false;
  }
  std::vector<uint16_t> bitsPerSamples(spp);
  if(TIFFGetFieldDefaulted(tif, TIFFTAG_BITSPERSAMPLE, bitsPerSamples.data()) != 1) {
    // Check they are all the same
    info.depth = bitsPerSamples[0];
    for(int i = 1 ; i < spp ; ++i)
      if(bitsPerSamples[i] != info.depth) {
        Information::err << "getTIFFInto: Warning, the bits per samples are not all equal.\n"
                            "this is badly handled by this library." << endl;
      }
  }
  Information::out << "TIFFTAG_SAMPLESPERPIXEL = " << spp << endl;
  // Set default values
  info.labels = false;
  info.nbChannels() = spp;
  info.nbTimePoints() = 1;
  if(has_xres and xres > 0 and has_yres and yres > 0) {
    xres = 1.0 / xres;
    yres = 1.0 / yres;
  }
  if(has_resunit) {
    if(resunit == RESUNIT_INCH) {
      unit = 0.0254;
    }
    else if(resunit == RESUNIT_CENTIMETER) {
      unit = 1e-2;
    }
  }

  bool contiguous = spp == 1;

  // And check description
  bool known_format = false;
  bool has_nchannels = false, has_ntimepoints = false;
  QString description = (has_desc ? QString::fromLatin1(desc) : "");
  //Information::out << "Description=\n" << description << endl;
  // Default axis order ...
  if(contiguous)
    info.axisOrder = { ImageInfo::X, ImageInfo::Y, ImageInfo::C, ImageInfo::Z, ImageInfo::T };
  else
    info.axisOrder = { ImageInfo::C, ImageInfo::X, ImageInfo::Y, ImageInfo::Z, ImageInfo::T  };
  if(description.startsWith("ImageJ")) {
    known_format = true;
    // Ordering of stacks in ImageJ:
    Information::out << "**** Detected ImageJ TIFF" << endl;
    QStringList fields = description.split("\n");
    for(QString f : fields) {
      QStringList entry = f.split("=");
      if(entry.size() == 2) {
        QString key = entry[0].trimmed();
        QString value = entry[1].trimmed();
        if(key == "spacing") {
          SETSTATUS("Found spacing = " << value);
          has_zres = true;
          bool ok;
          zres = value.toFloat(&ok);
          if(!ok or zres <= 0)
            zres = 1;
        }
        else if(key == "unit") {
          for(int i = 0; i < NB_UNITS; ++i) {
            if(value == units[i]) {
              unit = unit_value[i];
              SETSTATUS("Found unit = " << value << " => " << unit << "m");
              break;
            }
          }
        }
        else if(key == "labels") {
          info.labels = process::stringToBool(value);
          SETSTATUS("Found labels = " << value << " -> " << process::boolToString(info.labels));
        }
        else if(key == "channels") {
          SETSTATUS("Found channels = " << value);
          has_nchannels = true;
          bool ok;
          info.nbChannels() = value.toUInt(&ok);
          if(not ok)
            info.nbChannels() = 1;
        }
        else if(key == "frames") {
          has_ntimepoints = true;
          SETSTATUS("Found channels = " << value);
          bool ok;
          info.nbTimePoints() = value.toUInt(&ok);
          if(not ok)
            info.nbTimePoints() = 1;
        }
        else if(key == "slices") {
          SETSTATUS("Found nb slices = " << value);
          bool ok;
          info.size.z() = value.toUInt(&ok);
          if(not ok) {
            SETSTATUS("Error, invalid number of slices");
            return false;
          }
        }
        else if(key == "origin_x") {
          bool ok;
          info.origin.x() = value.toFloat(&ok);
          SETSTATUS("Found origin_x = " << value << " => " << info.origin.x());
          if(not ok) {
            SETSTATUS("Error, invalid origin_x value");
          }
        }
        else if(key == "origin_y") {
          bool ok;
          info.origin.y() = value.toFloat(&ok);
          SETSTATUS("Found origin_y = " << value << " => " << info.origin.y());
          if(not ok) {
            SETSTATUS("Error, invalid origin_y value");
          }
        }
        else if(key == "origin_z") {
          bool ok;
          info.origin.z() = value.toFloat(&ok);
          SETSTATUS("Found origin_z = " << value << " => " << info.origin.z());
          if(not ok) {
            SETSTATUS("Error, invalid origin_z value");
          }
        }
      }
    }
  } else if(description.startsWith("<?xml")) { // This may not be OME TIFF, but it is at least XML
    QDomDocument doc;
    unit = 1; // In OME-TIFF, unit is per axis
    Information::out << "**** Detected XML-TIFF" << endl;
    if(doc.setContent(description)) {
      QDomElement root = doc.documentElement();
      if(root.tagName() == "OME") {
        known_format = true;
        Information::out << "**** Detected OME-TIFF" << endl;
        // We have an OME-TIFF: find the resolution
        QDomElement image = root.firstChildElement("Image");
        if(!image.isNull()) {
          QDomElement pixels = image.firstChildElement("Pixels");
          if(!pixels.isNull()) {
            auto order = readOMETIFFDimensionOrder(pixels);
            if(order[0] != ImageInfo::X)
              return false;
            info.axisOrder = order;
            xres = readOMETIFFresolution(pixels, "X");
            has_xres = xres > 0;
            if(not has_xres)
              xres = 1; // Default to 1 µm per pixel
            info.step.x() = xres;
            yres = readOMETIFFresolution(pixels, "Y");
            has_yres = yres > 0;
            if(not has_yres)
              yres = xres;
            info.step.y() = yres;
            zres = readOMETIFFresolution(pixels, "Z");
            has_zres = zres > 0;
            if(not has_zres)
              zres = xres; // Copy xres if zres is not specified
            info.step.z() = zres;

            auto sizex = readOMETIFFSize(pixels, "X");
            if(sizex > 0)
              info.size.x() = sizex;
            else
              return false;
            auto sizey = readOMETIFFSize(pixels, "Y");
            if(sizey > 0)
              info.size.y() = sizey;
            else
              return false;
            auto sizez = readOMETIFFSize(pixels, "Z");
            if(sizez > 0)
              info.size.z() = sizez;
            else
              return false;
            auto sizet = readOMETIFFSize(pixels, "T");
            if(sizet > 0)
              info.nbTimePoints() = sizet;
            auto sizec = readOMETIFFSize(pixels, "C");
            if(sizec > 0)
              info.nbChannels() = sizec;
            has_nchannels = true;
            has_ntimepoints = true;
          }
        }
      }
    }
  }
  if(not known_format) {
    Information::out << "*** TIFF of unknown source" << endl;
    size_t nb_images = 0;
    tdir_t first_dir = TIFFCurrentDirectory(tif);
    do {
      nb_images++;
    } while(TIFFReadDirectory(tif));
    TIFFSetDirectory(tif, first_dir);
    info.size.z() = nb_images;
    info.nbTimePoints() = 1;
  }
  if(spp > 1 and spp != info.nbChannels())
    SETSTATUS("Warning, number of channels is not the same as number of sample per pixel");
  unit *= 1e6;
  if(not has_zres) {
    zres = xres;
    Information::out << QString("Warning, no z resolution found, using default (x resolution): %1 %2/px")
                        .arg(unit * zres)
                        .arg(UM) << endl;
  }
  if(contiguous and not has_nchannels) {
    if(info.nbChannels() * info.nbTimePoints() * info.size.z() != nb_slices) {
      if(info.nbChannels() == 1) {
        info.nbChannels() = nb_slices / (info.nbTimePoints() * info.size.z());
        if(info.nbChannels() * info.nbTimePoints() * info.size.z() != nb_slices) {
          SETSTATUS("Error, cannot find a number of channels matching the number of images in the TIFF file");
          return false;
        }
        SETSTATUS(QString("Found %1 channels").arg(info.nbChannels()));
      } else {
        SETSTATUS("Error, the number of images in the file doesn't match the number of slices, channels and timepoints");
        return false;
      }
    }
  } else if(not contiguous and not has_ntimepoints) {
    if(info.nbTimePoints() * info.size.z() != nb_slices) {
      if(info.nbTimePoints() == 1) {
        info.nbTimePoints() = nb_slices / info.size.z();
        if(info.nbTimePoints() * info.size.z() != nb_slices) {
          SETSTATUS("Error, cannot find a number of channels matching the number of images in the TIFF file");
          return false;
        }
        SETSTATUS(QString("Found %1 time points").arg(info.nbTimePoints()));
      }
    }
  }
  info.step = unit * Point3f(xres, yres, zres);
  return true;
}

bool readImagePlaneStrips(TIFF* tif, Image5D& data, size_t channel, size_t offset)
{
  uint32_t rps;
  if(TIFFGetFieldDefaulted(tif, TIFFTAG_ROWSPERSTRIP, &rps) != 1) {
    SETSTATUS("Error, no rows per strip");
    return false;
  }
  uint32_t* bc;
  uint16_t planar_config, bps, spp, format, orientation;
  if(TIFFGetFieldDefaulted(tif, TIFFTAG_BITSPERSAMPLE, &bps) != 1) {
    SETSTATUS("Error, no bits per sample");
    return false;
  }
  if(TIFFGetFieldDefaulted(tif, TIFFTAG_SAMPLESPERPIXEL, &spp) != 1) {
    SETSTATUS("Error, no samples per pixel");
    return false;
  }
  if(TIFFGetFieldDefaulted(tif, TIFFTAG_STRIPBYTECOUNTS, &bc) != 1) {
    SETSTATUS("Error, no strip byte counts");
    return false;
  }
  if(TIFFGetFieldDefaulted(tif, TIFFTAG_SAMPLEFORMAT, &format) != 1) {
    format = 1;
  }
  if(TIFFGetFieldDefaulted(tif, TIFFTAG_ORIENTATION, &orientation) != 1) {
    orientation = 1;
  }
  int32_t w = data.size.x(), h = data.size.y();
  /* Note: If we try this, then it crashes when reading ZIP files
   *bool has_compression = TIFFGetField(tif, TIFFTAG_ZIPQUALITY, &data.compression_level);
   *if(not has_compression)
   *  data.compression_level = 0;
   */
  if(bps != 8 and bps != 16 and bps != 32) {
    SETSTATUS("Error, this reader handles only 8, 16 and 32 bits per sample");
    return false;
  }
  StripDataIncrement incr;
  switch(orientation) {
  case ORIENTATION_TOPRIGHT: // line = top, column = right
    incr.shift_column = -1;
    incr.shift_row = 0;
    incr.start_column = w - 1;
    incr.start_row = h - 1;
    incr.line_end = w;
    break;
  case ORIENTATION_BOTRIGHT: // line = bottom column = right
    incr.shift_column = -1;
    incr.shift_row = 2 * w;
    incr.start_column = w - 1;
    incr.start_row = 0;
    incr.line_end = w;
    break;
  case ORIENTATION_BOTLEFT: // line = bottom, column = left
    incr.shift_column = 1;
    incr.shift_row = 0;
    incr.start_column = 0;
    incr.start_row = 0;
    incr.line_end = w;
    break;
  case ORIENTATION_LEFTTOP: // line = left, column = top
    std::swap(w, h);
    incr.shift_column = -w;
    incr.shift_row = h * w + 1;
    incr.start_column = 0;
    incr.start_row = h - 1;
    incr.line_end = h;
    break;
  case ORIENTATION_RIGHTTOP: // line = right, column = top
    std::swap(w, h);
    incr.shift_column = -w;
    incr.shift_row = h * w - 1;
    incr.start_column = w - 1;
    incr.start_row = h - 1;
    incr.line_end = h;
    break;
  case ORIENTATION_RIGHTBOT: // line = right, column = bottom
    std::swap(w, h);
    incr.shift_column = w;
    incr.shift_row = -h * w - 1;
    incr.start_column = w - 1;
    incr.start_row = 0;
    incr.line_end = h;
    break;
  case ORIENTATION_LEFTBOT: // line = left, column = bottom
    std::swap(w, h);
    incr.shift_column = w;
    incr.shift_row = -h * w + 1;
    incr.start_column = 0;
    incr.start_row = 0;
    incr.line_end = h;
    break;
  case ORIENTATION_TOPLEFT: // line = top, column = left
  default:
    incr.shift_column = 1;
    incr.shift_row = -2 * w;
    incr.start_column = 0;
    incr.start_row = h - 1;
    incr.line_end = w;
    break;
  }
  size_t sample = (data.axisOrder[0] == ImageInfo::C ? channel : 0);
  // SETSTATUS(QString("TIFF file %1x%2 with %3 bps").arg(w).arg(h).arg(bps));
  size_t tot_pixels = 0, max_pixels = data.size.x() * data.size.y();
  ushort* p = &data[offset + incr.start_column + incr.start_row * w];
  uint32_t strip_size = TIFFStripSize(tif);
  size_t nb_pixels = rps * w;
  TIFFAllocated alloc = _TIFFmalloc(strip_size+1);
  for(size_t strip = 0; strip < TIFFNumberOfStrips(tif); ++strip) {
    size_t size_buf = TIFFReadEncodedStrip(tif, strip, alloc.pointer(), strip_size+1);
    if(size_buf == 0)
      Information::err << "Warning: empty buffer!" << endl;
    if(size_buf > strip_size+1) {
      throw ImageError("Error when reading encoded strip: incomplete data!");
    }
    // Now, convert the data
    if((tot_pixels + nb_pixels) > max_pixels)
      nb_pixels = max_pixels - tot_pixels;
    tot_pixels += nb_pixels;
    switch(bps) {
      case 8: {
        switch(format) {
          case SAMPLEFORMAT_UINT:
            readStrip(data, p, incr, imageBuffer((unsigned char*)alloc.pointer()), nb_pixels, spp, sample);
            break;
          case SAMPLEFORMAT_INT:
            readStrip(data, p, incr, imageBuffer((signed char*)alloc.pointer()), nb_pixels, spp, sample);
            break;
          default:
            SETSTATUS(QString("Error, cannot handle 8 bits data format: %1").arg(format));
            return false;
        }
      } break;
      case 16: {
        switch(format) {
          case SAMPLEFORMAT_UINT:
            readStrip(data, p, incr, imageBuffer((unsigned short*)alloc.pointer()), nb_pixels, spp, sample);
            break;
          case SAMPLEFORMAT_INT:
            readStrip(data, p, incr, imageBuffer((signed short*)alloc.pointer()), nb_pixels, spp, sample);
            break;
          default:
            SETSTATUS(QString("Error, cannot handle 16 bits data format: %1").arg(format));
            return false;
        }
      } break;
      case 32: {
        switch(format) {
          case SAMPLEFORMAT_UINT:
            readStrip(data, p, incr, imageBuffer((uint*)alloc.pointer()), nb_pixels, spp, sample);
            break;
          case SAMPLEFORMAT_INT:
            readStrip(data, p, incr, imageBuffer((int*)alloc.pointer()), nb_pixels, spp, sample);
            break;
          case SAMPLEFORMAT_IEEEFP:
            readStrip(data, p, incr, imageBuffer((float*)alloc.pointer()), nb_pixels, spp, sample);
            break;
          default:
            SETSTATUS(QString("Error, cannot handle 32 bits data format: %1").arg(format));
            return false;
        }
      } break;
      default:
        SETSTATUS(QString("Error, cannot handle %1 bits per sample").arg(bps));
        return false;
    }
  }
  if(tot_pixels != max_pixels)
    SETSTATUS("Warning, read " << tot_pixels << " when " << max_pixels << " where expected");
  return true;
}

bool readImagePlaneTiles(TIFF* tif, Image5D& data, size_t channel, size_t offset)
{
  uint32_t* bc;
  uint16_t planar_config, bps, spp, format, orientation, tw, th;
  uint32_t rw, rh;

  if(TIFFGetField(tif, TIFFTAG_TILEWIDTH, &tw) != 1) {
    SETSTATUS("No tile width");
    return false;
  }
  if(TIFFGetField(tif, TIFFTAG_TILELENGTH, &th) != 1) {
    SETSTATUS("No tile length");
    return false;
  }
  if(TIFFGetFieldDefaulted(tif, TIFFTAG_BITSPERSAMPLE, &bps) != 1) {
    SETSTATUS("Error, no bits per sample");
    return false;
  }
  if(TIFFGetFieldDefaulted(tif, TIFFTAG_SAMPLESPERPIXEL, &spp) != 1) {
    SETSTATUS("Error, no samples per pixel");
    return false;
  }
  if(TIFFGetFieldDefaulted(tif, TIFFTAG_SAMPLEFORMAT, &format) != 1) {
    format = 1;
  }
  if(TIFFGetFieldDefaulted(tif, TIFFTAG_ORIENTATION, &orientation) != 1) {
    orientation = 1;
  }
  int32 w = data.size.x(), h = data.size.y();
  if(bps != 8 and bps != 16 and bps != 32) {
    SETSTATUS("Error, this reader handles only 8, 16 and 32 bits per sample");
    return false;
  }
  TileDataIncrement incr;
  incr.start_column = 0;
  incr.start_row = h - 1;
  incr.line_width = w;
  incr.tile_width = tw;
  incr.tile_height = th;

  uint32_t tile_size = TIFFTileSize(tif);
  TIFFAllocated alloc = _TIFFmalloc(tile_size+1);
  size_t nb_tiles = TIFFNumberOfTiles(tif) / spp;
  size_t nb_tile_width = (data.size.x() + tw - 1) / tw;
  size_t nb_tile_height = (data.size.y() + th - 1) / th;
  if(nb_tiles != nb_tile_width*nb_tile_height) {
    SETSTATUS("Error, inconsistent number of tiles");
    return false;
  }
  size_t tile = (spp > 1 ? channel*nb_tiles : 0);
  size_t last_nb_cols = data.size.x() % tw;
  if(last_nb_cols == 0) last_nb_cols = tw;
  size_t last_nb_rows = data.size.y() % th;
  if(last_nb_rows == 0) last_nb_rows = tw;
  for(size_t tile_y = 0 ; tile_y < nb_tile_height ; ++tile_y) {
    size_t nb_rows = (tile_y == nb_tile_height-1 ? last_nb_rows : th);
    for(size_t tile_x = 0 ; tile_x < nb_tile_width ; ++tile_x, ++tile) {
      size_t nb_cols = (tile_x == nb_tile_width-1 ? last_nb_cols : tw);
      int size_buf = TIFFReadEncodedTile(tif, tile, alloc.pointer(), tile_size+1);
      if(size_buf < 0) {
        SETSTATUS("Error, couldn't get buffer for tile " << tile);
        return false;
      }
      size_t start_index = offset + (incr.start_row - th*tile_y)*w + tw*tile_x;
      ushort* p = &data[start_index];
      // Now, convert the data
      switch(bps) {
        case 8: {
          switch(format) {
            case SAMPLEFORMAT_UINT:
              readTile(data, p, incr, imageBuffer((unsigned char*)alloc.pointer()), nb_rows, nb_cols);
              break;
            case SAMPLEFORMAT_INT:
              readTile(data, p, incr, imageBuffer((signed char*)alloc.pointer()), nb_rows, nb_cols);
              break;
            default:
              SETSTATUS(QString("Error, cannot handle 8 bits data format: %1").arg(format));
              return false;
          }
        } break;
        case 16: {
          switch(format) {
            case SAMPLEFORMAT_UINT:
              readTile(data, p, incr, imageBuffer((unsigned short*)alloc.pointer()), nb_rows, nb_cols);
              break;
            case SAMPLEFORMAT_INT:
              readTile(data, p, incr, imageBuffer((signed short*)alloc.pointer()), nb_rows, nb_cols);
              break;
            default:
              SETSTATUS(QString("Error, cannot handle 16 bits data format: %1").arg(format));
              return false;
          }
        } break;
        case 32: {
          switch(format) {
            case SAMPLEFORMAT_UINT:
              readTile(data, p, incr, imageBuffer((uint*)alloc.pointer()), nb_rows, nb_cols);
              break;
            case SAMPLEFORMAT_INT:
              readTile(data, p, incr, imageBuffer((int*)alloc.pointer()), nb_rows, nb_cols);
              break;
            case SAMPLEFORMAT_IEEEFP:
              readTile(data, p, incr, imageBuffer((float*)alloc.pointer()), nb_rows, nb_cols);
              break;
            default:
              SETSTATUS(QString("Error, cannot handle 32 bits data format: %1").arg(format));
              return false;
          }
        } break;
        default:
          SETSTATUS(QString("Error, cannot handle %1 bits per sample").arg(bps));
          return false;
      }
    }
  }
  return true;
}

bool readImagePlane(TIFF* tif, Image5D& data, size_t channel, size_t offset)
{
  uint32_t rw, rh;
  if(TIFFGetFieldDefaulted(tif, TIFFTAG_IMAGEWIDTH, &rw) != 1) {
    SETSTATUS("Error, no width");
    return false;
  }
  if(TIFFGetFieldDefaulted(tif, TIFFTAG_IMAGELENGTH, &rh) != 1) {
    SETSTATUS("Error, no height");
    return false;
  }
  if(rw != data.size.x() or rh != data.size.y()) {
    SETSTATUS(QString("Error, plane has dimensions %1x%2 while dimensions %3x%4 was expected.").arg(rh).arg(rw).arg(data.size.x()).arg(data.size.y()));
    return false;
  }
  //Information::out << "IsTiled: " << TIFFIsTiled(tif) << endl;
  if(TIFFIsTiled(tif))
    return readImagePlaneTiles(tif, data, channel, offset);
  return readImagePlaneStrips(tif, data, channel, offset);
}

/*
 * Base function to load TIFF images
 *
 * The 'selection' argument is a function taking 'z' and 't' and returning the
 * offset in the data at which the plane starts. If the offset is
 * std::numeric_limit<size_t>::max() than the total size of the dataset, then
 * the plane is ignored.
 */
template <typename Selection>
size_t loadTIFFImage(TIFF* tif, Image5D& data, size_t channel, ImageInfo& info,
                     Progress* progress, Selection selection,
                     size_t nb_planes = std::numeric_limits<size_t>::max())
{
  bool planeReady = true;
  size_t planes_read = 0;
  Point3u zct; // Current position in the files
  Point3u dims(info.size.z(), info.nbChannels(), info.nbTimePoints());
  bool contiguous = (info.axisOrder[0] != ImageInfo::C);
  std::array<int, 5> axes = {{-1, -1, 0, 1, 2}};
  size_t start_idx = (contiguous ? 2 : 3);
  if(not contiguous) zct[1] = channel;
  if(TIFFSetDirectory(tif, 0) == 0) {
    SETSTATUS("Error, cannot open main TIFF directory");
    return 0;
  }
  bool limit_reads = nb_planes <= data.size.z();
  while(planeReady) {
    DEBUG_OUTPUT("Reading plane -> zct = " << zct << endl);
    if(zct[1] == channel) {
      size_t offset = selection(Point2u(zct[0], zct[2]));
      if(offset != std::numeric_limits<size_t>::max()) {
        if(not readImagePlane(tif, data, channel, offset))
          return 0;
        ++planes_read;
        if(progress and !progress->advance(planes_read))
          throw process::UserCancelException();
        if(limit_reads and nb_planes == planes_read)
          break;
      }
    }
    for(size_t i = start_idx ; i < 5 ; ++i) {
      auto d = axes[info.axisOrder[i]];
      ++zct[d];
      if(zct[d] >= dims[d])
        zct[d] = 0;
      else
        break;
    }
    planeReady = TIFFReadDirectory(tif);
  }
  return planes_read;
}

} // namespace

void loadTIFFPlane(QString filename, Image5D& data,
                   uint plane, int channel, int timepoint)
{
  if(data.strides[0] != 1 or data.strides[1] != data.size[0] or
     data.axisOrder[0] != ImageInfo::X or data.axisOrder[1] != ImageInfo::Y)
    throw ImageError("loadTIFFPlane::Error, the axis order must start with 'X', 'Y' and the strides must be '1' and 'width'.");
  QByteArray ba = filename.toLocal8Bit();
  TIFFSetErrorHandler(tiffErrorHandler);
  TIFFHandler tif = TIFFOpen(ba.data(), "r");
  data.step = Point3f(1, 1, 1);
  data.updateStrides();
  size_t depth = 0;
  if(not tif)
    throw ImageError(QString("loadTIFFPlane::Error: File '%1' is not a valid TIFF file.").arg(filename));

  ImageInfo info(filename);
  if(not getTIFFInfo(tif, info))
    throw ImageError(QString("loadTIFFPlane::Error: Cannot extract TIFF information from file '%1'.")
                     .arg(filename));

  if(plane >= info.size.z())
    throw ImageError("Error, cannot extract plane: not enough planes.");

  if(channel >= info.nbChannels())
    throw ImageError("Error, cannot extract plane: not enough channels.");

  if(timepoint >= info.nbTimePoints())
    throw ImageError("Error, cannot extract plane: not enough time points.");

  if(data.size.z() != 1 or data.nbChannels() != 1 or data.nbTimePoints() != 1)
    throw ImageError("Error, cannot load a plane into an image that is not 2D only.");

  auto plane_loaded = loadTIFFImage(tif, data, channel, info, 0,
                                    [plane,timepoint,&data](const Point2u& zt) -> size_t {
                                      if(zt == Point2u(plane, timepoint))
                                        return 0;
                                      return std::numeric_limits<size_t>::max();
                                    }, 1);
  if(plane_loaded != 1)
    throw ImageError("loadTIFFPlane::Error: Cannot load plane");
}


void loadTIFFSamples(QString filename, Image5D& data, Progress* progress)
{
  QByteArray ba = filename.toLocal8Bit();
  TIFFSetErrorHandler(tiffErrorHandler);
  TIFFHandler tif = TIFFOpen(ba.data(), "r");
  data.step = Point3f(1, 1, 1);
  size_t depth = 0;

  if(not tif)
    throw ImageError(QString("loadTIFFPlane::Error: File '%1' is not a valid TIFF file.").arg(filename));

  // First, scan the number of directories
  ImageInfo info(filename);
  if(not getTIFFInfo(tif, info))
    throw ImageError(QString("loadTIFFPlane::Error: Cannot extract TIFF information from file '%1'.")
                     .arg(filename));
  depth = info.size.z();
  SETSTATUS(depth << " images in this TIFF file");

  data.step = info.step;
  data.origin = info.origin;
  data.labels = info.labels;

  auto expectedSize = Point5u{ info.size.x(), info.size.y(),  info.nbChannels() * info.nbTimePoints(), 1, 1} ;
  if(not data.data())
    data.allocate(expectedSize);
  else if(data.depth != 16)
    throw ImageError("When loading an image, the Image5D object must have a depth of 16 bits.");

  //SETSTATUS("data.size = " << data.size);

  if(data.size != expectedSize)
    throw ImageError(QString("loadTIFFSamples::Error: image size is %1 while a size of %2 was expected.")
                     .arg(toQString(data.size))
                     .arg(toQString(expectedSize)));

  if(progress)
    progress->setMaximum(data.size.z());

  size_t mid_z = info.size.z() / 2;

  Information::out << "mid_z = " << mid_z << endl;

  for(uint32_t c = 0 ; c < info.nbChannels() ; ++c) {
    DEBUG_OUTPUT("Loading channel " << c << endl);
    auto nb_channels = info.nbChannels();
    auto planes_read = loadTIFFImage(tif, data, c, info, progress,
                                     [mid_z,c,nb_channels,&data](const Point2u& zt) -> size_t {
                                       if(zt[0] == mid_z) {
                                         DEBUG_OUTPUT("  --> timepoint " << zt[1] << endl);
                                         return (c + zt[1]*nb_channels) * data.strides.z();
                                       }
                                       return std::numeric_limits<size_t>::max();
                                     }, info.nbTimePoints());
    if(planes_read < 1)
      throw ImageError("laodTIFFSamples::Error: not enough images in the file!");
  }
}

void loadSamples(QString filename, Image5D& data, Progress* progress)
{
  if(filename.endsWith(".tif", Qt::CaseInsensitive) or filename.endsWith(".tiff", Qt::CaseInsensitive))
    return loadTIFFSamples(filename, data, progress);

  try {
    loadImageMagickSamples({filename}, data, progress);
  } catch(std::exception& ex) {
    Information::err << "loadSamples::Warning: Couldn't use Image Magick:\n" << ex.what() << endl;

    CImgUS image(qPrintable(filename));
    auto expectedSize = Point5u{image.width(), image.height(), image.spectrum(), 1, 1};
    data.step = {1, 1, 1};
    if(not data.data())
      data.allocate(expectedSize);
    else if(data.depth != 16)
      throw ImageError("loadSamples::Error: When loading an image, the Image5D object must have a depth of 16 bits.");

    if(data.size != expectedSize)
      throw ImageError(QString("loadSamples::Error: image size is %1 while a size of %2 was expected.")
                       .arg(toQString(data.size))
                       .arg(toQString(expectedSize)));

    if(progress)
      progress->setMaximum(data.size.z());

    size_t mid_z = image.depth() / 2;

    auto size_plane = data.size.x() * data.size.y();

    for(uint32_t c = 0 ; c < data.size.z() ; ++c) {
      memcpy(&data[size_plane*c],
             image.data(0, 0, 0, c),
             size_plane * sizeof(ushort));
      if(progress and not progress->advance(c))
        throw process::UserCancelException();
    }
  }
}

void loadTIFFImage(QString filename, Image5D& data, int channel, int timepoint,
                   Progress* progress)
{
  QByteArray ba = filename.toLocal8Bit();
  TIFFSetErrorHandler(tiffErrorHandler);
  TIFFHandler tif = TIFFOpen(ba.data(), "r");
  data.step = Point3f(1, 1, 1);
  size_t depth = 0;
  if(not tif)
    throw ImageError(QString("loadTIFFPlane::Error: File '%1' is not a valid TIFF file.").arg(filename));
  // First, scan the number of directories
  ImageInfo info(filename);
  if(not getTIFFInfo(tif, info))
    throw ImageError(QString("loadTIFFPlane::Error: Cannot extract TIFF information from file '%1'.")
                     .arg(filename));
  depth = info.size.z();
  SETSTATUS(depth << " images in this TIFF file");
  if(channel >= (int)info.nbChannels())
    throw ImageError("loadTIFFImage::Error, invalid channel selected");
  if(timepoint >= (int)info.nbTimePoints())
    throw ImageError("loadTIFFImage::Error, invalid time point selected");
  Point5u targetRead = info.size;
  if(channel >= 0)
    targetRead[3] = 1;
  if(timepoint >= 0)
    targetRead[4] = 1;
  if(progress)
    progress->setMaximum(depth);
  data.step = info.step;
  data.origin = info.origin;
  data.labels = info.labels;
  if(not data.data())
    data.allocate(targetRead);
  else if(data.depth != 16)
    throw ImageError("When loading an image, the Image5D object must have a depth of 16 bits.");

  if(data.size != targetRead)
    throw ImageError(QString("loadSamples::Error: image size is %1 while a size of %2 was expected.")
                     .arg(toQString(data.size))
                     .arg(toQString(targetRead)));

  if(data.strides[0] != 1 or data.strides[1] != data.size[0] or
     data.axisOrder[0] != ImageInfo::X or data.axisOrder[1] != ImageInfo::Y)
    throw ImageError("loadTIFFImage::Error, the axis order must start with 'X', 'Y' and the strides must be '1' and 'width'.");

  if(channel < 0) channel = 0;
  for(int c = 0 ; c < data.nbChannels() ; ++c) {
    size_t planes_read;
    auto expected_planes = info.size.z();
    size_t channel_shift = c * data.strides[3];
    if(timepoint == -1) {
      expected_planes *=  data.nbTimePoints();
      size_t timestride = data.nbChannels() * data.size.z();
      planes_read = loadTIFFImage(tif, data, channel+c, info, progress,
                                  [&data, channel_shift](const Point2u& zt) -> size_t {
                                    return zt[0] * data.strides.z() + zt[1]*data.strides[4] + channel_shift;
                                  }, expected_planes);
    } else
      planes_read = loadTIFFImage(tif, data, channel+c, info, progress,
                                  [timepoint, channel_shift, &data](const Point2u& zt) -> size_t {
                                    if(zt[1] == timepoint)
                                      return zt[0] * data.strides.z() + channel_shift;
                                    return std::numeric_limits<size_t>::max();
                                  }, expected_planes);
    if(planes_read < expected_planes)
      throw ImageError(QString("Error, not enough images in the file: read %1 while %2 were expected!")
                       .arg(planes_read).arg(expected_planes));
  }
}

namespace {
template <typename... T>
void addImageJField(QString& desc, const QString& name, const T&... args)
{
  desc += QString("\n%1=%2").arg(name).arg(args...);
}
}

void saveTIFFImage(QString filename, const Image5D& data)
{
  if(data.axisOrder != std::array<ImageInfo::Axis,5>{{ImageInfo::X, ImageInfo::Y, ImageInfo::Z, ImageInfo::C, ImageInfo::T}})
    throw ImageError("saveTIFFImage::Error: can only save image in standard order: X, Y, Z, C, T.");
  QByteArray ba = filename.toLocal8Bit();
  TIFFSetErrorHandler(tiffErrorHandler);
  TIFFHandler tif = TIFFOpen(ba.data(), "w");
  if(not tif)
    throw ImageError(QString("loadTIFFPlane::Error: File '%1' is not a valid TIFF file.").arg(filename));

  tsize_t plane_size = data.size.x() * data.size.y();

  float xres = 1 / data.step.x(), yres = 1 / data.step.y(), zres = data.step.z();
  uint16_t resunit = RESUNIT_NONE;

  QString description = QString("ImageJ=1.49q");
  addImageJField(description, "images", size_t(data.size[2]) * data.size[3] * data.size[4]);
  addImageJField(description, "slices", data.size.z());
  if(data.size[3] > 1)
    addImageJField(description, "channels", data.size[3]);
  if(data.size[4] > 1)
    addImageJField(description, "frames", data.size[4]);
  addImageJField(description, "hyperstack", "true");
  addImageJField(description, "mode", "color");
  addImageJField(description, "unit", "micron");
  addImageJField(description, "space", zres);
  addImageJField(description, "loop", "false");
  addImageJField(description, "labels", (data.labels ? "true" : "false"));
  addImageJField(description, "origin_x", data.origin.x());
  addImageJField(description, "origin_y", data.origin.y());
  addImageJField(description, "origin_z", data.origin.z());

  //Information::out << "description='" << description << "'" << endl;
  QByteArray desc = description.toLatin1();

  for(uint t = 0 ; t < data.nbTimePoints() ; ++t)
    for(uint z = 0; z < data.size.z(); ++z)
      for(uint c = 0 ; c < data.nbChannels() ; ++c)
      {
        uint32_t w = data.size.x(), h = data.size.y(), rps = data.size.y();
        /*
         *if(TIFFSetField(tif, TIFFTAG_SUBFILETYPE, 0) != 1) {
         *  throw ImageError("saveTIFFImage::Error writing subtile type.");
         *  return false;
         *}
         */
        if(TIFFSetField(tif, TIFFTAG_BITSPERSAMPLE, 16) != 1) {
          throw ImageError("saveTIFFImage::Error writing bits per sample");
        }
        if(TIFFSetField(tif, TIFFTAG_SAMPLESPERPIXEL, 1) != 1) {
          throw ImageError("saveTIFFImage::Error writing samples per pixel");
        }
        if(TIFFSetField(tif, TIFFTAG_PLANARCONFIG, PLANARCONFIG_CONTIG) != 1) {
          throw ImageError("saveTIFFImage::Error writing planar configuration");
        }
        if(TIFFSetField(tif, TIFFTAG_SAMPLEFORMAT, SAMPLEFORMAT_UINT) != 1) {
          throw ImageError("saveTIFFImage::Error writing sample format");
        }
        if(TIFFSetField(tif, TIFFTAG_ORIENTATION, ORIENTATION_TOPLEFT) != 1) {
          throw ImageError("saveTIFFImage::Error writing orientation");
        }
        if(TIFFSetField(tif, TIFFTAG_IMAGEWIDTH, w) != 1) {
          throw ImageError("saveTIFFImage::Error writing width");
        }
        if(TIFFSetField(tif, TIFFTAG_IMAGELENGTH, h) != 1) {
          throw ImageError("saveTIFFImage::Error writing height");
        }
        if(TIFFSetField(tif, TIFFTAG_ROWSPERSTRIP, rps) != 1) {
          throw ImageError("saveTIFFImage::Error writing rows per strip");
        }
        if(data.compression_level > 0) {
          if(TIFFSetField(tif, TIFFTAG_COMPRESSION, COMPRESSION_DEFLATE) != 1) {
            throw ImageError("saveTIFFImage::Error setting DEFLATE compression scheme");
          }
          if(TIFFSetField(tif, TIFFTAG_ZIPQUALITY, data.compression_level) != 1) {
            throw ImageError("saveTIFFImage::Error setting ZIP quality");
          }
        } else if(TIFFSetField(tif, TIFFTAG_COMPRESSION, COMPRESSION_NONE) != 1) {
          throw ImageError("saveTIFFImage::Error setting compression scheme to NONE");
        } /*else if(TIFFSetField(tif, TIFFTAG_COMPRESSION, COMPRESSION_LZW) != 1) {
            throw ImageError("saveTIFFImage::Error setting LZW compression scheme");
            }*/
        if(TIFFSetField(tif, TIFFTAG_XRESOLUTION, xres) != 1) {
          throw ImageError("saveTIFFImage::Error writing x resolution");
        }
        if(TIFFSetField(tif, TIFFTAG_YRESOLUTION, yres) != 1) {
          throw ImageError("saveTIFFImage::Error writing y resolution");
        }
        if(TIFFSetField(tif, TIFFTAG_IMAGEDESCRIPTION, desc.data()) != 1) {
          throw ImageError("saveTIFFImage::Error writing description");
        }
        if(TIFFSetField(tif, TIFFTAG_RESOLUTIONUNIT, resunit) != 1) {
          throw ImageError("saveTIFFImage::Error writing resolution unit");
        }

        std::vector<ushort> sorted_data(plane_size);
        ushort *ps = sorted_data.data();
        const ushort *pi = data.data();
        size_t plane_size = data.size.x()*data.size.y();
        auto shift = plane_size*(z+1) + data.strides[3] * c + data.strides[4] * t - data.size.x();

        pi += shift;
        for(size_t y = 0 ; y < data.size.y() ; ++y) {
          for(size_t x = 0 ; x < data.size.x() ; ++x, ++ps, ++pi)
            *ps = *pi;
          pi -= 2*data.size.x();
        }

        if(TIFFWriteEncodedStrip(tif, 0, (tdata_t)sorted_data.data(), 2*plane_size) == -1) {
          throw ImageError("saveTIFFImage::Error writing image data");
        }

        TIFFWriteDirectory(tif);
      }
}

namespace {

QByteArray getRawImageMagickData(QStringList filenames, const ImageInfo& info, uint16_t depth, float gamma, Progress* progress)
{
  if(progress) progress->setMaximum(0);

  QProcess proc;
  auto pathConvert = QStandardPaths::findExecutable("convert");
  if(pathConvert.isEmpty())
    throw ImageError("Error, cannot find 'convert' executable. Ensure Image Magick is installed.");
  QStringList conversion = {"GRAY", "", "RGB", "RGBA"};
  QString conv = conversion[info.nbChannels()-1];
  proc.setProgram(pathConvert);
  Information::out << "Getting raw Image Magick data with depth = " << depth << endl;
  proc.setArguments(QStringList(filenames)
                    << "-depth" << QString::number(depth)
                    << "+gamma"<< QString::number(gamma, 'g', std::numeric_limits<float>::max_digits10)
                    << QString("%1:-").arg(conv));
  proc.setReadChannel(QProcess::StandardOutput);
  proc.start(QIODevice::ReadOnly);

  if(not proc.waitForStarted()) return {};
  while(not proc.waitForFinished(100)) {
    if(progress and not progress->advance(0)) {
      proc.kill();
      throw process::UserCancelException();
    }
  }

  if(proc.exitCode() != 0)
    throw ImageError(QString("getRawImageMagickData::Error running convert:\n%1")
                     .arg(QString::fromLocal8Bit(proc.readAllStandardError())));
  return proc.readAllStandardOutput();
}

} // namespace

LGX_EXPORT void loadImageMagickPlane(const QStringList& files, Image5D& data,
                                     uint plane, int channel, int timepoint)
{
  if(data.strides[0] != 1 or data.strides[1] != data.size[0] or
     data.axisOrder[0] != ImageInfo::X or data.axisOrder[1] != ImageInfo::Y)
    throw ImageError("loadImageMagickPlane::Error, the axis order must start with 'X', 'Y' and the strides must be '1' and 'width'.");
  float gamma;
  auto info = getImageMagickInfo(files, &gamma);
  //Information::out << "Loading with Image Magick image of size " << info.size << " with " << info.nbChannels() << " channels" << endl;
  if(info.nbChannels() > 4 or info.nbChannels() == 2)
    throw ImageError("loadImageMagickPlane::Error, Image Magick converter can only handle, 1, 3 and 4 channels.");

  size_t nb_channels = info.nbChannels();
  if(channel >= (int)nb_channels)
    throw ImageError(QString("loadImageMagickPlane::Error, cannot get channel %1, there are only %2.")
                     .arg(channel).arg(nb_channels));

  auto expectedSize = info.size;
  expectedSize.z() = 1;
  if(channel >= 0)
    expectedSize[3] = 1;
  if(timepoint >= 0)
    expectedSize[4] = 1;
  if(expectedSize != data.size)
    throw ImageError(QString("Error, cannot save an image of size %1 in an allocate area of size %2.")
                     .arg(toQString(expectedSize))
                     .arg(toQString(data.size)));

  bool is16bits = info.depth > 8;
  auto rawData = getRawImageMagickData(files, info, (is16bits ? 16 : 8), gamma, 0);
  auto expectedData = (is16bits ? 2 : 1)*info.nbVoxels();
  if(rawData.size() != expectedData)
    throw ImageError(QString("loadImageMagickPlane::Error: expected %1 bytes of data, but received %2.")
                     .arg(expectedData).arg(rawData.size()));
  auto rawArray16 = reinterpret_cast<const uint16_t*>(rawData.constData());
  auto rawArray8 = reinterpret_cast<const uint8_t*>(rawData.constData());

  data.updateStrides();

  if(data.size != info.size)
    throw ImageError(QString("loadImageMagickPlane::Error: the image size (%1) doesn't correspond to "
                             "the content of the file (%2).")
                     .arg(toQString(data.size))
                     .arg(toQString(info.size)));

  auto size_plane = size_t(data.size.x()) * data.size.y();
  auto size_line = data.size.x();
  size_t pos = channel + (plane+1)*size_plane - size_line;
  ushort* towrite = data.data();
  for(size_t y = 0 ; y < info.size.y() ; ++y) {
    for(size_t x = 0 ; x < info.size.x() ; ++x, ++towrite, pos += info.nbChannels()) {
      ushort val = (is16bits ? rawArray16[pos] : rawArray8[pos]);
      val *= data.brightness;
      *towrite = val;
    }
    pos -= 2*size_line;
  }
  data.filename = files.join(";;");
}

LGX_EXPORT void loadImageMagickSamples(const QStringList& files, Image5D& data,
                                       Progress* progress)
{
  float gamma;
  auto info = getImageMagickInfo(files, &gamma);
  Information::out << "Loading with Image Magick image of size " << info.size << " with " << info.nbChannels() << " channels" << endl;
  if(info.nbChannels() > 4 or info.nbChannels() == 2)
    throw ImageError("loadImageMagickSamples::Error, Image Magick converter can only handle, 1, 3 and 4 channels.");

  Point5u expectedSize = info.size;
  expectedSize.z() = info.nbChannels() * info.nbTimePoints();

  data.step = info.step;
  data.origin = info.origin;
  data.labels = info.labels;

  if(not data.data())
    data.allocate(expectedSize);
  else if(data.depth != 16)
    throw ImageError("When loading an image, the Image5D object must have a depth of 16 bits.");

  if(data.size != expectedSize)
    throw ImageError(QString("loadImageMagickSamples::Error: image size is %1 while a size of %2 was expected.")
                     .arg(toQString(data.size))
                     .arg(toQString(expectedSize)));

  if(data.strides[0] != 1 or data.strides[1] != data.size[0] or
     data.axisOrder[0] != ImageInfo::X or data.axisOrder[1] != ImageInfo::Y)
    throw ImageError("loadImageMagickSamples::Error, the axis order must start with 'X', 'Y' and the strides must be '1' and 'width'.");

  size_t nb_channels = info.nbChannels();

  bool is16bits = info.depth > 8;
  auto rawData = getRawImageMagickData(files, info, (is16bits ? 16 : 8), gamma, progress);
  auto expectedDataSize = (is16bits ? 2 : 1)*info.nbVoxels();
  if(rawData.size() != expectedDataSize)
    throw ImageError(QString("loadImageMagickSamples::Error: expected %1 bytes of data, but received %2.")
                     .arg(expectedDataSize).arg(rawData.size()));
  auto rawArray16 = reinterpret_cast<const uint16_t*>(rawData.constData());
  auto rawArray8 = reinterpret_cast<const uint8_t*>(rawData.constData());

  size_t mid_z = info.size.z() / 2;
  auto size_plane = size_t(data.size.x()) * data.size.y();
  auto size_line = data.size.x();
  for(size_t c = 0 ; c < nb_channels ; ++c) {
    size_t pos = c + mid_z*size_plane*info.nbChannels();
    ushort* towrite = &data[size_plane * (c+1) - size_line];
    for(size_t y = 0 ; y < info.size.y() ; ++y) {
      for(size_t x = 0 ; x < info.size.x() ; ++x, ++towrite, pos += info.nbChannels()) {
        ushort val = (is16bits ? rawArray16[pos] : rawArray8[pos]);
        val *= data.brightness;
        *towrite = val;
      }
      towrite -= 2*size_line;
    }
  }

  data.filename = files.join(";;");
}

void loadImageMagickImage(const QStringList& files, Image5D& data,
                          int channel, int timepoint,
                          Progress* progress)
{
  if(files.empty())
    throw ImageError("loadImageMagickImage::Error: Cannot load an empty list of images.");
  float gamma;
  auto info = getImageMagickInfo(files, &gamma);
  Information::out << "Loading with Image Magick image of size " << info.size << " with " << info.nbChannels() << " channels" << endl;
  if(info.nbChannels() > 4 or info.nbChannels() == 2)
    throw ImageError("loadImageMagickImage::Error, Image Magick converter can only handle, 1, 3 and 4 channels.");

  if(timepoint > 0)
    throw ImageError("loadImageMagickImage::Error, timepoint not supported.");

  size_t nb_channels = info.nbChannels();
  if(channel >= (int)nb_channels)
    throw ImageError(QString("loadImageMagickImage::Error, cannot get channel %1: there are only %2.")
                     .arg(channel).arg(nb_channels));

  auto expectedSize = info.size;
  if(channel >= 0)
    expectedSize[3] = 1;

  data.step = info.step;
  data.origin = info.origin;
  data.labels = info.labels;

  if(not data.data())
    data.allocate(expectedSize);
  else if(data.depth != 16)
    throw ImageError("When loading an image, the Image5D object must have a depth of 16 bits.");

  if(data.size != expectedSize)
    throw ImageError(QString("loadImageMagickImage::Error: image size is %1 while a size of %2 was expected.")
                     .arg(toQString(data.size))
                     .arg(toQString(expectedSize)));

  if(data.strides[0] != 1 or data.strides[1] != data.size[0] or
     data.axisOrder[0] != ImageInfo::X or data.axisOrder[1] != ImageInfo::Y)
    throw ImageError("loadImageMagickImage::Error, the axis order must start with 'X', 'Y' and the strides must be '1' and 'width'.");

  if(not data.data())
    throw ImageError("loadImageMagickImage::Error cannot write on an image without data.");

  data.filename = files.join(";;");

  bool is16bits = info.depth > 8;
  auto rawData = getRawImageMagickData(files, info, (is16bits ? 16 : 8), gamma, progress);
  auto expectedDataSize = (is16bits ? 2 : 1)*info.nbVoxels();
  if(rawData.size() != expectedDataSize)
    throw ImageError(QString("loadImageMagickImage::Error: expected %1 bytes of data, but received %2.")
                     .arg(expectedDataSize).arg(rawData.size()));
  auto rawArray16 = reinterpret_cast<const uint16_t*>(rawData.constData());
  auto rawArray8 = reinterpret_cast<const uint8_t*>(rawData.constData());

  auto size_line = size_t(data.size.x());
  auto size_plane = size_line * data.size.y();
  data.maxc = 0;
  data.minc = 65535;
  if(channel < 0) channel = 0;

  size_t prog_step = 1;
  size_t incr = 1;
  size_t prog = 0;
  if(progress) {
    prog_step = info.size[2] * info.size[3];
    progress->setMaximum(prog_step);
    prog_step /= 100;
    if(prog_step < 1) prog_step = 1;
#ifdef _OPENMP
    incr = omp_get_max_threads();
#endif
  }
  size_t next_prog = prog_step;

  bool canceled = false;

  for(size_t c = 0 ; c < data.nbChannels() ; ++c) {
#pragma omp parallel for shared(canceled)
    for(size_t z = 0 ; z < info.size.z() ; ++z) {
#ifdef _OPENMP
      if(canceled) continue;
#endif
      if(progress) {
#ifdef _OPENMP
        if(omp_get_thread_num() == 0)
#endif
        {
          if(prog >= next_prog) {
            if(not progress->advance(prog)) {
#ifdef _OPENMP
              canceled = true;
#else
              throw process::UserCancelException();
#endif
            }
            next_prog += prog_step;
          }
          prog += incr;
        }
      }
      size_t pos = channel + c + z * data.size.x() * data.size.y() * info.nbChannels();
      ushort* towrite = &data[(z+1)*size_plane - size_line + c*data.strides[3]];
      for(size_t y = 0 ; y < info.size.y() ; ++y) {
        for(size_t x = 0 ; x < info.size.x() ; ++x, ++towrite, pos += info.nbChannels()) {
          ushort voxel = (is16bits ? rawArray16[pos] : rawArray8[pos]);
          voxel *= data.brightness;
          *towrite = voxel;
          if(voxel > data.maxc)
            data.maxc = voxel;
          if(voxel < data.minc)
            data.minc = voxel;
        }
        towrite -= 2*size_line;
      }
    }
  }

#ifdef _OPENMP
  if(canceled)
    throw process::UserCancelException();
#endif
}

void loadImage(QString filename, Image5D& data,
               int channel, int timepoint,
               Progress* progress)
{
  // use libtiff if this is a TIFF file
  if(filename.endsWith(".tif", Qt::CaseInsensitive) or filename.endsWith(".tiff", Qt::CaseInsensitive))
    return loadTIFFImage(filename, data, channel, timepoint, progress);

  if(timepoint > 0)
    throw ImageError("loadImage::Error: Time points are supported only with TIFF file format.");

  try {
    loadImageMagickImage({filename}, data, channel, timepoint, progress);
  } catch(std::exception& ex) {
    Information::err << "Couldn't use Image Magick:\n" << ex.what() << endl;

    // At last, try with CImg
    CImgUS image(filename.toLocal8Bit().data());

    if(channel >= (int)image.spectrum())
      throw ImageError(QString("Error, requesting channel %1 when only %2 channels are available.")
                       .arg(channel).arg(image.spectrum()));

    auto targetSize = Point5u{image.width(), image.height(), image.depth(), image.spectrum(), 1};
    if(channel >= 0)
      targetSize[3] = 1;

    data.step = {1.f, 1.f, 1.f};
    data.origin = {0.f, 0.f, 0.f};

    if(not data.data())
      data.allocate(targetSize);
    else if(data.depth != 16)
      throw ImageError("When loading an image, the Image5D object must have a depth of 16 bits.");

    if(data.size != targetSize)
      throw ImageError(QString("loadImageMagickImage::Error: image size is %1 while a size of %2 was expected.")
                       .arg(toQString(data.size))
                       .arg(toQString(targetSize)));

    if(data.strides[0] != 1 or data.strides[1] != data.size[0] or
       data.axisOrder[0] != ImageInfo::X or data.axisOrder[1] != ImageInfo::Y)
      throw ImageError("loadImage::Error, the axis order must start with 'X', 'Y' and the strides must be '1' and 'width'.");

    if(not data.data())
      throw ImageError("Error, cannot write in a non-allocated image.");

    if(int(data.size.x()) != image.width() or int(data.size.y()) != image.height())
      throw ImageError(QString("Error: Bad image file: %1 size: %2x%3").arg(filename).arg(image.width()).arg(image.height()));
    ushort* p = data.data();
    if(channel < 0) channel = 0;
    for(uint c = 0 ; c < data.nbChannels() ; ++c)
      for(uint z = 0 ; z < data.size.z() ; z++)
        for(uint y = 0 ; y < data.size.y() ; y++)
          for(uint x = 0 ; x < data.size.x() ; x++, p++)
          {
            float voxel = 0;
            voxel = image(x, y, z, c+channel);
            voxel *= data.brightness;
            *p = ushort(voxel);
            if(voxel > data.maxc)
              data.maxc = voxel;
            if(voxel < data.minc)
              data.minc = voxel;
          }
  }
}

void loadImageSequence(QStringList filenames, Image5D& data,
                       int channel, int timepoint,
                       Progress* progress)
{
  if(filenames.isEmpty())
    throw ImageError("loadImageSequence::Error: Cannot load an empty list of images.");

  try {
    loadImageMagickImage(filenames, data, channel, timepoint, progress);
  } catch(std::exception& ex) {
    Information::err << "loadImageSequence::Warning: couldn't use Image Magick:\n"
      << ex.what() << endl;

    auto nb_images = filenames.size();
    auto info = getImageInfo(filenames.at(0));
    auto expectedSize = Point5u{ info.size.x(), info.size.y(), filenames.size(), 1, 1 };
    if(channel < 0)
      data.size[3] = info.nbChannels();

    if(not data.data())
      data.allocate(expectedSize);
    else if(data.depth != 16)
      throw ImageError("When loading an image, the Image5D object must have a depth of 16 bits.");

    if(data.strides[0] != 1 or data.strides[1] != data.size[0] or
       data.axisOrder[0] != ImageInfo::X or data.axisOrder[1] != ImageInfo::Y)
      throw ImageError("loadImageSequence::Error, the axis order must start with 'X', 'Y' and the strides must be '1' and 'width'.");

    if(data.size != expectedSize)
      throw ImageError(QString("loadImageMagickImage::Error: image size is %1 while a size of %2 was expected.")
                       .arg(toQString(data.size))
                       .arg(toQString(expectedSize)));

    if(progress) progress->setMaximum(filenames.size());
    auto tosave = data;
    if(channel >= 0)
      tosave = tosave.channel(channel);
    if(timepoint >= 0)
      tosave = tosave.timepoint(timepoint);
    for(uint z = 0 ; z < data.size.z() ; ++z) {
      auto view = tosave.plane(z);
      loadImage(filenames.at(z), view, channel, timepoint, 0);
      data.minc = std::min(data.minc, view.minc);
      data.maxc = std::max(data.maxc, view.maxc);
      if(progress and not progress->advance(z))
        throw process::UserCancelException();
    }
    data.step = info.step;
    data.origin = info.origin;
    data.labels = info.labels;
  }
}

ImageInfo getTIFFInfo(QString filename)
{
  QByteArray ba = filename.toLocal8Bit();
  TIFFSetErrorHandler(tiffErrorHandler);
  TIFFHandler tif = TIFFOpen(ba.data(), "r");
  if(tif) {
    ImageInfo result(filename);
    if(not getTIFFInfo(tif, result))
      return ImageInfo();
    return result;
  }
  return ImageInfo();
}

namespace {
// Doesn't contain the colorspaces with 3 channels
QHash<QString,int> colorspaceChannels =
{
  {"Gray", 1},
  {"Rec601Luma", 1},
  {"Rec709Luma", 1},
  {"Transparent", 1}
};

} // namespace

ImageInfo getImageMagickInfo(const QStringList& files, float *gamma)
{
  // First, find the 'identify' program
  ImageInfo result;
  result.size = 0u;

  if(files.isEmpty()) {
    Information::err << "getImageMagickInfo::Error: empty file list." << endl;
    return result;
  }

  auto pathIdentify = QStandardPaths::findExecutable("identify");
  if(pathIdentify.isEmpty()) {
    Information::err << "getImageMagickInfo::Error, cannot find program 'identify'. Ensure Image Magick is installed." << endl;
    return result;
  }

  QProcess proc;
  proc.setProgram(pathIdentify);
  proc.setArguments({"-format", "%w %h %n %[depth] %A %[gamma] %[colorspace]", files[0]});
  proc.setReadChannel(QProcess::StandardOutput);
  proc.start(QIODevice::ReadOnly);
  if(not proc.waitForStarted()) return result;
  if(not proc.waitForFinished()) return result;

  if(proc.exitCode() != 0) {
    Information::err << "getImageMagickInfo::Error executing 'identify': " << QString::fromLocal8Bit(proc.readAllStandardError()) << endl;
    return result;
  }
  QStringList fields = QString::fromLocal8Bit(proc.readAllStandardOutput()).split(" ");
  while(fields.size() > 7) {
    fields[fields.size()-2] += " ";
    fields[fields.size()-2] += fields[fields.size()-1];
    fields.pop_back();
  }
  if(fields.size() != 7) {
    Information::err << "getImageMagickInfo::Error, incorrect number of fields returned by 'identify': " << fields.size() << endl;
    return result;
  }
  Information::out << "fields = " << fields.join(" ** ") << endl;
  bool ok;
  uint w, h, n, d, c;

  w = fields[0].toUInt(&ok);
  if(not ok) {
    Information::err << "getImageMagickInfo::Error: invalid value for width: '" << fields[0] << "'" << endl;
    return result;
  }
  h = fields[1].toUInt(&ok);
  if(not ok) {
    Information::err << "getImageMagickInfo::Error: invalid value for height: '" << fields[1] << "'" << endl;
    return result;
  }
  n = fields[2].toUInt(&ok);
  if(not ok) {
    Information::err << "getImageMagickInfo::Error: invalid value for number of images: '" << fields[2] << "'" << endl;
    return result;
  }
  d = fields[3].toUInt(&ok);
  if(not ok) {
    Information::err << "getImageMagickInfo::Error: invalid value for depth: '" << fields[3] << "'" << endl;
    return result;
  }

  bool hasAlpha = process::stringToBool(fields[4]);

  if(gamma) {
    *gamma = fields[5].toFloat(&ok);
    if(not ok) {
      Information::err << "getImageMagickInfo::Error: invalid value for gamma: '" << fields[5] << "'" << endl;
      return result;
    }
  }
  c = colorspaceChannels.value(fields[6], 3);
  if(hasAlpha)
    c++;

  if(files.size() > 1) {
    if(n != 1) {
      Information::err << "getImageMagickInfo::Error: If loading many images, they should each contain a single plane." << endl;
      return result;
    }
    n = files.size();
  }

  result.size = {w, h, n, c, 1};
  result.depth = d;
  result.axisOrder = {{ImageInfo::C, ImageInfo::X, ImageInfo::Y, ImageInfo::Z, ImageInfo::T}};
  result.filename = files.join(";;");

  return result;
}

ImageInfo getCImgInfo(QString filename)
{
  CImgUS image(qPrintable(filename));
  ImageInfo result;
  result.size = {image.width(), image.height(), image.depth(), image.spectrum(), 1};
  result.labels = false;
  result.axisOrder = {{ImageInfo::X, ImageInfo::Y, ImageInfo::Z, ImageInfo::C, ImageInfo::T}};
  result.filename = filename;
  result.depth = 8;
  return result;
}

ImageInfo getImageInfo(QString filename)
{
  if(filename.endsWith(".tif", Qt::CaseInsensitive) or filename.endsWith(".tiff", Qt::CaseInsensitive))
    return getTIFFInfo(filename);

  ImageInfo result = getImageMagickInfo({filename});

  if(not result)
    result = getCImgInfo(filename);
  return result;
}

std::tuple<QString,int,int> parseImageName(const QString& filename, bool single_image)
{
  if(not filename.contains('?')) {
    return std::make_tuple(filename, -1, -1);
  }
  auto idx = filename.indexOf('?');
  int channel = (single_image ? 0 : -1);
  int timepoint = (single_image ? 0 : -1);
  QString params = filename.mid(idx+1);
  QString result = filename.left(idx);
  QStringList fields = params.split('&', QString::SkipEmptyParts);
  for(const auto& field: fields) {
    auto idx_eq = field.indexOf('=');
    if(idx_eq >= 0) {
      QString name = field.left(idx_eq);
      QString value = field.mid(idx_eq+1);
      if(name == "C") {
        bool ok;
        channel = value.toUInt(&ok);
        if(not ok)
          throw ImageError("Channel ('C') field in filename must be an unsigned integer");
      } else if(name == "T") {
        bool ok;
        timepoint = value.toUInt(&ok);
        if(not ok)
          throw ImageError("Time Point ('T') field in filename must be an unsigned integer");
      } else {
        throw ImageError(QString("Unknown field in filename: '%1'").arg(name));
      }
    }
  }
  return std::make_tuple(result, channel, timepoint);
}

bool isImageList(const QString& filename)
{
  QFile file(filename);
  if(not file.open(QIODevice::ReadOnly))
    return false;
  auto header = file.read(7);
  return header == "[Stack]";
}

void parseImageList(const QString& filename, Point3u& size, Point3f& step, float& brightness, QStringList& files)
{
  if(QFile::exists(filename) and filename.endsWith(".txt", Qt::CaseInsensitive)) {
    if(not isImageList(filename))
      throw ImageError(QString("parseProfileFile::Error, file '%1' cannot be read or doesn't start with '[Stack]' "
                               "and so cannot be a profile file.").arg(filename));

    util::Parms parms(filename);

    bool ok = true;
    ok &= parms("Stack", "SizeX", size.x());
    ok &= parms("Stack", "SizeY", size.y());
    // New version: StepX and StepY and separate
    if(parms("Stack", "StepX", step.x())) {
      parms("Stack", "StepY", step.y());
      parms("Stack", "StepZ", step.z());
    } else {
      // This is for backward compatibility
      if(!parms("Stack", "StepXY", step.x()) or !parms("Stack", "StepZ", step.z())) {
        float zScale;
        if(parms("Stack", "ZScale", zScale)) {
          step.z() = .5;
          step.x() = step.z() / zScale;
        } else
          ok = false;
      }
      step.y() = step.x();
    }
    ok &= parms("Stack", "Brightness", brightness);
    if(!ok)
      throw ImageError(QString("parseProfileFile::Error:Cannot read parameter file: %1").arg(filename));
    parms.all("Stack", "ImageFile", files);
    size.z() = files.size();
    return;
  }
  files = filename.split(";;");
  if(files.size() > 1) {
    //CImgUS first_image(qPrintable(files.at(0)));
    //size = Point3u(first_image.width(), first_image.height(), files.size());
    auto image = getImageInfo(files.at(0));
    size = Point3u(image.size);
    if(size.z() != 1)
      throw ImageError(QString("parseImageList::Error:the file '%1' contains more than one image.")
                       .arg(files.at(0)));
    size.z() = files.size();
    return ;
  }
  throw ImageError(QString("loadStack::Error:Cannot interpret filename: '%1'").arg(filename));
}

} // namespace lgx
