#ifndef UTILITY_HPP
#define UTILITY_HPP

#include <type_traits>
#include <memory>
#include <QHash>
#include <functional>
#include <utility>

namespace std
{
template<typename T, typename... Args>
inline unique_ptr<T> make_unique(Args&&... args)
{
  return unique_ptr<T>(new T(std::forward<Args>(args)...));
}

template <>
struct hash<QString>
{
  size_t operator()(const QString& str) const
  {
    return qHash(str);
  }
};

template <typename _Tp, typename _Tp1>
inline weak_ptr<_Tp> dynamic_pointer_cast(const weak_ptr<_Tp1>& _r) noexcept
{
  auto _r1 = _r.lock();
  if(_Tp* _p = dynamic_cast<_Tp*>(_r1.get()))
    return std::shared_ptr<_Tp>(_r1, _p);
  return weak_ptr<_Tp>();
}

template <typename _Tp, typename _Tp1>
inline _Tp* dynamic_pointer_cast(_Tp1* _r) noexcept
{
  return dynamic_cast<_Tp*>(_r);
}

}

template <bool B>
using EnableIf = typename std::enable_if<B,int>::type;

template <typename SmartPtr>
struct ExtractPointer : public std::unary_function<const SmartPtr&, typename SmartPtr::element_type*>
{
  typename SmartPtr::element_type* operator()(const SmartPtr& arg) const
  {
    return arg.get();
  }
};

template <typename T>
struct WeakPointer : public std::unary_function<const std::shared_ptr<T>&, std::weak_ptr<T>>
{
  typename std::weak_ptr<T> operator()(const std::shared_ptr<T>& arg) const
  {
    return arg;
  }
};

template <typename PtrType, typename SubType>
struct IsSmartPtrInstanceOf : public std::unary_function<const PtrType&, bool>
{
  bool operator()(const PtrType& arg) const
  {
    return (bool) dynamic_cast<SubType*>(arg.get());
  }
};

template <typename Type, typename SubType>
struct IsSmartPtrInstanceOf<std::weak_ptr<Type>, SubType>
  : public std::unary_function<const std::weak_ptr<Type>&, bool>
{
  bool operator()(const std::weak_ptr<Type>& arg) const
  {
    auto sarg = arg.lock();
    return (bool) dynamic_cast<SubType*>(sarg.get());
  }
};

template <typename PtrType, typename SubType>
struct DynamicCastPtr : public std::unary_function<const PtrType&, SubType>
{
  typedef const PtrType& argument_type;
  typedef decltype(std::dynamic_pointer_cast<SubType>(std::declval<PtrType>())) result_type;

  result_type operator()(const PtrType& arg) const
  {
    return std::dynamic_pointer_cast<SubType>(arg);
  }

};

#endif // UTILITY_HPP

