/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef DEFER_HPP
#define DEFER_HPP

#include <LGXConfig.hpp>

#include <utility>
#include <type_traits>

namespace lgx {
namespace util {

template <typename Fct>
struct Defer
{
  typedef typename std::decay<Fct>::type function_type;
  Defer(const function_type& f)
    : fct(f)
  { }

  ~Defer()
  {
    fct();
  }

private:
  function_type fct;
};

template <typename Fct>
struct Defer<Fct&>
{
  Defer(Fct& f)
    : fct(f)
  { }

  ~Defer()
  {
    fct();
  }

private:
  Fct& fct;
};

/**
 * Defer execution of a function when this object is deleted.
 *
 * The function must be a function with no argument.
 */
template <typename Fct>
Defer<Fct&&> defer(Fct&& f)
{
  return Defer<Fct&&>(f);
}

} // namespace util
} // namespace lgx

#endif // DEFER_HPP
