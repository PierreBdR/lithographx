/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef PCAnalysis_HPP
#define PCAnalysis_HPP

#include <Process.hpp>

#include <Information.hpp>
#include <Mesh.hpp>
#include <Stack.hpp>
#include <Misc.hpp>

#include <QRegExp>
#include <QString>
#include <QStringList>

namespace lgx {
namespace process {

/**
 * \class PCAnalysis PCAnalysis.hpp <PCAnalysis.hpp>
 *
 * Perform a PCA on each labeled regions of the segmented stack, and create
 * shapes in the mesh reflecting the main components.
 *
 * \ingroup GlobalProcess
 */
class LGXCORE_EXPORT PCAnalysis : public GlobalProcess {
public:
  PCAnalysis(const GlobalProcess& process)
    : Process(process)
    , GlobalProcess(process)
  {
  }

  bool operator()(const ParmList& parms) override
  {
    if(!checkState().stack())
      return false;
    Stack* stk = currentStack();
    Store* store = stk->currentStore();
    QString fn = parms[0].toString();
    QString correction = parms[1].toString().trimmed();
    Point3f correcting_factor;
    if(correction == "Ellipsoid")
      correcting_factor = sqrt(5);
    else if(correction == "Cuboid")
      correcting_factor = sqrt(3);
    else if(correction == "Elliptical Cylinder")
      correcting_factor = Point3f(sqrt(3), sqrt(4), sqrt(4));
    else if(correction == "Maximum Span")
      correcting_factor = 0;
    else if(correction.endsWith('%')) {
      QString cor = correction.left(correction.size() - 1);
      bool ok;
      float cr = cor.toFloat(&ok);
      if(ok) {
        correcting_factor = -cr / 100.f;
      } else {
        setErrorMessage(QString("Error, percentage value is not valid: '%1'").arg(cor));
        return false;
      }
    } else {
      bool ok;
      float cr = correction.toFloat(&ok);
      if(ok)
        correcting_factor = cr;
      else {
        QStringList vs = util::splitFields(correction);
        if(vs.size() == 3) {
          correcting_factor.x() = vs[0].toFloat(&ok);
          if(!ok) {
            setErrorMessage(QString("Invalid x value for correction factor: '%1'.").arg(vs[0]));
            return false;
          }
          correcting_factor.y() = vs[1].toFloat(&ok);
          if(!ok) {
            setErrorMessage(QString("Invalid y value for correction factor: '%1'.").arg(vs[1]));
            return false;
          }
          correcting_factor.z() = vs[2].toFloat(&ok);
          if(!ok) {
            setErrorMessage(QString("Invalid z value for correction factor: '%1'.").arg(vs[2]));
            return false;
          }
        } else {
          setErrorMessage(QString("Invalid correction string '%1', expected one of 'Ellipsoid'"
                                  ", 'Cuboid', 'Elliptical Cylinder', Maximum Span', a percentage, a single "
                                  "value or three values"));
          return false;
        }
      }
    }
    bool draw_result = false;
    bool res = operator()(stk, store, fn, parms[3].toInt(), parms[2].toString(),
                          correcting_factor, parms[4].toInt(), draw_result);
    if(res and draw_result) {
      Mesh* m = mesh(stk->id());
      m->showSurface();
      m->showLabel();
    }
    return res;
  }

  /**
   * @brief operator ()
   * @param stack Stack to act on
   * @param store Store containing the data to process
   * @param filename File to save the result to
   * @param treshold Threshold for volume detection, used if the store is not labeled
   * @param shape Shape used to draw the cells, "No" or empty string for no drawing.
   * @param correcting_factor Vector giving the correction factor to apply for eigen values
   * if the x coordinate is 0, then maximum span is used instead, and negative values (between 0 and 1)
   * correspond to extracting the percentile of the span.
   * @param number of elements used to discretize the shape (if any)
   * @param draw_result Return whether the mesh has been replaced by cell shapes or not
   * @return
   */
  bool operator()(Stack* stack, Store* store, QString filename, int treshold, QString shape,
                  Point3f correcting_factor, int slices, bool& draw_result);

  QString folder() const override {
    return "Shape Analysis";
  }
  QString name() const override {
    return "PCAnalysis";
  }
  QString description() const override
  {
    return "Compute the principle components of the image. If the threshold is -1, then all the values are used, \n"
           "as is. If 'Draw Result' is set to true, the current mesh will be erased and replaced with shapes \n"
           "representing the cells fit. 'Splan Correction' can be either a shape, a single value of a vector \n"
           "of 3 values, corresponding to the correction to apply for the eigen-values on all three directions.";
  }
  QStringList parmNames() const override
  {
    return QStringList() << "Output"
                         << "Span Correction"
                         << "Draw Result"
                         << "Threshold"
                         << "Shape Details";
  }
  QStringList parmDescs() const override
  {
    return QStringList() << "File to write the output to"
                         << "Span correction can be made using pre-computed formula for regular shapes, or a "
              "percentage of the PC."
                         << "Shape used to display the result"
                         << "If the stack is not labeled, a single volume is considered with all voxels of "
              "intensity greater than this threshold."
                         << "How finely to draw the shape (for cylinders or ellipsoids)";
  }
  ParmList parmDefaults() const override
  {
    return ParmList() << "output.csv"
                      << "Ellipsoid"
                      << false
                      << 100
                      << 3;
  }
  ParmChoiceMap parmChoice() const override
  {
    ParmChoiceMap map;
    map[1] = QStringList() << "Ellipsoid"
                           << "Cuboid"
                           << "Elliptical Cylinder"
                           << "Maximum Span";
    map[2] = QStringList() << "No"
                           << "Ellipsoids"
                           << "Cuboids"
                           << "Cylinder";
    return map;
  }
  QIcon icon() const override {
    return QIcon(":/images/PCAnalysis.png");
  }
};
} // namespace process
} // namespace lgx

#endif // PCAnalysis_HPP
