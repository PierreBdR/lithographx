#include <QFile>
#include <QSettings>
#include <QString>
#include <QTextStream>

#include <stdio.h>

static QTextStream out(stdout);
static QTextStream err(stderr);

void usage(char* prgName)
{
  err << "Usage: " << prgName << " OUTPUT INPUT1 INPUT2 ..." << endl;
}

int main(int argc, char** argv)
{
  int nb_files = argc-2;
  if(nb_files < 0) {
    usage(argv[0]);
    return 1;
  }
  auto outputName = QString::fromUtf8(argv[1]);

  {
    QFile file(outputName);
    if(not file.open(QIODevice::WriteOnly)) {
      err << "Error cannot open file '" << outputName << "' for writing" << endl;
      return 2;
    }
  }

  out << nb_files << " files to process" << endl;

  QList<QVariant> names, versions, descs, deps;

  for(int i = 0 ; i < nb_files ; ++i) {
    auto fileName = QString::fromUtf8(argv[i+2]);
    out << "Processing file '" << fileName << "'" << endl;
    QSettings input(fileName, QSettings::IniFormat);
    input.beginGroup("Package");
    auto name = input.value("Name");
    auto ver = input.value("Version");
    auto dep = input.value("Dependencies", "");
    auto desc = input.value("Description", "");
    input.endGroup();
    if(not name.isNull() and not ver.isNull()) {
      names << name;
      versions << ver;
      descs << desc;
      deps << dep;
    }
  }

  QSettings output(outputName, QSettings::IniFormat);
  output.beginWriteArray("Packages", names.size());
  for(int i = 0 ; i < names.size() ; ++i) {
    output.setArrayIndex(i);
    output.setValue("Name", names[i]);
    output.setValue("Version", versions[i]);
    if(not deps[i].isNull())
      output.setValue("Dependencies", deps[i]);
    if(not descs[i].isNull())
      output.setValue("Description", descs[i]);
  }
  output.endArray();

  return 0;
}
