/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef STACKPROCESS_HPP
#define STACKPROCESS_HPP

/**
 * \file StackProcess.hpp
 * Contains various stack processes
 */

#include <Process.hpp>

#include <Misc.hpp>
#include <Stack.hpp>
#include <Store.hpp>

namespace lgx {
namespace process {

///\addtogroup StackProcess
///@{

/**
 * \class AutoScaleStack StackProcess.hpp <StackProcess.hpp>
 *
 * Scale the stack intensity to fill exactly the whole range.
 */
class LGXCORE_EXPORT AutoScaleStack : public StackProcess {
public:
  AutoScaleStack(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const ParmList& ) override
  {
    if(!checkState().store(STORE_NON_EMPTY))
      return false;
    Store* input = currentStack()->currentStore();
    Store* output = currentStack()->work();
    bool res = (*this)(input, output);
    if(res) {
      input->hide();
      output->show();
    }
    return res;
  }

  /**
   * Scale the stack values, so as to use the whole range.
   */
  bool operator()(const Store* store, Store* output);

  QString name() const override {
    return "Autoscale Stack";
  }
  QString description() const override {
    return "Scale the stack intensity to fill exactly the whole range.";
  }
  QString folder() const override {
    return "Filters";
  }
  QStringList parmNames() const override {
    return QStringList();
  }
  QStringList parmDescs() const override {
    return QStringList();
  }
  QIcon icon() const override {
    return QIcon(":/images/Palette.png");
  }
};

/**
 * \class ApplyTransferFunction StackProcess.hpp <StackProcess.hpp>
 *
 * Transform the stack to reflect the transfer function in use.
 */
class LGXCORE_EXPORT ApplyTransferFunction : public StackProcess {
public:
  ApplyTransferFunction(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const ParmList& parms) override
  {
    if(!checkState().store(STORE_NON_EMPTY))
      return false;
    Store* input = currentStack()->currentStore();
    Store* output = currentStack()->work();
    bool res
      = (*this)(input, output, parms[0].toFloat(), parms[1].toFloat(), parms[2].toFloat(), parms[3].toFloat());
    if(res) {
      input->hide();
      output->show();
    }
    return res;
  }

  /**
   * Apply the transfer function. The final value is computed as a linear combination of the red, green, blue and
   * alpha channels.
   * \param store Input store
   * \param output Output store
   * \param red Coefficient for the red channel
   * \param green Coefficient for the green channel
   * \param blue Coefficient for the blue channel
   * \param alpha Coefficient for the alpha channel
   * \note If the sum of the coefficient is not 1, values can globally increase/decrease
   */
  bool operator()(Store* store, Store* output, float red, float green, float blue, float alpha);

  QString name() const override {
    return "Apply Transfer Function";
  }
  QString description() const override {
    return "Apply the transfer function to the stack (modifies voxel values).";
  }
  QString folder() const override {
    return "Filters";
  }
  QStringList parmNames() const override
  {
    return QStringList() << "Red"
                         << "Green"
                         << "Blue"
                         << "Alpha";
  }
  QStringList parmDescs() const override
  {
    return QStringList() << "Red"
                         << "Green"
                         << "Blue"
                         << "Alpha";
  }
  ParmList parmDefaults() const override
  {
    return ParmList() << 0
                      << 0
                      << 0
                      << 1;
  }
  QIcon icon() const override {
    return QIcon(":/images/Palette.png");
  }
};

/**
 * \class BlobDetect StackProcess.hpp <StackProcess.hpp>
 *
 * Find and label blobs in an image.
 */
class LGXCORE_EXPORT BlobDetect : public StackProcess {
public:
  BlobDetect(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const ParmList& parms) override
  {
    if(!checkState().store(STORE_NON_EMPTY | STORE_NON_LABEL))
      return false;
    bool wat = parmToBool(parms[0]);
    Store* input = currentStack()->currentStore();
    Store* output = currentStack()->work();
    bool res = (*this)(input, output, wat, parms[1].toUInt());
    if(res) {
      input->hide();
      output->show();
    }
    return res;
  }

  bool operator()(const Store* input, Store* output, bool watershed, uint startlabel);

  QString folder() const override {
    return "Segmentation";
  }
  QString name() const override {
    return "Blob Detect";
  }
  QString description() const override {
    return "Find and label blobs in an image";
  }
  QStringList parmNames() const override
  {
    return QStringList() << "Use watershed"
                         << "Start Label";
  }
  QStringList parmDescs() const override
  {
    return QStringList() << "Use watershed"
                         << "Start Label";
  }
  ParmChoiceMap parmChoice() const override
  {
    ParmChoiceMap map;
    map[0] = booleanChoice();
    return map;
  }
  ParmList parmDefaults() const override
  {
    return ParmList() << false
                      << 1;
  }
  QIcon icon() const override {
    return QIcon(":/images/BlobDetect.png");
  }
};

/**
 * \class ClearWorkStack StackProcess.hpp <StackProcess.hpp>
 *
 * Erase the content of the work stack
 */
class LGXCORE_EXPORT ClearWorkStack : public StackProcess {
public:
  ClearWorkStack(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const ParmList& parms) override
  {
    if(!checkState().store(STORE_WORK))
      return false;
    return (*this)(currentStack(), parms[0].toUInt());
  }

  bool operator()(Stack* stack, uint fillValue);

  QString folder() const override {
    return "System";
  }
  QString name() const override {
    return "Clear Work Stack";
  }
  QString description() const override {
    return "Clear the work stack";
  }
  QStringList parmNames() const override {
    return QStringList() << "Fill value";
  }
  QStringList parmDescs() const override {
    return QStringList() << "Fill value";
  }
  ParmList parmDefaults() const override {
    return ParmList() << 0;
  }
  QIcon icon() const override {
    return QIcon(":/images/ClearStack.png");
  }
};

/**
 * \class ClearMainStack StackProcess.hpp <StackProcess.hpp>
 *
 * Erase the content of the main stack
 */
class LGXCORE_EXPORT ClearMainStack : public StackProcess {
public:
  ClearMainStack(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const ParmList& parms) override
  {
    if(!checkState().store(STORE_MAIN))
      return false;
    return (*this)(currentStack(), parms[0].toUInt());
  }

  bool operator()(Stack* stack, uint fillValue);

  QString folder() const override {
    return "System";
  }
  QString name() const override {
    return "Clear Main Stack";
  }
  QString description() const override {
    return "Clear the main stack";
  }
  QStringList parmNames() const override {
    return QStringList() << "Fill value";
  }
  QStringList parmDescs() const override {
    return QStringList() << "Fill value";
  }
  ParmList parmDefaults() const override {
    return ParmList() << 0;
  }
  QIcon icon() const override {
    return QIcon(":/images/ClearStack.png");
  }
};

/**
 * \class StackRelabel StackProcess.hpp <StackProcess.hpp>
 *
 * Relabel a 3D stack to use consecutive labels. The list of cells is
 * randomized before relabeling to ensure a different labeling at each run.
 */
class LGXCORE_EXPORT StackRelabel : public StackProcess {
public:
  StackRelabel(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const ParmList& parms) override
  {
    if(!checkState().store(STORE_NON_EMPTY | STORE_LABEL))
      return false;
    Stack* s = currentStack();
    Store* store = s->currentStore();
    Store* output = s->work();
    int start = parms[0].toInt();
    int step = parms[1].toInt();
    if((*this)(s, store, output, start, step)) {
      store->hide();
      output->show();
      return true;
    }
    return false;
  }

  bool operator()(Stack* stack, const Store* store, Store* output, int start, int step);

  QString folder() const override {
    return "Segmentation";
  }
  QString name() const override {
    return "Relabel";
  }
  QString description() const override
  {
    return "Relabel a 3D stack to use consecutive labels.\n"
           "The cells are shuffled so each relabling will be different.";
  }
  QStringList parmNames() const override
  {
    return QStringList() << "StartLabel"
                         << "Step";
  }
  QStringList parmDescs() const override
  {
    return QStringList() << "StartLabel"
                         << "Step";
  }
  ParmList parmDefaults() const override
  {
    return ParmList() << 1
                      << 1;
  }
  QIcon icon() const override {
    return QIcon(":/images/Relabel.png");
  }
};

/**
 * \class TransformStack StackProcess.hpp <StackProcess.hpp>
 *
 * Set the transformation frame (or transform) matrix to a given value.
 */
class LGXCORE_EXPORT TransformStack : public StackProcess {
public:
  TransformStack(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const ParmList& parms) override;

  bool operator()(Stack* stack, const Matrix4d& transform, bool combine);

  QString folder() const override {
    return "Transform";
  }
  QString name() const override {
    return "Transform Stack";
  }
  QString description() const override {
    return "Set the frame matrix (or transform if trans checked)";
  }
  QStringList parmNames() const override {
    return QStringList() << "Matrix" << "Combine";
  }
  QStringList parmDescs() const override {
    return QStringList() << "Matrix, in column-major format (e.g. consecutive numbers form columns)."
                         << "If true, the matrix is combined (e.g. multiplied) with the current one. "
                            "If false it replaces the current matrix.";
  }
  ParmList parmDefaults() const override {
    return ParmList() << "1 0 0 0 0 1 0 0 0 0 1 0 0 0 0 1" << false;
  }
  ParmChoiceMap parmChoice() const override
  {
    ParmChoiceMap map;
    map[1] = booleanChoice();
    return map;
  }
  QIcon icon() const override {
    return QIcon(":/images/save.png");
  }
};

/**
 * \class SaveTransform StackProcess.hpp <StackProcess.hpp>
 *
 * Save the frame (or transform) matrix to a file, as a list of values in column-major.
 */
class LGXCORE_EXPORT SaveTransform : public StackProcess {
public:
  SaveTransform(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool initialize(ParmList& parms, QWidget* parent);

  bool operator()(const ParmList& parms) override {
    return (*this)(currentStack(), parms[0].toString());
  }

  bool operator()(Stack* stack, const QString& filename);

  QString folder() const override {
    return "Transform";
  }
  QString name() const override {
    return "Save Transform";
  }
  QString description() const override {
    return "Save the frame matrix (or transform if trans checked) to a file";
  }
  QStringList parmNames() const override {
    return QStringList() << "Filename";
  }
  QStringList parmDescs() const override {
    return QStringList() << "Filename";
  }
  ParmList parmDefaults() const override {
    return ParmList() << "";
  }
  QIcon icon() const override {
    return QIcon(":/images/save.png");
  }
};

/**
 * \class LoadTransform StackProcess.hpp <StackProcess.hpp>
 *
 * Load the frame (or transform) matrix from a file containing a list of values in column-major.
 */
class LGXCORE_EXPORT LoadTransform : public StackProcess {
public:
  LoadTransform(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool initialize(ParmList& parms, QWidget* parent);

  bool operator()(const ParmList& parms) override {
    return (*this)(currentStack(), parms[0].toString());
  }

  bool operator()(Stack* stack, const QString& filename);

  QString folder() const override {
    return "Transform";
  }
  QString name() const override {
    return "Load Transform";
  }
  QString description() const override {
    return "Save the frame matrix (or transform if trans checked) from a file";
  }
  QStringList parmNames() const override {
    return QStringList() << "Filename";
  }
  QStringList parmDescs() const override {
    return QStringList() << "Filename";
  }
  ParmList parmDefaults() const override {
    return ParmList() << "";
  }
  QIcon icon() const override {
    return QIcon(":/images/open.png");
  }
};

///@}
} // namespace process
} // namespace lgx

#endif
