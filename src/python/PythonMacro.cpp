/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "Python.hpp"

#include "PythonMacro.hpp"
#include "PluginManager.hpp"
#include "QAlgorithms.hpp"

#include "lgx_module.hpp"

#include <SystemDirs.hpp>
#include <QRegularExpression>
#include <Defer.hpp>
#include <Utility.hpp>

// Python includes
#include <code.h>

namespace lgx {
namespace process {

using python::SafePyObject;

template <typename ProcessType>
PythonMacro<ProcessType>::PythonMacro(const PythonMacroDefinition& def, const ProcessType& process)
  : Process(process)
  , PythonProcess<ProcessType>(process)
  , _def(def)
{
}

python::SafePy<PyTupleObject> makeArgumentList(const ParmList& parms, PythonMacroDefinition::ParameterHandling handling, int extraArgs = 0)
{
  auto args = python::SafePy<PyTupleObject>(PyTuple_New(parms.size() + extraArgs));
  switch(handling) {
    case PythonMacroDefinition::StringOnlyParameters:
      for(Py_ssize_t i = 0 ; i < parms.size() ; ++i) {
        if(PyTuple_SetItem(args, i, python::makeString(parms[i].toString()).release()))
          throw ProcessError("Error, couldn't create argument tuples to launch Python's function");
      }
      break;
    case PythonMacroDefinition::SmartParameters:
    case PythonMacroDefinition::QVariantParameters:
      {
        bool previous = PythonInterpreter::enableAutoConversion(PythonInterpreter::sipQVariantType(),
                                                                handling==PythonMacroDefinition::SmartParameters);
        // Enable auto-conversion of QVariant
        for(Py_ssize_t i = 0 ; i < parms.size() ; ++i) {
          if(PyTuple_SetItem(args, i, python::makePyVariant(parms[i]).release()))
            throw ProcessError("Error, couldn't create argument tuples to launch Python's function");
        }
        PythonInterpreter::enableAutoConversion(PythonInterpreter::sipQVariantType(),
                                                previous);
      }
      break;
  }
  return args;
}

template <typename ProcessType>
python::SafePy<PyTupleObject> PythonMacro<ProcessType>::processParms(const ParmList& parms, PyObject *moduleDictionary)
{
  PyObject* parseFct = PyDict_GetItemString(moduleDictionary, "parseParms");

  auto args = makeArgumentList(parms, _def.parmsHandling);

  if(not parseFct or not PyCallable_Check(parseFct))
    return std::move(args);

  SafePyObject result = PyObject_Call(parseFct, args, nullptr);
  if(not result)
    throw ProcessError("Error while running 'parseParms': " + python::getError());
  if(not PyTuple_Check(result)) {
    auto error = QString("Call to 'parseParms' must return a tuple with one element per parameter.");
    throw ProcessError(error);
  }
  if(PyTuple_Size(result) != parms.size()) {
    auto error = QString("The tuple returned by 'parseParms' contains %1 elements while %2 were expected.")
                    .arg(PyTuple_Size(result)).arg(parms.size());
    throw ProcessError(error);
  }
  return python::SafePy<PyTupleObject>(std::move(result));
}

template <typename ProcessType>
bool PythonMacro<ProcessType>::initialize(ParmList& parms, QWidget* w)
{
  DEBUG_OUTPUT("Initialize Python" << endl);
  python::ProcessRunner::registerBaseProcess(this);

  PythonInterpreter interpreter;
  if(not interpreter.valid())
      return this->setErrorMessage("Python interpreter cannot be initialized.");
  auto defered = util::defer([]() -> void { python::ProcessRunner::popBaseProcess(); });

  DEBUG_OUTPUT("Import module" << endl);
  //SafePyObject moduleName = python::makeString(_def.module);
  auto pyName = python::makeString(_def.module);
  SafePyObject module = PyImport_Import(pyName);
  if(not module) {
    PyErr_Clear();
    throw ProcessError(QString("Couldn't import Python module '%1'").arg(_def.module));
  }

  DEBUG_OUTPUT("Check the initialize function even exists");

  PyObject* mod_dict = PyModule_GetDict(module);
  if(not mod_dict)
    throw ProcessError("Couldn't extract module dictionary");
  PyObject* initFct = PyDict_GetItemString(mod_dict, "initialize");

  if(not initFct or not PyCallable_Check(initFct))
    return true;

  DEBUG_OUTPUT("Creating arguments" << endl);

  //auto args = makeArgumentList(parms, _def.parmsHandling, 1);
  auto args = processParms(parms, mod_dict);
  if(not args)
    return false;

  {
    PyObject* obj = reinterpret_cast<PyObject*>(args.release());
    if(_PyTuple_Resize(&obj, parms.size()+1) == -1)
      throw ProcessError("Error resizing tuple:\n" + python::getError());
    args = python::SafePy<PyTupleObject>(obj);

    // Add the widget
    PyObject *py_widget = python::mapQWidgetToSelf(w);
    PyTuple_SetItem(args, parms.size(), py_widget);
  }

  DEBUG_OUTPUT("Arguments constructed" << endl);
  DEBUG_OUTPUT("Running functon" << endl);
  SafePyObject res = PyObject_Call(initFct, args, nullptr);
  if(PyErr_Occurred())
    throw ProcessError("Error initializing process:\n" + python::getError());
  if(not res or res.get() == Py_False or res.get() == Py_None)
    return false;

  // Make sure the returned argument is an iterable of the right length
  SafePyObject iter = PyObject_GetIter(res);
  if(iter) {
    ParmList vals;
    while(true) {
      SafePyObject next = PyIter_Next(iter);
      if(next) {
        vals << python::getVariant(next);
      } else
        break;
    }
    if(vals.size() != parms.size())
      throw ProcessError(QString("The initialize function returned %1 value when %2 was expected")
                         .arg(vals.size()).arg(parms.size()));
    parms = vals;
  } else if(parms.size() == 1)
    parms[0] = python::getVariant(res);
  else
    throw ProcessError(QString("The initialize function needs to return %1 values").arg(parms.size()));

  return true;
}

template <typename ProcessType>
bool PythonMacro<ProcessType>::operator()(const ParmList& parms)
{
  DEBUG_OUTPUT("Initialize Python" << endl);
  PythonInterpreter interpreter;
  if(not interpreter.valid())
      return this->setErrorMessage("Python interpreter cannot be initialized.");

  python::ProcessRunner::registerBaseProcess(this);

  auto defered = util::defer([]() -> void { python::ProcessRunner::popBaseProcess(); });

  DEBUG_OUTPUT("Import module" << endl);
  auto pyName = python::makeString(_def.module);
  SafePyObject module = PyImport_Import(pyName);
  if(not module) {
    PyErr_Clear();
    throw ProcessError(QString("Couldn't import Python module '%1'").arg(_def.module));
  }

  DEBUG_OUTPUT("Creating arguments" << endl);

  DEBUG_OUTPUT("Arguments constructed" << endl);

  PyObject* mod_dict = PyModule_GetDict(module);
  if(not mod_dict)
    return this->setErrorMessage("Couldn't extract module dictionary");

  //auto args = makeArgumentList(parms, _def.parmsHandling, 1);
  auto args = processParms(parms, mod_dict);

  PyObject* runFct = PyDict_GetItemString(mod_dict, "run");
  DEBUG_OUTPUT("Running functon" << endl);
  if(runFct and PyCallable_Check(runFct)) {
    SafePyObject res = PyObject_Call(runFct, args, nullptr);
    if(PyErr_Occurred())
      return this->setErrorMessage("Error running module:\n" + python::getError());
    if(not res or res.get() == Py_False)
      return false;
  } else
    return this->setErrorMessage(QString("Cannot find function '%1.run'").arg(_def.module));

  return true;
}

template <typename ProcessType>
PythonMacroFactory<ProcessType>::PythonMacroFactory(const PythonMacroDefinition& def)
  : ProcessFactory<ProcessType>()
  , _def(def)
{
}

template <typename ProcessType>
PythonMacroFactory<ProcessType>::PythonMacroFactory(const PythonMacroFactory& copy)
  : ProcessFactory<ProcessType>(copy)
  , _def(copy._def)
{
}

template <typename ProcessType>
PythonMacroFactory<ProcessType>::~PythonMacroFactory()
{
  PythonInterpreter interpreter;
  if(interpreter.valid()) {
    // Get module dictionnary
    if(DEBUG)
      Information::out << "Unloading module '" << _def.module << "'" << endl;
    char mod_attr[] = "modules";
    auto modules = PySys_GetObject(mod_attr);
    auto module_name = _def.module.toUtf8();
    PyDict_DelItemString(modules, module_name.data());
  }
}

template <typename ProcessType>
std::unique_ptr<ProcessType> PythonMacroFactory<ProcessType>::operator()(const ProcessType& process) const
{
  return std::make_unique<PythonMacro<ProcessType>>(_def, process);
}

// Registration of python macros
namespace {

struct MacroFinder {
  MacroFinder()
  {
  }

  ~MacroFinder()
  {
  }

  // This function calls imp.get_suffixes and retrieve the result
  QStringList getSuffixes()
  {
    const char* code =
#if PY_MAJOR_VERSION > 3 || (PY_MAJOR_VERSION == 3 && PY_MINOR_VERSION > 2)
      "from importlib import machinery\n"
      "suffixes = machinery.all_suffixes()"
#else
      "import imp\n"
      "suffixes = [suff[0] for suff in imp.get_suffixes()]"
#endif
      ;
    PyObject *main = PyImport_AddModule("__main__");
    if(not main) {
      Information::err << "Error, couldn't import __main__ module" << endl;
      return {};
    }
    PyObject *main_dict = PyModule_GetDict(main);
    if(not main_dict) {
      Information::err << "Error, couldn't get __main__ module's dictionary" << endl;
      return {};
    }
    SafePyObject dict = PyDict_New();
    SafePyObject suff_result = PyRun_String(code, Py_file_input, main_dict, dict);
    if(not suff_result) {
      Information::err << "Error, couldn't get python's suffixes:\n"
                       << python::getError() << "\n"
                       << "Cannot import Python's macros" << endl;
      return {};
    }
    // Extract the "suffixes" value of the dictionary
    PyObject *suffs = PyDict_GetItemString(dict, "suffixes");
    if(not suffs) {
      Information::err << "Couldn't find the 'suffixes' variable after evaluation.\n"
                       << "Cannot import Python's macros" << endl;
      return {};
    }
    if(not PyList_Check(suffs)) {
      Information::err << "The object 'suffixes' is not a list'" << endl
                       << "Cannot import Python's macros" << endl;
      return {};
    }
    QStringList result;

    Py_ssize_t size = PyList_Size(suffs);
    for(Py_ssize_t i = 0 ; i < size ; ++i) {
      PyObject *item = PyList_GetItem(suffs, i);
      if(not item) {
        Information::err << "Failed to retrieve suffix " << i << endl
                         << "Cannot import Python's macros" << endl;
        return {};
      }
      result << python::getString(item);
    }
    return result;
  }

  PythonMacroDefinition findModule(const QString& module_name)
  {
    PythonMacroDefinition result;
    result.module = module_name;
    result.name = module_name;

    // Load module and get its dictionary
    auto pyName = python::makeString(module_name);
    SafePyObject mod = PyImport_Import(pyName);
    if(not mod) {
      Information::err << "Error, couldn't import module " << module_name << endl
                       << python::getError() << endl;
      return {};
    }

    /*
    mod = PyImport_ReloadModule(mod);
    if(not mod) {
      Information::err << "Error, couldn't reload the module " << module_name << endl
                       << python::getError() << endl;
      return {};
    }
    */

    PyObject *mod_dict = PyModule_GetDict(mod);
    if(not mod_dict) {
      Information::err << "Error, couldn't get dictionary of module " << module_name << endl;
      return {};
    }

    // Find the run function
    PyObject *runFct = PyDict_GetItemString(mod_dict, "run");
    if(not runFct or not PyCallable_Check(runFct)) {
      return {};
    }
    std::vector<SafePyObject> methodStack; // Save all the new references to be destroyed
    while(not PyFunction_Check(runFct)) {
      SafePyObject callRun = PyObject_GetAttrString(runFct, "__call__");
      if(not callRun) {
        Information::err << "Cannot find run function!" << endl;
        return {};
      }
      runFct = callRun;
      methodStack.push_back(std::move(callRun));
    }

    // Find the module documentation
    PyObject *moduleDoc = PyDict_GetItemString(mod_dict, "__doc__");
    if(moduleDoc) {
      auto doc = python::getString(moduleDoc);
      result.description = doc;
    }

    // Find the arguments to the function
    PyObject* defaults = PyFunction_GetDefaults(runFct);
    Py_ssize_t nb_defaults = (defaults ? PyTuple_Size(defaults) : 0);
    PyObject* fct_code_obj = PyFunction_GetCode(runFct);
    if(not fct_code_obj) {
      Information::err << "Couldn't get code information for module " << module_name << endl;
      return {};
    }
    PyCodeObject *fct_code = reinterpret_cast<PyCodeObject*>(fct_code_obj);
#ifdef IS_PY3K
    if(fct_code->co_kwonlyargcount > 0) {
      Information::err << "Error, no keyword only variable allowed!" << endl;
      return {};
    }
#endif
    QString default_value;
    for(Py_ssize_t i = 0 ; i < fct_code->co_argcount ; ++i) {
      PyObject *py_varname = PyTuple_GetItem(fct_code->co_varnames, i);
      QString varname = python::getString(py_varname);
      if(varname.isEmpty()) return {};
      result.parmNames << varname;
      if(i >= fct_code->co_argcount - nb_defaults) {
        PyObject* value = PyTuple_GetItem(defaults, i - (fct_code->co_argcount - nb_defaults));
        if(value)
          default_value = python::getString(value);
        else
          default_value = "";
      }
      result.parms << default_value;
    }

    // Find if there is a __process__ attribute
    PyObject *proc = PyDict_GetItemString(mod_dict, "processName");
    if(proc) {
      result.name = python::getString(proc);
      if(result.name.isEmpty())
        result.name = module_name;
    }

    // Find if there is a __type__ attribute
    PyObject *type = PyDict_GetItemString(mod_dict, "processType");
    if(type) {
      result.type = python::getString(type);
      if(result.type.isEmpty())
        result.type = "Global";
    } else
      result.type = "Global";

    // Find if there is a processDescription attribute
    PyObject *description = PyDict_GetItemString(mod_dict, "processDescription");
    if(description) {
      result.description = python::getString(description);
      if(result.description.isEmpty())
        result.description = result.name;
    } else
      result.description = result.name;

    // Find if there is a __folder__ attribute
    PyObject *folder = PyDict_GetItemString(mod_dict, "processFolder");
    if(folder)
      result.folder = python::getString(folder);
    if(result.folder.isEmpty())
      result.folder = "Python";

    // Find the __paramDescs__ attribute
    PyObject* parmDescs = PyDict_GetItemString(mod_dict, "parmsDescs");
    result.parmDescs = result.parmNames;
    if(parmDescs and PyDict_Check(parmDescs)) {
      for(int i = 0 ; i < result.parms.size() ; ++i) {
        SafePyObject pyname = python::makeString(result.parmNames[i]);
        PyObject* desc = PyDict_GetItem(parmDescs, pyname);
        if(desc)
          result.parmDescs[i] = python::getString(desc);
      }
    }

    // Find if there is a __choice__ attribute
    PyObject* choice = PyDict_GetItemString(mod_dict, "parmsChoice");
    if(choice and PyDict_Check(choice)) {
      for(int i = 0 ; i < result.parms.size() ; ++i) {
        SafePyObject pyname = python::makeString(result.parmNames[i]);
        PyObject* values = PyDict_GetItem(choice, pyname);
        if(values) {
          SafePyObject iter = PyObject_GetIter(values);
          if(iter) {
            QStringList vals;
            while(true) {
              SafePyObject next = PyIter_Next(iter);
              if(next) {
                vals << python::getString(next);
              } else
                break;
            }
            result.parmChoice[i] = vals;
          }
          PyErr_Clear();
        }
      }
    }

    // Find if there is an __icon__ attribute
    PyObject* icon = PyDict_GetItemString(mod_dict, "processIcon");
    if(icon) {
      QString icon_name = python::getString(icon);
      if(icon_name.startsWith(":") or QDir::isAbsolutePath(icon_name))
        result.icon = QIcon(icon_name);
      else {
        // Get the __file__ attribute
        PyObject* file = PyDict_GetItemString(mod_dict, "__file__");
        if(file) {
          QString path = python::getString(file);
          QDir dir = QDir(path).dirName();
          result.icon = QIcon(dir.filePath(icon_name));
        }
      }
    }
    if(result.icon.isNull())
       result.icon = QIcon(":/images/Python.png");

    // Find if the module specifies how to handle parameters
    PyObject *parmType = PyDict_GetItemString(mod_dict, "parmsHandling");
    if(parmType) {
      QString pt = python::getString(parmType).toLower();
      if(pt == "string only")
        result.parmsHandling = PythonMacroDefinition::StringOnlyParameters;
      else if(pt == "qvariant")
        result.parmsHandling = PythonMacroDefinition::QVariantParameters;
      else if(pt == "smart")
        result.parmsHandling = PythonMacroDefinition::SmartParameters;
      else {
        Information::err << "Warning, parmsHandling property of the '"
                         << module_name
                         << "' module is invalid. It should be one of 'string only', 'qvariant' or 'smart'. "
                            "Default to 'smart'" << endl;
        result.parmsHandling = PythonMacroDefinition::SmartParameters;
      }
    } else
      result.parmsHandling = PythonMacroDefinition::SmartParameters;

    return result;
  }

};

struct PythonMacros
{
#ifdef _MSC_VER
  PythonMacros() = default;
  PythonMacros(const PythonMacros&) = default;
  PythonMacros& operator=(const PythonMacros&) = default;

  PythonMacros(PythonMacros&& other)
    : stack(std::move(other.stack))
    , mesh(std::move(other.mesh))
    , global(std::move(other.global))
  { }

  PythonMacros& operator=(PythonMacros&& other)
  {
    stack = std::move(other.stack);
    mesh = std::move(other.mesh);
    global = std::move(other.global);
    return *this;
  }
#endif
  std::vector<StackRegistration> stack;
  std::vector<MeshRegistration> mesh;
  std::vector<GlobalRegistration> global;
};

PythonMacros findMacros()
{
  PythonMacros result;

  MacroFinder finder;
  PythonInterpreter interpreter;
  if(not interpreter.valid())
      return result;

  QStringList moduleNames;

  // Find the ordered list of modules
  {
    // Find all module names
    QStringList extensions;
    extensions << ".py" << ".pyc";

    QStringList nameFilters;

    for(const auto& e: extensions)
      nameFilters << "*"+e;

    QString fe_pat = QString("(%1)$").arg(extensions.join("|"));
    QRegularExpression findExtension(fe_pat);

    for(const QDir& folder: util::macroDirs()) {
      if(folder.exists()) {
        for(QString moduleName: folder.entryList(nameFilters, QDir::Files)) {
          moduleName.remove(findExtension);
          moduleNames << moduleName;
        }
      }
    }
  }

  // Now update the loading order

  if(DEBUG) {
    Information::out << "List of modules:\n";
    for(const auto& m: moduleNames)
      Information::out << " - '" << m << "'\n";
    Information::out << endl;
  }

  for(const auto& module: moduleNames) {
    PythonMacroDefinition info = finder.findModule(module);
    if(not info.name.isEmpty()) {
      LoadingPlugin loader("Python", info.module);
      if(not loader) {
        Information::err << "Cannot create plugin " << loader.fullName() << endl;
        continue;
      }
      if(DEBUG) {
        DEBUG_OUT << "Module found:\n"
                  << "  Module: "      << info.module << "\n"
                  << "  Type: "        << info.type   << "\n"
                  << "  Name: "        << info.name   << "\n"
                  << "  Folder: "      << info.folder << "\n"
                  << "  Variables:\n";
        for(int i = 0 ; i < info.parmNames.size() ; ++i) {
          DEBUG_OUT << "    " << info.parmNames[i] << ": '" << info.parms[i].toString() << "' / " << info.parmDescs[i] << "\n";
          if(info.parmChoice.contains(i)) {
            DEBUG_OUT << "      " << info.parmChoice[i].join(" / ") << endl;
          }
        }
        DEBUG_OUT << endl;
      }
      if(info.type == "Stack")
        result.stack.push_back(StackRegistration(std::make_shared<PythonMacroFactory<StackProcess>>(info), info.name, PROCESS_VERSION));
      else if(info.type == "Mesh")
        result.mesh.push_back(MeshRegistration(std::make_shared<PythonMacroFactory<MeshProcess>>(info), info.name, PROCESS_VERSION));
      else if(info.type == "Global")
        result.global.push_back(GlobalRegistration(std::make_shared<PythonMacroFactory<GlobalProcess>>(info), info.name, PROCESS_VERSION));
      else {
        auto err = QString("Error, invalid process type: %1").arg(info.type);
        loader.fail(err);
        Information::err << err << endl;
        continue;
      }
      loader.validate();
    }
  }

  return result;
}

PythonMacros pythonMacros;

} // namespace

bool loadPythonMacros()
{
  pythonMacros = findMacros();
  return true;
}

bool unloadPythonMacros()
{
  pythonMacros.stack.clear();
  pythonMacros.mesh.clear();
  pythonMacros.global.clear();
  return true;
}

bool finalizePythonMacros()
{
  PythonInterpreter interp;
  interp.finalize();
  return true;
}

REGISTER_MACRO(Python, loadPythonMacros, unloadPythonMacros, finalizePythonMacros);

template class LGXPYTHON_EXPORT PythonMacro<StackProcess>;
template class LGXPYTHON_EXPORT PythonMacro<MeshProcess>;
template class LGXPYTHON_EXPORT PythonMacro<GlobalProcess>;

template class LGXPYTHON_EXPORT PythonMacroFactory<StackProcess>;
template class LGXPYTHON_EXPORT PythonMacroFactory<MeshProcess>;
template class LGXPYTHON_EXPORT PythonMacroFactory<GlobalProcess>;

} // namespace process
} // namespace lgx
