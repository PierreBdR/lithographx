/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "StackGammaFilter.hpp"

#include <Progress.hpp>

namespace lgx {
namespace process {
bool StackGammaFilter::operator()(const Store* input, Store* output, float gamma)
{
  // Check gamma
  if(gamma <= 0)
    throw(QString("Gamma must be positive"));

  // Start the progress bar
  const Stack* stack = output->stack();
  Progress progress(QString("Running Gamma Filter on stack %1").arg(stack->userId()), 0);

  // Get the stack dimensions
  Point3i imgSize = stack->size();

  // Get the input and output images, note they may be the same
  const HVecUS& src = input->data();
  HVecUS& dst = output->data();

#pragma omp parallel for
  for(int z = 0; z < imgSize.z(); ++z)
    for(int y = 0; y < imgSize.y(); ++y)
      for(int x = 0; x < imgSize.x(); ++x) {
        float pix = src[stack->offset(x, y, z)];
        pix = pow(pix / 65535.0, gamma) * 65535;
        dst[stack->offset(x, y, z)] = ushort(pix);
      }
  // Tell LithoGraphX to update the stack
  output->changed();
  // Copy the meta data for the stack in case store is different
  output->copyMetaData(input);

  // Print message in status bar
  SETSTATUS(QString("Processed Gamma Filter on stack %1").arg(stack->userId()));

  return true;
}
}
}
