/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef MULTI_STACK_GAMMA_FILTER_HPP
#define MULTI_STACK_GAMMA_FILTER_HPP

#include <Process.hpp>
#include <Stack.hpp>
#include <Store.hpp>

namespace lgx {
namespace process {
// Gamma filter on mesh signal, inherit from mesh process
class MultiStackGammaFilter : public StackProcess {
public:
  MultiStackGammaFilter(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  // Standard call interface for all processes
  bool operator()(const ParmList& parms) override
  {
    if(!checkState().store(STORE_NON_EMPTY))
      return false;
    Store* input = currentStack()->currentStore();
    Store* output = currentStack()->work();
    bool ok;
    float g = parms[0].toFloat(&ok);
    if(not ok or g <= 0)
      return setErrorMessage("Parameter 'Gamma' must be a positive floating point number.");
    int n = parms[1].toInt(&ok);
    if(not ok or n <= 0)
      return setErrorMessage("Parameter 'Passes' must be a positive integer.");
    bool res = (*this)(input, output, g, n);
    if(res) {
      input->hide();
      output->show();
    }
    return res;
  }

  // Call interface with parameters used
  bool operator()(const Store* input, Store* output, float gamma, int n);

  // Plug-in folder
  QString folder() const override {
    return "Filters";
  }
  // Plug-in name
  QString name() const override {
    return "Multi Gamma Filter";
  }
  // Plug-in long description
  QString description() const override {
    return "Run a gamma correction filter";
  }
  // List of parameter names
  QStringList parmNames() const override {
    return QStringList() << "Gamma" << "Passes";
  }
  // List of parameter long descriptions
  QStringList parmDescs() const override {
    return QStringList() << "Gamma value, must be positive."
                         << "Number of times the gamma filter is applied";
  }
  // List of parameter default values
  ParmList parmDefaults() const override {
    return ParmList() << 0.5 << 2;
  }
  // Plug-in icon
  QIcon icon() const override {
    return QIcon(":/images/GammaFilter.png");
  }
};
}
}
#endif
