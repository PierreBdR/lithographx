$<$<NOT:$<STREQUAL:"${LithoGraphX_${LithoGraphX_PACKAGE_NAME}_COMPONENTS}","">>:
foreach(module ${LithoGraphX_${LithoGraphX_PACKAGE_NAME}_COMPONENTS})
  lgx_find_module($$<0:>{module} TRUE)
  if(NOT LithoGraphX_$$<0:>{module}_FOUND)
    message(WARNING "Cannot load module ${LithoGraphX_PACKAGE_NAME}: missing dependent module $$<0:>{module}.")
    set(LithoGraphX_${LithoGraphX_PACKAGE_NAME}_FOUND FALSE)
  endif()
endforeach(module)>

if(LithoGraphX_${LithoGraphX_PACKAGE_NAME}_FOUND)

  ${${LithoGraphX_PACKAGE_NAME}_FIND_PACKAGES}

  if(NOT TARGET LithoGraphX::${LithoGraphX_PACKAGE_NAME})
    # Check if the package has been installed as user or system
    execute_process(COMMAND $$<0:>{LithoGraphX_lgxPack} --installed ${LithoGraphX_PACKAGE_NAME}
      OUTPUT_VARIABLE LithoGraphX_${LithoGraphX_PACKAGE_NAME}_TYPE
      RESULT_VARIABLE LithoGraphX_${LithoGraphX_PACKAGE_NAME}_TYPE_RESULT
      OUTPUT_STRIP_TRAILING_WHITESPACE)
    if(NOT $$<0:>{LithoGraphX_${LithoGraphX_PACKAGE_NAME}_TYPE_RESULT} EQUAL 0)
      message(FATAL_ERROR "Package '${LithoGraphX_PACKAGE_NAME}' doesn't seem to be installed.")
    endif()
    if(LithoGraphX_${LithoGraphX_PACKAGE_NAME}_TYPE STREQUAL "user")
      set(LithoGraphX_${LithoGraphX_PACKAGE_NAME}_LIB_DIR "$$<0:>{LithoGraphX_USER_LIB_DIR}" CACHE INTERNAL "")
      set(LithoGraphX_${LithoGraphX_PACKAGE_NAME}_INCLUDE_DIRS "$$<0:>{LithoGraphX_USER_PROCESS_DIR}/include/${LithoGraphX_PACKAGE_NAME}" CACHE INTERNAL "")
    else()
      set(LithoGraphX_${LithoGraphX_PACKAGE_NAME}_LIB_DIR "$$<0:>{LithoGraphX_LIB_DIR}" CACHE INTERNAL "")
      set(LithoGraphX_${LithoGraphX_PACKAGE_NAME}_INCLUDE_DIRS "$$<0:>{LithoGraphX_PROCESS_DIR}/include/${LithoGraphX_PACKAGE_NAME}" CACHE INTERNAL "")
    endif()
    list(APPEND LithoGraphX_${LithoGraphX_PACKAGE_NAME}_INCLUDE_DIRS
      ${${LithoGraphX_PACKAGE_NAME}_EXTRA_INCLUDE_DIRS})
    add_library(LithoGraphX::${LithoGraphX_PACKAGE_NAME} ${${LithoGraphX_PACKAGE_NAME}_LIB_TYPE} IMPORTED)
    set(LithoGraphX_${LithoGraphX_PACKAGE_NAME}_VERSION ${${LithoGraphX_PACKAGE_NAME}_VERSION} CACHE INTERNAL "Version of the module")
    set_property(TARGET LithoGraphX::${LithoGraphX_PACKAGE_NAME} PROPERTY
      $<$<BOOL:@WIN32@>:IMPORTED_IMPLIB>$<$<NOT:$<BOOL:@WIN32@>>:IMPORTED_SONAME> $$<0:>{LithoGraphX_${LithoGraphX_PACKAGE_NAME}_LIB_DIR}/$<TARGET_LINKER_FILE_NAME:${${LithoGraphX_PACKAGE_NAME}_MAIN_LIB}>)
    set_property(TARGET LithoGraphX::${LithoGraphX_PACKAGE_NAME} PROPERTY
      IMPORTED_LOCATION $$<0:>{LithoGraphX_${LithoGraphX_PACKAGE_NAME}_LIB_DIR}/$<TARGET_FILE_NAME:${${LithoGraphX_PACKAGE_NAME}_MAIN_LIB}>)
    set_property(TARGET LithoGraphX::${LithoGraphX_PACKAGE_NAME} PROPERTY
      INTERFACE_INCLUDE_DIRECTORIES $$<0:>{LithoGraphX_${LithoGraphX_PACKAGE_NAME}_INCLUDE_DIRS})
    set_property(TARGET LithoGraphX::${LithoGraphX_PACKAGE_NAME} PROPERTY
      INTERFACE_LINK_LIBRARIES LithoGraphX ${${LithoGraphX_PACKAGE_NAME}_SECONDARY_LIBRARIES}
      ${${LithoGraphX_PACKAGE_NAME}_EXTRA_LIBRARIES})
  endif()

endif()
