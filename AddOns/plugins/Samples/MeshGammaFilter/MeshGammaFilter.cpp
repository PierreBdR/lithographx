/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "MeshGammaFilter.hpp"

#include <Mesh.hpp>
#include <Progress.hpp>

namespace lgx {
namespace process {
bool MeshGammaFilter::operator()(Mesh* mesh, float gamma)
{
  // Check gamma
  if(gamma <= 0)
    throw(QString("Gamma must be positive"));

  // Get signal bounds
  Point2f sb = mesh->signalBounds();
  float sr = sb.y() - sb.x();

  // If the signal range is empty throw an error
  if(sr <= 0)
    throw(QString("Empty signal bounds"));

  // Get active vertices. These are selected vertices, or all if none selected
  const std::vector<vertex>& vs = mesh->activeVertices();

  // Start the progress bar
  Progress progress(QString("Running Gamma Filter on mesh %1").arg(mesh->userId()), 0);

#pragma omp parallel for
  for(size_t i = 0; i < vs.size(); ++i) {
    vertex v = vs[i];
    float s = trim((v->signal - sb.x()) / sr, 0.0f, 1.0f);
    s = pow(s, gamma);
    // set the signal and rescale to original range
    v->signal = s * sr + sb.x();
  }

  // Tell LithoGraphX to update the triangle color
  mesh->updateTriangles();

  // Print message in status bar
  SETSTATUS(QString("Processed Gamma Filter on mesh %1").arg(mesh->userId()));

  return true;
}
REGISTER_MESH_PROCESS(MeshGammaFilter);
}
}
