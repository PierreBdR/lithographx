#ifndef VTKMESHCREATION_HPP
#define VTKMESHCREATION_HPP

#include <VTKProcess.hpp>
#include <VTKProgress.hpp>

#include <Stack.hpp>

namespace lgx {
namespace process {
class vtk_EXPORT VTKDiscreteMarchingCube : public MeshProcess {
public:
  VTKDiscreteMarchingCube(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  bool operator()(const ParmList& ) override
  {
    if(!checkState().store(STORE_LABEL).mesh())
      return false;
    const Store* store = currentStack()->currentStore();
    Mesh* mesh = currentMesh();
    return (*this)(store, mesh);
  }

  bool operator()(const Store* store, Mesh* mesh);

  QString folder() const override {
    return "VTK/Creation";
  }
  QString name() const override {
    return "Discrete Marching Cube";
  }
  QString description() const override {
    return "Extract all the labelled cells using VTK marching cube algorithm";
  }
  QStringList parmNames() const override {
    return QStringList();
  }
  QStringList parmDescs() const override {
    return QStringList();
  }
  QIcon icon() const override {
    return QIcon();
  }
};
} // namespace process
} // namespace lgx

#endif // VTKMESHCREATION_HPP
