/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "VTKProcess.hpp"
#include <vtkPolyData.h>
#include <vtkCellArray.h>
#include <vtkVersionMacros.h>

#include <Libraries.hpp>
#include <Store.hpp>
#include <Stack.hpp>

namespace lgx {
namespace process {
VTKImageConverter::VTKImageConverter()
  : vtkImageImport()
  , store(0)
{
}

VTKImageConverter::~VTKImageConverter() {
}

void VTKImageConverter::SetStore(const Store* s)
{
  store = s;
  const Stack* stack = s->stack();
  Point3u size = stack->size();
  Point3f step = stack->step();
  Point3f origin = stack->origin();
  this->SetDataScalarTypeToUnsignedShort();
  this->SetNumberOfScalarComponents(1);
  this->SetDataExtent(0, size.x() - 1, 0, size.y() - 1, 0, size.z() - 1);
  this->SetWholeExtent(0, size.x() - 1, 0, size.y() - 1, 0, size.z() - 1);
  this->SetDataSpacing(step.x(), step.y(), step.z());
  this->SetDataOrigin(origin.x(), origin.y(), origin.z());
  this->SetImportVoidPointer((void*)(&(s->data()[0])));
}

VTKImageConverter* VTKImageConverter::New() {
  return new VTKImageConverter;
}

DECLARE_LIBRARY("VTK", VTK_VERSION,
                "The Visualization Toolkit (VTK) is an open-source, freely available software"
                "system for 3D computer graphics, image processing, and visualization. It"
                "consists of a C++ class library and several interpreted interface layers"
                "including Tcl/Tk, Java, and Python.",
                "http://www.vtk.org", ":/images/VTKLogo.png");

} // namespace process
} // namespace lgx
