#ifndef SIEVE_HPP
#define SIEVE_HPP

#include <Process.hpp>
#include <Stack.hpp>

#include <functional>

namespace lgx {
namespace process {
namespace sieve {

enum FILTER_TYPE
{
  FT_MEDIAN,
  FT_OPENING,
  FT_CLOSING,
  FT_OPENCLOSE,
  FT_CLOSEOPEN
};

/**
 * Return the fitler type from a string
 */
FILTER_TYPE parseFilterType(const QString& type);

/**
 * Get the string for a filter type
 */
QString toString(FILTER_TYPE filter);

enum NUM_DIRS
{
  NDIR_4 = 4,  ///< 2D simply connected pels
  NDIR_8 = 8,  ///< 2D fully connected pels
  NDIR_6 = 6,  ///< 3D simply connected pels
  NDIR_26 = 26 ///< 3D fully connected pels
};

class SieveProgress
{
public:
  template <typename Reporter>
  SieveProgress(Reporter reporter)
    : _reporter(reporter)
  { }

  SieveProgress(const SieveProgress&) = default;
  SieveProgress(SieveProgress&&) = default;

  SieveProgress& operator=(const SieveProgress&) = default;
  SieveProgress& operator=(SieveProgress&&) = default;

  bool canceled() const { return _canceled; }
  bool advance(int value);

private:
  std::function<bool(int)> _reporter;
  bool _canceled = false;
};

/**
 * Base class for 2D and 3D Sieve.
 *
 * Implement the sieving itself, which only relies on the graph anyway
 */
struct Sieve {
  /// Note: implementation based on the 1998 MATLAB MEX file of J. Andrew Bangham

  /// Information stored for each image pel (28 bytes/pel)
  struct graph_node
  {
    intptr_t root_pel;  ///< Pointer to root pel (on the root, it's the opposite of the area of the region)
    intptr_t next_pel; ///< Pointer to next pel in region
    intptr_t next_adj;  ///< Pointer to next distinct boundary pel
  };

  Sieve(const HVecUS& data);

  /**
   * Expand the meshes up to the input area
   */
  bool expand(intptr_t area, FILTER_TYPE filter, SieveProgress progress);

  /**
   * Returns true if an error occured
   */
  bool error() const { return not _errorString.isEmpty(); }

  /**
   * Get the text of the last error
   */
  const QString& errorString() const { return _errorString; }

  size_t nbExtrema(FILTER_TYPE filter) const;

protected:
  // Internal structures
  std::vector<ushort> _image;                                ///< Copy of the image
  std::vector<uint32_t> _adj_flags;                          ///< Flags indicating, for each pel, which directions have different valued pels
  std::vector<intptr_t> _offsets;                            ///< Position shift for each direction in the image
  std::vector<graph_node> _G;                                ///< Graph of pel
  std::vector<intptr_t> _minima;                             ///< List of minima, ordered by position in the image
  std::vector<intptr_t> _maxima;                             ///< List of maxima, ordered by position in the image
  size_t _min_min_area = std::numeric_limits<size_t>::max(); ///< Minimum area of the minima
  size_t _max_min_area = std::numeric_limits<size_t>::max(); ///< Minimum area of the maxima

  mutable QString _errorString;

private:

  /**
   * Merge extrema of area exactly "area"
   */
  bool processExtremaList(intptr_t area, FILTER_TYPE filter, SieveProgress& progress);

  // Process a single extremum, returning its new area if this is still an extremum
  // or 0 if it became an intermediate area
  // This will also update the position of the current extremum if needed
  // Returns the new area and new root of the extremum. If the root is not an extremum anymore,
  // returns {0, -1}
  std::pair<intptr_t, intptr_t> processExtremum(intptr_t init_pos);
};


} // namespace sieve
} // namespace process
} // namespace lgx

#endif // SIEVE_HPP

