/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef QUATERNION_HPP
#define QUATERNION_HPP

/// \file Quaternion.hpp Implements the quaternion object

#include <LGXConfig.hpp>

#include <Geometry.hpp>
#include <Matrix.hpp>
#include <Vector.hpp>

namespace lgx {
namespace util {
/**
 * \class Quaternion Quaternion.hpp <Quaternion.hpp>
 *
 * Implements the quaternion operations
 */
struct Quaternion : public Point4f {
  /**
   * Default constructor.
   *
   * Provides an identity quaternion
   */
  Quaternion()
    : Point4f(0, 0, 0, 1)
  {
  }

  /**
   * Creates a quaternion specified by its components
   */
  Quaternion(float x, float y, float z, float w)
    : Point4f(x, y, z, w)
  {
  }

  /**
   * Copy constructor
   */
  Quaternion(const Quaternion& other)
    : Point4f(other)
  {
  }

  /**
   * Creates a Quaternion corresponding to an axis rotation
   *
   * \param axis Axis of the rotation. It needs not be normalized before
   * hand. If it is null, then the Quaternion will correspond to the identity
   * matrix.
   * \param angle Angle of the rotation.
   */
  Quaternion(const Point3f& axis, float angle);

  /**
   * Creates the quaternion corresponding to the rotation transforming \p
   * from into \p to.
   */
  Quaternion(const Point3f& from, const Point3f& to);

  /**
   * Creates the quaternion corresponding to the rotation matrix \p m
   */
  Quaternion(const Matrix3f& m);

  /**
   * Assignment operator for quaternions
   */
  Quaternion& operator=(const Quaternion& other);

  /**
   * Set the quaternion to the described rotation
   * \param axis Axis of the rotation
   * \param angle Angle of the rotation
   */
  void setAxisAngle(const Point3f& axis, float angle);

  /**
   * Accessing the real part of the quaternion
   */
  float& w() {
    return elems[3];
  }
  /**
   * Accessing the real part of the quaternion
   */
  const float& w() const {
    return elems[3];
  }

  /**
   * Quaternion in-place addition
   */
  Quaternion& operator+=(const Quaternion& other)
  {
    for(size_t i = 0; i < 4; ++i)
      elems[i] += other.elems[i];
    return *this;
  }

  /**
   * Quaternion addition
   */
  Quaternion operator+(const Quaternion& other) const
  {
    Quaternion tmp(*this);
    tmp += other;
    return tmp;
  }

  /**
   * Quaternion multiplication
   */
  Quaternion operator*(const Quaternion& other) const;
  /**
   * Quaternion in-place multiplication
   */
  Quaternion& operator*=(const Quaternion& other);

  /**
   * In-place multiplication of a quaternion by a scalar
   */
  Quaternion& operator*=(float s)
  {
    for(size_t i = 0; i < 4; ++i)
      elems[i] *= s;
    return *this;
  }

  /**
   * Return the quaternion corresponding to the inverse transform
   */
  Quaternion inverse() const
  {
    float n = lgx::util::normsq(*this);
    return Quaternion(-x() / n, -y() / n, -z() / n, w() / n);
  }

  /**
   * Return the conjugate of the current quaternion
   */
  Quaternion conjugate() const {
    return Quaternion(-x(), -y(), -z(), w());
  }

  /**
   * Division of a quaternion by a real number
   */
  Quaternion& operator/=(float v)
  {
    for(size_t i = 0; i < 4; ++i)
      elems[i] /= v;
    return *this;
  }

  /**
   * Division of a quaternion by a real number
   */
  Quaternion operator/(float v) const
  {
    Quaternion tmp(*this);
    tmp /= v;
    return tmp;
  }

  /**
   * Fill the matrix as argument from the quaternion
   *
   * Multiplying with this matrix is equivalent to performing a rotation with
   * this quaternion.
   */
  void setMatrix(Matrix3f& m) const;
  /**
   * Fill the matrix as argument from the quaternion
   *
   * Multiplying with this matrix is equivalent to performing a rotation with
   * this quaternion.
   */
  void setMatrix(Matrix4f& m) const;

  /**
   * Returns the axis of the rotation corresponding to this quaternion
   */
  Point3f axis() const;

  /**
   * Returns the angle of the rotation corresponding to this quaternion
   */
  float angle() const;

  /**
   * Apply the rotation contained in this quaternion on the vector
   */
  Point3f rotate(const Point3f& v) const;

  /**
   * Apply the inverse of the rotation contained in this quaternion on the
   * vector.
   *
   * It is identical to calling \c this->inverse().rotate()
   */
  Point3f inverseRotate(const Point3f& v) const;
};

/**
 * Perform spherical linear interpolation
 *
 * \relates Quaternion
 */
Quaternion slerp(const Quaternion& q1, const Quaternion& q2, float t);

inline Quaternion operator*(float s, const Quaternion& q)
{
  Quaternion tmp(q);
  tmp *= s;
  return tmp;
}

inline Quaternion operator*(const Quaternion& q, float s)
{
  Quaternion tmp(q);
  tmp *= s;
  return tmp;
}
} // namespace util
} // namespace lgx

namespace std {
/**
 * Power of a quaternion
 *
 * \relates Quaternion
 */
lgx::util::Quaternion pow(const lgx::util::Quaternion& q, float p);
} // namespace std

#endif // QUATERNION_HPP
