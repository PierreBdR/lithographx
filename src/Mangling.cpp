/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "Mangling.hpp"
#include <sstream>
#include <QString>
#include <stdlib.h>

#ifdef __GNUC__
#  include <cxxabi.h>
#endif

namespace lgx {
namespace util {
#ifdef __GNUC__
QString qdemangle(std::string s)
{
  std::istringstream ss(s);
  int status;
  std::string name;
  QString result;
  while(ss) {
    ss >> name;
    if(!ss)
      break;
    char* realname = abi::__cxa_demangle(name.c_str(), 0, 0, &status);
    if(status) {
      result += QString::fromStdString(name);
    } else {
      result += QString::fromLocal8Bit(realname);
      free(realname);
    }
    result += " ";
  }
  result.chop(1);
  return result;
}

std::string demangle(std::string s)
{
  QString res = qdemangle(s);
  return res.toStdString();
}

#else
std::string demangle(std::string s) {
  return s;
}

QString qdemangle(std::string s) {
  return QString::fromStdString(s);
}
#endif

QString qdemangle(QString s)
{
  std::string ss = s.toStdString();
  return qdemangle(ss);
}

QString qdemangle(const char* s)
{
  return qdemangle(std::string(s));
}

} // namespace util
} // namespace lgx
