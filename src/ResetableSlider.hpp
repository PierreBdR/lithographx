/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef RESETABLESLIDER_H
#define RESETABLESLIDER_H

#include <LGXConfig.hpp>

#include <QSlider>

class QMouseEvent;

namespace lgx {
namespace gui {

class ResetableSlider : public QSlider {
  Q_OBJECT
public:
  ResetableSlider(QWidget* parent = 0);
  ResetableSlider(Qt::Orientation orientation, QWidget* parent = 0);

  int defaultValue() {
    return default_value;
  }

public slots:
  void setValueAsDefault();
  void setDefaultValue(int val);
  void resetValue();

protected slots:
  void checkDefaultValue(int min, int max);

signals:
  void reset();

protected:
  void init();
  void mouseDoubleClickEvent(QMouseEvent* e);

private:
  int default_value;
};

} // namespace lgx
} // namespace gui
#endif
