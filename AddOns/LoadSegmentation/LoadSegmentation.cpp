/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "LoadSegmentation.hpp"

#include <Dir.hpp>
#include <Image.hpp>
#include <Progress.hpp>
#include <SystemProcessLoad.hpp>

#include <QFileDialog>
#include <algorithm>

namespace lgx {
namespace process {

bool LoadSegmentation::initialize(ParmList& parms, QWidget* parent)
{
  auto filename = parms.at(0).toString();
  if(filename.isEmpty())
    filename = util::currentPath();
  filename = QFileDialog::getOpenFileName(parent, QString("Select stack image file"),
                                          filename, QString("Any files (*.*)"), 0);
  if(filename.isEmpty())
    return false; // Canceled
  parms[0] = filename;
  return true;
}

bool LoadSegmentation::operator()(Stack *stk, Store* output, QString filename, const std::vector<double>& factors)
{
  Progress progress("Loading segmentation", 0, true);
  Image5D image;
  int channel, timepoint;
  std::tie(filename, channel, timepoint) = parseImageName(filename, false);
  if(timepoint < 0) timepoint = 0;
  // Load file
  if(filename.endsWith(".txt", Qt::CaseInsensitive)) {
    QStringList files;
    Point3u size;
    Point3f step;
    float brightness;
    parseImageList(filename, size, step, brightness, files);
    image.brightness = brightness;
    loadImageSequence(files, image, channel, timepoint);
  } else {
    loadImage(filename, image, channel, timepoint);
  }

  Information::out << "Image of total size: " << image.size << endl;

  // Now combine channels
  stk->setSize(Point3u(image.size));

  size_t nb_combined = std::min(factors.size(), size_t(image.nbChannels()));
  std::vector<size_t> shifts(nb_combined, 0);

  Information::out << "Strides = " << image.strides << endl;

  Information::out << "shifts =";
  for(size_t c = 0 ; c < nb_combined ; ++c) {
    shifts[c] = c * image.strides[3];
    Information::out << " " << shifts[c];
  }
  Information::out << endl;

  HVecUS& data = output->data();

  ushort min_value = 65535, max_value = 0;

//#pragma omp parallel for
  for(size_t k = 0 ; k < data.size() ; ++k) {
    double acc = 0.0;
    for(size_t c = 0 ; c < nb_combined ; ++c) {
      auto vox = image[shifts[c]+k];
      acc += factors[c] * vox;
    }
    data[k] = ushort(acc);
    if(data[k] < min_value)
      min_value = data[k];
    if(data[k] > max_value)
      max_value = data[k];
  }

  output->setBounds({min_value, max_value});
  stk->setLabel(max_value);
  stk->setStep(image.step);
  stk->setOrigin(multiply(stk->step(), Point3f(stk->size())) / -2.f);
  actingFile(filename);
  output->setFile(filename);
  // Tell LithoGraphX to update the stack
  output->changed();

  return true;
}
}
}
