smooth centroid out vec3 objectPos;
uniform float offset;

void main()
{
  //texCoord = vec3(gl_TextureMatrix[0] * gl_MultiTexCoord0);
  vec4 vproj = gl_ModelViewProjectionMatrix * gl_Vertex;
  gl_Position = vproj;
  objectPos = gl_Vertex.xyz / gl_Vertex[3];
  gl_FrontColor = gl_Color;
  //gl_ClipVertex = gl_ModelViewMatrix * gl_Vertex;
}
