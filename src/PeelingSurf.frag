uniform sampler2D backDepth;
uniform sampler2D frontDepth;
uniform bool peeling;

smooth centroid in vec4 realPos;
smooth centroid in float depthProj;

#define EPSILON 1e-7

void main()
{
  if(peeling)
  {
    vec3 screenPos = (realPos.xyz / realPos.w + 1.0) / 2.0;
    if(float(texture(backDepth, screenPos.xy).r-EPSILON) <= float(screenPos.z)) // Small delta to avoid calculation errors
      discard;
    else if(float(texture(frontDepth, screenPos.xy).r+EPSILON) >= float(screenPos.z)) // Small delta to avoid calculation errors
      discard;
    else
    {
      setColor();
      if(gl_FragColor.a == 0) discard;
    }
  }
  else
  {
    setColor();
    if(gl_FragColor.a == 0) discard;
  }
}

