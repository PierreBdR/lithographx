/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "ITKProgress.hpp"

#include "ITKFilter.hpp"

#include <itkSmoothingRecursiveGaussianImageFilter.h>
#include <itkCurvatureFlowImageFilter.h>
#include <itkRescaleIntensityImageFilter.h>
#include <itkCurvatureAnisotropicDiffusionImageFilter.h>
#include <itkGradientMagnitudeRecursiveGaussianImageFilter.h>
#include <itkDiscreteGaussianImageFilter.h>
#include <itkSigmoidImageFilter.h>
#include <itkPatchBasedDenoisingImageFilter.h>

namespace lgx {
namespace process {

namespace {

template <size_t dim>
bool itkDiscreteGaussianImageFilter(const Store* input, Store* output, const Point3d& sigma, Process* process)
{
  auto converter = UImageConverter<dim>::New();

  converter->SetStore(input);

  typedef itk::DiscreteGaussianImageFilter<UImageType<dim>, UImageType<dim>> FilterType;

  auto filter = FilterType::New();
  filter->SetUseImageSpacingOn();
  filter->SetVariance(sigma.c_data());
  filter->SetInput(converter->GetOutput());

  ITKProgress progress("Discrete Gaussian Image Filter");
  progress.setFilter(filter);

  filter->Update();

  auto result = filter->GetOutput();

  if(!UImageConverter<dim>::TransferImage(output->stack(), output, result)) {
    process->setErrorMessage("ITK Process tried to change the size of the stack");
    return false;
  }

  output->copyMetaData(input);
  output->changed();
  return true;
}

template <size_t dim>
bool itkSmoothingRecursiveGaussianImageFilter(const Store* input, Store* output, const Point3f& sigma, Process* process)
{
  auto converter = SImageConverter<dim>::New();

  converter->SetStore(input);

  typedef itk::SmoothingRecursiveGaussianImageFilter<SImageType<dim>, SImageType<dim>> FilterType;

  auto filter = FilterType::New();
  typename FilterType::SigmaArrayType sig(sigma.c_data());
  filter->SetSigmaArray(sig);
  filter->SetNormalizeAcrossScale(false);
  filter->SetInput(converter->GetOutput());

  ITKProgress progress("Smoothing Recursive Gaussian Image Filter");
  progress.setFilter(filter);

  filter->Update();

  auto result = filter->GetOutput();

  if(!SImageConverter<dim>::TransferImage(output->stack(), output, result)) {
    process->setErrorMessage("ITK Process tried to change the size of the stack");
    return false;
  }

  output->copyMetaData(input);
  output->changed();
  return true;
}

template <size_t dim>
bool itkCurvatureFlowImageFilter(const Store* input, Store* output, float timestep, int steps, Process* process)
{
  auto converter = UImageConverter<dim>::New();

  converter->SetStore(input);

  typedef itk::CurvatureFlowImageFilter<UImageType<dim>, FImageType<dim>> FilterType;
  auto filter = FilterType::New();
  filter->SetInput(converter->GetOutput());
  filter->SetTimeStep(timestep);
  filter->SetNumberOfIterations(steps);

  typedef itk::RescaleIntensityImageFilter<FImageType<dim>, UImageType<dim>> RescalerType;
  auto rescaler = RescalerType::New();
  rescaler->SetInput(filter->GetOutput());
  rescaler->SetOutputMinimum(0);
  rescaler->SetOutputMaximum(65535);

  ITKProgress progress("Curvature Flow Image Filter");
  progress.setFilter(filter);

  rescaler->Update();

  auto result = rescaler->GetOutput();

  if(!UImageConverter<dim>::TransferImage(output->stack(), output, result)) {
    process->setErrorMessage("ITK Process tried to change the size of the stack");
    return false;
  }

  output->copyMetaData(input);
  output->changed();
  return true;
}

template <size_t dim>
bool itkCurvatureAnisotropicDiffusionImageFilter(const Store* input, Store* output, float timestep,
                                                 int iterations, float conductance, Process* process)
{
  auto converter = FImageConverter<dim>::New();

  converter->SetStore(input);

  typedef itk::CurvatureAnisotropicDiffusionImageFilter<FImageType<dim>, FImageType<dim>> FilterType;
  auto filter = FilterType::New();
  filter->SetInput(converter->GetOutput());
  filter->SetTimeStep(timestep);
  filter->SetNumberOfIterations(iterations);
  filter->SetConductanceParameter(conductance);

  ITKProgress progress("Curvature Anisotropic Diffusion Image Filter");
  progress.setFilter(filter);

  filter->Update();

  auto result = filter->GetOutput();

  if(!FImageConverter<dim>::TransferImage(output->stack(), output, result)) {
    process->setErrorMessage("ITK Process tried to change the size of the stack");
    return false;
  }

  output->copyMetaData(input);
  output->changed();
  return true;
}

template <size_t dim>
bool itkGradientMagnitudeRecursiveGaussianImageFilter(const Store* input, Store* output, float sigma, Process* process)
{
  auto converter = UImageConverter<dim>::New();

  converter->SetStore(input);

  typedef itk::GradientMagnitudeRecursiveGaussianImageFilter<UImageType<dim>, UImageType<dim>> FilterType;
  auto filter = FilterType::New();
  filter->SetInput(converter->GetOutput());
  filter->SetSigma(sigma);

  typedef itk::RescaleIntensityImageFilter<UImageType<dim>, UImageType<dim>> RescalerType;
  auto rescaler = RescalerType::New();
  rescaler->SetInput(filter->GetOutput());
  rescaler->SetOutputMinimum(0);
  rescaler->SetOutputMaximum(65535);

  ITKProgress progress("Gradient Magnitude Recursive Gaussian Image Filter");
  progress.setFilter(filter);

  rescaler->Update();

  auto result = rescaler->GetOutput();

  if(!UImageConverter<dim>::TransferImage(output->stack(), output, result)) {
    process->setErrorMessage("ITK Process tried to change the size of the stack");
    return false;
  }

  output->copyMetaData(input);
  output->changed();
  return true;
}

template <size_t dim>
bool itkSigmoidImageFilter(const Store* input, Store* output, float alpha, float beta, Process* process)
{
  auto converter = UImageConverter<dim>::New();

  converter->SetStore(input);

  typedef itk::SigmoidImageFilter<UImageType<dim>, UImageType<dim>> FilterType;
  auto filter = FilterType::New();
  filter->SetInput(converter->GetOutput());
  filter->SetAlpha(alpha);
  filter->SetBeta(beta);

  ITKProgress progress("Sigmoid Image Filter");
  progress.setFilter(filter);

  filter->Update();

  auto result = filter->GetOutput();

  if(!UImageConverter<dim>::TransferImage(output->stack(), output, result)) {
    process->setErrorMessage("ITK Process tried to change the size of the stack");
    return false;
  }

  output->copyMetaData(input);
  output->changed();
  return true;
}

template <size_t dim>
bool itkPatchBasedDenoising(const Store* input, Store* output, float radius, size_t nb_iteration, const Point3f& bandwidth,
                            bool auto_bandwidth, bool useSmoothDisc, bool useConditionalDerivs, bool useFastTensor,
                            const QString& noiseModel_str, float fidelityWeight, float smoothingWeight, float noise,
                            Process* process)
{
  typedef itk::PatchBasedDenoisingImageFilter<UImageType<dim>, UImageType<dim>> FilterType;
  QString nm = noiseModel_str.toLower();
  typename FilterType::NoiseModelType noiseModel;
  if(nm == "nomodel")
    noiseModel = FilterType::NOMODEL;
  else if(nm == "gaussian")
    noiseModel = FilterType::GAUSSIAN;
  else if(nm == "rician")
    noiseModel = FilterType::RICIAN;
  else if(nm == "poisson")
    noiseModel = FilterType::POISSON;
  else {
    process->setWarningMessage(QString("Warning, no noise model '%1' known, using 'NoModel'").arg(noiseModel_str));
    noiseModel = FilterType::NOMODEL;
  }
  auto converter = UImageConverter<dim>::New();
  converter->SetStore(input);

  auto filter = FilterType::New();
  filter->SetInput(converter->GetOutput());

  // First, set patch and smoothing sizes
  filter->SetPatchRadius(radius);
  filter->SetNumberOfIterations(nb_iteration);
  filter->SetUseSmoothDiscPatchWeights(useSmoothDisc);
  filter->SetKernelBandwidthEstimation(auto_bandwidth);
  if(auto_bandwidth)
    filter->SetKernelBandwidthMultiplicationFactor(bandwidth.x());
  else {
    itk::Array<double> array(dim);
    array[0] = bandwidth.x();
    array[1] = bandwidth.y();
    if(dim == 3)
      array[2] = bandwidth[2];
    filter->SetKernelBandwidthSigma(array);
  }

  // Set options for the computation
  filter->SetComputeConditionalDerivatives(useConditionalDerivs);
  filter->SetUseSmoothDiscPatchWeights(useSmoothDisc);
  filter->SetUseFastTensorComputations(useFastTensor);

  // Set noise model
  filter->SetNoiseModel(noiseModel);
  filter->SetNoiseModelFidelityWeight(fidelityWeight);
  filter->SetSmoothingWeight(smoothingWeight);
  filter->SetNoiseSigma(noise);

  ITKProgress progress("Patch Based Denoising");
  progress.setFilter(filter);

  filter->Update();

  auto result = filter->GetOutput();

  if(!UImageConverter<dim>::TransferImage(output->stack(), output, result))
    return process->setErrorMessage("ITK Process tried to change the size of the stack");

  output->copyMetaData(input);
  output->changed();
  return true;

}

} // namespace

bool ITKDiscreteGaussianImageFilter::operator()(const Store* input, Store* output, const Point3f& sigma)
{
  const Stack* stack = input->stack();
  if(stack->is2D())
    return itkDiscreteGaussianImageFilter<2>(input, output, sigma, this);
  return itkDiscreteGaussianImageFilter<3>(input, output, sigma, this);
}

REGISTER_STACK_PROCESS(ITKDiscreteGaussianImageFilter);

bool ITKSmoothingRecursiveGaussianImageFilter::operator()(const Store* input, Store* output, const Point3f& sigma)
{
  const Stack* stack = input->stack();
  if(stack->is2D())
    return itkSmoothingRecursiveGaussianImageFilter<2>(input, output, sigma, this);
  return itkSmoothingRecursiveGaussianImageFilter<3>(input, output, sigma, this);
}
REGISTER_STACK_PROCESS(ITKSmoothingRecursiveGaussianImageFilter);

bool ITKCurvatureFlowImageFilter::operator()(const Store* input, Store* output, float timestep, int steps)
{
  const Stack* stack = input->stack();
  if(stack->is2D())
    return itkCurvatureFlowImageFilter<2>(input, output, timestep, steps, this);
  return itkCurvatureFlowImageFilter<3>(input, output, timestep, steps, this);
}
REGISTER_STACK_PROCESS(ITKCurvatureFlowImageFilter);

bool ITKCurvatureAnisotropicDiffusionImageFilter::operator()(const Store* input, Store* output, float timestep,
                                                             int iterations, float conductance)
{
  const Stack* stack = input->stack();
  if(stack->is2D())
    return itkCurvatureAnisotropicDiffusionImageFilter<2>(input, output, timestep, iterations, conductance, this);
  return itkCurvatureAnisotropicDiffusionImageFilter<3>(input, output, timestep, iterations, conductance, this);
}
REGISTER_STACK_PROCESS(ITKCurvatureAnisotropicDiffusionImageFilter);

bool ITKGradientMagnitudeRecursiveGaussianImageFilter::operator()(const Store* input, Store* output, float sigma)
{
  const Stack* stack = input->stack();
  if(stack->is2D())
    return itkGradientMagnitudeRecursiveGaussianImageFilter<2>(input, output, sigma, this);
  return itkGradientMagnitudeRecursiveGaussianImageFilter<3>(input, output, sigma, this);
}
REGISTER_STACK_PROCESS(ITKGradientMagnitudeRecursiveGaussianImageFilter);

bool ITKSigmoidImageFilter::operator()(const Store* input, Store* output, float alpha, float beta)
{
  const Stack* stack = input->stack();
  if(stack->is2D())
    return itkSigmoidImageFilter<2>(input, output, alpha, beta, this);
  return itkSigmoidImageFilter<3>(input, output, alpha, beta, this);
}
REGISTER_STACK_PROCESS(ITKSigmoidImageFilter);

bool ITKPatchBasedDenoising::operator()(const Store* input, Store* output, float radius, size_t nb_iteration, const Point3f& bandwidth,
                                        bool auto_bandwidth, bool useSmoothDisc, bool useConditionalDerivs, bool useFastTensor,
                                        const QString& noiseModel_str, float fidelityWeight, float smoothingWeight, float noise)
{
  const Stack* stack = input->stack();
  if(stack->is2D())
    return itkPatchBasedDenoising<2>(input, output, radius, nb_iteration, bandwidth,
                                     auto_bandwidth, useSmoothDisc, useConditionalDerivs, useFastTensor,
                                     noiseModel_str, fidelityWeight, smoothingWeight, noise, this);
  return itkPatchBasedDenoising<3>(input, output, radius, nb_iteration, bandwidth,
                                   auto_bandwidth, useSmoothDisc, useConditionalDerivs, useFastTensor,
                                   noiseModel_str, fidelityWeight, smoothingWeight, noise, this);
}

REGISTER_STACK_PROCESS(ITKPatchBasedDenoising);

} // namespace process
} // namespace itk
