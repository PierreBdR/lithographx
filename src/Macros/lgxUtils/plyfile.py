#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import division, absolute_import, print_function

import numpy as np

import sys
import re
#import pandas as pd

PY3 = sys.version > '3'

typeNames = ['uchar', 'char', 'ushort', 'short', 'uint', 'int', 'ulong', 'long', 'float', 'double']
typeIds = [np.uint8, np.int8, np.uint16, np.int16, np.uint32, np.int32, np.uint64, np.int64, np.float32, np.float64]

system_endianess = np.dtype(np.uint16).str[0]


def isproperty(obj, name):
    try:
        return isinstance(getattr(type(obj), name), property)
    except AttributeError:
        return False


class PlyProperty(object):
    """
    Object holding a property of an element
    """
    def __init__(self, name, parent=None, **kwords):
        """
        Parameters
        ----------
        name: str
            Name of the property
        parent: PlyElement
            element containing the property
        valueType: str
            must be one of :py:`typeNames`
        valueTypeId: np.typeid
            must be one of :py:`typeIds`
        kind: str
            one of 'value' or 'list'
        sizeType: str
            in case :py:`kind` is 'list', this holds the type used to store the length of the list
        sizeTypeId: np.typeid
            in case :py:`kind` is 'list', this holds the type used to store the length of the list
        """
        self._name = name
        self._valueType = None
        self._kind = 'value'
        self._valueType = 'double'
        self._valueTypeId = np.float64
        self._sizeType = 'uchar'
        self._sizeTypeId = np.uint8
        self._parent = parent
        self._content = None

        if parent is not None:
            parent._add(self)

        for prop in kwords:
            assert isproperty(self, prop), "Error, there is no property called '{0}'".format(prop)
            setattr(self, prop, kwords[prop])

    @property
    def byteorder(self):
        """
        Order of the bytes in memory
        """
        return self.valueTypeId.byteorder

    @property
    def dtype(self):
        """
        Return the dtype used to store the array
        """
        return self._content.dtype

    @byteorder.setter
    def byteorder(self, bo):
        assert self._content is not None
        if bo == '=':
            bo = system_endianess
        cur_bo = self.byteorder
        if cur_bo == '=':
            cur_bo = system_endianess
        if bo != cur_bo:  # We need to swap
            self._valueTypeId = self.valueTypeId.newbyteorder(bo)
            self._sizeTypeId = self.sizeTypeId.newbyteorder(bo)
            if self.kind == 'value':
                self._content = self._content.byteswap(True).newbyteorder()
            else:
                self._content = [lst.byteswap(True).newbyteorder() for lst in self._content]

    @byteorder.deleter
    def byteorder(self):
        assert self.allocated
        self.byteorder = system_endianess

    def writeAsciiValue(self, pos, fields):
        """
        Write the value in ASCII format
        """
        if self.valueType in ('float', 'double'):
            fields.append("{:.9g}".format(self[pos]))
        else:
            fields.append(str(self[pos]))

    def writeAsciiList(self, pos, fields):
        """
        Write the list in ASCII format
        """
        lst = self[pos]
        fields.append(str(len(lst)))
        if self.valueType in ('float', 'double'):
            fields += ["{:9g}".format(p) for p in lst]
        else:
            fields += [str(p) for p in lst]

    def writeBinaryValue(self, pos, f):
        """
        Write the value in binary
        """
        f.write(self[pos].tostring())

    def writeBinaryList(self, pos, f):
        """
        Write the list in binary
        """
        lst = self[pos]
        f.write(np.array(len(lst), dtype=self.sizeTypeId).tostring())
        f.write(lst.tostring())

    def readAsciiValue(self, pos, fields):
        """
        Read a value in ASCII
        """
        self[pos] = self.valueTypeId.type(fields.pop(0))

    def readAsciiList(self, pos, fields):
        """
        Read a list in ASCII
        """
        size = self.sizeTypeId.type(fields.pop(0))
        self.resize(pos, size)
        lst = self[pos]
        for i in range(size):
            lst[i] = self.valueTypeId.type(fields.pop(0))

    def readBinaryValue(self, pos, buff):
        """
        Read a value in binary
        """
        self[pos] = np.fromfile(buff, dtype=self.valueTypeId, count=1, sep='')

    def readBinaryList(self, pos, buff):
        """
        Read a list in binary
        """
        size = np.fromfile(buff, dtype=self.sizeTypeId, count=1, sep='')
        lst = np.fromfile(buff, dtype=self.valueTypeId, count=size, sep='')
        self._content[pos] = lst

    @property
    def allocated(self):
        """
        True if the content has been allocated
        """
        return self._content is not None

    @property
    def content(self):
        """
        Get the content of the property
        """
        return self._content

    def __repr__(self):
        content = [repr(self.name),
                   "valueType={0}".format(repr(self.valueType)),
                   "kind={0}".format(repr(self.kind))]
        if self.kind == 'list':
            content.append("sizeType={0}".format(repr(self.sizeType)))
        return "{0}.PlyProperty({1})".format(__name__, ", ".join(content))

    @property
    def name(self):
        """
        Name of the property
        """
        return self._name

    @name.setter
    def name(self, n):
        if n != self._name:
            if self.parent is not None:
                self.parent._rename(self, n)
            self._name = n

    @property
    def valueType(self):
        """
        Type of the values.

        Notes
        -----
        Must be an element of `typeNames`.
        """
        return self._valueType

    @valueType.setter
    def valueType(self, v):
        assert self._content is None, "Error, you can't change types after the property has been allocated"
        assert v in typeNames, "Error, the type must be one of: {}".format(", ".join(typeNames))
        self._valueType = v
        self._valueTypeId = typeIds[typeNames.index(v)]

    @property
    def valueTypeId(self):
        """
        Numpy-equivalent type of `valueType`
        """
        return self._valueTypeId

    @valueTypeId.setter
    def valueTypeId(self, v):
        assert self._content is None, "Error, you can't change types after the property has been allocated"
        assert v in typeIds, "Error, the type must be one of: {}".format(", ".join(repr(ti) for ti in typeIds))
        self._valueTypeId = v
        self._valueType = typeNames[typeIds.index(v)]

    @property
    def sizeType(self):
        """
        For a list, type of the list size.

        Notes
        -----
        Must be an element of `typeNames`.
        """
        return self._sizeType

    @sizeType.setter
    def sizeType(self, v):
        assert self._content is None, "Error, you can't change types after the property has been allocated"
        assert v in typeNames[:-2], "Error, the size type must be one of: {}".format(", ".join(typeNames[:-2]))
        self._sizeType = v
        self._sizeTypeId = typeIds[typeNames.index(v)]

    @property
    def sizeTypeId(self):
        """
        Numpy-equivalent type of `sizeType`
        """
        return self._sizeTypeId

    @sizeTypeId.setter
    def sizeTypeId(self, v):
        assert self._content is None, "Error, you can't change types after the property has been allocated"
        assert v in typeIds, "Error, the type must be one of: {}".format(", ".join(repr(ti) for ti in typeIds))
        self._sizeTypeId = v
        self._sizeType = typeNames[typeIds.index(v)]

    @property
    def parent(self):
        """
        Element this property is attached to, if any.
        """
        return self._parent

    @parent.setter
    def parent(self, p):
        if p is not self._parent:
            if self._parent is not None:
                self._parent._remove(self)
            self._parent = p
            if p is not None:
                self._parent._add(self)

    @property
    def kind(self):
        """
        Kind of propety: 'list' or 'value'
        """
        return self._kind

    @kind.setter
    def kind(self, k):
        assert self._content is None, "Error, cannot change the kind of a property after allocation"
        assert k in ('list', 'value'), "Error, 'kind' must be either 'value' or 'list'"
        self._kind = k

    def deallocate(self):
        """
        Free the content of this property
        """
        assert self.content is not None
        self._content = None
        self.valueTypeId = self.valueTypeId.type
        self.sizeTypeId = self.sizeTypeId.type

    def allocate(self, size, byteorder=system_endianess):
        """
        Allocate the content of this property
        """
        self.valueTypeId = np.dtype(self.valueTypeId).newbyteorder(byteorder)
        self.sizeTypeId = np.dtype(self.sizeTypeId).newbyteorder(byteorder)
        if self.kind == 'value':
            self._content = np.empty((size,), dtype=self.valueTypeId)
        else:
            self._content = np.empty((size,), dtype=object)
            self._content[:] = [np.empty((0,), dtype=self.valueTypeId)] * size

    def resize(self, elmt, size):
        """
        Resize the property.

        Notes
        -----
        The property needs to be allocated first.
        """
        assert self.kind == 'list', "This method can only be called on a list"
        assert self._content is not None, "Error, list hasn't been allocated yet"
        self._content[elmt] = np.empty((size,), dtype=self.valueTypeId)

    def __getitem__(self, item):
        """
        Access values within the property
        """
        assert self._content is not None, "Error, property hasn't been allocated"
        return self._content[item]

    def __setitem__(self, item, value):
        if self.kind == 'value':
            self._content[item] = value
        else:
            value = np.asarray(value, dtype=self.valueTypeId)
            assert len(value.shape) == 1, "You can only store 1D lists in a property"
            self._content[item] = value

    def __delitem__(self, item):
        assert self.kind == 'list', "This method can only be called on list properties"
        self._content[item] = np.empty((0,), dtype=self.valueTypeId)

    def __len__(self):
        assert self._content is not None, "The property hasn't been allocated yet"
        return len(self._content)

class PlyElement(object):
    """
    Object representing an element in a Ply file.

    An element has properties, which are stored in separate arrays. The properties can be accessed directly by appending
    a '_' to their name.
    """
    def __init__(self, name, size=0, parent=None, **kwords):
        """
        Create a new element without properties
        """
        self._props = []
        self._props_map = {}
        self._size = size
        self._name = name
        self._allocated = False
        self._parent = parent
        self._byteorder = system_endianess

        if parent is not None:
            parent._add(self)

        for p in kwords:
            assert isproperty(self, p), "Error, there is no property calld '{}'".format(p)
            setattr(self, p, kwords[p])

    def __repr__(self):
        return "{0}.PlyElement<name='{1}', size={2}, props=[{3}]>".format(__name__,
                                                                          self.name,
                                                                          self.size,
                                                                          ", ".join(p.name for p in self))

    @property
    def parent(self):
        """
        Get the file the element is a part of, if any.
        """
        return self._parent

    @parent.setter
    def parent(self, p):
        if p is not self._parent:
            if self._parent is not None:
                self._parent._remove(self)
            self._parent = p
            if p is not None:
                p._add(self)

    @property
    def name(self):
        """
        Name of the element
        """
        return self._name

    def keys(self):
        """
        Iterator on the property names
        """
        return self._props_map.keys()

    def values(self):
        """
        Iterator on the properties
        """
        return self._props_map.values()

    def to_dict(self):
        """
        Return a dictionnary of properties
        """
        return {k.name: k.content for k in self}

    @name.setter
    def name(self, n):
        if n != self._name:
            if self.parent is not None:
                self.parent._rename(self, n)
            self._name = n

    @property
    def size(self):
        """
        Number of items in this element
        """
        return self._size

    @size.setter
    def size(self, s):
        assert not self.allocated, "You cannot resize an element after it has been allocated."
        self._size = s

    def _rename(self, prop, new_name):
        old_name = prop.name
        assert old_name in self._props_map, "Error, renaming a property that is not defined in this element"
        if old_name != new_name:
            idx = self._props_map[old_name]
            if new_name in self._props_map:
                raise ValueError("Error, there is already a property called '{0}' "
                                 "in element '{1}'".format(new_name, self.name))
            del self._props_map[old_name]
            delattr(self, '{}_'.format(old_name))
            self._props_map[new_name] = idx
            setattr(self, '{}_'.format(new_name), prop)

    def add(self, prop_name, **kwords):
        """
        Add a property, specifying its arguments.
        """
        return PlyProperty(prop_name, parent=self, **kwords)

    def _add(self, prop):
        if prop.name in self._props_map:
            raise ValueError("Error, element '{0}' already has a property "
                             "named '{1}'".format(self.name, prop.name))
        self._props_map[prop.name] = len(self._props)
        self._props.append(prop)
        setattr(self, '{}_'.format(prop.name), prop)

    def __iter__(self):
        """
        Iterate on the property names
        """
        return iter(self._props)

    def __len__(self):
        """
        Number of properties in this element
        """
        return len(self._props)

    def __getitem__(self, idx):
        """
        Get a property by name or position
        """
        if isinstance(idx, str):
            return self._props[self._props_map[idx]]
        else:
            return self._props[idx]

    def _remove(self, prop):
        try:
            name = prop.name
        except AttributeError:
            name = prop
        assert name in self._props_map, "The property '{0}' doesn't exist in element '{1}'".format(name, self.name)
        idx = self._props_map[name]
        for i in range(idx + 1, len(self)):
            self._props_map[self._props[i].name] -= 1
        del self._props[idx]
        del self._props_map[name]

    def remove(self, prop):
        """
        Remove a property
        """
        try:
            assert prop.parent is self, "Error, property is not in this element"
        except AttributeError:
            prop = self[prop]
        prop.parent = None

    def allocate(self, byteorder='='):
        """
        Allocate the memory for all properties in this element.
        """
        assert not self.allocated, "Error, element has already been allocated"
        assert self.size > 0, "Error, cannot allocate 0 element"
        for prop in self._props:
            prop.allocate(self.size, byteorder)
        self._allocated = True

    @property
    def byteorder(self):
        """
        Specify the byte order for the properties
        """
        assert self.allocated
        return self._byteorder

    @byteorder.setter
    def byteorder(self, bo):
        assert self.allocated
        if bo == '=':
            bo = system_endianess
        if bo != self._byteorder:
            self._byteorder = bo
            for p in self._props:
                p.byteorder = bo

    @byteorder.deleter
    def byteorder(self):
        assert self.allocated
        self._byteorder = system_endianess
        for p in self._props:
            del p.byteorder

    def deallocate(self):
        """
        De-allocate the memory for the properties
        """
        if self.allocated:
            for prop in self._props:
                prop.deallocate()
            self._allocated = False

    @property
    def allocated(self):
        """
        Check if memory has been allocated
        """
        return self._allocated

    @property
    def df(self):
        """
        Get a DataFrame with the data
        """
        #return pd.DataFrame({p.name: p.content for p in self})
        res = np.recarray((self.size,), dtype=[(p.name, p.dtype.str) for p in self])
        for p in self:
            res[p.name] = p.content
        return res


class PlyFile(object):
    def __init__(self, filename=None, **kwords):
        """
        Create a new PlyFile.
        """
        self._elements = []
        self._elements_map = {}
        self._filetype = 'ply'
        self._format = 'ascii'
        self._version = "1.0"
        self._allocated = False

        for attr in kwords:
            assert isproperty(self, attr), "Unknown property '{0}'".format(attr)
            setattr(self, attr, kwords[attr])

        if filename is not None:
            self.parse(filename)

    def __repr__(self):
        return "{0}.PlyFile<format='{1}',version='{2}',elements=[{3}]>".format(__name__,
                                                                               self.format,
                                                                               self.version,
                                                                               ", ".join(el.name for el in self))

    def __len__(self):
        """
        Number of elements in the file
        """
        return len(self._elements)

    def __iter__(self):
        """
        Iterate over the element names.
        """
        return iter(self._elements)

    @property
    def filetype(self):
        """
        Type of file
        """
        return self._filetype

    @filetype.setter
    def filetype(self, ft):
        assert re.match(r'^\w+$', ft)
        self._filetype = ft

    @property
    def format(self):
        """
        Format of the file, must be one of 'ascii', 'binary_little_endian' or 'binary_big_endian'.
        """
        return self._format

    @format.setter
    def format(self, f):
        assert f in ['ascii', 'binary_little_endian', 'binary_big_endian']
        self._format = f

    def set_ascii(self):
        """
        Set the format to ASCII
        """
        self.format = 'ascii'

    def set_binary_little_endian(self):
        '''
        Set the format to binary, little endian
        '''
        self.format = 'binary_little_endian'

    def set_binary_big_endian(self):
        '''
        Set the format to binary, big endian
        '''
        self.format = 'binary_big_endian'

    def clear(self):
        """
        Remove all elements from the file
        """
        els = list(self)
        for e in els:
            e.parent = None
        self._format = 'ascii'
        self._version = "1.0"
        self._allocated = False

    def deallocate(self):
        """
        Free the memory used by the properties of the elements.
        """
        for e in self:
            e.deallocate()
        self._allocated = False

    def allocate(self, byteorder='='):
        """
        Allocate the memory for the properties of the elements
        """
        for e in self:
            e.allocate(byteorder)
        self._allocated = True

    @property
    def allocated(self):
        """
        Check if the memory has been allocated
        """
        return self._allocated

    @property
    def byteorder(self):
        """
        Byte order used in memory
        """
        return self._elements[0].byteorder

    @byteorder.setter
    def byteorder(self, byteorder):
        assert self.allocated
        for e in self:
            e.byteorder = byteorder

    @byteorder.deleter
    def byteorder(self):
        assert self.allocated
        for e in self:
            del e.byteorder

    def add(self, element_name, **kwords):
        """
        Add an element by name
        """
        return PlyElement(element_name, parent=self, **kwords)

    def _add(self, el):
        assert el.name not in self._elements_map, "Error, ply file already has a property named '{0}'".format(el.name)
        self._elements_map[el.name] = len(self._elements)
        self._elements.append(el)
        setattr(self, '{}_'.format(el.name), el)

    def _remove(self, el):
        try:
            name = el.name
        except AttributeError:
            name = el
        assert name in self._elements_map, "The element '{0}' doesn't exist in ply file".format(name)
        idx = self._elements_map[name]
        for i in range(idx + 1, len(self)):
            self._elements_map[self._elements[i].name] -= 1
        del self._elements[idx]
        del self._elements_map[name]

    def remove(self, el):
        """
        Remove an element
        """
        try:
            assert el.parent is self, "Error, element is not in this ply file"
        except AttributeError:
            el = self[el]
        el.parent = None

    def _rename(self, el, new_name):
        old_name = el.name
        assert old_name in self._elements_map, "Error, renaming an element that is not defined in this file"
        if old_name != new_name:
            idx = self._props_map[old_name]
            if new_name in self._props_map:
                raise ValueError("Error, there is already a property called '{0}' "
                                 "in element '{1}'".format(new_name, self.name))
            del self._props_map[old_name]
            delattr(self, '{}_'.format(old_name))
            self._props_map[new_name] = idx
            setattr(self, '{}_'.format(new_name), new_name)

    def __getitem__(self, idx):
        if isinstance(idx, str):
            return self._elements[self._elements_map[idx]]
        else:
            return self._elements[idx]

    @property
    def version(self):
        """
        Version of this file
        """
        return self._version

    @version.setter
    def version(self, v):
        try:
            major, minor = v.split(".")
            int(major)
            int(minor)
        except ValueError:
            raise ValueError("Invalid version number '{0}'. It shoud be MAJOR.MINOR".format(v))
        self._version = v

    def write(self, filename):
        """
        Write the content to a file
        """
        with open(filename, 'w') as f:
            self.writeHeader(f)
            if self.format == 'ascii':
                self.writeAsciiContent(f)
            else:
                self.writeBinaryContent(f)

    def writeHeader(self, f):
        print(self.filetype, file=f)
        print("format", self.format, self.version, file=f)
        print("comment File generated by plyfile.py", file=f)
        for el in self:
            print("element", el.name, el.size, file=f)
            for prop in el:
                content = ["property"]
                if prop.kind == 'list':
                    content += ["list", prop.sizeType]
                content += [prop.valueType, prop.name]
                print(*content, file=f)
        print("end_header", file=f)

    def writeAsciiContent(self, f):
        for el in self:
            for pos in range(el.size):
                line = []
                for prop in el:
                    if prop.kind == 'list':
                        prop.writeAsciiList(pos, line)
                    else:
                        prop.writeAsciiValue(pos, line)
                print(*line, file=f)

    def writeBinaryContent(self, f):
        if self.format == 'binary_little_endian':
            self.byteorder = '<'
        else:
            self.byteorder = '>'
        for el in self:
            for pos in range(el.size):
                for prop in el:
                    if prop.kind == 'list':
                        prop.writeBinaryList(pos, f)
                    else:
                        prop.writeBinaryValue(pos, f)

    def parse(self, filename):
        """
        Parse a file
        """
        with open(filename, 'rb') as f:
            self.parseHeader(f)
            if self.format == 'ascii':
                self.parseAsciiContent(f)
            else:
                self.parseBinaryContent(f)

    def parseHeader(self, f):
        """
        Parse only the header of a file
        """
        line = f.readline().strip().decode('ascii')
        self.fileformat = line
        #assert line == 'ply', "Error, the first line must be 'ply'"
        form = f.readline().strip().decode('ascii')
        fields = form.split()
        assert len(fields) == 3 and fields[0] == 'format', "Error, the second line must contain the format"
        self.clear()
        self.format = fields[1]
        self.version = fields[2]
        current_element = None
        line_nb = 2
        while not f.closed:
            line = f.readline().strip().decode('ascii')
            line_nb += 1
            if not line:
                continue
            if line == 'end_header':
                break
            fields = line.split()
            if fields[0] == 'element':
                if len(fields) != 3:
                    raise ValueError("Error on line {0}, the element definition is 'element NAME SIZE'".format(line_nb))
                current_element = self.add(fields[1], size=int(fields[2]))
            elif fields[0] == 'property':
                if current_element is None:
                    raise ValueError("Error on line {0}, the property is outside any element".format(line_nb))
                if len(fields) <= 2:
                    raise ValueError("Error on line {0}, a property has at least 3 parts".format(line_nb))
                if fields[1] == 'list':
                    if len(fields) != 5:
                        raise ValueError("Error on line {0}, "
                                         "a list property is 'property list SIZE_TYPE VALUE_TYPE NAME'".formar(line_nb))
                    current_element.add(fields[4], kind='list', sizeType=fields[2], valueType=fields[3])
                else:
                    if len(fields) != 3:
                        raise ValueError("Error on line {0}, "
                                         "a value property is 'property VALUE_TYPE NAME'".formar(line_nb))
                    current_element.add(fields[2], kind='value', valueType=fields[1])
        else:
            raise ValueError('Error, file closed before the end of the header')

    def parseAsciiContent(self, f):
        self.allocate()
        for el in self:
            for pos in range(el.size):
                line = f.readline().decode('ascii').split()
                for prop in el:
                    if prop.kind == 'list':
                        prop.readAsciiList(pos, line)
                    else:
                        prop.readAsciiValue(pos, line)

    def parseBinaryContent(self, f):
        if self.format == 'binary_little_endian':
            byteorder = '<'
        else:
            byteorder = '>'
        self.allocate(byteorder)
        for el in self:
            for pos in range(el.size):
                for prop in el:
                    if prop.kind == 'list':
                        prop.readBinaryList(pos, f)
                    else:
                        prop.readBinaryValue(pos, f)
