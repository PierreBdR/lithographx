/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef IMAGEINTERFACE_H
#define IMAGEINTERFACE_H

#include "ui_ImageInterface.h"

class ImageInterface : public QDialog, public Ui::ImageInterface {
  Q_OBJECT

public:
  ImageInterface(QWidget* parent)
    : QDialog(parent)
  {
    setupUi(this);
  }
  virtual ~ImageInterface() {
  }

public slots:
  void resetValues()
  {
    this->imgWidth->setValue(parentWidget()->width());
    this->imgHeight->setValue(parentWidget()->height());
    this->imgQuality->setValue(95);
    this->oversampling->setValue(1.0);
  }
};

#endif // IMAGEINTERFACE_H
