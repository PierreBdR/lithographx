/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef CURVATURE_H
#define CURVATURE_H

#include <LGXConfig.hpp>

#include <Geometry.hpp>
#include <Information.hpp>
#include <Misc.hpp>

#include <CImg.h>
#include <string>
#include <vector>

#define CIMat cimg_library::CImg<float>
#define CIMatL cimg_library::CImgList<float>

namespace lgx {
namespace util {

#ifndef isnan
using std::isnan;
#endif

enum CurvatureType {
  CT_NONE = 0,
  CT_GAUSSIAN,
  CT_MAX,
  CT_MIN,
  CT_SUMSQUARE,
  CT_AVERAGE,
  CT_SIGNED_AVERAGE,
  CT_ROOT_SUMSQUARE,
  CT_ANISOTROPY   // This needs to stay the last entry of the enum!
};

LGX_EXPORT CurvatureType readCurvatureType(QString type, bool* ok = 0);
LGX_EXPORT QString toString(CurvatureType type);

class LGX_EXPORT CurvatureMeasure {
public:
  virtual ~CurvatureMeasure() {
  }
  virtual float measure(float ev1, float ev2) const = 0;
  virtual QString unit() const = 0;
  virtual CurvatureType get_type() const = 0;
  virtual CurvatureMeasure* copy() const = 0;
};

class LGX_EXPORT GaussianCurvature : public CurvatureMeasure {
public:
  float measure(float ev1, float ev2) const {
    return ev1 * ev2;
  }
  CurvatureType get_type() const {
    return CT_GAUSSIAN;
  }
  virtual CurvatureMeasure* copy() const {
    return new GaussianCurvature();
  }
  virtual QString unit() const {
    return UM_2;
  }
};

class LGX_EXPORT SumSquareCurvature : public CurvatureMeasure {
public:
  float measure(float ev1, float ev2) const {
    return (ev1 * ev1) + (ev2 * ev2);
  }
  CurvatureType get_type() const {
    return CT_SUMSQUARE;
  }
  virtual CurvatureMeasure* copy() const {
    return new SumSquareCurvature();
  }
  virtual QString unit() const {
    return UM_2;
  }
};

class LGX_EXPORT RootSumSquareCurvature : public CurvatureMeasure {
public:
  float measure(float ev1, float ev2) const {
    return sqrt((ev1 * ev1) + (ev2 * ev2));
  }
  CurvatureType get_type() const {
    return CT_ROOT_SUMSQUARE;
  }
  virtual CurvatureMeasure* copy() const {
    return new RootSumSquareCurvature();
  }
  virtual QString unit() const {
    return UM_1;
  }
};

class LGX_EXPORT AverageCurvature : public CurvatureMeasure {
public:
  float measure(float ev1, float ev2) const {
    return (ev1 + ev2) / 2;
  }
  CurvatureType get_type() const {
    return CT_AVERAGE;
  }
  virtual CurvatureMeasure* copy() const {
    return new AverageCurvature();
  }
  virtual QString unit() const {
    return UM_1;
  }
};

class LGX_EXPORT MinimalCurvature : public CurvatureMeasure {
public:
  float measure(float, float ev2) const {
    return ev2;
  }
  CurvatureType get_type() const {
    return CT_MIN;
  }
  virtual CurvatureMeasure* copy() const {
    return new MinimalCurvature();
  }
  virtual QString unit() const {
    return UM_1;
  }
};

class LGX_EXPORT MaximalCurvature : public CurvatureMeasure {
public:
  float measure(float ev1, float) const {
    return ev1;
  }
  CurvatureType get_type() const {
    return CT_MAX;
  }
  virtual CurvatureMeasure* copy() const {
    return new MaximalCurvature();
  }
  virtual QString unit() const {
    return UM_1;
  }
};

class LGX_EXPORT AnisotropyCurvature : public CurvatureMeasure {
public:
  float measure(float ev1, float ev2) const {
    return 1 - ev2 / ev1;
  }
  CurvatureType get_type() const {
    return CT_ANISOTROPY;
  }
  virtual CurvatureMeasure* copy() const {
    return new AnisotropyCurvature();
  }
  virtual QString unit() const {
    return "1";
  }
};

class LGX_EXPORT SignedAverageAbsCurvature : public CurvatureMeasure {
public:
  float measure(float ev1, float ev2) const
  {
    return (ev1 * ev2 < 0 ? -1.0f : 1.0f) * (fabs(ev1) + fabs(ev2)) / 2.0f;
  }
  CurvatureType get_type() const {
    return CT_SIGNED_AVERAGE;
  }
  virtual CurvatureMeasure* copy() const {
    return new SignedAverageAbsCurvature();
  }
  virtual QString unit() const {
    return UM_1;
  }
};

LGX_EXPORT CurvatureMeasure* curvatureMeasure(CurvatureType type);

class LGX_EXPORT Curvature {
public:
  // convenience constructor
  explicit Curvature(CurvatureType s = CT_GAUSSIAN)
    : W(2, 2)
    , ev1(0)
    , ev2(0)
  {
    cM = curvatureMeasure(s);
  }

  Curvature(const Curvature& copy)
    : W(copy.W)
    , b1(copy.b1)
    , b2(copy.b2)
    , b3(copy.b3)
    , ed1(copy.ed1)
    , ed2(copy.ed2)
    , ev1(copy.ev1)
    , ev2(copy.ev2)
    , cM(0)
  {
    if(copy.cM)
      cM = copy.cM->copy();
  }

  ~Curvature() {
    delete cM;
  }

  // update the curvature object, first entry of pos and norm must be the position, surface normal of the point of
  // interest
  void update(const std::vector<Point3f>& pos, const std::vector<Point3f>& nrml)
  {
    _updated = false;

    try {
      if(nrml.size() > 0 && nrml.size() == pos.size()) {

        // Calculate local Coordinate system
        update_local_Basis(nrml[0]);

        // Set up Least Square Problem
        int cnt = pos.size();
        CIMat X = CIMat(7, 3 * (cnt - 1));
        CIMat b = CIMat(1, 3 * (cnt - 1));
        Point3f vpos = pos[0];
        int id = 0;

        for(int i = 1; i < cnt; i++) {
          // Transform into local coordinates
          Point3f vw = pos[i] - vpos;
          float w1 = vw * b1;
          float w2 = vw * b2;
          float w3 = vw * b3;

          // Transform into local coordinates
          float n1, n2, n3;
          n1 = nrml[i] * b1;
          n2 = nrml[i] * b2;
          n3 = nrml[i] * b3;

          X(0, id) = 0.5 * w1 * w1;
          X(1, id) = w1 * w2;
          X(2, id) = 0.5 * w2 * w2;
          X(3, id) = w1 * w1 * w1;
          X(4, id) = w1 * w1 * w2;
          X(5, id) = w1 * w2 * w2;
          X(6, id) = w2 * w2 * w2;
          b(id) = w3;
          id++;

          X(0, id) = w1;
          X(1, id) = w2;
          X(2, id) = 0;
          X(3, id) = 3 * w1 * w1;
          X(4, id) = 2 * w1 * w2;
          X(5, id) = w2 * w2;
          X(6, id) = 0;
          b(id) = -n1 / n3;
          id++;

          X(0, id) = 0;
          X(1, id) = w1;
          X(2, id) = w2;
          X(3, id) = 0;
          X(4, id) = w1 * w1;
          X(5, id) = 2 * w1 * w2;
          X(6, id) = 3 * w2 * w2;
          b(id) = -n2 / n3;
          id++;
        }
        CIMat Xt = X.get_transpose();

        CIMat Q = Xt * X;

        b = Xt * b;

        float det = Q.det();
        if(!isnan(det) && !(det == 0)) {
          // solve least-squares problem
          b.solve(Q);

          // update Weingarten-matrix
          W(0, 0) = b[0];
          W(1, 0) = W(0, 1) = b[1];
          W(1, 1) = b[2];

          // calculate eigenvalues and eigenvectors
          CIMatL bL = W.get_symmetric_eigen();
          ev1 = bL(0)(0, 0);
          ev2 = bL(0)(1, 0);
          ed1 = -bL(1)(0, 0) * b1 - bL(1)(0, 1) * b2;
          ed2 = -bL(1)(1, 0) * b1 - bL(1)(1, 1) * b2;
          if(fabs(ev1) < fabs(ev2)) {
            using std::swap;
            swap(ev1, ev2);
            swap(ed1, ed2);
          }
        }
      }
    }
    catch(...) {
      Information::out << "Error calculating curvature" << endl;
    }

    _updated = true;
  }

  // get principal directions of curvature
  void get_eigenVectors(Point3f& evec1, Point3f& evec2)
  {
    evec1 = ed1;
    evec2 = ed2;
    return;
  }

  // get principal directions of curvature
  void get_eigenValues(float& eval1, float& eval2)
  {
    eval1 = ev1;
    eval2 = ev2;
    return;
  }

  // calculate a single-valued measure of curvature (for example "gaussian")
  float get_curvature() {
    return cM->measure(ev1, ev2);
  }

  // get the name of the curvature measure (for example "gaussian")
  CurvatureType get_type() {
    return cM->get_type();
  }

  // set the curvature measure
  void set_Measure(CurvatureMeasure* _cM)
  {
    if(cM)
      delete cM;
    cM = _cM;
    return;
  }

  bool updated() const {
    return _updated;
  }
  operator bool() const { return _updated; }

private:
  // calculate local basis with b1,b2, in-plane and b3 out-of-plane basis vectors
  void update_local_Basis(Point3f nrml)
  {
    nrml = nrml / nrml.norm();
    do {
      b1 = Point3f(cimg_library::cimg::rand(), cimg_library::cimg::rand(), cimg_library::cimg::rand());
      b1 = b1 / b1.norm();
    } while(fabs(b1 * nrml) == 1);
    b1 = b1 - (b1 * nrml) * nrml;
    b1 = b1 / b1.norm();
    b2 = b1.cross(nrml);
    b3 = nrml;

    return;
  }

  // Weingarten Matrix
  CIMat W;

  // Local Basis Vectors
  Point3f b1;
  Point3f b2;
  Point3f b3;

  // Principal directions of curvature
  Point3f ed1;
  Point3f ed2;

  // Principal values of curvature
  float ev1;
  float ev2;

  // Set to true if the curvature has been computed
  bool _updated;

  CurvatureMeasure* cM;
};
} // namespace util
} // namespace lgx
#endif
