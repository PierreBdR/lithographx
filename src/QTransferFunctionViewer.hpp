/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef QTRANSFERFUNCTIONVIEWER_HPP
#define QTRANSFERFUNCTIONVIEWER_HPP

#include <LGXConfig.hpp>

#include <TransferFunction.hpp>

#include <QColor>
#include <QLinearGradient>
#include <QPainterPath>
#include <QWidget>
#include <vector>

class QMouseEvent;
class QPixmap;
class QAction;
class QPaintEvent;

namespace lgx {
namespace gui {

class LGX_EXPORT QTransferFunctionViewer : public QWidget {
  Q_OBJECT
public:
  typedef TransferFunction::Colorf Colorf;
  enum BackgroundType { BG_CHECKS, BG_WHITE, BG_BLACK };

  QTransferFunctionViewer(QWidget* parent = 0, Qt::WindowFlags f = 0);
  virtual ~QTransferFunctionViewer() {
  }

  size_t nbValues() const;

  const TransferFunction& transferFunction() const {
    return transfer_fct;
  }

  double adjustRange() const { return _adjustRange; }

public slots:
  void changeNbValues(int n);
  void reverseFunction();
  void addOpacityScale();
  void makeOpaque();
  void changeTransferFunction(const TransferFunction& fct);
  void setupGradient();
  void setHistogram(const std::vector<uint64_t>& h);
  void setBounds(double min, double max);
  void setStickers(const std::vector<double>& s);
  void setMarkerSize(int s);
  void setCheckSize(int s);
  void setBackgroundType(BackgroundType type);
  void setInterpolation(TransferFunction::Interpolation i);
  void setSelectionColor(QColor col);
  void editMarkers();
  void autoAdjust();
  void autoAdjust(double p);
  void setRange(double low, double high);
  void setAutoAdjustRange(double p);

signals:
  void changedTransferFunction(const TransferFunction& fct);
  void hoverPos(int value);

protected:
  void paintEvent(QPaintEvent* event) override;
  void mouseDoubleClickEvent(QMouseEvent* e) override;
  void mousePressEvent(QMouseEvent* e) override;
  void mouseReleaseEvent(QMouseEvent* e) override;
  void mouseMoveEvent(QMouseEvent* e) override;

  void resetMouseInteraction();

  void prepareHistogram();

  void createBackground();
  QPixmap createChecks();

  TransferFunction transfer_fct;
  std::vector<uint64_t> histogram;
  std::vector<double> hist_values;
  double minValue, maxValue;
  double minRange, maxRange;   // Values of the left and right of the range
  QPainterPath hist_shape;
  bool use_histogram;
  std::vector<double> stickers;
  bool sticking;
  int marker_size;
  bool select_pos;
  double current_pos, hover_pos;
  int bg_size;
  double bg_bright;
  double bg_dark;
  QColor activ_pos_color;
  Colorf saved_color;
  double saved_pos;
  QAction *reverse_act, *edit_markers, *opacity_scale_act, *make_opaque_act;
  QLinearGradient gradient;
  BackgroundType bg_type;
  size_t _nb_values;
  double _adjustRange;
};
} // namespace gui
} // namespace lgx
#endif // QTRANSFERFUNCTIONVIEWER_HPP
