/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef PYTHONMACRO_HPP
#define PYTHONMACRO_HPP

#include <python/LGXPython.hpp>
#include <python/lgx_module.hpp>

#include <QMetaType>

namespace lgx {
namespace process {

struct LGXPYTHON_EXPORT PythonMacroDefinition : public BaseProcessDefinition {
  PythonMacroDefinition() : BaseProcessDefinition() {}
  PythonMacroDefinition(const PythonMacroDefinition&) = default;

  PythonMacroDefinition& operator=(const PythonMacroDefinition&) = default;

#ifndef _MSC_VER
  PythonMacroDefinition& operator=(PythonMacroDefinition&&) = default;
  PythonMacroDefinition(PythonMacroDefinition&&) = default;
#else
  PythonMacroDefinition(PythonMacroDefinition&& other)
    : BaseProcessDefinition(std::move(other))
    , module(std::move(other.module))
    , type(std::move(other.type))
  { }
  PythonMacroDefinition& operator=(PythonMacroDefinition&& other)
  {
    BaseProcessDefinition::operator=(std::move(other));
    module = std::move(other.module);
    type = std::move(other.type);
    return *this;
  }
#endif


  QString module;  ///< Name of the python module
  QString type;    ///< Type of the module
  enum ParameterHandling {
    StringOnlyParameters, ///< The process should always receive strings
    SmartParameters,      ///< The process should receive string, int or double, depending on the content of the QVariant
    QVariantParameters    ///< The process should receive un-modified QVariant
  } parmsHandling; ///< How to manage arguments
};

template <typename ProcessType>
class LGXPYTHON_EXPORT PythonMacro : public PythonProcess<ProcessType> {
public:
  PythonMacro(const PythonMacroDefinition& def, const ProcessType& process);

  bool initialize(ParmList& parms, QWidget* parent ) override;
  bool operator()(const ParmList& parms) override;

  QString name() const override { return _def.name; }
  QString description() const override { return _def.description; }
  QStringList parmNames() const override { return _def.parmNames; }
  QStringList parmDescs() const override { return _def.parmDescs; }
  ParmList parmDefaults() const override { return _def.parms; }
  ParmChoiceMap parmChoice() const override { return _def.parmChoice; }
  QIcon icon() const override { return _def.icon; }
  QString folder() const override { return _def.folder; }

protected:
  python::SafePy<PyTupleObject> processParms(const ParmList& parms, PyObject *moduleDictionary);
  PythonMacroDefinition _def;
};

template <typename ProcessType>
class LGXPYTHON_EXPORT PythonMacroFactory : public ProcessFactory<ProcessType> {
public:
  PythonMacroFactory(const PythonMacroDefinition& def);
  PythonMacroFactory(const PythonMacroFactory& copy);
  ~PythonMacroFactory();

  std::unique_ptr<ProcessType> operator()(const ProcessType& process) const override;

protected:
  PythonMacroDefinition _def;
};

LGXPYTHON_EXPORT void reloadPythonMacros();

}
}

#endif // PYTHONMACRO_HPP

