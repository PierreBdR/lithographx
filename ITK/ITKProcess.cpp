/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "ITKProcess.hpp"

#include <Libraries.hpp>
#include <Stack.hpp>
#include <Store.hpp>

#include <itkConfigure.h>
#include <itkImportImageFilter.h>
#include <itkSmoothingRecursiveGaussianImageFilter.h>
#include <itkImageSliceConstIteratorWithIndex.h>
#include <itkImageRegionConstIterator.h>
#include <itkImageFileWriter.h>
#include <itkAdaptImageFilter.h>

#include <Information.hpp>

namespace lgx {
namespace process {

namespace {

void size(const Stack* stack, itk::Size<2u>& result) {
  auto s2d = stack->size2D();
  result[0] = s2d.x();
  result[1] = s2d.y();
}

void size(const Stack* stack, itk::Size<3u>& result) {
  auto s3d = stack->size();
  result[0] = s3d.x();
  result[1] = s3d.y();
  result[2] = s3d.z();
}

template <size_t dim>
itk::Size<dim> size(const Stack* stack)
{
  itk::Size<dim> result;
  size(stack, result);
  return result;
}

template <typename T>
void origin(const Stack* stack, itk::Point<T, 2u>& result) {
  auto s2d = stack->to2D(stack->origin());
  result[0] = s2d.x();
  result[1] = s2d.y();
}

template <typename T>
void origin(const Stack* stack, itk::Point<T, 3u>& result) {
  auto s3d = stack->origin();
  result[0] = s3d.x();
  result[1] = s3d.y();
  result[2] = s3d.z();
}

template <size_t dim>
itk::Point<double, dim> origin(const Stack* stack)
{
  itk::Point<double, dim> result;
  origin(stack, result);
  return result;
}

template <typename T>
void step(const Stack* stack, itk::Vector<T, 2u>& result) {
  auto s2d = stack->step2D();
  result[0] = s2d.x();
  result[1] = s2d.y();
}

template <typename T>
void step(const Stack* stack, itk::Vector<T, 3u>& result) {
  auto s3d = stack->step();
  result[0] = s3d.x();
  result[1] = s3d.y();
  result[2] = s3d.z();
}

template <size_t dim>
itk::Vector<double, dim> step(const Stack* stack)
{
  itk::Vector<double, dim> result;
  step(stack, result);
  return result;
}

void setSize(Stack* stack, const itk::Size<2u>& s) {
  Point2u ss(s[0], s[1]);
  stack->setSize(stack->to3D(ss, 1u));
}

void setSize(Stack* stack, const itk::Size<3u>& s) {
  Point3u ss(s[0], s[1], s[2]);
  stack->setSize(ss);
}

template <typename T>
void setStep(Stack* stack, const itk::Vector<T, 2u>& s) {
  Point2f ss(s[0], s[1]);
  stack->setStep(stack->to3D(ss, float(ss[1])));
}

template <typename T>
void setStep(Stack* stack, const itk::Vector<T, 3u>& s) {
  Point3f ss(s[0], s[1], s[2]);
  stack->setStep(ss);
}

template <typename T>
void setOrigin(Stack* stack, const itk::Point<T, 2u>& s) {
  Point2f ss(s[0], s[1]);
  stack->setOrigin(stack->to3D(ss));
}

template <typename T>
void setOrigin(Stack* stack, const itk::Point<T, 3u>& s) {
  Point3f ss(s[0], s[1], s[2]);
  stack->setOrigin(ss);
}

}

template <typename T, size_t dim>
ImageConverter<T, dim>::ImageConverter()
{
  importer = ImportFilterType::New();
  adapter = AdapterType::New();

  adapter->SetInput(importer->GetOutput());
}

template <typename T, size_t dim>
ImageConverter<T, dim>::~ImageConverter()
{
  importer->Delete();
  adapter->Delete();
}

template <typename T, size_t dim>
void ImageConverter<T, dim>::SetStore(const Store* s)
{
  store = s;

  const Stack* stack = store->stack();

  typename OImageType::IndexType start;
  start.Fill(0);

  typename OImageType::RegionType region;
  region.SetSize(size<dim>(stack));
  region.SetIndex(start);

  Information::out << "Size of the input image: " << region.GetSize(0) << "x" << region.GetSize(1) << "x"
                   << region.GetSize(2) << endl;

  importer->SetRegion(region);
  importer->SetSpacing(step<dim>(stack));
  importer->SetOrigin(origin<dim>(stack));

  typedef typename OImageType::PixelType PixelType;

  PixelType* pixelData = const_cast<PixelType*>(store->data().data());

  unsigned long totalNumberOfPixels = store->size();

  static const bool importFilterWillDeleteTheInputBuffer = false;

  importer->SetImportPointer(pixelData, totalNumberOfPixels, importFilterWillDeleteTheInputBuffer);
}

template <typename T, size_t dim>
void ImageConverter<T, dim>::Update() {
  adapter->Update();
}

template <typename T, size_t dim>
typename ImageConverter<T,dim>::Pointer ImageConverter<T, dim>::New() {
  return Pointer(new ImageConverter());
}

template <typename T, size_t dim>
itk::SmartPointer<itk::LightObject> ImageConverter<T, dim>::CreateAnother() const
{
  return itk::SmartPointer<itk::LightObject>(new ImageConverter());
}

template <typename T, size_t dim>
const char* ImageConverter<T, dim>::GetNameOfClass() const {
  return "ImageConverter";
}

template <typename T, size_t dim>
typename itk::Image<T, dim>::Pointer ImageConverter<T, dim>::GetOutput() {
  return adapter->GetOutput();
}

template <typename T, size_t dim>
bool ImageConverter<T, dim>::TransferImage(Stack* stack, Store* store, typename ImageType::ConstPointer image)
{
  typename ImageType::RegionType region = image->GetBufferedRegion();

  auto img_size = region.GetSize();
  auto img_spacing = image->GetSpacing();
  auto origin = image->GetOrigin();
  setStep(stack, img_spacing);
  setSize(stack, img_size);
  setOrigin(stack, origin);

  return ImageConverter<T,dim>::TransferImage((const Stack*)stack, store, image);
}

template <typename T, size_t dim>
bool ImageConverter<T,dim>::TransferImage(const Stack*, Store* store, typename ImageType::ConstPointer image)
{
  typedef itk::AdaptImageFilter<ImageType, OImageType, ToUnsignedAccessor<T>> AdapterType;

  typename AdapterType::Pointer adapter = AdapterType::New();

  adapter->SetInput(image);

  adapter->Update();

  typename OImageType::ConstPointer result = adapter->GetOutput();
  typename OImageType::RegionType region = result->GetBufferedRegion();
  typename ImageType::RegionType::SizeType img_size = region.GetSize();

  uint ss = img_size[0] * img_size[1];
  if(dim == 3)
    ss *= img_size[2];
  if(ss != store->size())
    return false;

  ushort* out = store->data().data();

  typedef itk::ImageRegionConstIterator<OImageType> IteratorType;

  IteratorType it(result, region);

  it.GoToBegin();
  while(!it.IsAtEnd()) {
    *out++ = it.Get();
    ++it;
  }

  return true;
}

template <size_t dim>
ImageConverter<ushort, dim>::ImageConverter()
{
  importer = ImportFilterType::New();
}

template <size_t dim>
ImageConverter<ushort, dim>::~ImageConverter()
{
  importer->Delete();
}

template <size_t dim>
void ImageConverter<ushort, dim>::SetStore(const Store* s)
{
  store = s;

  const Stack* stack = store->stack();

  typename OImageType::IndexType start;
  start.Fill(0);

  typename OImageType::RegionType region;
  region.SetSize(size<dim>(stack));
  region.SetIndex(start);

  Information::out << "Size of the input image: " << region.GetSize(0) << "x" << region.GetSize(1) << "x"
                   << region.GetSize(2) << endl;

  importer->SetRegion(region);
  importer->SetSpacing(step<dim>(stack));
  importer->SetOrigin(origin<dim>(stack));

  typedef typename OImageType::PixelType PixelType;

  PixelType* pixelData = const_cast<PixelType*>(store->data().data());

  unsigned long totalNumberOfPixels = store->size();

  static const bool importFilterWillDeleteTheInputBuffer = false;

  importer->SetImportPointer(pixelData, totalNumberOfPixels, importFilterWillDeleteTheInputBuffer);
}

template <size_t dim>
void ImageConverter<ushort, dim>::Update() {
  importer->Update();
}

template <size_t dim>
typename ImageConverter<ushort,dim>::Pointer ImageConverter<ushort, dim>::New() {
  return Pointer(new ImageConverter());
}

template <size_t dim>
itk::SmartPointer<itk::LightObject> ImageConverter<ushort, dim>::CreateAnother() const
{
  return itk::SmartPointer<itk::LightObject>(new ImageConverter());
}

template <size_t dim>
const char* ImageConverter<ushort, dim>::GetNameOfClass() const {
  return "ImageConverter";
}

template <size_t dim>
typename itk::Image<ushort, dim>::Pointer ImageConverter<ushort, dim>::GetOutput() {
  return importer->GetOutput();
}

template <size_t dim>
bool ImageConverter<ushort, dim>::TransferImage(Stack* stack, Store* store, typename ImageType::ConstPointer image)
{
  typename ImageType::RegionType region = image->GetBufferedRegion();

  auto img_size = region.GetSize();
  auto img_spacing = image->GetSpacing();
  auto origin = image->GetOrigin();
  setStep(stack, img_spacing);
  setSize(stack, img_size);
  setOrigin(stack, origin);

  return ImageConverter<ushort,dim>::TransferImage((const Stack*)stack, store, image);
}

template <size_t dim>
bool ImageConverter<ushort,dim>::TransferImage(const Stack*, Store* store, typename ImageType::ConstPointer image)
{
  typename OImageType::RegionType region = image->GetBufferedRegion();
  typename ImageType::RegionType::SizeType img_size = region.GetSize();

  uint ss = img_size[0] * img_size[1];
  if(dim == 3)
    ss *= img_size[2];
  if(ss != store->size())
    return false;

  ushort* out = store->data().data();

  typedef itk::ImageRegionConstIterator<OImageType> IteratorType;

  IteratorType it(image, region);

  it.GoToBegin();
  while(!it.IsAtEnd()) {
    *out++ = it.Get();
    ++it;
  }

  return true;
}

template class ImageConverter<float, 2>;
template class ImageConverter<float, 3>;
template class ImageConverter<signed short, 2>;
template class ImageConverter<signed short, 3>;
template class ImageConverter<unsigned short, 2>;
template class ImageConverter<unsigned short, 3>;

DECLARE_LIBRARY("ITK", ITK_VERSION_STRING,
                "ITK is an open-source, cross-platform system that provides"
                "developers with an extensive suite of software tools for image"
                "analysis.  Developed through extreme programming methodologies,"
                "ITK employs leading-edge algorithms for registering and"
                "segmenting multidimensional data. The goals for ITK include:"
                "<ul>"
                "<li>Supporting the Visible Human Project.</li>"
                "<li>Establishing a foundation for future research.</li>"
                "<li>Creating a repository of fundamental algorithms.</li>"
                "<li>Developing a platform for advanced product development.</li>"
                "<li>Support commercial application of the technology.</li>"
                "<li>Create conventions for future work.</li>"
                "<li>Grow a self-sustaining community of software users and developers.</li>"
                "</ul>", "http://www.itk.org", ":/images/ITKLogo.png");

} // namespace process
} // namespace lgx
