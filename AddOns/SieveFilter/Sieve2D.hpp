#ifndef SIEVE2D_HPP
#define SIEVE2D_HPP

#include "Sieve.hpp"

#include <Stack.hpp>

namespace lgx {
namespace process {
namespace sieve {

struct Sieve2D : public Sieve {
  /// Note: implementation based on the 1998 MATLAB MEX file of J. Andrew Bangham

  /**
   * Directions in 2D:
   *
   *    4 2 6
   *     \|/
   *   0--*--1
   *     /|\
   *    5 3 7
   *
   * Top-left corner is (0,0)
   */
  enum DIRECTION : uint8_t
  {
    LEFT         = 1,
    RIGHT        = 2,
    TOP          = 4,
    BOTTOM       = 8,
    TOP_LEFT     = 16,
    BOTTOM_LEFT  = 32,
    TOP_RIGHT    = 64,
    BOTTOM_RIGHT = 128
  };

  // Input data
  NUM_DIRS _ndir;             ///< Number of directions for the neighborhood

  // Internal structures
  int32_t _N = 0;                                            ///< Number of lines in the image
  int32_t _M = 0;                                            ///< Number of columns in the image
  std::array<Point2i, 8> _borders;                           ///< Border of the image for various positions

  Sieve2D(const HVecUS& data,
          const Point2u& size,
          FILTER_TYPE first_filter,
          NUM_DIRS ndir,
          SieveProgress progress);

  // For debugging only
  void saveFiles(const QString& prefix = QString());

  /**
   * Write the resulting image to the output
   */
  bool writeTo(HVecUS& output) const;

  /**
   * Returns true if an error occured
   */
  bool error() const { return not _errorString.isEmpty(); }

  /**
   * Get the text of the last error
   */
  const QString& errorString() const { return _errorString; }

private:

  int32_t offX(intptr_t off) const
  {
    return off % _M;
  }

  int32_t offY(intptr_t off) const
  {
    return off / _M;
  }

  Point2i offXY(intptr_t off) const
  {
    return Point2i{offX(off), offY(off)};
  }

  /**
   * Initial preparation of the mesh
   */
  bool prepareMesh(FILTER_TYPE first_filter, SieveProgress& progress);

  /**
   * Find the connected sets in the initial, prepared, mesh
   */
  bool findConnectedSets(SieveProgress& progress);

  /**
   * Process the graph to find adjacency and extrema
   *
   * The root of each zone is given its area as new root
   */
  bool findAdjacencyAndExtrema(SieveProgress& progress);

};

} // namespace sieve
} // namespace process
} // namespace lgx

#endif // SIEVE2D_HPP

