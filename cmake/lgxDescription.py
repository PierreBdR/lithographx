from __future__ import print_function

import argparse
import sys
from PyQt5.QtCore import QSettings

parser = argparse.ArgumentParser()
parser.add_argument("--field", action="append", dest="fields", help="Field to read. Should be 'Name', 'Version', 'Description', 'Dependencies' or 'OS'.")
parser.add_argument("inifile", metavar="DESCRIPTION", help="Path to a description.ini file.")
args = parser.parse_args()

settings = QSettings(args.inifile, QSettings.IniFormat)
settings.beginGroup("Package")
for field in args.fields:
    print(settings.value(field, ""))
settings.endGroup()
