.. LithoGraphX documentation master file, created by
   sphinx-quickstart on Wed May  6 11:41:40 2015.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to LithoGraphX's documentation!
=======================================

.. image:: images/LithoGraphX-intro.png
    :alt: Screen shot of LithoGraphX
    :align: center
    :width: 75%


`LithoGraphX <http://www.lithographx.com>`_ is a software to visualize, process
and analyse 3D images and meshes. This documentation is organized into
a reference manual describing the user interface in details and a collection of
tutorials describing how to perform usual tasks.

The main website of LithoGraphX is: http://www.lithographx.com

LithoGraphX is a fork of the `MorphoGraphX <http://www.morphographx.org>`_
project.

Getting help:

  #. Read this documentation
  #. If you have problems, want help or have suggestions, use the
       `LithoGraphX bitbucket issue tracker
       <https://bitbucket.org/PierreBdR/lithographx/issues?status=new&status=open>`_.
  #. You can also contact me by email at pierre@barbierdereuille.net


.. hint::

    If you have never used LithoGraphX, I recommend you start by reading the
    definitions and the description of the main interface. Then, you should probably
    read tutorials close to what you want to do.

.. toctree::
    :numbered:
    :maxdepth: 2

    installation
    intro
    data_collection
    tutorials
    python
    FAQ
    extensions
    changelog
    Copyright

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

