#include "Sieve3D.hpp"

#include <Algorithms.hpp>

#include <QFile>
#include <QTextStream>
#include <QString>

#include <unordered_set>

namespace lgx {
namespace process {
namespace sieve {

const std::array<Point3i, 26> Sieve3D::_shifts =
{ Point3i(-1,0,0), Point3i(1,0,0), Point3i(0,-1,0), Point3i(0,1,0), Point3i(0,0,-1), Point3i(0,0,1),
  Point3i(-1,-1,-1), Point3i(-1,0,-1), Point3i(-1,1,-1),
  Point3i(-1,-1,0), Point3i(-1,1,0),
  Point3i(-1,-1,1), Point3i(-1,0,1), Point3i(-1,1,1),
  Point3i(1,-1,-1), Point3i(1,0,-1), Point3i(1,1,-1),
  Point3i(1,-1,0), Point3i(1,1,0),
  Point3i(1,-1,1), Point3i(1,0,1), Point3i(1,1,1),
  Point3i(0,-1,-1), Point3i(0,-1,1),
  Point3i(0,1,-1), Point3i(0,1,1) };


Sieve3D::Sieve3D(const HVecUS& data,
                 const Point3u& size,
                 FILTER_TYPE first_filter,
                 NUM_DIRS ndir,
                 SieveProgress progress)
  : Sieve(data)
  , _ndir(ndir)
  , _M(size[0])
  , _N(size[1])
  , _P(size[2])
{
  if(error()) return;

  if(_N < 0 or _M < 0 or _P < 0) {
    _errorString = "Error, the image is too large to be processed.";
    return;
  }
  if(data.size() == 0) {
    _errorString = "Error, the image is empty.";
    return;
  }
  if(ndir != NDIR_6 and ndir != NDIR_26) {
    _errorString = "Error, in 3D there must be either 6 or 26 directions";
    return;
  }

  if(not progress.advance(0)) {
    _errorString = "Progress abort";
    return;
  }

  // Generate offsets and borders arrays
  _offsets.resize(_ndir, 0);
  _borders.fill(Point3i());
  std::array<int,3> bx = {0, -1, _M-1};
  std::array<int,3> by = {0, -1, _N-1};
  std::array<int,3> bz = {0, -1, _P-1};
  for(uint8_t dir = 0 ; dir < _ndir ; ++dir) {
    Point3i s = _shifts[dir];
    _offsets[dir] = s.x() + _M*s.y() + _N*_M*s.z();
    _borders[dir] = Point3i(bx[s.x()+1], by[s.y()+1], bz[s.z()+1]);
  }

  // Allocate memory for the pel graph
  if(not prepareMesh(first_filter, progress)) {
    _errorString = "Failed to prepare the mesh.";
    return;
  }

  if(not findConnectedSets(progress)) {
    _errorString = "Failed to find connected sets.";
    return;
  }
  if(not findAdjacencyAndExtrema(progress)) {
    _errorString = "Failed to find adjacency and extrema.";
    return;
  }
}

bool Sieve3D::writeTo(HVecUS& output) const
{
  if(output.size() != _M*_N*_P) {
    _errorString = "Error, output doesn't have the same size as the current image";
    return false;
  }

#pragma omp parallel for
  for(intptr_t z = 0 ; z < _P ; ++z) {
    intptr_t pos = z*_M*_N;
    for(intptr_t y = 0 ; y < _N ; ++y)
      for(intptr_t x = 0 ; x < _M ; ++x, ++pos) {
        auto root = _G[pos].root_pel;
        if(root < 0) root = pos;
        output[pos] = _image[root];
      }
  }

  return true;
}

bool Sieve3D::prepareMesh(FILTER_TYPE first_filter, SieveProgress& progress)
{
  auto progress_step = _G.size() / 33;
  if(progress_step < 1)
    progress_step = 1;
  double progress_ratio = 33. / _G.size();

  intptr_t pos = 0;
  for(intptr_t z = 0 ; z < _P ; ++z) {
    for(intptr_t y = 0 ; y < _N ; ++y) {
      for(intptr_t x = 0 ; x < _M ; ++x, ++pos) {
        if(pos % progress_step == 0)
          if(not progress.advance(pos * progress_ratio))
            return false;

        auto val = _image[pos];
        auto val_min = val, val_max = val;
        ushort lt_val = 0, gt_val = std::numeric_limits<ushort>::max();
        for(uint32_t dir = 0 ; dir < _ndir ; ++dir) {
          if(_borders[dir].x() != x and _borders[dir].y() != y and _borders[dir].z() != z) {
            auto nval = _image[pos + _offsets[dir]];
            if(nval < val_min) val_min = nval;
            if(nval > val_max) val_max = nval;
            if(nval <= val and nval > lt_val) lt_val = nval;
            if(nval >= val and nval < gt_val) gt_val = nval;
          }
        }
        if(first_filter == FT_OPENING) {
          if(val_max == val)
            val = lt_val;
        } else if(first_filter == FT_CLOSING) {
          if(val_min == val)
            val = gt_val;
        } else if(val_min == val)
          val = gt_val;
        else if(val_max == val)
          val = lt_val;
        _image[pos] = val;
      }
    }
  }
  return true;
}

bool Sieve3D::findConnectedSets(SieveProgress& progress)
{
  // In this function:
  //  - root_pel will point to the root, that may be the pel itself
  //  - next_adj is 1 if the pel has been processed, 0 otherwise
  //  - next_pel is set to the index of the next pel of same intensity in the neighborhood
  //  - adj_flags contains first the direction in which the search should be
  //    continued and, after processing, the direction where different value
  //    pels are found.

  Information::out << "Computing directions" << endl;
  std::array<uint32_t, 26> broad_dirs;
  broad_dirs.fill(0);

  for(int dir = 0 ; dir < _ndir ; ++dir) {
    uint32_t flags = 0;
    uint32_t bit = 1;
    for(int dir2 = 0 ; dir2 < _ndir ; ++dir2, bit <<= 1) {
      auto tot = fabs(_shifts[dir] + _shifts[dir2]);
        if(tot.x() + tot.y() + tot.z() > 1)
          flags |= bit;
    }
    broad_dirs[dir] = flags;
    //Information::out << "Broad dir for " << dir << ": " << QString::number(flags, 2).rightJustified(_ndir, '0') << endl;
  }

  std::vector<uint8_t> forward_dirs, other_dirs;
  for(int dir = 0 ; dir < _ndir ; ++dir) {
    auto shift = _shifts[dir];
    if(shift.z() < 0 or
       (shift.z() == 0 and (shift.y() < 0 or
                            (shift.y() == 0 and shift.x() < 0))))
      other_dirs.push_back(dir);
    else
      forward_dirs.push_back(dir);
  }
  Information::out << " ... done" << endl;

  /*
   *Information::out << "Positive dirs:";
   *for(auto d: forward_dirs)
   *  Information::out << " " << d;
   *Information::out << endl;
   */

  auto progress_step = _G.size() / 33;
  if(progress_step < 1)
    progress_step = 1;
  double progress_ratio = 33. / _G.size();

  intptr_t processed = 0;
  intptr_t pos = 0;
  for(int32_t z = 0 ; z < _P ; ++z) {
    for(int32_t y = 0 ; y < _N ; ++y) {
      for(int32_t x = 0 ; x < _M ; ++x, ++pos) {

        auto& cur = _G[pos];
        auto val = _image[pos];

        //Information::out << "Testing voxel " << x << " " << y << " " << z << " value " << val << endl;

        // Test whether the current position has already been processed
        if(cur.next_adj == 0) {
          cur.root_pel = pos;
          cur.next_adj = 1;
          uint32_t other_regions = 0;
          auto last_pel = pos;

          for(auto dir: forward_dirs) {
            auto& b = _borders[dir];
            if(x != b.x() and y != b.y() and z != b.z()) {
              auto npos = pos + _offsets[dir];
              if(val == _image[npos]) {
                //Information::out << "      initial add " << offXYZ(npos) << endl;
                _G[last_pel].next_pel = npos;
                last_pel = npos;
                _adj_flags[last_pel] = 1 << dir;
                _G[last_pel].next_adj = 1;
              } else
                other_regions |= 1 << dir;
            }
          }

          for(auto dir: other_dirs) {
            auto& b = _borders[dir];
            if(x != b.x() and y != b.y() and z != b.z() and _image[pos + _offsets[dir]] != val)
              other_regions |= 1 << dir;
          }

          // Store the direction to other regions
          _adj_flags[pos] = other_regions;

          // End the linked list of pels
          _G[last_pel].next_pel = -1;

          if(++processed % progress_step == 0)
            if(not progress.advance(33 + processed * progress_ratio)) return false;

          // Loop over all pels added to the region to expand it to the whole area of common value
          for(auto next_pel = cur.next_pel ; next_pel >= 0 ;
              next_pel = _G[next_pel].next_pel) {
            auto& node = _G[next_pel];
            auto val = _image[next_pel];

            // Set the root of the region
            node.root_pel = pos;

            // Get the previous direction
            auto last_dirn = _adj_flags[next_pel];
            auto x0 = offX(next_pel);
            auto y0 = offY(next_pel);
            auto z0 = offZ(next_pel);

            //Information::out << "   expand from " << x0 << " " << y0 << " " << z0 << " / " << last_dirn << endl;

            other_regions = 0;

            // Consider all possible directions from which we need
            // to expand, but do not try to come back on our tracks
            uint32_t bit = 1;
            for(uint8_t dir = 0 ; dir < _ndir ; ++dir, bit <<= 1) {
              auto b = _borders[dir];
              if((broad_dirs[dir] & last_dirn) and x0 != b.x() and y0 != b.y() and z0 != b.z()) {
                auto npos = next_pel + _offsets[dir];
                //Information::out << "      Testing pos " << offXYZ(npos) << endl;
                if(val == _image[npos]) {
                  if(_G[npos].next_adj == 0) {
                    //Information::out << "       adding!" << endl;
                    _G[last_pel].next_pel = npos;
                    last_pel = npos;
                    _adj_flags[last_pel] = bit;
                    _G[last_pel].next_adj = 1;
                  }
                } else
                  other_regions |= bit;
              }
            }

            // Store directions to other regions
            _adj_flags[next_pel] = other_regions;

            if(++processed % progress_step == 0)
              if(not progress.advance(33 + processed * progress_ratio)) return false;

            // End the linked list of pels
            _G[last_pel].next_pel = -1;
          }
        }
      }
    }
  }
  return true;
}

bool Sieve3D::findAdjacencyAndExtrema(SieveProgress& progress)
{
  // At that point, a root node has itself as root_pel and next_adj is 1 for all node
  std::vector<int8_t> extrema(_G.size(), 0);

  auto process = [&extrema, this](intptr_t pos) -> bool {
    std::unordered_set<intptr_t> known_neighbors;
    known_neighbors.reserve(25);
    auto& root = _G[pos];

    // Is the current node a root node?
    if(root.root_pel == pos) {
      intptr_t area = 0;
      intptr_t prev_adj = -1;
      int8_t sign = 0;
      auto val = _image[pos];

      known_neighbors.clear();

      // For the whole area from this root
      for(intptr_t pel = pos ; pel >= 0 ; pel = _G[pel].next_pel) {
        ++area;

        // Find new regions adjacent to the current pel
        uint32_t flags = 0;
        uint32_t bit = 1;
        uint32_t off = 0;

        _G[pel].next_adj = -1;

        for(uint32_t dirns = _adj_flags[pel] ; dirns != 0 ;
            dirns >>= 1, bit <<= 1, ++off) {
          if(dirns & 1) {
            auto adj = pel + _offsets[off];
            auto adj_root = (_G[adj].root_pel >= 0) ? _G[adj].root_pel : adj;
            // If the root wasn't known yet
            if(known_neighbors.insert(adj_root).second) {
              flags |= bit;
              sign |= (_image[adj_root] > val ? 2 : 1);
            }
          }
        }

        // Set node data
        if(prev_adj == -1)
          prev_adj = pel;
        else if(flags) {
          _G[prev_adj].next_adj = pel;
          prev_adj = pel;
        }

        _adj_flags[pel] = flags;
      }

      root.root_pel = -area;

      // At that point, sign is 1 if the area is a max, 2 if it's a min and 3 if neither
      extrema[pos] = sign;
    }
    return true;
  };

  auto size = intptr_t(_G.size());

  double progress_ratio = 33. / size;
  SieveProgress newProgress([&progress, progress_ratio](int value) -> bool {
                                return progress.advance(66 + int(value * progress_ratio)); });

  bool result = util::apply_kernel_it(process, util::Counter(0), util::Counter(size), newProgress);
  if(newProgress.canceled())
    Process::userCancel();

  if(not result)
    return false;

  for(intptr_t pos = 0 ; pos < size ; ++pos) {
    if(extrema[pos] == 2) {
      _minima.push_back(pos);
      auto area = -_G[pos].root_pel;
      if(_min_min_area > area)
        _min_min_area = area;
    } else if(extrema[pos] == 1) {
      _maxima.push_back(pos);
      auto area = -_G[pos].root_pel;
      if(_max_min_area > area)
        _max_min_area = area;
    }
  }

  return true;
}

} // namespace sieve
} // namespace process
} // namespace lgx
