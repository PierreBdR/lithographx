/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef PROCESSUTILS_HPP
#define PROCESSUTILS_HPP

#include <LGXConfig.hpp>

#include <QHash>

class QTreeWidgetItem;
class QTreeWidget;
class QRegularExpression;

namespace process_util {
/**
 * Find or create a folder from a process tree.
 *
 * \param name Name of the folder to create, sub-folders are separated by '/'
 * \param folders Map of previously created folders
 * \param tree Tree in which the folders are created
 */
QTreeWidgetItem* getFolder(QString name, QHash<QString, QTreeWidgetItem*>& folders, QTreeWidget* tree);

/**
 * Show only processes whose name match the regular expression
 *
 * \param tree Tree of processes to filter
 * \param filter Regular expression to filter with
 */
void filterProcesses(QTreeWidget* tree, const QString& filter_text);
} // namespace process_util

#endif // PROCESSUTILS_HPP

