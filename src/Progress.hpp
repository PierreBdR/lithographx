/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef PROGRESS_H
#define PROGRESS_H

#include <LGXConfig.hpp>

#include <QEvent>
#include <QList>
#include <QMutex>
#include <QObject>
#include <QPointer>
#include <QString>
#include <QtGui>
#include <string>

class QProgressDialog;
class QPushButton;
class QTimer;

namespace lgx {
// Progress dialog class
class LGX_EXPORT GlobalProgress : public QObject {
  Q_OBJECT

  QWidget* parent;

  // On linux we can't recycle the same dialog, we have to re-create is all the time or it will not
  // be located on the screen properly (Arrg!)
  QProgressDialog* prg;
  static GlobalProgress* _instance;
  QList<int> value_stack;
  QList<bool> cancel_stack;
  QStringList stack;

  bool ismodal;
  bool _canceled;

  QMutex access;

  QTimer* showTimer;

public:
  GlobalProgress();
  ~GlobalProgress();

  void setParent(QWidget* parent);
  void start(const QString& msg, int steps, bool allow_cancel = true);
  void start(const char* msg, int steps, bool allow_cancel = true)
  {
    start(QString::fromLocal8Bit(msg), steps, allow_cancel);
  }
  void start(std::string msg, int steps, bool allow_cancel = true)
  {
    start(QString::fromStdString(msg), steps, allow_cancel);
  }
  bool advance(int step);
  bool canceled();
  void stop();
  void setMaximum(int steps);
  void setText(const QString& txt);
  static void clear();   // Remove any progress bars and clear the stack
  int exec();

  int maximum() const;
  QString text() const;

  static void useModal();
  static void useModeless();

  static GlobalProgress& instance();
  QProgressDialog* dialog() {
    return prg;
  }

public slots:
  void close();

protected:
  bool event(QEvent* e);
  void createDialog(const QString& msg, int steps, int value, bool allow_cancel);
  QPointer<QPushButton> cancel;

private slots:
  void wasCanceled();
  void showDialog();
};

/**
 * \class Progress Progress.hpp <Progress.hpp>
 *
 * Provide a progress bar for the process.
 */
class LGX_EXPORT Progress : public QObject {
  Q_OBJECT
public:
  /**
   * Initialize a new progress bar with a message
   *
   * \param msg Message to write
   * \param steps Number of steps before completion. If 0, this will only show a waiting bar.
   * \param allow_cancel If true, the cancel button will be enabled.
   */
  Progress(const QString& msg, int steps, bool allow_cancel = true)
  {
    GlobalProgress::instance().start(msg, steps, allow_cancel);
  }
  /**
   * Initialize a new progress bar with a message
   *
   * \param msg Message to write
   * \param steps Number of steps before completion. If 0, this will only show a waiting bar.
   * \param allow_cancel If true, the cancel button will be enabled.
   */
  Progress(std::string msg, int steps, bool allow_cancel = true)
  {
    GlobalProgress::instance().start(msg, steps, allow_cancel);
  }
  /**
   * Initialize a new progress bar with a message
   *
   * \param msg Message to write
   * \param steps Number of steps before completion. If 0, this will only show a waiting bar.
   * \param allow_cancel If true, the cancel button will be enabled.
   */
  Progress(const char* msg, int steps, bool allow_cancel = true)
  {
    GlobalProgress::instance().start(msg, steps, allow_cancel);
  }

  ~Progress() {
    GlobalProgress::instance().stop();
  }

  /**
   * Restart the progress with a new message and a new number of steps.
   */
  bool restart(const QString& msg, int steps)
  {
    GlobalProgress::instance().stop();
    GlobalProgress::instance().start(msg, steps);
    return true;
  }
  /**
   * Restart the progress with a new message and a new number of steps.
   */
  bool restart(std::string msg, int steps) {
    return restart(QString::fromStdString(msg), steps);
  }
  /**
   * Restart the progress with a new message and a new number of steps.
   */
  bool restart(const char* msg, int steps) {
    return restart(QString::fromLocal8Bit(msg), steps);
  }

  /**
   * Advance the progress
   *
   * \param step Current step
   *
   * \returns true unless the user pressed the cancel button
   */
  bool advance(int step) {
    return GlobalProgress::instance().advance(step);
  }

  /**
   * Check if the cancel button has been pressed
   */
  bool canceled() {
    return GlobalProgress::instance().canceled();
  }

  /**
   * Change the number of steps
   */
  void setMaximum(int step) {
    GlobalProgress::instance().setMaximum(step);
  }

  /**
   * Change the text
   */
  void setText(const QString& txt)
  {
    GlobalProgress::instance().setText(txt);
  }

  /**
   * Get the current number of steps
   */
  int maximum() const
  {
    return GlobalProgress::instance().maximum();
  }

  /**
   * Get the number of steps
   */
  QString text() const
  {
    return GlobalProgress::instance().text();
  }

  /**
   * Not for use by the processes ...
   */
  int exec() {
    return GlobalProgress::instance().exec();
  }
};
} // namespace lgx
#endif
