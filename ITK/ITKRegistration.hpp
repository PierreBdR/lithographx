/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef ITKREGISTRATION_HPP
#define ITKREGISTRATION_HPP

#include <ITKConfig.hpp>
#include <ITKProcess.hpp>

#include <Stack.hpp>
#include <Store.hpp>

namespace lgx {
namespace process {

class itk_EXPORT ITKDemonsRegistration : public StackProcess {
public:
  ITKDemonsRegistration(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const ParmList& parms) override
  {
    Stack* stk = currentStack();
    Stack* ref_stk = stack(stk->id() == 0 ? 1 : 0);
    Store* ref = ref_stk->currentStore();
    return operator()(ref, stk, parms[0].toString(), stringToBool(parms[1].toString()),
                      parms[2].toInt(), parms[3].toInt(),
                      parms[4].toInt(), parms[5].toFloat());
  }

  bool operator()(const Store* reference, Stack* image, QString output, bool thresholdAtMean, int nbHistLevels,
                  int nbMatchPoints, int nbIterations, float sigma);

  QString name() const override {
    return "ITK Demons Registration";
  }
  QString folder() const override {
    return "ITK/Registration";
  }
  QString description() const override {
    return "Register the current image with the current one on the other stack";
  }

  QStringList parmNames() const override
  {
    return QStringList() << "Deformation File"
                         << "Threshold at mean intensity"
                         << "# Histogram levels"
                         << "# Match points"
                         << "# Iterations"
                         << "Sigma";
  }
  QStringList parmDescs() const override
  {
    return QStringList() << "Deformation File"
                         << "Threshold at mean intensity"
                         << "# Histogram levels"
                         << "# Match points"
                         << "# Iterations"
                         << "Sigma";
  }
  ParmList parmDefaults() const override
  {
    return ParmList() << "deformation.vtk"
                      << true
                      << 1024
                      << 7
                      << 150
                      << 1.0;
  }

  ParmChoiceMap parmChoice() const override
  {
    ParmChoiceMap map;
    map[0] = booleanChoice();
    return map;
  }
  QIcon icon() const override {
    return QIcon(":/images/Blur.png");
  }
};
}
}

#endif // ITKREGISTRATION_HPP
