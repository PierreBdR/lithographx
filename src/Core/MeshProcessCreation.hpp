/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef MESH_PROCESS_CREATION_HPP
#define MESH_PROCESS_CREATION_HPP

#include <Process.hpp>

#include <Mesh.hpp>
#include <Misc.hpp>
#include <Polygonizer.hpp>
#include <Stack.hpp>

#include <memory>

namespace lgx {
namespace process {
///\addtogroup MeshProcess
///@{
/**
 * \class MarchingCubeSurface MeshProcessCreation.hpp <MeshProcessCreation.hpp>
 *
 * Find the surface of a volume defined by an iso-surface on the image
 * intensity using a variation on the Marching Cube method.
 */
class LGXCORE_EXPORT MarchingCubeSurface : public MeshProcess, public util::ImplicitFunction {
public:
  MarchingCubeSurface(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  bool operator()(const ParmList& parms) override
  {
    if(!checkState().store(STORE_NON_EMPTY).mesh())
      return false;

    _stack = currentStack();
    _store = _stack->currentStore();
    Mesh* mesh = currentMesh();

    bool result = (*this)(mesh, _store, parms[0].toFloat(), parms[1].toInt());
    if(result)
      mesh->showSignal();
    if(result and not mesh->isSurfaceVisible() and not mesh->isMeshVisible())
      mesh->showSurface();
    return result;
  }

  bool operator()(Mesh* mesh, const Store* store, float cubeSize, int threshold);

  QString folder() const override {
    return "Creation";
  }
  QString name() const  override{
    return "Marching Cubes Surface";
  }
  QString description() const override
  {
    return "Extract surface mesh with marching cubes. A threshold to 0 will interpret the image as binary.";
  }
  QStringList parmNames() const override {
    return QStringList() << QString("Cube size (%1)").arg(UM) << "Threshold";
  }
  QStringList parmDescs() const override
  {
    return QStringList() << "Size for the marching cubes."
                         << "Minimal signal used for surface extraction.";
  }
  ParmList parmDefaults() const override
  {
    return ParmList() << 5.0
                      << 5000;
  }
  QIcon icon() const override {
    return QIcon(":/images/MarchCubes.png");
  }

  bool eval(Point3f p) override;

private:
  int _threshold;
  const Stack* _stack;
  const Store* _store;
  std::unique_ptr<Progress> progress;
};

/**
 * \class MarchingCubeSurface MeshProcessCreation.hpp <MeshProcessCreation.hpp>
 *
 * Find the surface of the segmented volumes using a variation on
 * the Marching Cube method.
 */
class LGXCORE_EXPORT MarchingCube3D : public MeshProcess, public util::ImplicitFunction {
public:
  MarchingCube3D(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  bool operator()(const ParmList& parms)
  {
    if(!checkState().store(STORE_LABEL).mesh())
      return false;

    _stack = currentStack();
    _store = _stack->currentStore();
    Mesh* mesh = currentMesh();

    bool result = (*this)(mesh, _store, parms[0].toFloat(), parms[1].toInt(), parms[2].toInt(), parms[3].toInt());
    if(result)
      mesh->showLabel();
    if(result and not mesh->isSurfaceVisible() and not currentMesh()->isMeshVisible())
      currentMesh()->showSurface();
    return result;
  }

  bool operator()(Mesh* mesh, const Store* store, float cubeSize, int minVoxels, int smooth, int singleLabel);

  QString folder() const {
    return "Creation";
  }
  QString name() const {
    return "Marching Cubes 3D";
  }
  QString description() const {
    return "Extract 3D meshes with marching cubes";
  }
  QStringList parmNames() const
  {
    return QStringList() << QString("Cube size (%1)").arg(UM) << "Min Voxels"
                         << "Smooth Passes"
                         << "Label";
  }
  QStringList parmDescs() const
  {
    return QStringList() << "Size for the marching cubes."
                         << "Minimal number of voxels for extracting surface."
                         << "Smooth Passes."
                         << "Extract surface only for this label.";
  }
  ParmList parmDefaults() const
  {
    return ParmList() << 5.0
                      << 0
                      << 3
                      << 0;
  }
  QIcon icon() const {
    return QIcon(":/images/MarchCubes3D.png");
  }

  bool eval(Point3f p);

private:
  int _label;
  const Stack* _stack;
  const Store* _store;
  std::unique_ptr<Progress> progress;
};

/**
 * \class CuttingSurfMesh MeshProcessCreation.hpp <MeshProcessCreation.hpp>
 *
 * Create a surface matching the current cutting surface, whether it's a
 * plane or a Bezier surface.
 */
class LGXCORE_EXPORT CuttingSurfMesh : public MeshProcess {
public:
  CuttingSurfMesh(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  bool operator()(const ParmList& )
  {
    if(!checkState().mesh())
      return false;
    return (*this)(currentMesh());
  }

  bool operator()(Mesh* mesh);

  QString folder() const {
    return "Creation";
  }
  QString name() const {
    return "Mesh Cutting Surface";
  }
  QString description() const {
    return "Make mesh from cutting surface";
  }
  QStringList parmNames() const {
    return QStringList();
  }
  QStringList parmDescs() const {
    return QStringList();
  }
  QIcon icon() const {
    return QIcon(":/images/MakePlane.png");
  }
};

class LGXCORE_EXPORT MeshFromLabelledPoints : public MeshProcess {
public:
  MeshFromLabelledPoints(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  bool operator()(const ParmList& parms) override
  {
    if(!checkState().store(STORE_NON_EMPTY | STORE_LABEL))
      return false;
    bool ok;
    float radius = parms[0].toFloat(&ok);
    if(not ok or radius <= 0)
      return setErrorMessage("Parameter 'Radius' must be a strictly positive number.");
    uint detail = parms[2].toUInt(&ok);
    if(not ok or detail == 0)
      return setErrorMessage("Parameter 'Detail' must be a strictly positive integer number");
    bool res = (*this)(currentMesh(), currentStack()->currentStore(), radius, parms[1].toString(), detail);
    if(res) {
      currentMesh()->showSurface();
      currentMesh()->showLabel();
    }
    return res;
  }

  bool operator()(Mesh* mesh, const Store* store, float radius, QString type, uint details);

  QString folder() const override {
    return "Creation";
  }
  QString name() const override {
    return "Mesh from Labelled Points";
  }
  QString description() const override {
    return "Add a sphere around each point labelled in the 3D image.";
  }
  QStringList parmNames() const override {
    return QStringList() << QString("Radius (%1)").arg(UM)
                         << "Shape"
                         << "Detail level";
  }
  QStringList parmDescs() const override
  {
    return QStringList() << "Radius of the spheres to be added"
                         << "How is the shape created:\n"
                            " - RegularSphere: Start from a regular shape and subdivide it n times.\n"
                            " - UniformSphere: Spread uniformely n points on a sphere\n"
                            " - Cube : Add a simple cube"
                         << "The meaning of this parameter depends on the shape:\n"
                            " - RegularSphere: Number of subdivision\n"
                            " - UniformSphere: Number of points spread\n"
                            " - Cube: the parameter is ignored";
  }

  ParmChoiceMap parmChoice() const override
  {
    ParmChoiceMap map;
    map[1] = QStringList{ "RegularSphere", "UniformSphere", "Cube" };
    return map;
  }

  ParmList parmDefaults() const override
  {
    return ParmList() << 1.0
                      << "RegularSphere"
                      << 3;
  }
  QIcon icon() const override {
    return QIcon(":/images/PointLabels.png");
  }
};

class LGXCORE_EXPORT CellMeshFrom2DImage : public MeshProcess {
public:
  CellMeshFrom2DImage(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  bool operator()(const ParmList& parms)
  {
    if(!checkState().store(STORE_NON_EMPTY | STORE_LABEL))
      return false;
    bool ok;
    float minLength = parms[0].toFloat(&ok);
    if(!ok)
      return setErrorMessage("Error, parameter 'Min Length' must be a number.");
    uint zslice = parms[1].toUInt(&ok);
    if(!ok)
      return setErrorMessage("Error, parameter 'Z Slice' must be an integer number.");
    bool res = (*this)(currentMesh(), currentStack()->currentStore(), minLength,
                       zslice, parmToBool(parms[2]));
    if(res) {
      currentMesh()->showSurface();
      currentMesh()->showLabel();
    }
    return res;
  }

  bool operator()(Mesh* mesh, const Store* store, float minLength, uint zslice, bool showErrors);

  QString folder() const {
    return "Cell Mesh";
  }
  QString name() const {
    return "Cell Mesh From 2D Image";
  }
  QString description() const {
    return "Create a Mesh from a labeled 2D image.";
  }
  QStringList parmNames() const {
    return QStringList() << QString("Edge Length (%1)").arg(UM)
                         << "Z Slice"
                         << "Show Errors with Mesh";
  }
  QStringList parmDescs() const
  {
    return QStringList() << "All edges will have at most this length."
                         << "For 3D images, which slice should the algorithm use."
                         << "If true, replace mesh with points showing where the errors are.";
  }
  ParmList parmDefaults() const
  {
    return ParmList() << -1.0
                      << 0
                      << true;
  }
  QIcon icon() const {
    return QIcon(":/images/MarchCubes3D.png");
  }
};

///@}
} // namespace process
} // namespace lgx

#endif
