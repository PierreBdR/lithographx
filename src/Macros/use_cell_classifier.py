# -*- coding:utf-8 -*-
from __future__ import print_function, division
from lgxPy import parmToBool, Mesh, ProcessError
import threading

from PyQt5.QtWidgets import QFileDialog, QDialog, QDialogButtonBox
from PyQt5.uic import loadUi
from PyQt5.QtCore import pyqtSlot, pyqtSignal, QTimer, Qt
from PyQt5.QtGui import QColor, QPalette
from cell_classifier.utils import processFilePattern, isValidFilePattern, printable, correctFormat
from lgxUtils.path import Path
from cell_classifier.find_files_model import FindFilesModel

try:
    import pandas as pd
    from sklearn import externals
except ImportError:
    raise RuntimeError("""The following modules need to be installed:
- scikit-learn 0.17 or later (available from http://scikit-learn.org)
- numpy (available from http://numpy.org)
- scipy (available from http://scipy.org)""")


processType = "Global"
processName = "Use Cell Classifier"
processFolder = "Cell Classifier"
processIcon = ":/images/CellFeatures.png"
parmsDescs = dict(Folder=u"Folder containing all the files.",
                  Main_Pattern=u"Pattern used to find main files",
                  Recursive=u"If true, files will be also searched in sub-folders",
                  Feature_Suffix=u"""Template used to generate the feature file names when they are computed.
In this string, '{dirname}' will be replaced by the folder containing the main file and
'{basename}' will be replaced by the file name without its extension.

If the features are not computed, this should be a traditional file pattern (such as '*-shapes.csv').""",
                  Classifier_Filename=u"Name of the file in which the classifier will be saved.",
                  Output_Suffix='''Template used to generate the output file name.
It follows the same pattern as for the 'Feature File Name' parameter''',
                  Output_Features_Suffix='If specified, a new feature file will be saved with the classified cells.',
                  Min_Cell_Area='''Any cell smaller than this area will not be classified.''')
parmsChoice = dict(Recursive=["Yes", "No"])


class UseClassifierDlg(QDialog):

    preventValidation = pyqtSignal(bool)
    allowValidation = pyqtSignal(bool)

    def __init__(self, parent):
        QDialog.__init__(self, parent)
        self._initialized = False
        pth = Path(__file__).dirname() / 'UseClassifier.ui'
        self.ui = loadUi(pth, self)
        bbox = self.ui.buttonBox
        okBtn = bbox.button(bbox.Ok)
        self.preventValidation.connect(okBtn.setDisabled)
        self.allowValidation.connect(okBtn.setEnabled)
        model = FindFilesModel(self.ui.baseFolder.text(), ('Main Files', self.ui.mainFilePattern.text()),
                               [('Features Files', self.ui.featureFileSuffix.text(), "mandatory"),
                                ('Output Features', self.ui.outputFeatureSuffix.text(), "output"),
                                ('Output Files', self.ui.outputFileSuffix.text(), "output")],
                               False, self)
        model.setObjectName("filesModel")
        model.modelReset.connect(self.on_filesModel_modelReset)
        btn = self.ui.buttonBox.addButton("Refresh files", QDialogButtonBox.ActionRole)
        btn.clicked.connect(self.refreshFiles)
        self._model = model
        self.ui.filesView.setModel(model)

        pal = self.ui.mainFilePattern.palette()
        self.goodBack = pal.color(self.ui.mainFilePattern.backgroundRole())
        self.badBack = QColor(Qt.red).lighter()

        self.resetTimer = QTimer()
        self.resetTimer.setInterval(10)
        self.resetTimer.setSingleShot(True)
        self.resetTimer.timeout.connect(self.resizeColumns)

        self.resetTimer.start()

    @pyqtSlot(bool)
    def on_outputFeature_toggled(self, on):
        self._model.showColumn(2, on)

    @pyqtSlot()
    def on_filesModel_modelReset(self):
        self.resetTimer.start()

    @pyqtSlot()
    def resizeColumns(self):
        n = self._model.columnCount()
        for i in range(n):
            self.ui.filesView.resizeColumnToContents(i)
        h = self.ui.filesView.header()
        h.stretchLastSection()

    @pyqtSlot()
    def refreshFiles(self):
        self._model.findFiles()

    def exec_(self):
        self._initialized = True
        return QDialog.exec_(self)

    @pyqtSlot(str)
    def on_baseFolder_textChanged(self, txt):
        p = Path(txt)
        if p.isdir():
            self._model.baseFolder = txt

    @pyqtSlot(bool)
    def on_computeFeatures_toggled(self, on):
        self._model.showColumn(1, on)

    @pyqtSlot(bool)
    def on_findRecursive_toggled(self, on):
        self._model.recursive = on

    def setRecursive(self, on):
        self._model.recursive = on
        self.ui.findRecursive.setChecked(on)

    @pyqtSlot(str)
    def on_mainFilePattern_textChanged(self, txt):
        if not isValidFilePattern(txt):
            self.setError(self.ui.mainFilePattern)
            del self._model.mainFilePattern
            self.preventValidation.emit(True)
        else:
            self.setError(self.ui.mainFilePattern, False)
            self.allowValidation.emit(True)
            self._model.mainFilePattern = txt

    def setError(self, item, err=True):
        pal = item.palette()
        pal.setColor(QPalette.Base, self.badBack if err else self.goodBack)
        item.setPalette(pal)

    @pyqtSlot(str)
    def on_featureFileSuffix_textChanged(self, txt):
        self._model.setColumnFormat(1, txt)

    @pyqtSlot(str)
    def on_outputFeatureSuffix_textChanged(self, txt):
        self._model.setColumnFormat(2, txt)

    @pyqtSlot(str)
    def on_outputFileSuffix_textChanged(self, txt):
        self._model.setColumnFormat(3, txt)

    @pyqtSlot()
    def on_chooseFolder_clicked(self):
        newFolder = self.ui.baseFolder.text()
        newFolder = QFileDialog.getExistingDirectory(self, "Choose Base Folder", newFolder)
        if newFolder:
            self.ui.baseFolder.setText(newFolder)
            self.checkClassifier()

    @pyqtSlot()
    def on_chooseClassifier_clicked(self):
        baseDir = Path(self.ui.baseFolder.text())
        newFile = self.ui.classifierFilename.text()
        newFile = QFileDialog.getOpenFileName(self, "Choose file that contains the classifier.",
                                              newFile, "Pickle Files (*.pkl);;All Files (*.*)")
        newFile = newFile[0]
        if newFile:
            if not newFile.endswith('.pkl'):
                newFile += '.pkl'
            newFile = Path(newFile).relpath(baseDir)
            self.ui.classifierFilename.setText(newFile)

    @pyqtSlot(str)
    def on_classifierFilename_textChanged(self, txt):
        self.checkClassifier()

    def checkClassifier(self):
        pth = self._model.baseFolder / self.ui.classifierFilename.text()
        if not pth.isfile():
            self.setError(self.ui.classifierFilename)
            self.ui.classifierFilename.setToolTip("File doesn't exist!")
            self.preventValidation.emit(True)
        else:
            self.setError(self.ui.classifierFilename, False)
            self.ui.classifierFilename.setToolTip("File exists.")
            self.allowValidation.emit(True)

    def showEvent(self, event):
        self.allowValidation.emit(isValidFilePattern(self.ui.mainFilePattern.text()))


def parseParms(Folder, Main_Pattern, Recursive,
               Feature_Suffix, Classifier_Filename,
               Output_Suffix, Output_Features_Suffix, Min_Cell_Area):
    """
    Python macro function to transform parameters into valid Python objects
    """
    Folder = Path(Folder)
    Main_Pattern = str(Main_Pattern)
    Recursive = parmToBool(Recursive)
    Feature_Suffix = str(Feature_Suffix)
    Classifier_Filename = Path(Classifier_Filename)
    Output_Suffix = str(Output_Suffix)
    Min_Cell_Area = float(Min_Cell_Area)
    Output_Features_Suffix = str(Output_Features_Suffix)

    if not Folder:
        Folder = Path('.')
    Folder = Folder.abspath()
    return (Folder, Main_Pattern, Recursive,
            Feature_Suffix, Classifier_Filename,
            Output_Suffix, Output_Features_Suffix, Min_Cell_Area)


def initialize(Folder, Main_Pattern, Recursive,
               Feature_Suffix, Classifier_Filename,
               Output_Suffix, Output_Features_Suffix, Min_Cell_Area,
               parent):
    """
    Provide a user interface to initialize the process
    """
    dlg = UseClassifierDlg(parent)

    dlg.ui.baseFolder.setText(Folder)
    dlg.ui.mainFilePattern.setText(Main_Pattern)
    dlg.ui.featureFileSuffix.setText(Feature_Suffix)
    dlg.ui.classifierFilename.setText(Classifier_Filename)
    dlg.ui.outputFileSuffix.setText(Output_Suffix)
    dlg.ui.minCellArea.setValue(Min_Cell_Area)
    dlg.ui.outputFeature.setChecked(bool(Output_Features_Suffix))
    if Output_Features_Suffix:
        dlg.ui.outputFeatureSuffix.setText(Output_Features_Suffix)
    dlg.setRecursive(Recursive)

    if dlg.exec_() == QDialog.Accepted:
        Folder = dlg.ui.baseFolder.text()
        Main_Pattern = dlg.ui.mainFilePattern.text()
        Recursive = dlg.ui.findRecursive.isChecked()
        Feature_Suffix = dlg.ui.featureFileSuffix.text()
        Classifier_Filename = dlg.ui.classifierFilename.text()
        Output_Suffix = dlg.ui.outputFileSuffix.text()
        Min_Cell_Area = dlg.ui.minCellArea.value()
        if dlg.ui.outputFeature.isChecked():
            Output_Features_Suffix = dlg.ui.outputFeatureSuffix.text()
        else:
            Output_Features_Suffix = ""
        return (Folder, Main_Pattern, Recursive,
                Feature_Suffix, Classifier_Filename,
                Output_Suffix, Output_Features_Suffix, Min_Cell_Area)


def run(Folder="", Main_Pattern="{*}.ply", Recursive=False,
        Feature_Suffix="-features.csv", Classifier_Filename="classifier.pkl",
        Output_Suffix="-celltypes.csv", Output_Features_Suffix="-features-typed.csv", Min_Cell_Area=3):
    """
    Execute the process
    """
    th = threading.current_thread()
    th.name = 'MainThread'

    Classifier_Filename = Folder / Classifier_Filename
    estimator = externals.joblib.load(Classifier_Filename)

    Feature_Suffix = correctFormat(Feature_Suffix)
    Output_Suffix = correctFormat(Output_Suffix)
    if Output_Features_Suffix:
        Output_Features_Suffix = correctFormat(Output_Features_Suffix)

    feature_files = []
    output_files = []

    main_glob, main_basename = processFilePattern(Main_Pattern)

    main_files = None
    if Recursive:
        main_files = Folder.walkfiles(main_glob)
    else:
        main_files = Folder.files(main_glob)

    used_main_files = []

    for main in main_files:
        b = main_basename(main.relpath(Folder))
        feat = Folder / Feature_Suffix.format(b)
        out = Folder / Output_Suffix.format(b)
        print("feature file: ", printable(feat))

        if feat.isfile():
            used_main_files.append(main)
            feature_files.append(feat)
            output_files.append(out)

    pd_data = [pd.read_csv(f) for f in feature_files]

    if not pd_data:
        raise ValueError("No file could be found")

    columns = estimator.fields_name

    print("Selected columns: ", printable(", ".join(columns)))
    print("=======")

    area = [c for c in columns if c.startswith("Area")]
    if area:
        area = area[0]
    else:
        area = None

    if Output_Features_Suffix:
        Mesh.Reset("")

    for data, main, feat, out in zip(pd_data, used_main_files, feature_files, output_files):
        if area is not None:
            sel = data[area] > Min_Cell_Area
        data = data[sel]
        exog = data[columns].as_matrix()
        cell_type = estimator.predict(exog)
        labels = data.Label[sel]
        result = pd.DataFrame(dict(Label=labels, CellType=cell_type), columns=["Label", "CellType"])
        result.to_csv(out, index=False)
        if Output_Features_Suffix:
            Mesh.View('Yes', 'Parents', '', '', '', '', '', '', '', '', '', '', '', -1, -1)
            Mesh.Load_Parents(out, False)
            new_feat = Folder / Output_Features_Suffix.format(main_basename(main.relpath(Folder)))
            print("New feature file:", new_feat)
            Mesh.Add_Lineage_to_Table(feat, new_feat, 0, 'Estimated Cell Type', '', False)
