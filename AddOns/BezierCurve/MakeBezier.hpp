#ifndef FindBezier_HPP
#define FindBezier_HPP

#include <Process.hpp>

namespace lgx {
namespace process {
class FindBezier : public MeshProcess {
public:
    enum CellParameter { UNIFORM_PARAM, DISTANCE_PARAM, HEAT_PARAM };

    enum CellOrdering { PCA_ORDERING, LOCAL_ORDERING };

    enum CellType { CELL_LABEL, CELL_CONNECTIVITY, CELL_CONNECTIVITY_EXTEND, CELL_VERTICES };

    FindBezier(const MeshProcess& process)
        : Process(process)
        , MeshProcess(process)
    {
    }

    bool operator()(const ParmList& parms) override
    {
        if(!checkState().mesh(MESH_NON_EMPTY))
            return false;

        CellType ct;
        QString ps = parms[0].toString().toLower();
        if(ps == "label")
            ct = CELL_LABEL;
        else if(ps == "connectivity")
            ct = CELL_CONNECTIVITY;
        else if(ps == "connectivity-extend")
            ct = CELL_CONNECTIVITY_EXTEND;
        else if(ps == "vertices")
            ct = CELL_VERTICES;
        else
            return setErrorMessage(
                QString("Cell parameter must be one of 'Label', 'Connectivity', 'Connectivity-extend' or 'Vertices'"));

        CellParameter p;
        ps = parms[1].toString().toLower();
        if(ps == "uniform")
            p = UNIFORM_PARAM;
        else if(ps == "heat")
            p = HEAT_PARAM;
        else if(ps == "distance")
            p = DISTANCE_PARAM;
        else
            return setErrorMessage(QString("Cell Parameter must be one of 'Uniform','Heat' or 'Distance'"));

        CellOrdering co;
        ps = parms[2].toString().toLower();
        if(ps == "pca")
            co = PCA_ORDERING;
        else if(ps == "local neighborhood")
            co = LOCAL_ORDERING;
        else
            return setErrorMessage("Cell Ordering parameter must be one of 'PCA' or 'Local neighborhood'.");

        bool ok;
        int max_order = parms[4].toInt(&ok);
        if(!ok)
            return setErrorMessage("Error, parameger 'Max Order' must be an integer number");
        float tmin = parms[5].toFloat(&ok);
        if(!ok)
            return setErrorMessage("Error, parameger 't min' must be a number");
        float tmax = parms[6].toFloat(&ok);
        if(!ok)
            return setErrorMessage("Error, parameger 't max' must be a number");

        return operator()(currentMesh(), ct, p, co, parms[3].toString(), max_order, tmin, tmax);
    }

    bool operator()(Mesh* mesh, CellType ct, CellParameter param, CellOrdering ordering, QString order, int max_order,
        float tmin, float tmax);

    QString folder() const { return "Bezier Curve"; }
    QString name() const { return "Find Bezier Curve"; }
    QString description() const
    {
        return "Find the Bezier Curve going through the selected cells. The order should be a mathematical expression "
               "with N being the number of cells selected.";
    }
    QStringList parmNames() const
    {
        return QStringList() << "Cells"
                             << "Cell Parameter"
                             << "Cell ordering"
                             << "Order"
                             << "Max Order"
                             << "t min"
                             << "t max";
    }
    QStringList parmDescs() const
    {
        return QStringList()
            << "Define how cells are defined. Can be one of:\n"
               " - Label: all selected vertices with the same label are part of the same cell\n"
               " - Connectivity: all connect component of selected cells are part of the same cell\n"
               " - Connectivity-extend: all vertex connected to a selected vertex will be part of the same cell\n"
               " - Vertices: Each vertex is its own cell"
            << "Quantity used to set the parameter to fit for each cell.\n"
               "'Distance' is recommended for a parameter almost proportional to the arc-length parameter."
            << "Method to find the ordering of the cells. Can be one of:\n"
               "  - 'PCA': order the cells as their projections on the main PC of the point cloud\n"
               "  - 'Local neighborhood': Try to deduce the ordering from the local neighborhood of each cell."
            << "Order of the Bezier curve. Can be the string 'BIC', a number, or a formula using 'N' as the number of "
               "cells or a fix number.\n"
               "If the string 'BIC' is used, all orders from 1 to 'Max Order' will be tested and the one with the "
               "smallest Bayesian Information Criterion"
            << "If the order is defined by a formula, this is the maximum order that will be ever used."
            << "Parameter of the first cell."
            << "Parameter of the last cell.";
    }
    ParmList parmDefaults() const
    {
        return ParmList() << "Label"
                          << "Distance"
                          << "Local neighborhood"
                          << "BIC"
                          << "10"
                          << "0"
                          << "1";
    }
    ParmChoiceMap parmChoice() const
    {
        ParmChoiceMap map;
        map[0] = QStringList() << "Label"
                               << "Connectivity"
                               << "Connectivity-extend"
                               << "Vertices";
        map[1] = QStringList() << "Uniform"
                               << "Distance"
                               << "Heat";
        map[2] = QStringList() << "PCA"
                               << "Local neighborhood";
        map[3] = QStringList() << "BIC";
        return map;
    }
    QIcon icon() const { return QIcon(":/images/FindBezier.png"); }
};

class ReverseBezierCurveProcess : public MeshProcess {
public:
    ReverseBezierCurveProcess(const MeshProcess& process)
        : Process(process)
        , MeshProcess(process)
    {
    }

    bool operator()(const ParmList&) { return operator()(); }

    bool operator()();

    QString folder() const { return "Bezier Curve"; }
    QString name() const { return "Reverse Bezier Curve"; }
    QString description() const { return "Reverse the order of the control points in the Bezier curve."; }
    QStringList parmNames() const { return QStringList(); }
    QIcon icon() const { return QIcon(":/images/ReverseBezierCurveProcess.png"); }
};
} // namespace process
} // namespace lgx

#endif // FindBezier_HPP
