/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef SYSTEMDIRS_HPP
#define SYSTEMDIRS_HPP

#include <LGXConfig.hpp>

#include <QDir>
#include <QList>

namespace lgx {
namespace util {
/// Returns the folder where LithoGraphX was installed
LGX_EXPORT QDir installDir();
/// Returns the base dir for configuration
LGX_EXPORT QDir configurationDir(bool create=false);
/// Returns resource directory
LGX_EXPORT QDir resourcesDir();
/// Returns user processes directory
LGX_EXPORT QDir userProcessesDir(bool create=false);
/// Returns system processes directory
LGX_EXPORT QDir systemProcessesDir();
/// Returns processes directories
LGX_EXPORT QList<QDir> processesDirs();
/// Returns the includes directory
LGX_EXPORT QDir includesDir();
/// Returns the libs directory
LGX_EXPORT QDir libsDir();
/// Returns the user libs directory
LGX_EXPORT QDir userLibsDir(bool create=false);
/// Returns the documentation directory
LGX_EXPORT QDir docsDir();
/// Return the macro directories
LGX_EXPORT QList<QDir> macroDirs();
/// Return the user macro directory
LGX_EXPORT QDir systemMacroDir();
/// Return the user macro directory
LGX_EXPORT QDir userMacroDir(bool create=false);
/// Return the folder containing the description of system packages
LGX_EXPORT QDir systemPackagesDir();
/// Return the folder containing the description of user packages
LGX_EXPORT QDir userPackagesDir(bool create=false);
/// Return packages directories
LGX_EXPORT QList<QDir> packagesDirs();

} // namespace util
} // namespace lgx

#endif // SYSTEMDIRS_HPP
