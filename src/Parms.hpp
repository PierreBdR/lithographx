/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef __PARMS_HPP__
#define __PARMS_HPP__
/**
 * \file Parms.hpp
 *
 * Defines the util::Parms class
 */

#include <LGXConfig.hpp>

#include <Forall.hpp>
#include <Information.hpp>

#include <iostream>
#include <iterator>
#include <list>
#include <map>
#include <QString>
#include <QStringList>
#include <QTextStream>
#include <set>
#include <sstream>
#include <string>
#include <vector>

LGX_EXPORT QTextStream& operator>>(QTextStream& ss, bool& b);

namespace lgx {
namespace util {
/**
 * \class Parms Parms.hpp <Parms.hpp>
 * \brief A utility class to parse L-Studio like parameter files
 *
 * <h2>Format of the parameter file</h2>
 *
 * The basic information in the parameter file is a key associated to
 * a value, possibly with a C++-like comment:
 *
 * \code
 * key: value // Comment
 * \endcode
 *
 * Keys can be organized in sections:
 *
 * \code
 * [Section1]
 * key1: value1
 * key2: value2
 * ...
 *
 * [Section2]
 * key1: value3
 * key2: value4
 * ...
 * \endcode
 *
 * Empty line or comment-only line are ignored, any other line will raise an
 * error. The default section is named with an empty string "".
 *
 * <h2> Usage example of util::Parm </h2>
 *
 * Here is an example:
 *
 * \code
 * int a,b;
 * QString s1, s2;
 * std::vector<double> all_d;
 * util::Parms parms( "view.v", 2 );
 *
 * // First read simple parameters
 * parms( "", "a", a );
 * parms( "Main", "b", b, 0 );
 * parms( "Main", "string1", s1 );
 * parms( "Main", "string2", s2 );
 *
 * // Then read all the keys "double" in section "Values"
 * parms.all( "Values, "double", all_d);
 * \endcode
 *
 * <h2>Reading typed parameters</h2>
 *
 * To read user-defined typed as parameter, it is enough to overload the
 * function
 * \code QTextStream& operator>>( QTextStream&, const T& ). \endcode
 * \p T being the type of the parameter to read.
 *
 * <h2>Key duplicates</h2>
 *
 * If the same key is used may times in the same section, either all the
 * values can be retrieved using the Parms::all() method, or only the last
 * can be retrieved using the normal Parms::operator()().
 *
 * <h2> \anchor ParmVerbosity Verbosity</h2>
 *
 * The class accepts 5 verbosity levels:
 * - 0: No output
 * - 1: Errors only
 * - 2: Errors and warning
 * - 3: User information
 * - 4: Debug information
 *
 * Debug information output the raw string before evaluation to set up
 * a parameter.
 *
 */
class LGX_EXPORT Parms {
public:
  /**
   * Default constructor, with no file
   */
  explicit Parms(int verboseLevel = 1);
  Parms(QString sep, QString comment, int verboseLevel = 1);
  Parms(const QString& parmFile, QString sep, QString comment, int verboseLevel = 1);
  Parms(const QString& parmFile, int verboseLevel = 1);
  Parms(QIODevice* parmDev, QString sep, QString comment, int verboseLevel = 1);
  Parms(QIODevice* parmDev, int verboseLevel = 1);

  ~Parms();

  /**
   * Change the verbosity level.
   *
   * \see \ref ParmVerbosity "Verbosity"
   */
  void verboseLevel(int vl) {
    VerboseLevel = (vl < 0) ? 0 : vl;
  }

  /**
   * Returns true if the parameter object has been correctly loaded
   */
  bool isLoaded() const {
    return loaded;
  }

  /**
   * Returns true if the parameter file was empty (e.g. no valid values were found)
   */
  bool empty() const {
    return Parameters.empty();
  }

  /**
   * Same of operator(), but doesn't raise an error if not present
   */
  template <typename T>
  bool optional(const QString& section, const QString& key, T& value);

  /**
   * This operator retrieve a single parameter.
   * \param section Section in which the parameter is looked for
   * \param key Key to look for
   * \param value Variable to set up if possible
   *
   * If the [section]key exists, \c value is set up to the parameter value.
   * Any key placed before the first section is considered in the first
   * secion. If the [section]key is multiply defined, only the last one is
   * taken and a warning is issued (see \ref ParmVerbosity "Verbosity"). If
   * the [section]key does not exist, or its content cannot be interpreted as
   * the requested type, then ar error is issued.
   *
   * \returns True if there was no error while converting the different
   * parameters from their string representation and the [section]key
   * existes.
   */
  template <typename T>
  bool operator()(const QString& section, const QString& key, T& value);

  /**
   * Retrieve a single parameter.
   *
   * Boolean value must read "true" or "false", case being meaningless.
   *
   * \see operator()( const QString& section, const QString& key, T& value )
   */
  bool operator()(const QString& section, const QString& key, bool& value);
  /**
   * Retrieve a single parameter.
   *
   * \see operator()( const QString& section, const QString& key, T& value )
   */
  bool operator()(const QString& section, const QString& key, int& value);
  /**
   * Retrieve a single parameter.
   *
   * \see operator()( const QString& section, const QString& key, T& value )
   */
  bool operator()(const QString& section, const QString& key, float& value);
  /**
   * Retrieve a single parameter.
   *
   * \see operator()( const QString& section, const QString& key, T& value )
   */
  bool operator()(const QString& section, const QString& key, double& value);
  /**
   * Retrieve a single parameter.
   *
   * For string, the value is the whole line with whitespaces and comment
   * stripped before and after the parameter.
   *
   * \see operator()( const QString& section, const QString& key, T& value )
   */
  bool operator()(const QString& section, const QString& key, std::string& value);

  /**
   * Retrieve a single parameter.
   *
   * For string, the value is the whole line with whitespaces and comment
   * stripped before and after the parameter.
   *
   * \see operator()( const QString& section, const QString& key, T& value )
   */
  bool operator()(const QString& section, const QString& key, QString& value);

  /**
   * Variation on the previous, but if the [section]key is not found, an
   * information message is issued (instead of an error) and \c value is set
   * up to \c def.
   */
  template <typename T> bool operator()(const QString& section, const QString& key, T& value, const T& def);

  /**
   * This operator retrieves all parameters with same [section]key.
   * \param section Section in which the parameter is looked for
   * \param key Key to look for
   * \param value Variable to set up
   *
   * \c value is filled with the different values found having same
   * [section]key. If none, \c value is simply empty. The only error that can
   * arise is a reading error, if one parameter has invalid value. This
   * parameter will simply be ignored, all other parameters being read.
   *
   * \returns True if there was no error while converting the different
   * parameters from their string representation.
   */
  template <typename Container> bool all(const QString& section, const QString& key, Container& value);

  /**
   * Retrieve a all parameters with same [section]key.
   *
   * \see all( const QString& section, const QString& key, std::vector<T>& value )
   */
  bool all(const QString& section, const QString& key, std::vector<bool>& value);
  /**
   * Retrieve a all parameters with same [section]key.
   *
   * \see all( const QString& section, const QString& key, std::vector<T>& value )
   */
  bool all(const QString& section, const QString& key, std::vector<int>& value);
  /**
   * Retrieve a all parameters with same [section]key.
   *
   * \see all( const QString& section, const QString& key, std::vector<T>& value )
   */
  bool all(const QString& section, const QString& key, std::vector<float>& value);
  /**
   * Retrieve a all parameters with same [section]key.
   *
   * \see all( const QString& section, const QString& key, std::vector<T>& value )
   */
  bool all(const QString& section, const QString& key, std::vector<double>& value);
  /**
   * Retrieve a all parameters with same [section]key.
   *
   * \see all( const QString& section, const QString& key, std::vector<T>& value )
   */
  bool all(const QString& section, const QString& key, std::vector<std::string>& value);
  /**
   * Retrieve a all parameters with same [section]key.
   *
   * \see all( const QString& section, const QString& key, std::vector<T>& value )
   */
  bool all(const QString& section, const QString& key, std::vector<QString>& value);
  /**
   * Retrieve a all parameters with same [section]key.
   *
   * \see all( const QString& section, const QString& key, std::vector<T>& value )
   */
  bool all(const QString& section, const QString& key, QStringList& value);

  /**
   * This operator retrieves all parameters with same [section].
   * \param section Section in which the parameter is looked for
   * \param value Variable to set up
   *
   * \c value is filled with the different keys found in [section] and, for
   * each key, the vector of all the values associated to it. If none, \c
   * value is simply empty. The only error that can arise is a reading error,
   * if one parameter has invalid value. This parameter will simply be
   * ignored, all other parameters being read.
   *
   * \returns True if there was no error while converting the different
   * parameters from their string representation.
   */
  template <typename T, typename Container> bool all(const QString& section, std::map<QString, Container>& values);

  /**
   * Retrieve a all parameters with same [section].
   *
   * \see all( const QString& section, std::map<QString, std::vector<T> >& value )
   */
  bool all(const QString& section, std::map<QString, std::vector<bool> >& value);
  /**
   * Retrieve a all parameters with same [section].
   *
   * \see all( const QString& section, std::map<QString, std::vector<T> >& value )
   */
  bool all(const QString& section, std::map<QString, std::vector<int> >& value);
  /**
   * Retrieve a all parameters with same [section].
   *
   * \see all( const QString& section, std::map<QString, std::vector<T> >& value )
   */
  bool all(const QString& section, std::map<QString, std::vector<float> >& value);
  /**
   * Retrieve a all parameters with same [section].
   *
   * \see all( const QString& section, std::map<QString, std::vector<T> >& value )
   */
  bool all(const QString& section, std::map<QString, std::vector<double> >& value);
  /**
   * Retrieve a all parameters with same [section]key.
   *
   * \see all( const QString& section, std::map<QString, std::vector<T> >& value )
   */
  bool all(const QString& section, std::map<QString, std::vector<std::string> >& value);
  /**
   * Retrieve a all parameters with same [section]key.
   *
   * \see all( const QString& section, std::map<QString, std::vector<T> >& value )
   */
  bool all(const QString& section, std::map<QString, std::vector<QString> >& value);
  /**
   * Retrieve a all parameters with same [section]key.
   *
   * \see all( const QString& section, std::map<QString, std::vector<T> >& value )
   */
  bool all(const QString& section, std::map<QString, QStringList>& value);

  /// Check if a section exists
  bool hasSection(const QString& section);

  /// Check is a value exists
  bool hasValue(const QString& section, const QString& name);

private:
  /**
   * Initialize the object from an opened device
   */
  void init(QIODevice* dev);

  /**
   * Initialize the object from a file name
   */
  void init(const QString& filename);

  /**
   * Extract a value as a string given \c section and \c key.
   *
   * \returns True if [section]key exist.
   */
  bool extractValues(const QString& section, const QString& key, QStringList& values);

  /**
   * Read a value from a string.
   *
   * If the value to read is a boolean, the input string has to be equal to
   * "true" or "false" (case insensitive).
   *
   * \returns True if the reading succeeded
   */
  bool readValue(const QString& value, bool& variable);
  /**
   * Read a value from a string.
   *
   * If the value to read is a string, the input one is simply copied.
   *
   * \returns True if the reading succeeded
   */
  bool readValue(const QString& value, std::string& variable);
  /**
   * Read a value from a string.
   *
   * If the value to read is a string, the input one is simply copied.
   *
   * \returns True if the reading succeeded
   */
  bool readValue(const QString& value, QString& variable);
  /**
   * Read a vector of values from a string.
   *
   * Default is to use operator>> on a \c istringstream defined on the input
   * string. Print an error is the flux operator fails.
   *
   * \returns True if the reading succeeded
   */
  template <typename T> bool readValue(const QString& value, std::vector<T>& variable);
  /**
   * Read a list of values from a string.
   *
   * Default is to use operator>> on a \c istringstream defined on the input
   * string. Print an error is the flux operator fails.
   *
   * \returns True if the reading succeeded
   */
  template <typename T> bool readValue(const QString& value, std::list<T>& variable);
  /**
   * Read a set of values from a string.
   *
   * Default is to use operator>> on a \c istringstream defined on the input
   * string. Print an error is the flux operator fails.
   *
   * \returns True if the reading succeeded
   */
  template <typename T> bool readValue(const QString& value, std::set<T>& variable);
  /**
   * Read a value from a string.
   *
   * Default is to use operator>> on a \c istringstream defined on the input
   * string. Print an error is the flux operator fails.
   *
   * \returns True if the reading succeeded
   */
  template <typename T> bool readValue(const QString& value, T& variable);

  template <typename T, typename InsertIterator> bool readContainer(const QString& value, InsertIterator container);

  /**
   * Check the existence of \c key.
   *
   * \param key [Section]key written as "section:key"
   */
  bool check(const QString& key);

  /**
   * Map of all the parameters in string form as read from the file. The
   * strings are already stripped from whitespaces and comments.
   */
  std::map<QString, QStringList> Parameters;

  /**
   * Current section while reading the parameter file
   */
  QString Section;

  /**
   * Current verbosity level
   */
  int VerboseLevel = 1;

  /**
   * Set to True if the existence of a key output an error. Used with the
   * default value to avoid printing an error in that case.
   */
  bool CheckExist = true;

  /**
   * Make sure the parameter file is correctly loaded
   */
  bool loaded = false;

  /**
   * String used to separate keys from values
   */
  QString separator = ":";

  /**
   * String used to start commends
   */
  QString comment_start = "//";
};

LGX_EXPORT Parms iniFile(int verboseLevel = 1);
LGX_EXPORT Parms iniFile(const QString& filename, int verboseLevel = 1);
LGX_EXPORT Parms iniFile(QIODevice* dev, int verboseLevel = 1);

template <typename T, typename InsertIterator> bool Parms::readContainer(const QString& value, InsertIterator it)
{
  QString val(value);
  QTextStream iss(&val, QIODevice::ReadOnly);
  while(!iss.atEnd() and iss.status() == QTextStream::Ok) {
    T v;
    iss >> v;
    *it++ = v;
  }
  return true;
}

template <typename T> bool Parms::readValue(const QString& value, std::vector<T>& variable)
{
  return readContainer<T>(value, std::back_insert_iterator<std::vector<T> >(variable));
}

template <typename T> bool Parms::readValue(const QString& value, std::list<T>& variable)
{
  return readContainer<T>(value, std::back_insert_iterator<std::list<T> >(variable));
}

template <typename T> bool Parms::readValue(const QString& value, std::set<T>& variable)
{
  return readContainer<T>(value, std::insert_iterator<std::set<T> >(variable, variable.end()));
}

template <typename T> bool Parms::readValue(const QString& value, T& variable)
{
  QString val(value);
  QTextStream iss(&val, QIODevice::ReadOnly);
  iss >> variable;
  return iss.status() == QTextStream::Ok;
}

template <typename T>
bool Parms::optional(const QString& section, const QString& key, T& value)
{
  bool found = true;
  CheckExist = false;
  if(not (*this)(section, key, value)) {
    found = false;
    if(VerboseLevel > 2)
      Information::err << "Parms::operator()::Info key [" << section << "]" << key << " not found" << endl;
  }
  CheckExist = true;
  return found;
}

template <typename T> bool Parms::operator()(const QString& section, const QString& key, T& value)
{
  QStringList values;
  if(!extractValues(section, key, values))
    return false;

  if((values.size() > 1) && (VerboseLevel > 1)) {
    Information::err << "Parms::operator():Warning multiple value for key [" << section << "]" << key
                     << ", last one used.\n" << flush;
  }

  if(!readValue(values.back(), value)) {
    if(VerboseLevel > 0) {
      Information::err << "Parms::operator():Error getting value for key [" << section << "]" << key << " value "
                       << values.back() << "\n" << flush;
    }
    return false;
  }
  return true;
}

template <typename T> bool Parms::operator()(const QString& section, const QString& key, T& value, const T& def)
{
  bool found = true;
  CheckExist = false;
  if(!(*this)(section, key, value)) {
    found = false;
    if(VerboseLevel > 2) {
      Information::err << "Parms::operator()::Info key [" << section << "]" << key
                       << " not found, using default value" << endl;
    }
    value = def;
  }
  CheckExist = true;
  return found;
}

template <typename Container> bool Parms::all(const QString& section, const QString& key, Container& value)
{
  bool valid = true;
  typedef typename Container::value_type T;
  CheckExist = false;
  QStringList values;
  if(!extractValues(section, key, values))
    return false;
  value.clear();
  std::insert_iterator<Container> it(value, value.end());
  forall(const QString& val, values) {
    T single_value;
    if(readValue(val, single_value)) {
      *it++ = (const T&)single_value;
    } else {
      if(VerboseLevel > 2) {
        Information::err << "Parms::all:Error reading key [" << section << "]" << key << " with value " << val
                         << "\n" << flush;
      }
      valid = false;
    }
  }
  CheckExist = true;
  return valid;
}

template <typename T, typename Container> bool Parms::all(const QString& section, std::map<QString, Container>& result)
{
  bool valid = true;
  CheckExist = false;
  typedef std::map<QString, QStringList>::value_type value_type;
  result.clear();
  int pos = section.size() + 1;
  forall(const value_type& pair, Parameters) {
    QString sec(pair.first.mid(0, pos - 1));
    if(sec != section)
      continue;
    QString key(pair.first.mid(pos));
    Container& value = result[key];
    value.clear();
    forall(const QString& val, pair.second) {
      T single_value;
      if(readValue(val, single_value)) {
        value.push_back(single_value);
      } else {
        if(VerboseLevel > 2) {
          Information::err << "Parms::all:Error reading key [" << section << "]" << key << " with value "
                           << val << endl;
        }
        valid = false;
      }
    }
  }
  CheckExist = true;
  return valid;
}
} // namespace util
} // namespace lgx
#endif // __PARMS_HPP__
