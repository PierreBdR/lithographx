/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef MESH_PROCESS_SEGMENTATION_HPP
#define MESH_PROCESS_SEGMENTATION_HPP

#include <Process.hpp>

#include <Mesh.hpp>
#include <Misc.hpp>

class Ui_LoadHeatMap;

namespace lgx {
namespace process {
///\addtogroup MeshProcess
///@{
/**
 * \class SegmentMesh MeshProcessSegmentation.hpp <MeshProcessSegmentation.hpp>
 *
 * Segment the mesh using a seeded watershed algorithm.
 *
 * The mesh is assumed to already contain seeds for the watershed. For proper
 * use on this process, you should read the protocols and manuals published.
 */
class LGXCORE_EXPORT SegmentMesh : public MeshProcess {
public:
  SegmentMesh(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  bool operator()(const ParmList& parms) override
  {
    if(!checkState().mesh())
      return false;
    return (*this)(currentMesh(), parms[0].toUInt(), currentMesh()->activeVertices());
  }

  bool operator()(Mesh* mesh, uint maxsteps, const std::vector<vertex>& to_segment);

  QString folder() const override {
    return "Segmentation";
  }
  QString name() const override {
    return "Watershed Segmentation";
  }
  QString description() const override {
    return "Propagate labels on mesh using the watershed algorithm.";
  }
  QStringList parmNames() const override {
    return QStringList() << "Steps";
  }
  QStringList parmDescs() const override
  {
    return QStringList() << "Number of propagation steps performed before updating display. "
                            "Increase value to run faster.";
  }
  ParmList parmDefaults() const override {
    return ParmList() << 20000;
  }
  QIcon icon() const override {
    return QIcon(":/images/SegmentMesh.png");
  }
};

/**
 * \class SegmentClear MeshProcessSegmentation.hpp <MeshProcessSegmentation.hpp>
 *
 * Remove any segmentation from the current mesh.
 */
class LGXCORE_EXPORT SegmentClear : public MeshProcess {
public:
  SegmentClear(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  bool operator()(const ParmList&) override
  {
    if(!checkState().mesh())
      return false;
    return (*this)(currentMesh());
  }

  bool operator()(Mesh* mesh);

  QString folder() const override {
    return "Segmentation";
  }
  QString name() const override {
    return "Segmentation Clear";
  }
  QString description() const override {
    return "Clean all mesh labels.";
  }
  QStringList parmNames() const override {
    return QStringList();
  }
  QStringList parmDescs() const override {
    return QStringList();
  }
  QIcon icon() const override {
    return QIcon(":/images/SegmentClear.png");
  }
};

/**
 * \class MeshRelabel MeshProcessSegmentation.hpp <MeshProcessSegmentation.hpp>
 *
 * Relabel a segmented mesh.
 *
 * This process will relabel all the mesh. The labels are assigned in an
 * arbitrary, non-random, manner.
 */
class LGXCORE_EXPORT MeshRelabel : public MeshProcess {
public:
  MeshRelabel(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  bool operator()(const ParmList& parms) override
  {
    if(!checkState().mesh(MESH_NON_EMPTY | MESH_LABEL))
      return false;
    Mesh* m = currentMesh();
    int start = parms[0].toInt();
    int step = parms[1].toInt();
    return (*this)(m, start, step);
  }

  bool operator()(Mesh* m, int start, int step);

  QString folder() const override {
    return "Segmentation";
  }
  QString name() const override {
    return "Relabel";
  }
  QString description() const override {
    return "Relabel a mesh randomly to use consecutive labels.";
  }
  QStringList parmNames() const override
  {
    return QStringList() << "Label Start"
                         << "Label Step";
  }
  QStringList parmDescs() const override
  {
    return QStringList() << "Starting label."
                         << "Increment step.";
  }
  ParmList parmDefaults() const override
  {
    return ParmList() << 1
                      << 1;
  }
  QIcon icon() const override {
    return QIcon(":/images/Relabel.png");
  }
};

/**
 * \class GrabLabelsSegment MeshProcessSegmentation.hpp <MeshProcessSegmentation.hpp>
 *
 * Grab labels from other mesh (3D meshes).
 */
class LGXCORE_EXPORT GrabLabelsSegment : public MeshProcess {
public:
  GrabLabelsSegment(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  bool operator()(const ParmList& parms) override
  {
    if(!checkState().mesh(MESH_NON_EMPTY, 0).mesh(MESH_NON_EMPTY, 1))
      return false;

    Mesh* mesh1, *mesh2;
    if(currentMesh() == mesh(0)) {
      mesh1 = mesh(0);
      mesh2 = mesh(1);
    } else if(currentMesh() == mesh(1)) {
      mesh2 = mesh(0);
      mesh1 = mesh(1);
    } else
      return false;

    bool res = (*this)(mesh1, mesh2, parms[0].toFloat());
    return res;
  }

  bool operator()(Mesh* mesh1, Mesh* mesh2, float tolerance);

  QString name() const override {
    return "3D Grab Labels";
  }
  QString description() const override {
    return "Grab labels from other mesh (3D meshes).";
  }
  QString folder() const override {
    return "Segmentation";
  }
  QStringList parmNames() const override {
    return QStringList() << QString("Tolerance (%1)").arg(UM);
  }
  QStringList parmDescs() const override {
    return QStringList() << "Maximal distance between matching cells. ";
  }
  ParmList parmDefaults() const override {
    return ParmList() << 5.0;
  }
  QIcon icon() const override {
    return QIcon(":/images/GrabLabels.png");
  }
};

/**
 * \class LabelSelected MeshProcessSegmentation.hpp <MeshProcessSegmentation.hpp>
 *
 * Change the label of the selected vertices.
 */
class LGXCORE_EXPORT LabelSelected : public MeshProcess {
public:
  LabelSelected(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  bool operator()(const ParmList& parms) override
  {
    if(!checkState().mesh(MESH_LABEL))
      return false;
    return (*this)(currentMesh(), parms[0].toInt());
  }

  bool operator()(Mesh* mesh, int label);

  QString folder() const override {
    return "Segmentation";
  }
  QString name() const override {
    return "Label Selected Vertices";
  }
  QString description() const override {
    return "Label selected vertices with the same value, which can be a new label";
  }
  QStringList parmNames() const override {
    return QStringList() << "Label";
  }
  QStringList parmDescs() const override {
    return QStringList() << "Asign this label to all selected vertices.";
  }
  ParmList parmDefaults() const override {
    return ParmList() << 0;
  }
  QIcon icon() const override {
    return QIcon(":/images/LabelSelected.png");
  }
};

/**
 * \class FloodFillSegmentMesh MeshProcessSegmentation.hpp <MeshProcessSegmentation.hpp>
 *
 * Flood fill the mesh, starting from a given vertex
 */
class LGXCORE_EXPORT FloodFillSegmentMesh : public MeshProcess {
public:
  FloodFillSegmentMesh(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  bool operator()(const ParmList& parms) override
  {
    if(!checkState().mesh(MESH_LABEL))
      return false;
    QString vs_str = parms[0].toString();
    QStringList vs_fields = util::splitFields(vs_str);
    std::vector<size_t> vs;
    vs.reserve(vs_fields.size());
    bool ok;
    for(QString& s: vs_fields) {
      size_t a = s.toUInt(&ok);
      if(not ok)
        return setErrorMessage("The 'Start vertex' parameter must be a list of positive integers.");
      vs.push_back(a);
    }
    int n = parms[1].toUInt(&ok);
    if(not ok)
        return setErrorMessage("The 'New label' parameter must be a positive integer.");
    return (*this)(currentMesh(), vs, n);
  }

  bool operator()(Mesh* mesh, const std::vector<size_t>& start_vertices, int new_label);

  QString folder() const override {
    return "Segmentation";
  }
  QString name() const override {
    return "Flood Fill Segmentation";
  }
  QString description() const override {
    return "Flood fill from the current position, replacing the label with a new one.\n"
           "Cells boundary will stop the flood fill, and the fill cannot replace a negative label.";
  }
  QStringList parmNames() const override {
    return QStringList() << "Start vertex" << "New label";
  }
  QStringList parmDescs() const override {
    return QStringList() << "Index of the vertex to start from. This may contain more than one vertex id."
                         << "Label to use";
  }
  ParmList parmDefaults() const override {
    return ParmList() << 0 << 0;
  }
  QIcon icon() const override {
    return QIcon(":/images/FillLabel.png");
  }
};

/**
 * \class ReplaceMeshLabel MeshProcessSegmentation.hpp <MeshProcessSegmentation.hpp>
 *
 * Flood fill the mesh, starting from a given vertex
 */
class LGXCORE_EXPORT ReplaceMeshLabel : public MeshProcess {
public:
  ReplaceMeshLabel(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  bool operator()(const ParmList& parms) override
  {
    if(!checkState().mesh(MESH_LABEL))
      return false;
    bool ok;
    uint old_label = parms[0].toUInt(&ok);
    if(not ok)
      return setErrorMessage("Old label parameter must be a positive integer");
    uint new_label = parms[1].toUInt(&ok);
    if(not ok)
      return setErrorMessage("New label parameter must be a positive integer");
    return (*this)(currentMesh(), old_label, new_label);
  }

  bool operator()(Mesh* mesh, int old_label, int new_label);

  QString folder() const override {
    return "Segmentation";
  }
  QString name() const override {
    return "Replace Mesh Label";
  }
  QString description() const override {
    return "Replace a label with another one.";
  }
  QStringList parmNames() const override {
    return QStringList() << "Old label" << "New label";
  }
  QStringList parmDescs() const override {
    return QStringList() << "Label to replace"
                         << "Label to put instead";
  }
  ParmList parmDefaults() const override {
    return ParmList() << 1 << 0;
  }
  QIcon icon() const override {
    return QIcon(":/images/FillLabel.png");
  }
};

/**
 * \class RelabelCells3D MeshProcessSegmentation.hpp <MeshProcessSegmentation.hpp>
 *
 * Relabel 3D cells. Each connected region will be given a unique label.
 */
class LGXCORE_EXPORT RelabelCells3D : public MeshProcess {
public:
  RelabelCells3D(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  bool operator()(const ParmList& parms) override
  {
    if(!checkState().mesh(MESH_NON_EMPTY))
      return false;
    Mesh* mesh = currentMesh();
    bool res = (*this)(mesh, parms[0].toInt(), parms[1].toInt());
    if(res)
      mesh->showLabel();
    return res;
  }

  bool operator()(Mesh* mesh, int label_start, int label_step);

  QString folder() const override {
    return "Segmentation";
  }
  QString name() const override {
    return "Relabel 3D Cells";
  }
  QString description() const override
  {
    return "Relabel 3D cells (connected regions)."
           " A start label of -1 is used to indicate continuing using current labels.";
  }
  QStringList parmNames() const override
  {
    return QStringList() << "Label start"
                         << "Label step";
  }
  QStringList parmDescs() const override
  {
    return QStringList() << "Starting label. Use -1 to continue from last current label."
                         << "Increment step.";
  }
  ParmList parmDefaults() const override
  {
    return ParmList() << -1
                      << 1;
  }
  QIcon icon() const override {
    return QIcon(":/images/CellFiles3D.png");
  }
};

/**
 * \class MeshCombineRegions MeshProcessSegmentation.hpp <MeshProcessSegmentation.hpp>
 *
 * Combine over-segmented regions, based on mesh signal.
 */
class LGXCORE_EXPORT MeshCombineRegions : public MeshProcess {
public:
  MeshCombineRegions(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  bool operator()(const ParmList& parms) override
  {
    if(!checkState().mesh(MESH_NON_EMPTY))
      return false;
    return (*this)(currentMesh(), parms[0].toFloat(), parms[1].toFloat());
  }

  bool operator()(Mesh* mesh, float borderDist, float Difference);

  QString folder() const override {
    return "Segmentation";
  }
  QString name() const override {
    return "Combine Labels";
  }
  QString description() const override {
    return "Combine over-segmented regions, based on mesh signal.";
  }
  QStringList parmNames() const override {
    return QStringList() << QString("Border Distance (%1)").arg(UM) << "Threshold";
  }
  QStringList parmDescs() const override
  {
    return QStringList() << "Half of the cell border width on the mesh."
                         << "Ratio of border signal over internal signal. If a border between 2 cells "
              "has a ratio below the threshold, the cells are fused.";
  }
  ParmList parmDefaults() const override
  {
    return ParmList() << 1
                      << 1.5;
  }
  QIcon icon() const override {
    return QIcon(":/images/Merge.png");
  }
};

/**
 * \class MeshAutoSegment MeshProcessSegmentation.hpp <MeshProcessSegmentation.hpp>
 *
 * Auto-Segmentation of the mesh surface based on signal. After clearing the
 * current segmentation, it combines blurring, auto-seeding, label propagation
 * by watershed and fusion of over-segmented cells.
 */
class LGXCORE_EXPORT MeshAutoSegment : public MeshProcess {
public:
  MeshAutoSegment(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  bool operator()(const ParmList& parms) override
  {
    if(!checkState().mesh(MESH_NON_EMPTY))
      return false;
    return (*this)(currentMesh(), parmToBool(parms[0]), parmToBool(parms[1]), parms[2].toFloat(),
                   parms[3].toFloat(), parms[4].toFloat(), parms[5].toFloat(), parms[6].toFloat(),
                   parms[7].toFloat());
  }

  bool operator()(Mesh* mesh, bool updateView, bool normalize, float gaussianRadiusCell, float localMinimaRadius,
                  float gaussianRadiusWall, float normalizeRadius, float borderDist, float threshold);

  QString folder() const override {
    return "Segmentation";
  }
  QString name() const override {
    return "Auto-Segmentation";
  }
  QString description() const override
  {
    return "Auto-Segmentation of the mesh surface based on signal. Combines blurring, auto-seeding, "
           "label propagation by watershed and fusion of over-segmented cells.";
  }
  QStringList parmNames() const override
  {
    return QStringList() << "Update"
                         << "Normalize" << QString("Blur Cell Radius (%1)").arg(UM)
                         << QString("Auto-Seed Radius (%1)").arg(UM) << QString("Blur Borders Radius (%1)").arg(UM)
                         << QString("Normalization Radius (%1)").arg(UM) << QString("Border Distance (%1)").arg(UM)
                         << "Combine Threshold";
  }
  QStringList parmDescs() const override
  {
    return QStringList()
           << "Option to update mesh drawing while running processes."
           << "Option to normalize mesh signal before merging the cells. Most usefull in case of low contrast mesh "
              "signal."
           << "Radius used for gaussian blur of mesh signal. In normal cases, should be equal to auto-seed radius."
           << "Radius used for auto-seeding the mesh, based on local minima of signal. Should be equal to radius "
              "of smallest cells."
           << "Radius used for gaussian blur of signal on cell borders before watershed segmentation. Use small "
              "values (roughly, half the border width) to avoid border signal distortion."
           << "Radius used for local normalization of signal. Roughly, radius of largest cells."
           << "Half of the cell border width after blurring. Used for fusion of over-segmented cells."
           << "Ratio of border signal over internal signal. If a borders between 2 cells have a ratio below the "
              "threshold, the cells are fused.";
  }
  ParmList parmDefaults() const override
  {
    return ParmList() << true
                      << true
                      << 2.0
                      << 2.0
                      << 1
                      << 5.0
                      << 1.0
                      << 1.8;
  }
  ParmChoiceMap parmChoice() const override
  {
    ParmChoiceMap map;
    map[0] = booleanChoice();
    map[1] = booleanChoice();
    return map;
  }
  QIcon icon() const override {
    return QIcon(":/images/SegmentMesh.png");
  }
};
///@}
} // namespace process
} // namespace lgx

#endif
