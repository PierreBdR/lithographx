/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef INFORMATION_H
#define INFORMATION_H

#include <LGXConfig.hpp>

#include <QEvent>
#include <QString>
#include <QTextStream>
#include <QMutex>

#ifdef LGX_CLI

#  define SETSTATUS(msg)                             \
    do {                                             \
      ::lgx::Information::out << msg;                \
    } while(false)

#else

class QMainWindow;

#  define SETSTATUS(msg)                             \
    do {                                             \
      QString __status_msg;                          \
      {                                              \
        QTextStream __status_msgstrm(&__status_msg); \
        __status_msgstrm << msg;                     \
      }                                              \
      ::lgx::Information::Print(__status_msg);       \
    } while(false)

#endif

#define DEBUG_OUT ::lgx::Information::out

#define DEBUG_OUTPUT(msg) \
  if(::lgx::DEBUG)        \
    do {                  \
      DEBUG_OUT << msg;   \
    } while(false)

namespace lgx {
LGX_EXPORT extern bool DEBUG;

namespace Information {

// write-only thread shafe text stream
LGX_EXPORT class ThreadSafeTextStream {
public:
  ThreadSafeTextStream(QTextStream& stream);
  ~ThreadSafeTextStream();

  template <typename T>
  ThreadSafeTextStream& write(const T& t) {
    QMutexLocker locker(&mutex);
    stream << t;
    return *this;
  }

  template <typename T>
  ThreadSafeTextStream& write(T&& t) {
    QMutexLocker locker(&mutex);
    stream << std::move(t);
    return *this;
  }

  QTextStream::Status status() const {
    return stream.status();
  }

  void setRealNumberNotation(QTextStream::RealNumberNotation notation);
  void setRealNumberPrecision(int precision);
  void setCodec(QTextCodec * codec);
  void setCodec(const char * codecName);
  void setFieldAlignment(QTextStream::FieldAlignment mode);
  void setFieldWidth(int width);
  void setGenerateByteOrderMark(bool generate);
  void setIntegerBase(int base);
  void setLocale(const QLocale & locale);
  void setNumberFlags(QTextStream::NumberFlags flags);
  void setPadChar(QChar ch);
  bool atEnd() const;
  QTextStream::FieldAlignment fieldAlignment() const;
  int fieldWidth() const;
  void flush();
  bool generateByteOrderMark() const;
  int integerBase() const;
  QLocale locale() const;
  QTextStream::NumberFlags numberFlags() const;
  QChar padChar() const;
  QTextStream::RealNumberNotation realNumberNotation() const;
  int realNumberPrecision() const;
  bool autoDetectUnicode() const;
  QTextCodec* codec() const;

private:
  QTextStream& stream;
  QMutex mutex;
};

LGX_EXPORT extern ThreadSafeTextStream out;
LGX_EXPORT extern ThreadSafeTextStream err;

struct LGX_EXPORT Event : public QEvent {
  Event(const QString& msg)
    : QEvent(QEvent::User)
    , message(msg)
  {
  }

  QString message;
};

template <typename T>
ThreadSafeTextStream& operator<<(ThreadSafeTextStream& stream, const T& t)
{
  return stream.write(t);
}

inline ThreadSafeTextStream& operator<<(ThreadSafeTextStream& stream, QTextStream& (*fct)(QTextStream&))
{
  return stream.write(fct);
}

inline ThreadSafeTextStream& operator<<(ThreadSafeTextStream& stream, QTextStreamManipulator&& manip)
{
  return stream.write(std::move(manip));
}

#ifndef LGX_CLI
LGX_EXPORT void setMainWindow(QMainWindow* wnd);
LGX_EXPORT void Print(const QString& _text);
LGX_EXPORT void Print(const std::string _text);
#endif
} // namespace Information
} // namespace lgx
#endif
