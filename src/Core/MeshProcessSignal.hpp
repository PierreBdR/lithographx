/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef MESH_PROCESS_SIGNAL_HPP
#define MESH_PROCESS_SIGNAL_HPP

#include <Process.hpp>

#include <Curvature.hpp>
#include <Mesh.hpp>

#include <vector>

class Ui_LoadHeatMap;

namespace lgx {
namespace process {
typedef std::unordered_map<int, HVec3U> IntHVec3UMap;

///\addtogroup MeshProcess
///@{
/**
 * \class ViewMeshProcess MeshProcessSignal.hpp <MeshProcessSignal.hpp>
 *
 * This process is used to change how the mesh is seen. Mostly useful to
 * write scripts taking screenshots.
 */
class LGXCORE_EXPORT ViewMeshProcess : public MeshProcess {
public:
  ViewMeshProcess(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  bool operator()(const ParmList& parms) override;

  QString folder() const override {
    return "System";
  }
  QString name() const override {
    return "View";
  }
  QString description() const override {
    return "Modify how the current mesh is viewed. Useful for scripts.";
  }

  QStringList parmNames() const override
  {
    return QStringList() << "Show Surface"
                         << "Surface Type"
                         << "Signal Type"
                         << "Blend"
                         << "Cull"
                         << "Show Mesh"
                         << "Mesh View"
                         << "Show Lines"
                         << "Show Points"
                         << "Show Map"
                         << "Scale"
                         << "Transform"
                         << "BBox"
                         << "Brightness"
                         << "Opacity";
  }

  QStringList parmDescs() const override
  {
    return QStringList() << "Draw mesh as a continuous surface."
                         << "Normal: show projected signal, Labels: color triangles according to assigned "
                            "labels, Parents: color triangles with parent labels, Heat: color triangles "
                            "with computed heat map (e.g. growth)"
                         << "Signal: show projected signal, Tex: Use 3D stack as texture, Img: Use 2D "
                            "texture (height map)."
                         << "Semi-transparent mesh, for example to superimpose to meshes or view the "
                            "stack through the mesh."
                         << "Draw the triangles (with signal or labels) only on the outside of the mesh."
                         << "Draw triangle edges and nodes"
                         << "All: draw all triangles. Border: draw outside edge of the mesh only. "
                            "Cells: draw cell outlines only. Selected: draw selected nodes only. "
                         << "Show connecting lines between nodes in the mesh."
                         << "Show mesh nodes."
                         << "Mapping of text on the labels (e.g. label number)"
                         << "Change scaling of mesh/stacks, independently in 3 directions (x,y,z). NB: "
                            "a stack saved with 'Scale' turned on will have a modified voxel size, "
                            "while saved meshes are unaffected. "
                         << "Apply rotation and translation to the mesh/stack."
                         << "Display the bounding box (i.e. total size) of a stack."
                         << "Brightness of signal, labels or heat"
                         << "Opacity of signal, labels or heat";
  }

  ParmList parmDefaults() const override
  {
    return ParmList() << ""
                      << ""
                      << ""
                      << ""
                      << ""
                      << ""
                      << ""
                      << ""
                      << ""
                      << ""
                      << ""
                      << ""
                      << ""
                      << ""
                      << "";
  }

  ParmChoiceMap parmChoice() const override
  {
    QStringList bools = QStringList() << "" << booleanChoice();
    ParmChoiceMap map;
    map[0] = bools;
    map[1] = QStringList() << ""
                           << "Normal"
                           << "Heat"
                           << "Label"
                           << "Parents";
    map[2] = QStringList() << ""
                           << "Signal"
                           << "Texture"
                           << "Image";
    map[3] = map[4] = map[5] = bools;
    map[6] = QStringList() << ""
                           << "All"
                           << "Border"
                           << "Cells"
                           << "Selected";
    map[7] = map[8] = map[9] = map[10] = map[11] = map[12] = bools;
    return map;
  }

  QIcon icon() const override {
    return QIcon(":/images/Palette.png");
  }
};

/**
 * \class ProjectSignal MeshProcessSignal.hpp <MeshProcessSignal.hpp>
 *
 * Project stack signal onto the current mesh.
 */
class LGXCORE_EXPORT ProjectSignal : public MeshProcess {
public:
  ProjectSignal(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  bool operator()(const ParmList& parms) override
  {
    if(!checkState().store(STORE_NON_LABEL).mesh(MESH_NON_EMPTY))
      return false;
    float minDist, maxDist, minSignal = 0.f, maxSignal = 0.f;
    bool ok;
    minDist = parms[0].toFloat(&ok);
    if(not ok) return setErrorMessage("Parameter 'Min Dist' must be a number");
    maxDist = parms[1].toFloat(&ok);
    if(not ok) return setErrorMessage("Parameter 'Max Dist' must be a number");
    bool useMinSignal = not parms[4].toString().isEmpty();
    if(useMinSignal) {
      minSignal = parms[4].toFloat(&ok);
      if(not ok) return setErrorMessage("Parameter 'Min Signal' must be empty, or a number");
    }
    bool useMaxSignal = not parms[5].toString().isEmpty();
    if(useMaxSignal) {
      maxSignal = parms[5].toFloat(&ok);
      if(not ok) return setErrorMessage("Parameter 'Max Signal' must be empty, or a number");
    }
    Mesh *mesh = currentMesh();
    bool res = (*this)(currentStack()->currentStore(),
                       mesh,
                       minDist, maxDist,
                       parmToBool(parms[2]), parmToBool(parms[3]),
                       useMinSignal, useMaxSignal,
                       minSignal, maxSignal);
    if(res) {
      mesh->showSignal();
      mesh->showNormal();
    }
    return res;
  }

  bool operator()(const Store* store, Mesh* mesh,
                  float minDist, float maxDist,
                  bool averageSignal, bool projectPerCell,
                  bool useMinSignal, bool useMaxSignal,
                  float absSignalMin, float absSignalMax);

  QString folder() const override {
    return "Signal";
  }
  QString name() const override {
    return "Project Signal";
  }
  QString description() const override {
    return "Project signal onto mesh, perpendicular to its curved surface.";
  }
  QStringList parmNames() const override
  {
    return QStringList() << QString("Min Dist (%1)").arg(UM)
                         << QString("Max Dist (%1)").arg(UM)
                         << "Average Signal"
                         << "Project per cell"
                         << "Min Signal"
                         << "Max Signal";
  }
  QStringList parmDescs() const override
  {
    return QStringList() << "Distance (triangle-voxel) above which the signal is projected."
                         << "Maximal distance (triangle-voxel) used for signal projection."
                         << "If true, compute the average signal along the projection line.\n"
                            "If false, compute the integral of the signal along the line."
                         << "If true, projects perpendicular to the plane fitting the cell.\n"
                            "A cell is a group of vertices with same label > 0."
                         << "Lower bound of signal value, if not empty"
                         << "Upper bound of projected signal value, if not empty";
  }
  ParmList parmDefaults() const override
  {
    return ParmList() << 2.0
                      << 6.0
                      << false
                      << false
                      << ""
                      << "";
  }
  ParmChoiceMap parmChoice() const override
  {
    ParmChoiceMap map;
    map[2] = booleanChoice();
    map[3] = booleanChoice();
    return map;
  }
  QIcon icon() const override {
    return QIcon(":/images/ProjectColor.png");
  }
};

/**
 * \class SmoothMeshSignal MeshProcessSignal.hpp <MeshProcessSignal.hpp>
 *
 * Smooth the mesh signal using local averaging.
 */
class LGXCORE_EXPORT SmoothMeshSignal : public MeshProcess {
public:
  SmoothMeshSignal(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  bool operator()(const ParmList& parms) override
  {
    if(!checkState().mesh(MESH_NON_EMPTY))
      return false;
    return (*this)(currentMesh(), parms[0].toUInt());
  }

  bool operator()(Mesh* mesh, uint passes);

  QString folder() const override {
    return "Signal";
  }
  QString name() const override {
    return "Smooth Mesh Signal";
  }
  QString description() const override {
    return "Averages the signal of each node, based on its immediate neighbors.";
  }
  QStringList parmNames() const override {
    return QStringList() << "Passes";
  }
  QStringList parmDescs() const override {
    return QStringList() << "Number of smoothing iterations.";
  }
  ParmList parmDefaults() const override {
    return ParmList() << 3;
  }
  QIcon icon() const override {
    return QIcon(":/images/SmoothColor.png");
  }
};

/**
 * \class ClearMeshSignal MeshProcessSignal.hpp <MeshProcessSignal.hpp>
 *
 * Clear the mesh signal to a defined value.
 */
class LGXCORE_EXPORT ClearMeshSignal : public MeshProcess {
public:
  ClearMeshSignal(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  bool operator()(const ParmList& parms) override
  {
    if(!checkState().mesh(MESH_NON_EMPTY))
      return false;
    return (*this)(currentMesh(), parms[0].toUInt());
  }

  bool operator()(Mesh* mesh, uint value);

  QString folder() const override {
    return "Signal";
  }
  QString name() const override {
    return "Clear Mesh Signal";
  }
  QString description() const override {
    return "Erase the signal on the mesh";
  }
  QStringList parmNames() const override {
    return QStringList() << "Value";
  }
  QStringList parmDescs() const override {
    return QStringList() << "Assign this signal value to the entire mesh.";
  }
  ParmList parmDefaults() const override {
    return ParmList() << 50000;
  }
  QIcon icon() const override {
    return QIcon(":/images/ClearSignal.png");
  }
};

/**
 * \class ProjectCurvature MeshProcessSignal.hpp <MeshProcessSignal.hpp>
 *
 * Set the mesh signal to be the curvature of the mesh. The process can also
 * output a CSV file with the full curvature tensor for each vertex.
 */
class LGXCORE_EXPORT ProjectCurvature : public MeshProcess {
public:
  ProjectCurvature(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  bool operator()(const ParmList& parms) override
  {
    if(!checkState().mesh(MESH_NON_EMPTY))
      return false;
    bool ok;
    float neighborhood = parms[2].toFloat(&ok);
    if(not ok)
      return setErrorMessage("Error, parameter 'Neighborhood' must be a number");
    float mincurv = parms[4].toFloat(&ok);
    if(not ok)
      return setErrorMessage("Error, parameter 'Min Curv' must be a number");
    float maxcurv = parms[5].toFloat(&ok);
    if(not ok)
      return setErrorMessage("Error, parameter 'Max Curv' must be a number");
    float percentile = parms[6].toFloat(&ok);
    if(not ok)
      return setErrorMessage("Error, parameter 'Percentile' must be a number");
    return (*this)(currentMesh(), parms[0].toString(), parms[1].toString(), neighborhood, parmToBool(parms[3]), mincurv, maxcurv,
                   percentile);
  }

  bool operator()(Mesh* mesh, QString output, QString type, float neighborhood, bool auto_scale, float mincurv,
                  float maxcurv, int percentile);

  QString folder() const override {
    return "Signal";
  }
  QString name() const override {
    return "Project Mesh Curvature";
  }
  QString description() const override
  {
    return "Compute curvature at each node of the mesh, for a given neighborhood size. "
           "Curvature values are stored as signal.";
  }
  QStringList parmNames() const override
  {
    return QStringList() << "Output"
                         << "Type" << QString("Neighborhood (%1)").arg(UM) << "AutoScale"
                         << "Min Curv"
                         << "Max Curv"
                         << "Autoscale percentile";
  }
  QStringList parmDescs() const override
  {
    return QStringList() << "Name of output file, if desired."
                         << "Minimal = minCurv, Maximal = maxCurv, Gaussian = maxCurv * minCurv, "
              "SumSquare = maxCurv^2 + minCurv^2, Average = (maxCurv + minCurv)/2, "
              "SignedAverageAbs = sign(max or min) x (abs(maxCurv) + abs(minCurv))/2"
                         << QString("Neighborhood (%1)").arg(UM)
                         << "Clip max and min signal range according to curvature distribution"
                         << "Minimal curvature value displayed"
                         << "Maximal curvature value displayed"
                         << "Auto-scale signal range based on curvature percentile";
  }
  ParmList parmDefaults() const override
  {
    return ParmList() << ""
                      << "Gaussian"
                      << 3.0
                      << true
                      << -50.0
                      << 50.0
                      << 85;
  }
  ParmChoiceMap parmChoice() const override
  {
    ParmChoiceMap map;
    map[1] = QStringList() << "Minimal"
                           << "Maximal"
                           << "Gaussian"
                           << "SumSquare"
                           << "Average"
                           << "SignedAverageAbs";
    map[3] = booleanChoice();
    return map;
  }
  QIcon icon() const override {
    return QIcon(":/images/Curvature.png");
  }
};

/**
 * \class MeshGaussianBlur MeshProcessSignal.hpp <MeshProcessSignal.hpp>
 *
 * Smooth the mesh signal using a gaussian kernel.
 */
class LGXCORE_EXPORT MeshGaussianBlur : public MeshProcess {
public:
  MeshGaussianBlur(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  bool operator()(const ParmList& parms) override
  {
    if(!checkState().mesh(MESH_NON_EMPTY))
      return false;
    return (*this)(currentMesh(), parms[0].toFloat());
  }

  bool operator()(Mesh* mesh, float radius);

  QString folder() const override {
    return "Signal";
  }
  QString name() const override {
    return "Gaussian Blur";
  }
  QString description() const override {
    return "Apply Gaussian Blur to mesh signal";
  }
  QStringList parmNames() const override {
    return QStringList() << QString("Radius (%1)").arg(UM);
  }
  QStringList parmDescs() const override
  {
    return QStringList() << "Size of neighborhood used for Gaussian blur. The blur function standard "
                            "deviation is given by sigma = radius/2. ";
  }
  ParmList parmDefaults() const override {
    return ParmList() << 2.0;
  }
  QIcon icon() const override {
    return QIcon(":/images/Blur.png");
  }
};

/**
 * \class MeshLocalMinima MeshProcessSignal.hpp <MeshProcessSignal.hpp>
 *
 * Find the local minima in the current mesh.
 */
class LGXCORE_EXPORT MeshLocalMinima : public MeshProcess {
public:
  MeshLocalMinima(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  bool operator()(const ParmList& parms) override
  {
    if(!checkState().mesh(MESH_NON_EMPTY))
      return false;
    return (*this)(currentMesh(), parms[0].toFloat());
  }

  bool operator()(Mesh* mesh, float radius);

  QString folder() const override {
    return "Segmentation";
  }
  QString name() const override {
    return "Auto-Seeding";
  }
  QString description() const override {
    return "Put a seed at local minima of mesh signal.";
  }
  QStringList parmNames() const override {
    return QStringList() << QString("Radius (%1)").arg(UM);
  }
  QStringList parmDescs() const override
  {
    return QStringList() << "Size of neighborhood used for search of local minima. Typically, "
                            "the radius of smallest cells in the sample.";
  }
  ParmList parmDefaults() const override {
    return ParmList() << 3.0;
  }
  QIcon icon() const override {
    return QIcon(":/images/LocalMinima.png");
  }
};

/**
 * \class MeshNormalize MeshProcessSignal.hpp <MeshProcessSignal.hpp>
 *
 * Local normalization of the mesh signal.
 */
class LGXCORE_EXPORT MeshNormalize : public MeshProcess {
public:
  MeshNormalize(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  bool operator()(const ParmList& parms) override
  {
    if(!checkState().mesh(MESH_NON_EMPTY))
      return false;
    return (*this)(currentMesh(), parms[0].toFloat());
  }

  bool operator()(Mesh* mesh, float radius);

  QString folder() const override {
    return "Signal";
  }
  QString name() const override {
    return "Normalize Signal";
  }
  QString description() const override {
    return "Normalize mesh signal locally.";
  }
  QStringList parmNames() const override {
    return QStringList() << QString("Radius (%1)").arg(UM);
  }
  QStringList parmDescs() const override {
    return QStringList() << "Size of neighborhood used for normalization.";
  }
  ParmList parmDefaults() const override {
    return ParmList() << 5.0;
  }
  QIcon icon() const override {
    return QIcon(":/images/Normalize.png");
  }
};

/**
 * \class MeshDilation MeshProcessSignal.hpp <MeshProcessSignal.hpp>
 *
 * Lithological dilation on the mesh, using a spherical kernel. This process
 * replace the signal intensity on each vertex with the maximum within a
 * given distance.
 */
class LGXCORE_EXPORT MeshDilation : public MeshProcess {
public:
  MeshDilation(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  bool operator()(const ParmList& parms) override
  {
    if(!checkState().mesh(MESH_NON_EMPTY))
      return false;
    return (*this)(currentMesh(), parms[0].toFloat());
  }

  bool operator()(Mesh* mesh, float radius);

  QString folder() const override {
    return "Signal";
  }
  QString name() const override {
    return "Dilate Signal";
  }
  QString description() const override {
    return "Lithological dilation of signal on mesh.";
  }
  QStringList parmNames() const override {
    return QStringList() << QString("Radius (%1)").arg(UM);
  }
  QStringList parmDescs() const override {
    return QStringList() << "Size of neighborhood used for dilation.";
  }
  ParmList parmDefaults() const override {
    return ParmList() << 1.0;
  }
  QIcon icon() const override {
    return QIcon(":/images/Dilate.png");
  }
};

/**
 * \class MeshErosion MeshProcessSignal.hpp <MeshProcessSignal.hpp>
 *
 * Lithological erosion on the mesh, using a spherical kernel. This process
 * replace the signal intensity on each vertex with the minimum within a
 * given distance.
 */
class LGXCORE_EXPORT MeshErosion : public MeshProcess {
public:
  MeshErosion(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  bool operator()(const ParmList& parms) override
  {
    if(!checkState().mesh(MESH_NON_EMPTY))
      return false;
    return (*this)(currentMesh(), parms[0].toFloat());
  }

  bool operator()(Mesh* mesh, float radius);

  QString folder() const override {
    return "Signal";
  }
  QString name() const override {
    return "Erode Signal";
  }
  QString description() const override {
    return "Apply morphological erosion to mesh signal (opposite to Dilate Signal).";
  }
  QStringList parmNames() const override {
    return QStringList() << QString("Radius (%1)").arg(UM);
  }
  QStringList parmDescs() const override {
    return QStringList() << "Size of neighborhood used for erosion.";
  }
  ParmList parmDefaults() const override {
    return ParmList() << 1.0;
  }
  QIcon icon() const override {
    return QIcon(":/images/Erode.png");
  }
};

/**
 * \class MeshClosing MeshProcessSignal.hpp <MeshProcessSignal.hpp>
 *
 * Lithological closing on the mesh. This process is equivalent to call the
 * MeshDilation followed by MeshErosion with the same size.
 */
class LGXCORE_EXPORT MeshClosing : public MeshProcess {
public:
  MeshClosing(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  bool operator()(const ParmList& parms) override
  {
    if(!checkState().mesh(MESH_NON_EMPTY))
      return false;
    return (*this)(currentMesh(), parms[0].toFloat());
  }

  bool operator()(Mesh* mesh, float radius);

  QString folder() const override {
    return "Signal";
  }
  QString name() const override {
    return "Close Signal";
  }
  QString description() const override {
    return "Apply morphological dilation followed by erosion to mesh signal.";
  }
  QStringList parmNames() const override {
    return QStringList() << QString::fromWCharArray(L"Radius (\xb5m)");
  }
  QStringList parmDescs() const override {
    return QStringList() << "Size of neighborhood used for dilation/erosion.";
  }
  ParmList parmDefaults() const override {
    return ParmList() << 1.0;
  }
  QIcon icon() const override {
    return QIcon(":/images/Closing.png");
  }
};

/**
 * \class MeshOpening MeshProcessSignal.hpp <MeshProcessSignal.hpp>
 *
 * Lithological opening on the mesh. This process is equivalent to call the
 * MeshErosion followed by MeshDilation with the same size.
 */
class LGXCORE_EXPORT MeshOpening : public MeshProcess {
public:
  MeshOpening(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  bool operator()(const ParmList& parms) override
  {
    if(!checkState().mesh(MESH_NON_EMPTY))
      return false;
    return (*this)(currentMesh(), parms[0].toFloat());
  }

  bool operator()(Mesh* mesh, float radius);

  QString folder() const override {
    return "Signal";
  }
  QString name() const override {
    return "Open Signal";
  }
  QString description() const override {
    return "Apply morphological erosion followed by dilation to mesh signal.";
  }
  QStringList parmNames() const override {
    return QStringList() << QString::fromWCharArray(L"Radius (\xb5m)");
  }
  QStringList parmDescs() const override {
    return QStringList() << "Size of neighborhood used for erosion/dilation.";
  }
  ParmList parmDefaults() const override {
    return ParmList() << 1.0;
  }
  QIcon icon() const override {
    return QIcon(":/images/Opening.png");
  }
};

class LGXCORE_EXPORT RescaleSignal : public MeshProcess {
public:
  RescaleSignal(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  bool operator()(const ParmList& parms) override
  {
    if(!checkState().mesh(MESH_NON_EMPTY))
      return false;
    bool ok;
    float percentile = parms[1].toFloat(&ok);
    if(!ok)
      return setErrorMessage("Error, argument 'Percentile' must be a number");
    float minimum = parms[2].toFloat(&ok);
    if(!ok)
      return setErrorMessage("Error, argument 'Minimum' must be a number");
    float maximum = parms[3].toFloat(&ok);
    if(!ok)
      return setErrorMessage("Error, argument 'Maximum' must be a number");
    return (*this)(currentMesh(), parmToBool(parms[0]), percentile, minimum, maximum);
  }

  bool operator()(Mesh* mesh, bool use_zero, float percentile, float minimum, float maximum);

  QString folder() const override {
    return "Signal";
  }
  QString name() const override {
    return "Rescale Signal";
  }
  QString description() const override
  {
    return "Change the colorbar of the signal.\n"
           "If percentile is set to 0, it uses the minimum and maximum arguments.";
  }
  QStringList parmNames() const override
  {
    return QStringList() << "Zero as reference"
                         << "Percentile"
                         << "Minimum"
                         << "Maximum";
  }
  QStringList parmDescs() const override
  {
    return QStringList()
           << "If true, 0 will be used as a reference.\n"
              "If the signal is all positive (resp. negative), 0 will be added as a minimum (resp. maximum).\n"
              "If the signal is both positive and negative, 0 will be place at the center of the range"
           << "Keep only this percentage of the signal to compute the range."
           << "If the percentile specified is 0, uses this as the minimum value for the range"
           << "If the percentile specified is 0, uses this as the maximum value for the range";
  }
  ParmList parmDefaults() const override
  {
    return ParmList() << false
                      << 99
                      << 0
                      << 1;
  }

  ParmChoiceMap parmChoice() const override
  {
    ParmChoiceMap map;
    map[0] = booleanChoice();
    return map;
  }

  QIcon icon() const override {
    return QIcon(":/images/Normalize.png");
  }
};

/**
 * \class SetSignalProcess MeshProcessSignal.hpp <MeshProcessSignal.hpp>
 *
 * Set the signal to a given value for all currently active vertices.
 */
class LGXCORE_EXPORT SetSignalProcess : public MeshProcess {
public:
  SetSignalProcess(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  bool operator()(const ParmList& parms) override
  {
    if(not checkState().mesh(MESH_VISIBLE))
      return false;
    bool ok;
    float value = parms[0].toFloat(&ok);
    if(not ok)
      return setErrorMessage("Parameter 'Value' must be a number");
    float percentile = parms[2].toFloat(&ok);
    if(not ok)
      return setErrorMessage("Parameter 'Percentile' must be a number");
    return (*this)(currentMesh(), value, parmToBool(parms[1]), percentile, parmToBool(parms[3]));
  }

  bool operator()(Mesh* m, float value, bool rescale, float percentile, bool use_zero);

  QString folder() const override {
    return "Signal";
  }
  QString name() const override {
    return "Set Signal";
  }
  QString description() const override
  {
    return "Set the signal for the whole mesh, or for the currently selected part of it.";
  }

  QStringList parmNames() const override
  {
    return QStringList() << "Value"
                         << "Rescale"
                         << "Percentile"
                         << "Use zero";
  }
  QStringList parmDescs() const override
  {
    return QStringList() << "New value for the signal"
                         << "If true, the signal bounds will be rescaled"
                         << "If rescaling, which percentile to use?"
                         << "If rescaling, should we use zero as reference?";
  }

  ParmList parmDefaults() const override
  {
    return ParmList() << 1
                      << true
                      << 100
                      << false;
  }
  ParmChoiceMap parmChoice() const override
  {
    ParmChoiceMap map;
    map[1] = booleanChoice();
    map[3] = booleanChoice();
    return map;
  }

  QIcon icon() const override {
    return QIcon(":/images/Sharpen.png");
  }
};

///@}

class LGXCORE_EXPORT ProjectCurvatureOp {
public:
  ProjectCurvatureOp(const vvgraph& S, const VtxVec& vs, std::string type, float radius, FloatVec& values,
                     std::vector<util::Curvature>& curvs);
  void operator()(int i);

private:
  const vvgraph& S;
  const VtxVec& vs;
  std::string type;
  float radius;
  FloatVec& values;
  std::vector<util::Curvature>& curvs;
};

class LGXCORE_EXPORT MeshGaussianBlurOp {
public:
  MeshGaussianBlurOp(const vvgraph& S, float radius);
  bool operator()(const vertex& v);

private:
  const vvgraph& S;
  float radius;
};

class LGXCORE_EXPORT MeshLocalMinimaOp {
public:
  MeshLocalMinimaOp(const vvgraph& S, const VtxVec& vs, float radius, int startLabel);
  bool operator()(int i);

private:
  const vvgraph& S;
  const VtxVec& vs;
  float radius;
  int label;
};

class LGXCORE_EXPORT MeshNormalizeOp {
public:
  MeshNormalizeOp(const vvgraph& S, float radius);

  bool operator()(const vertex& v);

private:
  const vvgraph& S;
  float radius;
};

class LGXCORE_EXPORT MeshDilationOp {
public:
  MeshDilationOp(const vvgraph& S, float radius);
  bool operator()(const vertex& v);

private:
  const vvgraph& S;
  float radius;
};

class LGXCORE_EXPORT MeshErosionOp {
public:
  MeshErosionOp(const vvgraph& S, float radius);
  bool operator()(const vertex& v);

private:
  const vvgraph& S;
  float radius;
};
} // namespace process
} // namespace lgx

#endif
