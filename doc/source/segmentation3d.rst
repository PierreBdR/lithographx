**********************
Segmenting Cells in 3D
**********************

.. figure:: images/segmentation-3d-cover.*
    :alt: Shoot apical meristem of A. thaliana, segmented in 3D
    :align: center
    :figwidth: 85%
    :width: 100%

    Shoot apical meristem of *A. thaliana*, segmented in 3D

.. _dataset: http://www.botany.unibe.ch/deve/lithographx/Meristem3D.zip

In this section, we are going to see how we can extract the 3D shape of cells in
images of the shoot apical meristem of *Arabidopsis thaliana*, and see how we
can use an extra channel with the segmentation. To start, download this
`dataset`_, provided by Dr. Agata Burian.

Loading the data
================

For this tutorial, you can start either by loading the file
``Arabido_DR5-YFP_pi.tif`` or ``Arabido_DR5-YFP_pi_cleaned.tif``.

Cleaning the data
=================

If you have loaded ``Arabido_DR5-YFP_pi.tif``, you will see in addition to the
shoot apical meristem a few lateral organs, including those partially removed to
acquire the image. To extract only what we want to see, we need to clean the
image.

#. Copy the stack to the work store. At the same time, we will autoscale the
   stack's intensity. This is an important step as it makes to rest of the
   process much easier.

    .. LGXProcess::
        :name: Autoscale Stack
        :folder: Filters
        :type: Stack

#. Select the :ref:`pixel edit tool <pixel-edit-tool>`. In the :ref:`view-tab`
   make sure ``Fill`` checkbox in the :ref:`view-tab-stack-edit` area is not
   checked. Then, pressing the Alt key, you should see a circle above the
   drawing area. This circle marks the cylinder that will get erased. Click to
   erase.
#. When you are happy with your cleaning, save the stack!

.. note::

    If the area to erase is too large, nothing will change until you
    release the mouse button.

.. hint::

    You can also do the cleaning on the segmented stack. In this case, another
    useful tool is the :ref:`fill volume tool <fill-volume-tool>`. Ensure no
    label is currently selected by clicking on the :ref:`current label
    <current-label-button>` button. Then click on the labels you want to erase.

3D Cells Segmentation
=====================

#. Make sure the stack is in the Main store.
#. Blur the stack

    .. LGXProcess::
        :name: Gaussian Blur Stack
        :folder: Filters
        :type: Stack

        X Sigma (μm): 0.3
        Y Sigma (μm): 0.3
        Z Sigma (μm): 0.3

#. Optionnally, use the Sieve filter. It can be quite slow, but it really
   improves the segmentation.


    .. LGXProcess::
      :name: SieveFilter
      :folder: Morphology
      :type: Stack

      Type: Median
      Size (μm²/μm³): 4

#. Segment the stack:

    .. LGXProcess::
        :name: ITK Watershed Auto Seeded
        :folder: ITK/Segmentation
        :type: Stack

        Level: 1500

#. Remove external cells:

    .. LGXProcess::
        :name: Erase at Border
        :folder: Segmentation
        :type: Stack

        Distance (μm): 0
        X: Yes
        Y: Yes
        Z: Yes

#. Extract the cell shapes:

    .. LGXProcess::
        :name: Marching Cubes 3D
        :folder: Creation
        :type: Mesh

        Cube Size (μm): 1
        Smooth Passes: 3

.. hint::

    Use the clipping planes to scan through the volume and make sure the
    segmentation worked correctly in the depth of the tissue.

3D Signal Quantification
========================

.. figure:: images/segmentation-3d-quant.*
    :alt: Signal quantification on 3D mesh
    :align: center
    :figwidth: 75%
    :width: 100%

    Signal quantification on 3D mesh

#. Load the YFP channel in the work store of the stack 1. To do that, the
   simplest is to drag&drop the file onto the :ref:`work store area <main-tab>`.
#. Launch the ``Heat map`` mesh process:

    .. LGXProcess::
        :name: Heat Map
        :folder: Heat Map
        :type: Mesh

#. In the dialog box, select the ``Volume`` heat map type and the
   ``Total signal`` visualization and make sure ``Signal average`` is not ticked.

The result is, per cell, the total amount of signal present in the YFP channel.

.. note::

    This is **not** sufficient for a proper signal quantification. This is only
    qualitative. For a proper quantification of the signal you need a reference
    channel.
