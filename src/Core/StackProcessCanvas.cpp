/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "StackProcessCanvas.hpp"
#include "Image.hpp"

#include "CImg.h"

#include <string.h>

using namespace cimg_library;

typedef CImg<ushort> CImgUS;

namespace lgx {
namespace process {

bool ResizeCanvas::operator()(Stack* stack, bool isRelative, bool center, Point3i ds)
{
  Point3u size = stack->size();

  HVecUS& MainData = stack->main()->data();
  HVecUS& WorkData = stack->work()->data();

  if(isRelative)
    size += ds;
  else
    for(int i = 0; i < 3; ++i)
      if(ds[i] > 0)
        size[i] = uint(ds[i]);

  {
    HVecUS mi = resize(MainData, stack->size(), size, center);
    MainData.swap(mi);
  }
  {
    HVecUS wi = resize(WorkData, stack->size(), size, center);
    WorkData.swap(wi);
  }
  stack->setSize(size);

  stack->main()->changed();
  stack->work()->changed();

  return true;
}

REGISTER_STACK_PROCESS(ResizeCanvas);

bool ScaleStack::operator()(Stack* stack, Point3f newsize, bool percent)
{
  if(newsize.x() <= 0 and newsize.y() <= 0.0 and newsize.z() <= 0) {
    setErrorMessage("Error, no rescale specified.");
    return false;
  }
  Point3u size = stack->size();
  Point3f step = stack->step();

  HVecUS& MainData = stack->main()->data();
  HVecUS& WorkData = stack->work()->data();

  CImgUS mainImage(MainData.data(), size.x(), size.y(), size.z(), 1, false);
  CImgUS workImage(WorkData.data(), size.x(), size.y(), size.z(), 1, false);

  if(percent)
    for(int i = 0; i < 3; i++)
      newsize[i] *= size[i] / 100.0;

  for(int i = 0; i < 3; i++)
    if(newsize[i] > 0) {
      step[i] *= size[i] / newsize[i];
      size[i] = newsize[i];
    }

  mainImage.resize(size.x(), size.y(), size.z(), 1, 5);
  workImage.resize(size.x(), size.y(), size.z(), 1, 5);

  stack->setSize(size);
  stack->setStep(step);

  memcpy(MainData.data(), mainImage.data(), MainData.size() * 2);
  memcpy(WorkData.data(), workImage.data(), WorkData.size() * 2);

  stack->main()->changed();
  stack->work()->changed();

  return true;
}

REGISTER_STACK_PROCESS(ScaleStack);

bool ShiftStack::operator()(Stack* stack, bool origin, Point3i ds)
{
  Point3u size = stack->size();

  Store* main = stack->main();
  Store* work = stack->work();

  HVecUS& MainData = main->data();
  HVecUS& WorkData = work->data();

  if(origin)
    stack->setOrigin(stack->origin() + ds);
  else {
    CImgUS mainImage(MainData.data(), size.x(), size.y(), size.z(), 1, true);
    mainImage.shift(ds.x(), ds.y(), ds.z(), 0, 0);

    CImgUS workImage(WorkData.data(), size.x(), size.y(), size.z(), 1, true);
    workImage.shift(ds.x(), ds.y(), ds.z(), 0, 0);
  }

  main->changed();
  work->changed();
  return true;
}

REGISTER_STACK_PROCESS(ShiftStack);

bool ChangeVoxelSize::operator()(Stack* stack, Point3f nv)
{
  if(nv.x() <= 0)
    nv.x() = stack->step().x();
  if(nv.y() <= 0)
    nv.y() = stack->step().y();
  if(nv.z() <= 0)
    nv.z() = stack->step().z();
  Point3f ratio = nv / stack->step();
  stack->setStep(nv);
  stack->setOrigin(multiply(stack->origin(), ratio));
  stack->main()->changed();
  stack->work()->changed();
  return true;
}

REGISTER_STACK_PROCESS(ChangeVoxelSize);

bool ReverseStack::operator()(Store* output, const Store* input, bool rx, bool ry, bool rz)
{
  const Stack* stack = output->stack();
  Point3u size = stack->size();
  if(input == output) {
    HVecUS& data = output->data();
    if(rx and ry and rz) {
      ushort* s = data.data();
      ushort* e = s + data.size() - 1;
      uint nb_iter = data.size() >> 1;
      for(uint i = 0; i < nb_iter; ++i) {
        std::swap(*e--, *s++);
      }
      output->changed();
    } else if(rx xor ry xor rz)     // Only one if true
    {
      int dx = 1, dy = 1, dz = 1;
      Point3u start;
      if(rx) {
        dx = -1;
        start.x() = size.x() - 1;
        size.x() >>= 1;
      } else if(ry) {
        dy = -1;
        start.y() = size.y() - 1;
        size.y() >>= 1;
      } else {
        dz = -1;
        start.z() = size.z() - 1;
        size.z() >>= 1;
      }
      for(uint z = 0, z1 = start.z(); z < size.z(); ++z, z1 += dz)
        for(uint y = 0, y1 = start.y(); y < size.y(); ++y, y1 += dy)
          for(uint x = 0, x1 = start.x(); x < size.x(); ++x, x1 += dx) {
            std::swap(data[stack->offset(x, y, z)], data[stack->offset(x1, y1, z1)]);
          }
      output->changed();
    } else if(rx and ry) {
      uint size_xy = size.x() * size.y();
      uint to_shift = size_xy >> 1;
      for(uint z = 0; z < size.z(); ++z) {
        ushort* s = &data[stack->offset(0, 0, z)];
        ushort* e = s + size_xy - 1;
        for(uint i = 0; i < to_shift; ++i) {
          std::swap(*e--, *s++);
        }
      }
      output->changed();
    } else if(rz)     // and rx or ry
    {
      Point3u start;
      int dx = 1, dy = 1;
      start.z() = size.z() - 1;
      if(rx) {
        start.x() = size.x() - 1;
        dx = -1;
      } else       // if(ry)
      {
        start.y() = size.y() - 1;
        dy = -1;
      }
      uint half_z = size.z() >> 1;
      for(uint z = 0, z1 = start.z(); z < half_z; ++z, --z1)
        for(uint y = 0, y1 = start.y(); y < size.y(); ++y, y1 += dy)
          for(uint x = 0, x1 = start.x(); x < size.x(); ++x, x1 += dx) {
            std::swap(data[stack->offset(x, y, z)], data[stack->offset(x1, y1, z1)]);
          }
      if(size.z() & 1)       // If there is an off number of planes
      {
        uint z = half_z;
        uint end_y = (ry ? size.y() >> 1 : size.y());
        uint end_x = (rx ? size.x() >> 1 : size.x());
        for(uint y = 0, y1 = start.y(); y < end_y; ++y, y1 += dy)
          for(uint x = 0, x1 = start.x(); x < end_x; ++x, x1 += dx) {
            std::swap(data[stack->offset(x, y, z)], data[stack->offset(x1, y1, z)]);
          }
      }
      output->changed();
    }
  } else if(rx or ry or rz) {
    const HVecUS& data = input->data();
    HVecUS& out = output->data();
    Point3u size = stack->size();
    int dx = (rx ? -1 : 1), dy = (ry ? -1 : 1), dz = (rz ? -1 : 1);
    uint startx = (rx ? size.x() - 1 : 0);
    uint starty = (ry ? size.y() - 1 : 0);
    uint startz = (rz ? size.z() - 1 : 0);
    for(uint z1 = 0, z2 = startz; z1 < size.z(); ++z1, z2 += dz)
      for(uint y1 = 0, y2 = starty; y1 < size.y(); ++y1, y2 += dy)
        for(uint x1 = 0, x2 = startx; x1 < size.x(); ++x1, x2 += dx)
          out[stack->offset(x2, y2, z2)] = data[stack->offset(x1, y1, z1)];
    output->copyMetaData(input);
    output->changed();
  }
  return true;
}

REGISTER_STACK_PROCESS(ReverseStack);

} // namespace process
} // namespace lgx
