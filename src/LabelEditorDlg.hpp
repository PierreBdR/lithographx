/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef LABELEDITORDLG_HPP
#define LABELEDITORDLG_HPP

#include <LGXConfig.hpp>

#include <Color.hpp>
#include <Misc.hpp>

#include <QAbstractListModel>
#include <vector>
#include <QDialog>

class QAbstractButton;
class QPoint;

class LabelModel : public QAbstractListModel {
public:
  LabelModel(std::vector<lgx::Colorf>* colors);

  int rowCount(const QModelIndex& parent = QModelIndex()) const;
  int columnCount(const QModelIndex& /*parent*/ = QModelIndex()) const {
    return 1;
  }

  Qt::ItemFlags flags(const QModelIndex& index) const;

  QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const;
  bool setData(const QModelIndex& index, const QVariant& value, int role = Qt::EditRole);

  void apply();
  void reset();

  void makeGray();
  void makeRandom();

  void setNbColors(int n);

  bool insertRows(int row, int count, const QModelIndex& parent = QModelIndex());
  bool removeRows(int row, int count, const QModelIndex& parent = QModelIndex());

protected:
  std::vector<lgx::Colorf>* toChange;
  std::vector<lgx::Colorf> localCopy;
};

namespace Ui {
class LabelEditorDlg;
} // namespace Ui

class LabelEditorDlg : public QDialog {
  Q_OBJECT
public:
  LabelEditorDlg(std::vector<lgx::Colorf>* colors, QWidget* parent);
  ~LabelEditorDlg();

public slots:
  void importLabels();
  void exportLabels();
  bool importLabels(QString filename);
  bool exportLabels(QString filename);
  void makeLabelCurrent();
  void selectLabel();
  void setCurrentLabel(int label);

protected slots:
  void on_buttonBox_clicked(QAbstractButton* btn);
  void on_setNbColors_clicked();
  void on_labelsView_doubleClicked(const QModelIndex& idx);
  void on_labelsView_customContextMenuRequested(const QPoint& pos);
  void on_showCurrent_clicked();
  void on_makeGray_clicked();
  void on_makeRandom_clicked();
  void changeNbItems();

signals:
  void update();
  void labelSelected(int label, int repeat, bool replaceSelection);
  void currentLabelChosen(int label);

protected:
  int currentLabel, selectedLabel;
  LabelModel* _model;
  Ui::LabelEditorDlg* ui;
  QPushButton* importButton, *exportButton;
  QAction* selectLabelAction, *makeLabelCurrentAction;
};

#endif // LABELEDITORDLG_HPP
