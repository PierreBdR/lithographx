/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "Python.hpp"

#include <LGXPython.hpp>
#include <Version.hpp>
#include <Misc.hpp>
#include <SystemDirs.hpp>

#include "lgx_module.hpp"
#include "sip.h"

#include <QtGlobal>
#include <QString>
#include <QStandardPaths>

namespace lgx {
namespace process {

namespace {
const sipAPIDef *get_sip_api()
{
#if defined(SIP_USE_PYCAPSULE)
  python::SafePyObject sip_sipmod = PyImport_ImportModule(SIP_MODULE_NAME);
  PyObject* sip_capiobj = PyDict_GetItemString(PyModule_GetDict(sip_sipmod), "_C_API");
  return reinterpret_cast<const sipAPIDef *>(PyCapsule_GetPointer(sip_capiobj, SIP_MODULE_NAME "._C_API"));
#else
  PyObject *sip_module;
  PyObject *sip_module_dict;
  PyObject *c_api;

  /* Import the SIP module. */
  sip_module = PyImport_ImportModule("sip");

  if (sip_module == NULL)
    return NULL;

  /* Get the module's dictionary. */
  sip_module_dict = PyModule_GetDict(sip_module);

  /* Get the "_C_API" attribute. */
  c_api = PyDict_GetItemString(sip_module_dict, "_C_API");

  if (c_api == NULL)
    return NULL;

  /* Sanity check that it is the right type. */
  if (!PyCObject_Check(c_api))
    return NULL;

  /* Get the actual pointer from the object. */
  return (const sipAPIDef *)PyCObject_AsVoidPtr(c_api);
#endif
}
} // namespace


template <typename ProcessType>
PythonProcess<ProcessType>::PythonProcess(const process_t& process)
  : Process(process)
  , ProcessType(process)
{ }

template class LGXPYTHONSHARED_EXPORT PythonProcess<StackProcess>;
template class LGXPYTHONSHARED_EXPORT PythonProcess<MeshProcess>;
template class LGXPYTHONSHARED_EXPORT PythonProcess<GlobalProcess>;

LGXPYTHONSHARED_EXPORT const sipAPIDef* PythonInterpreter::_api = nullptr;
LGXPYTHONSHARED_EXPORT const sipTypeDef* PythonInterpreter::_sip_QWidgetType = nullptr;
LGXPYTHONSHARED_EXPORT const sipTypeDef* PythonInterpreter::_sip_QVariantType = nullptr;

namespace {
#ifdef IS_PY3K
wchar_t progName[] = L"LithoGraphX";
#else
char progName[] = "LithoGraphX";
#endif
} // namespace

bool PythonInterpreter::valid() const
{
    return Py_IsInitialized();
}

PythonInterpreter::PythonInterpreter(bool global_interpreter)
  : is_global(global_interpreter)
{
  bool need_initializing = not (global_interpreter or Py_IsInitialized());
  if(need_initializing) {
    Py_SetProgramName(progName);

#ifdef WIN32
    // Under windows, check python was found (with LGX_PYTHON)
    if(not qEnvironmentVariableIsSet("LGX_PYTHON")) {
        Information::out << "Warning, Python couldn't be found with this installation.\n"
                            "You need to install Python and/or set its path in the LGX_PYTHON environment variable." << endl;
        return;
    }
#endif

    Information::out << "Initialize Python interpreter" << endl;

    Py_Initialize();

    // Check versions
    auto runtimeVersion = util::Version(Py_GetVersion());
    auto compiledVersion = util::Version(PY_VERSION);

    Information::out << "Python runtime version: " << Py_GetVersion() << endl;

    if(runtimeVersion != compiledVersion) {
        if(runtimeVersion < compiledVersion) {
            QString msg = QString("Problem: LithoGraphX was compiled with Python version %1, but is running with Python version %2")
                                .arg(compiledVersion.toString()).arg(runtimeVersion.toString());
            auto line = QString(msg.size(), '#');
            Information::out << line << "\n"
                             << msg << "\n"
                             << line << endl;
        } else {
            QString msg = QString("Carefule: LithoGraphX was compiled with Python version %1, but is running with Python version %2")
                                .arg(compiledVersion.toString()).arg(runtimeVersion.toString());
            Information::out << msg << endl;
        }
    }

#if defined(WIN32) && !defined(IS_PY3K)
    do {
        Information::out << "Registering cp65001 code page to Python" << endl;
        // For Python 2, register cp65001 as a valid codecs
        QString codecScript("import codecs\n"
                            "def cp65001ToUtf8(name):\n"
                            "    if name == '65001' or name == 'cp65001':\n"
                            "        return codecs.lookup('utf8')\n"
                            "codecs.register(cp65001ToUtf8)");

        PyObject *main = PyImport_AddModule("__main__");
        if(not main) {
            Information::err << "Error, couldn't import __main__ module" << endl;
            break;
        }
        PyObject *main_dict = PyModule_GetDict(main);
        if(not main_dict) {
            Information::err << "Error, couldn't get __main__ module's dictionary" << endl;
            break;
        }
        python::SafePyObject res_codecs = PyRun_String(codecScript.toUtf8().data(), Py_file_input, main_dict, main_dict);
        if(not res_codecs) {
            Information::out << "Warning, couldn't setup codecs for cp65001 code page:\n"
                             << python::getError() << endl;
        }
    } while(false);
#endif
    char path[] = "path";
    PyObject* sys_path = PySys_GetObject(path);

    for(const QDir& folder: util::macroDirs()) {
      QString path = folder.canonicalPath();
#ifdef WIN32
      path = path.replace('/', '\\');
#endif
      python::SafePyObject pypath = python::makeString(path);
      PyList_Insert(sys_path, 0, pypath);
    }

    char dont_write_bytecode[] = "dont_write_bytecode";

    // Set dont_write_bytecode to True
    PySys_SetObject(dont_write_bytecode, Py_True);

    // To start with, import lgxPy and PyQt5 modules (will save time later and we make sure everything is alright)
    python::SafePyObject lgxPy = PyImport_ImportModule("lgxPy");
    if(PyErr_Occurred())
      Information::err << "Error loading lgxPy: " << python::getError() << endl;
    python::SafePyObject core = PyImport_ImportModule("PyQt5.QtCore");
    if(PyErr_Occurred())
      Information::err << "Error loading PyQt5.QtCore: " << python::getError() << endl;
    python::SafePyObject gui = PyImport_ImportModule("PyQt5.QtGui");
    if(PyErr_Occurred())
      Information::err << "Error loading PyQt5.QtGui: " << python::getError() << endl;
    python::SafePyObject widgets = PyImport_ImportModule("PyQt5.QtWidgets");
    if(PyErr_Occurred())
      Information::err << "Error loading PyQt5.QtWidgets: " << python::getError() << endl;

    _api = get_sip_api();
    _sip_QWidgetType = findSipType("QWidget");
    _sip_QVariantType = findSipType("QVariant");
  }
}

const sipTypeDef* PythonInterpreter::findSipType(const char* type)
{
  if(_api)
    return _api->api_find_type(type);
  return nullptr;
}

PyObject* PythonInterpreter::convertFromSipType(void* obj, const sipTypeDef* type, PyObject* transferObj)
{
  if(_api)
    return _api->api_convert_from_type(obj, type, transferObj);
  return nullptr;
}

PyObject* PythonInterpreter::convertFromSipType(void* obj, const char* type, PyObject* transferObj)
{
  if(_api)
    return convertFromSipType(obj, findSipType(type), transferObj);
  return nullptr;
}

bool PythonInterpreter::canConvertToSipType(PyObject* obj, const sipTypeDef* type, int flags)
{
  if(_api)
    return _api->api_can_convert_to_type(obj, type, flags);
  return false;
}

void* PythonInterpreter::convertToSipType(PyObject* obj, const sipTypeDef* type, PyObject* transferObj,
                                          int flags, int* state, int* iserr)
{
  if(_api)
    return _api->api_convert_to_type(obj, type, transferObj, flags, state, iserr);
  return nullptr;
}

bool PythonInterpreter::enableAutoConversion(const sipTypeDef* td, bool enable)
{
  if(_api)
    return _api->api_enable_autoconversion(td, enable);
  return false;
}

PyTypeObject* PythonInterpreter::convertSipTypeToPyTypeObject(const sipTypeDef* tdef)
{
  return sipTypeAsPyTypeObject(const_cast<sipTypeDef*>(tdef));
}

PythonInterpreter::~PythonInterpreter()
{
  if(is_global) {
    finalize();
  }
}

void PythonInterpreter::finalize()
{
  if(Py_IsInitialized()) {
    Information::err << "Finalizing Python's interpreter" << endl;
    Py_Finalize();
    _api = nullptr;
    _sip_QWidgetType = nullptr;
    _sip_QVariantType = nullptr;
  }
}

namespace {
PythonInterpreter interpreter(true); // Global interpreter
} // namespace

} // namespace process
} // namespace lgx
