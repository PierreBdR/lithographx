/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "SettingsDlg.hpp"

#include <cuda/CudaExport.hpp>
#include <GL.hpp>
#include <ImageData.hpp>
#include <Information.hpp>
#include <LithoGraphX.hpp>
#include <LithoViewer.hpp>

#include <QDialog>
#include <QFontDialog>

#include "ui_ModeSelectionDlg.h"

SettingsDlg::SettingsDlg(LithoViewer* l)
  : QDialog(l)
  , viewer(l)
{
  ui.setupUi(this);
  loadImageQuality();
  loadGeneral();
  loadRendering();
  loadScaleBar();
  loadColorBar();
}

void SettingsDlg::loadRendering()
{
  ui.renderingMaxTexX->setValue(lgx::ImgData::MaxTexSize.x());
  ui.renderingMaxTexY->setValue(lgx::ImgData::MaxTexSize.y());
  ui.renderingMaxTexZ->setValue(lgx::ImgData::MaxTexSize.z());
  ui.renderingMax2DTexX->setValue(lgx::ImgData::Max2DTexSize.x());
  ui.renderingMax2DTexY->setValue(lgx::ImgData::Max2DTexSize.y());
  ui.maxCudaMem->setValue(getHoldMemGPU());
  ui.renderingNormalSize->setValue(lgx::ImgData::DrawNormals);
  ui.renderingMeshLineThickness->setValue(lgx::ImgData::MeshLineWidth);

  ui.rendering16Bits->setChecked(lgx::ImgData::defaultTex16Bits);
  ui.rendering8Bits->setChecked(not lgx::ImgData::defaultTex16Bits);

  ui.renderingMaxPeel->setValue(viewer->property("maxNbPeels").toInt());
  auto unsharp = viewer->property("unsharpening").toFloat();
  lgx::Information::out << "unsharp = " << unsharp << endl;
  ui.renderingUnsharp->setValue(unsharp);
  auto shininess = viewer->property("shininess").toFloat();
  lgx::Information::out << "shininess = " << shininess << endl;
  ui.renderingShininess->setValue(shininess);
  auto specular = viewer->property("specular").toFloat();
  lgx::Information::out << "specular = " << specular << endl;
  ui.renderingSpecular->setValue(specular);
}

void SettingsDlg::loadImageQuality()
{
  setImageQualityGlobalContrast(viewer->property("globalContrast").toFloat());
  ui.imageQualityGlobalContrast->setDefaultValue(5000);
  setImageQualityGlobalBrightness(viewer->property("globalBrightness").toFloat());
  ui.imageQualityGlobalBrightness->setDefaultValue(0);
  setImageQualitySlices(viewer->property("slices").toUInt());
  ui.imageQualitySlices->setDefaultValue(1000);
  setImageQualityScreenSampling(viewer->property("screenSampling").toFloat());
  ui.imageQualityScreenSampling->setDefaultValue(20);

  connect(viewer, SIGNAL(changedGlobalContrast(float)), this, SLOT(setImageQualityGlobalContrast(float)));
  connect(viewer, SIGNAL(changedGlobalBrightness(float)), this, SLOT(setImageQualityGlobalBrightness(float)));
  connect(viewer, SIGNAL(changedSlices(uint)), this, SLOT(setImageQualitySlices(uint)));
  connect(viewer, SIGNAL(changedScreenSampling(float)), this, SLOT(setImageQualityScreenSampling(float)));
}

void SettingsDlg::on_setLaptop_clicked()
{
  ui.renderingMaxTexX->setValue(512);
  ui.renderingMaxTexY->setValue(512);
  ui.renderingMaxTexZ->setValue(512);
  ui.rendering8Bits->setChecked(true);
}

void SettingsDlg::on_setDesktop_clicked()
{
  GLint maxTexSize, max2DTexSize;
  lgx::opengl->glGetIntegerv(GL_MAX_3D_TEXTURE_SIZE, &maxTexSize);
  lgx::opengl->glGetIntegerv(GL_MAX_TEXTURE_SIZE, &max2DTexSize);
  ui.renderingMaxTexX->setValue(maxTexSize);
  ui.renderingMaxTexY->setValue(maxTexSize);
  ui.renderingMaxTexZ->setValue(maxTexSize);
  ui.renderingMax2DTexX->setValue(max2DTexSize);
  ui.renderingMax2DTexY->setValue(max2DTexSize);
  ui.rendering16Bits->setChecked(true);
}

void SettingsDlg::setImageQualityGlobalContrast(float value)
{
  ui.imageQualityGlobalContrast->setValue(int(value*5000.f));
}

void SettingsDlg::setImageQualityGlobalBrightness(float value)
{
  ui.imageQualityGlobalBrightness->setValue(int(value*5000.f));
}

void SettingsDlg::setImageQualitySlices(uint value)
{
  ui.imageQualitySlices->setValue(value);
}

void SettingsDlg::setImageQualityScreenSampling(float value)
{
  ui.imageQualityScreenSampling->setValue(int(value*10));
}

void SettingsDlg::on_imageQualityGlobalBrightness_valueChanged(int value)
{
  viewer->setProperty("globalBrightness", float(value) / 5000.f);
}

void SettingsDlg::on_imageQualityGlobalContrast_valueChanged(int value)
{
  viewer->setProperty("globalContrast", float(value) / 5000.f);
}

void SettingsDlg::on_imageQualitySlices_valueChanged(int value)
{
  viewer->setProperty("slices", (uint)value);
}

void SettingsDlg::on_imageQualityScreenSampling_valueChanged(int value)
{
  viewer->setProperty("screenSampling", float(value) / 10.f);
}

void SettingsDlg::loadGeneral()
{
#ifdef LithoGraphX_CHECK_UPDATES
  QSettings settings;
  auto autoUpdate = settings.value("AutoUpdate/CheckUpdate", true).toBool();
  bool ok;
  auto intervalTime = settings.value("AutoUpdate/CheckInterval", 1).toUInt(&ok);
  if(not ok) intervalTime = 1;
  ui.checkNewVersion->setChecked(autoUpdate);
  ui.checkVersionInterval->setValue(intervalTime);
#else
  auto idx = ui.tabWidget->indexOf(ui.tabGeneral);
  ui.tabWidget->removeTab(idx);
#endif
}

void SettingsDlg::loadScaleBar()
{
  auto& scaleBar = lgx::ImgData::scaleBar;

  ui.scaleBarPosition->setCurrentIndex((int)scaleBar.position);
  ui.scaleBarOrientation->setCurrentIndex((int)scaleBar.orientation);
  ui.scaleBarTextPosition->setCurrentIndex((int)scaleBar.textPosition);
  ui.scaleBarMinSize->setValue((int)scaleBar.minimumSize());
  ui.scaleBarMaxSize->setValue((int)scaleBar.maximumSize());
  ui.scaleBarThickness->setValue((int)scaleBar.thickness());

  scaleBarFont = scaleBar.font();
}

void SettingsDlg::loadColorBar()
{
  auto& colorBar = lgx::ImgData::colorBar;

  ui.colorBarPosition->setCurrentIndex((int)colorBar.position);
  ui.colorBarOrientation->setCurrentIndex((int)colorBar.orientation);
  ui.colorBarLength->setValue(100*colorBar.scale_length);
  ui.colorBarWidth->setValue((int)colorBar.width);
  ui.colorBarDistText->setValue((int)colorBar.text_to_bar);
  ui.colorBarDistBorder->setValue((int)colorBar.distance_to_border);
  ui.colorBarTickSize->setValue((int)colorBar.tick_size);
  ui.colorBarLineWidth->setValue((int)colorBar.line_width);

  colorBarFont = colorBar.font;
}

void SettingsDlg::loadDefaults(LithoGraphX* lgx)
{
  QSettings settings;

  {
    settings.beginGroup("Rendering");
    bool ok;

    if(not settings.contains("Default16Bits")) {
      QDialog dlg(lgx);
      auto ui = Ui::ModeSelectionDlg();
      ui.setupUi(&dlg);
      switch(dlg.exec()) {
        case QDialog::Accepted:
          {
            settings.remove("MaxTexX");
            settings.remove("MaxTexY");
            settings.remove("MaxTexZ");
            lgx::ImgData::defaultTex16Bits = true;
            settings.setValue("Default16Bits", true);
          }
          break;
        case QDialog::Rejected:
          {
            settings.setValue("MaxTexX", 512);
            settings.setValue("MaxTexY", 512);
            settings.setValue("MaxTexZ", 512);
            lgx::ImgData::defaultTex16Bits = false;
            settings.setValue("Default16Bits", false);
          }
      }
    }


    lgx::ImgData::defaultTex16Bits = settings.value("Default16Bits", true).toBool();

    auto cudaMem = settings.value("CudaMem", 0).toUInt(&ok);
    if(ok) setHoldMemGPU(cudaMem);

    float normalSize = settings.value("MeshNormalSize", 0.).toFloat(&ok);
    if(ok) lgx::ImgData::DrawNormals = normalSize;

    settings.endGroup();
  }

}

void SettingsDlg::loadDefaults(LithoViewer* viewer)
{
  QSettings settings;

  {
    settings.beginGroup("ImageQuality");
    bool ok;

    float globalContrast = settings.value("GlobalContrast", 1.0).toFloat(&ok);
    if(ok and globalContrast > 0) viewer->setProperty("globalContrast", globalContrast);

    float globalBrightness = settings.value("GlobalBrightness",0.0).toFloat(&ok);
    if(ok and globalBrightness > 0) viewer->setProperty("globalBrightness", globalBrightness);

    uint slices = settings.value("Slices", 1000u).toUInt(&ok);
    if(ok and slices > 0) viewer->setProperty("slices", slices);

    float screenSampling = settings.value("ScreenSampling", 2).toFloat(&ok);
    if(ok and screenSampling > 0) viewer->setProperty("screenSampling", screenSampling);

    settings.endGroup();
  }

  {
    settings.beginGroup("Rendering");
    bool ok;

    if(lgx::opengl) {
      GLint maxTexSize, max2DTexSize;
      lgx::opengl->glGetIntegerv(GL_MAX_3D_TEXTURE_SIZE, &maxTexSize);
      lgx::opengl->glGetIntegerv(GL_MAX_TEXTURE_SIZE, &max2DTexSize);
      lgx::Information::out << "maxTexSize = " << maxTexSize << endl;
      lgx::Information::out << "max2DTexSize = " << max2DTexSize << endl;

      lgx::Point3u maxTex;
      maxTex.x() = settings.value("MaxTexX").toInt(&ok);
      if(not ok) {
        maxTex.x() = maxTexSize;
        settings.setValue("MaxTexX", maxTex.x());
      }
      maxTex.y() = settings.value("MaxTexY").toInt(&ok);
      if(not ok) {
        maxTex.y() = maxTexSize;
        settings.setValue("MaxTexY", maxTex.y());
      }
      maxTex.z() = settings.value("MaxTexZ").toInt(&ok);
      if(not ok) {
        maxTex.z() = maxTexSize;
        settings.setValue("MaxTexZ", maxTex.z());
      }
      lgx::ImgData::MaxTexSize = maxTex;
      lgx::Point2u max2DTex;
      max2DTex.x() = settings.value("Max2DTexX").toInt(&ok);
      if(not ok) {
        max2DTex.x() = max2DTexSize;
        settings.setValue("Max2DTexX", max2DTex.x());
      }
      max2DTex.y() = settings.value("Max2DTexY").toInt(&ok);
      if(not ok) {
        max2DTex.y() = max2DTexSize;
        settings.setValue("Max2DTexY", max2DTex.y());
      }
      lgx::ImgData::Max2DTexSize = max2DTex;

    } else {
      lgx::Information::out << "Warning, OpenGL not initialized" << endl;
    }


    auto maxPeels = settings.value("MaxPeels", 10).toUInt(&ok);
    if(ok and maxPeels > 0) viewer->setProperty("maxNbPeels", maxPeels);

    auto unsharpening = settings.value("Unsharpening", 1.f).toFloat(&ok);
    if(ok) viewer->setProperty("unsharpening", unsharpening);

    auto shininess = settings.value("Shininess", 32.f).toFloat(&ok);
    if(ok) viewer->setProperty("shininess", shininess);

    auto specular = settings.value("Specular", 0.2f).toFloat(&ok);
    if(ok) viewer->setProperty("specular", specular);
    settings.endGroup();
  }

  {
    lgx::ImgData::scaleBar.loadDefaults();
  }
  {
    lgx::ImgData::colorBar.loadDefaults();
  }
}

void SettingsDlg::applyGeneral()
{
  QSettings settings;
  settings.setValue("AutoUpdate/CheckUpdate", ui.checkNewVersion->isChecked());
  settings.setValue("AutoUpdate/CheckInterval", ui.checkVersionInterval->value());
}

void SettingsDlg::saveGeneral()
{
  applyGeneral();
}

void SettingsDlg::applyImageQuality()
{
  viewer->setProperty("globalBrightness", float(ui.imageQualityGlobalBrightness->value()) / 5000.f);
  viewer->setProperty("globalContrast", float(ui.imageQualityGlobalContrast->value()) / 5000.f);
  viewer->setProperty("slices", (uint)ui.imageQualitySlices->value());
  viewer->setProperty("screenSampling", float(ui.imageQualityScreenSampling->value()) / 10.f);
}

void SettingsDlg::saveImageQuality()
{
  applyImageQuality();

  QSettings settings;

  settings.beginGroup("ImageQuality");

  settings.setValue("GlobalContrast", viewer->property("globalContrast"));
  settings.setValue("GlobalBrightness", viewer->property("globalBrighness"));
  settings.setValue("Slices", viewer->property("slices"));
  settings.setValue("ScreenSampling", viewer->property("screenSampling"));

  settings.endGroup();
}

void SettingsDlg::applyRendering()
{
  lgx::ImgData::MaxTexSize.x() = (uint)ui.renderingMaxTexX->value();
  lgx::ImgData::MaxTexSize.y() = (uint)ui.renderingMaxTexY->value();
  lgx::ImgData::MaxTexSize.z() = (uint)ui.renderingMaxTexZ->value();
  lgx::ImgData::Max2DTexSize.x() = (uint)ui.renderingMax2DTexX->value();
  lgx::ImgData::Max2DTexSize.y() = (uint)ui.renderingMax2DTexY->value();

  lgx::ImgData::defaultTex16Bits = ui.rendering16Bits->isChecked();

  ui.maxCudaMem->setValue(setHoldMemGPU(ui.maxCudaMem->value()));

  lgx::ImgData::DrawNormals = ui.renderingNormalSize->value();
  lgx::ImgData::MeshLineWidth = ui.renderingMeshLineThickness->value();

  viewer->setProperty("maxNbPeels", ui.renderingMaxPeel->value());
  viewer->setProperty("unsharpening", ui.renderingUnsharp->value());
  viewer->setProperty("shininess", ui.renderingShininess->value());
  viewer->setProperty("specular", ui.renderingSpecular->value());

  emit update3DDrawing();
}

void SettingsDlg::saveRendering()
{
  applyRendering();

  QSettings settings;

  settings.beginGroup("Rendering");

  settings.setValue("MaxTexX", lgx::ImgData::MaxTexSize.x());
  settings.setValue("MaxTexY", lgx::ImgData::MaxTexSize.y());
  settings.setValue("MaxTexZ", lgx::ImgData::MaxTexSize.z());
  settings.setValue("Max2DTexX", lgx::ImgData::Max2DTexSize.x());
  settings.setValue("Max2DTexY", lgx::ImgData::Max2DTexSize.y());
  settings.setValue("Default16Bits", lgx::ImgData::defaultTex16Bits);
  settings.setValue("CudaMem", getHoldMemGPU());
  settings.setValue("MeshNormalSize", lgx::ImgData::DrawNormals);
  settings.setValue("MeshLineWidth", lgx::ImgData::MeshLineWidth);
  settings.setValue("MaxPeels", viewer->property("maxNbPeels").toUInt());
  settings.setValue("Unsharpening", viewer->property("unsharpening").toFloat());
  settings.setValue("Shininess", viewer->property("shininess").toFloat());
  settings.setValue("Specular", viewer->property("specular").toFloat());

  settings.endGroup();
}

void SettingsDlg::applyScaleBar()
{
  auto& scaleBar = lgx::ImgData::scaleBar;

  scaleBar.position = (lgx::ScaleBar::Position) ui.scaleBarPosition->currentIndex();
  scaleBar.orientation = (lgx::ScaleBar::Orientation) ui.scaleBarOrientation->currentIndex();
  scaleBar.textPosition = (lgx::ScaleBar::TextPosition) ui.scaleBarTextPosition->currentIndex();
  scaleBar.setSizeRange(ui.scaleBarMinSize->value(), ui.scaleBarMaxSize->value());
  scaleBar.setThickness(ui.scaleBarThickness->value());
  scaleBar.setFont(scaleBarFont);

  emit update2DDrawing();
}

void SettingsDlg::saveScaleBar()
{
  applyScaleBar();

  auto& scaleBar = lgx::ImgData::scaleBar;
  QSettings settings;

  settings.beginGroup("ScaleBar");

  settings.setValue("Position", (int)scaleBar.position);
  settings.setValue("Orientation", (int)scaleBar.orientation);
  settings.setValue("TextPosition", (int)scaleBar.textPosition);
  settings.setValue("Font", scaleBar.font());
  settings.setValue("MinimumSize", scaleBar.minimumSize());
  settings.setValue("MaximumSize", scaleBar.maximumSize());
  settings.setValue("Thickness", scaleBar.thickness());

  settings.endGroup();
}

void SettingsDlg::applyColorBar()
{
  auto& colorBar = lgx::ImgData::colorBar;

  colorBar.position = (lgx::ColorBar::Position) ui.colorBarPosition->currentIndex();
  colorBar.orientation = (lgx::ColorBar::Orientation) ui.colorBarOrientation->currentIndex();
  colorBar.scale_length = ui.colorBarLength->value() / 100.;
  colorBar.width = ui.colorBarWidth->value();
  colorBar.text_to_bar = ui.colorBarDistText->value();
  colorBar.distance_to_border = ui.colorBarDistBorder->value();
  colorBar.tick_size = ui.colorBarTickSize->value();
  colorBar.font = colorBarFont;
  colorBar.line_width = ui.colorBarLineWidth->value();

  emit update2DDrawing();
}

void SettingsDlg::saveColorBar()
{
  applyColorBar();
  auto& colorBar = lgx::ImgData::colorBar;

  QSettings settings;
  settings.beginGroup("ColorBar");

  settings.setValue("Position", (int)colorBar.position);
  settings.setValue("Orientation", (int)colorBar.orientation);
  settings.setValue("Length", colorBar.scale_length);
  settings.setValue("Width", colorBar.width);
  settings.setValue("DistanceToText", colorBar.text_to_bar);
  settings.setValue("DistanceToBorder", colorBar.distance_to_border);
  settings.setValue("TickSize", colorBar.tick_size);
  settings.setValue("Font", colorBar.font);
  settings.setValue("LineWidth", colorBar.line_width);

  settings.endGroup();
}

void SettingsDlg::on_buttonBox_accepted()
{
  applyAll();
}

void SettingsDlg::applyAll()
{
  applyGeneral();
  applyRendering();
  applyScaleBar();
  applyColorBar();
}

void SettingsDlg::on_buttonBox_clicked(QAbstractButton* btn)
{
  switch(ui.buttonBox->buttonRole(btn)) {
    case QDialogButtonBox::ApplyRole:
      applyAll();
      break;
    default:
      break;
  }
}

void SettingsDlg::on_imageQualitySetDefaults_clicked()
{
  saveImageQuality();
}

void SettingsDlg::on_generalSetDefaults_clicked()
{
  saveGeneral();
}

void SettingsDlg::on_renderingSetDefaults_clicked()
{
  saveRendering();
}

void SettingsDlg::on_scaleBarSetDefaults_clicked()
{
  saveScaleBar();
}

void SettingsDlg::on_colorBarSetDefaults_clicked()
{
  saveColorBar();
}

void SettingsDlg::on_scaleBarMinSize_valueChanged(int value)
{
  int maxValue = ui.scaleBarMaxSize->value();
  if(maxValue < 2.5*value) {
    maxValue = (int)std::ceil(2.5*value);
    ui.scaleBarMaxSize->setValue(maxValue);
  }
}

void SettingsDlg::on_scaleBarMaxSize_valueChanged(int value)
{
  int minValue = ui.scaleBarMinSize->value();
  if(minValue > value/2.5)
    minValue = (int)std::floor(value/2.5);
  ui.scaleBarMinSize->setValue(minValue);
}

void SettingsDlg::on_scaleBarChooseFont_clicked()
{
  QFontDialog dlg(scaleBarFont, this);
  if(dlg.exec() == QDialog::Accepted) {
    scaleBarFont = dlg.currentFont();
  }
}

void SettingsDlg::on_colorBarChooseFont_clicked()
{
  QFontDialog dlg(colorBarFont, this);
  if(dlg.exec() == QDialog::Accepted) {
    colorBarFont = dlg.currentFont();
  }
}

void SettingsDlg::on_checkVersionInterval_valueChanged(int value)
{
  if(value > 1)
    ui.checkVersionInterval->setSuffix(" days");
  else
    ui.checkVersionInterval->setSuffix(" day");
}
