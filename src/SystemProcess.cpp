/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "SystemProcess.hpp"
#include "SystemProcessLoad.hpp"
#include "SystemProcessSave.hpp"
#include "Information.hpp"
#include "Libraries.hpp"
#include "Thrust.hpp"

#include <QtCoreVersion>
#include <Eigen/Core>

#include <tiffvers.h>
#include <CImg.h>
#include <qhull/libqhull.h>

namespace lgx {
namespace process {

#define INIT_PROCESS(varName, ClassName, ProcessClass) \
  varName(std::make_shared<ClassProcessFactory<ProcessClass, ClassName>>(), \
          lgx::util::qdemangle(typeid(ClassName).name()), \
          PROCESS_VERSION)

struct SystemRegistration {
  SystemRegistration()
    : INIT_PROCESS(stackImport, StackImport, StackProcess)
    , INIT_PROCESS(stackOpen, StackOpen, StackProcess)
    , INIT_PROCESS(stackExport, StackExport, StackProcess)
    , INIT_PROCESS(stackSave, StackSave, StackProcess)
    , INIT_PROCESS(meshLoad, MeshLoad, MeshProcess)
    , INIT_PROCESS(meshExport, MeshExport, MeshProcess)
    , INIT_PROCESS(meshSave, MeshSave, MeshProcess)
    , INIT_PROCESS(meshReset, ResetMeshProcess, MeshProcess)
    , INIT_PROCESS(loadAllData, LoadAllData, GlobalProcess)
    , INIT_PROCESS(loadProjectFile, LoadProjectFile, GlobalProcess)
    , INIT_PROCESS(saveProjectFile, SaveProjectFile, GlobalProcess)
    , INIT_PROCESS(saveAll, SaveAll, GlobalProcess)
    , INIT_PROCESS(resetAll, ResetAll, GlobalProcess)
    , INIT_PROCESS(takeSnapshot, TakeSnapshot, GlobalProcess)
  {
  }

  StackRegistration stackImport, stackOpen, stackExport, stackSave;
  MeshRegistration meshLoad, meshExport, meshSave, meshReset;
  GlobalRegistration loadAllData, loadProjectFile, saveProjectFile, saveAll, resetAll, takeSnapshot;
};

namespace {
SystemRegistration* systemRegistration = 0;
std::vector<LibraryDeclaration> libraries = {};
} // namespace

void registerSystemProcesses()
{
  if(systemRegistration == 0)
    systemRegistration = new SystemRegistration();

  Information::out << "Declaring libraries" << endl;

  // Add known libraries
  libraries.push_back({{"Eigen", QString("%1.%2.%3").arg(EIGEN_WORLD_VERSION).arg(EIGEN_MAJOR_VERSION).arg(EIGEN_MINOR_VERSION),
                         "Eigen is a C++ template library for linear algebra:"
                         "matrices, vectors, numerical solvers, and related algorithms.",
                         "http://eigen.tuxfamily.org", ":/images/EigenLogo.png"}});

  libraries.push_back({{"Qt", QTCORE_VERSION_STR, "Qt is the leading independent technology for cross-platform development",
                         "http://qt.io", ":/images/QtLogo.png"}});

  libraries.push_back({{"Thrust", QString("%1.%2.%3").arg(THRUST_MAJOR_VERSION).arg(THRUST_MINOR_VERSION).arg(THRUST_SUBMINOR_VERSION),
                      "Thrust is a parallel algorithms library "
                      "which resembles the C++ Standard Template Library (STL). Thrust's "
                      "high-level interface greatly enhances programmer productivity while "
                      "enabling performance portability between GPUs and multicore CPUs. "
                      "Interoperability with established technologies (such as CUDA, TBB, and "
                      "OpenMP) facilitates integration with existing software. "
                      "Develop high-performance applications rapidly with Thrust",
                      "https://thrust.github.io/",
                      ":/images/ThrustLogo.png"}});

#ifdef THRUST_BACKEND_CUDA
  int cudaMajor, cudaMinor;
  cudaVersionGPU(&cudaMajor, &cudaMinor);
  libraries.push_back({{"Cuda", QString("%1.%2").arg(cudaMajor).arg(cudaMinor),
                      QString::fromUtf8(u8"CUDA® is a parallel computing platform and programming"
                      "model invented by NVIDIA. It enables dramatic increases"
                      "in computing performance by harnessing the power of the"
                      "graphics processing unit (GPU). With millions of"
                      "CUDA-enabled GPUs sold to date, software developers,"
                      "scientists and researchers are using GPU-accelerated"
                      "computing for broad-ranging applications."),
                      "https://developer.nvidia.com/cuda-zone",
                      ":/images/NVidiaLogo.png"}});
#endif

  QString version = TIFFLIB_VERSION_STR;
  {
    int fst = version.indexOf(",");
    int snd = version.indexOf("\n");
    version = version.left(snd);
    if(fst < snd)
      version = version.mid(fst+1);
    version = version.trimmed();
  }
  libraries.push_back({{"LibTIFF", version,
                      "libtiff is a set of C functions (a library) that support"
                      "the manipulation of TIFF image files. The library"
                      "requires an ANSI C compilation environment for building"
                      "and presumes an ANSI C environment for use.",
                      "http://www.libtiff.org/",
                      ":/images/LibTIFF.png"}});

  QString qhullVersion = "2012.1 2012/02/18";

  libraries.push_back({{"QHull", qhullVersion,
                      "Qhull computes the convex hull, Delaunay triangulation,"
                      "Voronoi diagram, halfspace intersection about a point,"
                      "furthest-site Delaunay triangulation, and furthest-site"
                      "Voronoi diagram. The source code runs in 2-d, 3-d, 4-d,"
                      "and higher dimensions. Qhull implements the Quickhull"
                      "algorithm for computing the convex hull. It handles"
                      "roundoff errors from floating point arithmetic. It"
                      "computes volumes, surface areas, and approximations to"
                      "the convex hull.", "http://qhull.org",
                      ":/images/QHullLogo.gif"}});

#ifndef WIN32
  // Because for some reason, it doesn't work under windows
  if(qhullVersion != QString::fromLocal8Bit(qh_version))
      Information::err << "WARNING -- qhull version changed to " << qh_version << endl;
#endif

  libraries.push_back({{"CImg", QString("%1.%2.%3").arg(cimg_version / 100).arg((cimg_version/10)%10).arg(cimg_version%10),
                      "The CImg Library is a small, open-source, and modern C++ toolkit for image processing.",
                      "http://cimg.eu", ":/images/CImgLogo.png"}});
}

void unregisterSystemProcesses()
{
  if(lgx::DEBUG)
    Information::out << "Trying to unregister system processes" << endl;
  if(systemRegistration)
    delete systemRegistration;
  systemRegistration = 0;

  libraries.clear();
}
} // namespace process
} // namespace lgx
