Installing LithoGraphX from Source on Microsoft Windows
=======================================================

There are two ways of compiling for Windows: the recommended way using
:ref:`MinGW64 <compile_mingw64>` or using :ref:`Visual Studio <compile_msvc>`.

.. _compile_mingw64:

Compilation with MinGW64 -- The Easy Way
----------------------------------------

The easier way to compile with MinGW64 is to install it via MSys2: 
http://sourceforge.net/projects/msys2/

Here is a step by step method to install all the libraries you need for
LithoGraphX.

#. (Cuda version only) Install Visual Studio 2013, for example the community edition which is
   available for free on http://www.visualstudio.com. From this version, you
   only need the C++ component.
#. (Cuda version only) Install the CUDA drivers. **Note:** These must be installed after Visual
   Studio, so CUDA find it and install all the necessary components.
#. Install MSys2 using the graphical installer
#. Open the MSys2 Win64 shell using the shortcut installed in the start menu.
#. Upgrade MSys2 using ``pacman``, the package manager of MSys2::

    $ pacman -Su
#. Install the packages we need for LithoGraphX::

    $ pacman -S mingw-w64-x86_64-libtiff mingw-w64-x86_64-qt5 mingw-w64-x86_64-python2-pyqt5 mingw-w64-x86_64-eigen3 mingw-w64-x86_64-cmake doxygen
#. (Optional). Install VTK::

    $ pacman -S mingw-w64-x86_64-vtk mingw-w64-x86_64-opencv

   We also need to install OpenCV as it is a dependence of VTK. However, as far
   as the version 20150512 of MSys2 is concerned, OpenCV is not installed
   automatically with VTK.
#. (Optional). Install ITK. Sadly, the version of ITK that comes with MSys2
   doesn't include the auto-seeded morphological watershed, central in so many
   protocols of LithoGraphX. So we need to compile it ourselve:

   #. Download ITK from http://www.itk.org
   #. Prepare your directory structure. For simplicity, I will assume everything
      linked to ITK will be in the same folder. This folder will contain three
      sub-folder:

        ``ITK_Source``

            Contains the source code of ITK

        ``ITK_Build``

            Will contain the build files of ITK

        ``ITK``

            Will contain the installed version of ITK

   #. Make sure the three directories exist, and ``ITK_Source`` contains the
      source code.
   #. Launch a MSys2 Win64 shell, using the shortcut in the start menu.
   #. Go to the ITK folder. Careful: MSys2 has an odd syntax. For example, if
      your folder is ``C:\Users\Me\ITK``, then you need to type::

        $ cd /C/Users/Me/ITK
   #. Configure ITK::

        $ cd ITK_Build
        $ cmake -G "MinGW Makefiles" -DCMAKE_CXX_FLAGS="-std=c++11" -DCMAKE_BUILD_TYPE=Release -DBUILD_TESTING=OFF -DBUILD_EXAMPLES=OFF -DBUILD_SHARED_LIBS=ON -DModule_ITKReview=ON -DCMAKE_INSTALL_PREFIX=../ITK ../ITK_Source

   #. Then, compile and install ITK::

        $ make -j4 install
#. (Optional). Install the GNU Scientific Library::

    $ pacman -S -s mingw-w64-x86_64-gsl
#. (NoCuda version only) Download and extract the thrust library
   https://thrust.github.io. As this library only contains header files, there
   is nothing to compile. Simply mark down where it is installed.
#. (Cuda version only) Create batch scripts to have both MinGW64 and Visual Studio compilers
   available. For the default installation of Visual Studio and MinGW64, the
   script should contain::

    call "C:\Program Files (x86)\Microsoft Visual Studio 12.0\VC\vcvarsall.bat" amd64
    @echo Visual Studio installed in current shell
    @set MINGW64=C:\msys64\mingw64
    @set PATH=%MINGW64%\bin;%PATH%
    @echo MINGW64 installed in current shell

   You might also want to create a shortcut to start the shell. Right-click
   somewhere (for example the desktop) and select ``New->Shortcut``. In the
   dialog box that appear, for location type::

    %comspec% /k ""C:\Path\to\script.bat""

   For the following, use the shortcut to open a terminal, and go to the
   LithoGraphX source tree::

    $ cd C:\Users\Me\LithoGraphX
#. (NoCuda version only). Open an msys Win64 shell from the start menu, and go
   to the LithoGraphX source tree::

    $ cd /C/Users/Me/LithoGraphX
#. Create a ``Build`` folder in the LithoGraphX source tree::

    $ mkdir Build
    $ cd Build
#. Configure LithoGraphX::

    $ cmake-gui ..

   #. (Cuda version) Select ``MinGW Makefiles``
   #. (NoCuda version) Select ``MSys Makefiles``
   #. On the right of the search box, check the boxes ``Grouped`` and
      ``Advanced``.
   #. Press the ``Configure`` button and check for errors until there are none
      left. Typically, you will need to inform the following variables:

        PYTHON_EXECUTABLE

            CMake will find the program ``python.exe``, but really you want
            ``python2.exe`` in the same folder

        PYTHON_LIBRARY

            If you already have python installed, CMake may find it instead of
            the python for MinGW64. If you left the default installation folder
            of MSys2, the correct path for the library is
            ``C:/msys64/mingw64/lib/libpython2.7.dll.a``

        SIP_EXECUTABLE

            This is the path to the ``sip.exe`` program. It should be at
            ``C:/msys64/mingw64/bin/sip.exe``.

        SIP_PYQT5_CORE_PATH

            This is the path to the Core module of PyQt5. This should be
            ``C:/msys64/mingw64/share/sip/Py2-Qt5/QtCore``

        SIP_PYQT5_PATH

            This is the path to all the modules of PyQt5. This should be:
            ``C:/msys64/mingw64/share/sip/Py2-Qt5``

        THRUST_BACKEND_CUDA / THRUST_BACKEND_OMP

            Check one or the other depending on whether you want to compile with
            CUDA or not. If not, you can delete the whole CUDA group.

        CMAKE_BUILD_TYPE

            Set this one to ``Release`` to compile with all the optimizations.

        CMAKE_INSTALL_PREFIX

            If you don't want to install LithoGraphX into the ``Program Files``
            folder, change the value. Note that to install in ``Program Files``,
            you need to set the proper permissions, unless you use the
            installer.

        BUILD_ITK

            If you have build ITK, check this box. The last build of ITK should
            be found automatically.

        BUILD_VTK

            If you have installed VTK, check this box. Again, CMake should find
            where it is automatically.
   #. When you are done configuring, press the ``Generate`` button. If there is
      no error, you are done with configuring LithoGraphX.
#. Now, you need to compile LithoGraphX. If you use the MSys command line, you
   need to type::

    $ make -j8

   Otherwise, you need to type::

    $ mingw32-make
#. Once compiled, you can either directly install LithoGraphX, or create
   a package you can re-distribute. We recommend the creation of a package. For
   this, from the command line, type::

    $ cpack -G NSIS64

Compilation with MinGW64 -- The Hard Way
----------------------------------------

This is another way to get MinGW64 with all the libraries. It avoids compiling
MinGW64 and Qt, but almost all the other dependencies are compiled.

Because CUDA requires Visual C++ under windows, and because we are using MinGW64
for the rest of the compilation, you have to be careful when installing your
development environment. Here, I will describe how to install it for CUDA 7.0:

#. Install Visual Studio 2013, for example the community edition which is
   available for free on http://www.visualstudio.com. From this version, you
   only need the C++ component.
#. Install the CUDA drivers. **Note:** These must be installed after Visual
   Studio, so CUDA find it and install all the necessary components.
#. Install python. We highly recommend using Anaconda. Once installed, you will
   need to install the ``libpython`` conda package.
#. Download the source code of SIP and PyQt5 from http://www.riverbankcomputing.com
#. Download the version of Qt5 corresponding to the version of PyQt5 you
   downloaded from http://sourceforge.net/projects/qt64ng
   **Note:** This version of Qt5 also contains python 2.7. However, to make it
   work you need to go into ``mingw64\opt\bin`` and copy the file
   ``libpython2.7.dll`` to ``mingw64\libs\python27.dll`` (the folder doesn't
   exist)
#. Download MSys from the MinGW64 website here:
   http://sourceforge.net/projects/mingw-w64/files/External%20binary%20packages%20%28Win64%20hosted%29/
#. To make your life simpler, create two scripts that will add the path to MinGW64
   to your PATH environment variable: one for the command shell, and one for
   UNIX shell. Assuming you installed MinGW64 in ``C:\MinGW64``, the first
   script should be called ``mingw64.bat`` and have for content::

    @set MINGW64=C:\MinGW64
    @set PATH=%MINGW64%\bin;%MINGW64%\opt\bin;%MINGW64%\opt\libs;%PATH%

   The second script should be called ``mingw64.sh`` and have for content::

    export PATH=/c/MinGW64/bin:$PATH

   This way, when you open a terminal, if this is a windows command, you can
   type::

    $ call mingw64.bat

   and have MinGW64 available, or for MSys shell::

    $ source mingw64.sh

#. Compilation of SIP
    #. Start a windows command and setup your environment variables for MinGW64::

        $ call mingw64.bat

    #. Configure sip::

        $ python configure.py -p win32-g++

    #. Compile and install::

        $ mingw32-make
        $ mingw32-make install

    #. Copy the file ``sip.exe`` in ``%MINGW64%\opt`` to ``%MINGW64%\opt\bin``

# Compilation of PyQt5
    #. Start a windows command and setup your environment variables for MinGW64
       (you can re-use the one open for SIP)
    #. Configure PyQt5::

        $ python configure.py --spec=win32-g++

    #. Compile and install::

        $ mingw32-make
        $ mingw32-make install

    #. Copy the files in ``%MINGW64%\opt`` to ``%MINGW64%\opt\bin``

#. Download the source code of libtiff5 from the GNUWin32 project:
   http://gnuwin32.sourceforge.net/packages/tiff.htm

    #. Launch the MSys shell
    #. Setup the environment variables for MinGW64
    #. Create a build directory and change to it (optional)
    #. Run the configure script, specifying a prefix
    #. Compile and install

#. (Optional). Download and compile ITK at: http://www.itk.org

   #. Prepare your directory structure. For simplicity, I will assume everything
      linked to ITK will be in the same folder. This folder will contain three
      sub-folder:

        ``ITK_Source``

            Contains the source code of ITK

        ``ITK_Build``

            Will contain the build files of ITK

        ``ITK``

            Will contain the installed version of ITK

   #. Make sure the three directories exist, and ``ITK_Source`` contains the
      source code.
   #. Launch a windows command and setup your environment variables for MinGW64
   #. From the main directory, configure ITK::

        $ cd ITK_Build
        $ cmake -G "MinGW Makefiles" -DCMAKE_CXX_FLAGS="-std=c++11" -DCMAKE_BUILD_TYPE=Release -DBUILD_TESTING=OFF -DBUILD_EXAMPLES=OFF -DBUILD_SHARED_LIBS=ON -DModule_ITKReview=ON -DCMAKE_INSTALL_PREFIX=..\ITK ..\ITK_Source

   #. Then, compile and install ITK::

        $ mingw32-make install

#. (Optional). Download and compile VTK at: http://www.vtk.org

   #. Prepare your directory structure. For simplicity, I will assume everything
      linked to VTK will be in the same folder. This folder will contain three
      sub-folder:

        ``VTK_Source``

            Contains the source code of VTK

        ``VTK_Build``

            Will contain the build files of VTK

        ``VTK``

            Will contain the installed version of VTK

   #. Create a build directory, for example call ``VTK_build``
   #. Launch a windows command and setup your environment variables for MinGW64
   #. From the main directory, configure VTK::

        $ cd VTK_Build
        $ cmake -G "MinGW Makefiles" -DCMAKE_CXX_FLAGS="-std=c++11" -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=..\VTK ..\VTK_Source

   #. Then, compile and install VTK::

        $ mingw32-make install

#. (Optional, for extra modules only) Download the GNU scientific library from their website.

   #. Prepare your directory structure. For simplicity, I will assume everything
      linked to the GSL will be in the same folder. This folder will contain two
      sub-folder:

        ``GSL_Source``

            Contains the source code of the GSL

        ``GSL``

            Will contain the installed version of the GSL

   #. Launch the MSys shell
   #. Setup the environment variables for MinGW64
   #. Create a build directory and change to it (optional)
   #. Run the configure script, specifying a prefix::

        $ cd GSL_Source
        $ configure --prefix=../GSL

   #. Compile and install::

        $ make && make install

#. Configure LithoGraphX for compilation

   #. Create a build directory
   #. Start a x64 Native Tools Command Prompt from the Visual Studio
      tools.
   #. Setup the MinGW64 environment variables::

        $ call mingw64.bat

   #. Change the current folder to the build directory you created
   #. Run cmake-gui, specifying the path to the source. For example, if you
      created your build folder as a sub-folder in the source tree::

        $ cmake-gui ..

   #. Select the ``MinGW Makefiles`` generator
   #. Check the ``Grouped`` check box to simplify the interface. This will put
      together all the options starting with the same work.
   #. Press the ``Configure`` button and check errors until there are none left.
      Typically you will need to inform the following variables have (in order
      in which they will appear):

        CUDA_CL_VERSION

            You need to set this to your version of Visual Studio, so here 2013.

        TIFF_INCLUDE_DIR

            This should be the ``include`` of the installation folder for
            libtiff.

        TIFF_LIBRARY

            This should be the file ``libtiff.a`` in the ``lib`` folder of the
            installation of libtiff.

        Eigen_SIGNATURE

            You need to select the file ``signature_of_eigen3_matrix_library``
            in the Eigen folder.

        SIP_EXECUTABLE

            If this is not found by default, this is because you didn't install
            sip in the default python location. It should be in the same folder
            as the python you used to compile it. If this is the case, you
            should activate the advanced options and make sure you select the
            python interpreter SIP is compiled for.

        SIP_INCLUDE_PATH

            This is the ``include`` folder, wherever the sip executable is

        SIP_PYQT5_PATH

            This should be the ``sip\PyQt5`` folder, still where the sip
            executable is.

        PYTHON_LIBRARY

            To access this variable, you need to show the advance options. By
            default, this will look for ``python27.lib``. If you use Anaconda,
            you need to install ``libpython`` and select the file
            ``libpython27.a`` instead.

        BUILD_EXTRA_INCLUDES

            If you didn't install CImg in a standard path, chances are, you will
            need to add its path in this variable, for example if it is not
            found. This shouldn't be a problem if you copy CImg.h in the ``src``
            folder of LithoGraphX.

        CMAKE_BUILD_TYPE

            Set this to ``Release`` for maximal performance.

        CMAKE_INSTALL_PREFIX

            Choose the folder where LithoGraphX will be installed. If this is in
            ``Program Files``, make sure the folder already exists and you can
            write in it, as normal programs cannot create a folder there.

        BUILD_ITK

            If you have built ITK, check this box

        BUILD_VTK

            If you have built VTK, check this box

#. Without closing the command line, start the compilation::

    $ mingw32-make

#. Then, install::

    $ mingw32-make install


You can contact the development team to get a zip file with all the versions we
are using currently.

.. _compile_msvc:

Compilation with Visual Studio 2013
-----------------------------------

Note the following process should also work with other versions of Visual
Studio. However, I haven't tested it, so it is not granted.

#. Install Visual Studio 2013, for example the community edition which is
   available for free on http://www.visualstudio.com. From this version, you
   only need the C++ component.
#. Install the CUDA drivers. **Note:** These must be installed after Visual
   Studio, so CUDA find it and install all the necessary components.
#. Download the source code of SIP and PyQt5 from http://www.riverbankcomputing.com
#. Download the version of Qt5 corresponding to the version of PyQt5 you
   downloaded.
#. Download Python 2.7 or 3.4. If you are an academic, I recommend using
   the Anaconda distribution.
#. Compilation of SIP
    #. Start a x64 Native Tools Command Prompt from the Visual Studio
       tools.

    #. Configure sip. At this time, only Visual Studio 2010 is supported, but it
       seems to work for Visual Studio 2013 too::

        $ python configure.py -p win32-msvc2010

    #. Compile and install::

        $ nmake
        $ nmake install

#. Compilation of PyQt5
    #. Start the same command prompt used for SIP (you can re-use the one open for SIP)
    #. Configure PyQt5::

        $ python configure.py --spec=win32-msvc2013

    #. Compile and install::

        $ nmake
        $ nmake install

#. Download the source code of libtiff5 from the GNUWin32 project:
   http://gnuwin32.sourceforge.net/packages/tiff.htm

    #. Launch the x64 Native Tools Command Prompt
    #. Compile::

        $ nmake /f Makefile.vc

    #. The programs may fail to compile, but the library should be fine. It will
       be in the ``libtiff`` folder.

#. Download CImg from http://cimg.sourceforge.net/. Just uncompress it
   somewhere: this is only a header file, so there is no compilation necessary.
   Also, copy the header file of CImg in the ``src`` folder of LithoGraphX.

#. (Optional). Download and compile ITK at: http://www.itk.org

   #. Prepare your directory structure. For simplicity, I will assume everything
      linked to ITK will be in the same folder. This folder will contain three
      sub-folder:

        ``ITK_Source``

            Contains the source code of ITK

        ``ITK_Build``

            Will contain the build files of ITK

        ``ITK``

            Will contain the installed version of ITK

   #. Make sure the three directories exist, and ``ITK_Source`` contains the
      source code.
   #. Launch a VS2013 x64 Native Tools Command Prompt
   #. From the main directory, configure ITK::

        $ cd ITK_Build
        $ cmake -G "NMake Makefiles" -DCMAKE_BUILD_TYPE=Release -DBUILD_TESTING=OFF -DBUILD_EXAMPLES=OFF -DBUILD_SHARED_LIBS=ON -DModule_ITKReview=ON -DCMAKE_INSTALL_PREFIX=..\ITK ..\ITK_Source

   #. Then, compile and install ITK::

        $ nmake install

#. (Optional). Download and compile VTK at: http://www.vtk.org

   #. Prepare your directory structure. For simplicity, I will assume everything
      linked to VTK will be in the same folder. This folder will contain three
      sub-folder:

        ``VTK_Source``

            Contains the source code of VTK

        ``VTK_Build``

            Will contain the build files of VTK

        ``VTK``

            Will contain the installed version of VTK

   #. Create a build directory, for example call ``VTK_build``
   #. Launch a VS2013 x64 Native Tools Command Prompt
   #. From the main directory, configure VTK::

        $ cd VTK_Build
        $ cmake -G "NMake Makefiles" -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=..\VTK ..\VTK_Source

   #. Then, compile and install VTK::

        $ nmake install


#. Create a ``build`` folder, either in or outside of the LithoGraphX folder

#. Run cmake, and select correct the source code and build binary folders

   #. You can use either the ``Visual Studio 2013 Win64`` or the ``NMake
      Makefiles`` generator.
   #. Check the ``Grouped`` check box to simplify the interface. This will put
      together all the options starting with the same work.
   #. Press the ``Configure`` button and check errors until there are none left.
      Typically you will need to inform the following variables have (in order
      in which they will appear):

        CUDA_CL_VERSION

            You need to set this to your version of Visual Studio, so here 2013.

        CUDA_HOST_COMPILER

            You need to put here the path to the cl program. To get this, from
            the command line you have opened earlier, type::

                $ where cl

            And copy the path writen.

        TIFF_INCLUDE_DIR

            This should be the ``libtiff`` sub-folder where you compiled libtiff
            in a previous step

        TIFF_LIBRARY

            This should be the file ``libtiff.lib`` in the same folder as for
            TIFF_INCLUDE_DIR

        Eigen_SIGNATURE

            You need to select the file ``signature_of_eigen3_matrix_library``
            in the Eigen folder.

        SIP_EXECUTABLE

            If this is not found by default, this is because you didn't install
            sip in the default python location. It should be in the same folder
            as the python you used to compile it. If this is the case, you
            should activate the advanced options and make sure you select the
            python interpreter SIP is compiled for.

        SIP_INCLUDE_PATH

            This is the ``include`` folder, wherever the sip executable is

        SIP_PYQT5_PATH

            This should be the ``sip\PyQt5`` folder, still where the sip
            executable is.

        BUILD_EXTRA_INCLUDES

            If you didn't install CImg in a standard path, chances are, you will
            need to add its path in this variable, for example if it is not
            found. This shouldn't be a problem if you copy CImg.h in the ``src``
            folder of LithoGraphX.

        CMAKE_INSTALL_PREFIX

            Choose the folder where LithoGraphX will be installed. If this is in
            ``Program Files``, make sure the folder already exists and you can
            write in it, as normal programs cannot create a folder there.


   #. Press the ``Generate`` button
#. If you used the ``NMake Makefile`` generator, open a x64 Native Tools Command
   Prompt, and compile LithoGraphX::

        $ nmake install

   Otherwise, open the file ``ALL_BUILD.vcxproj``, select the ``Release``
   compilation (the default is ``Debug``), click on the ``INSTALL`` solution and
   compile with the ``Build INSTALL solution`` entry from the ``Build`` menu.
   **Note:** The entry will only be available if the ``INSTALL`` solution is the
   one currently selected on the left pane.
