/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "MultiStackGammaFilter.hpp"
#include "StackGammaFilter.hpp"

#include <Progress.hpp>

namespace lgx {
namespace process {
bool MultiStackGammaFilter::operator()(const Store* input, Store* output, float gamma, int n)
{
  // Check gamma
  if(gamma <= 0)
    return setErrorMessage("Gamma must be positive");
  if(n <= 0)
    return setErrorMessage("Passes must be strictly positive");

  StackGammaFilter filter(*this);

  Progress progress(QString("Running Multi Gamma Filter"), n);

  filter(input, output, gamma);
  if(not progress.advance(1))
    userCancel();
  for(int i = 1 ; i < n ; ++i) {
    filter(output, output, gamma);
    if(not progress.advance(1))
      userCancel();
  }

  return true;
}
}
}
