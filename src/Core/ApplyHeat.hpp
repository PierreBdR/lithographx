/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef APPLYHEAT_HPP
#define APPLYHEAT_HPP

#include <Process.hpp>

namespace lgx {
namespace process {

class ApplyHeat : public MeshProcess {
public:
  ApplyHeat(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  bool operator()(const ParmList& parms) override
  {
    if(!checkState().mesh(MESH_NON_EMPTY))
      return false;
    Mesh* m = currentMesh();
    bool ok;
    float heat = parms[1].toFloat(&ok);
    if(not ok) return setErrorMessage("Error, invalid value for Heat parameter");
    float minHeat = parms[2].toFloat(&ok);
    if(not ok) return setErrorMessage("Error, invalid value for minHeat parameter");
    float maxHeat = parms[3].toFloat(&ok);
    if(not ok) return setErrorMessage("Error, invalid value for maxHeat parameter");
    return operator()(m, parms[0].toString(), heat, minHeat, maxHeat);
  }

  bool operator()(Mesh* mesh, const QString& heatUnit, float heat, float minHeat, float maxHeat);

  QString folder() const override {
    return "Heat Map";
  }
  QString name() const override {
    return "Apply Heat";
  }
  QString description() const override {
    return "Apply heat to selected labels and scale the heat to range";
  }
  QStringList parmNames() const override
  {
    return QStringList() << "Units"
                         << "Heat"
                         << "Min Heat"
                         << "Max Heat";
  }
  QStringList parmDescs() const override
  {
    return QStringList() << "Units for the new heat."
                         << "New heat value to apply."
                         << "Lower bound for heat."
                         << "Upper bound for heat.";
  }
  ParmList parmDefaults() const override
  {
    return ParmList() << ""
                      << "1.0"
                      << "1.0"
                      << "10.0";
  }
  QIcon icon() const override {
    return QIcon(":/images/ApplyHeat.png");
  }
};
}
}
#endif
