#include <QCoreApplication>
#include <QProcessEnvironment>
#include <QTextStream>
#include <stdio.h>
#include <QFileInfo>
#include <QDir>

// this program finds the python.exe in the path with the right version number

#define PY_VERSION "Python @LithoGraphX_PYTHON_VERSION@"

QTextStream out(stdout);

bool testPath(const QString& path) {
    QString python = QDir(path).filePath("python.exe");
    auto pyFile = QFileInfo(python);
    if(not pyFile.isFile() or not pyFile.isExecutable())
        return false;
    QProcess proc;
    proc.setProcessChannelMode(QProcess::MergedChannels);
    proc.start(python, QStringList() << "--version");
    if(not proc.waitForFinished())
        return false;
    auto output = QString::fromUtf8(proc.readAll()).split("\n");
    for(const auto& line: output) {
        if(line.startsWith(PY_VERSION)) {
            // and check there is a site.py attached to this version
            auto site_path = QDir(path);
            if(site_path.cd("lib")) {
                auto site_fi = QFileInfo(site_path.filePath("site.py"));
                if(site_fi.isFile() and site_fi.isReadable())
                    return true;
            }
            break;
        }
    }
    return false;
}

int main()
{
    auto env = QProcessEnvironment::systemEnvironment();
    if(env.contains("LGX_PYTHON")) {
        auto pth = env.value("LGX_PYTHON");
        if(testPath(pth)) {
            out << pth << endl;
            return 0;
        }
    }
    if(env.contains("PYTHONHONE")) {
        auto pth = env.value("PYTHONHONE");
        if(testPath(pth)) {
            out << pth << endl;
            return 0;
        }
    }
    if(env.contains("PATH")) {
        QStringList paths = env.value("PATH").split(";");
        for(const auto& pth : paths) {
            if(testPath(pth)) {
                out << pth << endl;
                return 0;
            }
        }
    }
    return 1;
}
