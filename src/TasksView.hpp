/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef TASKSVIEW_H
#define TASKSVIEW_H

#include <LGXConfig.hpp>

#include <QTreeView>

class QKeyEvent;

namespace lgx {
namespace gui {

class TasksView : public QTreeView {
  Q_OBJECT
public:
  TasksView(QWidget* parent = 0);

  static const QString internal_format;
  static const QString itemlist_format;

protected:
  void keyPressEvent(QKeyEvent* event);
  void dragEnterEvent(QDragEnterEvent* event);
  void dragLeaveEvent(QDragLeaveEvent* event);
  void dragMoveEvent(QDragMoveEvent* event);

signals:
  void deleteItems(const QModelIndexList& idx);
};

} // namespace lgx
} // namespace gui
#endif
