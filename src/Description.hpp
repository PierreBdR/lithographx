/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef DESCRIPTION_HPP
#define DESCRIPTION_HPP

#include <LGXConfig.hpp>

#include <QString>
#include <QStringList>
#include <utility>
#include <vector>

namespace lgx {
/**
 * \class Description Description.hpp <Description.hpp>
 *
 * Class holding a description, that is a type and a set of fields with values, all stored as strings.
 *
 * The fields are encoded with an association map, so has to keep the ordering. Note that this is reasonably efficient
 * only as long as the number of fields is small.
 */
class LGX_EXPORT Description {
  typedef std::vector<std::pair<QString, QString> > container_t;

public:
  typedef container_t::value_type value_type;
  typedef container_t::reference reference;
  typedef container_t::const_reference const_reference;
  typedef container_t::pointer pointer;
  typedef container_t::const_pointer const_pointer;
  typedef container_t::iterator iterator;
  typedef container_t::const_iterator const_iterator;
  typedef container_t::reverse_iterator reverse_iterator;
  typedef container_t::const_reverse_iterator const_reverse_iterator;

  /**
   * Create a new, empty, description object
   */
  explicit Description(const QString& type = QString())
    : _type(type)
  {
  }

  /**
   * Copy a description
   */
  Description(const Description& copy) = default;

  /**
   * Move a description
   */
  Description(Description&& copy) = default;


  /**
   * Copy a description onto another one
   */
  Description& operator=(const Description&) = default;

  /**
   * Copy a description onto another one
   */
  Description& operator=(Description&&) = default;

  /**
   * Swap the content of two descriptions
   */
  void swap(Description& other)
  {
    using std::swap;
    swap(_type, other._type);
    swap(_fields, other._fields);
  }

  /**
   * Get/Set the type of the object described
   */
  QString& type() {
    return _type;
  }
  /**
   * Get/Set the type of the object described
   */
  const QString& type() const {
    return _type;
  }

  /**
   * Get the name of a field
   */
  QString& fieldName(size_t idx);

  /**
   * Get the name of a field
   */
  const QString& fieldName(size_t idx) const;

  /**
   * Get the list of all fields
   */
  QStringList fieldNames() const;

  /**
   * Get the value of a field by index.
   */
  QString& operator[](size_t idx);

  /**
   * Get the value of a field by index.
   */
  const QString& operator[](size_t idx) const;

  /**
   * Get the value of a field by name.
   *
   * If the fields doesn't exist, a new one gets created at the end.
   */
  QString& operator[](const QString& name);

  /**
   * Get the value of a field.
   *
   * If the field doesn't exist, \c def is returned instead
   */
  QString field(const QString& name, const QString& def = QString()) const;

  /**
   * Get the value of a field.
   *
   * If the field doesn't exist, \c def is returned instead
   */
  QString field(size_t i, const QString& def = QString()) const;

  /**
   * Number of fields
   */
  size_t size() const {
    return _fields.size() / 2;
  }

  /**
   * Are there any fields?
   */
  bool empty() const {
    return _fields.empty();
  }

  /**
   * Get a comma separated value representation of this description
   */
  QString toCSV() const;

  /**
   * Get a list of values representing this description
   */
  QStringList toList() const;

  /**
   * Create a description from a comma separated value string
   *
   * \param value String using CSV to represent the description
   * \param ok If provided, its value is true if value was valid, false otherwise
   */
  static Description fromCSV(const QString& value, bool* ok = 0);

  /**
   * Create a description from a list of values
   *
   * \param value list of values from which the description is created
   * \param ok If provided, its value is true if value was valid, false otherwise
   */
  static Description fromList(const QStringList& value, bool* ok = 0);

  /**
   * Add a field at the end of the current description
   */
  Description& operator<<(const std::pair<QString, QString>& field);

  /**
   * Remove a field by name
   */
  bool remove(const QString& name);

  /**
   * Remove a field by position
   */
  bool remove(size_t i);

  /**
   * Clear the description object from type and fields
   */
  void clear()
  {
    _type.clear();
    _fields.clear();
  }

  //@{
  /// \name STL methods
  reference front() {
    return _fields.front();
  }
  const_reference front() const {
    return _fields.front();
  }

  reference back() {
    return _fields.back();
  }
  const_reference back() const {
    return _fields.back();
  }

  iterator begin() {
    return _fields.begin();
  }
  iterator end() {
    return _fields.end();
  }

  const_iterator begin() const {
    return _fields.begin();
  }
  const_iterator end() const {
    return _fields.end();
  }

  reverse_iterator rbegin() {
    return _fields.rbegin();
  }
  reverse_iterator rend() {
    return _fields.rend();
  }

  const_reverse_iterator rbegin() const {
    return _fields.rbegin();
  }
  const_reverse_iterator rend() const {
    return _fields.rend();
  }

  void erase(iterator it) {
    _fields.erase(it);
  }
  //@}

protected:
  QString _type;
  container_t _fields;
};

/**
 * Swap the content of two descriptions
 */
inline void swap(Description& d1, Description& d2) {
  d1.swap(d2);
}
} // namespace lgx
#endif // DESCRIPTION_HPP
