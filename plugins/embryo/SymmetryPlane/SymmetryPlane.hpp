#ifndef SymmetryPlane_HPP
#define SymmetryPlane_HPP

#include <Process.hpp>

namespace process
{
class SymmetryPlane : public StackProcess
{
public:
    SymmetryPlane(const StackProcess& process)
        : Process(process)
        , StackProcess(process)
    { }
    
    bool operator()(const QStringList& strings, const QList<float>& )
    {
        if(!checkState().stack(STACK_NON_EMPTY))
            return false;
        QString axis = strings[0].toLower();
        int ax = 2;
        if(axis == "x")
            ax = 0;
        else if(axis == "y")
            ax = 1;
        else if(axis == "z")
            ax = 2;
        else
            return setErrorMessage("Error, the axis must be one of 'x', 'y' or 'z'");
        return operator()(currentStack(), ax, stringToBool(strings[1]), stringToBool(strings[2]));
    }
    
    bool operator()(Stack *stk, int axis_inverted, bool apply_to_stack, bool apply_to_other_stack);
    
    QString folder() const { return "Misc"; }
    QString name() const { return "Find Symmetry Plane"; }
    QString description() const { return "Find the plane of symmetry of a stack. The stack must have been inverted and rotated such that it came back to its original position."; }
    QStringList parameters() const { return QStringList() << "Axis inverted" << "Apply to current stack" << "Apply to other stack"; }
    QStringList default_strings() const { return QStringList() << "z" << "Yes" << "Yes"; }
    StringChoiceMap string_choice() const {
        StringChoiceMap map;
        map[0] = QStringList() << "x" << "y" << "z";
        map[1] = boolean_choice();
        map[2] = boolean_choice();
        return map;
    }
    QList<float> default_values() const { return QList<float>(); }
    QIcon icon() const
    {
        return QIcon(":/images/SymmetryPlane.png");
    }
    
};
}

#endif // SymmetryPlane_HPP


