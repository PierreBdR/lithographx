/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef MESH_PROCESSS_PDG_HPP
#define MESH_PROCESSS_PDG_HPP

#include <Process.hpp>
#include <MeshProcessCellMesh.hpp>
#include <MeshProcessCellAxis.hpp>

namespace lgx {
namespace process {
///\addtogroup MeshProcess
///@{
/**
 * \class CorrespondanceJunctions <MeshProcessGrowth.hpp>
 */
class LGXCORE_EXPORT CorrespondanceJunctions : public MeshProcess {
public:
  CorrespondanceJunctions(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  bool operator()(const ParmList& parms) override
  {
    if(!checkState().mesh(MESH_NON_EMPTY, 0).mesh(MESH_NON_EMPTY, 1))
      return false;
    Mesh* mesh1, *mesh2;
    if(currentMesh() == mesh(0)) {
      mesh1 = mesh(0);
      mesh2 = mesh(1);
    } else if(currentMesh() == mesh(1)) {
      mesh2 = mesh(0);
      mesh1 = mesh(1);
    } else
      return false;

    bool res = (*this)(mesh1, mesh2, parmToBool(parms[0]), parmToBool(parms[1]));
    return res;
  }

  bool operator()(Mesh* mesh1, Mesh* mesh2, bool eraseMarginCells, bool ShowVVCorrespondance);

  QString folder() const override {
    return "Cell Axis/Growth 2D";
  }
  QString name() const override {
    return "Check Correspondence";
  }
  QString description() const override
  {
    return "Find matching cell junctions between 2 meshes based on parent labeling.\n"
           "Both meshes are simplified with Make Cells to keep only the cell junctions and centers.\n"
           "SAVE your meshes before running!";
  }
  QStringList parmNames() const override
  {
    return QStringList() << "Erase margin cells"
                         << "Show correspondence";
  }
  QStringList parmDescs() const override
  {
    return QStringList() << "Take away the cells touching the mesh outer edge while converting to a cellular mesh."
                         << "Draw connecting lines between corresponding junctions.";
  }
  ParmList parmDefaults() const override
  {
    return ParmList() << true
                      << false;
  }
  ParmChoiceMap parmChoice() const override
  {
    ParmChoiceMap map;
    map[0] = map[1] = booleanChoice();
    return map;
  }
  QIcon icon() const override {
    return QIcon(":/images/CorrespondanceJunctions.png");
  }
};

class LGXCORE_EXPORT LoadGrowth2D : public LoadCellAxis {
public:
  LoadGrowth2D(const MeshProcess& process)
    : Process(process)
    , LoadCellAxis(process)
  {
  }

  bool operator()(Mesh* mesh, const QString& filename) override;

  QString folder() const override {
    return "Cell Axis/Growth 2D";
  }
  QString name() const override {
    return "Load Growth 2D";
  }
  QString description() const override {
    return "Load 2D growth to a spreadsheet file";
  }
};

class LGXCORE_EXPORT SaveGrowth2D : public SaveCellAxis {
public:
  SaveGrowth2D(const MeshProcess& process)
    : Process(process)
    , SaveCellAxis(process)
  {
  }

  bool operator()(Mesh* mesh, const QString& filename);

  QString folder() const override {
    return "Cell Axis/Growth 2D";
  }
  QString name() const override {
    return "Save Growth 2D";
  }
  QString description() const override {
    return "Save 2D growth to a spreadsheet file";
  }
};

/**
 * \class ComputeGrowth2D <MeshProcessGrowth.hpp>
 */
class LGXCORE_EXPORT ComputeGrowth2D : public MeshProcess {
public:
  ComputeGrowth2D(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  bool operator()(const ParmList& parms) override
  {
    if(!checkState().mesh(MESH_NON_EMPTY, 0).mesh(MESH_NON_EMPTY, 1))
      return false;

    Mesh* mesh1, *mesh2;
    if(currentMesh() == mesh(0)) {
      mesh1 = mesh(0);
      mesh2 = mesh(1);
    } else if(currentMesh() == mesh(1)) {
      mesh2 = mesh(0);
      mesh1 = mesh(1);
    } else
      return false;

    bool ok;
    float dt = parms[2].toFloat(&ok);
    if(!ok)
      return setErrorMessage("Error, argument 'dt' must be a number.");
    bool res = (*this)(mesh1, mesh2, parms[0].toString(), parms[1].toString(), dt);

    if(res)
      showResult(mesh1, mesh2);

    return res;
  }
  void showResult(Mesh* mesh1, Mesh* mesh2);

  bool operator()(Mesh* mesh1, Mesh* mesh2, QString measure, QString ordering, float dt);

  QString folder() const override {
    return "Cell Axis/Growth 2D";
  }
  QString name() const override {
    return "Compute 2D Growth";
  }
  QString description() const override
  {
    return "Compute PDGs based on correspondence between junctions.\n"
           "Adapted from Goodall and Green, 'Quantitative Analysis of Surface Growth.'"
           "Botanical Gazette (1986) \n"
           "and Dumais and Kwiatkowska, 'Analysis of surface growth in shoot apices.'"
           "Plant Journal (2002)";
  }
  QStringList parmNames() const override
  {
    return QStringList() << "Measure"
                         << "Axes ordering"
                         << "Time-step";
  }
  ParmList parmDefaults() const override
  {
    return ParmList() << "Strain"
                      << "Absolute"
                      << 0.;
  }

  ParmChoiceMap parmChoice() const override
  {
    ParmChoiceMap map;
    map[0] = QStringList() << "Strain"
                           << "Stretch ratio"
                           << "Engineering Strain";
    map[1] = QStringList() << "Absolute"
                           << "Relative";
    return map;
  }

  QIcon icon() const override {
    return QIcon(":/images/PDG.png");
  }
};

/**
 * \class DisplayGrowth2D <MeshProcessGrowth.hpp>
 */
class LGXCORE_EXPORT DisplayGrowth2D : public MeshProcess {
public:
  DisplayGrowth2D(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  bool operator()(const ParmList& parms) override
  {
    if(!checkState().mesh(MESH_NON_EMPTY))
      return false;

    bool ok;
    float anisotropyThreshold = parms[5].toFloat(&ok);
    if(!ok)
      return setErrorMessage("Error, argument 'Threshold' must be a number");
    float axisLineWidth = parms[6].toFloat(&ok);
    if(!ok)
      return setErrorMessage("Error, argument 'Line Width' must be a number");
    float scaleAxisLength = parms[7].toFloat(&ok);
    if(!ok)
      return setErrorMessage("Error, argument 'Line Scale' must be a number");
    float axisOffset = parms[8].toFloat(&ok);
    if(!ok)
      return setErrorMessage("Error, argument 'Line Offset' must be a number");

    return (*this)(currentMesh(), parms[1].toString(), QColor(parms[3].toString()),
                   QColor(parms[4].toString()), axisLineWidth, scaleAxisLength,
                   axisOffset, parms[0].toString(), parms[2].toString(), anisotropyThreshold);
  }

  bool operator()(Mesh* mesh, QString anisotropy, const QColor& ColorExpansion, const QColor& ColorShrinkage,
                  float axisLineWidth, float scaleAxisLength, float axisOffset, const QString displayHeatMap,
                  const QString displayStretch, float anisotropyThreshold);

  QString folder() const override {
    return "Cell Axis/Growth 2D";
  }
  QString name() const override {
    return "Display 2D Growth";
  }
  QString description() const override {
    return "Display the principle growth directions";
  }
  QStringList parmNames() const override
  {
    return QStringList() << "Heatmap"
                         << "Anisotropy"
                         << "Show Axis"
                         << "Color +"
                         << "Color -"
                         << "Threshold"
                         << "Line Width"
                         << "Line Scale"
                         << "Line Offset";
  }
  ParmList parmDefaults() const override
  {
    return ParmList() << "kmax"
                      << "1-kmin/kmax"
                      << "both"
                      << "white"
                      << "red"
                      << 0
                      << 2
                      << 10
                      << 0.1;
  }
  ParmChoiceMap parmChoice() const override
  {
    ParmChoiceMap map;
    map[0] = QStringList() << "none"
                           << "kmax"
                           << "kmin"
                           << "karea"
                           << "anisotropy";
    map[1] = QStringList() << "1-kmin/kmax"
                           << "kmax/kmin"
                           << "|(kmax-kmin)/(kmax+kmin)|";
    map[2] = QStringList() << "both"
                           << "kmax"
                           << "kmin"
                           << "none";
    return map;
  }
  QIcon icon() const override {
    return QIcon(":/images/PDG.png");
  }
};
///@}
} // namespace process // namespace process
} // namespace lgx // namespace lgx

#endif
