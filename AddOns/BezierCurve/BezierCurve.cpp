#include "MakeBezier.hpp"
#include "BezierReferenceSystemProcess.hpp"

namespace lgx {
namespace process {

REGISTER_MESH_PROCESS(FindBezier);
REGISTER_MESH_PROCESS(ReverseBezierCurveProcess);
REGISTER_MESH_PROCESS(BezierReferenceSystemProcess);

}
}
