/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef PYTHONSCRIPT_HPP
#define PYTHONSCRIPT_HPP

#include <python/LGXPython.hpp>

namespace lgx {
namespace process {

/**
 * \class PythonScript PythonScript.hpp <PythonScript.hpp>
 *
 * This process evaluate a Python script from which other processes can be called.
 *
 * \ingroup GlobalProcess
 */
class LGXPYTHON_EXPORT PythonScript : public PythonProcess<GlobalProcess> {
public:
  PythonScript(const GlobalProcess& process)
    : Process(process)
    , PythonProcess<GlobalProcess>(process)
  {
  }

  bool initialize(ParmList& parms, QWidget* parent) override;

  bool operator()(const ParmList& parms) override {
    return (*this)(parms[0].toString());
  }

  bool operator()(QString filename);

  QString name() const override {
    return "Python Script";
  }
  QString folder() const override {
    return "Python";
  }
  QString description() const override {
    return "Run python script";
  }
  QStringList parmNames() const override {
    return QStringList() << "Script"
                         << "Choose file";
  }
  QStringList parmDescs() const override {
    return QStringList() << "Script"
                         << "If false and a script name is provided, the dialog box to choose the script won't be shown";
  }
  ParmList parmDefaults() const override {
    return ParmList() << ""
                         << "True";
  }
  ParmChoiceMap parmChoice() const override {
    ParmChoiceMap map;
    map[1] = booleanChoice();
    return map;
  }

  QIcon icon() const override {
    return QIcon(":/images/Python.png");
  }
};
} // namespace process
} // namespace lgx

#endif // PYTHONSCRIPT_HPP
