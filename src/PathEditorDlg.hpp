/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef PATHEDITORDLG_HPP
#define PATHEDITORDLG_HPP

#include <LGXConfig.hpp>

#include <ui_PathEditorDlg.h>

namespace qglviewer {
class KeyFrameInterpolator;
} // namespace qglviewer

class LGXCamera;

class PathEditorModel : public QAbstractItemModel {
  Q_OBJECT
public:
  PathEditorModel(LGXCamera* c, QObject* parent);

  int rowCount(const QModelIndex& parent = QModelIndex()) const;

  int columnCount(const QModelIndex& /*parent*/ = QModelIndex()) const {
    return 1;
  }

  QVariant data(const QModelIndex& index, int role) const;

  QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;

  Qt::ItemFlags flags(const QModelIndex& index) const;

  QModelIndex index(int row, int column, const QModelIndex& parent = QModelIndex()) const;

  QModelIndex parent(const QModelIndex& index) const;

  bool isPath(const QModelIndex& idx) const;

  bool isKeyframe(const QModelIndex& idx) const;

  quint32 pathId(const QModelIndex& idx) const;

  quint32 keyframeId(const QModelIndex& idx) const;

  qglviewer::KeyFrameInterpolator* path(const QModelIndex& idx) const;

  void moveTo(const QModelIndex& idx) const;

  LGXCamera* camera;
};

class PathEditorDlg : public QDialog {
  Q_OBJECT
public:
  PathEditorDlg(QWidget* parent, LGXCamera* camera);

protected slots:
  void on_buttonBox_clicked(QAbstractButton* button);
  void on_pathsView_clicked(const QModelIndex& idx);
  void on_pathsView_doubleClicked(const QModelIndex& idx);

private:
  Ui_PathEditorDlg ui;
  mutable LGXCamera* camera;
  PathEditorModel* model;
};

#endif // PATHEDITORDLG_HPP
