/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef SHADER_HPP
#define SHADER_HPP

#include <LGXConfig.hpp>
#include <GL.hpp>

#include <Matrix.hpp>
#include <Parms.hpp>
#include <Vector.hpp>

#include <QHash>
#include <QString>

#include <iostream>
#include <string>
#include <string.h>
#include <vector>

/**
 * \define CHECK_GL_ERROR(cmd)
 *
 * Execute the command and show if OpenGL reports an error
 *
 * \relates drawing::Shader
 */
#define CHECK_GL_ERROR(cmd) do { cmd; lgx::reportGLError(#cmd, __FILE__, __LINE__); } while(false)
/**
 * \define REPORT_GL_ERROR(str)
 *
 * Check if there is an OpenGL error. "str" will be shown in the line.
 *
 * \relates drawing::Shader
 */
#define REPORT_GL_ERROR(str) lgx::reportGLError(str, __FILE__, __LINE__)

class QString;

namespace lgx {

typedef util::Vector<1,GLint> ivec1;
typedef util::Vector<2,GLint> ivec2;
typedef util::Vector<3,GLint> ivec3;
typedef util::Vector<4,GLint> ivec4;
typedef util::Vector<1,GLuint> uvec1;
typedef util::Vector<2,GLuint> uvec2;
typedef util::Vector<3,GLuint> uvec3;
typedef util::Vector<4,GLuint> uvec4;
typedef util::Vector<1,GLfloat> vec1;
typedef util::Vector<2,GLfloat> vec2;
typedef util::Vector<3,GLfloat> vec3;
typedef util::Vector<4,GLfloat> vec4;
typedef util::Matrix<2,2,GLfloat> mat2;
typedef util::Matrix<3,3,GLfloat> mat3;
typedef util::Matrix<4,4,GLfloat> mat4;

/**
 * Type of a uniform or an attribute
 */
enum GLSL_TYPE
{
  GLSL_INT,
  GLSL_INT2,
  GLSL_INT3,
  GLSL_INT4,
  GLSL_UINT,
  GLSL_UINT2,
  GLSL_UINT3,
  GLSL_UINT4,
  GLSL_FLOAT,
  GLSL_FLOAT2,
  GLSL_FLOAT3,
  GLSL_FLOAT4,
  GLSL_MATRIX2,
  GLSL_MATRIX3,
  GLSL_MATRIX4
};

class LGX_EXPORT GLSLValue {
  class Value {
  public:
    virtual ~Value() {}
    virtual void setUniform(GLint location) const = 0;
    virtual void setAttrib(GLuint location) const = 0;
    virtual QString typeName() const = 0;
    virtual QString toGLSL() const = 0;
    virtual QTextStream& read(QTextStream& s) = 0;
    virtual QTextStream& write(QTextStream& s) const = 0;
    virtual std::istream& read(std::istream& s) = 0;
    virtual std::ostream& write(std::ostream& s) const = 0;
    virtual Value* copy() = 0;
    virtual size_t size() const = 0;
    virtual size_t buffer_size() const = 0;
    virtual void* buffer() = 0;
  } *value;

  template <typename T>
  class ValueImpl : public Value {
  public:
    typedef typename T::value_type value_type;
    typedef void (*uniform_fct)(GLint, GLsizei, const value_type*);
    typedef void (*attrib_fct)(GLuint, const value_type*);
    typedef QString (*type_fct)(GLint);
    typedef QString (*glslstring_fct)(GLint, const value_type*);

    ValueImpl(uniform_fct ufct, attrib_fct afct,
              type_fct tfct, glslstring_fct sfct,
              const T* v, int count)
      : Value()
      , value(v, v + count)
      , glUniform(ufct)
      , glVertexAttrib(afct)
      , typeFct(tfct)
      , glslFct(sfct)
    { }

    ValueImpl(uniform_fct ufct, attrib_fct afct, type_fct tfct, glslstring_fct sfct)
      : Value()
      , glUniform(ufct)
      , glVertexAttrib(afct)
      , typeFct(tfct)
      , glslFct(sfct)
    { }

    ValueImpl(const ValueImpl& copy)
      : Value()
      , value(copy.value)
      , glUniform(copy.glUniform)
      , glVertexAttrib(copy.glVertexAttrib)
      , typeFct(copy.typeFct)
      , glslFct(copy.glslFct)
    { }

    virtual QString typeName() const
    {
      return typeFct((GLint)value.size());
    }
    virtual QString toGLSL() const
    {
      return glslFct((GLint)value.size(), value[0].c_data());
    }

    virtual Value* copy()
    {
      return new ValueImpl(*this);
    }

    virtual void setUniform(GLint location) const
    {
      glUniform(location, (GLint)value.size(), value[0].c_data());
    }
    virtual void setAttrib(GLuint location) const
    {
      glVertexAttrib(location, value[0].c_data());
    }
    virtual QTextStream& read(QTextStream& s)
    {
      value.resize(1);
      s >> value[0];
      return s;
    }
    virtual QTextStream& write(QTextStream& s) const
    {
      s << value[0];
      return s;
    }
    virtual std::istream& read(std::istream& s)
    {
      value.resize(1);
      s >> value[0];
      return s;
    }
    virtual std::ostream& write(std::ostream& s) const
    {
      s << value[0];
      return s;
    }
    virtual size_t size() const
    {
      return value.size();
    }
    virtual size_t buffer_size() const
    {
      return sizeof(T)*value.size();
    }
    virtual void* buffer()
    {
      return &value[0];
    }
    std::vector<T> value;
    uniform_fct glUniform;
    attrib_fct glVertexAttrib;
    type_fct typeFct;
    glslstring_fct glslFct;
  };

  GLSL_TYPE type;

public:
  GLSLValue()
    : value(0)
    , type(GLSL_INT)
  { }

  /**
   * Copy a value
   */
  GLSLValue(const GLSLValue& copy)
    : value(0)
    , type(copy.type)
  {
    if(copy.value) value = copy.value->copy();
  }

  /**
   * Create a value from a C++ object
   */
  template <typename T>
  explicit GLSLValue(const T& val)
    : value(0)
  {
    setValue(val);
  }

  /**
   * Creates a value for a GLSL array from a STL vector
   */
  template <typename T>
  explicit GLSLValue(const std::vector<T>& val)
    : value(0)
  {
    setValue(val);
  }

  /**
   * Creates a value for a GLSL array from a C-style array
   */
  template <typename T>
  explicit GLSLValue(const T* val, int count)
    : value(0)
  {
    setValue(val, count);
  }

  ~GLSLValue()
  {
    delete value;
  }

  /**
   * Copy the value.
   */
  GLSLValue& operator=(const GLSLValue& copy)
  {
    delete value;
    value = 0;
    if(copy.value)
      value = copy.value->copy();
    type = copy.type;
    return *this;
  }

  /**
   * Get the GLSL name of the type
   */
  QString typeName() const { return value->typeName(); }

  /**
   * Get the GLSL expression to create this value
   */
  QString toGLSL() const { return value->toGLSL(); }

  /**
   * Send this value as a uniform for a given location
   */
  void setUniform(GLint location) const
  {
    value->setUniform(location);
  }

  /**
   * Send this value as a attribute to a specific location
   */
  void setAttrib(GLuint location) const
  {
    value->setAttrib(location);
  }

  /**
   * Read the value from a STL C++ stream
   */
  std::istream& read(std::istream& s);
  /**
   * Write the value to a STL c++ stream
   */
  std::ostream& write(std::ostream& s) const;
  /**
   * Read the value from a QTextStream
   */
  QTextStream& read(QTextStream& s);
  /**
   * Write the value to a QTextStream
   */
  QTextStream& write(QTextStream& s) const;

  void setValue(const GLint* value, int count);
  void setValue(const ivec1* value, int count);
  void setValue(const ivec2* value, int count);
  void setValue(const ivec3* value, int count);
  void setValue(const ivec4* value, int count);
  void setValue(const GLuint* value, int count);
  void setValue(const uvec1* value, int count);
  void setValue(const uvec2* value, int count);
  void setValue(const uvec3* value, int count);
  void setValue(const uvec4* value, int count);
  void setValue(const GLfloat* value, int count);
  void setValue(const vec1* value, int count);
  void setValue(const vec2* value, int count);
  void setValue(const vec3* value, int count);
  void setValue(const vec4* value, int count);
  void setValue(const mat2* value, int count);
  void setValue(const mat3* value, int count);
  void setValue(const mat4* value, int count);

  void setValue(const GLint& value) { setValue(&value, 1); }
  void setValue(const ivec1& value) { setValue(&value, 1); }
  void setValue(const ivec2& value) { setValue(&value, 1); }
  void setValue(const ivec3& value) { setValue(&value, 1); }
  void setValue(const ivec4& value) { setValue(&value, 1); }
  void setValue(const GLuint& value) { setValue(&value, 1); }
  void setValue(const uvec1& value) { setValue(&value, 1); }
  void setValue(const uvec2& value) { setValue(&value, 1); }
  void setValue(const uvec3& value) { setValue(&value, 1); }
  void setValue(const uvec4& value) { setValue(&value, 1); }
  void setValue(const GLfloat& value) { setValue(&value, 1); }
  void setValue(const vec1& value) { setValue(&value, 1); }
  void setValue(const vec2& value) { setValue(&value, 1); }
  void setValue(const vec3& value) { setValue(&value, 1); }
  void setValue(const vec4& value) { setValue(&value, 1); }
  void setValue(const mat2& value) { setValue(&value, 1); }
  void setValue(const mat3& value) { setValue(&value, 1); }
  void setValue(const mat4& value) { setValue(&value, 1); }

  /**
   * Is the value valid?
   */
  bool valid() const { return value != 0; }

  /**
   * Test two values for equality
   */
  bool operator==(const GLSLValue& other) const
  {
    if(type != other.type) return false;
    if(value == other.value) return true;
    if(value->buffer_size() != other.value->buffer_size()) return false;
    return memcmp(value->buffer(), other.value->buffer(), value->buffer_size()) == 0;
  }

  /**
   * Test two values for inequality
   */
  bool operator!=(const GLSLValue& other) const
  {
    return !(*this == other);
  }
};

inline QTextStream& operator<<(QTextStream& s, const GLSLValue& ut) { return ut.write(s); }

inline QTextStream& operator>>(QTextStream& s, GLSLValue& ut) { return ut.read(s); }

inline std::ostream& operator<<(std::ostream& s, const GLSLValue& ut) { return ut.write(s); }

inline std::istream& operator>>(std::istream& s, GLSLValue& ut) { return ut.read(s); }

/**
 * Class used to represent shaders
 *
 * There can be up to three shaders: vertex, fragment and geometry.
 *
 * Each shader is defined into a set of fragments that may be specified as a
 * file to read of code.
 *
 * The fragments are concatenated in order before compilation. In addition, the
 * version of the GLSL language used and definitions are inserted at the
 * beginning of the shaders.
 *
 * \note Due to a bug in the OpenGL version of Mac OS X (at least up to 10.6),
 * all parts of a shader program are concatenated together instead of compiled
 * separately. Otherwise, the use of the keyword "discard" would crash the
 * program.
 */
class LGX_EXPORT Shader
{
public:
  enum ActiveTextures {
    AT_NONE = 0,
    AT_TEX3D,
    AT_SECOND_TEX3D,
    AT_TEX2D,
    AT_LABEL_TEX,
    AT_SURF_TEX,
    AT_HEAT_TEX,
    AT_DEPTH_TEX,
    AT_CMAP_TEX,
    AT_SECOND_CMAP_TEX,
    AT_SURF_RENDER_TEX,
    AT_FINAL_VOLUME_TEX,
    AT_FRONT_TEX,
    AT_BACK_TEX,
    AT_FRONT_COLOR_TEX,
    AT_OCCLUSION_TEX,
    AT_END
  };

  typedef std::pair<QString,bool> code_t;

  /**
   * Build a shader for a given version of OpenGL
   *
   * \param version Version to appear in the #version directive of the shader
   * \param verbosity Verbosity of the output
   *
   * Verbosity can be one of:
   *
   * 0. No output
   * 1. Critical errors only
   * 2. All errors
   * 3. Warnings and errors
   * 4. All of the above and minor output about shader compilation
   * 5. All of the above and print the content of the shaders at each compilation
   */
  Shader(QString version = QString(), int verbosity = 2);

  /**
   * Copy the current shader with all uniforms and code
   *
   * The new shader will have to be re-compiled though!
   */
  Shader(const Shader& copy);

  /**
   * Move a shader into a new object.
   *
   * Unlike the copy-constructor, the new object will already be compiled if
   * the old one was.
   *
   * The old program may be in any state and shouldn't be used again without
   * being reset explicitely.
   */
  Shader(Shader&& other);

  /**
   * Copy the code and uniforms of another shader.
   */
  Shader& operator=(const Shader& other);

  /**
   * Move a shader.
   *
   * The other shader shouldn't be used again withough being reset
   * explicitely.
   */
  Shader& operator=(Shader&& other);

  //@{
  ///\name General setup methods

  /**
   * Helping static function to use an active texture.
   *
   * \param at Active texture to use, this should be a number between 0 and
   * GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS-1.
   */
  static void activeTexture(GLenum at)
  {
    opengl->glActiveTexture(at+GL_TEXTURE0);
  }

  /**
   * Initialize the shader system
   *
   * Requires GLEW to be initialized first
   */
  static bool init();

  /**
   * Change the default version of GLSL shaders
   */
  static void setDefaultGLSLVersion(QString v);

  /**
   * Get the default version of GLSL shaders
   */
  static QString defaultGLSLVersion() { return default_version; }

  /**
     * Change the verbosity of the output.
     *
     * See Shader::Shader() for details on the meaning of the verbosity
     */
    void setVerbosity(int verb) { verbosity = verb; }

    //@}

    //@{
    ///\name Vertex shader construction and introspection
  /**
   * Check if the vertex shader fragment \c pos is code and not a file
   */
  bool isVertexShaderCode(unsigned int pos) const
  {
    return vertex_shaders_code[pos].second;
  }

  /**
   * Get the vertex shader fragment at a given position
   *
   * If the fragment is in a file, it will be read.
   */
  QString getVertexShader(unsigned int pos) const;

  /**
   * Add a code fragment at the end of the vertex shader
   */
  void addVertexShaderCode(QString code);
  /**
   * Change a fragment of the vertex shader to a piece of code
   */
  bool changeVertexShaderCode(int pos, QString code);
  /**
   * Remove the fragment of vertex shader containing this code
   */
  void removeVertexShaderCode(QString code);
    /**
     * Add a file fragment at the end of the vertex shader
     */
  void addVertexShader(QString filename);
    /**
     * Change a fragment of the vertex shader to the content of a file
     */
  bool changeVertexShader(int pos, QString filename);
    /**
     * Remove the fragment of the vertex shader containing this file name
     */
  void removeVertexShader(QString filename);
    /**
     * Remove the fragment of the vertex shader at the given position
     */
  void removeVertexShader(int pos);
    /**
     * Remove all fragments from the vertex shader
     */
  void clearVertexShader();

  /**
     * Get the full code of the vertex shader as it is sent to the GPU
     */
    QString vertexShader() const;
    //@}

    //@{
    ///\name Fragment shader construction and introspection
    /**
     * Check if the fragment shader fragment \c pos is code and not a file
     */
    bool isFragmentShaderCode(unsigned int pos) const
    {
      return fragment_shaders_code[pos].second;
    }

    /**
     * Get the fragment shader fragment at a given position
     *
     * If the fragment is in a file, it will be read.
     */
    QString getFragmentShader(unsigned int pos) const;

    /**
   * Add a code fragment at the end of the fragment shader
   */
  void addFragmentShaderCode(QString code);
  /**
   * Change a fragment of the fragment shader to a piece of code
   */
  bool changeFragmentShaderCode(int pos, QString code);
  /**
   * Remove the fragment of fragment shader containing this code
   */
  void removeFragmentShaderCode(QString code);
  /**
   * Add a file fragment at the end of the fragment shader
   */
  void addFragmentShader(QString filename);
  /**
   * Change a fragment of the fragment shader to the content of a file
   */
  bool changeFragmentShader(int pos, QString filename);
  /**
   * Remove the fragment of the fragment shader containing this file name
   */
  void removeFragmentShader(QString filename);
  /**
   * Remove the fragment of the fragment shader at the given position
   */
  void removeFragmentShader(int pos);
  /**
   * Remove all fragments from the fragment shader
   */
  void clearFragmentShader();

  /**
     * Get the full code of the fragment shader as it is sent to the GPU
     */
    QString fragmentShader() const;
    //@}

    //@{
    ///\name Geometry shader construction and introspection
    /**
     * Check if the geometry shader fragment \c pos is code and not a file
     */
    bool isGeometryShaderCode(unsigned int pos) const
    {
      return geometry_shaders_code[pos].second;
    }

    /**
     * Get the geometry shader fragment at a given position
     *
     * If the fragment is in a file, it will be read.
     */
    QString getGeometryShader(unsigned int pos) const;

    /**
   * Add a code fragment at the end of the geometry shader
   */
  void addGeometryShaderCode(QString code);
  /**
   * Change a fragment of the geometry shader to a piece of code
   */
  bool changeGeometryShaderCode(int pos, QString code);
  /**
   * Remove the fragment of geometry shader containing this code
   */
  void removeGeometryShaderCode(QString code);
  /**
   * Add a file fragment at the end of the geometry shader
   */
  void addGeometryShader(QString filename);
  /**
   * Change a fragment of the geometry shader to the content of a file
   */
  bool changeGeometryShader(int pos, QString filename);
  /**
   * Remove the fragment of the geometry shader containing this file name
   */
  void removeGeometryShader(QString filename);
  /**
   * Remove the fragment of the geometry shader at the given position
   */
  void removeGeometryShader(int pos);
  /**
   * Remove all fragments from the geometry shader
   */
  void clearGeometryShader();

  /**
     * Get the full code of the geometry shader as it is sent to the GPU
   */
    QString geometryShader() const;
    //@}

    //@{
    ///\name Whole program methods

  /**
     * Return true if the program has been correctly compiled.
   */
    bool initialized() const { return _initialized; }

  /**
     * Change the version of GLSL used
   */
    void setVersion(QString v) { _version = v; }
  /**
     * Get the version of GLSL used
   */
    const QString& version() const { return _version; }

  /**
     * Reset the shader.
     *
     * This will remove any code and uniform from this shader.
   */
    void reset(bool include_version = false);
  /**
     * Indicate the shader needs re-compiling
   */
    void invalidate() { _initialized = false; }

  /**
     * Compile the shader.
   */
    bool setupShaders();
  /**
     * Set this shader as the current shader for rendering
   *
     * \param validate If set to true, the program will be validated after
     * being activated.
   */
    bool useShaders(bool validate = true);
  /**
   * Delete the shaders from the GPU.
   *
   * After this, you will need to re-compile the shaders to use them.
   */
  void cleanShaders();

  /**
   * Get the id of the compiled program
   */
  GLuint program() const { return _program; }

    //@}

    //@{
    ///\name Uniforms and attributes management
  /**
   * Send all the uniforms to the GPU.
   *
   * This is automatically called by useShader(), but you may need to call it
   * yourself if you change the uniforms after that.
   */
  void setupUniforms();

  /**
   * Get the location of an attribute
   */
  GLint attribLocation(QString name);

  /**
   * Set the value of a named attribute
   *
   * \return false if the attribute doesn't exist
   */
  bool setAttrib(QString name, const GLSLValue& value);
  /**
   * Set an attribute by Id
   */
  void setAttrib(GLint loc, const GLSLValue& value);

  /**
   * Remove a uniform from the list of store uniforms
   */
  bool removeUniform(QString name);
  /**
   * Add a uniform to send everytime the shader is in use.
   *
   * For the uniform to actually be used, you need to call setupUniform() or useShader() after this call.
   */
  bool setUniform(QString name, const GLSLValue& value);
  /**
   * Send a uniform directly to the GPU, without storing it.
   *
   * This call can only be performed when the shader is in use.
   */
  bool setUniform_instant(QString name, const GLSLValue& value);
    /**
     * Get the location of a uniform
     */
  GLint uniformLocation(QString name);

    //@}

    //@{
    ///\name Constant and definitions

    /**
     * Add a constant.
     *
     * Constants are added at the start of each shader as "const type name = value;"
     */
  void setConstant(QString name, const GLSLValue& value);
    /**
     * Remove a constant variable
     */
  void removeConstant(QString name);
    /**
     * List the constants currently defined in the shader
     */
  QStringList constantsNames();
    /**
     * Get the value currently assigned to a constant.
     *
     * If the constant doesn't exist, returns an empty GLSLValue.
     */
  const GLSLValue& getConstant(QString name) const;

    /**
     * Add a definition to the shaders
     *
     * Definitions are added at the start of each shader as "#define NAME VALUE"
     */
  void setDefinition(QString name, QString value = "");
    /**
     * Remove a definition from this shader
     */
  void removeDefinition(QString name);
    /**
     * Get the list of defined symbols for this shader
     */
  QStringList definitionsNames();
    /**
     * Get the current value for this definition
     */
  const QString& getDefinition(QString name) const;
    //@}

    //@{
    ///\name System test static methods
    /**
     * Check whether the system supports shaders
     */
    static bool hasShaders() { return has_shaders; }

    //@}

    //@{
    ///\name General utility functions

    /**
     * Go back to the default rendering mode (e.g. without shader)
     */
    static bool stopUsingShaders();

    /**
     * Get a string describing the type of the shader given as argument
     */
    static QString shaderTypeName(GLenum shader_type);

    /**
     * Compile the shader whose code is contained in the file name
     */
    static GLuint compileShaderFile(GLenum shader_type, QString filename);
    /**
     * Compile a shader whose code is provided directly
     */
    static GLuint compileShader(GLenum shader_type, QString content);

    /**
     * Get the program info log.
     *
     * This is useful when an error occurs while trying to use a shader.
     */
    static void printProgramInfoLog(GLuint object);
    /**
     * Get the program info log.
     *
     * This is useful when an error occurs while compiling a shader.
     */
    static void printShaderInfoLog(GLuint object);

    //@}

  protected:
  bool loadUniform(GLint program, QString name, const GLSLValue& value);

  static bool has_shaders;
  static QString default_version;

  // if bool is true, then it's really code, otherwise, it's a file
  std::vector<code_t> vertex_shaders_code, fragment_shaders_code, geometry_shaders_code;

  std::vector<GLuint> vertex_shaders, fragment_shaders, geometry_shaders;

  std::vector<QString> uniform_names, model_uniform_names;
  std::vector<GLSLValue> uniform_values, model_uniform_values;

  QHash<QString, GLSLValue> constants;
  QHash<QString, QString> definitions;

  int verbosity;

  GLuint _program;

  bool _initialized;
  QString _version;
};

LGX_EXPORT bool reportGLError(const char* id, const char* file, int line);
LGX_EXPORT bool reportGLError(QString id, const char* file, int line);

}

#endif // SHADER_HPP

