/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef STACK_HPP
#define STACK_HPP

#include <LGXConfig.hpp>
#include <GL.hpp>

#include <Geometry.hpp>
#include <LGXViewer/qglviewer.h>
#include <Store.hpp>

namespace lgx {

enum class STORE : unsigned char;

namespace process {
class SetupProcess;
} // namespace process

/**
 * \class Stack Process.hpp <Process.hpp>
 * The Stack class represent the dimensions of the 3D data, and the frames transformations.
 *
 * The stack contains two stores (i.e. two 3D images): main and work. Typical algorithm will read the current store, and
 * write the result in the work store. If, at the end of the process, the main and work store are not of the size stored
 * in the stack, then both will be erased.
 *
 * \ingroup ProcessUtils
 */
class LGX_EXPORT Stack {
  friend class process::SetupProcess;

public:
  /**
   * Create an empty stack with a main and a work store
   */
  Stack(int id);

  /**
   * Deep copy of a stack (i.e. the content of main and work store will be copied)
   */
  Stack(const Stack& copy);

  /**
   * Delete the stores attached to the stack, but not the meshes
   */
  ~Stack();

  /**
   * Id of a stack.
   *
   * This is the same id used in the Process:stack(int) method.
   */
  int id() const {
    return _id;
  }

  /**
   * Change the id of the stack.
   *
   * Please do not use this method for stacks attached to a process.
   */
  void setId(int i) {
    _id = i;
  }

  /**
   * Id as seen by the user
   *
   * This is, typically id()+1
   */
  int userId() const {
    return _id + 1;
  }

  /**
   * Access a store
   */
  Store* store(STORE store);

  /**
   * Access a store
   */
  const Store* store(STORE store) const;

  /**
   * Access the main store
   */
  const Store* main() const {
    return _main;
  }
  /**
   * Access the main store
   */
  Store* main() {
    return _main;
  }
  /**
   * Change the main store.
   *
   * \note A process shouldn't use this method. It is more sensible to clear, or resize the existing store.
   */
  void setMain(Store* m);

  /**
   * Access the work store
   */
  const Store* work() const {
    return _work;
  }
  /**
   * Access the work store
   */
  Store* work() {
    return _work;
  }
  /**
   * Change the work store.
   *
   * \note A process shouldn't use this method. It is more sensible to clear, or resize the existing store.
   */
  void setWork(Store* w);

  /**
   * Returns the current store
   */
  const Store* currentStore() const;
  /**
   * Returns the current store
   */
  Store* currentStore();

  /**
   * Adapt the store's visibility to set the main store as current
   */
  void setMainAsCurrent();
  /**
   * Adapt the store's visibility to set the work store as current
   */
  void setWorkAsCurrent();
  /**
   * Adapt the store's visibility to set the selected one as current
   */
  void setCurrent(STORE which);

  /**
   * Returns the size, in voxels, of the stores
   */
  Point3u size() const {
    return _size;
  }
  /**
   * Returns the bounding box, in voxels, of the stores
   */
  BoundingBox3i boundingBox() const {
    return BoundingBox3i(Point3i(), Point3i(size()));
  }

  /**
   * Returns the size, in micro-meters, of a voxel
   */
  Point3f step() const
  {
    return multiply(_step, scale());
  }
  // Point3f step() const { return _step * scale(); }// old
  /**
   * Returns the size, in number of elements, of the stores
   */
  size_t storeSize() const {
    return size_t(_size.x()) * size_t(_size.y()) * size_t(_size.z());
  }
  /**
   * True if the stack is empty (i.e. of zero size)
   */
  bool empty() const {
    return _size == Point3u();
  }

  /**
   * Scaling factor to apply, if scaled
   */
  Point3f scale() const {
    return (_showScale ? _scale : Point3f(1.0, 1.0, 1.0));
  }
  // float scale() const { return (_showScale ? _scale : 1.0f); }
  /**
   * Position of the point (0,0,0) of the image, in world coordinate
   */
  Point3f origin() const {
    return multiply(scale(), _origin);
  }
  // Point3f origin() const { return scale()*_origin; }
  /**
   * Set the scaling factor of the stack
   */
  void setScale(Point3f s) {
    _scale = s;
  }
  // void setScale(float s) { _scale = s; }
  /**
   * Set the origin in coordinate of the front-bottom-left corner
   */
  void setOrigin(const Point3f& s);

  /**
   * Change the size (in voxel) of the stack.
   *
   * \note This method will resize the two stores, keeping the data when possible.
   */
  void setSize(const Point3u& s);
  /**
   * Change the dimensions of a voxel.
   */
  void setStep(const Point3f& s);

  /**
   * True if the stack is scaled
   */
  bool showScale() const {
    return _showScale;
  }
  /**
   * Set if the stack should be scaled
   */
  void setShowScale(bool s) {
    _showScale = s;
  }

  /**
   * True if the 3 scaling axis are tied together
   */
  bool tieScales() const {
    return _tieScales;
  }
  /**
   * Set if the scales should be tied
   */
  void setTieScales(bool s) {
    _tieScales = s;
  }

  /**
   * True if the stack is shown transformed
   */
  bool showTrans() const {
    return _showTrans;
  }
  /**
   * Set if the stack should be transformed
   */
  void setShowTrans(bool s) {
    _showTrans = s;
  }

  /**
   * True if the Bounding Box of the stack is shown
   */
  bool showBBox() const {
    return _showBBox;
  }
  /**
   * Set if the bounding box of the stack should be visible
   */
  void setShowBBox(bool s) {
    _showBBox = s;
  }

  /// Matrix transforming a world point to an image one
  Matrix4f worldToImage() const;

  /// Matrix transforming an image point to a world one
  Matrix4f imageToWorld() const;

  /// Matrix transforming a world vector to an image one
  Matrix4f worldToImageVector() const;

  /// Matrix transforming an image vector to a world one
  Matrix4f imageToWorldVector() const;

  /// Go from image coordinates to world coordinates (for a point)
  template <typename T>
  Point3f imageToWorld(const util::Vector<3, T>& img) const
  {
    return multiply(Point3f(img) + Point3f(.5f, .5f, .5f), step()) + origin();
  }

  /// Go from world coordinates to image coordinates (for a point)
  template <typename T>
  util::Vector<3, T> worldToImage(Point3f wrld) const
  {
    return util::Vector<3, T>((wrld - origin()) / step() - Point3f(.5f, .5f, .5f));
  }

  /// Go from image coordinates to world coordinates (for a vector)
  template <typename T>
  Point3f imageToWorldVector(const util::Vector<3, T>& img) const
  {
    return multiply(Point3f(img), step());
  }

  /// Go from world coordinates to image coordinates (for a vector)
  template <typename T>
  util::Vector<3, T> worldToImageVector(Point3f wrld) const
  {
    return util::Vector<3, T>(wrld / step());
  }

  /// Go from image coordinates to world coordinates (for a point)
  template <typename T>
  BoundingBox3f imageToWorld(const util::BoundingBox<3, T>& img) const
  {
    return BoundingBox3f(imageToWorld(img.pmin()), imageToWorld(img.pmax()));
  }

  /// Go from world coordinates to image coordinates (for a point)
  template <typename T>
  util::BoundingBox<3, T> worldToImage(const BoundingBox3f& wrld) const
  {
    return util::BoundingBox<3, T>(worldToImage<T>(wrld.pmin()), worldToImage<T>(wrld.pmax()));
  }

  Point3f worldToImagef(const Point3f& a) const {
    return worldToImage<float>(a);
  }
  Point3i worldToImagei(const Point3f& a) const {
    return worldToImage<int>(a);
  }
  Point3u worldToImageu(const Point3f& a) const {
    return worldToImage<uint>(a);
  }

  BoundingBox3f worldToImagef(const BoundingBox3f& a) const {
    return worldToImage<float>(a);
  }
  BoundingBox3i worldToImagei(const BoundingBox3f& a) const {
    return worldToImage<int>(a);
  }
  BoundingBox3u worldToImageu(const BoundingBox3f& a) const {
    return worldToImage<uint>(a);
  }

  Point3f worldToImageVectorf(const Point3f& a) const {
    return worldToImageVector<float>(a);
  }
  Point3i worldToImageVectori(const Point3f& a) const {
    return worldToImageVector<int>(a);
  }
  Point3u worldToImageVectoru(const Point3f& a) const {
    return worldToImageVector<uint>(a);
  }

  // Transform from world to global coordinates
  Point3f worldToGlobal(const Point3f& v) const {
    const auto& frame = getFrame();
    return Point3f(frame.inverseCoordinatesOf(qglviewer::Vec(v)));
  }

  // Transform a vector from world to global coordinates
  Point3f worldToGlobalVector(const Point3f& v) const {
    const auto& frame = getFrame();
    return Point3f(frame.inverseTransformOf(qglviewer::Vec(v)));
  }

  // Transform from world to global coordinates
  Point3f globalToWorld(const Point3f& v) const {
    const auto& frame = getFrame();
    return Point3f(frame.coordinatesOf(qglviewer::Vec(v)));
  }

  // Transform a vector from world to global coordinates
  Point3f globalToWorldVector(const Point3f& v) const {
    const auto& frame = getFrame();
    return Point3f(frame.transformOf(qglviewer::Vec(v)));
  }

  /**
   * Go from abstract unit to world unit.
   *
   * Abstract unit is a reference system in which the stack maximal dimensions are [-0.5,0.5]
   */
  Point3f abstractToWorld(const Point3f& p) const
  {
    return divide(p, scale()) * _texScale;
  }

  /**
   * Size of the image, in world coordinate
   */
  Point3f worldSize() const {
    return imageToWorldVector(_size);
  }

  /// Check if (x,y,z) is in the image
  bool boundsOK(int x, int y, int z) const
  {
    if(x < 0 or y < 0 or z < 0 or x >= int(_size.x()) or y >= int(_size.y()) or z >= int(_size.z()))
      return false;
    else
      return true;
  }

  /// Compute offset for image data
  size_t offset(uint x, uint y, uint z) const {
    return (size_t(z) * _size.y() + y) * _size.x() + x;
  }

  /**
   * Returns the position, in the image, of the point of image coordinate \c ipos.
   */
  size_t offset(Point3i ipos) const {
    return (size_t(ipos.z()) * _size.y() + ipos.y()) * _size.x() + ipos.x();
  }

  /// Compute the position from the offset
  Point3u position(size_t offset) const
  {
    size_t x = offset % _size.x();
    offset = (offset - x) / _size.x();
    size_t y = offset % _size.y();
    size_t z = (offset - y) / _size.y();
    return Point3u(x, y, z);
  }

  /**
   * Shift the image so it is centered
   */
  void center();

  /**
   * Returns the manipulated frame
   */
  const qglviewer::ManipulatedFrame& frame() const {
    return _frame;
  }

  /**
   * Returns the manipulated frame
   */
  qglviewer::ManipulatedFrame& frame() {
    return _frame;
  }

  /**
   * Returns the transformation frame
   */
  const qglviewer::ManipulatedFrame& trans() const {
    return _trans;
  }
  /**
   * Returns the transformation frame
   */
  qglviewer::ManipulatedFrame& trans() {
    return _trans;
  }

  /**
   * Returns the active frame (i.e. frame() or trans()).
   *
   * It will be the manipulated frame or the transformation frame, depending on which is used (i.e. is showTrans()
   * true or false)
   */
  qglviewer::ManipulatedFrame& getFrame() {
    return (_showTrans ? _trans : _frame);
  }

  /**
   * Returns the active frame (i.e. frame() or trans()).
   *
   * It will be the manipulated frame or the transformation frame, depending on which is used (i.e. is showTrans()
   * true or false)
   */
  const qglviewer::ManipulatedFrame& getFrame() const {
    return (_showTrans ? _trans : _frame);
  }

  /**
   * Return the label to be used
   */
  int viewLabel() const {
    return _CurrLabel;
  }
  /**
   * Change the label to be used
   */
  void setLabel(int l)
  {
    changed_label = true;
    _CurrLabel = l;
  }
  /**
   * Return the next label to be used and change it so successive calls return successive labels.
   */
  int nextLabel()
  {
    changed_label = true;
    _CurrLabel++;
    if(_CurrLabel > ((1 << 16) - 1))
      _CurrLabel = 1;
    return _CurrLabel;
  }
  /**
   * Returns true if the label changed during this process.
   */
  bool labelChanged() const {
    return changed_label;
  }

  /**
   * Erase everything
   */
  void reset();

  /**
   * Check if the image is really at most 2D
   */
  bool is2D() const {
    return _size[0] == 1 or _size[1] == 1 or _size[2] == 1;
  }

  /**
   * Convert any 3D vector into the 2D equivalent
   */
  template <typename T>
  util::Vector<2,T> to2D(const util::Vector<3,T>& v) const {
    if(_size[0] == 1) return util::Vector<2,T>{v[1], v[2]};
    if(_size[1] == 1) return util::Vector<2,T>{v[0], v[2]};
    return util::Vector<2,T>{v[0], v[1]};
  }

  /**
   * Convert any 2D vector into the 3D equivalent.
   *
   * 0 is added to the missing dimension.
   */
  template <typename T>
  util::Vector<3,T> to3D(const util::Vector<2, T>& v, const T& missing = T()) const {
    if(_size[0] == 1) return util::Vector<3,T>{missing, v[0], v[1]};
    if(_size[1] == 1) return util::Vector<3,T>{v[0], missing, v[1]};
    return util::Vector<3,T>{v[0], v[1], missing};
  }

  /**
   * Convert any 3D BoundingBox into the 2D equivalent
   */
  template <typename T>
  util::BoundingBox<2,T> to2D(const util::BoundingBox<3,T>& v) const {
    return util::BoundingBox<2,T>{to2D(v[0]), to2D(v[1])};
  }

  /**
   * Convert any 2D BoundingBox into the 3D equivalent.
   *
   * 0 is added to the missing dimension.
   */
  template <typename T>
  util::BoundingBox<3,T> to3D(const util::BoundingBox<2,T>& v, const T& missing = T()) const {
    return util::BoundingBox<3,T>{to3D(v[0], missing), to3D(v[1], missing)};
  }

  /**
   * Convert any 3D matrix into the 2D equivalent
   */
  template <typename T>
  util::Matrix<2,2,T> to2D(const util::Matrix<3,3,T>& v) const {
    if(_size[0] == 1) return util::Matrix<2,2,T>({{v(1,1), v(1,2)}, {v(2,1), v(2,2)}});
    if(_size[1] == 1) return util::Matrix<2,2,T>({{v(0,0), v(0,2)}, {v(2,0), v(2,2)}});
    return util::Matrix<2,2,T>({{v(0,0), v(0,1)}, {v(1,0), v(1,1)}});
  }

  /**
   * Convert any 2D Matrix into the 3D equivalent.
   *
   * 0 is added to the missing dimension and a 1 in the diagonal.
   */
  template <typename T>
  util::Matrix<3,3,T> to3D(const util::Matrix<2,2,T>& v) const {
    if(_size[0] == 1) return util::Matrix<3,3,T>({{1, 0, 0}, {0, v(0,0), v(0,1)}, {0, v(1,0), v(1,1)}});
    if(_size[1] == 1) return util::Matrix<3,3,T>({{v(0,0), 0, v(0,1)}, {0, 1, 0}, {v(1,0), 0, v(1,1)}});
    return util::Matrix<3,3,T>({{v(0,0), v(0,1), 0}, {v(1,0), v(1,1), 0}, {0, 0, 1}});
  }

  /**
   * Return the 2D size of the image.
   *
   * If the image is 3D, this will return the dimension of a (x,y) plane in the image.
   */
  Point2u size2D() const {
    return to2D(_size);
  }

  /**
   * Return the 2D step of the image
   *
   * If the image is 3D, the step in the (x,y) plane will be returned
   */
  Point2f step2D() const {
    return to2D(_step);
  }

private:
  void resetModified();
  void updateSizes();
  Point3f _scale;
  // float _scale;
  Point3f _origin;
  float _texScale;
  Point3u _size;
  Point3f _step;
  qglviewer::ManipulatedFrame _frame;
  qglviewer::ManipulatedFrame _trans;
  bool changed_frame, changed_trans, changed_label;
  int _id;
  int _CurrLabel;
  bool _showScale, _showTrans, _showBBox, _tieScales;
  Store* _main, *_work;
};
} // namespace lgx

#endif // STACK_HPP
