/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "MeshProcessLineage.hpp"

#include "Progress.hpp"
#include "CSVStream.hpp"

#include <QFileDialog>
#include <QMessageBox>
#include <QList>

namespace lgx {
namespace process {
bool SaveParents::initialize(ParmList& parms, QWidget* parent)
{
  QString filename = QFileDialog::getSaveFileName(parent, "Select file to save labels in",
                                                  parms[0].toString(), "CSV files (*.csv)");
  // check filename
  if(filename.isEmpty())
    return false;
  // check ending, add suffix if required
  QString suffix = ".csv";
  if(filename.right(suffix.size()) != suffix)
    filename.append(suffix);

  parms[0] = filename;
  return true;
}

bool SaveParents::operator()(Mesh* mesh, const QString& filename)
{
  const IntIntMap& parentLabel = mesh->parentLabelMap();
  if(parentLabel.size() == 0) {
    setErrorMessage(QString("There are no parent labels").arg(filename));
    return false;
  }

  QFile file(filename);

  if(!file.open(QIODevice::WriteOnly | QIODevice::Text)) {
    setErrorMessage(QString("File '%1' cannot be opened for writing").arg(filename));
    return false;
  }
  util::CSVStream out(&file);
  out << "Label" << "Parent Label" << eol;

  // store to file
  SETSTATUS("Storing " << parentLabel.size() << " parent labels to file " << filename);
  forall(const IntIntPair& p, parentLabel) {
    int label = p.first;
    int parent = p.second;
    out << label << parent << eol;
  }

  file.close();
  return true;
}
REGISTER_MESH_PROCESS(SaveParents);

bool LoadParents::initialize(ParmList& parms, QWidget* parent)
{
  QString filename = QFileDialog::getOpenFileName(parent, "Choose parent label file to load",
                                                  parms[0].toString(), "CSV files (*.csv)");

  // check file
  if(filename.isEmpty())
    return false;

  QFile file(filename);
  if(!file.open(QIODevice::ReadOnly)) {
    QMessageBox::critical(parent, "Error opening file",
                          QString("File '%1' cannot be opened for reading").arg(filename));
    return false;
  }
  util::CSVStream ss(&file);
  QStringList fields;
  ss >> fields;
  if(fields.size() != 2) {
    QMessageBox::critical(parent, "Error reading file",
                          QString("File '%1' should be a CSV file with 2 columns").arg(filename));
    return false;
  }

  // ok now we're happy
  parms[0] = filename;
  return true;
}

bool LoadParents::operator()(Mesh* mesh, const QString& filename, bool markDaughters)
{
  QFile file(filename);
  if(!file.open(QIODevice::ReadOnly)) {
    setErrorMessage(QString("File '%1' cannot be opened for reading").arg(filename));
    return false;
  }
  util::CSVStream ss(&file);
  QStringList fields;
  ss >> fields;
  if(fields.size() != 2) {
    setErrorMessage(QString("File '%1' should be a CSV file with 2 columns").arg(filename));
    return false;
  }

  // read file
  IntIntMap parentLabel;
  IntFloatMap labelHeat;
  int line_nb = 1;
  while(ss.status() == QTextStream::Ok) {
    ++line_nb;
    ss >> fields;
    if(fields.size() != 2)
      break;
    bool ok;
    int label = fields[0].toInt(&ok);
    if(!ok) {
      setErrorMessage(QString("Error line %1: the first column is not an integer number but '%2'")
                      .arg(line_nb)
                      .arg(fields[0].trimmed()));
      return false;
    }
    int parent = fields[1].toInt(&ok);
    if(!ok) {
      setErrorMessage(QString("Error line %1: the second column is not an integer number but '%2'")
                      .arg(line_nb)
                      .arg(fields[1].trimmed()));
      return false;
    }
    parentLabel[label] = parent;
    if(markDaughters)
      labelHeat[label] = 1;
    // SETSTATUS("Seed " << parent << " to be mapped to local label " << label);
  }
  // done
  SETSTATUS(parentLabel.size() << " parent labels loaded from file " << filename);

  // clear tables related to parents
  mesh->parentCenter().clear();
  mesh->parentCenterVis().clear();
  mesh->parentNormal().clear();
  mesh->parentNormalVis().clear();

  // store it
  mesh->parentLabelMap() = std::move(parentLabel);
  if(markDaughters) {
    mesh->labelHeat() = std::move(labelHeat);
    mesh->heatMapUnit() = "ON/OFF";
    mesh->heatMapBounds() = Point2f(0, 1);
  }

  mesh->updateTriangles();
  return true;
}
REGISTER_MESH_PROCESS(LoadParents);

bool ClearParents::operator()(Mesh* mesh)
{
  mesh->parentLabelMap().clear();
  mesh->parentCenter().clear();
  mesh->parentCenterVis().clear();
  mesh->parentNormal().clear();
  mesh->parentNormalVis().clear();
  mesh->labelHeat().clear();
  mesh->updateTriangles();
  return true;
}
REGISTER_MESH_PROCESS(ClearParents);

// Eliminate from parentLabelMap labels which are not in mesh1 or mesh2
bool CorrectParents::operator()(Mesh* mesh1, Mesh* mesh2)
{
  IntIntMap& daughterParentMap = mesh2->parentLabelMap();
  if(daughterParentMap.size() == 0) {
    setErrorMessage(QString("No parent labels on second mesh"));
    return false;
  }

  vvgraph S1 = mesh1->graph();
  vvgraph S2 = mesh2->graph();

  // Check the parentLabelMap for cells which are not in the mesh1 or mesh2
  IntIntMap daughterParentMaptemp;
  std::set<int> labelsS1;
  std::set<int> labelsS2;
  std::set<int> problemsS1;
  std::set<int> problemsS2;
  forall(vertex c, S1)
    if(c->label > 0)
      labelsS1.insert(c->label);
  forall(vertex c, S2)
    if(c->label > 0)
      labelsS2.insert(c->label);
  for(IntIntMap::iterator it = daughterParentMap.begin(); it != daughterParentMap.end(); it++) {
    int lParent = it->second;
    int lDaughter = it->first;
    if(labelsS1.count(lParent) == 0)
      problemsS1.insert(lParent);
    if(labelsS2.count(lDaughter) == 0)
      problemsS2.insert(lDaughter);
    if(labelsS1.count(lParent) != 0 and labelsS2.count(lDaughter) != 0)
      daughterParentMaptemp[lDaughter] = lParent;
  }
  daughterParentMap = daughterParentMaptemp;

  if(problemsS1.size() > 0 or problemsS2.size() > 0) {
    Information::out << "problems with parentLabelMap. Some labels in the map are not present in the mesh:\n"
                     << "in mesh1, the following labels do not exist:\n";
    forall(int l, problemsS1)
      Information::out << l << ", ";
    Information::out << "\nin mesh2, the following labels do not exist:\n";
    forall(int l, problemsS2)
      Information::out << l << ", ";
    Information::out << endl;
  }
  return true;
}
REGISTER_MESH_PROCESS(CorrectParents);

// Make a heat map based on how many daughter cells a parent cell has
bool HeatMapDaughterCells::operator()(Mesh* mesh)
{
  vvgraph& S = mesh->graph();

  IntIntMap parents = mesh->parentLabelMap();
  IntSet labels;

  // get list of labels
  forall(const vertex& v, S)
    if(v->label > 0)
      labels.insert(v->label);

  IntFloatMap daughters;

  // find how daughters each parent has
  forall(IntIntPair p, parents)
    if(p.first > 0 and p.second > 0)
      daughters[p.second] += 1;

  // Build heat map
  IntFloatMap labelDaughters;
  forall(IntIntPair p, parents)
    if(p.first > 0 and p.second > 0 and daughters[p.second] > 0)
      labelDaughters[p.first] = daughters[p.second];

  // Go over all the volumes to find the min and max
  float dmin = 1, dmax = 1;

  forall(const IntFloatPair& v, labelDaughters) {
    if(v.second < dmin)
      dmin = v.second;
    if(v.second > dmax)
      dmax = v.second;
  }

  if(dmax <= dmin)
    dmax = dmin + 1;

  // Set the map bounds. A heat of 0 will be interpreted has vol_min, and a heat of 1 as vol_max.
  mesh->heatMapBounds() = Point2f(dmin, dmax);
  // Unit of the heat map
  mesh->heatMapUnit() = QString("#");
  // Set the values
  mesh->labelHeat() = labelDaughters;
  // Tell the system the triangles have changed (here, the heat map)
  mesh->updateTriangles();

  return true;
}
REGISTER_MESH_PROCESS(HeatMapDaughterCells);

// Copy parent labels overtop of labels
bool CopyParentsToLabels::operator()(Mesh* mesh)
{
  vvgraph& S = mesh->graph();
  IntIntMap& parents = mesh->parentLabelMap();
  forall(const vertex& v, S)
    if(v->label > 0) {
      if(parents[v->label] > 0)
        v->label = parents[v->label];
      else
        v->label = 0;
    }

  parents.clear();

  mesh->updateAll();
  return true;
}
REGISTER_MESH_PROCESS(CopyParentsToLabels);

bool AddLineageToTable::initialize(ParmList& parms, QWidget* parent)
{
  QString filename = QFileDialog::getOpenFileName(parent, "Choose CSV file to load.",
                                                  parms[0].toString(), "CSV files (*.csv)");

  // check file
  if(filename.isEmpty())
    return false;

  parms[0] = filename;
  return true;
}

namespace {

struct Block
{
  size_t first, last, width;
};

} // namespace

bool AddLineageToTable::operator()(Mesh* m, const QString& inputFile, const QString& outputFile,
                                   const QString& cellColumnStr, const QString& parentColumnStr,
                                   size_t headerLine, bool removeNoLineage)
{
  // First, extract the table
  QFile inFile(inputFile);
  if(not inFile.open(QIODevice::ReadOnly))
    return setErrorMessage(QString("Cannot open file '%1' for reading.").arg(inputFile));
  util::CSVStream input(&inFile);
  QStringList line;
  size_t line_nb = 0;

  // Find header line
  if(headerLine == 0) {
    std::vector<Block> blocks;
    while(!input.atEnd()) {
      ++line_nb;
      input >> line;
      if(line.isEmpty() and input.status() != QTextStream::Ok)
        return setErrorMessage(QString("Error reading line %1 of the file '%2'").arg(line_nb).arg(inputFile));
      if(blocks.empty() or blocks.back().width != line.size())
        blocks.push_back(Block{line_nb, line_nb, (size_t)line.size()});
      Block& block = blocks.back();
      block.last = line_nb;
      if(block.last - block.first + 1 >= 10) {
        headerLine = block.first;
        break;
      }
    }
    if(blocks.empty())
      return setErrorMessage(QString("Error, file '%1' is empty").arg(inputFile));
    if(headerLine == 0) {
      size_t maxLength = 0;
      for(const Block& blk: blocks) {
        auto length = blk.last - blk.first + 1;
        if(length > maxLength) {
          maxLength = length;
          headerLine = blk.first;
        }
      }
    }
  }

  // Now parse the file and copy whatever is before the header
  input.seek(0); // rewing the stream
  QList<QStringList> output_lines;

  line_nb = 1;
  while(line_nb < headerLine) {
    input >> line;
    if(input.status() != QTextStream::Ok)
      return setErrorMessage(QString("Cannot parse line %1 of file '%2'").arg(line_nb).arg(inputFile));
    output_lines << line;
    ++line_nb;
  }

  if(input.atEnd())
    return setErrorMessage(QString("Header declared to be on line %1 while the file has only %2 lines.").arg(headerLine).arg(line_nb));

  size_t cellColumn;
  QStringList header;
  input >> header;
  if(input.status() != QTextStream::Ok)
    return setErrorMessage(QString("Cannot parse line %1 of file '%2'").arg(line_nb).arg(inputFile));
  ++line_nb;

  // Find the column to read data from
  int idx = header.indexOf(QRegularExpression("\\A" + cellColumnStr + "\\z"));
  if(idx >= 0)
    cellColumn = (size_t)idx;
  else {
    bool ok;
    cellColumn = cellColumnStr.toUInt(&ok);
    if(not ok or cellColumn >= header.size())
      return setErrorMessage(QString("Cannot find column '%1' either as string or number"));
  }
  // Check if the column to write to already exists
  auto idx_output_col = header.indexOf(parentColumnStr);
  size_t block_width = header.size();
  if(idx_output_col < 0)
    header << parentColumnStr;
  output_lines << header;

  auto& parentMap = m->parentLabelMap();

  while(not input.atEnd()) {
    input >> line;
    if(input.status() != QTextStream::Ok)
      return setErrorMessage(QString("Cannot parse line %1 of file '%2'").arg(line_nb).arg(inputFile));
    if(line.size() == block_width) {
      bool ok;
      int cellId = line[cellColumn].toInt(&ok);
      if(not ok)
        return setErrorMessage(QString("On line %1, the column %2 doesn't contain an integer number").arg(line_nb).arg(cellColumn));
      auto found = parentMap.find(cellId);
      int parent = (found != parentMap.end() ? found->second : 0);
      if(not removeNoLineage or parent != 0) {
        if(idx_output_col < 0)
          line << QString::number(parent);
        else
          line[idx_output_col] = QString::number(parent);
        output_lines << line;
      }
    } else {
      output_lines << line;
      break;
    }
    ++line_nb;
  }

  while(not input.atEnd()) {
    input >> line;
    if(input.status() != QTextStream::Ok)
      return setErrorMessage(QString("Cannot parse line %1 of file '%2'").arg(line_nb).arg(inputFile));
    output_lines << line;
    ++line_nb;
  }

  inFile.close();

  QString realOutput = (outputFile.isEmpty() ? inputFile : outputFile);
  QFile outFile(realOutput);
  if(not outFile.open(QIODevice::WriteOnly))
    return setErrorMessage(QString("Cannot open file '%1' for writing.").arg(realOutput));
  util::CSVStream output(&outFile);

  for(const auto& l: output_lines)
    output << l;

  return true;
}
REGISTER_MESH_PROCESS(AddLineageToTable);

} // namespace process
} // namespace lgx
