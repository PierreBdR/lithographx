#ifndef PRIVATEPROCESSUTILS_HPP
#define PRIVATEPROCESSUTILS_HPP

#include <Process.hpp>

#include <unordered_map>

namespace lgx {
namespace process {

typedef StackRegistration::processFactory stackProcessFactory;
typedef MeshRegistration::processFactory meshProcessFactory;
typedef GlobalRegistration::processFactory globalProcessFactory;

typedef ProcessDefinition<StackProcess> stackProcessDefinition;
typedef ProcessDefinition<MeshProcess> meshProcessDefinition;
typedef ProcessDefinition<GlobalProcess> globalProcessDefinition;

struct lgx_private_process_QStringHash
{
  size_t operator()(const QString& s) const
  {
    return qHash(s);
  }
};

class LGX_EXPORT DummyProcess : public GlobalProcess {
public:
  DummyProcess()
    : Process()
    , GlobalProcess()
  {
  }

  ~DummyProcess()
  {
  }

  bool operator()(const ParmList&) override {
    return true;
  }
  QString name() const override {
    return "Global";
  }
  QString description() const override {
    return "Dummy Process";
  }
  QStringList parmNames() const override {
    return QStringList();
  }

};


} // namespace process
} // namespace lgx

#endif // PRIVATEPROCESSUTILS_HPP

