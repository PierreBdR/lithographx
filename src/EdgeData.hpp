/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef EDGE_DATA_H
#define EDGE_DATA_H

#include <LGXConfig.hpp>

/**
 * \file EdgeData.hpp
 *
 * This file contains the definition of an edge data.
 */

namespace lgx {
/**
 * \class EdgeData EdgeData.hpp <EdgeData.hpp>
 * This class defines the fields available from an edge.
 */
struct LGX_EXPORT EdgeData {
};
inline bool operator==(const EdgeData&, const EdgeData&) { return true; }
} // namespace lgx

#endif
