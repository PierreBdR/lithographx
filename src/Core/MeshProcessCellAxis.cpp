/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "MeshProcessCellAxis.hpp"

//#include "MeshProcessCellMesh.hpp"
#include "CSVStream.hpp"
#include "Dir.hpp"
#include "MeshProcessCurv.hpp"
#include "MeshProcessFibril.hpp"
#include "MeshProcessGrowth.hpp"
#include "Misc.hpp"
#include "Progress.hpp"

#include <QFileDialog>

namespace lgx {
namespace process {

using namespace util;

bool SaveCellAxis::initialize(ParmList& parms, QWidget* parent)
{
  QString filename = parms[0].toString();
  if(filename.isEmpty())
    filename = QDir::currentPath();
  filename = QFileDialog::getSaveFileName(parent, "Choose spreadsheet file to save", filename, "CSV files (*.csv)");
  if(filename.isEmpty())
    return false;
  if(!filename.endsWith(".csv", Qt::CaseInsensitive))
    filename += ".csv";
  parms[0] = stripCurrentDir(filename);
  return true;
}

bool SaveCellAxis::saveFile(Mesh* mesh, const QString& filename, const QString& k1Name, const QString& k2Name,
                            const QString& k3Name)
{
  QFile file(filename);
  if(!file.open(QIODevice::WriteOnly))
    return setErrorMessage(QString("File '%1' cannot be opened for writing").arg(filename));
  util::CSVStream ts(&file);
  ts << mesh->cellAxisDesc().toList();
  QStringList fields = QStringList() << "Label"
                                     << "x1"
                                     << "y1"
                                     << "z1"
                                     << "x2"
                                     << "y2"
                                     << "z2" << k1Name;
  if(not k2Name.isEmpty())
    fields << k2Name;
  if(not k3Name.isEmpty())
    fields << k3Name;
  ts << fields;
  std::vector<int> labels;
  const IntSymTensorMap& cellAxis = mesh->cellAxis();
  forall(const IntSymTensorPair& p, cellAxis)
    labels.push_back(p.first);
  std::sort(labels.begin(), labels.end());
  forall(int label, labels) {
    const SymmetricTensor& tensor = cellAxis.find(label)->second;
    const Point3f& v1 = tensor.ev1();
    const Point3f& v2 = tensor.ev2();
    const Point3f& values = tensor.evals();
    ts << label << v1.x() << v1.y() << v1.z() << v2.x() << v2.y() << v2.z() << values[0];
    if(not k2Name.isEmpty())
      ts << values[1];
    if(not k3Name.isEmpty())
      ts << values[2];
    ts << eol;
  }
  return true;
}

bool LoadCellAxis::initialize(ParmList& parms, QWidget* parent)
{
  QString filename = parms[0].toString();
  if(filename.isEmpty())
    filename = QDir::currentPath();
  filename = QFileDialog::getOpenFileName(parent, "Choose spreadsheet to load",
                                          filename, "CSV files (*.csv);;All files (*.*)");
  if(filename.isEmpty())
    return false;
  if(!filename.endsWith(".csv", Qt::CaseInsensitive))
    filename += ".csv";
  parms[0] = stripCurrentDir(filename);
  return true;
}

LoadCellAxis::FileHeader LoadCellAxis::readFile(Mesh* mesh, const QString& type, const QString& filename,
                                                const QRegularExpression& k1Name, const QRegularExpression& k2Name,
                                                const QRegularExpression& k3Name)
{
  FileHeader header;
  QFile file(filename);
  if(!file.open(QIODevice::ReadOnly))
    return setErrorMessage(QString("File '%1' cannot be opened for reading").arg(filename));
  util::CSVStream ts(&file);
  QStringList fields;
  ts >> fields;
  bool ok;
  Description desc = Description::fromList(fields, &ok);
  if(not ok or desc.type() != type) {
    header.wrong_type = true;
    setErrorMessage(QString("File '%1' is not of type '%2'").arg(filename).arg(type));
    return header;
  } else
    header.wrong_type = false;
  ts >> fields;
  size_t nb_fields = fields.size();
  int field_x1, field_y1, field_z1;
  int field_x2, field_y2, field_z2;
  int field_k1 = -1, field_k2 = -1, field_k3 = -1;
  int field_label;

  field_label = fields.indexOf("Label");
  if(field_label < 0)
    return setErrorMessage("Error, no field 'Label' in the CSV file");
  field_x1 = fields.indexOf("x1");
  if(field_x1 < 0)
    return setErrorMessage("Error, no field 'x1' in the CSV file");
  field_y1 = fields.indexOf("y1");
  if(field_y1 < 0)
    return setErrorMessage("Error, no field 'y1' in the CSV file");
  field_z1 = fields.indexOf("z1");
  if(field_z1 < 0)
    return setErrorMessage("Error, no field 'z1' in the CSV file");
  field_x2 = fields.indexOf("x2");
  if(field_x2 < 0)
    return setErrorMessage("Error, no field 'x2' in the CSV file");
  field_y2 = fields.indexOf("y2");
  if(field_y2 < 0)
    return setErrorMessage("Error, no field 'y2' in the CSV file");
  field_z2 = fields.indexOf("z2");
  if(field_z2 < 0)
    return setErrorMessage("Error, no field 'z2' in the CSV file");
  field_k1 = fields.indexOf(k1Name);
  if(field_k1 < 0)
    return setErrorMessage(QString("Error, no field '%1' in the CSV file").arg(k1Name.pattern()));
  header.k1Match = k1Name.match(fields[field_k1]);
  if(k2Name.isValid() and not k2Name.pattern().isEmpty()) {
    field_k2 = fields.indexOf(k2Name);
    if(field_k2 < 0)
      return setErrorMessage(QString("Error, no field '%1' in the CSV file").arg(k2Name.pattern()));
    header.k2Match = k2Name.match(fields[field_k2]);
  }
  if(k3Name.isValid() and not k3Name.pattern().isEmpty()) {
    field_k3 = fields.indexOf(k3Name);
    if(field_k3 < 0)
      return setErrorMessage(QString("Error, no field '%1' in the CSV file").arg(k3Name.pattern()));
    header.k3Match = k3Name.match(fields[field_k3]);
  }

  mesh->clearCellAxis();
  IntSymTensorMap& cellAxis = mesh->cellAxis();

  int linenum = 1;
  while(ts.status() == QTextStream::Ok) {
    ++linenum;
    ts >> fields;
    if(fields.empty() or fields.size() == 1)
      break;

    if(fields.size() < (int)nb_fields)
      return setErrorMessage(
        QString("Error on line %1: not enough fields (Expected: %2, Read: %3)")
        .arg(linenum).arg(nb_fields).arg( fields.size()));

    bool ok;
    int label = fields[field_label].toInt(&ok);
    if(!ok) {
      setErrorMessage(
        QString("Error line %1: invalid label '%2'").arg(linenum).arg(fields[field_label].trimmed()));
      return false;
    }

    Point3f ev1, ev2, evals;
    ev1.x() = fields[field_x1].toFloat(&ok);
    if(!ok)
      return setErrorMessage(QString("Error line %1: invalid x1 '%2'").arg(linenum).arg(fields[field_x1]));
    ev1.y() = fields[field_y1].toFloat(&ok);
    if(!ok)
      return setErrorMessage(QString("Error line %1: invalid y1 '%2'").arg(linenum).arg(fields[field_y1]));
    ev1.z() = fields[field_z1].toFloat(&ok);
    if(!ok)
      return setErrorMessage(QString("Error line %1: invalid z1 '%2'").arg(linenum).arg(fields[field_z1]));

    ev2.x() = fields[field_x2].toFloat(&ok);
    if(!ok)
      return setErrorMessage(QString("Error line %1: invalid x2 '%2'").arg(linenum).arg(fields[field_x2]));
    ev2.y() = fields[field_y2].toFloat(&ok);
    if(!ok)
      return setErrorMessage(QString("Error line %1: invalid y2 '%2'").arg(linenum).arg(fields[field_y2]));
    ev2.z() = fields[field_z2].toFloat(&ok);
    if(!ok)
      return setErrorMessage(QString("Error line %1: invalid z2 '%2'").arg(linenum).arg(fields[field_z2]));

    evals.x() = fields[field_k1].toFloat(&ok);
    if(!ok)
      return setErrorMessage(QString("Error line %1: invalid k1 '%2'").arg(linenum).arg(fields[field_k1]));
    evals.y() = (field_k2 < 0 ? 0.f : fields[field_k2].toFloat(&ok));
    if(!ok)
      return setErrorMessage(QString("Error line %1: invalid k2 '%2'").arg(linenum).arg(fields[field_k2]));
    evals.z() = (field_k3 < 0 ? 0.f : fields[field_k3].toFloat(&ok));
    if(!ok)
      return setErrorMessage(QString("Error line %1: invalid k3 '%2'").arg(linenum).arg(fields[field_k3]));

    // make sure ev1 and ev2 are unit vectors before storing in cellAxis. It is not the case in the older csv files
    ev1.normalize();
    ev2.normalize();

    cellAxis[label] = SymmetricTensor(ev1, ev2, evals);
  }
  mesh->cellAxisDesc() = desc;
  this->axisType = type;
  header.valid = true;
  return header;
}

void LoadCellAxis::showResult(Mesh*)
{
  if(this->axisType.isEmpty())
    return;
  ParmList parms;
  QString processName = QString("Display %1").arg(this->axisType);
  if(not getDefaultParms("Mesh", processName, parms))
    return;
  runProcess("Mesh", processName, parms);
}

bool LoadCellAxis::readOldFile(Mesh* mesh, const QString& type, const QString& filename, QString& k1, QString& k2,
                               QString& k3)
{
  QFile file(filename);
  if(!file.open(QIODevice::ReadOnly))
    return setErrorMessage(QString("File '%1' cannot be opened for reading").arg(filename));
  util::CSVStream ts(&file);
  QStringList fields;
  ts >> fields;
  QHash<QString, int> fieldMap;
  int field_x1, field_y1, field_z1;
  int field_x2, field_y2, field_z2;
  int field_k1 = -1, field_k2 = -1, field_k3 = -1;
  int field_label;
  if(fields.size() < 10) {
    setErrorMessage(QString("File '%1' should be a CSV file with at least 10 columns").arg(filename));
    return false;
  }

  field_label = fields.indexOf("Label");
  if(field_label < 0)
    return setErrorMessage("Error, no field 'Label' in the CSV file");
  field_x1 = max(fields.indexOf("xMax"), fields.indexOf("x1"));
  if(field_x1 < 0)
    return setErrorMessage("Error, no field 'x1' or 'xMax' in the CSV file");
  field_y1 = max(fields.indexOf("yMax"), fields.indexOf("y1"));
  if(field_y1 < 0)
    return setErrorMessage("Error, no field 'y1' or 'yMax' in the CSV file");
  field_z1 = max(fields.indexOf("zMax"), fields.indexOf("z1"));
  if(field_z1 < 0)
    return setErrorMessage("Error, no field 'z1' or 'zMax' in the CSV file");
  field_x2 = max(fields.indexOf("xMin"), fields.indexOf("x2"));
  if(field_x2 < 0)
    return setErrorMessage("Error, no field 'x2' or 'xMin' in the CSV file");
  field_y2 = max(fields.indexOf("yMin"), fields.indexOf("y2"));
  if(field_y2 < 0)
    return setErrorMessage("Error, no field 'y2' or 'yMin' in the CSV file");
  field_z2 = max(fields.indexOf("zMin"), fields.indexOf("z2"));
  if(field_z2 < 0)
    return setErrorMessage("Error, no field 'z2' or 'zMin' in the CSV file");

  QString axisUnit;
  if(type == "2D Growth") {
    field_k1 = fields.indexOf("stretchRatioMax");
    field_k2 = fields.indexOf("stretchRatioMin");
    field_k3 = fields.indexOf("stretchRatioNormal");
    axisUnit = "1";
  } else if(type == "Tissue Curvature") {
    field_k1 = fields.indexOf("1/radiusCurvMax");
    field_k2 = fields.indexOf("1/radiusCurvMin");
    field_k3 = fields.indexOf("1/radiusCurvNormal");
    axisUnit = UM_1;
  } else if(type == "Fibril Orientation") {
    field_k1 = fields.indexOf("alignmentMax");
    field_k2 = fields.indexOf("alignmentMin");
    field_k3 = fields.indexOf("alignmentNormal");
    axisUnit = "a.u.";
  }

  QRegExp find_k1("k1\\s*(.*)");
  QRegExp find_k2("k2\\s*(.*)");
  QRegExp find_k3("k3\\s*(.*)");

  if(field_k1 == -1) {
    field_k1 = fields.indexOf(find_k1);
    if(field_k1 < 0)
      return setErrorMessage("Error, no field 'k1' in the CSV file");

    find_k1.indexIn(fields[field_k1]);
    axisUnit = find_k1.cap(1).trimmed();
    if(axisUnit.length() > 1) {
      if(axisUnit[axisUnit.size() - 1] == ')')
        axisUnit.chop(1);
      if(axisUnit[0] == '(')
        axisUnit = axisUnit.mid(1);
      axisUnit = axisUnit.trimmed();
    }
  }
  if(field_k2 == -1) {
    field_k2 = fields.indexOf(find_k2);
    if(field_k2 < 0)
      return setErrorMessage("Error, no field 'k2' in the CSV file");
  }
  if(field_k3 == -1) {
    field_k3 = fields.indexOf(find_k3);
    if(field_k3 < 0)
      return setErrorMessage("Error, no field 'k3' in the CSV file");
  }

  k1 = fields[field_k1];
  k2 = fields[field_k2];
  k3 = fields[field_k3];

  int nb_fields = fields.size();

  mesh->clearCellAxis();
  IntSymTensorMap& cellAxis = mesh->cellAxis();

  int linenum = 1;
  while(ts.status() == QTextStream::Ok) {
    ++linenum;
    ts >> fields;
    if(fields.empty() or fields.size() == 1)
      break;

    if(fields.size() < nb_fields)
      return setErrorMessage(
        QString("Error on line %1: not enough fields (Expected: %2, Read: %3)").arg(linenum).arg(nb_fields).arg(
          fields.size()));

    bool ok;
    int label = fields[field_label].toInt(&ok);
    if(!ok) {
      setErrorMessage(
        QString("Error line %1: invalid label '%2'").arg(linenum).arg(fields[field_label].trimmed()));
      return false;
    }

    Point3f ev1, ev2, evals;
    ev1.x() = fields[field_x1].toFloat(&ok);
    if(!ok)
      return setErrorMessage(QString("Error line %1: invalid x1 '%2'").arg(linenum).arg(fields[field_x1]));
    ev1.y() = fields[field_y1].toFloat(&ok);
    if(!ok)
      return setErrorMessage(QString("Error line %1: invalid y1 '%2'").arg(linenum).arg(fields[field_y1]));
    ev1.z() = fields[field_z1].toFloat(&ok);
    if(!ok)
      return setErrorMessage(QString("Error line %1: invalid z1 '%2'").arg(linenum).arg(fields[field_z1]));

    ev2.x() = fields[field_x2].toFloat(&ok);
    if(!ok)
      return setErrorMessage(QString("Error line %1: invalid x2 '%2'").arg(linenum).arg(fields[field_x2]));
    ev2.y() = fields[field_y2].toFloat(&ok);
    if(!ok)
      return setErrorMessage(QString("Error line %1: invalid y2 '%2'").arg(linenum).arg(fields[field_y2]));
    ev2.z() = fields[field_z2].toFloat(&ok);
    if(!ok)
      return setErrorMessage(QString("Error line %1: invalid z2 '%2'").arg(linenum).arg(fields[field_z2]));

    evals.x() = fields[field_k1].toFloat(&ok);
    if(!ok)
      return setErrorMessage(QString("Error line %1: invalid k1 '%2'").arg(linenum).arg(fields[field_k1]));
    evals.y() = fields[field_k2].toFloat(&ok);
    if(!ok)
      return setErrorMessage(QString("Error line %1: invalid k2 '%2'").arg(linenum).arg(fields[field_k2]));
    evals.z() = fields[field_k3].toFloat(&ok);
    if(!ok)
      return setErrorMessage(QString("Error line %1: invalid k3 '%2'").arg(linenum).arg(fields[field_k3]));

    // make sure ev1 and ev2 are unit vectors before storing in cellAxis. It is not the case in the older csv files
    ev1.normalize();
    ev2.normalize();

    cellAxis[label] = SymmetricTensor(ev1, ev2, evals);
  }

  mesh->updateCentersNormals();

  // display the parent labels if they exist, to make sure the PDGs will be displayed properly
  bool axisForParents = false;
  if(not mesh->parentLabelMap().empty()) {
    int countParents = 0, countCells = 0;
    const IntIntMap& parentLabelMap = mesh->parentLabelMap();
    const IntPoint3fMap& labelCenter = mesh->labelCenter();
    forall(const IntSymTensorPair& p, cellAxis) {
      if(parentLabelMap.find(p.first) != parentLabelMap.end())
        ++countParents;
      if(labelCenter.find(p.first) != labelCenter.end())
        ++countCells;
    }
    axisForParents = countParents >= countCells;
  }
  if(axisForParents) {
    // try to guess if the labels are for the parents
    mesh->showParents();
    mesh->setParentAxis();
  } else
    mesh->setCellAxis();
  mesh->cellAxisDesc() = Description(type);
  mesh->cellAxisUnit() = axisUnit;

  mesh->updateTriangles();

  SETSTATUS(QString("Loaded directions for %1 cells").arg(cellAxis.size()));
  this->axisType = type;
  return true;
}

bool ClearCellAxis::operator()(Mesh* mesh)
{
  mesh->clearCellAxis();
  mesh->updateTriangles();
  return true;
}
REGISTER_MESH_PROCESS(ClearCellAxis);

bool HideCellAxis::operator()(Mesh* mesh)
{
  mesh->cellAxisScaled().clear();
  mesh->cellAxisColor().clear();

  mesh->updateTriangles();
  return true;
}
REGISTER_MESH_PROCESS(HideCellAxis);

} // namespace process
} // namespace lgx
