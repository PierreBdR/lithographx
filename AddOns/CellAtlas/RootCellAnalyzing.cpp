/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "RootCellAnalyzing.hpp"

#include <Information.hpp>

namespace mgx
{
namespace process
{

static bool uniqueTri(const vvgraph &S, const vertex &v, const vertex &n, const vertex &m)
{
  if(v.id() <= n.id() or v.id() <= m.id() or n.id() == m.id()
                                or !S.edge(v, n) or !S.edge(v, m) or !S.edge(n, m))
    return false;
  else
    return true;
}

// finds an intersection point of a given cellmidpoint and a direction vector with the cell wall
// recursive to function with stopcounter to handle rare cases of no solutions, in case of multiple solutions, just take the last one found
Point3f findIntersectPoint(Point3f coordCellCentroid, Point3f& dirVec, triVector& cellTriangles, int& stopCounter)
{
  int counter=0;
  Point3f coord (0,0,0);

  // check which triangles are hit by the vector
  forall(const tri &t, cellTriangles){
    Point3f t0 = t[0]->pos;
    Point3f t1 = t[1]->pos;
    Point3f t2 = t[2]->pos;
    Point3f intersectp;
    // filter out degenerated triangles
    double fac = 100;
    double maxTriLength = std::max(norm(t1-t0), std::max(norm(t2-t0), norm(t2-t1)));
    double minTriLength = std::min(norm(t1-t0), std::min(norm(t2-t0), norm(t2-t1)));

    if(maxTriLength < fac*minTriLength && rayTriangleIntersect(coordCellCentroid, coordCellCentroid+dirVec, t0, t1, t2, intersectp)==1){
      double weight = (double)(1)/(double)(counter+1);
      coord = (1-weight) * coord + weight * intersectp;
      counter++;
    }
  }

  if(counter==0) // no point found
  {
    Point3f add (0.001,0.001,0.001);
    if(stopCounter>10) // abort and give back zeroCoord (0,0,0)
    {
      return coord;
    } else if(stopCounter>3) { // move the cell center a bit forward or backward
      Point3f newCoordCellCentroid = coordCellCentroid-add;
      stopCounter++;
      return findIntersectPoint(newCoordCellCentroid, dirVec, cellTriangles, stopCounter);
    } else {
      Point3f newCoordCellCentroid = coordCellCentroid+add;
      stopCounter++;
      return findIntersectPoint(newCoordCellCentroid, dirVec, cellTriangles, stopCounter);
    }  
  } else { // point found
    stopCounter = counter;
    return coord;
  }


}

// estimates the size of the cell in a given direction
double RootCellAnalyzing::estimateCellLength(int currentLabel, Point3f coordCellCentroid, Point3f& dirVec, triVector& cellTriangles)
{
  Point3f coordPos, coordNeg;
  Point3f negDirVec = -1*dirVec;
  Point3f zeroCoord (0,0,0);
  int stopCountPos = 0;
  int stopCountNeg = 0;

  if(coordCellCentroid == zeroCoord) // cell too small
    return 0;

  coordPos = findIntersectPoint(coordCellCentroid, dirVec, cellTriangles, stopCountPos);
  coordNeg = findIntersectPoint(coordCellCentroid, negDirVec, cellTriangles, stopCountNeg);

  if(coordPos == zeroCoord || coordNeg == zeroCoord){
    Information::out << "*** Could not find intersect & determine cell size at label: " << currentLabel << endl;
    //rcp.rootData.cellBad[currentLabel] = true;
    badCells[currentLabel] = true;
    return 0;
  }

    return norm(coordPos - coordNeg);
}

// generates a map label -> triangles
bool generateLabelTriangleMap(int numCells, std::vector<int>& uniqueLabels, const vvgraph& segmentedMesh, labelVertexMap& lvMap, labelTriMap& cellTriangles)
{
  Progress progress("Analyze Cells 3D - Generate Label/Triangle Map", 0);
  for(int i=0; i<numCells; i++){
    int currentLabel = uniqueLabels[i];
    forall(const vertex &v, lvMap[currentLabel]){
      forall(const vertex &n, segmentedMesh.neighbors(v)){
        if(!progress.advance(1)) return false;
        if(v->label == n->label){
          const vertex& m = segmentedMesh.nextTo(v,n);
          if(uniqueTri(segmentedMesh, v, n, m)){
            if(m->label == n->label){
              tri currentTriangle;
              currentTriangle[0] = v;
              currentTriangle[1] = n;
              currentTriangle[2] = m;
              cellTriangles[v->label].push_back(currentTriangle);
            }
          }
        }
      }
    }
  }

  return true;
  }


// copied from MGX
Point3f closestpoint(Point3f n, double d, Point3f p)
{
  // n is the vector [A,B,C] that defines the plane
  // d is the distance of the plane from the origin
  // p is the point  [P,Q,R]

  double sumPn = p[0]*n[0] + p[1]*n[1] + p[2]*n[2];
  double sumNn = n[0]*n[0] + n[1]*n[1] + n[2]*n[2];

  double v = (d - sumPn) / sumNn;
  Point3f x = p + v * n;
  return x;
}

// analyzes the root based on: Bezier, segmeneted mesh and surface mesh
// creates multiple outputs:
// cellTriangles: map of label -> unique triangles
// cellCentroids: center of all cells
bool RootCellAnalyzing::analyzeEmbryo(std::vector<int>& uniqueLabels, const vvgraph& segmentedMesh, const vvgraph& surfaceMesh, bezierMap& bMap, bezierMap& diffbMap, labelVertexMap& lvMap, RootCellProcessing& rootDataBox, int firstLabel, double minVolume)
{
  Progress progress("Analyze Cells 3D - Analyze Cells", 0);
  // data structure with all cells (unique cell labels)
  int numCells = uniqueLabels.size();
  labelPosMap cellCentroids;
  labelDataMap cellVolumes;
  labelBoolMap cellBad;

  // generate labelTriMap
  labelTriMap cellTriangles;
  progress.restart("Analyze Cells 3D - Generate Label/Triangle Map", 0);
  if(!generateLabelTriangleMap(numCells, uniqueLabels, segmentedMesh, lvMap, cellTriangles)) return false;

  // calculate cell centroids and cell volumes
  Information::out << "*** The following cells will be ignored due to small volumes. label: ";
  progress.restart("Analyze Cells 3D - Calculate cell centroids and cell volumes", 0);
  for(int i=0; i<numCells; i++){
    int currentLabel = uniqueLabels[i];
    cellVolumes[currentLabel] = 0;
    cellCentroids[currentLabel] = 0;
    forall(const tri &t, cellTriangles[currentLabel]){

      // Compute x component of center of mass
      float volume = signedTetraVolume(t[0]->pos, t[1]->pos, t[2]->pos);
      cellCentroids[currentLabel] +=volume * (t[0]->pos + t[1]->pos + t[2]->pos) / 4.0;
      cellVolumes[currentLabel] += volume;
    }
    // Find cell center
    if(cellVolumes[currentLabel] > minVolume) {
      cellCentroids[currentLabel] /= cellVolumes[currentLabel];
      cellBad[currentLabel] = false;
    } else {
      cellBad[currentLabel] = true;
      Information::out << currentLabel << ", ";
      cellVolumes[currentLabel] = 0;
      Point3f zeroCoord (0,0,0);
      cellCentroids[currentLabel] = zeroCoord;
    }
  }
  Information::out << "." << endl;

  labelDataMap radialDis;
  labelDataMap scaledRadialDis;
  labelDataMap arclengths;
  labelPosMap diffBezInterp;
  labelPosMap bezInterp;
  int bezSize = bMap.size();

  // calculate arclengths by finding the shortest distance to the bezier line for each centroid
//#pragma omp parallel for
  progress.restart("Analyze Cells 3D - Calculate Arclengths", 0);
  for(int i=0; i<numCells; i++){
    //cout << "DEBUG" << __LINE__ << bMap.size() << endl;
    int currentLabel = uniqueLabels[i];
    Point3f currentCentroid = cellCentroids[currentLabel];
    double minDis = 1E20;
    double arclength = 0;
    Point3f diffBezInterpPoint (0,0,0); 
    Point3f bezInterpPoint (0,0,0); 

    for(int j=1; j<bezSize; j++){
      double currentDistance = distLinePoint(bMap[j-1], bMap[j], currentCentroid, true);
      if(currentDistance < minDis){
        minDis = currentDistance;
        // interpolate the discretized bezier for more accurate arclength
        double a = norm(bMap[j] - currentCentroid);
        double b = currentDistance;
        double weight = 0;
        if(a*a-b*b>0) // could be sometimes smaller 0 by a tiny bit
          weight = (double)(std::sqrt(a*a-b*b))/(double)(norm(bMap[j]-bMap[j-1]));
        arclength = (double)(j-1)/bMap.size() + weight * (double)(1)/bMap.size();
        diffBezInterpPoint = (1-weight) * diffbMap[j-1] + weight * diffbMap[j];
        bezInterpPoint = (1-weight) * bMap[j-1] + weight * bMap[j];
      }
      //radialDis.push_back(minDis); // commented out here as this is calculated later
      //scaledRadialDis.push_back(minDis);
    }
    arclengths[currentLabel] = arclength;
    diffBezInterp[currentLabel] = diffBezInterpPoint;
    bezInterp[currentLabel] = bezInterpPoint;
  }


  // NEW AZIM
  labelDataMap azimCoord;

  //int firstLabel;
  Point3f firstCentroid = cellCentroids[firstLabel];
  Point3f firstBez = bezInterp[firstLabel];
  Point3f firstRad = (firstCentroid - firstBez)/norm(firstCentroid - firstBez);
  Point3f firstLong = diffBezInterp[firstLabel]/norm(diffBezInterp[firstLabel]);

  Point3f planeNormal = firstLong ^ firstRad;
//#pragma omp parallel for
  progress.restart("Analyze Cells 3D - Calculate Circumferential Angles", 0);
  for(int i=0; i<numCells; i++){
    int currentLabel = uniqueLabels[i];
    double d = planeNormal * bezInterp[currentLabel];
    Point3f planePoint = closestpoint(planeNormal,d,cellCentroids[currentLabel]);
    Point3f xP = planePoint - bezInterp[currentLabel];
    Point3f xC = cellCentroids[currentLabel] - bezInterp[currentLabel];
    double sP = xP * firstRad;
    double sC = xC * planeNormal;
    azimCoord[currentLabel] = acos(norm(xP) / norm(xC));


    if(sP < 0){
      if(sC < 0){
        azimCoord[currentLabel]=PI-azimCoord[currentLabel];
      } else {
        azimCoord[currentLabel]+=PI;
      }
    } else {
      if(sC < 0){
        // do nothing
      } else {
        azimCoord[currentLabel]=2*PI-azimCoord[currentLabel];
      }
    }

    //cout << "azim   " << azimCoord[currentLabel] << endl;
  }

  // calculate cell sizes
  // create cell local coordinate system

  // distance of each vertex to the bezier
  std::map<int, double> surfaceRadialDis;
  std::map<int, double> surfaceArclengths;

  std::map<int, Point3f> surfDiffBezInterp;
  std::map<int, Point3f> surfBezInterp;

  progress.restart("Analyze Cells 3D - Finding radial of surface mesh nodes", 0);
  // Finding radial of surface mesh nodes
  forall(const vertex &v, surfaceMesh){
    if(!progress.advance(1)) return false;
    Point3f currentPos = v->pos;
    double minDis = 1E20, arclength = 0;
    int bezSize = bMap.size();
    Point3f diffBezInterpPoint (0,0,0); 
    Point3f bezInterpPoint (0,0,0); 
    for(int j=1; j<bezSize; j++){
      double currentDistance = distLinePoint(bMap[j-1], bMap[j], currentPos, true);
      if(currentDistance < minDis){
        minDis = currentDistance;
        // interpolate the discretized bezier for more accurate arclength
        double a = norm(bMap[j] - currentPos);
        double b = currentDistance;
        double weight = (double)(std::sqrt(a*a-b*b))/(double)(norm(bMap[j]-bMap[j-1]));
        arclength = (double)(j-1)/bMap.size() + weight * (double)(1)/bMap.size();
        diffBezInterpPoint = (1-weight) * diffbMap[j-1] + weight * diffbMap[j];
        bezInterpPoint = (1-weight) * bMap[j-1] + weight * bMap[j];
      }
    }
    surfaceRadialDis[v->saveId] = minDis;
    surfDiffBezInterp[v->saveId] = diffBezInterpPoint;
    surfBezInterp[v->saveId] = bezInterpPoint;
    surfaceArclengths[v->saveId] = arclength;
    //cout << "trim " << currentPos << "  dis  " << minDis << "    arc   " << arclength << "   "   << endl;
  }
  
  // NEW AZIM OF SURFACE MESH

  std::map<int, double> surfaceAzim;
  progress.restart("Analyze Cells 3D - Calculate Circumferential Angles of the surface mesh", 0);
  forall(const vertex &v, surfaceMesh){
    if(!progress.advance(1)) return false;
    Point3f currentPos = v->pos;

    double d = planeNormal * bezInterp[v->saveId];
    Point3f planePoint = closestpoint(planeNormal,d,currentPos);
    Point3f xP = planePoint - bezInterp[v->saveId];
    Point3f xC = currentPos - bezInterp[v->saveId];
    double sP = xP * firstRad;
    double sC = xC * planeNormal;
    surfaceAzim[v->saveId] = acos(norm(xP) / norm(xC));


    if(sP < 0){
      if(sC < 0){
        surfaceAzim[v->saveId]=PI-surfaceAzim[v->saveId];
      } else {
        surfaceAzim[v->saveId]+=PI;
      }
    } else {
      if(sC < 0){
        // do nothing
      } else {
        surfaceAzim[v->saveId]=2*PI-surfaceAzim[v->saveId];
      }
    }

  }

  // Finding local cell axes
  labelPosMap dirRad, dirLong, dirCirc;

  // distance of each cell to trimming (surface) mesh
  //int sizeSurfaceMesh = surfaceMesh.size();
  std::vector<double> surfDisVec;
  progress.restart("Analyze Cells 3D - Calculate distance of each cell to the surface mesh", 0);
  for(int i=0; i<numCells; i++){
    //cout << "DEBUG" << __LINE__ << endl;
    if(!progress.advance(1)) return false;
    int currentLabel = uniqueLabels[i];
    Point3f currentCentroid = cellCentroids[currentLabel];
    double minDis = 1E20;
    Point3f minPos;
    int minId;
    //int counter = 0;
    forall(const vertex &v, surfaceMesh){
      //surfDisVec.push_back(norm(currentCentroid-v->pos));
      double epsilon = 0.015*10;
      double arcDif = surfaceArclengths[v->saveId] - arclengths[currentLabel];
      if(arcDif < 0) arcDif = -arcDif;
      if(arcDif < epsilon) {
        if(norm(currentCentroid-v->pos) < minDis){
          minDis = norm(currentCentroid-v->pos);
          minPos = v->pos;
          minId = v->saveId;
        }
      }
      //counter++;
    }
    if(minDis == 1E20){
      cout << "Warning: Radial of cell " << currentLabel << " set to 0 (could not calculate distance to the surface mesh)" << endl; 
      radialDis[currentLabel] = 0;
      scaledRadialDis[currentLabel] = 0;
      dirRad[currentLabel] = currentCentroid;
    } else {
    //cout << "DEBUG" << __LINE__ << endl;
      radialDis[currentLabel] = std::max(0.0,surfaceRadialDis[minId] - minDis);
      scaledRadialDis[currentLabel] = radialDis[currentLabel]/surfaceRadialDis[minId];

    // generate rad coord system
      dirRad[currentLabel] = (minPos - currentCentroid)/minDis;
    }
    //cout << "  minPos  " << minPos<< "  minId  " << minId<< "  minDis  " << minDis<< "  radDir  " << radDir[currentLabel] << endl;
  }

  //generate tan & binorm coord system
//#pragma omp parallel for
  progress.restart("Analyze Cells 3D - Generate cell coord system", 0);
  for(int i=0; i<numCells; i++){
    // interpolate diffBez in arclengths points for all 3 dimensions
    int currentLabel = uniqueLabels[i];
    dirLong[currentLabel] = diffBezInterp[currentLabel]/norm(diffBezInterp[currentLabel]);
    dirCirc[currentLabel] = dirLong[currentLabel] ^ dirRad[currentLabel];
    dirLong[currentLabel] = - dirCirc[currentLabel] ^ dirRad[currentLabel];
  }
 
  labelDataMap lengthLong, lengthRad, lengthCirc;
  // extract cell morphology
//#pragma omp parallel for
  progress.restart("Analyze Cells 3D - Calculate cell lengths", 0);
  for(int i=0; i<numCells; i++){
    int currentLabel = uniqueLabels[i];

    lengthLong[currentLabel] = estimateCellLength(currentLabel, cellCentroids[currentLabel], dirLong[currentLabel], cellTriangles[currentLabel]);

    lengthRad[currentLabel] = estimateCellLength(currentLabel, cellCentroids[currentLabel], dirRad[currentLabel], cellTriangles[currentLabel]);

    lengthCirc[currentLabel] = estimateCellLength(currentLabel, cellCentroids[currentLabel], dirCirc[currentLabel], cellTriangles[currentLabel]);

  }
  progress.restart("Analyze Cells 3D - Fill data structures", 0);
  // write all data to root data structure
  rootDataBox.rootData.cellVolumes = cellVolumes;

  rootDataBox.rootData.cellBad = cellBad;

  rootDataBox.rootData.radialDis = radialDis;
  rootDataBox.rootData.scaledRadialDis = scaledRadialDis;
  rootDataBox.rootData.arclengths = arclengths;

  rootDataBox.rootData.dirRad = dirRad;
  rootDataBox.rootData.dirLong = dirLong;
  rootDataBox.rootData.dirCirc = dirCirc;
  
  rootDataBox.rootData.lengthRad = lengthRad;
  rootDataBox.rootData.lengthLong = lengthLong;
  rootDataBox.rootData.lengthCirc = lengthCirc;
  rootDataBox.rootData.cellBad = badCells;
  rootDataBox.rootData.azimuthal = azimCoord;

  rootDataBox.rootData.bez = bMap;
  rootDataBox.rootData.diffBez = diffbMap;
  rootDataBox.rootData.cellCentroids = cellCentroids;
  rootDataBox.rootData.bezInterp = bezInterp;
  rootDataBox.rootData.diffBezInterp = diffBezInterp;

  rootDataBox.rootData.labelFirstCell = firstLabel;
  rootDataBox.correctDirections();

  // curently not needed
  //rootDataBox.rootData.lvMap = lvMap;
  //rootDataBox.rootData.cellTriangles = cellTriangles;
  //rootDataBox.rootData.cellCentroids = cellCentroids;

  return true;

}
}
}
