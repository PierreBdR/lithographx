/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "Python.hpp"

#include "Libraries.hpp"
#include "PythonScript.hpp"

#include <Defer.hpp>
#include "Dir.hpp"
#include <Forall.hpp>
#include <Information.hpp>
#include "lgx_module.hpp"

#include <QFileDialog>
#include <QTextStream>
#include <stddef.h>
#include <stdio.h>

namespace lgx {
namespace process {

bool PythonScript::initialize(ParmList& parms, QWidget* parent)
{
  QString filename = parms[0].toString();
  bool choose_file = parmToBool(parms[1]);

  if(choose_file or filename.isEmpty()) {
    filename = QFileDialog::getOpenFileName(parent, "Choose python script to execute", filename,
                                            "Python scripts (*.py);;All files (*.*)");
    // check file
    if(filename.isEmpty())
      return false;
  }
  // ok all good
  parms[0] = filename;
  parms[1] = "Yes"; // Always reset to Yes!

  return true;
}

bool PythonScript::operator()(QString filename)
{
  bool success = false;
  QByteArray filename_8bit = filename.toLocal8Bit();
  PythonInterpreter interpreter;
  if(not interpreter.valid())
      return setErrorMessage("Python interpreter cannot be initialized.");
  filename = util::resolvePath(filename);
  Information::out << "Opening file '" << filename << "'" << endl;
  python::ProcessRunner::registerBaseProcess(this);
  auto defered = util::defer([]() -> void {
                               PyErr_Clear();
                               python::ProcessRunner::popBaseProcess();
                             });
  {
    PyObject *main = PyImport_AddModule("__main__");
    if(not main) {
      Information::err << "Error, couldn't import __main__ module" << endl;
      return {};
    }
    PyObject *main_dict = PyModule_GetDict(main);
    if(not main_dict) {
      Information::err << "Error, couldn't get __main__ module's dictionary" << endl;
      return {};
    }
    // Create dictionnaries for the context
    python::SafePyObject locals = PyDict_New();
    // First, add the __main__ module and set its file name to the file loaded
    QString initScript = "__file__ = '%1'\n"
                         "from lgxPy import Stack, Mesh, Global";

    initScript = initScript.arg(filename);

    python::SafePyObject res_init = PyRun_String(initScript.toUtf8().data(), Py_file_input, main_dict, locals);
    if(not res_init)
      return setErrorMessage("Couldn't initialize python's script: " + python::getError());

#ifdef IS_PY3K
    FILE* file = fopen(filename_8bit.data(), "r");
    if(not file) {
        setErrorMessage(QString("Cannot open file '%1' for reading: ").arg(filename));
        return false;
    }

    python::SafePyObject res = PyRun_FileEx(file, filename_8bit.data(), Py_file_input,
                                            main_dict, locals, 1);
    if(not res) {
      return setErrorMessage(python::getError());
    } else {
      success = true;
    }
#else
    python::SafePyObject file = PyFile_FromString(filename_8bit.data(), "r");
    if(not file) {
        setErrorMessage(QString("Cannot open file '%1' for reading: ").arg(filename) + python::getError());
        return false;
    }

    python::SafePyObject res = PyRun_FileEx(PyFile_AsFile(file), filename_8bit.data(), Py_file_input,
                                            main_dict, locals, 0);
    if(not res) {
      return setErrorMessage(python::getError());
    } else {
      success = true;
    }
#endif
  }
  return success;
}

REGISTER_GLOBAL_PROCESS(PythonScript);

DECLARE_LIBRARY("Python", PY_VERSION, "Python is a programming language that"
                "lets you work more quickly and integrate your systems more"
                "effectively.", "https://www.python.org",
                ":/images/PythonLogo.png");

} // namespace process
} // namespace lgx
