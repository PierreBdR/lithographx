/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef MISC_H
#define MISC_H
/**
 * \file Misc.hpp
 *
 * Misc. definitions and utilities
 */
#include <LGXConfig.hpp>
#include <GL.hpp>

#include <Color.hpp>
#include <EdgeData.hpp>
#include <LGXViewer/qglviewer.h>
#include <Vector.hpp>
#include <VertexData.hpp>
#include <VVGraph.hpp>

#include <QDir>
#include <QList>
#include <QtGui>

namespace lgx {

extern LGX_EXPORT const QString UM;
extern LGX_EXPORT const QString UM2;
extern LGX_EXPORT const QString UM3;
extern LGX_EXPORT const QString UM_1;
extern LGX_EXPORT const QString UM_2;

#ifdef _MSC_VER
template class LGX_EXPORT graph::Vertex<VertexData, std::allocator<VertexData> >;
template class LGX_EXPORT graph::Edge<EdgeData>;
template class LGX_EXPORT graph::VVGraph<VertexData, EdgeData>;
#endif

// Type of the VV graph
typedef graph::VVGraph<VertexData, EdgeData> vvgraph;

// Type of a vertex
typedef vvgraph::vertex_t vertex;

// Type of an edge
typedef vvgraph::edge_t edge;

using qglviewer::Quaternion;
typedef util::Vector<2, int> Point2i;
typedef util::Vector<3, GLuint> Point3GLui;
typedef util::Vector<4, GLuint> Point4GLui;
typedef util::Vector<3, GLubyte> Point3GLub;
typedef util::Vector<4, GLubyte> Point4GLub;
typedef util::Color<float> Colorf;
typedef util::Vector<3, float> Point3f;

namespace util {
/**
 * Extract a Point3f from a string.
 *
 * If the string is not a valid 3D point, returns NaN in all values of the
 * point.
 */
LGX_EXPORT Point3f stringToPoint3f(QString s, bool *ok = nullptr);
/**
 * Extract a Point2f from a string.
 *
 * If the string is not a valid 2D point, returns NaN in all values of the
 * point.
 */
LGX_EXPORT Point2f stringToPoint2f(QString s, bool *ok = nullptr);
/**
 * Extract a Point3u from a string.
 */
LGX_EXPORT Point3u stringToPoint3u(QString s, bool *ok = nullptr);

/**
 * Extract a Point3u from a string.
 */
LGX_EXPORT Point3i stringToPoint3i(QString s, bool *ok = nullptr);

/**
 * Split a string into fields.
 *
 * Fields are separated by: space, comma, colon or semi-colon
 *
 * \param s String to split
 * \param seps List of separator. Follows Perl regexp characters.
 *
 * \note if \c seps is empty, it is equivalent to \c "\\s*[[:space:],;:]\\s*"
 */
LGX_EXPORT QStringList splitFields(const QString& s);

/// Map unique colors to indices
inline Point3GLub vMapColor(uint u)
{
  u++;
  return (Point3GLub(u / (256 * 256), u / 256 % 256, u % 256));
}

/// Map unique colors to indices
inline uint vMapColor(Point3GLub& p) {
  return ((int(p.x()) * 256 * 256 + int(p.y()) * 256 + int(p.z())) - 1);
}

/// Create representation of a string that can be written in a single line, without spaces
inline QString shield(QString s)
{
  s.replace("\\", "\\\\");
  s.replace(" ", "\\s");
  s.replace("\n", "\\n");
  s.replace("\t", "\\t");
  s.replace("\"", "\\\"");
  s.replace("\'", "\\\'");
  return s;
}

/// Retrieve a string that has been shielded with shield
inline QString unshield(QString s)
{
  s.replace("\\\'", "\'");
  s.replace("\\\"", "\"");
  s.replace("\\s", " ");
  s.replace("\\n", "\n");
  s.replace("\\t", "\t");
  s.replace("\\\\", "\\");
  return s;
}

/// Shield a string to send it to the python interpreter
inline QString shield_python(QString s)
{
  s.replace("\\", "\\\\");
  s.replace("\n", "\\n");
  s.replace("\t", "\\t");
  s.replace("\"", "\\\"");
  s.replace("\'", "\\\'");
  return s;
}

/// Retrieve a string that is retrieved from the python representation
inline QString unshield_python(QString s)
{
  s.replace("\\\'", "\'");
  s.replace("\\\"", "\"");
  s.replace("\\n", "\n");
  s.replace("\\t", "\t");
  s.replace("\\\\", "\\");
  return s;
}

/// Return a percent-encoding of the text
QString toPercentEncoding(QString text);

/// Convert a percent-encoded text to the original one
QString fromPercentEncoding(QString text);

/// Convert an object to a QString using stream operators
template <typename T>
QString toQString(const T& t) {
  QString result;
  {
    QTextStream ts(&result);
    ts << t;
  }
  return result;
}

} // namespace util
} // namespace lgx
#endif
