/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "ColorBar.hpp"

#include "Colors.hpp"
#include "Geometry.hpp"
#include "Information.hpp"
#include "Tie.hpp"

#include <climits>
#include <cmath>
#include <QFontMetricsF>
#include <QSettings>
#include <QStringList>
#include <QTextStream>
#include <vector>

#ifdef _MSC_VER
#ifdef round
#  undef round
#endif
namespace {
double round(double value) {
  return std::floor(value + 0.5);
}
}
#endif

namespace lgx {

using Information::err;

Point2d toPoint2d(const QPoint& p) {
  return Point2d(p.x(), p.y());
}

Point2d toPoint2d(const QPointF& p) {
  return Point2d(p.x(), p.y());
}

ColorBar::ColorBar(Position pos)
  : font()   //"Monospace")
  , prev_width(-1)
  , prev_height(-1)
{
  // font.setStyleHint(QFont::TypeWriter);
  font.setStyleStrategy(QFont::OpenGLCompatible);
  font.setStyleStrategy(QFont::PreferAntialias);
  position = pos;
  vmin = 0;
  vmax = 1;

  scale_length = 0.3;
  width = 20;
  distance_to_border = 5;
  text_to_bar = 5;
  tick_size = 2;
  exp_size = 0.7;
  epsilon = 1e-9;
  line_width = 2;
  globalScaling = 1.0;
}

void limit_rect(QRectF& rect, const QSize& size, double lim_width, double lim_height)
{
  if(rect.left() < lim_width) {
    rect.moveLeft(lim_width);
    if(size.width() - rect.right() < lim_width)
      rect.setRight(size.width() - lim_width);
  } else if(size.width() - rect.right() < lim_width) {
    rect.moveRight(size.width() - lim_width);
    if(rect.left() < lim_width)
      rect.setLeft(lim_width);
  }
  if(rect.top() < lim_height) {
    rect.moveTop(lim_height);
    if(size.height() - rect.bottom() < lim_height)
      rect.setBottom(size.height() - lim_height);
  } else if(size.height() - rect.bottom() < lim_height) {
    rect.moveBottom(size.height() - lim_height);
    if(rect.top() < lim_height)
      rect.setTop(lim_height);
  }
}

void ColorBar::draw(QGLViewer* viewer, GLuint texId, QPaintDevice* device) const
{
  if(device == 0)
    device = viewer;
  if(vmin == vmax) {
    DEBUG_OUTPUT("Not drawing colorbar: vmin == vmax");
    return;
  }
  if(fabs(vmin - vmax) / (fabs(vmin) + fabs(vmax)) < epsilon) {
    DEBUG_OUTPUT("Not drawing colorbar: vmin ~ vmax");
    return;
  }
  if(not std::isfinite(vmin) or not std::isfinite(vmax)) {
    DEBUG_OUTPUT("Not drawing colorbar: vmin or vmax are not finite");
    return;
  }
  opengl->glPushAttrib(GL_ALL_ATTRIB_BITS);
  opengl->glDisable(GL_LIGHTING);
  opengl->glDisable(GL_LIGHT0);
  opengl->glDisable(GL_LIGHT1);
  opengl->glDisable(GL_LIGHT2);
  opengl->glDisable(GL_LIGHT3);
  opengl->glDisable(GL_DITHER);
  opengl->glDisable(GL_DEPTH_TEST);
  opengl->glDisable(GL_CULL_FACE);
  opengl->glDisable(GL_POLYGON_OFFSET_FILL);
  opengl->glDisable(GL_POLYGON_OFFSET_POINT);
  opengl->glDisable(GL_POLYGON_OFFSET_LINE);
  opengl->glDisable(GL_AUTO_NORMAL);

  opengl->glColor3f(1, 1, 1);

  startScreenCoordinatesSystem(device);
  do {
    DEBUG_OUTPUT("***** START ColorBar::draw ******" << endl);
    double w = device->width() / globalScaling;
    double h = device->height() / globalScaling;

    if(w == 0 or h == 0) {
      DEBUG_OUTPUT("Zero-sized device" << endl);
      break;
    }

    // First, find the position of the scale bar
    QFontMetricsF metric(font, device);
    QStringList font_test;
    double lim_width = 0;
    for(int i = 0; i < 10; ++i) {
      QString s = QString::number(i);
      font_test << s + s + s + s + s;
    }

    forall(QString t, font_test) {
      lim_width = std::max(lim_width, metric.width(t));
    }
    double lim_height = 3 * metric.height();
    double shift_length;
    switch(position) {
    case Top:
    case Bottom:
      shift_length = w * (1 - scale_length) / 2;
      break;
    case Left:
    case Right:
      shift_length = h * (1 - scale_length) / 2;
      break;
    default:
      shift_length = distance_to_border;
    }

    double delta_value = vmax - vmin;
    double shift_width = distance_to_border;

    QSize size(w, h);

    Point2d start_pos, end_pos, next_side;
    QRectF scale_rect;

    switch(position) {
    case Top: {
      scale_rect = QRectF(shift_length, shift_width, scale_length * w, width);
      limit_rect(scale_rect, size, lim_width, lim_height);
      start_pos = toPoint2d(scale_rect.bottomLeft());
      end_pos = toPoint2d(scale_rect.bottomRight());
      next_side = toPoint2d(scale_rect.topRight() - scale_rect.bottomRight());
    } break;
    case Right: {
      scale_rect = QRectF(w - shift_width - width, shift_length, width, scale_length * h);
      limit_rect(scale_rect, size, lim_width, lim_height);
      start_pos = toPoint2d(scale_rect.bottomLeft());
      end_pos = toPoint2d(scale_rect.topLeft());
      next_side = toPoint2d(scale_rect.bottomRight() - scale_rect.bottomLeft());
    } break;
    case Bottom: {
      scale_rect = QRectF(shift_length, h - shift_width - width, scale_length * w, width);
      limit_rect(scale_rect, size, lim_width, lim_height);
      start_pos = toPoint2d(scale_rect.topLeft());
      end_pos = toPoint2d(scale_rect.topRight());
      next_side = toPoint2d(scale_rect.bottomRight() - scale_rect.topRight());
    } break;
    case Left: {
      scale_rect = QRectF(shift_width, shift_length, width, scale_length * h);
      limit_rect(scale_rect, size, lim_width, lim_height);
      start_pos = toPoint2d(scale_rect.bottomRight());
      end_pos = toPoint2d(scale_rect.topRight());
      next_side = toPoint2d(scale_rect.bottomLeft() - scale_rect.bottomRight());
    } break;
    case TopLeft: {
      if(orientation == Horizontal) {
        scale_rect = QRectF(shift_length, shift_width, scale_length * w, width);
        limit_rect(scale_rect, size, lim_width, lim_height);
        start_pos = toPoint2d(scale_rect.bottomLeft());
        end_pos = toPoint2d(scale_rect.bottomRight());
        next_side = toPoint2d(scale_rect.topRight() - scale_rect.bottomRight());
      } else {
        scale_rect = QRectF(shift_width, shift_length, width, scale_length * h);
        limit_rect(scale_rect, size, lim_width, lim_height);
        start_pos = toPoint2d(scale_rect.bottomRight());
        end_pos = toPoint2d(scale_rect.topRight());
        next_side = toPoint2d(scale_rect.bottomLeft() - scale_rect.bottomRight());
      }
    } break;
    case TopRight: {
      if(orientation == Horizontal) {
        scale_rect = QRectF(w * (1 - scale_length) - shift_length, shift_width, scale_length * w, width);
        limit_rect(scale_rect, size, lim_width, lim_height);
        start_pos = toPoint2d(scale_rect.bottomLeft());
        end_pos = toPoint2d(scale_rect.bottomRight());
        next_side = toPoint2d(scale_rect.topRight() - scale_rect.bottomRight());
      } else {
        scale_rect = QRectF(w - shift_width - width, shift_length, width, scale_length * h);
        limit_rect(scale_rect, size, lim_width, lim_height);
        start_pos = toPoint2d(scale_rect.bottomLeft());
        end_pos = toPoint2d(scale_rect.topLeft());
        next_side = toPoint2d(scale_rect.bottomRight() - scale_rect.bottomLeft());
      }
    } break;
    case BottomLeft: {
      if(orientation == Horizontal) {
        scale_rect = QRectF(shift_length, h - shift_width - width, scale_length * w, width);
        limit_rect(scale_rect, size, lim_width, lim_height);
        start_pos = toPoint2d(scale_rect.topLeft());
        end_pos = toPoint2d(scale_rect.topRight());
        next_side = toPoint2d(scale_rect.bottomRight() - scale_rect.topRight());
      } else {
        scale_rect = QRectF(shift_width, h * (1 - scale_length) - shift_length, width, scale_length * h);
        limit_rect(scale_rect, size, lim_width, lim_height);
        start_pos = toPoint2d(scale_rect.bottomRight());
        end_pos = toPoint2d(scale_rect.topRight());
        next_side = toPoint2d(scale_rect.bottomLeft() - scale_rect.bottomRight());
      }
    } break;
    case BottomRight: {
      if(orientation == Horizontal) {
        scale_rect
          = QRectF(w * (1 - scale_length) - shift_length, h - width - shift_width, scale_length * w, width);
        limit_rect(scale_rect, size, lim_width, lim_height);
        start_pos = toPoint2d(scale_rect.topLeft());
        end_pos = toPoint2d(scale_rect.topRight());
        next_side = toPoint2d(scale_rect.bottomRight() - scale_rect.topRight());
      } else {
        scale_rect
          = QRectF(w - shift_width - width, h * (1 - scale_length) - shift_length, width, scale_length * h);
        limit_rect(scale_rect, size, lim_width, lim_height);
        start_pos = toPoint2d(scale_rect.bottomLeft());
        end_pos = toPoint2d(scale_rect.topLeft());
        next_side = toPoint2d(scale_rect.bottomRight() - scale_rect.bottomLeft());
      }
    } break;
    default:
      break;
    }
    if(scale_rect.width() == 0 or scale_rect.height() == 0) {
      DEBUG_OUTPUT("Zero-size colorbar" << endl);
      break;
    }
    Point2d shift_pos = (end_pos - start_pos) / delta_value;
    double length;
    if(orientation == Vertical)
      length = scale_rect.height();
    else
      length = scale_rect.width();

    DEBUG_OUTPUT("Scale bar rect = " << scale_rect.width() << "x" << scale_rect.height() << "+" << scale_rect.height()
                                     << "+" << scale_rect.width() << endl);

    // Get the ticks
    array ticks = selectValues(length, orientation == Vertical, &metric);
    if(ticks.size() == 0)
      break;
    if(DEBUG) {
      DEBUG_OUT << "List of ticks = ";
      for(size_t i = 0; i < ticks.size(); ++i) {
        DEBUG_OUT << ticks[i] << " ";
      }
      DEBUG_OUT << endl;
    }
    QString ticks_extra;
    QStringList ticks_str = _tick2str(ticks, &ticks_extra);

    DEBUG_OUTPUT("Ticks strings = " << ticks_str.join(", ") << endl);

    // Now, draw the gradient
    DEBUG_OUTPUT("Draw gradient" << endl);

    std::vector<Point2d> area(4);
    std::vector<float> texture(4);
    std::vector<Point3f> colors(4);
    auto bg = Point3f(Colors::instance()->getColor(Colors::BackgroundColor));
    colors[0] = bg;
    colors[1] = bg;
    colors[2] = bg;
    colors[3] = bg;
    texture[0] = 0;
    texture[1] = 0;
    texture[2] = 1;
    texture[3] = 1;
    area[0] = start_pos;
    area[1] = start_pos + next_side;
    area[2] = end_pos + next_side;
    area[3] = end_pos;

    opengl->glEnable(GL_BLEND);
    opengl->glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    opengl->glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    opengl->glEnableClientState(GL_VERTEX_ARRAY);
    opengl->glEnableClientState(GL_COLOR_ARRAY);

    // First, draw the background
    opengl->glVertexPointer(2, GL_DOUBLE, 0, area.data());
    opengl->glColorPointer(3, GL_FLOAT, 0, colors.data());
    opengl->glDrawArrays(GL_QUADS, 0, 4);

    opengl->glDisableClientState(GL_COLOR_ARRAY);
    opengl->glEnableClientState(GL_TEXTURE_COORD_ARRAY);
    // Then, draw the gradient
    opengl->glEnable(GL_TEXTURE_1D);
    opengl->glBindTexture(GL_TEXTURE_1D, texId);
    opengl->glTexCoordPointer(1, GL_FLOAT, 0, texture.data());
    opengl->glDrawArrays(GL_QUADS, 0, 4);
    opengl->glDisable(GL_TEXTURE_1D);

    opengl->glDisableClientState(GL_TEXTURE_COORD_ARRAY);
    opengl->glDisableClientState(GL_VERTEX_ARRAY);

    /*
     *opengl->glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
     *opengl->glEnable(GL_TEXTURE_1D);
     *opengl->glBindTexture(GL_TEXTURE_1D, texId);
     *opengl->glBegin(GL_QUADS);
     *opengl->glTexCoord1f(0);
     *opengl->glVertex2dv(start_pos.c_data());
     *opengl->glVertex2dv((start_pos + next_side).c_data());
     *opengl->glTexCoord1f(1);
     *opengl->glVertex2dv((end_pos + next_side).c_data());
     *opengl->glVertex2dv(end_pos.c_data());
     *opengl->glEnd();
     *opengl->glDisable(GL_TEXTURE_1D);
     */

    // Draw text
    DEBUG_OUTPUT("Draw text" << endl);

    // Compute the shifts
    double max_width = 0;
    double max_height = metric.height();
    forall(QString tick, ticks_str) {
      double w = metric.width(tick);
      if(max_width < w)
        max_width = w;
      DEBUG_OUTPUT(QString("Width of '%1' = %2").arg(tick).arg(w) << endl);
    }
    double shift = 0;
    if(orientation == Vertical) {
      switch(position) {
      case Left:
      case TopLeft:
      case BottomLeft:
        shift = text_to_bar + line_width;
        break;
      case Right:
      case TopRight:
      case BottomRight:
        shift = -text_to_bar - max_width - line_width;
        break;
      default:
        break;
      }
    } else {
      switch(position) {
      case Top:
      case TopLeft:
      case TopRight:
        shift = text_to_bar + line_width;
        break;
      case Bottom:
      case BottomLeft:
      case BottomRight:
        shift = -text_to_bar - max_height - line_width;
        break;
      default:
        break;
      }
    }

    DEBUG_OUTPUT("Shift = " << shift << endl);

    opengl->glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    Colorf col = Colors::getColor(Colors::LegendColor);
    opengl->glColor3fv(col.c_data());

    {
      QPainter paint(device);
      paint.scale(globalScaling, globalScaling);
      paint.setFont(font);
      paint.setPen(Colors::getQColor(Colors::LegendColor));

      for(size_t i = 0; i < ticks.size(); ++i) {
        QString ts = ticks_str[i];
        double t = ticks[i];
        double h = metric.height();
        double w = metric.width(ts);
        DEBUG_OUTPUT(QString("Width of '%1' = %2").arg(ts).arg(w) << endl);
        Point2d pos = start_pos + shift_pos * (t - vmin);
        if(orientation == Vertical) {
          pos.x() += shift + max_width - w;
          pos.y() -= h / 2;
        } else {
          pos.x() -= w / 3;
          pos.y() += shift;
        }
        pos.y() += h;
        paint.drawText(pos.x(), pos.y(), ts);
      }

      if(!ticks_extra.isEmpty() or !label.isEmpty()) {
        DEBUG_OUTPUT("Add extra text: '" << ticks_extra << "'" << endl);
        QString exp_txt = QString::fromUtf8("×10");
        DEBUG_OUTPUT(QString("exp_txt = '%1'").arg(exp_txt) << endl);
        double width = metric.width(exp_txt);
        QFont exp_font = QFont(font);
        if(exp_font.pixelSize() != -1)
          exp_font.setPixelSize(exp_size * exp_font.pixelSize());
        else
          exp_font.setPointSizeF(exp_size * exp_font.pointSizeF());
        QFontMetricsF exp_metric(exp_font, device);
        double exp_width = exp_metric.boundingRect(ticks_extra).width();
        double exp_height = exp_metric.boundingRect(ticks_extra).height();
        double total_height = std::max(metric.ascent(), metric.ascent() / 2 + exp_metric.ascent());
        double label_width;
        if(!label.isEmpty())
          label_width = metric.boundingRect(label).width();
        else
          label_width = 0;
        double space_width = metric.width(" ");
        Point2d pos = toPoint2d(scale_rect.topRight());
        if(ticks_extra.isEmpty()) {
          exp_width = width = space_width = 0;
          exp_txt = "";
        }
        double total_width = width + exp_width + label_width + space_width;
        if(orientation == Vertical) {
          pos.x() -= (scale_rect.width() + total_width) / 2;
          if(pos.x() < 0) {
            pos.x() = text_to_bar;
          } else if(pos.x() + total_width + text_to_bar > w) {
            pos.x() = w - total_width - text_to_bar;
          }
          pos.y() -= text_to_bar + total_height / 2;
        } else {
          switch(position) {
          case Bottom:
          case BottomLeft:
          case BottomRight:
            pos.y() += scale_rect.height() + text_to_bar + total_height;
            pos.x() -= total_width;
            break;
          case Top:
          case TopLeft:
          case TopRight:
            pos.y() -= text_to_bar;
            pos.x() -= total_width;
            break;
          default:
            break;
          }
        }

        DEBUG_OUTPUT("Rendering exp text" << endl);

        paint.drawText(pos.x(), pos.y(), exp_txt);

        Point2d label_pos = pos;

        label_pos.x() += width + exp_width + space_width;
        paint.drawText(label_pos.x(), label_pos.y(), label);

        DEBUG_OUTPUT("Rendering exponent" << endl);
        pos.x() += width;
        pos.y() -= metric.ascent() * 0.5;
        paint.setFont(exp_font);
        paint.drawText(pos.x(), pos.y() - exp_height, exp_width, exp_height,
                       Qt::AlignBottom | Qt::AlignLeft | Qt::TextDontClip, ticks_extra);
        paint.setFont(font);
      }
    }

    // Draw the box
    DEBUG_OUTPUT("Draw box" << endl);
    double lw = line_width * globalScaling;
    opengl->glLineWidth(lw);
    col = Colors::getColor(Colors::LegendColor);
    opengl->glColor3fv(col.c_data());
    double correct = (line_width > 0) ? (line_width / 2) : 0;

    Point2d p1 = start_pos;
    Point2d p2 = start_pos + next_side;
    Point2d p3 = end_pos + next_side;
    Point2d p4 = end_pos;

    Point2d d12 = correct * normalized(p2 - p1);
    Point2d d23 = correct * normalized(p3 - p2);
    Point2d d34 = correct * normalized(p4 - p3);
    Point2d d41 = correct * normalized(p1 - p4);

    opengl->glBegin(GL_LINES);
    opengl->glVertex2dv((p1 - d12).c_data());
    opengl->glVertex2dv((p2 + d12).c_data());
    opengl->glVertex2dv((p2 - d23).c_data());
    opengl->glVertex2dv((p3 + d23).c_data());
    opengl->glVertex2dv((p3 - d34).c_data());
    opengl->glVertex2dv((p4 + d34).c_data());
    opengl->glVertex2dv((p4 - d41).c_data());
    opengl->glVertex2dv((p1 + d41).c_data());
    opengl->glEnd();

    // At last, draw the ticks
    DEBUG_OUTPUT("Draw ticks" << endl);
    opengl->glBegin(GL_LINES);
    for(size_t i = 0; i < ticks.size(); ++i) {
      double t = ticks[i];
      Point2d pos1 = start_pos + shift_pos * (t - vmin);
      Point2d pos2 = pos1;
      if(orientation == Vertical) {
        pos1.x() = scale_rect.left();
        pos2.x() = pos1.x() + tick_size;
        opengl->glVertex2dv(pos1.c_data());
        opengl->glVertex2dv(pos2.c_data());

        pos1.x() = scale_rect.right();
        pos2.x() = pos1.x() - tick_size;
        opengl->glVertex2dv(pos1.c_data());
        opengl->glVertex2dv(pos2.c_data());
      } else {
        pos1.y() = scale_rect.top();
        pos2.y() = pos1.y() + tick_size;
        opengl->glVertex2dv(pos1.c_data());
        opengl->glVertex2dv(pos2.c_data());

        pos1.y() = scale_rect.bottom();
        pos2.y() = pos1.y() - tick_size;
        opengl->glVertex2dv(pos1.c_data());
        opengl->glVertex2dv(pos2.c_data());
      }
    }
    opengl->glEnd();

    DEBUG_OUTPUT("Done" << endl);
  } while(false);

  stopScreenCoordinatesSystem();
  opengl->glPopAttrib();
}

void ColorBar::_getValues(double start, double end, double delta, array& result) const
{
  double new_start = floor(start / delta) * delta;
  if((start < epsilon and fabs(new_start - start) > epsilon)
     or (start >= epsilon and fabs((new_start - start) / start) > epsilon))
    new_start += delta;
  int nb_values = floor((end + delta / 100 - new_start) / delta) + 1;
  DEBUG_OUTPUT("Values from " << start << " to " << end << " by " << delta << endl);
  DEBUG_OUTPUT("  Nb values = " << nb_values << endl);
  result.resize(nb_values);
  int i = 0;
  // TODO: reverse value and i in loop
  for(double value = new_start; value < end + delta / 100; value += delta) {
    result[i++] = value;
  }
  if(DEBUG) {
    DEBUG_OUT << "  value = ";
    for(size_t i = 0; i < result.size(); ++i) {
      DEBUG_OUT << result[i] << " ";
    }
  }
}

ColorBar::array ColorBar::selectValues(double length, bool is_vertical, const QFontMetricsF* arg_metric) const
{
  const QFontMetricsF* metric = arg_metric ? arg_metric : new QFontMetricsF(font);
  double min_dist = 1;
  if(!is_vertical)   // Find the maximum size of 2 figures
  {
    for(int i = 0; i < 10; ++i) {
      QString t = QString::number(i) + QString::number(i);
      double w = metric->boundingRect(t).width();
      if(min_dist < w)
        min_dist = w;
    }
  }
  DEBUG_OUTPUT("Min_dist between values = " << min_dist << endl);
  array result = _selectValues_direct(length, is_vertical, *metric, min_dist);
  if(arg_metric == 0)
    delete metric;
  return result;
}

ColorBar::array ColorBar::_selectValues_direct(double length, bool is_vertical, const QFontMetricsF& metric,
                                               double min_dist) const
{
  double start = vmin;
  double end = vmax;
  double real_delta, exp;
  util::tie(real_delta, exp) = _significant_digits(start, end);
  array ticks;
  _getValues(start, end, real_delta * exp, ticks);
  while(!_canRenderTicks(ticks, length, min_dist, is_vertical, metric)) {
    DEBUG_OUTPUT("Error, can't render ticks" << endl);
    if(real_delta == 1)
      real_delta = 2;
    else if(real_delta == 2)
      real_delta = 5;
    else if(real_delta == 5) {
      exp *= 10;
      real_delta = 1;
    } else {
      exp *= 10;
      real_delta = 2;
    }
    _getValues(start, end, real_delta * exp, ticks);
    if(ticks.size() < 2)
      return array();
    else
      DEBUG_OUTPUT("New number of values = " << ticks.size() << endl);
  }
  return ticks;
}

std::pair<double, double> ColorBar::_significant_digits(double start, double end) const
{
  double delta = (end - start) / 10;
  double first_num = floor(log10(delta));
  double exp = pow(10.0, first_num);
  double real_delta = ceil(delta / exp);
  if(real_delta > 6)
    real_delta = 10;
  else if(real_delta > 2)
    real_delta = 5;
  return std::make_pair(real_delta, exp);
}

QStringList ColorBar::_tick2str(const array& ticks, QString* extra) const
{
  if(DEBUG) {
    DEBUG_OUT << "_tick2str" << endl;
    DEBUG_OUT << "Representation of the ticks (";
    for(size_t i = 0; i < ticks.size(); ++i) {
      DEBUG_OUT << ticks[i];
      if(i < ticks.size() - 1)
        DEBUG_OUT << ',';
    }
    DEBUG_OUT << ")" << endl;
  }
  double m = abs(ticks).max();
  double exp;
  array new_ticks = ticks;
  double factor = 1;
  array absticks = abs(ticks);
  array absolute_numbers = absticks[ticks != 0.0];

  if(DEBUG) {
    DEBUG_OUT << "Working on numbers (";
    for(size_t i = 0; i < absolute_numbers.size(); ++i) {
      DEBUG_OUT << absolute_numbers[i];
      if(i < absolute_numbers.size() - 1)
        DEBUG_OUT << ',';
    }
    DEBUG_OUT << ")" << endl;
  }
  if(m <= 0.01) {
    DEBUG_OUTPUT("Small numbers" << endl);
    exp = log10(absolute_numbers).max();
    exp = floor(exp);
    factor = pow(10.0, exp);
    new_ticks /= factor;
    array t = abs(new_ticks.apply(round) - new_ticks);
    t[new_ticks != 0.0] /= new_ticks[new_ticks != 0.0];
    if((t > epsilon).max()) {
      exp = floor(log10(m));
      factor = pow(10.0, exp);
      new_ticks = ticks / factor;
    }
  } else if(m >= 20000) {
    DEBUG_OUTPUT("Big numbers" << endl);
    exp = round(log10(absolute_numbers).min());
    factor = pow(10.0, exp);
    new_ticks /= factor;
    DEBUG_OUTPUT("Last number = " << ticks[ticks.size() - 1] / factor << endl);
    DEBUG_OUTPUT("Last number = " << new_ticks[ticks.size() - 1] << endl);
    array t = abs(new_ticks.apply(round) - new_ticks);
    t[new_ticks != 0.0] /= new_ticks[new_ticks != 0.0];
    if((t > epsilon).max()) {
      exp = floor(log10(m));
      factor = pow(10.0, exp);
      new_ticks = ticks / factor;
    }
  } else {
    DEBUG_OUTPUT("Regular numbers" << endl);
#ifdef _MSC_VER
    unsigned long nan[2] = { 0xffffffff, 0x7fffffff };
    exp = *(double*)nan;
// exp = std::numeric_limits<double>::quiet_NaN();
#else
    exp = nan("");
#endif
  }
  new_ticks[abs((new_ticks * factor) / abs(ticks).max()) < epsilon] = 0;
  QStringList ticks_str;
  if(DEBUG) {
    DEBUG_OUT << "new_ticks = (";
    for(size_t i = 0; i < new_ticks.size(); ++i) {
      DEBUG_OUT << new_ticks[i];
      if(i < new_ticks.size() - 1)
        DEBUG_OUT << ',';
    }
    DEBUG_OUT << ")" << endl;
  }
  for(size_t i = 0; i < new_ticks.size(); ++i) {
    ticks_str << QString::number(new_ticks[i]);
  }
  if(extra) {
    if(std::isnan(exp))
      *extra = QString();
    else
      *extra = QString::number(int(round(exp)));
  }
  if(DEBUG) {
    DEBUG_OUT << "  max tick = " << ticks.max() << endl << "  exp = " << exp << endl << "  factor = " << factor << endl;
    if(extra)
      DEBUG_OUT << "  extra = " << *extra << endl;
  }
  return ticks_str;
}

bool ColorBar::_canRenderTicks(const array& ticks, double length, double min_dist, bool is_vertical,
                               const QFontMetricsF& font_metric) const
{
  double b = vmin;
  double e = vmax;
  double dl = length / (e - b);
  array pos = dl * (ticks - b);
  double cur_pos = -length;
  QStringList ticks_str = _tick2str(ticks);
  for(size_t i = 0; i < pos.size(); ++i) {
    QString t = ticks_str[i];
    double p = pos[i];
    QRectF r = font_metric.boundingRect(t);
    double w;
    if(is_vertical)
      w = r.height() / 2;
    else
      w = r.width() / 2;
    double left = p - w;
    if(left < cur_pos)
      return false;
    cur_pos = p + w + min_dist;
  }
  return true;
}

template <typename stream> void writepos(stream& s, const ColorBar::Position& pos)
{
  switch(pos) {
  case ColorBar::Left:
    s << "Left";
    break;
  case ColorBar::Right:
    s << "Right";
    break;
  case ColorBar::Top:
    s << "Top";
    break;
  case ColorBar::Bottom :
    s << "Bottom";
    break;
  case ColorBar::TopLeft:
    s << "TopLeft";
    break;
  case ColorBar::TopRight:
    s << "TopRight";
    break;
  case ColorBar::BottomLeft:
    s << "BottomLeft";
    break;
  case ColorBar::BottomRight:
    s << "BottomRight";
    break;
  }
}

template <typename str> bool readpos(const str& s, ColorBar::Position& pos)
{
  if(s == "Left")
    pos = ColorBar::Left;
  else if(s == "Right")
    pos = ColorBar::Right;
  else if(s == "Top")
    pos = ColorBar::Top;
  else if(s == "Bottom")
    pos = ColorBar::Bottom;
  else if(s == "TopLeft")
    pos = ColorBar::TopLeft;
  else if(s == "TopRight")
    pos = ColorBar::TopRight;
  else if(s == "BottomLeft")
    pos = ColorBar::BottomLeft;
  else if(s == "BottomRight")
    pos = ColorBar::BottomRight;
  else {
    pos = ColorBar::TopLeft;
    return false;
  }
  return true;
}

std::ostream& operator<<(std::ostream& s, const ColorBar::Position& pos)
{
  writepos(s, pos);
  return s;
}

std::istream& operator>>(std::istream& s, ColorBar::Position& pos)
{
  std::string str;
  s >> str;
  if(!readpos(str, pos)) {
    std::cerr << "Error, cannot interpret the position '" << str << "', position will default to Left" << std::endl;
  }
  return s;
}

QTextStream& operator<<(QTextStream& s, const ColorBar::Position& pos)
{
  writepos(s, pos);
  return s;
}

QTextStream& operator>>(QTextStream& s, ColorBar::Position& pos)
{
  QString str;
  s >> str;
  if(!readpos(str, pos)) {
    err << "Error, cannot interpret the position '" << str << "', position will default to Right" << endl;
  }
  return s;
}

QDataStream& operator<<(QDataStream& s, const ColorBar::Position& pos)
{
  s << (unsigned int)pos;
  return s;
}

QDataStream& operator>>(QDataStream& s, ColorBar::Position& pos)
{
  unsigned int i;
  s >> i;
  if(i < 4) {
    pos = (ColorBar::Position)i;
  } else {
    std::cerr << "Error, integer value " << i << " doesn't correspond to a valid position, using TopLeft as default"
              << std::endl;
    pos = ColorBar::TopLeft;
  }
  return s;
}

QTextStream& operator<<(QTextStream& ts, ColorBar::Orientation orientation)
{
  switch(orientation) {
    case ColorBar::Horizontal:
      ts << "Horizontal";
      break;
    case ColorBar::Vertical:
      ts << "Vertical";
      break;
  }
  return ts;
}

QTextStream& operator>>(QTextStream& ts, ColorBar::Orientation& orientation)
{
  QString ori;
  ts >> ori;
  ori = ori.toLower();
  if(ori == "horizontal")
    orientation = ColorBar::Horizontal;
  else if(ori == "vertical")
    orientation = ColorBar::Vertical;
  else {
    Information::err << "Error, invalid orientation: " << ori << endl;
    orientation = ColorBar::Vertical;
  }
  return ts;
}


void ColorBar::readParms(util::Parms& parms, QString section)
{
  loadDefaults();

  parms.optional(section, "ScaleLength", scale_length);
  parms.optional(section, "Width", width);
  parms.optional(section, "DistanceToBorder", distance_to_border);
  parms.optional(section, "TextToBar", text_to_bar);
  parms.optional(section, "TickSize", tick_size);
  parms(section, "ExpSize", exp_size, 0.7);
  parms(section, "Epsilon", epsilon, 1e-9);
  parms.optional(section, "LineWidth", line_width);
  int ps = 12;
  bool ok = parms.optional(section, "FontSize", ps);
  if(ok)
    font.setPointSize(ps);
  parms.optional(section, "Orientation", orientation);
  parms.optional(section, "Position", position);
  QString font_str;
  parms(section, "Font", font_str, font.toString());
  font.fromString(font_str);
}

void ColorBar::writeParms(QTextStream& pout, QString section)
{
  pout << endl;
  pout << "[" << section << "]" << endl;
  pout << "ScaleLength: " << scale_length << endl;
  pout << "Width: " << width << endl;
  pout << "DistanceToBorder: " << distance_to_border << endl;
  pout << "TextToBar: " << text_to_bar << endl;
  pout << "TickSize: " << tick_size << endl;
  pout << "ExpSize: " << exp_size << endl;
  pout << "Epsilon: " << epsilon << endl;
  pout << "LineWidth: " << line_width << endl;
  pout << "Position: " << position << endl;
  pout << "FontSize: " << font.pointSize() << endl;
  pout << "Orientation: " << orientation << endl;
  pout << "Font: " << font.toString() << endl;
}

void ColorBar::startScreenCoordinatesSystem(QPaintDevice* device) const
{
  opengl->glMatrixMode(GL_PROJECTION);
  opengl->glPushMatrix();
  opengl->glLoadIdentity();
  opengl->glOrtho(0, device->width() / globalScaling, device->height() / globalScaling, 0, 0.0, -1.0);

  opengl->glMatrixMode(GL_MODELVIEW);
  opengl->glPushMatrix();
  opengl->glLoadIdentity();
}

void ColorBar::scaleDrawing(double scale) {
  globalScaling = scale;
}

void ColorBar::restoreScale() {
  globalScaling = 1.0;
}

void ColorBar::stopScreenCoordinatesSystem() const
{
  opengl->glMatrixMode(GL_PROJECTION);
  opengl->glPopMatrix();

  opengl->glMatrixMode(GL_MODELVIEW);
  opengl->glPopMatrix();
}

void ColorBar::loadDefaults()
{
  QSettings settings;
  settings.beginGroup("ColorBar");

  QFont default_font;
  default_font.setStyleStrategy(QFont::OpenGLCompatible);
  default_font.setStyleStrategy(QFont::PreferAntialias);
  default_font.setPointSize(10);

  bool ok;
  auto pos = settings.value("Position", ColorBar::TopLeft).toInt(&ok);
  if(ok and pos >= 0 and pos < 8)
    position = (lgx::ColorBar::Position)pos;

  auto int_orient = settings.value("Orientation", ColorBar::Vertical).toInt(&ok);
  if(ok and int_orient >= 0 and int_orient < 2)
    orientation = (lgx::ColorBar::Orientation)int_orient;

  auto length = settings.value("Length", 0.3).toDouble(&ok);
  if(ok and length > 0 and length < 1) scale_length = length;

  auto width = settings.value("Width", 20).toUInt(&ok);
  if(ok and width > 2) this->width = width;

  auto dist_text = settings.value("DistanceToText", 5).toUInt(&ok);
  if(ok) text_to_bar = dist_text;

  auto dist_border = settings.value("DistanceToBorder", 5).toUInt(&ok);
  if(ok) distance_to_border = dist_border;

  auto tick_size = settings.value("TickSize", 2).toUInt(&ok);
  if(ok) this->tick_size = tick_size;

  auto textFont = settings.value("Font", default_font);
  if(textFont.canConvert<QFont>())
    font = textFont.value<QFont>();
  else
    textFont = default_font;

  auto line_width = settings.value("LineWidth", 2).toInt(&ok);
  if(ok) this->line_width = line_width;

  settings.endGroup();
}

} // namespace lgx
