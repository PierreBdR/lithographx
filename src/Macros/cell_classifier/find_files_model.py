from __future__ import absolute_import, print_function

from PyQt5.QtCore import Qt, QAbstractItemModel, QModelIndex, QVariant, QMetaType
from PyQt5.QtGui import QPalette, QColor
from lgxUtils.path import Path
from .utils import processFilePattern, correctFormat


OUTPUT_FILE = 0
OPTIONAL_FILE = 1
MANDATORY_FILE = 2


def typeToId(t):
    t = str(t).lower()
    if t == "output":
        return OUTPUT_FILE
    elif t == "optional":
        return OPTIONAL_FILE
    return MANDATORY_FILE


class FindFilesModel(QAbstractItemModel):
    """
    Model to find a list of files.

    The model uses a main file from which a base name is computed. Other files
    are found by using the basename in a format expression (e.g. replacing '{}'
    or '{0}' with the basename).
    """
    def __init__(self, base_folder, main_file, secondary_files, recursive, parent):
        """
        Parameters
        ==========
        main_file: (str, str)
            The name of the column and the pattern for the main file (first column)
        secondary_files : [(str, str, str)]
            List of (column name, col_format, col_type) for the other files.
            `col_type` can be "output", "optional" or "mandatory". The default is "mandatory".
        """
        QAbstractItemModel.__init__(self, parent)
        self._base_folder = Path(base_folder)
        self._recursive = bool(recursive)
        self._column_names = [str(main_file[0])] + [str(f[0]) for f in secondary_files]
        self._main_pattern = str(main_file[1])
        self._col_format = [correctFormat(f[1]) for f in secondary_files]
        self._col_type = [typeToId(f[2]) for f in secondary_files]
        self._used_columns = list(range(len(self._column_names)))
        self._found_files = []
        self._used_files = []
        self.findFiles()

    @property
    def baseFolder(self):
        return self._base_folder

    @baseFolder.setter
    def baseFolder(self, folder):
        """
        Change the folder from which the files are searched
        """
        folder = Path(folder)
        if not folder:
            folder = Path(".")
        if self._base_folder != folder:
            self._base_folder = folder
            self.findFiles()

    @property
    def recursive(self):
        return self._recursive

    @recursive.setter
    def recursive(self, on):
        """
        If True, the files are search in subfolders too
        """
        on = bool(on)
        if on != self._recursive:
            self._recursive = on
            self.findFiles()

    @property
    def mainFilePattern(self):
        return self._main_pattern

    @mainFilePattern.setter
    def mainFilePattern(self, pattern):
        """
        Change the pattern for the main file.
        """
        pattern = str(pattern)
        if pattern != self._main_pattern:
            self._main_pattern = pattern
            self.findFiles()

    @mainFilePattern.deleter
    def mainFilePattern(self):
        if self._main_pattern:
            self._main_pattern = ""
            self.findFiles()

    def setColumnFormat(self, col, fmt):
        """
        Change the format for a column, except the main one.
        """
        fmt = correctFormat(fmt)
        if self._col_format[col-1] != fmt:
            self._col_format[col-1] = fmt
            self.findColumnFiles(col)

    def columnCount(self, parent=QModelIndex()):
        return len(self._used_columns)

    def rowCount(self, parent=QModelIndex()):
        if parent.isValid():
            return 0
        return len(self._used_files)

    def index(self, row, column, parent=QModelIndex()):
        if parent.isValid():
            return QModelIndex()
        return self.createIndex(row, column, 0)

    def parent(self, idx):
        return QModelIndex()

    def headerData(self, section, orientation, role=Qt.DisplayRole):
        if orientation == Qt.Horizontal:
            if role == Qt.DisplayRole:
                return QVariant(self._column_names[self._used_columns[section]])
        return QVariant()

    def data(self, index, role=Qt.DisplayRole):
        if not index.isValid():
            return QVariant()
        item = self._found_files[self._used_files[index.row()]]
        col = self._used_columns[index.column()]
        if role == Qt.DisplayRole:
            # print("Display item col ", col, " on item ", index.row(), " = ", item[col])
            return str(item[col])
        elif role == Qt.BackgroundRole:
            if col > 0 and self._col_type[col-1] == OUTPUT_FILE:
                if (self._base_folder / item[col]).isfile():
                    return QColor(255, 128, 0)
            elif not item[col]:
                return QPalette().color(QPalette.Active, QPalette.Shadow)
        elif role == Qt.ToolTipRole:
            if col > 0 and self._col_type[col-1] == OUTPUT_FILE:
                if (self._base_folder / item[col]).isfile():
                    return item[col] + " already exists"
                else:
                    return item[col] + " doesn't exist"
            else:
                return item[col]
        return QVariant()

    def findFiles(self):
        """
        Find the main files
        """
        baseDir = self._base_folder
        main_glob, main_basename = processFilePattern(self._main_pattern)
        if self._recursive:
            files = baseDir.walkfiles(main_glob)
        else:
            files = baseDir.files(main_glob)

        nb_cols = len(self._column_names)

        items = []
        for f in files:
            f = f.relpath(baseDir)
            # print("Testing file: ", f)
            basename = main_basename(f)
            item = [""]*nb_cols
            item[0] = f
            if (baseDir / f).isfile():
                for i, (col_format, need_file) in enumerate(zip(self._col_format, self._col_type)):
                    fname = col_format.format(basename)
                    # print("   fname = ", fname)
                    if self._col_type[i] != OUTPUT_FILE and not (baseDir / fname).isfile():
                        fname = ""
                    item[i+1] = fname
                # print("     item = ", item)
                items.append(item)

        # print("Found ", len(items), " items")

        self._found_files = items
        self.updateRows(True)

    def findColumnFiles(self, col):
        """
        Update the file list for a given column.
        """
        if col == 0:
            return self.findFiles()
        nb_cols = len(self._column_names)
        if col >= nb_cols:
            raise ValueError("Error, cannot update column {0} (only {1} columns)".format(col, nb_cols))
        baseDir = self._base_folder
        main_glob, main_basename = processFilePattern(self._main_pattern)
        col_format = self._col_format[col-1]
        col_type = self._col_type[col-1]
        for item in self._found_files:
            f = item[0]
            basename = main_basename(f)
            fname = col_format.format(basename)
            if col_type != OUTPUT_FILE and not (baseDir / fname).isfile():
                fname = ""
            item[col] = fname
        if col in self._used_columns:
            idx = self._used_columns.index(col)
            self.dataChanged.emit(self.index(0, idx), self.index(len(self._used_files)-1, idx))
            self.updateRows()

    def updateRows(self, force_reset=False):
        check_cols = [i for i in self._used_columns[1:] if self._col_type[i-1] == MANDATORY_FILE]
        used_files = self._used_files
        new_used_files = [i for i in range(len(self._found_files))
                          if all(self._found_files[i][j] for j in check_cols)]
        # print("Keeping ", len(new_used_files), " files")
        if force_reset or self._used_files != new_used_files:
            self.beginResetModel()
            self._used_files = new_used_files
            self.endResetModel()

    def showColumn(self, col, on=True):
        on = bool(on)
        used_cols = list(self._used_columns)
        if on:
            for i, c in enumerate(used_cols):
                if c == col:
                    return  # nothing to do
                if c > col:
                    used_cols.insert(i, col)
                    break
            else:
                used_cols.append(col)
        else:
            try:
                idx = used_cols.index(col)
                del used_cols[idx]
            except ValueError:
                return  # nothing to do
        self.beginResetModel()
        self._used_columns = used_cols
        self.endResetModel()
        self.updateRows()
