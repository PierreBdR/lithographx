# -*- coding:utf-8 -*-
from lgxPy import Stack, parmToBool
from PyQt5.QtWidgets import QMessageBox

processType = "Stack"
processName = "Segment Section"
processFolder = "Segmentation"
processIcon = ":/images/SegmentMesh.png"
parmsDescs = dict(BlurX=u"Blur radius along the X axis in µm",
                  BlurY=u"Blur radius along the Y axis in µm",
                  nBlur=u"Number of blurs to perform",
                  WatershedLevel=u"Level of pre-fill of the watershel algorithm",
                  ApplyTransferFct=u"If true, the transfer function is applied first",
                  Invert=u"If true, the image is inverted during segmentation and restored at the end.")
parmsChoice = dict(ApplyTransferFct=["Yes", "No"],
                   Invert=["Yes", "No"])


def parseParms(BlurX, BlurY, nBlur, WatershedLevel, ApplyTransferFct, Invert):
    return (float(BlurX), float(BlurY), int(nBlur), int(WatershedLevel),
            parmToBool(ApplyTransferFct), parmToBool(Invert))


def initialize(*args):
    parent = args[-1]
    args = args[:-1]
    result = QMessageBox.warning(parent, "Irreversible process",
                                 "Warning, this process is going to change your 'Main' stack. Do you want to proceed?",
                                 QMessageBox.Yes | QMessageBox.No, QMessageBox.No)
    if result == QMessageBox.Yes:
        return args


def run(BlurX=0.3, BlurY=0.3, nBlur=1, WatershedLevel=1500, ApplyTransferFct=True, Invert=False):
    if ApplyTransferFct:
        Stack.Apply_Transfer_Function(0, 0, 0, 1)
    if Invert:
        Stack.Invert()
    Stack.Copy_Work_to_Main_Stack()
    for i in range(nBlur):
        Stack.Gaussian_Blur_Stack(BlurX, BlurY, 0)
    Stack.ITK_Watershed_Auto_Seeded(False, False, WatershedLevel)
    if Invert:
        Stack.Swap_Main_and_Work_Stacks()
        Stack.Invert()
        Stack.Swap_Main_and_Work_Stacks()
