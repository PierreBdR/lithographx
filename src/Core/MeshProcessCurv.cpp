/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "MeshProcessCurv.hpp"

#include "Algorithms.hpp"
#include "Curvature.hpp"
#include "Information.hpp"
#include "Misc.hpp"
#include "Progress.hpp"
#include "SetVector.hpp"
#include "Thrust.hpp"

#include <algorithm>
#include <vector>
#include <unordered_set>

#include <QFile>
#include <QTextStream>

using std::swap;

namespace {
using namespace lgx;
using namespace lgx::util;

struct ComputeTissueCurvatureKernel {
  ComputeTissueCurvatureKernel(const vvgraph& _S, const std::vector<vertex>& _cells, float nh2,
                               std::vector<Curvature>& cs, std::vector<set_vector<vertex> >& _vert)
    : S(_S)
    , cells(_cells)
    , neighborhood_sq(nh2)
    , curvs(cs)
    , vert(_vert)
  {
  }

  bool operator()(int ci)
  {
    const vertex& center = cells[ci];
    std::vector<Point3f> points, normals;

    // the cell center has to be the first element of points and normals
    // it will be used to calculate the local coordinate system in curvature computation
    points.push_back(center->pos);
    normals.push_back(center->nrml);

    set_vector<vertex> neighCenters;
    set_vector<vertex> neighVertices;
    set_vector<vertex> newJunctions;
    neighCenters.insert(center);

    do {
      newJunctions.clear();
      // in neighboring cells, look for all the junctions within neighborhood radius
      forall(const vertex& c, neighCenters)
        forall(const vertex& v, S.neighbors(c)) {
          if(normsq(v->pos - center->pos) > neighborhood_sq or neighVertices.count(v) != 0)
            continue;
          neighVertices.insert(v);
          newJunctions.insert(v);
        }

      // look for next neighbors cells connected to junctions
      set_vector<vertex> newCenters;
      forall(const vertex& j, newJunctions)
        forall(const vertex& v, S.neighbors(j))
          if(v->type == 'c' and neighCenters.count(v) == 0 and v->label > 0)
            newCenters.insert(v);

      swap(neighCenters, newCenters);
    } while(newJunctions.size() != 0);

    forall(vertex v, neighVertices) {
      if(v->type == 'c')
        continue;
      points.push_back(v->pos);
      normals.push_back(v->nrml);
    }

    if(points.size() > 3)
      curvs[ci].update(points, normals);

    // store all the vertices used to compute the curvature (center + neighboring junctions)
    // this is used when we want to check how the computation was made (checkLabel option)
    neighVertices.insert(center);
    vert[ci] = neighVertices;
    return true;
  }

  const vvgraph& S;
  const std::vector<vertex>& cells;
  float neighborhood_sq;
  std::vector<Curvature>& curvs;
  std::vector<set_vector<vertex> >& vert;
};
} // namespace

namespace lgx {
namespace process {

bool TissueCurvature::operator()(Mesh* mesh, float neighborhood, bool checkLabel)
{

  if(neighborhood <= 0)
    throw QString("Error, neighborhood argument must be strictly positive.");
  if(checkLabel and !mesh->isMeshVisible() and selectedLabel() > 0)
    throw QString("The mesh must be visible. Vertices used to compute curvature "
                  "for chosen label will be selected.");

  // Firts extract the centers of the cells from the mesh surface.
  // We will use these centers to compute curvature, because the centers given by
  // MakeCellMesh tend to be away from the surface for large cells in curved tissues.
  mesh->clearCellAxis();
  if(mesh->labelCenterVis().empty())
    mesh->updateCentersNormals();

  // We use a cellular mesh to compute curvature. The original mesh is unchanged,
  // unless we use the option "checkLabel" (to check which vertices are used for computation)
  vvgraph T;
  if(!mesh->cells()) {
    float sizeCellMesh = neighborhood / 10;
    MakeCellMesh mcm(*this);
    bool result;
    if(checkLabel and selectedLabel() > 0) {
      result = mcm(mesh, sizeCellMesh, mesh->graph(), true);
      T = mesh->graph();
    } else
      result = mcm(mesh, sizeCellMesh, T, true);
    if(!result)
      return false;
  } else
    T = mesh->graph();
  // The curvature computation is done on each cell center, but takes into account only cell borders
  IntIntMap labelToId;
  std::vector<vertex> cells;
  std::vector<Curvature> curvs;
  std::vector<set_vector<vertex> > vert;

  Progress progress(QString("Tissue curvature on Mesh %1").arg(mesh->userId()), 3 * cells.size());

  int cid = 0;
  forall(const vertex& c, T)
    if(c->type == 'c' and c->label > 0) {
      cells.push_back(c);
      labelToId[c->label] = cid++;
    }

  curvs.resize(cells.size());
  vert.resize(cells.size());

  float neighborhood_sq = neighborhood * neighborhood;

  {
    ComputeTissueCurvatureKernel kernel(T, cells, neighborhood_sq, curvs, vert);
    apply_kernel_it(kernel, Counter(0), Counter(cells.size()), progress);
  }

  // Select the vertices used for curvature computation in a particular cell (checkLabel)
  if(checkLabel > 0) {
    IntIntMap::const_iterator found = labelToId.find(checkLabel);
    if(found != labelToId.end()) {
      const set_vector<vertex>& vertices = vert[found->second];
      const vvgraph& S = mesh->graph();
#pragma omp parallel for
      for(size_t i = 0; i < S.size(); ++i)
        S[i]->selected = false;
#pragma omp parallel for
      for(size_t i = 0; i < vertices.size(); ++i)
        vertices[i]->selected = true;
    }
    mesh->updateSelection();
  }

  // fill in the cell axis for display of curvature directions
  IntSymTensorMap& cellTensors = mesh->cellAxis();

  for(uint i = 0; i < cells.size(); ++i) {
    if(not progress.advance(cells.size() + i))
      userCancel();
    Curvature& curv = curvs[i];
    if(curv) {
      const vertex& c = cells[i];
      Point3f e1, e2;
      float c1, c2;
      curv.get_eigenVectors(e1, e2);
      curv.get_eigenValues(c1, c2);

      // Store as a 3D tensor with the third component to 0
      cellTensors[c->label].ev1() = e1;
      cellTensors[c->label].ev2() = e2;
      cellTensors[c->label].evals() = Point3f(c1, c2, 0);
    }
  }

  mesh->setCellAxis();
  mesh->cellAxisDesc() = Description("Tissue Curvature")
    << std::make_pair("Radius", QString::number(neighborhood) + UM);
  mesh->cellAxisUnit() = UM_1;

  return true;
}

void TissueCurvature::showResult()
{
  ParmList parms;
  if(not getDefaultParms("Mesh", "Display Tissue Curvature", parms))
    return;
  runProcess("Mesh", "Display Tissue Curvature", parms);
}

REGISTER_MESH_PROCESS(TissueCurvature);

// Display curvature axis (separatly from each other) and heatmaps of gaussian curv etc.
bool DisplayTissueCurvature::operator()(Mesh* mesh, QString displayHeatMapStr, bool compareZero,
                                        float heatmapPercentile, QString displayCurv, const QColor& qColorExpansion,
                                        const QColor& qColorShrinkage, float axisLineWidth, float axisLineScale,
                                        float axisOffset, float curvThreshold)
{
  // if there is no cell axis stored, display error message
  const IntSymTensorMap& Dirs = mesh->cellAxis();
  if(Dirs.size() == 0 or mesh->cellAxisDesc().type() != "Tissue Curvature") {
    setErrorMessage(QString("No curvature cell axis stored in active mesh!"));
    return false;
  }
  bool ok;
  CurvatureType displayHeatMap = readCurvatureType(displayHeatMapStr, &ok);
  if(not ok)
    return setErrorMessage(QString("Error, unknown curvature type: '%1'").arg(displayHeatMapStr));

  if(heatmapPercentile < 1)
    heatmapPercentile = 1;
  else if(heatmapPercentile > 100)
    heatmapPercentile = 100;

  displayCurv = displayCurv.toLower();

  // Update the normals and center of the cell for visualisation.
  if(mesh->labelCenter().empty())
    mesh->updateCentersNormals();

  if(displayCurv != "none") {
    // Scale curvature for visualization
    mesh->cellAxisWidth() = axisLineWidth;
    mesh->cellAxisOffset() = axisOffset;
    Colorf colorExpansion = qColorExpansion;
    Colorf colorShrinkage = qColorShrinkage;
    mesh->prepareAxisDrawing(axisLineScale, isPositive, colorExpansion, colorShrinkage,
                             Point3b(displayCurv == "curvmax" or displayCurv == "both",
                                     displayCurv == "curvmin" or displayCurv == "both", false));
  } else {
    mesh->cellAxisScaled().clear();
    mesh->cellAxisColor().clear();
  }

  CurvatureMeasure* m = curvatureMeasure(displayHeatMap);

  if(m) {
    IntFloatMap& labelHeatMap = mesh->labelHeat();
    labelHeatMap.clear();
    std::vector<float> values;
    values.reserve(Dirs.size());

    QString unit = m->unit();

    AnisotropyCurvature anisotropy;
    IntMatrix3x3fMap& cellAxisScaled = mesh->cellAxisScaled();
    IntMatrix3x3fMap& cellAxisColor = mesh->cellAxisColor();

    forall(const IntSymTensorPair& p, Dirs) {
      const SymmetricTensor& tensor = p.second;
      int cell = p.first;

      float curvMax = tensor.evals()[0];
      float curvMin = tensor.evals()[1];
      float value = m->measure(curvMax, curvMin);
      float an = anisotropy.measure(curvMax, curvMin);

      labelHeatMap[cell] = value;
      values.push_back(value);

      if(an < curvThreshold) {
        cellAxisScaled.erase(cell);
        cellAxisColor.erase(cell);
      }
    }

    delete m;

    Information::out << "Curvature computed" << endl;

    sort(values.begin(), values.end());

    int rankmax = values.size() - 1;
    int rankmin = 0;
    if(heatmapPercentile < 100) {
      rankmax = (values.size() * heatmapPercentile) / 100;
      rankmin = values.size() - rankmax;
    }

    float maxValue = values[rankmax];
    float minValue = values[rankmin];

    if(compareZero) {
      if(displayHeatMap == CT_ANISOTROPY) {
        if(maxValue > 1)
          maxValue = 2;
        else
          maxValue = 1;
        if(minValue < 1)
          minValue = 0;
        else
          minValue = 1;
      } else {
        if(maxValue <= 0)
          maxValue = 0;
        else if(minValue >= 0)
          minValue = 0;
        else
          minValue = -maxValue;
      }
    }

    mesh->heatMapDesc() = Description("Tissue Curvature") << std::make_pair("Measure", toString(displayHeatMap));
    mesh->heatMapBounds() = Point2f(minValue, maxValue);
    mesh->heatMapUnit() = unit;
    mesh->showHeat();
    mesh->updateTriangles();
  }

  return true;
}
REGISTER_MESH_PROCESS(DisplayTissueCurvature);

bool SaveTissueCurvature::operator()(Mesh* mesh, const QString& filename)
{
  QString k1Name = "MaxCurvature (" + UM_1 + ")";
  QString k2Name = "MinCurvature (" + UM_1 + ")";
  return saveFile(mesh, filename, k1Name, k2Name);
}

REGISTER_MESH_PROCESS(SaveTissueCurvature);

bool LoadTissueCurvature::operator()(Mesh* mesh, const QString& filename)
{
  QRegularExpression k1Name("MaxCurvature \\(" + UM_1 + "\\)");
  QRegularExpression k2Name("MinCurvature \\(" + UM_1 + "\\)");
  QRegularExpression k3Name;
  auto header = readFile(mesh, "Tissue Curvature", filename, k1Name, k2Name, k3Name);
  if(not header) {
    if(not header.wrong_type)
      return false;
    QString k1, k2, k3;
    if(not readOldFile(mesh, "Tissue Curvature", filename, k1, k2, k3))
      return false;
    mesh->cellAxisDesc() = Description("Tissue Curvature") << std::make_pair("Radius", "-1.");
  }
  mesh->cellAxisUnit() = UM_1;
  return true;
}

REGISTER_MESH_PROCESS(LoadTissueCurvature);
} // namespace process
} // namespace lgx
