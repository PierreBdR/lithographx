/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef PACKAGEMANAGER_HPP
#define PACKAGEMANAGER_HPP

#include <LGXConfig.hpp>

#include <Utility.hpp>
#include <Version.hpp>
#include <Dependency.hpp>

#include <QString>
#include <QStringList>
#include <memory>
#include <unordered_map>

namespace lgx {

class Package
{
public:
  Package();
  Package(const QString& name, const util::Version& version, bool user=false);
  Package(const Package& copy);
  Package(Package&&) = default;

  Package& operator=(const Package&);
  Package& operator=(Package&&) = default;

  static Package fromDescription(const QString& filename, bool user=false);

  bool writeDescription(const QString& filename) const;

  bool valid() const;
  explicit operator bool() const
  { return valid(); }

  bool operator==(const Package& other) const {
    return _name == other._name and _version == other._version;
  }

  bool operator!=(const Package& other) const {
    return _name != other._name or _version != other._version;
  }

  const util::Version& version() const {
    return _version;
  }

  QString name() const {
    return _name;
  }

  bool canInstall(const PackageManager& manager, bool testSelf = true) const;

  QStringList files() const;
  QStringList libraries() const;

  bool isUserPackage() const { return _user; }
  void setUserPackage(bool user) { _user = user; }

  void setDependencies(QString dep);
  void setDependencies(std::unique_ptr<Dependency>&& dep);
  void setDependencies(std::shared_ptr<Dependency> dep);

  const Dependency* dependencies() const;

  void setDescription(QString txt);
  const QString& description() const;
  const QString& OS() const { return _OS; }
  void setOS(QString os);

  void setSourcePackage(const QString& s);
  const QString& sourcePackage() const { return _source; }

private:
  QString _name, _description, _OS, _source;
  lgx::util::Version _version;
  std::unique_ptr<Dependency> _depend;
  bool _user; ///< User package? If not: system
  mutable QStringList _files;
};


class PackageManagerPrivate;

class PackageManager
{
public:
  PackageManager();
  PackageManager(const PackageManager& other) = default;
  PackageManager(PackageManager&& other) = default;
  virtual ~PackageManager();

  virtual void loadPackages();

  virtual bool hasPackage(QString name) const;
  virtual bool hasUserPackage(QString name) const;
  virtual bool hasSystemPackage(QString name) const;
  virtual const Package* package(QString name) const;

  virtual QStringList packages() const;
  virtual QStringList userPackages() const;
  virtual QStringList systemPackages() const;

  /**
   * Check if an installed package is still valid
   */
  virtual bool check(const QString& name) const;

  /// Returns the list of packages depending on this package (e.g. that wouldn't work without it)
  virtual QStringList reverseDependencies(const QString& name) const;
  /// Returns the list of packages that wouldn't work with the new version
  virtual QStringList testVersionChange(const QString& name, const util::Version& new_version) const;

  /**
   * Returns whether the package can be installed and, if not, if this is because other packages would break.
   */
  virtual std::pair<bool, QStringList> canInstall(const Package& pkg) const;

  /// Returns the list of packages that would break if the package is un-installed
  virtual std::pair<bool, QStringList> canUninstall(const QString& pkgName) const;

  PackageManager& operator=(const PackageManager&) = default;
  PackageManager& operator=(PackageManager&&) = default;

private:
  virtual void loadPackages(bool user);
  std::shared_ptr<PackageManagerPrivate> _p;
};

} // namespace lgx

#endif // PACKAGEMANAGER_HPP
