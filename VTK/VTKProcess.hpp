#ifndef VTKPROCESS_HPP
#define VTKPROCESS_HPP

#include <VTKConfig.hpp>
#include <Process.hpp>
#include <vtkImageImport.h>

class vtkPolyData;

namespace lgx {
namespace process {

class vtkutil_EXPORT VTKImageConverter : public vtkImageImport {
public:
  static VTKImageConverter* New();

  typedef vtkImageImport SuperClass;

  void SetStore(const Store* store);
  const Store* GetStore() const {
    return store;
  }

protected:
  VTKImageConverter();
  ~VTKImageConverter();

  const Store* store;
};

vtkutil_EXPORT bool triangulatedPolyDataToMesh(vtkPolyData* data, Mesh* mesh, bool replace = true);
}
}

#endif // VTKPROCESS_HPP
