/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/


#include "StackProcessMeshInteractions.hpp"

#include "Information.hpp"
#include "Progress.hpp"
#include "cuda/CudaExport.hpp"
#include <Mesh.hpp>
#include "Clip.hpp"

#include <algorithm>

namespace lgx {
namespace process {

namespace {
// Get triangle list for cuda operations
void getPointList(const Mesh* mesh, HVec3F& pts, HVec3F& nrmls)
{
  // Get triangle list
  uint idx = 0;
  const std::vector<vertex>& av = mesh->activeVertices();
  pts.resize(av.size());
  nrmls.resize(av.size());
  forall(const vertex& v, av) {
    pts[idx] = v->pos;
    nrmls[idx] = v->nrml;
    idx++;
  }
}

void getTriangleList(const Mesh* mesh, HVec3F& pts, HVec3U& tris, HVec3F& nrmls, int label)
{
  const vvgraph& S = mesh->graph();
  // Get triangle list
  uint tricount = 0;
  const std::vector<vertex>& av = mesh->activeVertices();
  bool has_selection = av.size() != S.size();
  std::set<vertex> vSet;
  forall(const vertex& v, av) {
    forall(const vertex& n, S.neighbors(v)) {
      vertex m = S.nextTo(v, n);
      if(has_selection and (!n->selected or !m->selected))
        continue;
      if(label >= 0 and label != mesh->getLabel(v, n, m))
        continue;
      if(mesh->uniqueTri(v, n, m))
        tricount++;

      // Add vertices to set
      vSet.insert(v);
      vSet.insert(n);
      vSet.insert(m);
    }
  }

  // Put points in vector
  uint idx = 0;
  pts.resize(vSet.size());
  forall(const vertex& v, vSet) {
    pts[idx] = v->pos;
    v->saveId = idx++;
  }

  idx = 0;
  tris.resize(tricount);
  nrmls.resize(tricount);
  forall(const vertex& v, av)
    forall(const vertex& n, S.neighbors(v)) {
      vertex m = S.nextTo(v, n);
      if(has_selection and (!n->selected or !m->selected))
        continue;
      if(label >= 0 and label != mesh->getLabel(v, n, m))
        continue;
      if(!mesh->uniqueTri(v, n, m))
        continue;
      tris[idx] = Point3u(v->saveId, n->saveId, m->saveId);
      nrmls[idx++] = ((n->pos - v->pos) ^ (m->pos - v->pos)).normalize();
    }
}


} // namespace

bool Annihilate::operator()(const Store* input, Store* output, const Mesh* mesh, float minDist, float maxDist,
                            bool fill, uint fillval)
{
  HVecUS tdata(input->data());

  const Stack* stack = output->stack();

  // Data for cuda
  HVec3F pts;     // Points for triangles
  HVec3F nrmls;   // Normals for triangles

  // Get point list, nrmls, and bounding box
  getPointList(mesh, pts, nrmls);
  BoundingBox3f bBoxW;   // Bounding box in world coordinates
  forall(const Point3f& p, pts)
    bBoxW |= p;

  float distance = std::max(0.0f, std::max(-minDist, -maxDist));   // negative distance is outside mesh, expand bBox
  if(distance > 0) {
    Point3f distance3f(distance, distance, distance);
    bBoxW[0] -= distance3f;
    bBoxW[1] += distance3f;
  }
  BoundingBox3i bBoxI(stack->worldToImagei(bBoxW[0]), stack->worldToImagei(bBoxW[1]));
  Point3i imgSize = stack->size();
  bBoxI &= BoundingBox3i(Point3i(0, 0, 0), imgSize);
  Point3i size = bBoxI.size();

  // Buffer for mask
  HVecUS mask(size_t(size.x()) * size.y() * size.z(), 0);

  Progress progress(QString("Annihilating Stack %1").arg(stack->userId()), 100);

  // call cuda to get stack mask
  if(nearMeshGPU(bBoxI[0], size, stack->step(), stack->origin(), pts, nrmls, minDist, maxDist, mask))
    return false;
  if(!progress.advance(0))
    userCancel();

  int xIdx = 0;
  for(int z = 0; z < imgSize.z(); ++z)
    for(int y = 0; y < imgSize.y(); ++y)
      for(int x = 0; x < imgSize.x(); ++x, ++xIdx) {
        // Inside bBox
        if(x >= bBoxI[0].x() and x < bBoxI[1].x() and y >= bBoxI[0].y() and y < bBoxI[1].y()
           and z >= bBoxI[0].z() and z < bBoxI[1].z()) {
          size_t mIdx = ((size_t(z) - bBoxI[0].z()) * size.y() + y - bBoxI[0].y()) * size.x() + x
            - bBoxI[0].x();
          if(mask[mIdx]) {
            if(fill)
              output->data()[xIdx] = fillval;
            else
              output->data()[xIdx] = input->data()[xIdx];
            continue;
          }
        }
        if(fill and fillval == 0)
          output->data()[xIdx] = input->data()[xIdx];
        else
          output->data()[xIdx] = 0;
      }
  output->copyMetaData(input);
  output->changed();
  return true;
}

REGISTER_STACK_PROCESS(Annihilate);

bool ClipStack::operator()(const Store* input, Store* output)
{
  // Setup matrices and planes for clip test
  const Stack* stack = output->stack();

  // Indicator if in use
  Point3u clipDo;
  clipDo[0] = (clip1()->enabled() ? 1 : 0);
  clipDo[1] = (clip2()->enabled() ? 1 : 0);
  clipDo[2] = (clip3()->enabled() ? 1 : 0);

  // Frame matrix
  Matrix4d frm(stack->getFrame().worldMatrix());

  // Clip plane matrices
  Matrix4d clm[3];
  clm[0] = Matrix4d(clip1()->frame().inverse().worldMatrix());
  clm[1] = Matrix4d(clip2()->frame().inverse().worldMatrix());
  clm[2] = Matrix4d(clip3()->frame().inverse().worldMatrix());

  // Clipping planes, point normal form
  Point4f cn[6];
  cn[0] = Point4f(clip1()->normal());
  cn[1] = -cn[0];
  cn[0].t() = cn[1].t() = clip1()->width();
  cn[2] = Point4f(clip2()->normal());
  cn[3] = -cn[2];
  cn[2].t() = cn[3].t() = clip2()->width();
  cn[4] = Point4f(clip3()->normal());
  cn[5] = -cn[4];
  cn[4].t() = cn[5].t() = clip3()->width();

  // ... expressed in image coordinate
  Point4f pn[6];
  for(size_t i = 0 ; i < 6 ; ++i) {
    size_t j = i / 2;
    pn[i] = Point4f(frm * clm[j] * cn[i]);
    Information::out << "pn[" << i << "] = " << pn[i] << endl;
  }

  // Put in host vector
  HVec4F Hpn;
  Hpn.resize(6);
  for(int i = 0; i < 6; i++)
    Hpn[i] = pn[i];

  // Call cuda to clip
  clipStackGPU(stack->size(), stack->step(), stack->origin(), clipDo, Hpn, input->data(), output->data());

  SETSTATUS("Stack " << stack->userId() << " clipped to intersection of clipping planes");
  output->copyMetaData(input);
  output->changed();
  return true;
}

REGISTER_STACK_PROCESS(ClipStack);

bool StackMeshProcess::operator()(const Store* input, Store* output, const Mesh* mesh, bool fill, uint fillval)
{
  const Stack* stack = input->stack();

  if(fill)
    SETSTATUS("Filling stack " << stack->userId() << " from mesh");
  else
    SETSTATUS("Trimming stack " << stack->userId() << " from mesh");

  // Data for cuda
  HVec3F pts;     // Points for triangles
  HVec3U tris;    // Triangle vertices
  HVec3F nrmls;   // Normals for triangles

  // Get triangle list and bounding box
  getTriangleList(mesh, pts, tris, nrmls, -1);
  BoundingBox3f bBoxW;
  forall(const Point3f& p, pts)
    bBoxW |= p;

  BoundingBox3i bBoxI(stack->worldToImagei(bBoxW[0]), stack->worldToImagei(bBoxW[1]) + Point3i(1, 1, 1));
  Point3i imgSize = stack->size();
  bBoxI &= BoundingBox3i(Point3i(0, 0, 0), imgSize);
  Point3i size = bBoxI.size();

  // Buffer for mask
  HVecUS mask(size_t(size.x()) * size.y() * size.z(), 0);

  // call cuda to get stack mask
  Progress progress(QString("Trimming Stack %1").arg(stack->userId()), 100);
  if(insideMeshGPU(bBoxI[0], size, stack->step(), stack->origin(), pts, tris, nrmls, mask))
    return false;
  if(!progress.advance(0))
    userCancel();

  int xIdx = 0;
  for(int z = 0; z < imgSize.z(); ++z)
    for(int y = 0; y < imgSize.y(); ++y)
      for(int x = 0; x < imgSize.x(); ++x, ++xIdx) {
        // Inside bBox
        if(x >= bBoxI[0].x() and x < bBoxI[1].x() and y >= bBoxI[0].y() and y < bBoxI[1].y()
           and z >= bBoxI[0].z() and z < bBoxI[1].z()) {
          size_t mIdx = ((size_t(z) - bBoxI[0].z()) * size.y() + y - bBoxI[0].y()) * size.x() + x
            - bBoxI[0].x();
          if(mask[mIdx]) {
            if(fill)
              output->data()[xIdx] = fillval;
            else
              output->data()[xIdx] = input->data()[xIdx];
            continue;
          }
        }
        if(fill and fillval == 0)
          output->data()[xIdx] = input->data()[xIdx];
        else
          output->data()[xIdx] = 0;
      }

  output->copyMetaData(input);
  output->changed();
  return true;
}

REGISTER_STACK_PROCESS(FillStackToMesh);
REGISTER_STACK_PROCESS(TrimStackProcess);

bool FillStack3D::operator()(const Store* input, Store* output, const Mesh* mesh)
{
  const Stack* stack = input->stack();

  SETSTATUS("Filling stack " << stack->userId() << " from 3D mesh");

  // Data for cuda
  HVec3F pts;     // Points for triangles
  HVec3U tris;    // Triangle vertices
  HVec3F nrmls;   // Normals for triangles
                  //    Point3f bBoxW[2];  // Bounding box in world coordinates

  // Get list of labels
  std::set<int> Labels;
  forall(const vertex& v, mesh->graph())
    if(v->label > 0)
      Labels.insert(v->label);

  // Loop over all labels
  Progress progress(QString("Filling Stack %1 from 3D mesh").arg(stack->userId()), 0);
  forall(int label, Labels) {
    // Get triangle list and bounding box
    getTriangleList(mesh, pts, tris, nrmls, label);
    BoundingBox3f bBoxW;
    forall(const Point3f& p, pts)
      bBoxW |= p;
    BoundingBox3i bBoxI(stack->worldToImagei(bBoxW[0]), stack->worldToImagei(bBoxW[1]) + Point3i(1, 1, 1));
    Point3i imgSize = stack->size();
    bBoxI &= BoundingBox3i(Point3i(0, 0, 0), imgSize);
    Point3i size = bBoxI.size();
    // Buffer for mask
    HVecUS mask(size_t(size.x()) * size.y() * size.z(), 0);

    // call cuda to get stack mask
    if(insideMeshGPU(bBoxI[0], size, stack->step(), stack->origin(), pts, tris, nrmls, mask))
      return false;
    if(!progress.advance(0))
      userCancel();
    for(int z = bBoxI[0].z(); z < bBoxI[1].z(); ++z)
      for(int y = bBoxI[0].y(); y < bBoxI[1].y(); ++y) {
        size_t xIdx = stack->offset(bBoxI[0].x(), y, z);
        for(int x = bBoxI[0].x(); x < bBoxI[1].x(); ++x, ++xIdx) {
          size_t mIdx = ((size_t(z) - bBoxI[0].z()) * size.y() + y - bBoxI[0].y()) * size.x() + x
            - bBoxI[0].x();
          if(mask[mIdx])
            output->data()[xIdx] = label;
        }
      }
  }
  output->copyMetaData(input);
  output->changed();
  return true;
}
REGISTER_STACK_PROCESS(FillStack3D);

bool StackRelabelFromMesh::operator()(Stack* stack, const Store* store, Store* output, const Mesh* mesh,
                                      bool delete_unknown)
{
  VVGraphVec regions;
  VIntMap cells;
  int n = mesh->getConnectedRegions(mesh->graph(), regions, cells);
  SETSTATUS("Number of cells = " << n);
  std::vector<Point3f> centers(regions.size());
  std::vector<ushort> labels(regions.size());
  std::unordered_map<ushort, ushort> label_mapping;
  Point3u stkSize = stack->size();
  const HVecUS& in_data = store->data();
  HVecUS& data = output->data();
  ushort max_label = 0;

  for(size_t i = 0; i < regions.size(); ++i) {
    const vvgraph& S = regions[i];
    short lab = ushort(S.any()->label & 0xFFFF);
    if(lab > max_label)
      max_label = lab;
    labels[i] = lab;
    Point3f& c = centers[i];
    forall(const vertex& v, S)
      c += v->pos;
    c /= S.size();
    Point3i imgPos = stack->worldToImagei(c);
    if(imgPos.x() >= 0 and imgPos.y() >= 0 and imgPos.z() >= 0 and (size_t) imgPos.x() < stkSize.x()
       and (size_t) imgPos.y() < stkSize.y() and (size_t) imgPos.z() < stkSize.z()) {
      ushort l = in_data[stack->offset(imgPos)];
      label_mapping[l] = lab;
      SETSTATUS("Mapping " << l << " to " << lab);
    } else
      SETSTATUS("No mapping for cell labeled " << lab);
  }

  for(size_t i = 0; i < data.size(); ++i) {
    std::unordered_map<ushort, ushort>::const_iterator found = label_mapping.find(in_data[i]);
    if(found != label_mapping.end())
      data[i] = found->second;
    else if(delete_unknown)
      data[i] = 0;
    else {
      label_mapping[in_data[i]] = ++max_label;
      data[i] = max_label;
    }
  }

  output->copyMetaData(store);
  output->changed();

  return true;
}
REGISTER_STACK_PROCESS(StackRelabelFromMesh);

} // namespace process
} // namespace lgx
