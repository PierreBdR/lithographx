/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef PLUGINMANAGER_HPP
#define PLUGINMANAGER_HPP

#include <LGXConfig.hpp>
#include <Process.hpp>
#include <Utility.hpp>

#include <QDialog>
#include <QString>
#include <QList>

#include <memory>
#include <vector>
#include <unordered_map>

namespace lgx {
namespace process {

struct LGX_EXPORT NoSuchPlugin : public std::exception
{
  NoSuchPlugin(const QString& name)
    : std::exception()
  {
    message = QString("No plugin called %1.").arg(name).toUtf8();
  }

  ~NoSuchPlugin() throw() override { }

  const char* what() const throw() override {
    return message.data();
  }

  QByteArray message;
};

struct LGX_EXPORT NoSuchProcessInPlugin : public NoSuchProcess
{
  NoSuchProcessInPlugin(const QString& pluginName, const QString& processType, const QString& registrationName)
    : NoSuchProcess(processType, registrationName)
  {
    message = QString("No %1 process registered by class %2 in plugin %3.")
      .arg(registrationName).arg(processType).arg(pluginName).toUtf8();
  }

  ~NoSuchProcessInPlugin() throw() override { }
};

struct LGX_EXPORT NoSuchMacroLanguage : public std::exception
{
  NoSuchMacroLanguage(const QString& pluginName, const QString& languageName) {
    QString msg;
    msg = QString("No macro language named '%1'").arg(languageName);
    if(not pluginName.isEmpty())
      msg += QString(" in plugin '%1'").arg(pluginName);
    msg += ".";
    message = msg.toUtf8();
  }

  ~NoSuchMacroLanguage() throw() override { }

  const char* what() const throw() override {
    return message.data();
  }

  QByteArray message;
};

struct PluginManagerPrivate;
struct PluginPrivate;

/**
 * \class Plugin
 *
 * Opaque structure holding a plugin.
 */
class LGX_EXPORT Plugin : public std::enable_shared_from_this<Plugin>
{
  friend class PluginManager;

  std::unique_ptr<PluginPrivate> p;
  Plugin(const Plugin&) = delete;
  Plugin(Plugin&&) = delete;

public:
  Plugin(QString type, QString name);
  ~Plugin();

  //@{
  ///\name Create/update processes and macro database

  /**
   * Register a new process.
   *
   * \returns The name of the process on success, an empty string on failure.
   */
  template <typename P>
  bool registerProcess(QString registrationName, FactoryPointer<P> factory);

  /**
   * Un-register a process.
   *
   * \returns The name of the removed process or an empty string in case of failure
   */
  template <typename P>
  QString unregisterProcess(QString registrationName, FactoryPointer<P> factory);

  /**
   * Register a new macro language.
   *
   * this method fails if a language with the same name already exists.
   *
   * \returns true on success
   */
  bool registerMacroLanguage(QString name,
                             macro_loader_t loader,
                             macro_unloader_t unloader,
                             macro_finalizer_t finalizer);

  /**
   * Un-register a macro language.
   *
   * \returns true on success.
   */
  bool unregisterMacroLanguage(QString name);

  /**
   * Finalize a macro language.
   *
   * This is meant to called only once before un-loading the extension.
   *
   * \returns true on success.
   */
  bool finalizeMacroLanguage(QString name);

  /**
   * Create a macro language, without specifying anything
   *
   * \note this has no effect if the macro language has already be been registered
   */
  bool createMacroLanguage(QString name);

  /**
   * Mark the plugin as correctly loaded
   */
  void validate();

  /**
   * Indicate an error during loading
   */
  void error(QString message);

  /**
   * Load a given macro language
   */
  bool loadMacro(QString name);

  /**
   * Unload a given macro language
   */
  bool unloadMacro(QString name);
  //@}

  //@{
  ///\name Plugin properties

  /// Get plugin name
  QString name() const;

  /// Get plugin type
  QString type() const;

  /// Get the 'full' name of the plugin
  QString fullName() const;

  /**
   * Check if this is a system plugin or not
   *
   * \note System plugins cannot be disabled or re-ordered.
   */
  bool isSystem() const;

  /**
   * Return true if the plugin has already been loaded.
   *
   * A plugin is considered loaded if it has macro languages and/or processes registered.
   */
  bool loaded() const;

  /**
   * Check if this plugin has been correctly loaded
   */
  bool valid() const;

  /**
   * Return the error string in case the plugin is not valid
   */
  QString errorMessage() const;

  /**
   * Check if the plugin is currently in use
   */
  bool used() const;

  /**
   * Decide whether or not to use the plugin
   */
  void use(bool on = true);

  /**
   * Get the process definition
   *
   * \param registrationName Name of the registration class of the process
   *
   * \note the argument is not the same as the one for the PluginManager!
   */
  template <typename P>
  const ProcessDefinition<P>& processDefinition(const QString& registrationName) const;

  /**
   * Get the process definition
   *
   * \param registrationName Name of the registration class of the process
   *
   * \note the argument is not the same as the one for the PluginManager!
   */
  template <typename P>
  ProcessDefinition<P>& processDefinition(const QString& registrationName);

  /**
   * List all processes of type P
   *
   * The processes are identified by their registration class name
   */
  template <typename P>
  QStringList processes() const;

  /**
   * Number of processes of a given type
   */
  template <typename P>
  size_t nbProcesses() const;

  /**
   * Total number of processes
   */
  size_t nbProcesses() const;

  /**
   * List of macro languages defined in this plugin
   */
  QStringList macroLanguages() const;

  /**
   * List of macro languages defined in this plugin
   */
  size_t nbMacroLanguages() const;
  //@}
};

class LGX_EXPORT PluginManager : public QObject {
  Q_OBJECT

  friend class ::LithoGraphX;

  std::unique_ptr<PluginManagerPrivate> p;

  PluginManager();
  PluginManager(const PluginManager&) = delete;
  PluginManager(PluginManager&&) = delete;
  ~PluginManager();

  static bool allocate();
  static bool deallocate();
public:
  static PluginManager* instance();

  //@{
  ///\name Create/update plugin database

  /**
   * Loading new C++ extensions.
   *
   * Check if there are new DLLs that can be loaded
   */
  bool loadNewExtensions();

  /**
   * Try to un-load an extension
   */
  bool unloadExtension(const QString& dllPath);

private:

  /**
   * Load C++ extensions.
   *
   * This can be only be done once. Subsequent attempts will always fail.
   *
   * \returns true on success
   */
  bool loadExtensions();

  /**
   * Un-load all extensions.
   */
  bool unloadExtensions();

public:

  /**
   * Remove any plugin that are not currently loaded
   */
  void cleanPluginList();

  /**
   * Refresh everything
   *
   * \note This also stores the current state to the QSettings
   */
  bool refreshAll();

  /**
   * Load macros (e.g. processes written in a scripting language)
   *
   * Unlike plugins, macros can be re-loaded at will.
   *
   * \returns true on success
   */
  bool loadMacros();

  /**
   * Unload macros (e.g. processes written in a scripting language)
   *
   * Unlike plugins, macros can be re-loaded at will.
   *
   * \returns true on success
   */
  bool unloadMacros();

  /**
   * Start the loading of a specific plugin
   *
   * \returns True if the current plugin was successfully set.
   *
   * \note If a plugin was already loading, this method has no effect.
   */
  bool loadingPlugin(QString pluginType, QString pluginPath);

  /**
   * Signal that a plugin has been fully loaded.
   *
   * \returns True if that was the current plugin
   */
  bool pluginLoaded(QString pluginPath);

  /**
   * Register a new process.
   *
   * \returns the name of the plugin it is registered to
   */
  template <typename P>
  QString registerProcess(QString registrationName, FactoryPointer<P> factory);

  /**
   * Un-register a process.
   *
   * \param plugin Full name of the plugin
   * \param registrationName Name of the class as defined by the registration class
   * \param factory Factory function used at registration
   *
   * \returns The name of the removed process or an empty string in case of failure
   */
  template <typename P>
  QString unregisterProcess(QString plugin, QString registrationName, FactoryPointer<P> factory);

  /**
   * Register a new macro language.
   *
   * this method fails if a language with the same name already exists.
   *
   * \returns the name of the plugin on success and an empty string on failure.
   */
  QString registerMacroLanguage(QString name,
                                macro_loader_t loader,
                                macro_unloader_t unloader,
                                macro_finalizer_t finalizer);

  /**
   * Un-register a macro language.
   *
   * \returns true on success.
   */
  bool unregisterMacroLanguage(QString plugin, QString name);

  /**
   * Force a refresh of the process list
   *
   * This will take into account the visibility of the plugins, and their loading order.
   *
   * \returns true on success
   */
  bool refreshProcesses();

private:
  /**
   * Load extensions and macros and then compute the process list
   */
  bool initialize();


  /**
   * Un-load all extensions and clear the data structures
   */
  bool finalize();

public:

  /// Load plugin settings
  void loadSettings();

  /// Save plugin settings
  void saveSettings();

  //@}

  //@{
  ///\name Query methods

  /**
   * List all available plugins
   */
  QStringList pluginsName() const;

  /**
   * Get a plugin
   */
  std::shared_ptr<Plugin> plugin(QString fullName);

  /**
   * Get a plugin
   */
  std::shared_ptr<const Plugin> plugin(QString fullName) const;


  /**
   * Returns the type of plugin.
   *
   * This can be "Library" for C++ written extension or the name of the macro loader.
   */
  QString pluginType(QString pluginPath) const;

  /**
   * List all usable processes of type P
   */
  template <typename P>
  QStringList processes() const;

  /**
   * Number of processes of type P
   */
  template <typename P>
  size_t nbProcesses() const;

  /**
   * Total number of processes registered
   */
  template <typename P>
  size_t nbRegisteredProcesses() const;

  /**
   * Check if a process exists
   */
  bool hasProcess(const QString& processType, const QString& processName) const;

  /**
   * Check if a process exists
   */
  template <typename P>
  bool hasProcess(const QString& processName) const;

  /**
   * Return the process definition, if it exists or nullptr
   */
  BaseProcessDefinition& baseProcessDefinition(const QString& processType, const QString& processName);

  /**
   * Get the process definition
   */
  template <typename P>
  const ProcessDefinition<P>& processDefinition(const QString& processName) const;

  /**
   * Get the process definition
   */
  template <typename P>
  ProcessDefinition<P>& processDefinition(const QString& processName);

  /**
   * Create a process
   */
  std::unique_ptr<Process> makeProcess(const QString& processType, const QString& processName, const Process* proc) const;

  /**
   * Move a plugin in the loading order
   *
   * \param fullName Full name of the plugin whose order is defined
   * \param dst Order at which the plugin will be loaded
   */
  bool movePluginLoadingOrder(QString fullName, int dst);

  /**
   * Loading order for libraries
   */
  QStringList pluginsLoadingOrder() const;

  /**
   * Change loading order of libraries
   *
   * \note Any library not specified will be added, in alphabetical order, at the end
   */
  void setPluginsLoadingOrder(QStringList order);
  //@}

signals:
  void processesChanged();
};

inline PluginManager* pluginManager() {
  return PluginManager::instance();
}

class LGX_EXPORT LoadingPlugin {
  QString _fullName;
  bool _success;
  std::shared_ptr<Plugin> _plugin;

public:
  LoadingPlugin(QString pluginType, QString pluginName);
  ~LoadingPlugin();

  void fail(QString message);
  void validate();

  QString fullName() const;

  operator bool() const { return _success; }
};

///\cond NO_DOC
// Explicit instanciations of method templates
extern template LGX_EXPORT
ProcessDefinition<StackProcess>& Plugin::processDefinition<StackProcess>(const QString&);
extern template LGX_EXPORT
ProcessDefinition<MeshProcess>& Plugin::processDefinition<MeshProcess>(const QString&);
extern template LGX_EXPORT
ProcessDefinition<GlobalProcess>& Plugin::processDefinition<GlobalProcess>(const QString&);

extern template LGX_EXPORT
const ProcessDefinition<StackProcess>& Plugin::processDefinition<StackProcess>(const QString&) const;
extern template LGX_EXPORT
const ProcessDefinition<MeshProcess>& Plugin::processDefinition<MeshProcess>(const QString&) const;
extern template LGX_EXPORT
const ProcessDefinition<GlobalProcess>& Plugin::processDefinition<GlobalProcess>(const QString&) const;

extern template LGX_EXPORT
ProcessDefinition<StackProcess>& PluginManager::processDefinition<StackProcess>(const QString&);
extern template LGX_EXPORT
ProcessDefinition<MeshProcess>& PluginManager::processDefinition<MeshProcess>(const QString&);
extern template LGX_EXPORT
ProcessDefinition<GlobalProcess>& PluginManager::processDefinition<GlobalProcess>(const QString&);

extern template LGX_EXPORT
const ProcessDefinition<StackProcess>& PluginManager::processDefinition<StackProcess>(const QString&) const;
extern template LGX_EXPORT
const ProcessDefinition<MeshProcess>& PluginManager::processDefinition<MeshProcess>(const QString&) const;
extern template LGX_EXPORT
const ProcessDefinition<GlobalProcess>& PluginManager::processDefinition<GlobalProcess>(const QString&) const;

extern template LGX_EXPORT
QString PluginManager::registerProcess<StackProcess>(QString, FactoryPointer<StackProcess>);
extern template LGX_EXPORT
QString PluginManager::registerProcess<MeshProcess>(QString, FactoryPointer<MeshProcess>);
extern template LGX_EXPORT
QString PluginManager::registerProcess<GlobalProcess>(QString, FactoryPointer<GlobalProcess>);

extern template LGX_EXPORT
bool Plugin::registerProcess<StackProcess>(QString, FactoryPointer<StackProcess>);
extern template LGX_EXPORT
bool Plugin::registerProcess<MeshProcess>(QString, FactoryPointer<MeshProcess>);
extern template LGX_EXPORT
bool Plugin::registerProcess<GlobalProcess>(QString, FactoryPointer<GlobalProcess>);

extern template LGX_EXPORT
QString PluginManager::unregisterProcess<StackProcess>(QString, QString, FactoryPointer<StackProcess>);
extern template LGX_EXPORT
QString PluginManager::unregisterProcess<MeshProcess>(QString, QString, FactoryPointer<MeshProcess>);
extern template LGX_EXPORT
QString PluginManager::unregisterProcess<GlobalProcess>(QString, QString, FactoryPointer<GlobalProcess>);

extern template LGX_EXPORT
QString Plugin::unregisterProcess<StackProcess>(QString, FactoryPointer<StackProcess>);
extern template LGX_EXPORT
QString Plugin::unregisterProcess<MeshProcess>(QString, FactoryPointer<MeshProcess>);
extern template LGX_EXPORT
QString Plugin::unregisterProcess<GlobalProcess>(QString, FactoryPointer<GlobalProcess>);

extern template LGX_EXPORT
QStringList PluginManager::processes<StackProcess>() const;
extern template LGX_EXPORT
QStringList PluginManager::processes<MeshProcess>() const;
extern template LGX_EXPORT
QStringList PluginManager::processes<GlobalProcess>() const;

extern template LGX_EXPORT
QStringList Plugin::processes<StackProcess>() const;
extern template LGX_EXPORT
QStringList Plugin::processes<MeshProcess>() const;
extern template LGX_EXPORT
QStringList Plugin::processes<GlobalProcess>() const;

extern template LGX_EXPORT
size_t PluginManager::nbProcesses<StackProcess>() const;
extern template LGX_EXPORT
size_t PluginManager::nbProcesses<MeshProcess>() const;
extern template LGX_EXPORT
size_t PluginManager::nbProcesses<GlobalProcess>() const;

extern template LGX_EXPORT
size_t PluginManager::nbRegisteredProcesses<StackProcess>() const;
extern template LGX_EXPORT
size_t PluginManager::nbRegisteredProcesses<MeshProcess>() const;
extern template LGX_EXPORT
size_t PluginManager::nbRegisteredProcesses<GlobalProcess>() const;

extern template LGX_EXPORT
bool PluginManager::hasProcess<StackProcess>(const QString&) const;
extern template LGX_EXPORT
bool PluginManager::hasProcess<MeshProcess>(const QString&) const;
extern template LGX_EXPORT
bool PluginManager::hasProcess<GlobalProcess>(const QString&) const;

extern template LGX_EXPORT
size_t Plugin::nbProcesses<StackProcess>() const;
extern template LGX_EXPORT
size_t Plugin::nbProcesses<MeshProcess>() const;
extern template LGX_EXPORT
size_t Plugin::nbProcesses<GlobalProcess>() const;

///\endcond

} // namespace process
} // namespace lgx

#endif // PLUGINMANAGER_HPP
