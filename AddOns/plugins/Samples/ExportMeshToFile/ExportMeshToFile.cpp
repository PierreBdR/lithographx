/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "ExportMeshToFile.hpp"

#include <Progress.hpp>
#include <Mesh.hpp>

#include <QFileDialog>

namespace lgx {
namespace process {
// Initialize, pop-up dialog to get filename (runs in GUI thread)
bool ExportMeshToFile::initialize(ParmList& parms, QWidget* parent)
{
  QString fileName = parms[0].toString();
  QString fileType = parms[1].toString();

  QString objFilter("Wavefront OBJ files (*.obj)");
  QString plyFilter("Stanford Polygon Files (*.ply)");

  QString filterList = QString("%1;;%2").arg(objFilter).arg(plyFilter);

  QString filter;

  if(fileName.endsWith(".obj", Qt::CaseInsensitive))
    filter = objFilter;
  else if(fileName.endsWith(".ply", Qt::CaseInsensitive))
    filter = plyFilter;

  // Get the file name
  fileName = QFileDialog::getSaveFileName(parent, QString("Select export file"), fileName, filterList, &filter, 0);

  // Return error if no file name
  if(fileName.isEmpty())
    return false;

  // Add extension if not there
  if(filter == objFilter) {
    if(!fileName.endsWith(".obj", Qt::CaseInsensitive))
      fileName += ".obj";
    parms[1] = "obj";
  } else if(filter == plyFilter) {
    if(!fileName.endsWith(".ply", Qt::CaseInsensitive))
      fileName += ".ply";
    parms[1] = "ply";
  }

  // Save the fileName to the parameters
  parms[0] = fileName;

  return true;
}

// Do the work (runs in process thread)
bool ExportMeshToFile::operator()(Mesh* mesh, const QString& fileName, const QString& fileType)
{
  QFile file(fileName);

  // Open file fopr writing
  if(!file.open(QIODevice::WriteOnly))
    throw(QString("Error:Cannot open output file: %1").arg(fileName));

  // Get the graph for the mesh
  const vvgraph& S = mesh->graph();

  // Create output stream
  QTextStream out(&file);

  // Start the progress bar
  Progress progress(QString("Saving Text Mesh %1 in File '%2'").arg(mesh->userId()).arg(fileName), S.size() * 2);

  // Used to number vertices
  int saveId = 0, i = 0;
  // Save OBJ File
  if(fileType == "obj") {
    // First write the vertices and normals, mark the id of each
    forall(const vertex& v, S) {
      if(!progress.advance(i++))
        userCancel();
      v->saveId = saveId++;
      out << "v " << v->pos << endl;
      out << "vn " << v->nrml << endl;
    }

    // Write the triangles
    forall(const vertex& v, S)
      // loop over the neighborhoods of the vertices
      forall(const vertex& n, S.neighbors(v)) {
        if(!progress.advance(i++))
          userCancel();
        // Get vertex next to n in neighborhood of v
        vertex m = S.nextTo(v, n);
        // The three vertices make a triangle, write if unique
        if(mesh->uniqueTri(v, n, m))
          out << "f " << v->saveId + 1 << " " << n->saveId + 1 << " " << m->saveId + 1 << endl;
      }
  } else if(fileType == "ply") {
    // First count the triangles for the header
    int triCount = 0;
    forall(const vertex& v, S)
      forall(const vertex& n, S.neighbors(v)) {
        vertex m = S.nextTo(v, n);
        if(mesh->uniqueTri(v, n, m))
          triCount++;
      }
    // Write header
    out << "ply" << endl;
    out << "format ascii 1.0" << endl;
    out << "element vertex " << S.size() << endl;
    out << "property float x" << endl;
    out << "property float y" << endl;
    out << "property float z" << endl;
    out << "property float signal" << endl;
    out << "property int label" << endl;
    out << "element face " << triCount << endl;
    out << "property list int int vertex_index" << endl;
    out << "end_header" << endl;

    // First write vertices
    forall(const vertex& v, S) {
      if(!progress.advance(i++))
        userCancel();
      out << v->pos << " " << v->signal << " " << v->label << endl;
      v->saveId = saveId++;
    }
    // Now write cells (faces)
    forall(const vertex& v, S) {
      if(!progress.advance(i++))
        userCancel();
      forall(const vertex& n, S.neighbors(v)) {
        vertex m = S.nextTo(v, n);
        if(mesh->uniqueTri(v, n, m))
          out << "3 " << v->saveId << " " << n->saveId << " " << m->saveId << endl;
      }
    }
  } else
    throw(QString("Invalid file type: '%1'").arg(fileType));

  file.close();
  SETSTATUS("Export mesh to file:" << mesh->file() << ", vertices:" << S.size());

  return true;
}
REGISTER_MESH_PROCESS(ExportMeshToFile);
} // namespace process
} // namespace lgx
