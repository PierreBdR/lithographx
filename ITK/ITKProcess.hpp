/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef ITKPROCESS_HPP
#define ITKPROCESS_HPP

#include <ITKConfig.hpp>
#include <Process.hpp>

#include <itkObject.h>
#include <itkImage.h>
#include <itkImportImageFilter.h>
#include <itkImageSliceConstIteratorWithIndex.h>
#include <itkImageRegionConstIterator.h>
#include <itkImageFileWriter.h>
#include <itkAdaptImageFilter.h>

namespace lgx {
namespace process {
template <size_t dim>
using UImageType = itk::Image<unsigned short, dim>;

template <size_t dim>
using SImageType = itk::Image<signed short, dim>;

template <size_t dim>
using FImageType = itk::Image<float, dim>;

using U2DImageType = UImageType<2>;
using S2DImageType = SImageType<2>;
using F2DImageType = FImageType<2>;

using U3DImageType = UImageType<3>;
using S3DImageType = SImageType<3>;
using F3DImageType = FImageType<3>;

template <typename T>
struct FromUnsignedAccessor;

template <typename T>
struct ToUnsignedAccessor;

template <>
struct itkutil_EXPORT FromUnsignedAccessor<signed short> {
  typedef signed short ExternalType;
  typedef unsigned short InternalType;

  static ExternalType Get(const InternalType& value) {
    return ExternalType(value >> 1);
  }

  static void Set(InternalType& output, const ExternalType& value)
  {
    if(value > 0)
      output = InternalType(value) << 1;
    else
      output = 0;
  }
};

using UnsignedToSignedAccessor = FromUnsignedAccessor<signed short>;

template <>
struct itkutil_EXPORT ToUnsignedAccessor<signed short> {
  typedef unsigned short ExternalType;
  typedef signed short InternalType;

  static ExternalType Get(const InternalType& value)
  {
    if(value > 0)
      return ExternalType(value) << 1;
    return 0;
  }

  static void Set(InternalType& output, const ExternalType& value) {
    output = InternalType(value >> 1);
  }
};

using SignedToUnsignedAccessor = FromUnsignedAccessor<signed short>;

template <>
struct itkutil_EXPORT FromUnsignedAccessor<float> {
  typedef float ExternalType;
  typedef unsigned short InternalType;

  static ExternalType Get(const InternalType& value) {
    return float(value) / 65535.;
  }

  static void Set(InternalType& output, const ExternalType& value)
  {
    if(value > 0)
      output = InternalType(value * 65535.);
    else
      output = 0;
  }
};

using UnsignedToFloatAccessor = FromUnsignedAccessor<float>;

template <>
struct itkutil_EXPORT ToUnsignedAccessor<float> {
  typedef unsigned short ExternalType;
  typedef float InternalType;

  static ExternalType Get(const InternalType& value)
  {
    if(value > 0)
      return ExternalType(value * 65535.);
    else
      return 0u;
  }

  static void Set(InternalType& output, const ExternalType& value) {
    output = InternalType(float(value) / 65535.);
  }
};

using FloatToUnsignedAccessor = ToUnsignedAccessor<float>;

template <typename T, size_t dim>
class itkutil_EXPORT ImageConverter : public itk::Object {
public:
  // Type of the image type
  using ImageType = itk::Image<T, dim>;

  // Type of the original image type
  using OImageType = UImageType<dim>;

private:
  ImageConverter();

  typedef itk::ImportImageFilter<ushort, dim> ImportFilterType;
  typedef itk::AdaptImageFilter<UImageType<dim>, ImageType, FromUnsignedAccessor<T>> AdapterType;

public:
  ~ImageConverter();

  typedef ImageConverter Self;
  typedef itk::SmartPointer<const Self> ConstPointer;
  typedef itk::SmartPointer<Self> Pointer;
  typedef typename ImportFilterType::SpacingType SpacingType;
  typedef typename ImportFilterType::RegionType RegionType;
  typedef typename ImportFilterType::OriginType OriginType;


  virtual itk::SmartPointer<itk::LightObject> CreateAnother() const;
  virtual const char* GetNameOfClass() const;

  void SetStore(const Store* store);
  void Update();
  typename ImageType::Pointer GetOutput();

  static bool TransferImage(const Stack* stack, Store* store, typename ImageType::ConstPointer image);
  static bool TransferImage(Stack* stack, Store* store, typename ImageType::ConstPointer image);

  static Pointer New();

  const SpacingType& GetSpacing() const {
    return importer->GetSpacing();
  }
  const RegionType& GetRegion() const {
    return importer->GetRegion();
  }
  const OriginType& GetOrigin() const {
    return importer->GetOrigin();
  }

protected:
  const Store* store;
  typename ImportFilterType::Pointer importer;
  typename AdapterType::Pointer adapter;
};

template <size_t dim>
class itkutil_EXPORT ImageConverter<unsigned short, dim> : public itk::Object {
public:
  // Type of the image type
  using ImageType = UImageType<dim>;

  // Type of the original image type
  using OImageType = UImageType<dim>;

private:
  ImageConverter();

  typedef itk::ImportImageFilter<ushort, dim> ImportFilterType;

public:
  ~ImageConverter();

  typedef ImageConverter Self;
  typedef itk::SmartPointer<const Self> ConstPointer;
  typedef itk::SmartPointer<Self> Pointer;
  typedef typename ImportFilterType::SpacingType SpacingType;
  typedef typename ImportFilterType::RegionType RegionType;
  typedef typename ImportFilterType::OriginType OriginType;

  virtual itk::SmartPointer<itk::LightObject> CreateAnother() const;
  virtual const char* GetNameOfClass() const;

  void SetStore(const Store* store);
  void Update();
  typename ImageType::Pointer GetOutput();

  static bool TransferImage(const Stack* stack, Store* store, typename ImageType::ConstPointer image);
  static bool TransferImage(Stack* stack, Store* store, typename ImageType::ConstPointer image);

  const SpacingType& GetSpacing() const {
    return importer->GetSpacing();
  }
  const RegionType& GetRegion() const {
    return importer->GetRegion();
  }
  const OriginType& GetOrigin() const {
    return importer->GetOrigin();
  }

  static Pointer New();

protected:
  const Store* store;
  typename ImportFilterType::Pointer importer;
};

#ifndef WIN32
extern template class itkutil_EXPORT ImageConverter<float, 2>;
extern template class itkutil_EXPORT ImageConverter<float, 3>;
extern template class itkutil_EXPORT ImageConverter<signed short, 2>;
extern template class itkutil_EXPORT ImageConverter<signed short, 3>;
extern template class itkutil_EXPORT ImageConverter<unsigned short, 2>;
extern template class itkutil_EXPORT ImageConverter<unsigned short, 3>;
#endif

template <size_t dim>
using UImageConverter = ImageConverter<unsigned short, dim>;
template <size_t dim>
using SImageConverter = ImageConverter<signed short, dim>;
template <size_t dim>
using FImageConverter = ImageConverter<float, dim>;

using U3DImageConverter = ImageConverter<unsigned short, 3>;
using U2DImageConverter = ImageConverter<unsigned short, 2>;
using S3DImageConverter = ImageConverter<signed short, 3>;
using S2DImageConverter = ImageConverter<signed short, 2>;
using F3DImageConverter = ImageConverter<float, 3>;
using F2DImageConverter = ImageConverter<float, 2>;

} // namespace process
} // namespace lgx

#endif // ITKPROCESS_HPP
