/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

// Parameter reader class
#include "Parms.hpp"

#include <algorithm>
#include <cctype>
#include <fstream>
#include "Information.hpp"
#include <iostream>
#include <iterator>
#include <map>
#include <qdir.h>
#include <sstream>

#ifdef __unix__
#  include <fcntl.h>
#  include <sys/file.h>
#  include <sys/stat.h>
#endif

QTextStream& operator>>(QTextStream& ss, bool& b)
{
  QString val;
  ss >> val;
  val = val.toLower();
  b = (val == "true");
  return ss;
}

namespace lgx {
namespace util {

using Information::err;
using std::map;
using std::ostream_iterator;
using std::copy;

Parms iniFile(int vl)
{
  return Parms("=", "#", vl);
}

Parms iniFile(const QString& filename, int vl)
{
  return Parms(filename, "=", "#", vl);
}

Parms iniFile(QIODevice* dev, int vl)
{
  return Parms(dev, "=", "#", vl);
}

Parms::Parms(QString sep, QString comment, int vl)
  : separator(sep)
  , comment_start(comment)
{
  verboseLevel(vl);
}

Parms::Parms(const QString& parmFile, QString sep, QString comment, int vl)
  : separator(sep)
  , comment_start(comment)
{
  verboseLevel(vl);
  init(parmFile);
}

Parms::Parms(int vl)
{
  verboseLevel(vl);
}

Parms::Parms(const QString& parmFile, int vl)
{
  verboseLevel(vl);
  init(parmFile);
}

Parms::Parms(QIODevice* parmDev, QString sep, QString comment, int vl)
  : separator(sep)
  , comment_start(comment)
{
  verboseLevel(vl);
  init(parmDev);
}

Parms::Parms(QIODevice* parmDev, int vl)
{
  verboseLevel(vl);
  init(parmDev);
}

void Parms::init(const QString& filename)
{
  if(filename.isEmpty()) {
    loaded = true;
    return;
  }
#ifdef __unix__
  // First, obtain the lock
  QByteArray ba = filename.toLocal8Bit();
  int fd = open(ba.data(), O_RDONLY);
  flock(fd, LOCK_SH);
#endif
  QFile fIn(filename);
  if(!fIn.open(QIODevice::ReadOnly)) {
    if(VerboseLevel > 0)
      err << "Parms::Parms:Error opening " << filename << endl;
    return;
  }
  init(&fIn);
#ifdef __unix__
  // At last, release the lock
  flock(fd, LOCK_UN);
#endif
}

void Parms::init(QIODevice* dev)
{
  QTextStream ss(dev);
  ss.setCodec("UTF-8");
  unsigned int line = 0;
  int pos;
  QString buff;
  if(VerboseLevel > 3) {
    err << "Separator: '" << separator << "' -- comment start = '" << comment_start << "'" << endl;
  }
  while(!ss.atEnd() and ss.status() == QTextStream::Ok) {
    line++;
    // read in line
    buff = ss.readLine();
    if(VerboseLevel > 3)
      err << "Parms::Parms:Read line " << line << ":\n'" << buff << "'" << endl;
    // find C++ style comments
    pos = buff.indexOf(comment_start);
    // and remove to end of line
    if(pos != -1)
      buff = buff.mid(0, pos);
    // remove leading and trailing whitespace
    buff = buff.trimmed();
    // skip line if blank
    if(buff.length() == 0) {
      if(VerboseLevel > 3)
        err << " -> empty line" << endl;
      continue;
    }
    // Look for section
    if(buff[0] == '[' && buff.right(1)[0] == ']') {
      Section = buff.mid(1, buff.length() - 2);
      if((Section.length() == 0) && (VerboseLevel > 0))
        err << "Parms::Parms:Error on line " << line << ", []" << endl;
      continue;
    }
    // split key and value
    pos = buff.indexOf(separator);
    // error if no : delimiter
    if(pos == -1) {
      if(VerboseLevel > 0)
        err << "Parms::Parms:Error on line " << line << ", missing :" << endl;
      continue;
    }
    // get key and value and remove leading/trailing blanks
    QString key = buff.mid(0, pos).trimmed();
    QString value = buff.mid(pos + 1).trimmed();
    // error if no key
    if(key.length() == 0) {
      if(VerboseLevel > 0)
        err << "Parms::Parms:Error on line " << line << ", missing key" << endl;
      continue;
    }
    // error if no value
    if(value.length() == 0) {
      if(VerboseLevel > 2)
        err << "Parms::Parms:Warning on line " << line << ", missing value" << endl;
    }

    // Now we have key and value, add to map
    Parameters[Section + ":" + key] << value;

    if(VerboseLevel > 3)
      err << "Parms::Parms:Adding new value " << Section << ":" << key << " = " << value << endl;
  }
  loaded = true;
}

Parms::~Parms() {
}

// Check if parm exists
bool Parms::check(const QString& key) {
  return (Parameters.find(key) != Parameters.end());
}

bool Parms::hasSection(const QString& key)
{
  return check(key);
}

bool Parms::hasValue(const QString& key, const QString& value)
{
  CheckExist = false;
  QStringList values;
  bool res = extractValues(key, value, values);
  CheckExist = true;
  return res and not values.empty();
}

bool Parms::operator()(const QString& section, const QString& key, bool& value)
{
  return operator()<bool>(section, key, value);
}

bool Parms::operator()(const QString& section, const QString& key, int& value)
{
  return operator()<int>(section, key, value);
}

bool Parms::operator()(const QString& section, const QString& key, float& value)
{
  return operator()<float>(section, key, value);
}

bool Parms::operator()(const QString& section, const QString& key, double& value)
{
  return operator()<double>(section, key, value);
}

bool Parms::operator()(const QString& section, const QString& key, std::string& value)
{
  return operator()<std::string>(section, key, value);
}

bool Parms::operator()(const QString& section, const QString& key, QString& value)
{
  return operator()<QString>(section, key, value);
}

bool Parms::all(const QString& section, const QString& key, std::vector<bool>& value)
{
  return all<std::vector<bool> >(section, key, value);
}

bool Parms::all(const QString& section, const QString& key, std::vector<int>& value)
{
  return all<std::vector<int> >(section, key, value);
}

bool Parms::all(const QString& section, const QString& key, std::vector<float>& value)
{
  return all<std::vector<float> >(section, key, value);
}

bool Parms::all(const QString& section, const QString& key, std::vector<double>& value)
{
  return all<std::vector<double> >(section, key, value);
}

bool Parms::all(const QString& section, const QString& key, std::vector<QString>& value)
{
  return all<std::vector<QString> >(section, key, value);
}

bool Parms::all(const QString& section, const QString& key, QStringList& value)
{
  return all<QStringList>(section, key, value);
}

bool Parms::all(const QString& section, std::map<QString, std::vector<bool> >& value)
{
  return all<bool>(section, value);
}

bool Parms::all(const QString& section, std::map<QString, std::vector<int> >& value)
{
  return all<int>(section, value);
}

bool Parms::all(const QString& section, std::map<QString, std::vector<float> >& value)
{
  return all<float>(section, value);
}

bool Parms::all(const QString& section, std::map<QString, std::vector<double> >& value)
{
  return all<double>(section, value);
}

bool Parms::all(const QString& section, std::map<QString, std::vector<QString> >& value)
{
  return all<QString>(section, value);
}

bool Parms::all(const QString& section, std::map<QString, QStringList>& value) {
  return all<QString>(section, value);
}

bool Parms::readValue(const QString& raw_value, bool& variable)
{
  QString value = raw_value.toLower();
  if(value == "true") {
    variable = true;
    return true;
  } else if(value == "false") {
    variable = false;
    return true;
  }
  return false;
}

bool Parms::readValue(const QString& value, QString& variable)
{
  variable = value;
  return true;
}

bool Parms::readValue(const QString& value, std::string& variable)
{
  variable = value.toStdString();
  return true;
}

bool Parms::extractValues(const QString& section, const QString& key, QStringList& values)
{
  QString real_key = section + ":" + key;

  if(check(real_key)) {
    values = Parameters[real_key];

    if(VerboseLevel > 3) {
      err << "Parms::extractValues:Debug strings for key [" << section << "]" << key << ": * " << values.join(" * ")
          << endl;
    }
    return true;
  }

  if(CheckExist && (VerboseLevel > 0))
    err << "Parms::operator():Error key not found [" << section << "]" << key << endl;
  return false;
}

} // namespace util
} // namespace lgx

