/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef PACKAGECONFIGURATIONDLG_HPP
#define PACKAGECONFIGURATIONDLG_HPP

#include <LGXConfig.hpp>

#include <PackageManager.hpp>

#include <ui_PackageConfigurationDlg.h>
#include <ui_RunningPackaging.h>

#include <QFileDialog>
#include <QGridLayout>
#include <QPointer>
#include <QPushButton>
#include <QEvent>

namespace lgx {
namespace gui {

class InstallPackageEvent : public QEvent
{
public:
  InstallPackageEvent(QString path);
  ~InstallPackageEvent() override;
  const QString& path() const { return _path; }

private:
  QString _path;

};

class RunningPackaging : public QDialog
{
  Q_OBJECT
public:
  RunningPackaging(const QString& title,
                   const QString& actionName,
                   QWidget *parent = nullptr,
                   Qt::WindowFlags fl = 0);

public slots:
  void reject() override;
  int exec() override;

  void finished();
  void step(const QString& name);
  void ok();
  void error(const QString& err, bool paused);
  bool progress();
  bool yesNo(const QString& msg);

private:
  Ui::RunningPackaging ui;
  bool isCanceled;
  QGridLayout *steps;
  QPixmap good, bad;
};

class PackageListModel : public QAbstractItemModel
{
  Q_OBJECT
public:

  PackageListModel(QObject *parent = nullptr);

  //@{
  ///\name Structure description
  int columnCount(const QModelIndex &parent = QModelIndex()) const override;
  int rowCount(const QModelIndex &parent = QModelIndex()) const override;
  QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
  //@}
  //

  //@{
  ///\name Definition of the items
  QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
  QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const override;
  Qt::ItemFlags flags(const QModelIndex &index) const override;
  QModelIndex parent(const QModelIndex &index) const override;
  //@}

  //@{
  ///\name Drag and Drop
  bool canDropMimeData(const QMimeData* data, Qt::DropAction action,
                       int row, int column, const QModelIndex& parent) const override;
  bool dropMimeData(const QMimeData * data, Qt::DropAction action,
                    int row, int column, const QModelIndex & parent) override;
  Qt::DropActions supportedDropActions() const override;
  QStringList mimeTypes() const override;
  //@}
  const Package* package(const QModelIndex& idx);

public slots:
  void updatePackages();

signals:
  void droppedPackage(const QString& path);

private:
  void makePackageList();

  PackageManager manager;
  QStringList packageList;
  QList<bool> isUserPackage;
  QList<bool> isValid;
  QPixmap good, bad;
};


class PackageConfigurationDlg : public QDialog
{
  Q_OBJECT
public:
  PackageConfigurationDlg(QWidget *parent = nullptr, Qt::WindowFlags f = 0);

public slots:
  void packageList_changeCurrent(const QModelIndex& current, const QModelIndex& previous);
  void on_buttonBox_clicked(QAbstractButton* btn);

  void installPackage(const QString& path);
  void delayedInstallPackage(const QString& path);
  void uninstallPackage(const QString& name);
  void createPackage(const QString& name, const QString& output_dir);

protected:
  void showEvent(QShowEvent *e) override;
  bool event(QEvent *event) override;

private:
  QString makeText(const Package* pkg);

  Ui::PackageConfigurationDlg ui;

  QString curFolder;
  QPointer<QFileDialog> fileDlg;
  QPointer<QPushButton> install_folder_btn, install_file_btn, uninstall_btn, create_btn;
  QPointer<PackageListModel> pkg_model;
};


} // namespace gui
} // namespace lgx

#endif // PACKAGECONFIGURATIONDLG_HPP
