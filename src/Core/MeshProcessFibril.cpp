/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "MeshProcessFibril.hpp"

#include "Information.hpp"
#include "Progress.hpp"
#include "Mesh.hpp"
#include "Misc.hpp"

#include <Eigen/Dense>

namespace lgx {
namespace process {

using util::SymmetricTensor;

namespace {

enum FibrilDisplayType {
  FD_NONE = 0,
  FD_SCORE,
};

FibrilDisplayType readFibrilDisplayType(QString value, bool* ok = 0)
{
  value = value.toLower();
  if(ok)
    *ok = true;
  if(value == "none")
    return FD_NONE;
  if(value == "score")
    return FD_SCORE;
  if(ok)
    *ok = false;
  return FD_NONE;
}

QString toString(FibrilDisplayType fd)
{
  switch(fd) {
  case FD_NONE:
    return "None";
  case FD_SCORE:
    return "Score";
  }
  return "";
}

// Class to calculate principle directions of cell to find plane of projection
struct PCA {
  Point3f p1, p2, p3;
  Point3f ev;
  Eigen::Matrix3f mat;
  Eigen::Matrix3f evec;

  PCA()
  {
  }

  ~PCA()
  {
  }

  bool operator()(const Eigen::Matrix3f& corr)
  {
    Eigen::SelfAdjointEigenSolver<Eigen::Matrix3f> eigensolver(corr);
    if(eigensolver.info() != Eigen::Success) {
      QString error;
      switch(eigensolver.info()) {
        case Eigen::Success:
          error = "Success? Error!";
          break;
        case Eigen::NumericalIssue:
          error = "Numerical Issue";
          break;
        case Eigen::NoConvergence:
          error = "No Convergence";
          break;
        case Eigen::InvalidInput:
          error = "Invalid Input";
          break;
      }
      Information::err << "Error computing eigen values: " << error << endl;
      return false;
    }

    ev = Point3f(eigensolver.eigenvalues());
    mat = eigensolver.eigenvectors();
    // Sort the eigenvalues

    size_t i0 = 0, i1 = 0, i2 = 0;
    if(ev[0] >= ev[1] and ev[0] >= ev[2]) {
      i0 = 0;
      if(ev[1] >= ev[2]) {
        i1 = 1;
        i2 = 2;
      } else {
        i1 = 2;
        i2 = 1;
      }
    } else if(ev[1] >= ev[0] and ev[1] >= ev[2]) {
      i0 = 1;
      if(ev[0] >= ev[2]) {
        i1 = 0;
        i2 = 2;
      } else {
        i1 = 2;
        i2 = 0;
      }
    } else {
      i0 = 2;
      if(ev[0] >= ev[1]) {
        i1 = 0;
        i2 = 1;
      } else {
        i1 = 1;
        i2 = 0;
      }
    }
    ev = Point3f(ev[i0], ev[i1], ev[i2]);
    Information::out << "ev = " << ev << endl;
    p1 = Point3f(mat.col(i0));
    p2 = Point3f(mat.col(i1));
    p3 = Point3f(mat.col(i2));
    if((p1 ^ p2) * p3 < 0)
      p3 = -p3;
    return true;
  }
};
} // namespace


bool FibrilOrientations::operator()(Mesh* mesh, float border, float minAreaRatio)
{
  mesh->clearCellAxis();
  IntPoint3fMap& centers = mesh->labelCenter();
  IntPoint3fMap& normals = mesh->labelNormal();
  std::unordered_map<int, Eigen::Matrix3f> correlations;
  IntFloatMap areas;
  IntFloatMap weightedAreas;
  IntFloatMap insideAreas;

  centers.clear();
  normals.clear();

  const vvgraph& S = mesh->graph();

  // Mark the vertices within this distance from the border.
  mesh->markBorder(border);

  // Find cell centers and areas.
  mesh->updateCentersNormals();

  forall(const vertex& v, S) {
    forall(const vertex& n, S.neighbors(v)) {
      const vertex& m = S.nextTo(v, n);
      if(!mesh->uniqueTri(v, n, m))
        continue;
      int label = mesh->getLabel(v, n, m);
      if(label <= 0)
        continue;

      Point3f nrml = (n->pos - v->pos) ^ (m->pos - v->pos);
      float area = norm(nrml) / 2;
      normals[label] += nrml;
      areas[label] += area;
      centers[label] += (v->pos + n->pos + m->pos) / 3.0 * area;
      if((n->minb == 0)and (v->minb == 0) and (m->minb == 0))
        insideAreas[label] += area;
    }
  }
  forall(const IntFloatPair& p, areas) {
    int label = p.first;
    centers[label] /= p.second;
    normals[label].normalize();
    correlations[label] = Eigen::Matrix3f::Zero();
  }

  // Calculate orientation the perpendicular to the average gradient of the signal
  forall(const vertex& v, S) {
    if(v->minb != 0)
      continue;
    forall(const vertex& n, S.neighbors(v)) {
      if(n->minb != 0)
        continue;
      const vertex& m = S.nextTo(v, n);
      if(m->minb != 0 or !mesh->uniqueTri(v, n, m))
        continue;
      int label = mesh->getLabel(v, n, m);
      if(label <= 0)
        continue;
      // if the area of the triangles used for computation is < half of cell area, don't compute the PO
      if(insideAreas[label] < areas[label] * minAreaRatio)
        continue;

      // Reduce the problem to a triangle centered on v, and with dn on the X axis
      float ds_n = n->signal - v->signal;
      float ds_m = m->signal - v->signal;

      Point3f dn = n->pos - v->pos;
      Point3f dm = m->pos - v->pos;

      Point3f nrml = dn ^ dm;
      float area = norm(nrml);
      nrml /= area;
      area /= 2.f;

      // Create the 2D triangle
      // Define the axis in the triangle plane
      float len_dn = norm(dn);
      Point3f x = dn / len_dn;
      Point3f y = nrml ^ x;

      // In this 3D context, the normal to the place is (a,b,c) and the equation of the plane is:
      // ax + by + cs = 0
      // Then the function of signal is s = -a/c x - b/c y
      // so the gradient is (-a/c, -b/c)
      Point3f n2(len_dn, 0, ds_n);
      Point3f m2(dm * x, dm * y, ds_m);
      Point3f grad2d = n2 ^ m2;
      grad2d /= grad2d.z();

      Point3f grad = -grad2d.x() * x - grad2d.y() * y;

      // Direction along the fibres
      grad = nrml ^ grad;

      Eigen::Matrix3f corr;
      corr = Eigen::Matrix3f::Zero();
      for(int i = 0; i < 3; i++)
        for(int j = 0; j < 3; j++)
          corr(i, j) = grad[i] * grad[j];

      correlations[label] += corr * area;       // big triangles should count more than small ones
      weightedAreas[label]
        += area * normsq(grad);         // we normalize the sum of correlations by the norm of gradient square
    }
  }

  // Do the PCA
  forall(const IntFloatPair& p, weightedAreas) {
    // corr is an average of the correlations, weighted by triangles size
    Eigen::Matrix3f corr = correlations[p.first] / p.second;

    PCA pca;
    if(!pca(corr)) {
      setWarningMessage("Error performing PCA");
      mesh->cellAxis().erase(p.first);
    } else {
      SymmetricTensor& tensor = mesh->cellAxis()[p.first];
      tensor.ev1() = pca.p1;
      tensor.ev2() = pca.p2;
      float score = (pca.ev[0] - pca.ev[1]) / (pca.ev[0] + pca.ev[1]);
      tensor.evals() = Point3f(score, pca.ev[0], 0.f);
    }
  }

  mesh->setCellAxis();
  Description axisType = Description("Fibril Orientations")
    << std::make_pair("Border Size", QString("%1 " + UM).arg(border))
    << std::make_pair("Min. Area ratio", QString::number(minAreaRatio));
  mesh->cellAxisDesc() = axisType;
  mesh->cellAxisUnit() = "a.u.";
  mesh->updateTriangles();
  return true;
}

bool FibrilOrientations::showResult()
{
  ParmList parms;
  if(not getDefaultParms("Mesh", "Display Fibril Orientations", parms))
    return setErrorMessage("Couldn't find process Mesh.Display_Fibril_Orientations");
  return runProcess("Mesh", "Display Fibril Orientations", parms);
}

REGISTER_MESH_PROCESS(FibrilOrientations);

// Display principal orientations (separatly from each other) and heatmaps of anisotropy etc.
bool DisplayFibrilOrientations::operator()(Mesh* mesh, const QString& displayHeatMapStr, const QColor& qcolorMax,
                                           float axisLineWidth, float scaleAxisLength, float axisOffset,
                                           float threshold)
{
  // if there is no cell axis stored, display error message
  const IntSymTensorMap& dirs = mesh->cellAxis();
  if(dirs.size() == 0 or mesh->cellAxisDesc().type() != "Fibril Orientations")
    return setErrorMessage(QString("No fibril axis stored in active mesh."));

  bool ok;
  FibrilDisplayType displayHeatMap = readFibrilDisplayType(displayHeatMapStr, &ok);
  if(not ok)
    return setErrorMessage(QString("Error, unknown heat map type: '%1'").arg(displayHeatMapStr));

  // Update the normals and center of the cell for visualisation.
  if(mesh->labelCenter().empty() or mesh->labelNormal().empty())
    mesh->updateCentersNormals();

  // Scale PDGs for visualization
  mesh->cellAxisWidth() = axisLineWidth;
  mesh->cellAxisOffset() = axisOffset;
  Colorf colorMax = qcolorMax;
  mesh->prepareAxisDrawing(scaleAxisLength, 0, colorMax, colorMax, Point3b(true, false, false));
  forall(const IntSymTensorPair& p, mesh->cellAxis()) {
    int cell = p.first;
    const SymmetricTensor& tensor = p.second;
    if(tensor.evals()[0] < threshold)
      mesh->cellAxisScaled()[cell].zero();
  }

  if(displayHeatMap != FD_NONE) {
    IntFloatMap labelHeatMap;
    float maxValue = -FLT_MAX;
    float minValue = FLT_MAX;
    forall(const IntSymTensorPair& p, dirs) {
      int cell = p.first;
      const SymmetricTensor& tensor = p.second;

      float value = tensor.evals()[0];
      labelHeatMap[cell] = value;

      if(value > maxValue)
        maxValue = value;
      if(value < minValue)
        minValue = value;
    }

    using std::swap;
    // Adjust heat map if run on parents
    if(mesh->isParentAxis()) {
      mesh->clearHeatmap();
      IntIntMap& parentMap = mesh->parentLabelMap();
      IntFloatMap& labelHeat = mesh->labelHeat();
      forall(const IntIntPair& p, parentMap) {
        int mother = p.second;
        int daughter = p.first;
        if(daughter > 0) {
          IntFloatMap::iterator found = labelHeatMap.find(mother);
          if(found != labelHeatMap.end())
            labelHeat[daughter] = found->second;
        }
      }
    } else
      swap(labelHeatMap, mesh->labelHeat());

    mesh->heatMapBounds() = Point2f(minValue, maxValue);
    mesh->showHeat();
    mesh->updateTriangles();
  }

  return true;
}
REGISTER_MESH_PROCESS(DisplayFibrilOrientations);

bool SaveFibrilOrientation::operator()(Mesh* mesh, const QString& filename)
{
  QString k1Name = "Score";
  QString k2Name = "Intensity";
  return saveFile(mesh, filename, k1Name, k2Name);
}

REGISTER_MESH_PROCESS(SaveFibrilOrientation);

bool LoadFibrilOrientation::operator()(Mesh* mesh, const QString& filename)
{
  QRegularExpression k1Name("Score");
  QRegularExpression k2Name("Intensity");
  QRegularExpression k3Name;
  if(not readFile(mesh, "Fibril Orientations", filename, k1Name, k2Name, k3Name)) {
    QString k1, k2, k3;
    if(not readOldFile(mesh, "Fibril Orientations", filename, k1, k2, k3))
      return false;
    // Compute the score ...
    typedef IntSymTensorMap::value_type value_type;
    forall(value_type& p, mesh->cellAxis()) {
      SymmetricTensor& tensor = p.second;
      float score = (tensor.evals()[0] - tensor.evals()[1]) / (tensor.evals()[0] + tensor.evals()[1]);
      tensor.evals() = Point3f(score, tensor.evals()[0], 0);
    }
  }
  mesh->cellAxisUnit() = "a.u.";
  return true;
}

REGISTER_MESH_PROCESS(LoadFibrilOrientation);
} // namespace process
} // namespace lgx
