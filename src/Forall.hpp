/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

/**
 * \file Forall.hpp
 *
 * This file contains the defines the forall loops.
 *
 * Forall allows to iterate on STL container or range.
 * \note A STL range is a pair of iterator where the first element of the pair
 * is the beginning the second element of the pair is the end.
 *
 * Use examples:
 *
 * \code
 * std::vector<int> va;
 * // ... Filling in va ...
 * forall(int a, va)
 *   {
 *   cout << a << endl;
 *   }
 * \endcode
 *
 * This example shows how to iterate on a container returned by value by
 * a function:
 * \code
 * std::vector<int> create_vector();
 * // Now put the output of the function in a local variable
 * std::vector<int> va = create_vector();
 * forall(int a, va)
 *   {
 *   cout << a << endl;
 *   }
 * \endcode
 * \note You should not iterate directly on it! The loop do not keep the
 * container alive.
 *
 * Use example with range:
 * \code
 * typedef std::vector<int>::iterator iterator;
 * std::pair<iterator,iterator> extract(std::vector<int> v, int first, int last);
 * std::vector<int> va;
 * // ... Filling in va ...
 * forall(int& i, extract(va, 2, 10))
 *   {
 *   i += 5;
 *   cout << i << endl;
 *   }
 * \endcode
 * In this example, the values in the vector are changed too.
 */
#ifndef UTIL_FORALL_HPP
#define UTIL_FORALL_HPP

#include <LGXConfig.hpp>

#include <iterator>
#include <utility>

namespace lgx {
namespace util {
/// \cond InternalDoc
namespace ForAll {
struct BaseForwardIter {
  virtual ~BaseForwardIter() {
  }
};

template <typename iterator> struct ForwardIter : public BaseForwardIter {
  typedef typename std::iterator_traits<iterator>::value_type value_type;
  typedef typename std::iterator_traits<iterator>::reference reference;

  ForwardIter(const iterator& fst, const iterator& lst)
    : it(fst)
    , end(lst)
    , brk(0)
  {
  }

  const reference value() const
  {
    ++brk;
    return *it;
  }
  bool is_end() const {
    return (it == end) || brk;
  }
  mutable iterator it;
  iterator end;
  mutable int brk;
};

template <typename Pair, typename iterator> inline ForwardIter<iterator> forwardIter(const Pair& range, const iterator*)
{
  return ForwardIter<iterator>(range.first, range.second);
}

template <typename iterator>
inline const ForwardIter<iterator>* castForwardIter(const BaseForwardIter* base, const iterator*)
{
  return static_cast<const ForwardIter<iterator>*>(base);
}

template <typename T> inline T* pointer(const T&) {
  return 0;
}

template <typename Container>
std::pair<typename Container::iterator, typename Container::iterator> make_range(Container& cont)
{
  return std::make_pair(cont.begin(), cont.end());
}

template <typename Container>
std::pair<typename Container::const_iterator, typename Container::const_iterator> make_range(const Container& cont)
{
  return std::make_pair(cont.begin(), cont.end());
}

template <typename Iterator> std::pair<Iterator, Iterator> make_range(const std::pair<Iterator, Iterator>& cont)
{
  return cont;
}

template <typename Container>
std::pair<typename Container::reverse_iterator, typename Container::reverse_iterator>
make_reverse_range(Container& cont)
{
  return std::make_pair(cont.rbegin(), cont.rend());
}

template <typename Container>
std::pair<typename Container::const_reverse_iterator, typename Container::const_reverse_iterator>
make_reverse_range(const Container& cont)
{
  return std::make_pair(cont.rbegin(), cont.rend());
}

template <typename Iterator>
std::pair<std::reverse_iterator<Iterator>, std::reverse_iterator<Iterator> >
make_reverse_range(const std::pair<Iterator, Iterator>& cont)
{
  typedef std::reverse_iterator<Iterator> reverse_it;
  return make_pair(reverse_it(cont.second), reverse_it(cont.first));
}
} // namespace ForAll
} // namespace util
} // namespace lgx

namespace std {

template <typename Iterator>
Iterator begin(const pair<Iterator,Iterator>& range)
{
  return range.first;
}

template <typename Iterator>
Iterator end(const pair<Iterator,Iterator>& range)
{
  return range.second;
}

} // namespace std
#define forall_pointer(obj) (true ? 0 : lgx::util::ForAll::pointer(obj))
/// \endcond

/**
 * Macro allowing for automatic iteration over \p range.first -> \p
 * range.second (range being a std::pair)
 */
#define forall_range(typed_var, range)                                                                 \
  for(const lgx::util::ForAll::BaseForwardIter& iter                                                   \
        = lgx::util::ForAll::forwardIter(range, forall_pointer((range).first));                        \
      !lgx::util::ForAll::castForwardIter(&iter, forall_pointer((range).first))->is_end();             \
      ++(lgx::util::ForAll::castForwardIter(&iter, forall_pointer((range).first))->it))                \
    for(typed_var = lgx::util::ForAll::castForwardIter(&iter, forall_pointer((range).first))->value(); \
        lgx::util::ForAll::castForwardIter(&iter, forall_pointer((range).first))->brk;                 \
        --(lgx::util::ForAll::castForwardIter(&iter, forall_pointer((range).first))->brk))

/**
 * \def forall(var, container)
 *
 * Macro iterating over a standard container from \p cont.begin() to \p
 * cont.end() or \p cont.first to \p cont.second if \p cont is a pair.
 *
 * \warning The container has to be maintained for the whole duration of
 * the loop. All functions of VV are safe to that respect. But you should
 * not use it to iterate on a container returned by value.
 */
#if __cplusplus >= 201103L
#  define forall(typed_var, cont) for(typed_var: cont)
#else
#  define forall(typed_var, cont) forall_range(typed_var, lgx::util::ForAll::make_range(cont))
#endif

/**
 * \def forall_reverse(var, container)
 *
 * Macro iterating over a standard reversible container from \p cont.rbegin()
 * to \p cont.rend() ot from \p std::reverse_iterator(cont.second) to \p
 * std::reverse_iterator(cont.first) if \p cont is a pair.
 *
 * \warning The container has to be maintained for the whole duration of
 * the loop. All functions of VV are safe to that respect. But you should
 * not use it to iterate on a container returned by value.
 */
#define forall_reverse(typed_var, cont) forall_range(typed_var, lgx::util::ForAll::make_reverse_range(cont))

/**
 * Macro iterating avec a a container but from \p cont.begin_name() to \p
 * cont.end_name()
 *
 * \warning In the current implementation, \p cont is evaluated twice!
 */
#define forall_named(typed_var, cont, name) \
  forall_range(typed_var, std::make_pair((cont).begin_ ## name(), (cont).end_ ## name()))
#endif // UTIL_FORALL_HPP
