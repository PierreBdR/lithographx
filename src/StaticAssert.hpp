/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef STATIC_ASSERT_H
#define STATIC_ASSERT_H
/**
 * \file StaticAssert.hpp
 *
 * Define the STATIC_ASSERT macro
 */

namespace lgx {
namespace util {
// Extracted from boost static_asset.h ...
template <bool x> struct STATIC_ASSERTION_FAILURE;
template <> struct STATIC_ASSERTION_FAILURE<true> {
  enum { value = 1 };
};
template <int x> struct static_assert_test {
};

#define _LGX_JOIN(X, Y) _LGX_DO_JOIN(X, Y)
#define _LGX_DO_JOIN(X, Y) _LGX_DO_JOIN2(X, Y)
#define _LGX_DO_JOIN2(X, Y) X ## Y

/**
 * \def STATIC_ASSERT
 * Assertion that works at compile time.
 *
 * If the parameters evaluate to false, the program won't compile. Useful to
 * check that a template method is not used when it is invalid.
 *
 * A typical example can be found in the implementation of the util::Vector
 * class to make sure the constructor with n (fixed) values are used only if
 * the vector is of dimension n:
 * \code
 *   template <size_t N, typename T>
 *   Vector<N,T>::Vector( const T& v1, const T& v2)
 *   {
 *    STATIC_ASSERT(N == 2); // Compilation will fail if N != 2
 *    // ...
 *   }
 * \endcode
 *
 * \note This will work because, as specified by the standard,
 * a (non-virtual) method of class template is instantiated only if used. So,
 * as long as the user do not try to construct a vector with two values, this
 * specific constructor won't even exist.
 */
#define STATIC_ASSERT(B)                                                                        \
  typedef lgx::util::static_assert_test<sizeof(lgx::util::STATIC_ASSERTION_FAILURE<(bool)(B)>)> \
    _LGX_JOIN (static_assert_typedef_, __LINE__)
} // namespace util
} // namespace lgx
#endif // STATIC_ASSERT_H
