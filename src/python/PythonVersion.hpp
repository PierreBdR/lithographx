/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef PYTHON_PYTHONVERSION_HPP
#define PYTHON_PYTHONVERSION_HPP

#include <Version.hpp>

// repeated to avoid including Python.h
#ifdef WIN32
#    ifdef lgxPython_EXPORTS
#        define LGXPYTHON_EXPORT __declspec(dllexport)
#    else
#        define LGXPYTHON_EXPORT __declspec(dllimport)
#    endif
#else
#  define LGXPYTHON_EXPORT
#endif

#define LGX_PYTHON_VERSION 0x010000

namespace lgx {
extern LGXPYTHON_EXPORT util::Version pythonVersion;
}

#endif // PYTHON_PYTHONVERSION_HPP

