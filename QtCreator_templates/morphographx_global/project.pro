MGX_LIBS=$$system(MorphoGraphX --libs)
MGX_INCLUDE=$$system(MorphoGraphX --include)
MGX_PROCESSES=$$system(MorphoGraphX --process)
MGX_USER_PROCESSES=$$system(MorphoGraphX --user-process)
CUDA_INCLUDE=
CIMG_INCLUDE=

isEmpty(DEST) {
  DEST=system
}

TEMPLATE = lib

TARGET=%ProjectName%

DEFINES += cimg_display=0
SOURCES += %ProjectName%.cpp
HEADERS += %ProjectName%.hpp
RESOURCES = %ProjectName%.qrc

CONFIG += qt plugin release no_plugin_name_prefix
QT += xml opengl

*g++* {
QMAKE_CXXFLAGS += -Wno-unused-local-typedefs -fopenmp
LIBS += -fopenmp
}

INCLUDEPATH += $$MGX_INCLUDE $$MGX_PROCESSES/include $$CUDA_INCLUDE $$CIMG_INCLUDE
LIBS += -L$$MGX_LIBS -lmgx

contains(DEST,system) {
  target.path = $$MGX_PROCESSES
} else:contains(DEST,user) {
  target.path = $$MGX_USER_PROCESSES
} else {
  error(Variable DEST must be either \'system\' or \'user\'. Current value is \'$$DEST\')
}
INSTALLS = target

