/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef MESH_PROCESS_CELL_MESH_HPP
#define MESH_PROCESS_CELL_MESH_HPP

#include <Process.hpp>

#include <Mesh.hpp>
#include <Misc.hpp>

namespace lgx {
namespace process {
///\addtogroup MeshProcess
///@{
/**
 * \class FixMeshCorners MeshProcessCellMesh.hpp <MeshProcessCellMesh.hpp>
 *
 * Fix labelling of cell corners. Depending on the topology, it may be that a
 * triangle between three segmented cells cannot be labeled correctly. This
 * process identifies such triangle and sub-divide them in the hope of
 * changing the topology and solve the problem. This process may need to be
 * called many times to solve all the issues as this is only a heuristic that
 * tends to improve the situation.
 */
class LGXCORE_EXPORT FixMeshCorners : public MeshProcess {
public:
  FixMeshCorners(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  bool operator()(const ParmList& parms) override
  {
    if(!checkState().mesh(MESH_NON_EMPTY))
      return false;
    if(parms[2].toInt() < 1) {
      setErrorMessage("Number of runs must be at least one.");
      return false;
    }
    return (*this)(currentMesh(), parmToBool(parms[0]), parmToBool(parms[1]), parms[2].toInt());
  }

  bool operator()(Mesh* mesh, bool autoSegment, bool selectBadVertices, int maxRun);

  QString folder() const override {
    return "Cell Mesh";
  }
  QString name() const override {
    return "Fix Corners";
  }
  QString description() const override {
    return "Fix labelling of cell corners.";
  }
  QStringList parmNames() const override
  {
    return QStringList() << "Auto segmentation?"
                         << "Select bad vertices"
                         << "Max iterations";
  }
  QStringList parmDescs() const override
  {
    return QStringList() << "Re-run watershed automatically at each turn."
                         << "Select remaining bad vertices at the end.\n"
              "Helps in deleting bad vertices that cannot be segmented properly."
                         << "Maximal number of turns.";
  }
  ParmList parmDefaults() const override
  {
    return ParmList() << true
                      << true
                      << 5;
  }
  ParmChoiceMap parmChoice() const override
  {
    ParmChoiceMap map;
    map[0] = booleanChoice();
    map[1] = booleanChoice();
    return map;
  }
  QIcon icon() const override {
    return QIcon(":/images/FixCorners.png");
  }
};

/**
 * \class MakeCellMesh MeshProcessCellMesh.hpp <MeshProcessCellMesh.hpp>
 *
 * Convert a segmented mesh into a 2D cellular mesh. The final mesh only
 * contains a vertex per cell and vertices for their border.
 */
class LGXCORE_EXPORT MakeCellMesh : public MeshProcess {
public:
  MakeCellMesh(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  bool operator()(const ParmList& parms) override
  {
    if(!checkState().mesh(MESH_NON_EMPTY) or checkState().mesh(MESH_CELLS))
      return false;
    return (*this)(currentMesh(), parms[1].toFloat(), currentMesh()->graph(), parmToBool(parms[0]));
  }

  bool operator()(Mesh* mesh, float minWall, vvgraph& S, bool eraseMarginCells);

  QString folder() const override {
    return "Cell Mesh";
  }
  QString name() const override {
    return "Make Cells";
  }
  QString description() const override
  {
    return "Convert the segmented mesh into a 2D cellular mesh.\n"
           "The simplified mesh contains only vertices for cell centers and cell outlines";
  }
  QStringList parmNames() const override
  {
    return QStringList() << "Erase Margin Cells" << QString("Min Wall Length (%1)").arg(UM);
  }
  QStringList parmDescs() const override
  {
    return QStringList() << "Delete cells that touch the edge of the mesh."
                         << "Min length in between vertices within cell outlines.";
  }
  ParmList parmDefaults() const override
  {
    return ParmList() << false
                      << 1;
  }
  ParmChoiceMap parmChoice() const override
  {
    ParmChoiceMap map;
    map[0] = booleanChoice();
    return map;
  }
  QIcon icon() const override {
    return QIcon(":/images/MakeCells.png");
  }
};
///@}
} // namespace process
} // namespace lgx
#endif
