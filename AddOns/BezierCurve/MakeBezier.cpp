#include "MakeBezier.hpp"

#include <Assert.hpp>
#include <CuttingSurface.hpp>
#include <Geometry.hpp>
#include <Information.hpp>
#include <Mesh.hpp>
#include <Progress.hpp>

#include <algorithm>
#include <climits>
#include <cmath>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_eigen.h>
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include <QScriptEngine>
#include <QScriptValue>
#include <vector>

// Comment to use QR decomposition instead
// #define USE_LU

// Uncomment to get debug output
//#define FIND_BEZIER_DEBUG

namespace lgx {
namespace process {

namespace {

size_t computeOrder(QString expr, int nbPts)
{
    QString fct = QString("N = %1;\n"
                          "%2\n")
                      .arg(nbPts)
                      .arg(expr);
    QScriptEngine engine;
    QScriptValue eval = engine.evaluate(fct);
    if(engine.hasUncaughtException()) {
        Information::out << "Exception: " << eval.toString() << endl;
        return 0;
    }
    size_t N = eval.toUInt32();
#ifdef FIND_BEZIER_DEBUG
    Information::out << "order = " << N << endl;
#endif // FIND_BEZIER_DEBUG
    return N;
}

template <typename T> struct IndirectCompare {
    const std::vector<T>& values;
    IndirectCompare(const std::vector<T>& d)
        : values(d)
    {
    }

    bool operator()(size_t i1, size_t i2) const { return values[i1] < values[i2]; }
};

template <typename T> IndirectCompare<T> indirectCompare(const std::vector<T>& d) { return IndirectCompare<T>(d); }

size_t comb(size_t n, size_t k)
{
    size_t acc = 1;
    if(n - k < k)
        k = n - k;
    for(size_t j = 0; j < k; ++j)
        acc *= n - j;
    for(size_t j = 1; j < k; ++j)
        acc /= j + 1;
    return acc;
}

/*
 * Implement ideas from http://jimherold.com/2012/04/20/least-squares-bezier-fit/
 * to find an order n Bezier curve that best approximate m points.
 *
 * The idea is the following:
 *
 *
 * A bezier curve can be expressed in matrix operation as:
 *
 * Bx(t) = tt M Cx
 *
 * where tt = [ 1 t ... t^(n-1) t^n ] a line-vector where n is the order of the bezier curve
 *       M are the bezier coefficient
 *       Cx is a column vector with one coordinate of the control points
 *
 * To find M, we expand:
 *
 * Bx(t) = sum_{i=0}^n C(n,i) (1-t)^(n-i) t^i Cx_i
 *
 * Where C(n,k) is the number of combination of k element from a set of size n and Cx_i is
 * the x coordinate of the ith point. We can expand that further with:
 *
 * Bx(t) = sum_{i=0}^n C(n,i) (sum_{j=0}^{n-i} (-1)^j C(n-i,j) t^j) t^i Cx_i
 *
 * Then, if T is the matrix
 *  [[ 1 t1 ... t1^(n-1) t1^n ]
 *   [ 1 t2 ... t2^(n-1) t2^n ]
 *   ...
 *   ...
 *   [ 1 tm ... tm^(n-1) tm^n ]]
 *
 * where ti is the parameter of the point Pi on the curve.
 *
 * Then we can define the error made by a Bezier curve as:
 *
 * E(Cx) = (x - T M Cx) (x - T M Cx)'
 *
 * The minimum is the zero of the derivative, which is:
 *
 * Cx = ((TT') M)^(-1) (T x)
 */
std::vector<Point3f> findControlPoints(
    const std::vector<Point3f>& pts, const std::vector<double>& params, int order, double& sum_sq_residuals)
{
    int m = pts.size();
    int n = order + 1;


    gsl_matrix* M = gsl_matrix_calloc(n, n);
    gsl_matrix* T = gsl_matrix_alloc(n, m);

    gsl_matrix* P = gsl_matrix_alloc(m, 3);
    gsl_matrix* C = gsl_matrix_alloc(n, 3);

    gsl_vector_view Cx = gsl_matrix_column(C, 0);
    gsl_vector_view Cy = gsl_matrix_column(C, 1);
    gsl_vector_view Cz = gsl_matrix_column(C, 2);

    // Extract the vectors for each coordinate, and the matrix T
    for(int i = 0; i < m; ++i) {
        gsl_matrix_set(P, i, 0, pts[i].x());
        gsl_matrix_set(P, i, 1, pts[i].y());
        gsl_matrix_set(P, i, 2, pts[i].z());
        double t = params[i];
        double pt = 1.;
        for(int j = 0; j < n; ++j) {
            gsl_matrix_set(T, j, i, pt);
            pt *= t;
        }
    }

    // Now find coefs of M
    for(int i = 0; i < n; ++i) {
        int base = comb(order, i);
        for(int j = i; j <= order; ++j) {
            int coef = base * comb(order - i, j - i);
            if((j - i) % 2 == 1)
                coef *= -1;
            gsl_matrix_set(M, j, i, coef);
        }
    }

#ifdef FIND_BEZIER_DEBUG
    // Show M
    Information::out << "M = \n";
    for(int i = 0; i < n; ++i) {
        for(int j = 0; j < n; ++j)
            Information::out << gsl_matrix_get(M, i, j) << " ";
        Information::out << "\n";
    }
    Information::out << endl;
#endif // FIND_BEZIER_DEBUG

#ifdef USE_LU
    {
        gsl_matrix* TT = gsl_matrix_calloc(n, n);
        gsl_matrix* MTT = gsl_matrix_calloc(n, n);

        gsl_blas_dsyrk(CblasUpper, CblasNoTrans, 1.f, T, 0.f, TT);
        for(int i = 0; i < n; ++i)
            for(int j = 0; j < i; ++j)
                gsl_matrix_set(TT, i, j, gsl_matrix_get(TT, j, i));

        gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1., TT, M, 0., MTT); // MTT = (T T') M

        gsl_matrix* P1 = gsl_matrix_alloc(n, 3);

        // Compute T P
        gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1., T, P, 0., P1);

        // solve Cx = MTT^-1 * (T X)
        gsl_permutation* Perm = gsl_permutation_alloc(n);
        int signum;

        gsl_linalg_LU_decomp(MTT, Perm, &signum);

        gsl_vector_view X1 = gsl_matrix_column(P1, 0);
        gsl_vector_view Y1 = gsl_matrix_column(P1, 1);
        gsl_vector_view Z1 = gsl_matrix_column(P1, 2);

        gsl_linalg_LU_solve(MTT, Perm, &X1.vector, &Cx.vector);
        gsl_linalg_LU_solve(MTT, Perm, &Y1.vector, &Cy.vector);
        gsl_linalg_LU_solve(MTT, Perm, &Z1.vector, &Cz.vector);

        // Compute the residuals
        // T = M^T T
        gsl_blas_dtrmm(CblasLeft, CblasLower, CblasTrans, CblasNonUnit, 1., M, T);
        // Update X, Y, Z to store the difference in dimensions
        gsl_blas_dgemm(CblasTrans, CblasNoTrans, 1., T, C, -1., P);

        gsl_vector_view X = gsl_matrix_column(P, 0);
        gsl_vector_view Y = gsl_matrix_column(P, 1);
        gsl_vector_view Z = gsl_matrix_column(P, 2);

        sum_sq_residuals = 0.;
        double res;
        gsl_blas_ddot(&X.vector, &X.vector, &res);
        sum_sq_residuals += res;
        gsl_blas_ddot(&Y.vector, &Y.vector, &res);
        sum_sq_residuals += res;
        gsl_blas_ddot(&Z.vector, &Z.vector, &res);
        sum_sq_residuals += res;

        gsl_permutation_free(Perm);

        gsl_matrix_free(P1);
        gsl_matrix_free(TT);
        gsl_matrix_free(MTT);
    }
#else  // defined(USE_LU)
    {
        // Update T to become M'T
        gsl_blas_dtrmm(CblasLeft, CblasLower, CblasTrans, CblasNonUnit, 1., M, T);

        // Copy M'T and transpose it to get T'M
        gsl_matrix* Q = gsl_matrix_alloc(m, n);
        gsl_matrix_transpose_memcpy(Q, T);
        gsl_vector* tau = gsl_vector_alloc(n);
        gsl_vector* resX = gsl_vector_alloc(m);
        gsl_vector* resY = gsl_vector_alloc(m);
        gsl_vector* resZ = gsl_vector_alloc(m);

        gsl_linalg_QR_decomp(Q, tau);

        gsl_vector_view X = gsl_matrix_column(P, 0);
        gsl_vector_view Y = gsl_matrix_column(P, 1);
        gsl_vector_view Z = gsl_matrix_column(P, 2);

        gsl_linalg_QR_lssolve(Q, tau, &X.vector, &Cx.vector, resX);
        gsl_linalg_QR_lssolve(Q, tau, &Y.vector, &Cy.vector, resY);
        gsl_linalg_QR_lssolve(Q, tau, &Z.vector, &Cz.vector, resZ);

        sum_sq_residuals = 0.;
        double res;
        gsl_blas_ddot(resX, resX, &res);
        sum_sq_residuals += res;
        gsl_blas_ddot(resY, resY, &res);
        sum_sq_residuals += res;
        gsl_blas_ddot(resZ, resZ, &res);
        sum_sq_residuals += res;

        gsl_matrix_free(Q);
        gsl_vector_free(tau);
        gsl_vector_free(resX);
        gsl_vector_free(resY);
        gsl_vector_free(resZ);
    }
#endif // USE_LU

    std::vector<Point3f> result(order + 1);

    for(size_t i = 0; i < result.size(); ++i) {
        result[i].x() = gsl_vector_get(&Cx.vector, i);
        result[i].y() = gsl_vector_get(&Cy.vector, i);
        result[i].z() = gsl_vector_get(&Cz.vector, i);
    }

    gsl_matrix_free(M);
    gsl_matrix_free(T);
    gsl_matrix_free(P);
    gsl_matrix_free(C);

    return result;
}

std::vector<size_t> pca_ordering(const std::vector<Point3f>& positions, QString& errorMessage)
{
    Matrix3d coefs;
    size_t nb_cells = positions.size();
    Point3d mean;
    for(size_t i = 0; i < nb_cells; ++i)
        mean += positions[i];
    mean /= nb_cells;
    for(size_t i = 0; i < nb_cells; ++i) {
        Point3f dp = positions[i] - mean;
        for(size_t j = 0; j < 3; ++j)
            for(size_t k = 0; k < 3; ++k)
                coefs(j, k) += dp[i] * dp[j];
    }
    coefs /= nb_cells;

    Point3d eval;
    Matrix3d evec;

    // Compute eigenvectors with the GSL
    {
        gsl_eigen_symmv_workspace* workspace = gsl_eigen_symmv_alloc(3);

        gsl_matrix_view A = gsl_matrix_view_array(coefs.data(), 3, 3);
        gsl_vector_view eval_ = gsl_vector_view_array(eval.data(), 3);
        gsl_matrix_view evec_ = gsl_matrix_view_array(evec.data(), 3, 3);

        gsl_eigen_symmv(&A.matrix, &eval_.vector, &evec_.matrix, workspace);
        gsl_eigen_symmv_sort(&eval_.vector, &evec_.matrix, GSL_EIGEN_SORT_ABS_DESC);

        gsl_eigen_symmv_free(workspace);
    }

    if(eval[0] == 0) {
        errorMessage = "Not enough points, or too similar: eigenvalues are all 0";
        return std::vector<size_t>();
    }

    std::vector<double> proj(nb_cells);
    std::vector<size_t> order(nb_cells);
    Point3d ev0 = Point3d(evec(0, 0), evec(1, 0), evec(2, 0));
    for(size_t i = 0; i < nb_cells; ++i) {
        order[i] = i;
        proj[i] = positions[i] * ev0;
    }

    std::sort(order.begin(), order.end(), indirectCompare(proj));

    return order;
}


std::vector<size_t> local_ordering(const std::vector<Point3f>& positions, QString& errorMessage)
{
    size_t nb_cells = positions.size();
    std::vector<size_t> n1(nb_cells);
    std::vector<size_t> n2(nb_cells);

    for(size_t i = 0; i < nb_cells; ++i) {
        n1[i] = i;
        n2[i] = i;
    }
    // First, associate each point to two closest relative neighbors
    size_t start = nb_cells, end = nb_cells;
    for(size_t i = 0; i < nb_cells; ++i) {
        std::vector<size_t> ns(nb_cells);
        std::vector<float> dists(nb_cells);
        for(size_t j = 0; j < nb_cells; ++j) {
            ns[j] = j;
            dists[j] = norm(positions[i] - positions[j]);
        }

        sort(ns.begin(), ns.end(), indirectCompare(dists));

        size_t closest = ns[1];
        n1[i] = closest;

        for(size_t j = 2; j < nb_cells; ++j) {
            float dist = dists[ns[j]];
            bool ok = true;
            for(size_t k = 1; k < j; ++k) {
                float d2 = norm(positions[ns[j]] - positions[ns[k]]);
                if(d2 < dist) {
                    ok = false;
                    break;
                }
            }
            if(ok) {
                n2[i] = ns[j];
                break;
            }
        }

        if(n2[i] == i) {
            if(start == nb_cells)
                start = i;
            else if(end == nb_cells)
                end = i;
            else {
                errorMessage = "More than two cells with only one relative neighbor. Curvature of the file is surely "
                               "to high, or selection is bad.";
                return std::vector<size_t>();
            }
        }
    }

    if(start == nb_cells or end == nb_cells) {
        // Find vertices with the farthest neighbors
        std::vector<float> max_dist;
        std::vector<size_t> ordered_cells;
        for(size_t i = 0; i < nb_cells; ++i) {
            const Point3f& p = positions[i];
            if(n2[i] == i)
                continue;
            float l = norm(p - positions[n2[i]]); // Second neighbor is always the farther one
            max_dist.push_back(l);
            ordered_cells.push_back(i);
        }

        std::sort(ordered_cells.begin(), ordered_cells.end(), indirectCompare(max_dist));

        if(end == nb_cells) {
            end = ordered_cells.back();
            ordered_cells.pop_back();
        }
        if(start == nb_cells)
            start = ordered_cells.back();
    }

    /*
     *if(start == nb_cells)
     *{
     *    errorMessage = "Error, no cell has a unique relative neighbor. Cannot find start of cell file.";
     *    return std::vector<size_t>();
     *}
     *if(end == nb_cells)
     *{
     *    errorMessage = "Error, there is only one cell with a unique relative neighbor. Cannot find end of cell file.";
     *    return std::vector<size_t>();
     *}
     */

    using std::swap;
    std::vector<size_t> ordered(nb_cells);
    ordered[0] = start;
    ordered[1] = n1[start];
    for(size_t i = 1; i < nb_cells - 1; ++i) {
        size_t p = n1[ordered[i]];
        size_t n = n2[ordered[i]];
        if(n == ordered[i - 1])
            swap(n, p);
        if(p != ordered[i - 1]) {
            errorMessage = "Error, cell relative neighbors are inconsistents.";
            return std::vector<size_t>();
        }
        ordered[i + 1] = n;
    }
    if(ordered.back() != end) {
        errorMessage = "Error, the last cell is not the one we expect";
        return std::vector<size_t>();
    }
    return ordered;
}

std::vector<std::vector<vertex> > find_cells(const Mesh* mesh, process::FindBezier::CellType ct)
{
    using std::swap;
    std::vector<std::vector<vertex> > result;
    const std::vector<vertex>& actives = mesh->activeVertices();
    bool only_selected = actives.size() != mesh->size();
    const vvgraph& G = mesh->graph();
    switch(ct) {
    case process::FindBezier::CELL_LABEL: {
        std::unordered_map<int, size_t> label_map;
        forall(vertex v, actives) {
            if(v->label > 0) {
                  if(label_map.find(v->label) == label_map.end()) {
                    label_map[v->label] = result.size();
                    result.push_back(std::vector<vertex>());
                }
            }
        }
        forall(vertex v, G) {
            int lab = v->label;
            std::unordered_map<int, size_t>::const_iterator found = label_map.find(lab);
            if(found != label_map.end()) {
                size_t pos = found->second;
                result[pos].push_back(v);
            }
        }
        break;
    }
    case process::FindBezier::CELL_CONNECTIVITY_EXTEND:
        only_selected = false;
    case process::FindBezier::CELL_CONNECTIVITY: {
        std::unordered_set<vertex> used, to_process;
        forall(vertex v, actives) {
            if(used.find(v) != used.end())
                continue;
            result.push_back(std::vector<vertex>());
            std::vector<vertex>& cell = result.back();
            used.insert(v);
            to_process.insert(v);
            while(not to_process.empty()) {
                std::unordered_set<vertex> to_add;
                forall(vertex n, to_process) {
                    cell.push_back(n);
                    forall(vertex m, G.neighbors(n)) {
                        if((m->selected or not only_selected) and (used.find(m) == used.end())) {
                            used.insert(m);
                            to_add.insert(m);
                        }
                    }
                }
                swap(to_add, to_process);
            }
        }
        break;
    }
    case process::FindBezier::CELL_VERTICES: {
        forall(vertex v, actives)
            result.push_back(std::vector<vertex>(1, v));
    }
    }
    return result;
}
} // namespace

bool FindBezier::operator()(Mesh* mesh, CellType ct, CellParameter param, CellOrdering ordering, QString order,
    int max_order, float tmin, float tmax)
{
    std::vector<std::vector<vertex> > cells = find_cells(mesh, ct);
    size_t nb_cells = cells.size();
    std::vector<Point3f> positions(nb_cells);

    if(nb_cells < 3)
        return setErrorMessage("Error, you need at least 3 cells.");

    SETSTATUS(QString("FindBezier found %1 cells").arg(nb_cells));

    for(size_t i = 0; i < nb_cells; ++i) {
        const std::vector<vertex>& vs = cells[i];
        forall(vertex v, vs)
            positions[i] += v->pos;
        positions[i] /= vs.size();
    }

    // Now, I have the points, we need to order them

    std::vector<size_t> ordered;
    QString errorMessage;
    switch(ordering) {
    case PCA_ORDERING:
        ordered = pca_ordering(positions, errorMessage);
        break;
    case LOCAL_ORDERING:
        ordered = local_ordering(positions, errorMessage);
        break;
    }
    if(ordered.size() != nb_cells)
        return setErrorMessage(errorMessage);

    std::vector<Point3f> pts(nb_cells);
    for(size_t i = 0; i < nb_cells; ++i)
        pts[i] = positions[ordered[i]];

    std::vector<double> params(nb_cells);

    switch(param) {
    case DISTANCE_PARAM: {
        params[0] = 0;
        double acc = 0.0;
        for(size_t i = 1; i < nb_cells; ++i) {
            double d = norm(positions[ordered[i]] - positions[ordered[i - 1]]);
            acc += d;
            params[i] = acc;
        }
        for(size_t i = 1; i < nb_cells; ++i) {
            params[i] /= acc;
        }
        break;
    }
    case HEAT_PARAM: {
        if(ct != CELL_LABEL)
            return setErrorMessage(
                "You cannot use the heat of cells as parameters if they are not defined by their label!");
        const IntFloatMap& lab_heat = mesh->labelHeat();
        double sum = 0.0;
        double prev = 0;
        for(size_t i = 0; i < nb_cells; ++i) {
            int label = cells[ordered[i]][0]->label;
            IntFloatMap::const_iterator found = lab_heat.find(label);
            if(found == lab_heat.end())
                return setErrorMessage(QString("Error, cell %1 has no heat").arg(label));
            double h = found->second;
            if(i == 0)
                params[0] = 0;
            else {
                sum += (h + prev) / 2;
                params[i] = sum;
            }
            prev = h;
        }
        for(size_t i = 0; i < nb_cells; ++i) {
            params[i] /= sum;
        }
        break;
    }
    case UNIFORM_PARAM: {
        double dt = 1. / (nb_cells - 1);
        for(size_t i = 0; i < nb_cells; ++i) {
            params[i] = i * dt;
        }
        break;
    }
    default:
        return setErrorMessage("Incorrect cell parameter specification");
    }

    if(tmin != 0 or tmax != 1) {
        for(size_t i = 0; i < params.size(); ++i) {
            params[i] *= tmax - tmin;
            params[i] += tmin;
        }
    }

    int real_order = 1;
    if(order == "BIC") {
        double best_IC = HUGE_VAL;
        size_t nb_pts = pts.size();
        for(int o = 1; o <= max_order; ++o) {
            double sum_sq_residuals;
            std::vector<Point3f> ctrlPts = findControlPoints(pts, params, o, sum_sq_residuals);
            int nb_params = 3 * (o + 1);
            double log_L = nb_pts * std::log(sum_sq_residuals / nb_pts) / 2;
            double BIC = 2 * log_L + nb_params * std::log(nb_pts);
            Information::out << "BIC for order " << o << ": " << BIC << endl;

            if(best_IC > BIC) {
                best_IC = BIC;
                real_order = o;
            }
        }
        Information::out << "Best order: " << real_order << endl;
    }
    else {
        real_order = computeOrder(order, pts.size());
        if(real_order == 0)
            return setErrorMessage("Error computing order, see terminal output for details.");
        if((int)real_order > max_order)
            real_order = max_order;
    }

    double sum_sq_residuals;
    std::vector<Point3f> ctrlPts = findControlPoints(pts, params, real_order, sum_sq_residuals);

    CuttingSurface* surf = cuttingSurface();
    size_t N = ctrlPts.size();
    surf->setBezPoints(N);
    surf->initBez();

    std::vector<Point3f>& vs = surf->bezierV();
    for(size_t i = 0; i < N; ++i) {
        for(size_t j = 0; j < N; ++j)
            vs[N * i + j] = ctrlPts[i];
    }

    surf->showGrid();
    surf->hide();
    surf->setMode(CuttingSurface::BEZIER);
    surf->setSurfSize(Point2i(10 * ctrlPts.size(), 2));

    //    SETSTATUS("Found order");

    return true;
}


bool ReverseBezierCurveProcess::operator()()
{
    CuttingSurface* cut = cuttingSurface();

    if(not cut->bezier())
        return setErrorMessage("Error, the cutting surface is not a Bezier curve");

    std::vector<Point3f>& pts = cut->bezierV();
    std::reverse(pts.begin(), pts.end());

    return true;
}

} // namespace process
} // namespace lgx
