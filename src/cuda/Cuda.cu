/************************************************************************* */
/*  This file is part of LithoGraphX.                                      */
/*                                                                         */
/*  LithoGraphX is free software: you can redistribute it and/or modify    */
/*  it under the terms of the GNU General Public License as published by   */
/*  the Free Software Foundation, either version 3 of the License, or      */
/*  (at your option) any later version.                                    */
/*                                                                         */
/*  LithoGraphX is distributed in the hope that it will be useful,         */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of         */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          */
/*  GNU General Public License for more details.                           */
/*                                                                         */
/*  You should have received a copy of the GNU General Public License      */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.   */
/************************************************************************* */

#include "Cuda.hpp"

#if defined(THRUST_BACKEND_OMP)
#  include <omp.h>
#endif

using std::cout;
using std::cerr;
using std::endl;
using std::string;

namespace lgx {
namespace cuda {
// ---------- Initialization routines ----------

// Error message
inline int errMsg(const string& s)
{
  cerr << s << endl;
  return 1;
}

#ifdef THRUST_BACKEND_CUDA
// Check available memory
size_t memAvail()
{
  size_t free = 0, total = 0;
  cudaMemGetInfo(&free, &total);
  return (free);
}

static int cudaDevice = -1;
static size_t holdMemSize = 0;
static void** holdmem = 0;
static int cudaCores = 128;
static int cudaMaxThreads = 1024;

// Free hold memory
void freeMem()
{
  cudaThreadSynchronize();
  cudaError_t cuerr = cudaGetLastError();
  if(cuerr != cudaSuccess)
    cerr << "Cuda freeMem, error before freeing memory:" << cuerr << ":" << cudaGetErrorString(cuerr) << endl;
  if(holdmem != 0)
    cudaFree(holdmem);
  holdmem = 0;
  cudaThreadSynchronize();
  // cudaError_t
  cuerr = cudaGetLastError();
  if(cuerr != cudaSuccess)
    cerr << "Cuda freeMem, error freeing memory:" << cuerr << ":" << cudaGetErrorString(cuerr) << endl;
}

// Allocate hold memory
void holdMem()
{
  cudaError_t cuerr;
  cudaThreadSynchronize();
  if(holdmem != 0) {
    cudaFree(holdmem);
    cuerr = cudaGetLastError();
    if(cuerr != cudaSuccess)
      cerr << "Cuda holdMem, error freeing memory:" << cuerr << ":" << cudaGetErrorString(cuerr) << endl;
  }
  holdmem = 0;

  size_t trymem = holdMemSize;
  while(true) {
    cudaMalloc((void**)&holdmem, holdMemSize);
    cuerr = cudaGetLastError();
    if(cuerr != cudaSuccess) {
      holdMemSize -= 4 * MByte;
      if(holdMemSize < 8) {
        cerr << "Cuda holdMem, cannot allocate 8 Meg, giving up:" << cuerr << ":" << cudaGetErrorString(cuerr)
             << endl;
        holdmem = 0;
        break;
      }
    } else
      break;
  }
  if(holdMemSize != trymem)
    cerr << "Cuda holdMem, reduced memory to:" << holdMemSize / MByte << endl;
}

int setHoldMem(uint max)
{
  size_t oldmax = holdMemSize;
  if(max == 0) {
    if(oldmax == 0)
      holdMemSize = memAvail() / 4;
  } else
    holdMemSize = max * MByte;
  holdMem();
  if(holdMemSize != oldmax)
    if(oldmax != 0)
      cout << "Memory reserved for Cuda changed from: " << oldmax / MByte << " MB to: " << holdMemSize / MByte
           << " MB." << endl;
    else
      cout << "Memory reserved for Cuda: " << holdMemSize / MByte << " MB." << endl;
  return (holdMemSize / MByte);
}

void cudaVersion(int* major, int* minor)
{
  int runtimeVersion;
  cudaRuntimeGetVersion(&runtimeVersion);

  if(major)
    *major = runtimeVersion / 1000;
  if(minor)
    *minor = runtimeVersion % 1000;
}

// Initialize cuda
int initCuda()
{
  int count = 0;
  holdmem = 0;

  int driverVersion, runtimeVersion;

  cudaDriverGetVersion(&driverVersion);
  cudaRuntimeGetVersion(&runtimeVersion);

  cout << "Cuda driver version: " << (driverVersion / 1000) << "." << (driverVersion % 1000) << endl
       << "Cuda runtime version: " << (runtimeVersion / 1000) << "." << (runtimeVersion % 1000) << endl;

  cudaGetDeviceCount(&count);
  if(count == 0) {
    cerr << "There is no device." << endl;
    return (1);
  }

  cudaDeviceProp prop;
  for(cudaDevice = 0; cudaDevice < count; cudaDevice++) {
    if(cudaGetDeviceProperties(&prop, cudaDevice) == cudaSuccess) {
      if(prop.major >= 1) {
        break;
      }
    }
  }
  if(cudaDevice == count) {
    cudaDevice = -1;
    cerr << "There is no device supporting CUDA." << endl;
    return (2);
  }
  int computeCapability = prop.major * 10 + prop.minor;
  int resThreadsPerMulti = 768;
  switch(computeCapability) {
  case 10:
  case 11:
    cudaCores = prop.multiProcessorCount * 8;
    resThreadsPerMulti = 768;
    break;
  case 12:
  case 13:
    cudaCores = prop.multiProcessorCount * 8;
    resThreadsPerMulti = 1024;
    break;
  case 20:
    cudaCores = prop.multiProcessorCount * 32;
    resThreadsPerMulti = 1536;
    break;
  case 21:
    cudaCores = prop.multiProcessorCount * 48;
    resThreadsPerMulti = 1536;
    break;
  case 30:
    cudaCores = prop.multiProcessorCount * 192;
    resThreadsPerMulti = 2048;
    break;
  default:   // 1x cards
    cudaCores = prop.multiProcessorCount * 8;
    resThreadsPerMulti = 768;
    break;
  }
  // Take a guess on the future
  if(computeCapability > 30) {
    cudaCores = prop.multiProcessorCount * 192;
    resThreadsPerMulti = 2048;
  }
  cudaMaxThreads = prop.multiProcessorCount * resThreadsPerMulti * 10;

  cout << endl;
  cout << "Cuda capable device found, device " << cudaDevice << ": " << prop.name << endl;
  cout << "                 Compute capability: " << prop.major << "." << prop.minor << endl;
  cout << "                       Total memory: " << prop.totalGlobalMem / (1024 * 1024) << " Mb" << endl;
  cout << "                    MultiProcessors: " << prop.multiProcessorCount << endl;
  cout << "     Res.threads per MultiProcessor: " << resThreadsPerMulti << endl;
  cout << "               Max resident threads: " << prop.multiProcessorCount* resThreadsPerMulti << endl;
  cout << "                         Cuda cores: " << cudaCores << endl;
  cout << "                         Clock rate: " << float(prop.clockRate) / 1000000.0 << " GHz" << endl << endl;

  cudaSetDevice(cudaDevice);

  /*cudaInit(0);*/
  /*if(cudaDevice > 0)*/
  /*cudaCtxDestroy(ctx);*/
  /*cudaCtxCreate(&ctx, CU_CTX_SCHED_YIELD, cudaDevice);*/

  size_t free = 0, total = 0;
  cudaMemGetInfo(&free, &total);
  free /= MByte;
  total /= MByte;

  cout << "CUDA initialized, " << free << "MB memory available from " << total << " total" << endl << endl;

  setHoldMem(0);
  return (0);
}

// Re-initialize?
int reinitCuda()
{
  cudaThreadExit();
  cudaSetDevice(cudaDevice);
  return (0);
}

// Check for Cuda errors
int checkCudaError(const string& msg)
{
  cudaThreadSynchronize();
  cudaError_t cuerr = cudaGetLastError();
  if(cuerr != cudaSuccess)
    throw(string(msg + " Cuda error string:" + string(cudaGetErrorString(cuerr))));
  // return errMsg("Cuda error:" + string(cudaGetErrorString(cuerr)) + " - " + msg);
  return 0;
}

// Calculate optimal threads, start small
uint getThreadCount(uint threadPos, uint totThreads)
{
  static clock_t starttime;
  static uint numThreads;
  static float maxTime = 0;

  // Start with same number of threads as cores
  if(threadPos == 0) {
    numThreads = cudaCores;
    maxTime = 0;
    starttime = clock();
  } else {
    clock_t currtime = clock();
    float secs = float(currtime - starttime) / CLOCKS_PER_SEC;
#ifdef WIN32
    if(secs > .2)
      Sleep(100);
    else
      Sleep(1);
#else
    if(secs > .2)
      usleep(100000);
    else
      usleep(100);
#endif
    starttime = currtime;
    if(secs < .02)
      numThreads *= 2;
    else if(secs > .05)
      numThreads /= 2;
    if(numThreads < (uint)cudaCores)
      numThreads = cudaCores;
    if(numThreads > (uint)cudaMaxThreads)
      numThreads = cudaMaxThreads;

    if(secs > maxTime) {
      maxTime = secs;
      //      cout << "getThreadCount:Threads:" << numThreads << " Prev MaxTime:" << maxTime << endl;
    }
  }
  return (min(numThreads, totThreads - threadPos));
}

#else // THRUST_BACKEND_CUDA

// We'll assume we always have 2GB
size_t memAvail()
{
  return 2*1024*MByte;
}

int checkCudaError(const string& msg)
{
  return 0;
}

#ifndef THRUST_BACKEND_CUDA
#  ifdef THRUST_BACKEND_OMP
uint getThreadCount(uint threadPos, uint totThreads)
{
  return omp_get_num_procs();
}
#  else
uint getThreadCount(uint threadPos, uint totThreads)
{
  return 1;
}
#  endif
#endif

#endif // THRUST_BACKEND_CUDA


// Memory for user processes
size_t userMem()
{
  size_t avail = memAvail();

  // Allow 10% buffer
  return (avail - avail / 10);
  // return(holdMemSize - holdMemSize/10);
}

// Reduce available user mem
size_t memLeft(size_t memrq, size_t roomrq, size_t mem)
{
  if(memrq + roomrq > mem)
    throw(string("memLeft::Not enough cuda memory to allocate min/max list"));
  return (mem - memrq);
}


// Color gradient along Z axis only
struct colorGradOP {
  const float div;

  Point3u size;
  Point3u stride;
  ushort* Src;
  ushort* Dst;

  colorGradOP(float _div, DVecUS* _Src, DVecUS* _Dst)
    : div(_div)
    , Src(devP(*_Src))
    , Dst(devP(*_Dst))
  {
  }

  void setSizes(const Point3u&, const Point3u& _size)
  {
    size = _size;
    stride = getStride(size);
  }

  __device__ __host__ void operator()(uint idx) const
  {
    int x, y;
    getXY(idx, size, x, y);
    float grad, nextgrad, prevgrad = 0;

    size_t zIdx = offset(x, y, 0, size);
    for(int z = 0; z < size.z(); ++z, zIdx += stride.z()) {
      if(z < size.z() - 1)
        nextgrad = float(Src[zIdx] - Src[zIdx + stride.z()]) / div;
      else
        nextgrad = 0;
      grad = (nextgrad + prevgrad) / div;
      prevgrad = nextgrad;

      grad *= grad;
      if(grad > 0xFFFF)
        grad = 0xFFFF;
      Dst[zIdx] = (ushort)(grad);
    }
  }
};

// Edge detect along Y axis only, image is flipped
struct edgeDetectOP {
  const ushort low;
  const ushort high;
  const float mult;
  const ushort fillval;

  Point3u size;
  Point3u stride;
  ushort* Src;
  ushort* Dst;

  edgeDetectOP(ushort _low, ushort _high, float _mult, ushort _fillval, DVecUS* _Src, DVecUS* _Dst)
    : low(_low)
    , high(_high)
    , mult(_mult)
    , fillval(_fillval)
    , Src(devP(*_Src))
    , Dst(devP(*_Dst))
  {
  }

  void setSizes(const Point3u&, const Point3u& _size)
  {
    size = _size;
    stride = getStride(size);
  }

  __device__ __host__ void operator()(uint idx) const
  {
    int x, z;
    getXZ(idx, size, x, z);

    int count = 0;
    float avg = 0;
    size_t yIdx = offset(x, 0, z, size);
    for(int y = 0; y < size.y(); ++y, yIdx += stride.y()) {
      uint s = Src[yIdx];
      if(s >= low) {
        count++;
        avg += s;
      }
    }
    int thresh = high;
    if(count > 0) {
      avg /= count;
      if(avg < high)
        thresh = (int)avg;
    }

    int found = 0;
    yIdx = offset(x, size.y() - 1, z, size);
    for(int y = size.y() - 1; y >= 0; --y, yIdx -= stride.y()) {
      if(found == 0 && Src[yIdx] >= thresh)
        found = 1;
      if(found == 1)
        Dst[yIdx] = fillval;
      else
        Dst[yIdx] = 0;
    }
  }
};

// Clip stack to clip planes
struct clipStackOP {
  const Point3f step;
  const Point3f shift;
  const Point3u clipDo;
  Point4f* pn;

  Point3u base;
  Point3u size;
  Point3u stride;
  ushort* Src;
  ushort* Dst;

  clipStackOP(const Point3f& _step, const Point3f& _shift, const Point3u& _clipDo, DVec4F& _pn, DVecUS* _Src,
              DVecUS* _Dst)
    : step(_step)
    , shift(_shift)
    , clipDo(_clipDo)
    , pn(devP(_pn))
    , Src(devP(*_Src))
    , Dst(devP(*_Dst))
  {
  }

  void setSizes(const Point3u& _base, const Point3u& _size)
  {
    base = _base;
    size = _size;
    stride = getStride(size);
  }

  __device__ __host__ void operator()(uint idx) const
  {
    int x, y;
    getXY(idx, size, x, y);

    // Copy && clip
    size_t zIdx = offset(x, y, 0, size);
    for(int z = 0; z < size.z(); z++, zIdx += stride.z()) {
      Point3f wp = imageToWorld(base + Point3u(x, y, z), step, shift);
      if(clipPoint(wp, clipDo, &pn[0]))
        Dst[zIdx] = 0;
      else
        Dst[zIdx] = Src[zIdx];
    }
  }
};

// Calculate mask contained by a volume
struct insideMeshOP {
  const Point3f step;
  const Point3f shift;
  const Point3f* pts;
  const Point3u* tris;
  const Point3f* nrmls;
  const Point3u imgBase;
  const uint* gridIdx;
  const uint* gridCnt;
  const uint* gridTris;
  const Point3u gridSize;
  const Point3u gridPix;

  Point3u size;
  Point3u base;
  Point3u stride;
  ushort* Dst;

  insideMeshOP(const Point3f& _step, const Point3f& _shift, const Point3f* _pts, const Point3u* _tris,
               const Point3f* _nrmls, const Point3u& _imgBase, const uint* _gridIdx, const uint* _gridCnt,
               const uint* _gridTris, const Point3u _gridSize, const Point3u& _gridPix, DVecUS& _Dst)
    : step(_step)
    , shift(_shift)
    , pts(_pts)
    , tris(_tris)
    , nrmls(_nrmls)
    , imgBase(_imgBase)
    , gridIdx(_gridIdx)
    , gridCnt(_gridCnt)
    , gridTris(_gridTris)
    , gridSize(_gridSize)
    , gridPix(_gridPix)
    , Dst(devP(_Dst))
  {
  }

  void setSizes(const Point3u& _base, const Point3u& _size)
  {
    base = _base;
    size = _size;
    stride = getStride(size);
  }

  __device__ __host__ int rayXTriangleIntersect(const Point3f& pt, const Point3f& t0, const Point3f& t1,
                                                const Point3f& t2, const Point3f& n, Point3f& intp)
  {
    // This routine is optimized for axis aligned rays in X direction
    if(n.x() == 0)     // triangle lies in yz plane
      return 0;

    Point3f w0 = pt - t0;
    float a = -n * w0;
    float r = a / n.x();
    if(r < 0.0f)      // ray goes away from triangle
      return 0;       // => no intersect

    intp = pt;
    intp.x() += r;     // intersect point of ray and plane

    // check if intersect point inside triangle
    Point3f u = t1 - t0;
    Point3f v = t2 - t0;
    float uu, uv, vv, wu, wv, d;
    uu = u * u;
    uv = u * v;
    vv = v * v;
    Point3f w = intp - t0;
    wu = w * u;
    wv = w * v;
    d = uv * uv - uu * vv;

    // get and test parametric coords
    float s, t;
    s = (uv * wv - vv * wu) / d;
    if(s < -FLT_EPSILON || s > 1.0f + FLT_EPSILON)     // outside
      return 0;
    t = (uv * wu - uu * wv) / d;
    if(t < -FLT_EPSILON || (s + t) > 1.0f + FLT_EPSILON)     // outside
      return 0;

    return 1;     // inside
  }

  __device__ __host__ void operator()(uint idx)
  {
    Point3f intp;
    int y, z;
    getYZ(idx, size, y, z);

    // Look for intersections down a column of pixels
    int x = 0;
    Point3f pt = imageToWorld(imgBase + base + Point3u(x, y, z), step, shift);
    pt.x() -= 100;

    // Grid information
    int yshift = base.y();
    int zshift = base.z();

    Point3u gridPos(0, (yshift + y) / gridPix.y(), (zshift + z) / gridPix.z());
    int idxG = gridPos.z() * gridSize.y() + gridPos.y();
    do {
      // Loop through triangle list to find closest triangle along x axis
      Point3f minpt;
      float mindist = FLT_MAX;
      bool inside = false;
      int idxT = gridIdx[idxG];
      for(uint i = 0; i < gridCnt[idxG]; ++i, ++idxT) {
        int idxTri = gridTris[idxT];
        Point3f t0 = pts[tris[idxTri].x()];
        Point3f t1 = pts[tris[idxTri].y()];
        Point3f t2 = pts[tris[idxTri].z()];
        Point3f n = nrmls[idxTri];

        // Skip if triangle is behind
        float maxx = max(t0.x(), max(t1.x(), t2.x()));
        if(maxx < pt.x())
          continue;

        // See if we already have a closer triangle
        float minx = min(t0.x(), min(t1.x(), t2.x()));
        if(minx - pt.x() > mindist)
          continue;

        // Do intersection
        if(rayXTriangleIntersect(pt, t0, t1, t2, n, intp)) {
          float dist = intp.x() - pt.x();
          if(dist > 0 and dist < mindist) {
            mindist = dist;
            minpt = intp;
            inside = (n.x() > 0 ? true : false);
          }
        }
      }

      // If no triangle found exit
      if(mindist == FLT_MAX)
        return;
      if(x == 0)
        pt = imageToWorld(imgBase + base + Point3u(x, y, z), step, shift);
      size_t xIdx = offset(x, y, z, size);
      while(pt.x() <= minpt.x() and x < size.x()) {
        if(inside)
          Dst[xIdx++] = 1;
        pt = imageToWorld(imgBase + base + Point3u(++x, y, z), step, shift);
      }
    } while(x < size.x());
  }
};

// Calculate mask contained by a volume
struct nearMeshOP {
  const Point3f step;
  const Point3f shift;
  const Point3f* pts;
  const Point3f* nrmls;
  const Point3u imgBase;
  const uint* gridIdx;
  const uint* gridCnt;
  const uint* gridPts;
  const Point3u gridSize;
  const Point3u gridPix;
  const float minDist;
  const float maxDist;

  Point3u size;
  Point3u base;
  Point3u stride;
  ushort* Dst;

  nearMeshOP(const Point3f& _step, const Point3f& _shift, const Point3f* _pts, const Point3f* _nrmls,
             const Point3u& _imgBase, const uint* _gridIdx, const uint* _gridCnt, const uint* _gridPts,
             const Point3u _gridSize, const Point3u& _gridPix, float _minDist, float _maxDist, DVecUS& _Dst)
    : step(_step)
    , shift(_shift)
    , pts(_pts)
    , nrmls(_nrmls)
    , imgBase(_imgBase)
    , gridIdx(_gridIdx)
    , gridCnt(_gridCnt)
    , gridPts(_gridPts)
    , gridSize(_gridSize)
    , gridPix(_gridPix)
    , minDist(_minDist)
    , maxDist(_maxDist)
    , Dst(devP(_Dst))
  {
  }

  void setSizes(const Point3u& _base, const Point3u& _size)
  {
    base = _base;
    size = _size;
    stride = getStride(size);
  }

  __device__ __host__ void operator()(uint idx)
  {
    Point3f intp;
    int x, y, z;
    getXYZ(idx, size, x, y, z);

    // Calculate distance
    Point3f pt = imageToWorld(imgBase + base + Point3u(x, y, z), step, shift);

    // Grid information
    Point3u gridPos = divide(base + Point3u(x, y, z), gridPix);
    size_t idxG = offset(gridPos, gridSize);

    // Loop through triangle list to find closest triangle
    float dist = FLT_MAX;
    bool inside;
    int idxT = gridIdx[idxG];
    for(uint i = 0; i < gridCnt[idxG]; ++i, ++idxT) {
      Point3f p = pts[gridPts[idxT]];
      Point3f n = nrmls[gridPts[idxT]];

      float d = (p - pt).norm();
      if(d < dist) {
        dist = d;
        inside = ((p - pt) * n >= 0);
      }
    }
    // Simple case, single distance from mesh
    if(!inside)
      dist = -dist;
    if(dist >= minDist and dist <= maxDist)
      Dst[offset(x, y, z, size)] = 1;
  }
};

// Find min/max pixels for each slice
struct pixelFindMinMaxXOP {
  const Point3u base;
  const Point3u size;
  const Point3u imgSize;
  const Point3f step;
  const Point3f shift;
  const Point3f p;
  const Point3f pv;
  const Point2f width;

  Point2i* Dmin;
  Point2i* Dmax;

  pixelFindMinMaxXOP(const Point3u& _base, const Point3u& _size, const Point3u& _imgSize, const Point3f& _step,
                     const Point3f& _shift, const Point3f& _p, const Point3f& _pv, const Point2f& _width,
                     Point2i* _Dmin, Point2i* _Dmax)
    : base(_base)
    , size(_size)
    , imgSize(_imgSize)
    , step(_step)
    , shift(_shift)
    , p(_p)
    , pv(_pv)
    , width(_width)
    , Dmin(_Dmin)
    , Dmax(_Dmax)
  {
  }

  __device__ __host__ void operator()(const uint x) const
  {
    Point3f pint;     // intersect point
    Point3f xp = imageToWorld(Point3u(base.x() + x, 0, 0), step, shift);
    float s;

    if(planeLineIntersect(xp, Point3f(1.0f, 0.0f, 0.0f), p, p + pv, s, pint)) {
      xp = pint;
      xp.z() -= width.x();
      xp.y() -= width.y();
      Point3i xi = worldToImage(xp, step, shift);
      Dmin[x].x() = trim(xi.z() - 1, int(base.z()), int(base.z() + size.z()));
      Dmin[x].y() = trim(xi.y() - 1, int(base.y()), int(base.y() + size.y()));

      xp = pint;
      xp.z() += width.x();
      xp.y() += width.y();
      xi = worldToImage(xp, step, shift);
      Dmax[x].x() = trim(xi.z() + 1, int(base.z()), int(base.z() + size.z()));
      Dmax[x].y() = trim(xi.y() + 1, int(base.y()), int(base.y() + size.y()));
    } else {
      Dmin[x] = Point2i(0, 0);
      Dmax[x] = Point2i(0, 0);
    }
  }
};

// Find min/max pixels for each slice
struct pixelFindMinMaxYOP {
  const Point3u base;
  const Point3u size;
  const Point3u imgSize;
  const Point3f step;
  const Point3f shift;
  const Point3f p;
  const Point3f pv;
  const Point2f width;

  Point2i* Dmin;
  Point2i* Dmax;

  pixelFindMinMaxYOP(const Point3u& _base, const Point3u& _size, const Point3u& _imgSize, const Point3f& _step,
                     const Point3f& _shift, const Point3f& _p, const Point3f& _pv, const Point2f& _width,
                     Point2i* _Dmin, Point2i* _Dmax)
    : base(_base)
    , size(_size)
    , imgSize(_imgSize)
    , step(_step)
    , shift(_shift)
    , p(_p)
    , pv(_pv)
    , width(_width)
    , Dmin(_Dmin)
    , Dmax(_Dmax)
  {
  }

  __device__ __host__ void operator()(const uint y) const
  {
    Point3f pint;     // intersect point
    Point3f yp = imageToWorld(Point3u(0, base.y() + y, 0), step, shift);
    float s;

    if(planeLineIntersect(yp, Point3f(0.0f, 1.0f, 0.0f), p, p + pv, s, pint)) {
      yp = pint;
      yp.x() -= width.x();
      yp.z() -= width.y();
      Point3i yi = worldToImage(yp, step, shift);
      Dmin[y].x() = trim(yi.x() - 1, int(base.x()), int(base.x() + size.x()));
      Dmin[y].y() = trim(yi.z() - 1, int(base.z()), int(base.z() + size.z()));

      yp = pint;
      yp.x() += width.x();
      yp.z() += width.y();
      yi = worldToImage(yp, step, shift);
      Dmax[y].x() = trim(yi.x() + 1, int(base.x()), int(base.x() + size.x()));
      Dmax[y].y() = trim(yi.z() + 1, int(base.z()), int(base.z() + size.z()));
    } else {
      Dmin[y] = Point2i(0, 0);
      Dmax[y] = Point2i(0, 0);
    }
  }
};

// Find min/max pixels for each slice
struct pixelFindMinMaxZOP {
  const Point3u base;
  const Point3u size;
  const Point3u imgSize;
  const Point3f step;
  const Point3f shift;
  const Point3f p;
  const Point3f pv;
  const Point2f width;

  Point2i* Dmin;
  Point2i* Dmax;

  pixelFindMinMaxZOP(const Point3u& _base, const Point3u& _size, const Point3u& _imgSize, const Point3f& _step,
                     const Point3f& _shift, const Point3f& _p, const Point3f& _pv, const Point2f& _width,
                     Point2i* _Dmin, Point2i* _Dmax)
    : base(_base)
    , size(_size)
    , imgSize(_imgSize)
    , step(_step)
    , shift(_shift)
    , p(_p)
    , pv(_pv)
    , width(_width)
    , Dmin(_Dmin)
    , Dmax(_Dmax)
  {
  }
  __device__ __host__ void operator()(const uint z) const
  {
    Point3f pint;     // intersect point
    Point3f zp = imageToWorld(Point3u(0, 0, base.z() + z), step, shift);
    float s;

    if(planeLineIntersect(zp, Point3f(0.0f, 0.0f, 1.0f), p, p + pv, s, pint)) {
      zp = pint;
      zp.x() -= width.x();
      zp.y() -= width.y();
      Point3i zi = worldToImage(zp, step, shift);
      Dmin[z].x() = trim(zi.x() - 1, int(base.x()), int(base.x() + size.x()));
      Dmin[z].y() = trim(zi.y() - 1, int(base.y()), int(base.y() + size.y()));

      zp = pint;
      zp.x() += width.x();
      zp.y() += width.y();
      zi = worldToImage(zp, step, shift);
      Dmax[z].x() = trim(zi.x() + 1, int(base.x()), int(base.x() + size.x()));
      Dmax[z].y() = trim(zi.y() + 1, int(base.y()), int(base.y() + size.y()));

    } else {
      Dmin[z] = Point2i(0, 0);
      Dmax[z] = Point2i(imgSize.x(), imgSize.y());
    }
  }
};

// Find min/max pixels for each slice
struct pixelEditXOP {
  const uint dbase;
  const uint gbase;
  const Point3u size;
  const Point3u imgSize;
  const Point3f step;
  const Point3f shift;
  const Point3f p;
  const Point3f pv;
  const float radius;
  const ushort color;
  const Point3u clipDo;
  const Point4f* pn;

  const Point2i* Dmin;
  const Point2i* Dmax;
  ushort* Dpix;

  pixelEditXOP(uint _dbase, uint _gbase, const Point3u& _size, const Point3u& _imgSize, const Point3f& _step,
               const Point3f& _shift, const Point3f& _p, const Point3f& _pv, float _radius, ushort _color,
               const Point3u& _clipDo, const Point4f* _pn, const Point2i* _Dmin, const Point2i* _Dmax, ushort* _Dpix)
    : dbase(_dbase)
    , gbase(_gbase)
    , size(_size)
    , imgSize(_imgSize)
    , step(_step)
    , shift(_shift)
    , p(_p)
    , pv(_pv)
    , radius(_radius)
    , color(_color)
    , clipDo(_clipDo)
    , pn(_pn)
    , Dmin(_Dmin)
    , Dmax(_Dmax)
    , Dpix(_Dpix)
  {
  }

  __device__ __host__ void operator()(const uint idx) const
  {
    uint yb = idx % size.y();
    uint zb = idx / size.y();
    for(int x = 0; x < (int)size.x(); ++x) {
      uint dx = x + dbase;
      uint z = zb + Dmin[dx].x();
      uint y = yb + Dmin[dx].y();
      if((int)z >= Dmax[dx].x() || (int)y >= Dmax[dx].y())
        continue;

      bool erase = true;
      Point3f wp = imageToWorld(Point3u(gbase + dx, y, z), step, shift);
      if(clipPoint(wp, clipDo, pn))
        erase = false;

      // Test if within radius
      if(distLinePoint(p - pv, p + pv, wp, false) >= radius)
        erase = false;

      // Save result
      if(erase)
        Dpix[offset(yb, zb, x, size.y(), size.z())] = color;
    }
  }
};

// Find min/max pixels for each slice
struct pixelEditYOP {
  const uint dbase;
  const uint gbase;
  const Point3u size;
  const Point3u imgSize;
  const Point3f step;
  const Point3f shift;
  const Point3f p;
  const Point3f pv;
  const float radius;
  const ushort color;
  const Point3u clipDo;
  const Point4f* pn;

  const Point2i* Dmin;
  const Point2i* Dmax;
  ushort* Dpix;

  pixelEditYOP(uint _dbase, uint _gbase, const Point3u& _size, const Point3u& _imgSize, const Point3f& _step,
               const Point3f& _shift, const Point3f& _p, const Point3f& _pv, float _radius, ushort _color,
               const Point3u& _clipDo, const Point4f* _pn, const Point2i* _Dmin, const Point2i* _Dmax, ushort* _Dpix)
    : dbase(_dbase)
    , gbase(_gbase)
    , size(_size)
    , imgSize(_imgSize)
    , step(_step)
    , shift(_shift)
    , p(_p)
    , pv(_pv)
    , radius(_radius)
    , color(_color)
    , clipDo(_clipDo)
    , pn(_pn)
    , Dmin(_Dmin)
    , Dmax(_Dmax)
    , Dpix(_Dpix)
  {
  }

  __device__ __host__ void operator()(const uint idx) const
  {
    uint xb = idx % size.x();
    uint zb = idx / size.x();
    for(int y = 0; y < (int)size.y(); y++) {
      uint dy = dbase + y;
      uint x = xb + Dmin[dy].x();
      uint z = zb + Dmin[dy].y();
      if((int)x >= Dmax[dy].x() || (int)z >= Dmax[dy].y())
        continue;

      bool erase = true;
      Point3f wp = imageToWorld(Point3u(x, gbase + dy, z), step, shift);
      if(clipPoint(wp, clipDo, pn))
        erase = false;

      // Test if within radius
      if(distLinePoint(p - pv, p + pv, wp, false) >= radius)
        erase = false;

      // Save result
      if(erase)
        Dpix[offset(xb, zb, y, size.x(), size.z())] = color;
    }
  }
};

// Find min/max pixels for each slice
struct pixelEditZOP {
  const uint dbase;
  const uint gbase;
  const Point3u size;
  const Point3u imgSize;
  const Point3f step;
  const Point3f shift;
  const Point3f p;
  const Point3f pv;
  const float radius;
  const ushort color;
  const Point3u clipDo;
  const Point4f* pn;

  Point2i* Dmin;
  Point2i* Dmax;
  ushort* Dpix;

  pixelEditZOP(uint _dbase, uint _gbase, const Point3u& _size, const Point3u& _imgSize, const Point3f& _step,
               const Point3f& _shift, const Point3f& _p, const Point3f& _pv, float _radius, ushort _color,
               const Point3u& _clipDo, const Point4f* _pn, Point2i* _Dmin, Point2i* _Dmax, ushort* _Dpix)
    : dbase(_dbase)
    , gbase(_gbase)
    , size(_size)
    , imgSize(_imgSize)
    , step(_step)
    , shift(_shift)
    , p(_p)
    , pv(_pv)
    , radius(_radius)
    , color(_color)
    , clipDo(_clipDo)
    , pn(_pn)
    , Dmin(_Dmin)
    , Dmax(_Dmax)
    , Dpix(_Dpix)
  {
  }

  __device__ __host__ void operator()(uint idx) const
  {
    uint xb = idx % size.x();
    uint yb = idx / size.x();
    for(int z = 0; z < (int)size.z(); z++) {
      uint dz = z + dbase;
      uint x = xb + Dmin[dz].x();
      uint y = yb + Dmin[dz].y();
      if((int)x >= Dmax[dz].x() || (int)y >= Dmax[dz].y())
        continue;

      bool erase = true;
      // Test against clip planes
      Point3f wp = imageToWorld(Point3u(x, y, gbase + dz), step, shift);
      if(clipPoint(wp, clipDo, pn))
        erase = false;

      // Test if within radius
      if(distLinePoint(p - pv, p + pv, wp, false) >= radius)
        erase = false;

      // Save result
      if(erase)
        Dpix[offset(xb, yb, z, size.x(), size.y())] = color;
    }
  }
};
} // namespace cuda
} // namespace lgx

using namespace lgx;
using namespace lgx::cuda;

// ---------- Externally exported functions ----------
// Initialize cuda
#ifdef THRUST_BACKEND_CUDA
CU_DLLEXPORT int initGPU() {
  return initCuda();
}

CU_DLLEXPORT void cudaVersionGPU(int* major, int* minor)
{
  cudaVersion(major, minor);
}

CU_DLLEXPORT int setHoldMemGPU(uint max) {
  return setHoldMem(max);
}

CU_DLLEXPORT int getHoldMemGPU() {
  return holdMemSize / MByte;
}

// Return available memory
CU_DLLEXPORT size_t memAvailGPU() {
  return memAvail();
}

CU_DLLEXPORT void freeMemGPU() {
  freeMem();
}

CU_DLLEXPORT void holdMemGPU() {
  holdMem();
}

#else

CU_DLLEXPORT int initGPU() { return 1; }

CU_DLLEXPORT void cudaVersionGPU(int* major, int* minor) { }

CU_DLLEXPORT int setHoldMemGPU(uint max) {
  return 0;
}

CU_DLLEXPORT int getHoldMemGPU() {
  return 0;
}

// Return available memory
CU_DLLEXPORT size_t memAvailGPU() {
  return 0;
}

CU_DLLEXPORT void freeMemGPU() { }

CU_DLLEXPORT void holdMemGPU() { }

#endif

// Average voxels (separable)
CU_DLLEXPORT int averageGPU(const Point3u& imgSize, const Point3u& radius, const HVecUS& srcdata, HVecUS& dstdata)
{
#ifdef __MINGW32__
  try {
#endif
  AverageKernel op = AverageKernel();
  return processSepOP(op, op, op, imgSize, radius, srcdata, dstdata);
#ifdef __MINGW32__
  } catch(...) {
      fprintf(stderr, "Error, exception caught");
      return 1;
  }
#endif
}

// Dilate voxels (separable)
CU_DLLEXPORT int dilateGPU(const Point3u& imgSize, const Point3u& radius, const HVecUS& srcdata, HVecUS& dstdata)
{
#ifdef __MINGW32__
  try {
#endif
  DilateKernel op = DilateKernel();
  return processSepOP(op, op, op, imgSize, radius, srcdata, dstdata);
#ifdef __MINGW32__
  } catch(...) {
      fprintf(stderr, "Error, exception caught");
      return 1;
  }
#endif
}

// Erode voxels (separable)
CU_DLLEXPORT int erodeGPU(const Point3u& imgSize, const Point3u& radius, const HVecUS& srcdata, HVecUS& dstdata, bool byLabel)
{
#ifdef __MINGW32__
  try {
#endif
  if(byLabel) {
    ErodeLabelsKernel op = ErodeLabelsKernel();
    return processSepOP(op, op, op, imgSize, radius, srcdata, dstdata);
  } else {
    ErodeKernel op = ErodeKernel();
    return processSepOP(op, op, op, imgSize, radius, srcdata, dstdata);
  }
#ifdef __MINGW32__
  } catch(...) {
      fprintf(stderr, "Error, exception caught");
      return 1;
  }
#endif
}

// Guassian blur voxels (separable)
CU_DLLEXPORT int gaussianBlurGPU(const Point3u& imgSize, const Point3f& imgStep, const Point3f& sigma, const HVecUS& srcdata,
                                 HVecUS& dstdata)
{
#ifdef __MINGW32__
  try {
#endif
  Point3u radius(0, 0, 0);
  DVecF dMask[3];

  // Setup masks
  for(int i = 0; i < 3; ++i) {
    HVecF mask;
    getGaussianMask1D(imgStep[i], sigma[i], mask, radius[i]);
    normalizeMaskScale(mask);
    dMask[i] = mask;
  }
  return processSepOP(MaskKernel(devP(dMask[0])), MaskKernel(devP(dMask[1])), MaskKernel(devP(dMask[2])), imgSize,
                      radius, srcdata, dstdata);
#ifdef __MINGW32__
  } catch(...) {
      fprintf(stderr, "Error, exception caught");
      return 1;
  }
#endif
}

// Sharpen voxels (separable)
CU_DLLEXPORT int sharpenGPU(const Point3u& imgSize, const Point3f& imgStep, const Point3f& sigma, const float amount,
                            const HVecUS& srcdata, HVecUS& dstdata)
{
#ifdef __MINGW32__
  try {
#endif
  Point3u radius(0, 0, 0);
  DVecF dMask[3];

  // Setup masks
  for(int i = 0; i < 3; ++i) {
    HVecF mask;
    getGaussianMask1D(imgStep[i], sigma[i], mask, radius[i]);
    normalizeMaskScale(mask);

    if(radius[i] > 0) {
      // weight center, and flip sign
      float normalize = 0;
      for(int j = 0; j < (int)mask.size(); ++j)
        if(j != radius[i]) {
          normalize += mask[j];
          mask[j] *= -amount;
          normalize -= mask[j];
        }
      mask[radius[i]] += normalize;
    }
    dMask[i] = mask;
  }
  return processSepOP(MaskKernel(devP(dMask[0])), MaskKernel(devP(dMask[1])), MaskKernel(devP(dMask[2])), imgSize,
                      radius, srcdata, dstdata);
#ifdef __MINGW32__
  } catch(...) {
      fprintf(stderr, "Error, exception caught");
      return 1;
  }
#endif
}

// Apply a separable kernel
CU_DLLEXPORT int applyKernelGPU(const Point3u& imgSize, const HVecF& kernelX, const HVecF& kernelY, const HVecF& kernelZ,
                   const HVecUS& srcdata, HVecUS& dstdata)
{
#ifdef __MINGW32__
  try {
#endif
  Point3u radius((kernelX.size() - 1) / 2, (kernelY.size() - 1) / 2, (kernelZ.size() - 1) / 2);
  DVecF dKernelX(kernelX);
  DVecF dKernelY(kernelY);
  DVecF dKernelZ(kernelZ);

  return processSepOP(MaskKernel(devP(dKernelX)), MaskKernel(devP(dKernelY)), MaskKernel(devP(dKernelZ)), imgSize,
                      radius, srcdata, dstdata);
#ifdef __MINGW32__
  } catch(...) {
      fprintf(stderr, "Error, exception caught");
      return 1;
  }
#endif
}

// Color gradient (in Z only)
CU_DLLEXPORT int colorGradGPU(const Point3u& imgSize, float div, const HVecUS& srcData, HVecUS& dstData)
{
#ifdef __MINGW32__
  try {
#endif
  HVecUS Hsrc(srcData);
  DVecUS Dsrc, Ddst;
  processOP proc(imgSize, 1, sizeof(ushort) * 2);
  do {
    proc.write(Hsrc, Dsrc);
    proc.alloc(Ddst);
    if(proc.launch(colorGradOP(div, &Dsrc, &Ddst), XY))
      return 1;
    proc.read(Ddst, dstData);
  } while(proc.next());

  return 0;
#ifdef __MINGW32__
  } catch(...) {
      fprintf(stderr, "Error, exception caught");
      return 1;
  }
#endif
}

// Normalized edge detect
CU_DLLEXPORT int edgeDetectGPU(const Point3u& imgSize, ushort lowthresh, ushort highthresh, float mult, ushort fillval, const HVecUS& srcData,
                  HVecUS& dstData)
{
#ifdef __MINGW32__
  try {
#endif
  HVecUS Hsrc(srcData.size()), Hdst(srcData.size());
  // Flip Y and Z axes for processing
  Point3u flipImgSize;
  flipImage(YZ, imgSize, flipImgSize, srcData, Hsrc);

  DVecUS Dsrc, Ddst;
  processOP proc(flipImgSize, 0, sizeof(ushort) * 2);
  do {
    proc.write(Hsrc, Dsrc);
    proc.alloc(Ddst);
    if(proc.launch(edgeDetectOP(lowthresh, highthresh, mult, fillval, &Dsrc, &Ddst), XZ))
      return 1;
    proc.read(Ddst, Hdst);
  } while(proc.next());

  flipImage(YZ, flipImgSize, flipImgSize, Hdst, dstData);

  return 0;
#ifdef __MINGW32__
  } catch(...) {
      fprintf(stderr, "Error, exception caught");
      return 1;
  }
#endif
}

// Clip stack to clipping planes
CU_DLLEXPORT int clipStackGPU(const Point3u& imgSize, const Point3f& step, const Point3f& shift,
                              const Point3u& clipDo, const HVec4F& pn, const HVecUS& srcData, HVecUS& dstData)
{
#ifdef __MINGW32__
  try {
#endif
  // If all clip planes disabled, just copy data
  if(clipDo.x() == 0 && clipDo.y() == 0 && clipDo.z() == 0) {
    dstData = srcData;
    return (0);
  }
  dstData.resize(srcData.size());
  DVec4F Dpn(pn);   // Put clip planes on device
  DVecUS Dsrc, Ddst;
  processOP proc(imgSize, 0, sizeof(ushort) * 2);
  do {
    proc.write(srcData, Dsrc);
    proc.alloc(Ddst);
    if(proc.launch(clipStackOP(step, shift, clipDo, Dpn, &Dsrc, &Ddst), XY))
      return 1;
    proc.read(Ddst, dstData);
  } while(proc.next());

  return 0;
#ifdef __MINGW32__
  } catch(...) {
      fprintf(stderr, "Error, exception caught");
      return 1;
  }
#endif
}

// Find out if pixels are inside mesh (closed), return a list of pixels inside
CU_DLLEXPORT int insideMeshGPU(const Point3u& base, const Point3u& size, const Point3f& step,
                               const Point3f& shift, const HVec3F& pts, const HVec3U& tris,
                               const HVec3F& nrmls, HVecUS& dstData)
{
#ifdef __MINGW32__
  try {
#endif
  if(size_t(size.x()) * size.y() * size.z() <= 0)
    return errMsg("insideMeshGPU:Empty region");

  // Create YZ grid
  const Point3u gridPix(1, 32, 8);
  const Point3u gridSize = divide(size, gridPix) + Point3u(1, 1, 1);
  std::vector<std::set<int> > grid;
  grid.resize(gridSize.y() * gridSize.z());

  for(size_t i = 0; i < tris.size(); i++) {
    Point3f t0 = pts[tris[i].x()];
    Point3f t1 = pts[tris[i].y()];
    Point3f t2 = pts[tris[i].z()];

    // Triangle min max in world coordinates
    Point3f minW = min(t0, min(t1, t2));
    Point3f maxW = max(t0, max(t1, t2));
    // Min max in image coords
    Point3i minI = worldToImage(minW, step, shift) - base;
    Point3i maxI = worldToImage(maxW, step, shift) - base;
    Point3i minG = divide(minI, Point3i(gridPix));
    Point3i maxG = divide(maxI, Point3i(gridPix));
    // Triangle is outside grid
    if(maxG.y() < 0 or minG.y() >= gridSize.y())
      continue;
    if(maxG.z() < 0 or minG.z() >= gridSize.z())
      continue;
    minG = trim(minG, Point3i(0, 0, 0), Point3i(gridSize - Point3u(1, 1, 1)));
    maxG = trim(maxG, Point3i(0, 0, 0), Point3i(gridSize - Point3u(1, 1, 1)));
    // Add triangle to all grid squares
    for(int z = minG.z(); z <= maxG.z(); z++)
      for(int y = minG.y(); y <= maxG.y(); y++)
        grid[z * gridSize.y() + y].insert(i);
  }
  int maxtris = 0;
  int tottris = 0;
  for(size_t i = 0; i < grid.size(); i++) {
    int numtris = grid[i].size();
    tottris += numtris;
    if(maxtris < numtris)
      maxtris = numtris;
  }
  cout << "Grid Size:" << gridSize.y() << "x" << gridSize.z() << " MaxTris:" << maxtris << " TotTris:" << tottris
       << " Tris:" << tris.size() << endl;
  HVecU hGridIdx(grid.size());
  HVecU hGridCnt(grid.size());
  HVecU hGridTris(tottris);
  int idx = 0;
  for(size_t i = 0; i < grid.size(); i++) {
    hGridIdx[i] = idx;
    hGridCnt[i] = grid[i].size();
    std::copy(grid[i].begin(), grid[i].end(), hGridTris.begin() + idx);
    idx += grid[i].size();
  }

  // Check memory, we need room for points, triangles, normals, grid, and at least one slice
  size_t reqmem = (nrmls.size() + pts.size()) * sizeof(Point3f) + tris.size() * sizeof(Point3u)
    + (hGridIdx.size() + hGridCnt.size() + hGridTris.size()) * sizeof(uint);

  if(reqmem + 2 * size.x() * size.y() >= userMem())
    throw(string("InsideMeshGPU::Not enough GPU memory to store triangle list, try increasing CudaHoldMem"));

  // Put points, triangles, normals, and grid on device
  DVec3F dPts(pts);
  DVec3U dTris(tris);
  DVec3F dNrmls(nrmls);
  DVecU dGridIdx(hGridIdx);
  DVecU dGridCnt(hGridCnt);
  DVecU dGridTris(hGridTris);
  DVecUS Ddst;
  checkCudaError("InsideMeshGPU::Allocating device buffers for triangle list.");

  processOP proc(size, 0, sizeof(ushort));
  do {
    proc.alloc(Ddst);
    if(proc.launch(insideMeshOP(step, shift, devP(dPts), devP(dTris), devP(dNrmls), base, devP(dGridIdx),
                                devP(dGridCnt), devP(dGridTris), gridSize, gridPix, Ddst),
                   YZ))
      return 1;
    proc.read(Ddst, dstData);
  } while(proc.next());

  return 0;
#ifdef __MINGW32__
  } catch(...) {
      fprintf(stderr, "Error, exception caught");
      return 1;
  }
#endif
}

// Find out if pixels are inside mesh (closed), and return a mask
CU_DLLEXPORT int nearMeshGPU(const Point3u& base, const Point3u& size, const Point3f& step,
                const Point3f& shift, const HVec3F& pts, const HVec3F& nrmls,
                float minDist, float maxDist, HVecUS& dstData)
{
#ifdef __MINGW32__
  try {
#endif
  if(size_t(size.x()) * size.y() * size.z() <= 0)
    return errMsg("nearMeshGPU:Empty region");
  if(minDist >= maxDist)
    return errMsg("nearMeshGPU:Min Distance must be less than Max Distance");

  // Create XYZ grid
  float distance = max(fabs(minDist), fabs(maxDist));
  Point3f distance3f(distance, distance, distance);
  const Point3u gridPix = max(Point3u(32, 32, 8), Point3u(divide(distance3f, step)));
  const Point3u gridSize = divide(size, gridPix) + Point3u(1, 1, 1);
  std::vector<std::set<int> > grid;
  grid.resize(gridSize.x() * gridSize.y() * gridSize.z());

  for(size_t i = 0; i < pts.size(); i++) {
    Point3f p = pts[i];

    // Point min max in world coordinates
    Point3f minW = p - distance3f;
    Point3f maxW = p + distance3f;
    // Min max in image coords
    Point3i minI = worldToImage(minW, step, shift) - base;
    Point3i maxI = worldToImage(maxW, step, shift) - base;
    // Find grid coordinates
    Point3i minG = divide(minI, Point3i(gridPix));
    Point3i maxG = divide(maxI, Point3i(gridPix));
    // Point influence is outside grid
    if(maxG.x() < 0 or minG.x() >= gridSize.x())
      continue;
    if(maxG.y() < 0 or minG.y() >= gridSize.y())
      continue;
    if(maxG.z() < 0 or minG.z() >= gridSize.z())
      continue;
    minG = trim(minG, Point3i(0, 0, 0), Point3i(gridSize - Point3u(1, 1, 1)));
    maxG = trim(maxG, Point3i(0, 0, 0), Point3i(gridSize - Point3u(1, 1, 1)));
    // Add point to all grid squares
    for(int z = minG.z(); z <= maxG.z(); z++)
      for(int y = minG.y(); y <= maxG.y(); y++)
        for(int x = minG.x(); x <= maxG.x(); x++)
          grid[offset(x, y, z, gridSize)].insert(i);
  }
  int maxpts = 0;
  int totpts = 0;
  for(size_t i = 0; i < grid.size(); i++) {
    int numpts = grid[i].size();
    totpts += numpts;
    if(maxpts < numpts)
      maxpts = numpts;
  }
  cout << "Grid Size:" << gridSize.x() << "x" << gridSize.y() << "x" << gridSize.z() << " MaxPts:" << maxpts
       << " TotPts:" << totpts << " Pts:" << pts.size() << endl;
  HVecU hGridIdx(grid.size());
  HVecU hGridCnt(grid.size());
  HVecU hGridPts(totpts);
  int idx = 0;
  for(size_t i = 0; i < grid.size(); i++) {
    hGridIdx[i] = idx;
    hGridCnt[i] = grid[i].size();
    std::copy(grid[i].begin(), grid[i].end(), hGridPts.begin() + idx);
    idx += grid[i].size();
  }

  // Check memory, we need room for points, triangles, normals, grid, and at least one slice
  size_t reqmem = (nrmls.size() + pts.size()) * sizeof(Point3f)
    + (hGridIdx.size() + hGridCnt.size() + hGridPts.size()) * sizeof(uint);

  if(reqmem + 2 * size.x() * size.y() >= userMem())
    throw(string("NearMeshGPU::Not enough GPU memory to store point list, try increasing CudaHoldMem"));

  // Put points, triangles, normals, and grid on device
  DVec3F dPts(pts);
  DVec3F dNrmls(nrmls);
  DVecU dGridIdx(hGridIdx);
  DVecU dGridCnt(hGridCnt);
  DVecU dGridPts(hGridPts);
  DVecUS Ddst;
  checkCudaError("NearMeshGPU::Allocating device buffers for point list");

  processOP proc(size, 0, sizeof(ushort));
  do {
    proc.alloc(Ddst);
    if(proc.launch(nearMeshOP(step, shift, devP(dPts), devP(dNrmls), base, devP(dGridIdx), devP(dGridCnt),
                              devP(dGridPts), gridSize, gridPix, minDist, maxDist, Ddst),
                   XYZ))
      return 1;
    proc.read(Ddst, dstData);
  } while(proc.next());

  return 0;
#ifdef __MINGW32__
  } catch(...) {
      fprintf(stderr, "Error, exception caught");
      return 1;
  }
#endif
}

// Edit pixel data in 3D, erase, fill, and seed
CU_DLLEXPORT int pixelEditGPU(const Point3u& base, const Point3u& size, const Point3u& imgSize,
                              const Point3f& step, const Point3f& shift, const Point3f& p,
                              const Point3f& pv, float radius, ushort color, const Point3u& clipDo,
                              const HVec4F& pn, HVecUS& data)
{
#ifdef __MINGW32__
  try {
#endif
  bool err = 0;

  // Check bounds
  if(checkBounds(imgSize, base, size))
    throw(string("Bounds error (or empty)"));
  else
    try {
      // Max image coordinate
      size_t imgMax = size_t(imgSize.x()) * imgSize.y() * imgSize.z();

      // Declare device vectors
      DVec4F Dpn(pn);       // put clip planes on device
      checkCudaError("Allocating buffer for clipping planes");

      // Favor Z direction
      float vxy = Point2f(pv.x(), pv.y()).norm();
      if(vxy < .1 || fabs(pv.z() / vxy) > .4) {       // Process in z direction
        // Calculate memory
        size_t mem = memLeft(size.z() * 2 * sizeof(Point2i), 4 * MByte, userMem());

        // Allocate slice min/max vectors
        DVec2I Dmin(size.z()), Dmax(size.z());
        checkCudaError("Allocating min/max device vectors");

        // Cross product with plane projected view vector (ie, onto xz && yz)
        Point3f px(pv.x(), 0, pv.z());
        px.normalize();
        Point3f py(0, pv.y(), pv.z());
        py.normalize();
        Point2f width(radius / sqrt(px.z() * px.z()), radius / sqrt(py.z() * py.z()));

        // Launch threads on section
        thrust::for_each(DCountIter(0), DCountIter(size.z()),
                         pixelFindMinMaxZOP(base, size, Point3u(imgSize), step, shift, p, pv, width, devP(Dmin),
                                            devP(Dmax)));
        checkCudaError("pixelFindMinMaxZOP launch");

        // Get regions to process
        HVec2I Hmin(Dmin), Hmax(Dmax);
        checkCudaError("Min/max copy to host");

        // Find max size && which slices to process
        Point3u sz(0, 0, 0);
        for(int zIdx = 0; zIdx < size.z(); zIdx++) {
          int xsz = Hmax[zIdx].x() - Hmin[zIdx].x();
          int ysz = Hmax[zIdx].y() - Hmin[zIdx].y();

          if(xsz > sz.x())
            sz.x() = xsz;
          if(ysz > sz.y())
            sz.y() = ysz;
        }
        if(sz.x() > 0 && sz.y() > 0) {
          // Collect pixel data
          HVecUS Hpix(sz.x() * sz.y() * size.z());
          size_t idx = 0;
          for(int z = 0; z < size.z(); z++)
            for(int y = Hmin[z].y(); y < Hmin[z].y() + sz.y(); y++)
              for(int x = Hmin[z].x(); x < Hmin[z].x() + sz.x(); x++) {
                if(x < Hmax[z].x() && y < Hmax[z].y()) {
                  size_t gidx = offset(x, y, z + base.z(), imgSize.x(), imgSize.y());
                  if(gidx < imgMax)
                    Hpix[idx] = data[gidx];
                }
                idx++;
              }

          // Process pixels
          size_t zIdx = 0;
          while((sz.z() = getStep(zIdx, size.z(), sz.x() * sz.y(), sizeof(ushort), mem)) > 0) {
            // Put data on device
            DVecUS Dpix(Hpix.begin() + zIdx * sz.x() * sz.y(),
                        Hpix.begin() + (zIdx + sz.z()) * sz.x() * sz.y());
            checkCudaError("Copy pixels to device");

            // Process pixels, || far only erase
            thrust::for_each(DCountIter(0), DCountIter(sz.x() * sz.y()),
                             pixelEditZOP(zIdx, base.z(), Point3u(sz), Point3u(imgSize), step, shift, p, pv,
                                          radius, color, clipDo, devP(Dpn), devP(Dmin), devP(Dmax),
                                          devP(Dpix)));
            checkCudaError("pixEraseZOP launch");

            // Copy data back to host
            thrust::copy(Dpix.begin(), Dpix.end(), Hpix.begin() + zIdx * sz.x() * sz.y());
            checkCudaError("Copying pixels to host");

            zIdx += sz.z();
          }

          // Update results
          idx = 0;
          for(int z = 0; z < size.z(); z++)
            for(int y = Hmin[z].y(); y < Hmin[z].y() + sz.y(); y++)
              for(int x = Hmin[z].x(); x < Hmin[z].x() + sz.x(); x++) {
                if(x < Hmax[z].x() && y < Hmax[z].y()) {
                  size_t gidx = offset(x, y, base.z() + z, imgSize.x(), imgSize.y());
                  if(gidx < imgMax)
                    data[gidx] = Hpix[idx];
                }
                idx++;
              }
        }
      } else if(fabs(pv.y()) > fabs(pv.x())) {       // Process Y direction
        // Calculate memory
        size_t mem = memLeft(size.y() * 2 * sizeof(Point2i), 4 * MByte, userMem());

        // Allocate slice min/max vectors
        DVec2I Dmin(size.y()), Dmax(size.y());
        checkCudaError("Allocating min/max device vectors");

        // Cross product with plane projected view vector (ie, onto xy && zy)
        Point3f px = Point3f(pv.x(), pv.y(), 0).normalize();
        Point3f pz = Point3f(0, pv.y(), pv.z()).normalize();
        Point2f width = Point2f(radius / sqrt(px.y() * px.y()), radius / sqrt(pz.y() * pz.y()));

        // Launch threads on section
        thrust::for_each(DCountIter(0), DCountIter(size.y()),
                         pixelFindMinMaxYOP(base, size, Point3u(imgSize), step, shift, p, pv, width, devP(Dmin),
                                            devP(Dmax)));
        checkCudaError("pixelFindMinMaxYOP launch");

        // Get regions to process
        HVec2I Hmin(Dmin), Hmax(Dmax);
        checkCudaError("Min/max copy to host");

        // Find max size && which slices to process
        Point3u sz(0, 0, 0);
        for(int yIdx = 0; yIdx < size.y(); yIdx++) {
          int xsz = Hmax[yIdx].x() - Hmin[yIdx].x();
          int zsz = Hmax[yIdx].y() - Hmin[yIdx].y();

          if(xsz > sz.x())
            sz.x() = xsz;
          if(zsz > sz.z())
            sz.z() = zsz;
        }
        if(sz.x() > 0 && sz.z() > 0) {
          // Collect pixel data
          HVecUS Hpix(sz.x() * size.y() * sz.z());
          size_t idx = 0;
          for(int y = 0; y < size.y(); y++)
            for(int z = Hmin[y].y(); z < Hmin[y].y() + sz.z(); z++)
              for(int x = Hmin[y].x(); x < Hmin[y].x() + sz.x(); x++) {
                if(x < Hmax[y].x() && z < Hmax[y].y()) {
                  size_t gidx = offset(x, base.y() + y, z, imgSize.x(), imgSize.y());
                  if(gidx < imgMax)
                    Hpix[idx] = data[gidx];
                }
                idx++;
              }

          // Process pixels
          int yIdx = 0;
          while((sz.y() = getStep(yIdx, size.y(), sz.x() * sz.z(), sizeof(ushort), mem)) > 0) {
            // Put data on device
            DVecUS Dpix(Hpix.begin() + yIdx * sz.x() * sz.z(),
                        Hpix.begin() + (yIdx + sz.y()) * sz.x() * sz.z());
            checkCudaError("Copy pixels to device");

            // Process pixels
            thrust::for_each(DCountIter(0), DCountIter(sz.x() * sz.z()),
                             pixelEditYOP(yIdx, base.y(), Point3u(sz), Point3u(imgSize), step, shift, p, pv,
                                          radius, color, clipDo, devP(Dpn), devP(Dmin), devP(Dmax),
                                          devP(Dpix)));
            checkCudaError("pixEraseYOP launch");

            // Copy data back to host
            thrust::copy(Dpix.begin(), Dpix.end(), Hpix.begin() + yIdx * sz.x() * sz.z());
            checkCudaError("Copying pixels to host");

            yIdx += sz.y();
          }

          // Update reuslts
          idx = 0;
          for(int y = 0; y < size.y(); y++)
            for(int z = Hmin[y].y(); z < Hmin[y].y() + sz.z(); z++)
              for(int x = Hmin[y].x(); x < Hmin[y].x() + sz.x(); x++) {
                if(x < Hmax[y].x() && z < Hmax[y].y()) {
                  size_t gidx = offset(x, base.y() + y, z, imgSize.x(), imgSize.y());
                  if(gidx < imgMax)
                    data[gidx] = Hpix[idx];
                }
                idx++;
              }
        }
      } else {       // else do X direction
        // Calculate memory
        size_t mem = memLeft(size.x() * 2 * sizeof(Point2i), 4 * MByte, userMem());

        // Allocate slice min/max vectors
        DVec2I Dmin(size.x()), Dmax(size.x());
        checkCudaError("Allocating min/max device vectors");

        // Cross product with plane projected view vector (i,.e, onto xz && yz)
        Point3f pz = Point3f(pv.x(), 0, pv.z()).normalize();
        Point3f py = Point3f(pv.x(), pv.y(), 0).normalize();
        Point2f width = Point2f(radius / sqrt(pz.x() * pz.x()), radius / sqrt(py.x() * py.x()));

        // Launch threads on section
        thrust::for_each(DCountIter(0), DCountIter(size.x()),
                         pixelFindMinMaxXOP(base, size, Point3u(imgSize), step, shift, p, pv, width, devP(Dmin),
                                            devP(Dmax)));
        checkCudaError("pixelFindMinMaxXOP launch");

        // Get regions to process
        HVec2I Hmin(Dmin), Hmax(Dmax);
        checkCudaError("Min/max copy to host");

        // Find max size && which slices to process
        Point3u sz(0, 0, 0);
        for(int xIdx = 0; xIdx < size.x(); xIdx++) {
          int zsz = Hmax[xIdx].x() - Hmin[xIdx].x();
          int ysz = Hmax[xIdx].y() - Hmin[xIdx].y();

          if(zsz > sz.z())
            sz.z() = zsz;
          if(ysz > sz.y())
            sz.y() = ysz;
        }
        if(sz.y() > 0 && sz.z() > 0) {
          // Collect pixel data
          HVecUS Hpix(size.x() * sz.y() * sz.z());
          size_t idx = 0;
          for(int x = 0; x < size.x(); x++)
            for(int z = Hmin[x].x(); z < Hmin[x].x() + sz.z(); z++)
              for(int y = Hmin[x].y(); y < Hmin[x].y() + sz.y(); y++) {
                if(z < Hmax[x].x() && y < Hmax[x].y()) {
                  size_t gidx = offset(base.x() + x, y, z, imgSize.x(), imgSize.y());
                  if(gidx < imgMax)
                    Hpix[idx] = data[gidx];
                }
                idx++;
              }

          // Process pixels
          int xIdx = 0;
          while((sz.x() = getStep(xIdx, size.x(), sz.y() * sz.z(), sizeof(ushort), mem)) > 0) {
            // Put data on device
            DVecUS Dpix(Hpix.begin() + xIdx * sz.y() * sz.z(),
                        Hpix.begin() + (xIdx + sz.x()) * sz.y() * sz.z());
            checkCudaError("Copy pixels to device");

            // Process pixels
            thrust::for_each(DCountIter(0), DCountIter(sz.y() * sz.z()),
                             pixelEditXOP(xIdx, base.x(), Point3u(sz), Point3u(imgSize), step, shift, p, pv,
                                          radius, color, clipDo, devP(Dpn), devP(Dmin), devP(Dmax),
                                          devP(Dpix)));
            checkCudaError("pixEraseXOP launch");

            // Copy data back to host
            thrust::copy(Dpix.begin(), Dpix.end(), Hpix.begin() + xIdx * sz.y() * sz.z());
            checkCudaError("Copying pixels to host");

            xIdx += sz.x();
          }

          // Update reuslts
          idx = 0;
          for(int x = 0; x < size.x(); x++)
            for(int z = Hmin[x].x(); z < Hmin[x].x() + sz.z(); z++)
              for(int y = Hmin[x].y(); y < Hmin[x].y() + sz.y(); y++) {
                if(z < Hmax[x].x() && y < Hmax[x].y()) {
                  size_t gidx = offset(base.x() + x, y, z, imgSize.x(), imgSize.y());
                  if(gidx < imgMax)
                    data[gidx] = Hpix[idx];
                }
                idx++;
              }
        }
      }
    }
    catch(string& msg) {
      cerr << "pixelEditGPU::Error " << msg << endl;
      err = 1;
    }
    catch(std::exception& e) {
      cerr << "pixelEditGPU::Error" << e.what() << endl;
      err = 1;
    }
    catch(...) {
      cerr << "pixelEditGPU::Unknown exception" << endl;
      err = 1;
    }
  return (err);
#ifdef __MINGW32__
  } catch(...) {
      fprintf(stderr, "Error, exception caught");
      return 1;
  }
#endif
}

