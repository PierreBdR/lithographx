/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "MultiStackProcess.hpp"
#include "Information.hpp"
#include "Progress.hpp"
#include "cuda/CudaExport.hpp"

namespace lgx {
namespace process {

bool CopyMainToWork::operator()(Stack* stack)
{
  stack->work()->data() = stack->main()->data();
  stack->work()->changed();
  stack->work()->copyMetaData(stack->main());
  return true;
}

REGISTER_STACK_PROCESS(CopyMainToWork);

bool CopyWorkToMain::operator()(Stack* stack)
{
  stack->main()->data() = stack->work()->data();
  stack->main()->changed();
  stack->main()->copyMetaData(stack->work());
  return true;
}

REGISTER_STACK_PROCESS(CopyWorkToMain);

bool SwapStacks::operator()(Stack* stack)
{
  if(!stack->main()) {
    setErrorMessage("Error, the main stack is not initialized");
    return false;
  }
  if(!stack->work()) {
    setErrorMessage("Error, the work stack is not initialized");
    return false;
  }
  using std::swap;
  swap(stack->work()->data(), stack->main()->data());
  swapMetaData(stack->work(), stack->main());

  stack->main()->changed();
  stack->work()->changed();
  return true;
}

REGISTER_STACK_PROCESS(SwapStacks);

bool CopySwapStacks::operator()(const QString& storeStr, const QString& actionStr)
{
  Stack* stack1 = stack(0);
  if(!stack1) {
    setErrorMessage("Error, bad action, stack 1 empty");
    return false;
  }
  Stack* stack2 = stack(1);
  if(!stack2) {
    setErrorMessage("Error, bad action, stack 2 empty");
    return false;
  }
  Store* store1 = (storeStr == "Main" ? stack1->main() : stack1->work());
  if(!store1) {
    setErrorMessage("Error, bad action, store 1 empty");
    return false;
  }
  Store* store2 = (storeStr == "Main" ? stack2->main() : stack2->work());
  if(!store2) {
    setErrorMessage("Error, bad action, store 2 empty");
    return false;
  }

  float reference = (norm(stack1->step()) + norm(stack2->step())) / 2;
  bool diffStep = (norm(stack1->step() - stack2->step()) / reference) > 1e-6;
  bool diffOrigin = (norm(stack1->origin() - stack2->origin()) / reference) > 1e-6;
  bool diffSize = (stack1->size() != stack2->size());

  if(actionStr == "1 -> 2") {
    if(stack2->storeSize() == 0) {
      stack2->setStep(stack1->step());
      stack2->setOrigin(stack1->origin());
      stack2->setSize(stack1->size());
    } else if(diffStep or diffOrigin or diffSize)
      return setErrorMessage("Error, Stack 1 and 2 have different size, step, or origin.");
    store2->data() = store1->data();
    store2->copyMetaData(store1);
    store2->changed();
    return true;
  } else if(actionStr == "1 <- 2") {
    if(stack1->storeSize() == 0) {
      stack1->setStep(stack2->step());
      stack1->setOrigin(stack2->origin());
      stack1->setSize(stack2->size());
    } else if(diffStep or diffOrigin or diffSize) {
      setErrorMessage("Error, Stack 1 and 2 have different size, step, or origin.");
      return false;
    }
    store1->data() = store2->data();
    store1->copyMetaData(store2);
    store1->changed();
    return true;
  } else if(actionStr == "1 <-> 2") {
    if(diffStep or diffOrigin or diffSize) {
      setErrorMessage("Error, Stack 1 and 2 have different size, step, or origin.");
      return false;
    }
    HVecUS tempData(stack1->main()->data());
    Point3f tempStep(stack1->step());
    Point3f tempOrigin(stack1->origin());
    Point3f tempSize(stack1->size());

    bool l1 = store1->labels();
    bool l2 = store2->labels();
    QString f1 = store1->file();
    QString f2 = store2->file();

    stack1->setStep(stack2->step());
    stack1->setOrigin(stack2->origin());
    stack1->setSize(stack2->size());
    store1->data() = store2->data();

    stack2->setStep(tempStep);
    stack2->setOrigin(tempOrigin);
    stack2->setSize(tempSize);
    store2->data() = tempData;

    store1->setFile(f2);
    store1->setLabels(l2);
    store2->setFile(f1);
    store2->setLabels(l1);

    store1->changed();
    store2->changed();
    return (true);
  } else {
    setErrorMessage("Error, bad action:" + actionStr);
    return false;
  }
  return true;
}

REGISTER_STACK_PROCESS(CopySwapStacks);

bool AverageStores::operator()(Stack* stack)
{
  Store* main = stack->main();
  Store* work = stack->work();
  if(!main or !work)
    return false;
  HVecUS& mainData = main->data();
  HVecUS& workData = work->data();
  if(mainData.size() != workData.size())
    return false;
  for(uint idx = 0; idx < mainData.size(); idx++)
    workData[idx] = ushort((uint(mainData[idx]) + uint(workData[idx])) / 2);
  work->changed();
  return true;
}

REGISTER_STACK_PROCESS(AverageStores);


} // namespace process
} // namespace lgx
