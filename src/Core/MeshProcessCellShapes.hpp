/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef MESHPROCESSCELLSHAPES_HPP
#define MESHPROCESSCELLSHAPES_HPP

#include <Process.hpp>

#include <MeshProcessCellAxis.hpp>
#include <Geometry.hpp>
#include <Misc.hpp>
#include <Mesh.hpp>

namespace lgx {
namespace process {

class LGXCORE_EXPORT CellShapesProcess : public MeshProcess {
public:
  CellShapesProcess(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  { }

  bool initialize(ParmList& parms, QWidget* parent) override;

  bool operator()(const ParmList& parms) override
  {
    if(not checkState().mesh(MESH_NON_EMPTY|MESH_CELLS))
      return false;
    auto heat = parms[1].toString();
    auto* mesh = currentMesh();
    bool res = (*this)(mesh, parms[0].toString(), heat);
    if(res) {
      if(heat.toLower() != "none") {
        mesh->showHeat();
        mesh->showSurface();
      }
    }
    return res;
  }

  bool operator()(Mesh* mesh, const QString& filename, const QString& heat);

  QString folder() const override {
    return "Cell Mesh";
  }
  QString name() const override {
    return "Cell Shapes Features 2D";
  }
  QString description() const override {
    return "Compute a series of features extracted from the shape of the cell.\n"
           "Many of these features are computed from an ellipsis fitted to the cell shape, "
           "which is done using the Mesh.Cell_Shape_Axis_2D process.\n\n"
           "The stored properties are:\n"
           " - Position in the global coordinate system\n"
           " - Polar coordinates in the global coordinate system\n"
           " - Size of the major axis of the fitted ellipsis\n"
           " - Eccentricity of the fitted ellipsis\n"
           " - Angle between the global X axis and the major axis of the fitted ellipsis\n"
           " - Area of the cell\n"
           " - Perimeter of the cell\n"
           " - Average distance between the center of mass and the perimeter of the cell\n"
           " - Maximum distance between the center of mass and the perimeter of the cell\n"
           " - Minimum distance between the center of mass and the perimeter of the cell\n"
           " - Cell extension, defined as the geometric mean of the minor and major axis "
           "of the fitted ellipsis\n"
           " - Angle between the major axis and the radius of the center of mass "
           "(e.g. the vector from the origin to the center of mass).\n";
  }

  QStringList parmNames() const override
  {
    return QStringList() << "Filename"
                         << "Heat map";
  }
  QStringList parmDescs() const override
  {
    return QStringList() << "Output file name"
                         << "Which property should be displayed as a heat map";
  }
  ParmList parmDefaults() const override
  {
    return ParmList() << "" << "none";
  }
  ParmChoiceMap parmChoice() const override
  {
    ParmChoiceMap map;
    map[1] = QStringList() << "none"
                           << "Radial Position"
                           << "Angular Position"
                           << "Major Axis Length"
                           << "Minor Axis Length"
                           << "Eccentricity"
                           << "Major Axis Orientation"
                           << "Area"
                           << "Perimeter"
                           << "Average Radius"
                           << "Maximum Radius"
                           << "Minimum Radius"
                           << "Cell Extension"
                           << "Incline Angle";
    return map;
  }

  QIcon icon() const override {
    return QIcon(":/images/CellFeatures.png");
  }
};

class LGXCORE_EXPORT Compute2DCellShapeAxis : public MeshProcess {
public:
  Compute2DCellShapeAxis(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  { }

  bool operator()(const ParmList& parms) override
  {
    if(not checkState().mesh(MESH_NON_EMPTY))
      return false;
    Point2f correction_factor;
    QString correction = parms[0].toString().trimmed();
    if(correction == "Ellipse")
      correction_factor = 2;
    else if(correction == "Rectangle")
      correction_factor = sqrt(3);
    else if(correction == "Maximum Span")
      correction_factor = 0;
    else {
      correction_factor = util::stringToPoint2f(correction);
      if(correction_factor[0] != correction_factor[0]) {
        setErrorMessage(QString("Invalid correction string '%1', expected one of 'Ellipse',"
                                " 'Rectangle', 'Maximum Span', a single value or two values."));
        return false;
      }
    }

    bool res = (*this)(currentMesh(), correction_factor);
    if(res)
      res &= showResult();
    return res;
  }

  bool operator()(Mesh* mesh, const Point2f& correction_factor);

  QString name() const override { return "Cell Shape Axis 2D"; }
  QString folder() const override { return "Cell Axis/2D Shape"; }
  QString description() const override
  {
    return "Compute the shape of the cell using PC analysis on the cell area.\n"
           "For this process, either the mesh is a cell mesh, or each label is considered a different cell.";
  }
  QStringList parmNames() const override
  {
    return QStringList() << "Span Correction";
  }
  QStringList parmDescs() const override
  {
    return QStringList() << "Correction factor to apply to the principal components to convert to a known shape.\n"
                            "This can be one of the known shape (i.e. 'Ellipse', 'Rectangle' or 'Maximum Span'), "
                            "a 2D vector corresponding to the factors for the major and minor axes or a single "
                            "number of both factors are identical.\n"
                            "The shape 'Maximum Span', which is equivalent to a factor set to 0, is a special case where "
                            "the values of the PCs is ignored, and instead the lengths are set to contain exactly the whole "
                            "area covered by the cell.";
  }
  ParmList parmDefaults() const override
  {
    return ParmList() << "Ellipse";
  }
  ParmChoiceMap parmChoice() const override
  {
    ParmChoiceMap map;
    map[0] = QStringList() << "Ellipse" << "Rectangle" << "Maximum Span";
    return map;
  }
  QIcon icon() const override {
    return QIcon(":/images/PCAnalysis.png");
  }

protected:
  bool showResult();
};

/**
 * \class Display2DCellShapeAxis <MeshProcessCellShapes.hpp>
 *
 * Change the representation of the fibril orientation after it has been
 * computed.
 */
class LGXCORE_EXPORT Display2DCellShapeAxis : public MeshProcess {
public:
  Display2DCellShapeAxis(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  bool operator()(const ParmList& parms) override
  {
    if(!checkState().mesh(MESH_NON_EMPTY))
      return false;

    bool ok;
    float axisLineWidth = parms[3].toFloat(&ok);
    if(!ok)
      return setErrorMessage("Error, argument 'Line Color' must be a number");
    float axisOffset = parms[4].toFloat(&ok);
    if(!ok)
      return setErrorMessage("Error, argument 'Line Offset' must be a number");

    QColor majorColor, minorColor;

    minorColor = parms[1].value<QColor>();
    if(not minorColor.isValid())
      return setErrorMessage(QString("Invalid color definition: %1").arg(parms[1].toString()));

    majorColor = parms[2].value<QColor>();
    if(not majorColor.isValid())
      return setErrorMessage(QString("Invalid color definition: %1").arg(parms[2].toString()));

    QString heatMap = parms[0].toString();
    auto* mesh = currentMesh();

    bool res = (*this)(mesh, heatMap, minorColor, majorColor, axisLineWidth, axisOffset);
    if(res) {
      if(heatMap.toLower() != "none")
        mesh->showHeat();
      if(not mesh->isMeshVisible() and not mesh->isSurfaceVisible())
        mesh->showSurface();
    }
    return res;
  }

  bool operator()(Mesh* mesh, const QString& displayHeatMap, const QColor& minorColor, const QColor& majorColor,
                  float axisLineWidth, float axisOffset);

  QString folder() const override {
    return "Cell Axis/2D Shape";
  }
  QString name() const override {
    return "Display Cell Shape Axis 2D";
  }
  QString description() const override
  {
    return "Display the cell shape axis on the image.";
  }
  QStringList parmNames() const override
  {
    return QStringList() << "Heatmap"
                         << "Color Minor Axis"
                         << "Color Major Axis"
                         << "Line Width"
                         << "Line Offset";
  }
  QStringList parmDescs() const override
  {
    return QStringList() << "Choose which quantity to display as a heat map."
                         << "Line Color for the minor axis"
                         << "Line Width for the major axis"
                         << "Draw the vector ends a bit shifted up for proper display on surfaces.";
  }
  ParmList parmDefaults() const override
  {
    return ParmList() << "none"
                      << "red"
                      << "blue"
                      << 5.0
                      << 0.1;
  }
  ParmChoiceMap parmChoice() const override
  {
    ParmChoiceMap map;
    map[0] = QStringList() << "none"
                           << "Major Axis"
                           << "Minor Axis"
                           << "Eccentricity";
    map[2] = map[1] = QColor::colorNames();
    return map;
  }
  QIcon icon() const override {
    return QIcon(":/images/PrincipalOrientations.png");
  }
};


class LGXCORE_EXPORT Load2DCellShapeAxis : public LoadCellAxis {
public:
  Load2DCellShapeAxis(const MeshProcess& process)
    : Process(process)
    , LoadCellAxis(process)
  {
  }

  bool operator()(Mesh* mesh, const QString& filename) override;

  QString folder() const override {
    return "Cell Axis/2D Shape";
  }
  QString name() const override {
    return "Load Cell Shape Axis 2D";
  }
  QString description() const override {
    return "Load the 2D cell axis from a CSV file";
  }
};

class LGXCORE_EXPORT Save2DCellShapeAxis : public SaveCellAxis {
public:
  Save2DCellShapeAxis(const MeshProcess& process)
    : Process(process)
    , SaveCellAxis(process)
  {
  }

  bool operator()(Mesh* mesh, const QString& filename) override;

  QString folder() const override {
    return "Cell Axis/2D Shape";
  }
  QString name() const override {
    return "Save Cell Shape Axis 2D";
  }
  QString description() const override {
    return "Save the 2D cell axis to a CSV file";
  }
};


} // namespace process
} // namespace lgx

#endif // MESHPROCESSCELLSHAPES_HPP

