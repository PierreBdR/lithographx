/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "Information.hpp"

#ifndef LGX_CLI
# include <QMainWindow>
# include <QStatusBar>
# include <QThread>
# include <QCoreApplication>
#endif

#include <iostream>
using namespace std;

#include <QTextStream>
#include <stdio.h>

namespace lgx {

bool DEBUG = false;

namespace Information {

namespace {
QTextStream real_out(stdout);
QTextStream real_err(stderr);
}

ThreadSafeTextStream out(real_out);
ThreadSafeTextStream err(real_err);

ThreadSafeTextStream::ThreadSafeTextStream(QTextStream& s)
  : stream(s)
  , mutex(QMutex::Recursive)
{ }

ThreadSafeTextStream::~ThreadSafeTextStream()
{ }

void ThreadSafeTextStream::setRealNumberNotation(QTextStream::RealNumberNotation notation)
{
  QMutexLocker locker(&mutex);
  stream.setRealNumberNotation(notation);
}

void ThreadSafeTextStream::setRealNumberPrecision(int precision)
{
  QMutexLocker locker(&mutex);
  stream.setRealNumberPrecision(precision);
}

void ThreadSafeTextStream::setCodec(QTextCodec * codec)
{
  QMutexLocker locker(&mutex);
  stream.setCodec(codec);
}

void ThreadSafeTextStream::setCodec(const char * codecName)
{
  QMutexLocker locker(&mutex);
  stream.setCodec(codecName);
}

void ThreadSafeTextStream::setFieldAlignment(QTextStream::FieldAlignment mode)
{
  QMutexLocker locker(&mutex);
  stream.setFieldAlignment(mode);
}

void ThreadSafeTextStream::setFieldWidth(int width)
{
  QMutexLocker locker(&mutex);
  stream.setFieldWidth(width);
}

void ThreadSafeTextStream::setGenerateByteOrderMark(bool generate)
{
  QMutexLocker locker(&mutex);
  stream.setGenerateByteOrderMark(generate);
}

void ThreadSafeTextStream::setIntegerBase(int base)
{
  QMutexLocker locker(&mutex);
  stream.setIntegerBase(base);
}

void ThreadSafeTextStream::setLocale(const QLocale & locale)
{
  QMutexLocker locker(&mutex);
  stream.setLocale(locale);
}

void ThreadSafeTextStream::setNumberFlags(QTextStream::NumberFlags flags)
{
  QMutexLocker locker(&mutex);
  stream.setNumberFlags(flags);
}

void ThreadSafeTextStream::setPadChar(QChar ch)
{
  QMutexLocker locker(&mutex);
  stream.setPadChar(ch);
}

bool ThreadSafeTextStream::atEnd() const
{
  return stream.atEnd();
}

QTextStream::FieldAlignment ThreadSafeTextStream::fieldAlignment() const
{
  return stream.fieldAlignment();
}

int ThreadSafeTextStream::fieldWidth() const
{
  return stream.fieldWidth();
}

void ThreadSafeTextStream::flush()
{
  QMutexLocker locker(&mutex);
  stream.flush();
}

bool ThreadSafeTextStream::generateByteOrderMark() const
{
  return stream.generateByteOrderMark();
}

int ThreadSafeTextStream::integerBase() const
{
  return stream.integerBase();
}

QLocale ThreadSafeTextStream::locale() const
{
  return stream.locale();
}

QTextStream::NumberFlags ThreadSafeTextStream::numberFlags() const
{
  return stream.numberFlags();
}

QChar ThreadSafeTextStream::padChar() const
{
  return stream.padChar();
}

QTextStream::RealNumberNotation ThreadSafeTextStream::realNumberNotation() const
{
  return stream.realNumberNotation();
}

int ThreadSafeTextStream::realNumberPrecision() const
{
  return stream.realNumberPrecision();
}

bool ThreadSafeTextStream::autoDetectUnicode() const
{
  return stream.autoDetectUnicode();
}

QTextCodec* ThreadSafeTextStream::codec() const
{
  return stream.codec();
}

#ifndef LGX_CLI
static QStatusBar* status = 0;
static QMainWindow* mainWnd = 0;

void setMainWindow(QMainWindow* wnd)
{
  mainWnd = wnd;
  status = wnd->statusBar();
  out.setCodec("UTF-8");
  err.setCodec("UTF-8");
}

void Print(const QString& _text)
{
  if(status and status->thread() != QThread::currentThread()) {
    QCoreApplication::postEvent(mainWnd, new Event(QString(_text.data())));
  } else {
    out << _text << endl;
    if(status != 0)
      status->showMessage(_text);
  }
}

void Print(const string _text)
{
  QString txt = QString::fromStdString(_text);
  Print(txt);
}
#endif
} // namespace Information
} // namespace lgx
