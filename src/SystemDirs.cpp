/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "SystemDirs.hpp"

#include "Information.hpp"
#include "Dir.hpp"

#include <QCoreApplication>
#include <QStandardPaths>

namespace lgx {
namespace util {

QDir installDir()
{
  QDir dir = QCoreApplication::applicationDirPath();
  dir.cd("..");
  return dir;
}

QDir resourcesDir()
{
  QDir dir = QCoreApplication::applicationDirPath();
  if(dir.cd("..") and
     dir.cd("share") and
     dir.cd("LithoGraphX"))
    return dir;
  Information::err << "No standard folder found, fall back to ~/.LithoGraphX" << endl;
  return configurationDir();
}


QDir configurationDir(bool create)
{
  static const QString default_path = resolvePath("~/.LithoGraphX");
  QStringList dataPaths = QStandardPaths::standardLocations(QStandardPaths::GenericDataLocation);
  for(QString& path: dataPaths) {
      path += "/LithoGraphX";
  }
  dataPaths << default_path;
  for(const auto& d: dataPaths) {
      DEBUG_OUTPUT("Trying configuration folder '" << d << "'" << endl);
      if(QFileInfo(d).isDir() or (create and createPath(d)))
          return d;
  }
  return QDir(dataPaths[0]);
}

QDir userProcessesDir(bool create)
{
  QDir dir = configurationDir(create);
  if(not dir.cd("processes")) {
    if(create) {
      dir.mkdir("processes");
      dir.cd("processes");
    }
  }
  return dir;
}

QDir userMacroDir(bool create)
{
  QDir dir = configurationDir(create);
  if(not dir.cd("macros")) {
    if(create) {
      dir.mkdir("macros");
      dir.cd("macros");
    }
  }
  return dir;
}

QDir systemProcessesDir()
{
  QDir dir = resourcesDir();
  if(!dir.cd("processes"))
    dir = QDir(".");
  return dir;
}

QList<QDir> processesDirs()
{
  return { systemProcessesDir(), userProcessesDir() };
}

QDir systemMacroDir()
{
  QDir dir = resourcesDir();
  if(not dir.cd("macros"))
    dir = QDir(".");
  return dir;
}

QList<QDir> macroDirs()
{
  return { systemMacroDir(), userMacroDir() };
}

QDir includesDir()
{
  QDir dir = QCoreApplication::applicationDirPath();
  if(!dir.cd(".."))
    dir = QDir(".");
  else if(!dir.cd("include"))
    dir = QDir(".");
  else if(!dir.cd("LithoGraphX"))
    dir = QDir(".");
  return dir;
}

QDir libsDir()
{
  QDir dir = QCoreApplication::applicationDirPath();
  if(!dir.cd(".."))
    dir = QDir(".");
  else if(!dir.cd("lib"))
    dir = QDir(".");
  return dir;
}

QDir userLibsDir(bool create)
{
  QDir dir = configurationDir(create);
  if(!dir.cd("lib")) {
    if(create) {
      dir.mkdir("lib");
      dir.cd("lib");
    }
  }
  return dir;
}

QDir docsDir()
{
  QDir dir = QCoreApplication::applicationDirPath();
  if(!dir.cd(".."))
    dir = QDir(".");
  else if(!dir.cd("share"))
    dir = QDir(".");
  else if(!dir.cd("doc"))
    dir = QDir(".");
  else if(!dir.cd("LithoGraphX"))
    dir = QDir(".");
  return dir;
}

QDir systemPackagesDir()
{
  auto dir = resourcesDir();
  if(dir.exists() and dir.cd("packages"))
    return dir;
  return QDir();
}

QDir userPackagesDir(bool create)
{
  auto dir = configurationDir(create);
  if(dir.exists()) {
    if(not dir.cd("packages")) {
      if(create)
        dir.mkdir("packages");
      if(not dir.cd("packages"))
        return QDir();
    }
    return dir;
  }
  return QDir();
}

QList<QDir> packagesDirs()
{
  return { systemPackagesDir(), userPackagesDir() };
}

} // namespace util
} // namespace lgx
