/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef LGXPYTHON_HPP
#define LGXPYTHON_HPP

#include <python/Python.hpp>

#include <Process.hpp>

#include <vector>
#include <sip.h>

#ifndef PyObject_HEAD
struct _object;
typedef _object PyObject;
#endif

namespace lgx {
namespace process {
/**
 * \class PythonProcess python/LGXPython.h <python/LGXPython.h>
 * Define a python process for a process of type P
 *
 * P must be one of StackProcess, MeshProcess or GlobalProcess
 */
template <typename ProcessType>
class LGXPYTHONSHARED_EXPORT PythonProcess : public ProcessType
{
public:
  typedef ProcessType process_t;

  PythonProcess(const process_t& process);
};

/// Start and create a python interpreter, if it is not already started
struct LGXPYTHONSHARED_EXPORT PythonInterpreter
{
  PythonInterpreter(bool global_interpreter = false);
  ~PythonInterpreter();

  // Test whether the python interpreter can be used or not
  bool valid() const;

  // Finalize the interpreter! No python should be called after that
  void finalize();

  static const sipAPIDef* sip_api() { return _api; }
  static LGXPYTHONSHARED_EXPORT const sipTypeDef* findSipType(const char* type);
  static const sipTypeDef* sipQVariantType() { return _sip_QVariantType; }
  static const sipTypeDef* sipQWidgetType() { return _sip_QWidgetType; }
  static LGXPYTHONSHARED_EXPORT PyObject* convertFromSipType(void* obj, const sipTypeDef* type, PyObject* transferObj);
  static LGXPYTHONSHARED_EXPORT PyObject* convertFromSipType(void* obj, const char* type, PyObject* transferObj);
  static LGXPYTHONSHARED_EXPORT bool canConvertToSipType(PyObject* obj, const sipTypeDef* type, int flags);
  static LGXPYTHONSHARED_EXPORT void* convertToSipType(PyObject* obj, const sipTypeDef* type, PyObject* transferObj,
                                                 int flags, int* state, int* iserr);
  static LGXPYTHONSHARED_EXPORT bool enableAutoConversion(const sipTypeDef* td, bool enable);
  template <typename T>
  static T* convertToSipType(PyObject* obj, const sipTypeDef* type, PyObject* transferObj,
                             int flags, int* state, int* iserr)
  {
    return reinterpret_cast<T*>(convertToSipType(obj, type, transferObj, flags, state, iserr));
  }
  static LGXPYTHONSHARED_EXPORT PyTypeObject* convertSipTypeToPyTypeObject(const sipTypeDef* tdef);

private:
  bool is_global = false;
  static const sipAPIDef* _api;
  static const sipTypeDef* _sip_QVariantType;
  static const sipTypeDef* _sip_QWidgetType;
};

#ifndef WIN32
extern template class PythonProcess<StackProcess>;
extern template class PythonProcess<MeshProcess>;
extern template class PythonProcess<GlobalProcess>;
#endif

} // namespace process
} // namespace lgx

#endif // LGXPYTHON_HPP

