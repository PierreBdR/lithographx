/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "ResetableSlider.hpp"

namespace lgx {
namespace gui {

ResetableSlider::ResetableSlider(QWidget* parent)
  : QSlider(parent)
  , default_value(0)
{
  init();
}

ResetableSlider::ResetableSlider(Qt::Orientation orientation, QWidget* parent)
  : QSlider(orientation, parent)
{
  init();
}

void ResetableSlider::mouseDoubleClickEvent(QMouseEvent*) {
  emit reset();
}

void ResetableSlider::setDefaultValue(int val)
{
  if(val <= maximum() and val >= minimum()) {
    default_value = val;
  }
}

void ResetableSlider::setValueAsDefault() {
  setDefaultValue(value());
}

void ResetableSlider::checkDefaultValue(int min, int max)
{
  if(default_value < min)
    default_value = min;
  else if(default_value > max)
    default_value = max;
}

void ResetableSlider::init()
{
  connect(this, &ResetableSlider::rangeChanged, this, &ResetableSlider::checkDefaultValue);
  connect(this, &ResetableSlider::reset, this, &ResetableSlider::resetValue);
}

void ResetableSlider::resetValue() {
  setValue(default_value);
}

} // namespace gui
} // namespace lgx
