/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "SystemProcessSave.hpp"

#include <Dir.hpp>
#include <Forall.hpp>
#include <Image.hpp>
#include <Information.hpp>
#include <Mesh.hpp>
#include <Progress.hpp>
#include <Stack.hpp>
#include <Store.hpp>
#include <PlyFile.hpp>

#include <LGXViewer/qglviewer.h>

#include <CImg.h>
#include <limits>
#include <unordered_set>

#include <QAbstractItemModel>
#include <QComboBox>
#include <QFileDialog>
#include <QInputDialog>
#include <QItemDelegate>
#include <QRegExp>
#include <QtXml>

#include <ui_SaveMesh.h>
#include <ui_ExportMesh.h>
#include <ui_SaveAll.h>
#include <ui_ExportStack.h>

using namespace cimg_library;
using namespace qglviewer;
using namespace lgx;
using namespace lgx::util;

using std::begin;
using std::end;

#ifdef WIN32
#  define FileDialogOptions QFileDialog::DontUseNativeDialog
#else
#  define FileDialogOptions 0
#endif

namespace {
template <typename Container>
const typename Container::mapped_type& get_default(const Container& cont,
                                                   const typename Container::key_type& key,
                                                   const typename Container::mapped_type& def)
{
  const auto& found = cont.find(key);
  if(found != cont.end())
    return found->second;
  return def;
}

}

namespace lgx {
namespace process {

typedef CImg<ushort> CImgUS;

bool StackSave::operator()(const ParmList& parms)
{
  int stackId = parms[2].toInt();
  int compressionLevel = parms[3].toInt();
  auto which_store = parms[1].value<STORE>();
  if(!checkState().store((int)which_store | STORE_NON_EMPTY, stackId))
    return false;
  Stack* stack = this->stack(stackId);
  Store* store = stack->store(which_store);
  bool res = (*this)(stack, store, parms[0].toString(), compressionLevel);
  return res;
}

bool StackSave::operator()(Stack* stack, Store* store, const QString& filename, int compressionLevel)
{
  if(filename.isEmpty()) {
    setErrorMessage("Error, trying to save a stack with an empty filename.");
    return false;
  }

  HVecUS& data = store->data();

  Progress progress(QString("Saving %1 Stack %2").arg(store == stack->main() ? "main" : "work").arg(stack->userId()),
                    0);
  qint64 qty;

  // Process the various types
  if(filename.endsWith(".mgxs", Qt::CaseInsensitive)) {
    QFile file(filename);
    if(!file.open(QIODevice::WriteOnly))
      throw QString("Cannot open output file - %1").arg(filename);

    if(compressionLevel > 9)
      compressionLevel = 9;
    if(compressionLevel < -1)
      compressionLevel = -1;
    Point3u size = stack->size();
    Point3f step = stack->step();
    Point3f origin = stack->origin();
    quint32 xsz = size.x();
    quint32 ysz = size.y();
    quint32 zsz = size.z();
    quint8 cl = (quint8)compressionLevel;
    float xum = step.x();
    float yum = step.y();
    float zum = step.z();
    float sxum = origin.x();
    float syum = origin.y();
    float szum = origin.z();
    bool isLabel = store->labels();
    quint64 datasz = quint64(data.size()) * 2ul;

    static const char version[] = "MGXS 1.3 ";
    qty = file.write(version, sizeof(version) - 1);
    if(qty != sizeof(version) - 1)
      throw QString("Could not write data to file (written = %1, expected = %2)").arg(qty).arg(sizeof(version)
                                                                                               - 1);
    qty = file.write((const char*)&isLabel, sizeof(bool));
    if(qty != sizeof(bool))
      throw QString("Could not write data to file (written = %1, expected = %2)").arg(qty).arg(sizeof(bool));
    qty = file.write((const char*)&sxum, sizeof(float));
    if(qty != sizeof(float))
      throw QString("Could not write data to file (written = %1, expected = %2)").arg(qty).arg(sizeof(float));
    qty = file.write((const char*)&syum, sizeof(float));
    if(qty != sizeof(float))
      throw QString("Could not write data to file (written = %1, expected = %2)").arg(qty).arg(sizeof(float));
    qty = file.write((const char*)&szum, sizeof(float));
    if(qty != sizeof(float))
      throw QString("Could not write data to file (written = %1, expected = %2)").arg(qty).arg(sizeof(float));
    qty = file.write((const char*)&xsz, sizeof(quint32));
    if(qty != sizeof(quint32))
      throw QString("Could not write data to file (written = %1, expected = %2)").arg(qty).arg(sizeof(quint32));
    qty = file.write((const char*)&ysz, sizeof(quint32));
    if(qty != sizeof(quint32))
      throw QString("Could not write data to file (written = %1, expected = %2)").arg(qty).arg(sizeof(quint32));
    qty = file.write((const char*)&zsz, sizeof(quint32));
    if(qty != sizeof(quint32))
      throw QString("Could not write data to file (written = %1, expected = %2)").arg(qty).arg(sizeof(quint32));
    qty = file.write((const char*)&xum, sizeof(float));
    if(qty != sizeof(float))
      throw QString("Could not write data to file (written = %1, expected = %2)").arg(qty).arg(sizeof(float));
    qty = file.write((const char*)&yum, sizeof(float));
    if(qty != sizeof(float))
      throw QString("Could not write data to file (written = %1, expected = %2)").arg(qty).arg(sizeof(float));
    qty = file.write((const char*)&zum, sizeof(float));
    if(qty != sizeof(float))
      throw QString("Could not write data to file (written = %1, expected = %2)").arg(qty).arg(sizeof(float));
    qty = file.write((const char*)&datasz, sizeof(quint64));
    if(qty != sizeof(quint64))
      throw QString("Could not write data to file (written = %1, expected = %2)").arg(qty).arg(sizeof(quint64));
    qty = file.write((const char*)&cl, sizeof(quint8));
    if(qty != sizeof(quint8))
      throw QString("Could not write data to file (written = %1, expected = %2)").arg(qty).arg(sizeof(quint8));

    if(compressionLevel > 0) {
      // QByteArray stkdata = qCompress((const uchar *)data.data(), data.size() * 2ul, 1);
      // file.write(stkdata);
      // Make slices of 64MB
      quint64 nb_slices = datasz >> 26;
      quint64 slice_size = 1ul << 26;
      progress.setMaximum(nb_slices);
      for(quint64 i = 0; i < nb_slices; ++i) {
        QByteArray stkdata
          = qCompress((const uchar*)data.data() + i * slice_size, slice_size, compressionLevel);
        quint32 sz = stkdata.size();
        qty = file.write((const char*)&sz, sizeof(quint32));
        if(qty != sizeof(quint32)) {
          throw QString("Could not write enough data to file (written = %1, expected = %2)").arg(qty).arg(
                  sizeof(quint32));
        }
        qty = file.write(stkdata);
        if(qty != stkdata.size()) {
          throw QString("Could not write enough data to file (written = %1, expected = %2)").arg(qty).arg(
                  stkdata.size());
        }
        if(!progress.advance(i))
          userCancel();
      }
      quint64 left_size = datasz - (nb_slices << 26);
      QByteArray stkdata = qCompress((const uchar*)data.data() + (nb_slices << 26), left_size, compressionLevel);
      quint32 sz = stkdata.size();
      qint64 qty = file.write((const char*)&sz, sizeof(quint32));
      if(qty != sizeof(quint32)) {
        throw QString("Could not write enough data to file (written = %1, expected = %2)").arg(qty).arg(
                sizeof(quint32));
      }
      qty = file.write(stkdata);
      if(qty != stkdata.size()) {
        throw QString("Could not write enough data from MGXS file (written = %1, expected = %2)").arg(qty).arg(
                stkdata.size());
      }
      if(!progress.advance(nb_slices))
        userCancel();
    } else {
      // Ok, if greater than 64MB, write in bits
      quint64 nb_slices = datasz >> 26;
      quint64 slice_size = 1ul << 26;
      for(quint64 i = 0; i < nb_slices; ++i) {
        qty = file.write((char*)data.data() + i * slice_size, slice_size);
        if(quint64(qty) != slice_size) {
          throw QString("Could not write enough data to file (written = %1, expected = %2)").arg(qty).arg(
                  slice_size);
        }
      }
      quint64 left_size = datasz - (nb_slices << 26);
      qty = file.write((char*)data.data() + (nb_slices << 26), left_size);
      if(quint64(qty) != left_size) {
        throw QString("Could not write enough data from MGXS file (written = %1, expected = %2)").arg(qty).arg(
                left_size);
      }
    }
    progress.advance(0);

    file.close();
  } else if(filename.endsWith(".inr", Qt::CaseInsensitive)) {
    Point3u size = stack->size();
    Point3f step = stack->step();
    CImgUS image(data.data(), size.x(), size.y(), size.z(), 1, true);
    image.save_inr(filename.toStdString().data(), step.data());
  } else if(filename.endsWith(".tif", Qt::CaseInsensitive) or filename.endsWith(".tiff", Qt::CaseInsensitive)) {
    Point3u size = stack->size();
    Point3f step = stack->step();
    Image5D image(filename, data.data(), size, step);
    image.origin = stack->origin();
    image.labels = store->labels();
    image.compression_level = compressionLevel;
    saveTIFFImage(filename, image);
  } else
    throw QString("Invalid file type %1").arg(filename);

  store->setFile(stripCurrentDir(filename));

  actingFile(filename);

  SETSTATUS("Saved Stack " << store->file() << ", Size: " << stack->size() << " voxel size:" << stack->step());
  return true;
}

bool StackSave::initialize(ParmList& parms, QWidget* parent)
{
  int stackId = parms[2].toInt();
  STORE which_store = parms[1].value<STORE>();
  if(!checkState().store((int)which_store, stackId))
    return false;
  QString mgxsFilter("LithoGraphX Stack files (*.mgxs)"), inrFilter("Inria Stack files (*.inr)");
  QString tiffFilter("TIFF Stack files (*.tif *.tiff)"), filter;
  Stack* s = stack(stackId);
  Store* store = s->store(which_store);
  QString filename = store->file();
  if(filename.isEmpty())
    filename = parms[0].toString();
  auto channel_idx = filename.indexOf('?');
  if(channel_idx >= 0) {
    filename = filename.left(channel_idx);
  }
  // If we loaded a file before, strip of the ending
  if(filename.right(4) == ".txt") {
    filename = filename.left(filename.length() - 4);
    filename += ".tif";
  }

  if(filename.endsWith(".inr", Qt::CaseInsensitive))
    filter = inrFilter;
  else if(filename.endsWith(".mgxs", Qt::CaseInsensitive))
    filter = mgxsFilter;
  else
    filter = tiffFilter;

  // Get the file name
  filename = QFileDialog::getSaveFileName(parent, QString("Select stack file"), filename,
                                          QString(tiffFilter + ";;" + mgxsFilter + ";;" + inrFilter + ";;"), &filter,
                                          FileDialogOptions);
  if(filename.isEmpty())
    return false;

  // Check ending
  if(filter == inrFilter and !filename.endsWith(".inr", Qt::CaseInsensitive))
    filename += ".inr";
  else if(filter == tiffFilter and
          not (filename.endsWith(".tif", Qt::CaseInsensitive) or filename.endsWith(".tiff", Qt::CaseInsensitive)))
    filename += ".tif";
  else if(filter == mgxsFilter and !filename.endsWith(".mgxs", Qt::CaseInsensitive))
    filename += ".mgxs";

  parms[0] = filename;
  return true;
}

// REGISTER_STACK_PROCESS(StackSave);

bool StackExport::initialize(ParmList& parms, QWidget* parent)
{
  int stackId = parms[4].toInt();
  STORE which_store = parms[1].value<STORE>();
  if(!checkState().store((int)which_store, stackId))
    return false;
  Stack* s = stack(stackId);
  Store* store = s->store(which_store);
  QString filename = store->file();

  auto channel_idx = filename.indexOf('?');
  if(channel_idx >= 0) {
    filename = filename.left(channel_idx);
  }

  QDialog dlg(parent);
  Ui_ExportStackDialog ui;
  ui.setupUi(&dlg);
  this->dlg = &dlg;
  this->ui = &ui;

  connect(ui.SelectImageFile, &QAbstractButton::clicked, this, &StackExport::selectImageFile);
  connect(ui.ImageType, SIGNAL(currentIndexChanged(const QString &)), this, SLOT(setImageType(const QString &)));

  QStringList suppTypes = supportedImageWriteFormats();
  QStringList types;
  foreach(QString f, suppTypes) {
    types << f;
  }

  ui.ImageType->addItems(types);
  ui.generateVoxelSpacing->setChecked(parmToBool(parms[3]));

  if(dlg.exec() == QDialog::Accepted) {
    parms[2] = suppTypes[ui.ImageType->currentIndex()];
    parms[0] = ui.ImageFile->text();
    parms[3] = ui.generateVoxelSpacing->isChecked();
    parms[5] = ui.nbDigits->value();
    return true;
  }
  return false;
}

void StackExport::selectImageFile()
{
  QStringList filter;
  foreach(QString f, supportedImageWriteFormats()) {
    if(f != "CImg Auto")
      filter << QString("%1 images (*.%2 *.%1)").arg(f.toUpper()).arg(f.toLower());
    else
      filter << "CImg Auto (*.*)";
  }
  QString filename = ui->ImageFile->text();
  QString selected;
  filename = QFileDialog::getSaveFileName(dlg, "Filename to export to", filename, filter.join(";;"), &selected);
  if(!filename.isEmpty()) {
    int idx = filter.indexOf(selected);
    ui->ImageType->setCurrentIndex(idx);
    if(!selected.startsWith("CImg Auto")) {
      QString type = selected.left(selected.indexOf(" ")).toLower();
      if(!filename.endsWith("." + type, Qt::CaseInsensitive)) {
        filename += "." + type;
      }
    }
    ui->ImageFile->setText(filename);
  }
}

void StackExport::setImageType(const QString& type)
{
  if(!type.startsWith("CImg Auto")) {
    QString t = type.toLower();
    QString fn = ui->ImageFile->text();
    int idx = fn.lastIndexOf(".");
    if(idx != -1)
      fn = fn.left(idx + 1) + t;
    else
      fn += "." + t;
    ui->ImageFile->setText(fn);
  }
}

bool StackExport::operator()(const ParmList& parms)
{
  int stackId = parms[4].toInt();
  STORE which_store = parms[1].value<STORE>();
  if(!checkState().store((int)which_store, stackId))
    return false;
  Stack* s = stack(stackId);
  Store* store = s->store(which_store);
  QString filename = parms[0].toString();
  QString type = parms[2].toString();
  uint nb_digits = parms[5].toUInt();
  if(filename.isEmpty()) {
    setErrorMessage("Error, filename provided is empty");
    return false;
  }
  if(type.isEmpty()) {
    setErrorMessage("Error, type provided is empty");
    return false;
  }
  if(nb_digits > 500) {
    setErrorMessage("Error, the number of digits asked for is greater than 500. It seems unreasonnably high.");
    return false;
  }
  return (*this)(s, store, filename, type, nb_digits, parmToBool(parms[3]));
}

bool StackExport::operator()(Stack* stack, Store* store, const QString& filename, const QString& type,
                             unsigned int nb_digits, bool generate_voxel_spacing)
{
  actingFile(filename);
  Point3u size = stack->size();
  Point3f step = stack->step();
  Image5D image(filename, store->data().data(), size, step);
  saveImage(filename, image, type, nb_digits);
  if(generate_voxel_spacing) {
    QFileInfo fi(filename);
    QDir d = fi.dir();
    QFile file(d.filePath("voxelspacing.txt"));
    if(!file.open(QIODevice::WriteOnly)) {
      setErrorMessage(QString("Error, cannot open file '%1' for writing.").arg(file.fileName()));
      return false;
    }
    QTextStream ss(&file);
    ss << "x " << stack->step().x() << endl << "y " << stack->step().y() << endl << "z " << stack->step().z()
       << endl;
    file.close();
  }
  return true;
}

QString MeshExport::properFile(QString filename, const QString& type) const
{
  QFileInfo fi(filename);
  QString suf = fi.suffix();
  if(!suf.isEmpty())
    filename = filename.left(filename.size() - suf.size() - 1);
  if(type == "Text" or type == "Cells")
    filename += ".txt";
  else if(type == "MeshEdit")
    filename += ".mesh";
  else if(type == "STL")
    filename += ".stl";
  else if(type.startsWith("VTK Mesh"))
    filename += ".vtu";
  else if(type == "OBJ")
    filename += ".obj";
  else if(type.startsWith("PLY"))
    filename += ".ply";
  else if(type == "MorphoGraphX Mesh")
    filename += ".mgxm";
  return filename;
}

// REGISTER_STACK_PROCESS(StackExport);

bool MeshExport::initialize(ParmList& parms, QWidget* parent)
{
  int stackId = parms[3].toInt();
  if(!checkState().mesh(MESH_ANY, stackId))
    return false;

  QDialog dlg(parent);
  Ui_ExportMeshDialog ui;
  ui.setupUi(&dlg);

  this->ui = &ui;
  this->dlg = &dlg;

  connect(ui.SelectMeshFile, &QAbstractButton::clicked, this, &MeshExport::selectMeshFile);
  connect(ui.MeshType, SIGNAL(currentIndexChanged(const QString &)), this, SLOT(selectMeshType(const QString &)));

  Mesh* m = mesh(stackId);
  QString filename = m->file();
  if(filename.isEmpty())
    filename = parms[0].toString();

  setMeshFile(filename);
  ui.Transform->setChecked(parmToBool(parms[2]));

  bool res = false;
  if(dlg.exec() == QDialog::Accepted) {
    parms[1] = ui.MeshType->currentText();
    parms[0] = ui.MeshFile->text();
    parms[2] = ui.Transform->isChecked();
    res = true;
  }
  this->ui = 0;
  this->dlg = 0;
  return res;
}

void MeshExport::selectMeshType(const QString& type)
{
  QString filename = ui->MeshFile->text();
  if(filename.isEmpty())
    return;
  ui->MeshFile->setText(properFile(filename, type));
}

void MeshExport::selectMeshFile()
{
  QString filename = ui->MeshFile->text();
  QStringList filters;
  filters << "Stanford Polygon file (*.ply)"
          << "MorphoGraphX Mesh (*.mgxm)"
          << "VTK Mesh File (*.vtu)"
          << "Text or cell files (*.txt)"
          << "MeshEdit files (*.mesh)"
          << "STL CAD files (*.stl)"
          << "Wavefront Object files (*.obj)"
          << "All Mesh Files (*.ply *.vtu *.txt *.mesh *.stl *.obj)";
  QString filter;
  {
    QFileInfo fi(filename);
    QString suf = fi.suffix();
    if(suf == "ply")
      filter = filters[0];
    else if(suf == "mgxm")
      filter = filters[1];
    else if(suf == "vtu")
      filter = filters[2];
    else if(suf == "txt")
      filter = filters[3];
    else if(suf == "mesh")
      filter = filters[4];
    else if(suf == "stl")
      filter = filters[5];
    else if(suf == "obj")
      filter = filters[6];
    else
      filter = filters[7];
  }
  filename = QFileDialog::getSaveFileName(dlg, QString("Select mesh file"), filename, filters.join(";;"), &filter,
                                          FileDialogOptions);
  if(!filename.isEmpty())
    setMeshFile(filename);
}

void MeshExport::setMeshFile(const QString& filename)
{
  QFileInfo fi(filename);
  QString suf = fi.suffix();
  // This is nasty, if you add/change anything here you'll have to change the exportmesh.ui file
  if(suf == "ply")
    ui->MeshType->setCurrentIndex(0);
  else if(suf == "mgxm")
    ui->MeshType->setCurrentIndex(2);
  else if(suf == "vtu")
    ui->MeshType->setCurrentIndex(3);
  else if(suf == "txt")
    ui->MeshType->setCurrentIndex(5);
  else if(suf == "mesh")
    ui->MeshType->setCurrentIndex(7);
  else if(suf == "stl")
    ui->MeshType->setCurrentIndex(8);
  else if(suf == "obj")
    ui->MeshType->setCurrentIndex(9);
  else {
    ui->MeshType->setCurrentIndex(0);
    ui->MeshFile->setText(properFile(filename, "PLY Binary"));
    return;
  }
  ui->MeshFile->setText(filename);
}

bool MeshExport::operator()(const ParmList& parms)
{
  int meshId = parms[3].toInt();
  if(!checkState().mesh(STACK_ANY, meshId))
    return false;
  Mesh* mesh = this->mesh(meshId);
  QString type = parms[1].toString();
  if(type != "PLY Binary" and type != "PLY Ascii" and type != "MorphoGraphX Mesh"
     and type != "VTK Mesh Binary" and type != "VTK Mesh Ascii"
     and type != "Text" and type != "Cells" and type != "MeshEdit"
     and type != "STL" and type != "OBJ") {
    setErrorMessage("Error, type must be one of 'PLY Binary', 'PLY Ascii', 'MorphoGraphX Mesh',"
                    " 'VTK Mesh Binary', 'VTK Mesh Ascii', 'Text', 'Cells', 'MeshEdit', 'STL', 'OBJ'.");
    return false;
  }
  bool transform = parmToBool(parms[2]);
  bool res = (*this)(mesh, parms[0].toString(), type, transform);
  return res;
}

bool MeshExport::operator()(Mesh* mesh, const QString& filename, const QString& type, bool transform)
{
  if(filename.isEmpty()) {
    setErrorMessage("Filename is empty, cannot save mesh");
    return false;
  }
  actingFile(filename);
  Progress progress(QString("Saving Mesh %1 in File '%2'").arg(mesh->userId()).arg(filename), 0, false);
  QFileInfo fi(filename);
  QString suf = fi.suffix();
  bool success = false;
  if(type == "PLY Binary")
    success = savePLY(mesh, filename, transform, true);
  else if(type == "PLY Ascii")
    success = savePLY(mesh, filename, transform, false);
  else if(type == "MorphoGraphX Mesh")
    success = saveMGXM(mesh, filename, transform);
  else if(type == "VTK Mesh Binary")
    success = saveVTU(mesh, filename, transform, true);
  else if(type == "VTK Mesh Ascii")
    success = saveVTU(mesh, filename, transform, false);
  else if(type == "Text")
    success = saveText(mesh, filename, transform);
  else if(type == "Cells")
    success = saveCells(mesh, filename, transform);
  else if(type == "MeshEdit")
    success = saveMeshEdit(mesh, filename, transform);
  else if(type == "STL")
    success = saveMeshSTL(mesh, filename, transform);
  else if(type == "OBJ")
    success = saveOBJ(mesh, filename, transform);
  if(success) {
    mesh->setTransformed(transform);
    mesh->updateAll();
  }
  return success;
}

Point3f MeshExport::savedPos(Point3f pos, bool transform, const Stack* stack)
{
  if(transform)
    pos = Point3f(stack->getFrame().inverseCoordinatesOf(Vec(pos)));
  return pos;
}

Point3f MeshExport::savedNormal(Point3f normal, bool transform, const Stack* stack)
{
  if(transform) {
    normal = Point3f(stack->getFrame().inverseTransformOf(Vec(normal)));
    normal.normalize();
  }
  return normal;
}

static QString vtkBinaryEncoding(const char* data, int size)
{
  QByteArray ba = (QByteArray::fromRawData((const char*)&size, 4) + QByteArray::fromRawData(data, size)).toBase64();
  return QString::fromLatin1(ba);
}

template <typename T> static QDomText vtkBinaryEncoding(const std::vector<T>& data, QDomDocument& doc)
{
  QDomText text = doc.createTextNode(vtkBinaryEncoding((const char*)data.data(), data.size() * sizeof(T)));
  return text;
}

QString vtkAsciiEncoding(const int& i) {
  return QString::number(i);
}

QString vtkAsciiEncoding(const float& f) {
  return QString::number(f, 'g', 10);
}

QString vtkAsciiEncoding(const Point3f& p)
{
  return QString("%1 %2 %3").arg(p.x(), 0, 'g', 10).arg(p.y(), 0, 'g', 10).arg(p.z(), 0, 'g', 10);
}

QString vtkAsciiEncoding(const Point2i& p) {
  return QString("%1 %2").arg(p.x()).arg(p.y());
}

QString vtkAsciiEncoding(const Point3i& p) {
  return QString("%1 %2 %3").arg(p.x()).arg(p.y()).arg(p.z());
}

template <typename T> static QDomText vtkAsciiEncoding(const std::vector<T>& values, QDomDocument& doc)
{
  QStringList strings;
#if QT_VERSION >= 0x040700
  strings.reserve(values.size());
#endif
  forall(const T& v, values)
    strings << vtkAsciiEncoding(v);
  return doc.createTextNode(strings.join("\n"));
}

template <typename T> static QDomText vtkEncoding(const std::vector<T>& values, QDomDocument& doc, bool binary)
{
  if(binary)
    return vtkBinaryEncoding(values, doc);
  else
    return vtkAsciiEncoding(values, doc);
}

bool MeshExport::saveOBJ(Mesh* mesh, const QString& filename, bool transform)
{
  QFile file(filename);
  if(!file.open(QIODevice::WriteOnly)) {
    setErrorMessage(QString("MeshExport::Error:Cannot open output file: %1").arg(filename));
    return false;
  }

  QTextStream ts(&file);

  const vvgraph& S = mesh->graph();

  std::unordered_map<vertex, int> inv_vs;

  ts << "# Triangular mesh created by LithoGraphX" << endl;
  ts << QString::fromWCharArray(L"# Length unit: \xb5m") << endl << endl;

  ts << "# Vertices" << endl;

  // write vertices
  int i = 0;
  forall(const vertex& v, S) {
    inv_vs[v] = i + 1;
    ts << "v " << savedPos(v->pos, transform, mesh->stack()) << endl;
    ts << "vn " << normalized(savedPos(v->nrml, transform, mesh->stack())) << endl;
    ++i;
  }

  ts << endl << "# Triangles" << endl;

  // write triangles
  forall(const vertex& v, S) {
    forall(const vertex& n, S.neighbors(v)) {
      const vertex& m = S.nextTo(v, n);
      if(mesh->uniqueTri(v, n, m)) {
        ts << "f " << inv_vs[v] << " " << inv_vs[n] << " " << inv_vs[m] << endl;
      }
    }
  }

  file.close();
  return true;
}

bool MeshExport::saveVTU(Mesh* mesh, const QString& filename, bool transform, bool binary)
{
  QFile file(filename);
  if(!file.open(QIODevice::WriteOnly)) {
    setErrorMessage(QString("MeshExport::Error:Cannot open output file: %1").arg(filename));
    return false;
  }

  QDomDocument doc("VTKFile");
  QDomElement root = doc.createElement("VTKFile");
  root.setAttribute("type", "UnstructuredGrid");
  root.setAttribute("byte_order", "LittleEndian");
  root.setAttribute("version", "0.1");
  doc.appendChild(root);
  // Add the grid
  QDomElement grid = doc.createElement("UnstructuredGrid");
  root.appendChild(grid);
  // Add the pieces
  QDomElement piece = doc.createElement("Piece");
  grid.appendChild(piece);
  const vvgraph& S = mesh->graph();
  piece.setAttribute("NumberOfPoints", (uint)S.size());
  QString format;
  if(binary)
    format = "binary";
  else
    format = "ascii";
  // Points
  QDomElement points = doc.createElement("Points");
  piece.appendChild(points);
  QDomElement points_pos = doc.createElement("DataArray");
  points_pos.setAttribute("NumberOfComponents", "3");
  points_pos.setAttribute("type", "Float32");
  points_pos.setAttribute("format", format);
  std::vector<Point3f> points_pos_data(S.size());
  points.appendChild(points_pos);
  // Points attributes
  QDomElement points_data = doc.createElement("PointData");
  piece.appendChild(points_data);
  QString signal_name;
  if(mesh->signalUnit().isEmpty())
    signal_name = "Signal";
  else
    signal_name = QString("Signal (%1)").arg(mesh->signalUnit());
  points_data.setAttribute("Scalars", signal_name);
  points_data.setAttribute("Normals", "Normals");
  QDomElement points_signal = doc.createElement("DataArray");
  points_signal.setAttribute("Name", signal_name);
  points_signal.setAttribute("type", "Float32");
  points_signal.setAttribute("NumberOfComponents", "1");
  points_signal.setAttribute("format", format);
  std::vector<float> points_signal_data(S.size());
  QDomElement points_normal = doc.createElement("DataArray");
  points_normal.setAttribute("Name", "Normals");
  points_normal.setAttribute("type", "Float32");
  points_normal.setAttribute("NumberOfComponents", "3");
  points_normal.setAttribute("format", format);
  std::vector<Point3f> points_normal_data(S.size());
  QDomElement points_label = doc.createElement("DataArray");
  points_label.setAttribute("Name", "Label");
  points_label.setAttribute("type", "Int32");
  points_label.setAttribute("NumberOfComponents", "1");
  points_label.setAttribute("format", format);
  std::vector<int> points_label_data(S.size());
  points_data.appendChild(points_signal);
  points_data.appendChild(points_normal);
  points_data.appendChild(points_label);
  // Initialize vertex number and counts cells
  int saveId = 0;
  size_t count_cells = 0;
  forall(const vertex& v, S) {
    v->saveId = saveId++;
    forall(const vertex& n, S.neighbors(v)) {
      const vertex& m = S.nextTo(v, n);
      if(mesh->uniqueTri(v, n, m))
        ++count_cells;
    }
  }
  // Cells
  piece.setAttribute("NumberOfCells", (uint)count_cells);
  QDomElement cells = doc.createElement("Cells");
  piece.appendChild(cells);
  QDomElement cells_connectivity = doc.createElement("DataArray");
  cells_connectivity.setAttribute("type", "Int32");
  cells_connectivity.setAttribute("Name", "connectivity");
  cells_connectivity.setAttribute("format", format);
  std::vector<Point3i> cells_connectivity_data(count_cells);
  QDomElement cells_offsets = doc.createElement("DataArray");
  cells_offsets.setAttribute("type", "Int32");
  cells_offsets.setAttribute("Name", "offsets");
  cells_offsets.setAttribute("format", format);
  std::vector<int> cells_offsets_data(count_cells);
  QDomElement cells_types = doc.createElement("DataArray");
  cells_types.setAttribute("type", "UInt8");
  cells_types.setAttribute("Name", "types");
  cells_types.setAttribute("format", format);
  std::vector<quint8> cells_types_data(count_cells, quint8(5));
  cells.appendChild(cells_connectivity);
  cells.appendChild(cells_offsets);
  cells.appendChild(cells_types);
  // Cells attributes
  QDomElement cells_data = doc.createElement("CellData");
  piece.appendChild(cells_data);
  QDomElement cells_label = doc.createElement("DataArray");
  cells_label.setAttribute("Name", "Label");
  cells_label.setAttribute("type", "Int32");
  cells_label.setAttribute("NumberOfComponents", "1");
  cells_label.setAttribute("format", format);
  cells_data.appendChild(cells_label);
  std::vector<int> cells_label_data(count_cells);
  QDomElement cells_heat = doc.createElement("DataArray");
  const IntFloatMap& mesh_cell_heat = mesh->labelHeat();
  const IntIntFloatMap& mesh_wall_heat = mesh->wallHeat();
  bool has_cell_heat = !mesh_cell_heat.empty();
  bool has_wall_heat = !mesh_wall_heat.empty();
  QString heat_name = "Heat";
  if(has_cell_heat)
    heat_name = "Cell " + heat_name;
  else if(has_wall_heat)
    heat_name = "Wall " + heat_name;
  if(!mesh->heatMapUnit().isEmpty())
    heat_name += QString(" (%1)").arg(mesh->heatMapUnit());

  float minHeat = mesh->heatMapBounds()[0];
  float maxHeat = mesh->heatMapBounds()[1];

  cells_heat.setAttribute("Name", heat_name);
  cells_heat.setAttribute("type", "Float32");
  cells_heat.setAttribute("NumberOfComponents", "1");
  cells_heat.setAttribute("format", format);
  cells_heat.setAttribute("RangeMin", minHeat);
  cells_heat.setAttribute("RangeMax", maxHeat);
  std::vector<float> cells_heat_data;
  if(has_cell_heat or has_wall_heat) {
    cells_heat_data.resize(count_cells);
    cells_data.appendChild(cells_heat);
    cells_data.setAttribute("Scalars", heat_name);
  } else
    cells_data.setAttribute("Scalars", "Label");

  std::unordered_set<int> labels;

  // Fills in everything
  int cell_id = 0;
  forall(const vertex& v, S) {
    int id = v->saveId;
    Point3f pos = savedPos(v->pos, transform, mesh->stack());
    Point3f nrml = normalized(savedPos(v->nrml, transform, mesh->stack()));
    points_pos_data[id] = pos;
    points_label_data[id] = v->label;
    points_signal_data[id] = v->signal;
    points_normal_data[id] = nrml;
    forall(const vertex& n, S.neighbors(v)) {
      const vertex& m = S.nextTo(v, n);
      if(mesh->uniqueTri(v, n, m)) {
        cells_connectivity_data[cell_id] = Point3i(id, n->saveId, m->saveId);
        cells_offsets_data[cell_id] = 3 * (cell_id + 1);
        int label = mesh->getLabel(v, n, m);
        labels.insert(label);
        cells_label_data[cell_id] = label;
        if(has_cell_heat) {
          IntFloatMap::const_iterator found = mesh_cell_heat.find(label);
          if(found != mesh_cell_heat.end())
            cells_heat_data[cell_id] = found->second;
          else
            cells_heat_data[cell_id] = std::numeric_limits<float>::quiet_NaN();
        } else if(has_wall_heat) {
          IntIntPair wall;
          if(mesh->isBordTriangle(label, v, n, m, wall)) {
            IntIntFloatMap::const_iterator found = mesh_wall_heat.find(wall);
            if(found != mesh_wall_heat.end())
              cells_heat_data[cell_id] = found->second;
            else
              cells_heat_data[cell_id] = std::numeric_limits<float>::quiet_NaN();
          } else
            cells_heat_data[cell_id] = std::numeric_limits<float>::quiet_NaN();
        }
        cell_id++;
      }
    }
  }

  // Global fields
  QDomElement global_fields = doc.createElement("FieldData");
  grid.appendChild(global_fields);

  QDomElement scale = doc.createElement("DataArray");
  scale.setAttribute("Name", "Scale");
  scale.setAttribute("type", "Float32");
  scale.setAttribute("NumberOfTuples", 1);
  scale.setAttribute("format", "ascii");
  scale.appendChild(doc.createTextNode("1e-6"));
  global_fields.appendChild(scale);

  QDomElement labels_field = doc.createElement("DataArray");
  labels_field.setAttribute("Name", "Labels");
  labels_field.setAttribute("type", "Int32");
  labels_field.setAttribute("NumberOfTuples", (int)labels.size());
  labels_field.setAttribute("format", format);
  std::vector<int> labels_fields_data(labels.begin(), labels.end());
  std::sort(labels_fields_data.begin(), labels_fields_data.end());
  labels_field.appendChild(vtkEncoding(labels_fields_data, doc, binary));
  global_fields.appendChild(labels_field);

  if(has_cell_heat) {
    QDomElement cell_heat = doc.createElement("DataArray");
    QString cell_heat_name = "Cell Heat";
    if(!mesh->heatMapUnit().isEmpty())
      cell_heat_name += QString(" (%1)").arg(mesh->heatMapUnit());
    cell_heat.setAttribute("Name", cell_heat_name);
    cell_heat.setAttribute("type", "Float32");
    cell_heat.setAttribute("NumberOfTuples", (int)labels.size());
    cell_heat.setAttribute("format", format);
    cell_heat.setAttribute("RangeMin", minHeat);
    cell_heat.setAttribute("RangeMax", maxHeat);
    std::vector<float> cell_heat_data(labels.size());
    for(size_t i = 0; i < labels.size(); ++i) {
      int label = labels_fields_data[i];
      IntFloatMap::const_iterator found = mesh_cell_heat.find(label);
      if(found == mesh_cell_heat.end())
        cell_heat_data[i] = std::numeric_limits<float>::quiet_NaN();
      else
        cell_heat_data[i] = found->second;
    }
    cell_heat.appendChild(vtkEncoding(cell_heat_data, doc, binary));
    global_fields.appendChild(cell_heat);
  } else if(has_wall_heat) {
    QDomElement walls_fields = doc.createElement("DataArray");
    walls_fields.setAttribute("Name", "Walls");
    walls_fields.setAttribute("type", "Int32");
    walls_fields.setAttribute("NumberOfTuples", (int)mesh_wall_heat.size());
    walls_fields.setAttribute("format", format);
    std::vector<Point2i> walls_fields_data(mesh_wall_heat.size());

    QDomElement wall_heat = doc.createElement("DataArray");
    QString wall_heat_name = "Wall Heat";
    if(!mesh->heatMapUnit().isEmpty())
      wall_heat_name += QString(" (%1)").arg(mesh->heatMapUnit());
    wall_heat.setAttribute("Name", wall_heat_name);
    wall_heat.setAttribute("type", "Float32");
    wall_heat.setAttribute("NumberOfTuples", (int)mesh_wall_heat.size());
    wall_heat.setAttribute("format", format);
    wall_heat.setAttribute("RangeMin", minHeat);
    wall_heat.setAttribute("RangeMax", maxHeat);
    std::vector<float> wall_heat_data(labels.size());
    int i = 0;
    forall(const IntIntFloatPair& vs, mesh_wall_heat) {
      const IntIntPair& w = vs.first;
      float value = vs.second;
      walls_fields_data[i] = Point2i(w.first, w.second);
      wall_heat_data[i] = value;
      ++i;
    }
    walls_fields.appendChild(vtkEncoding(walls_fields_data, doc, binary));
    wall_heat.appendChild(vtkEncoding(wall_heat_data, doc, binary));
    global_fields.appendChild(walls_fields);
    global_fields.appendChild(wall_heat);
  }

  points_pos.appendChild(vtkEncoding(points_pos_data, doc, binary));
  points_label.appendChild(vtkEncoding(points_label_data, doc, binary));
  points_signal.appendChild(vtkEncoding(points_signal_data, doc, binary));
  points_normal.appendChild(vtkEncoding(points_normal_data, doc, binary));

  cells_connectivity.appendChild(vtkEncoding(cells_connectivity_data, doc, binary));
  cells_offsets.appendChild(vtkEncoding(cells_offsets_data, doc, binary));
  cells_types.appendChild(vtkEncoding(cells_types_data, doc, binary));

  if(has_cell_heat or has_wall_heat)
    cells_heat.appendChild(vtkEncoding(cells_heat_data, doc, binary));

  cells_label.appendChild(vtkEncoding(cells_label_data, doc, binary));

  QString xmlString = doc.toString();
  QByteArray xmlArray = xmlString.toUtf8();
  qint64 qty = file.write(xmlArray);
  if(qty != xmlArray.size())
    throw QString("Could not write data to file (written = %1, expected = %2)").arg(qty).arg(xmlArray.size());

  file.close();

  return true;
}

bool MeshExport::saveText(Mesh* mesh, const QString& filename, bool transform)
{
  const Stack* stack = mesh->stack();
  QFile file(filename);
  if(!file.open(QIODevice::WriteOnly)) {
    setErrorMessage(QString("MeshExport::Error:Cannot open output file: %1").arg(filename));
    return false;
  }

  const vvgraph& S = mesh->graph();

  QTextStream out(&file);
  int saveId = 0;
  int i = 0;

  out << S.size() << endl;

  Progress progress(QString("Saving Text Mesh %1 in File '%2'").arg(mesh->userId()).arg(filename), S.size() * 2);
  int interval = S.size() / 100;
  if(interval < 1)
    interval = 1;
  forall(const vertex& v, S) {
    v->saveId = saveId++;
    Point3f pos = savedPos(v->pos, transform, stack);
    out << v->saveId << " " << pos << " " << v->label << endl;
    if((i % interval == 0)and !progress.advance(i))
      userCancel();
    ++i;
  }
  forall(const vertex& v, S) {
    out << v->saveId << " " << S.valence(v);
    forall(const vertex& n, S.neighbors(v))
      out << " " << n->saveId;
    out << endl;
    if((i % interval == 0)and !progress.advance(i))
      userCancel();
    ++i;
  }
  if(!progress.advance(S.size() * 2))
    userCancel();
  file.close();
  SETSTATUS("Saved mesh, file:" << mesh->file() << ", vertices:" << S.size());
  return true;
}

bool MeshExport::saveCells(Mesh* mesh, const QString& filename, bool transform)
{
  const Stack* stack = mesh->stack();
  QFile file(filename);
  if(!file.open(QIODevice::WriteOnly)) {
    setErrorMessage(QString("MeshExport::Error:Cannot open output file: %1").arg(filename));
    return false;
  }

  const vvgraph& S = mesh->graph();

  QTextStream out(&file);
  int saveId = 0;
  Progress progress(QString("Saving Cells %1 in File '%2'").arg(mesh->userId()).arg(filename), S.size() * 2);
  out << S.size() << endl;
  int interval = S.size() / 100;
  if(interval < 1)
    interval = 1;
  int i = 0;
  forall(const vertex& v, S) {
    v->saveId = saveId++;
    Point3f pos = savedPos(v->pos, transform, stack);
    out << v->saveId << " " << pos << " " << v->label << " " << v->type << endl;
    if((i % interval == 0)and !progress.advance(i))
      userCancel();
    ++i;
  }
  forall(const vertex& v, S) {
    out << v->saveId << " " << S.valence(v);
    forall(const vertex& n, S.neighbors(v))
      out << " " << n->saveId;
    out << endl;
    if((i % interval == 0)and !progress.advance(i))
      userCancel();
    ++i;
  }
  if(!progress.advance(S.size() * 2))
    userCancel();
  SETSTATUS("Saved cellular mesh, file:" << mesh->file() << ", vertices:" << S.size());
  return true;
}

bool MeshExport::saveMeshEdit(Mesh* mesh, const QString& filename, bool transform)
{
  const Stack* stack = mesh->stack();
  QFile file(filename);
  if(!file.open(QIODevice::WriteOnly)) {
    setErrorMessage(QString("MeshExport::Error:Cannot open output file: %1").arg(filename));
    return false;
  }

  const vvgraph& S = mesh->graph();

  QTextStream out(&file);
  int saveId = 1;
  out << "MeshVersionFormatted 1" << endl;
  out << "Dimension 3" << endl;
  out << "Vertices" << endl;
  out << S.size() << endl;
  Progress progress(QString("Saving Mesh Edit %1 in File '%2'").arg(mesh->userId()).arg(filename), S.size());
  int i = 0;
  int interval = S.size() / 100;
  if(interval < 1)
    interval = 1;
  forall(const vertex& v, S) {
    v->saveId = saveId++;
    Point3f pos = savedPos(v->pos, transform, stack);
    out << pos << " " << v->label << endl;
  }
  // Count the triangles
  int count = 0;
  forall(const vertex& v, S)
    forall(const vertex& n, S.neighbors(v)) {
      vertex m = S.nextTo(v, n);
      if(!mesh->uniqueTri(v, n, m))
        continue;
      count++;
    }
  out << "Triangles" << endl;
  out << count << endl;
  forall(const vertex& v, S) {
    forall(const vertex& n, S.neighbors(v)) {
      vertex m = S.nextTo(v, n);
      if(!mesh->uniqueTri(v, n, m))
        continue;
      out << v->saveId << " " << n->saveId << " " << m->saveId << " ";
      out << mesh->getLabel(v, n, m) << endl;
    }
    if((i % interval == 0)and !progress.advance(i))
      userCancel();
  }
  if(!progress.advance(S.size()))
    userCancel();
  out << "End" << endl;
  SETSTATUS("Saving triangle mesh, file:" << mesh->file() << ", vertices:" << S.size());
  return true;
}

bool MeshExport::saveMeshSTL(Mesh* mesh, const QString& filename, bool transform)
{
  const Stack* stack = mesh->stack();
  QFile file(filename);
  if(!file.open(QIODevice::WriteOnly)) {
    setErrorMessage(QString("MeshExport::Error:Cannot open output file: %1").arg(filename));
    return false;
  }

  const vvgraph& S = mesh->graph();

  QTextStream out(&file);
  Progress progress(QString("Saving STL Mesh %1 in File '%2'").arg(mesh->userId()).arg(filename), S.size());
  int i = 0;
  int interval = S.size() / 100;
  if(interval < 1)
    interval = 1;

  out << "solid lgx" << endl;
  const std::vector<vertex>& av = mesh->activeVertices();
  forall(const vertex& v, av) {
    forall(const vertex& n, S.neighbors(v)) {
      vertex m = S.nextTo(v, n);
      if(!mesh->uniqueTri(v, n, m))
        continue;
      Point3f vpos = savedPos(v->pos, transform, stack);
      Point3f npos = savedPos(n->pos, transform, stack);
      Point3f mpos = savedPos(m->pos, transform, stack);
      Point3f nrml = ((npos - vpos) ^ (mpos - vpos)).normalize();
      out << "facet normal " << nrml.x() << " " << nrml.y() << " " << nrml.z() << endl;
      out << "  outer loop" << endl;
      out << "    vertex " << vpos.x() << " " << vpos.y() << " " << vpos.z() << endl;
      out << "    vertex " << npos.x() << " " << npos.y() << " " << npos.z() << endl;
      out << "    vertex " << mpos.x() << " " << mpos.y() << " " << mpos.z() << endl;
      out << "  endloop" << endl;
      out << "endfacet" << endl;
    }
    if((i % interval == 0)and !progress.advance(i))
      userCancel();
  }
  out << "endsolid lgx" << endl;
  if(!progress.advance(S.size()))
    userCancel();
  SETSTATUS("Saving STL mesh, file:" << mesh->file() << ", vertices:" << S.size());
  return true;
}

bool MeshExport::saveMGXM(Mesh* mesh, const QString& filename, bool transform)
{
  const Stack* stack = mesh->stack();
  actingFile(filename);
  QFile file(filename);
  if(!file.open(QIODevice::WriteOnly)) {
    setErrorMessage(QString("MeshExport::Error:Cannot open output file: %1").arg(filename));
    return false;
  }

  const vvgraph& S = mesh->graph();

  qint64 qty;

  file.write("MGXM 1.3 ", 9);
  bool scale = true;
  bool is_cells = mesh->cells();
  qty = file.write((char*)&is_cells, sizeof(bool));
  if(qty != sizeof(bool))
    throw QString("Could not write data to file (written = %1, expected = %2)").arg(qty).arg(sizeof(bool));
  qty = file.write((char*)&scale, sizeof(bool));
  if(qty != sizeof(bool))
    throw QString("Could not write data to file (written = %1, expected = %2)").arg(qty).arg(sizeof(bool));
  qty = file.write((char*)&transform, sizeof(bool));
  if(qty != sizeof(bool))
    throw QString("Could not write data to file (written = %1, expected = %2)").arg(qty).arg(sizeof(bool));

  Point2f signalBounds = mesh->signalBounds();
  qty = file.write((char*)signalBounds.data(), sizeof(float));
  if(qty != sizeof(float))
    throw QString("Could not write data to file (written = %1, expected = %2)").arg(qty).arg(sizeof(float));
  qty = file.write((char*)&signalBounds[1], sizeof(float));
  if(qty != sizeof(float))
    throw QString("Could not write data to file (written = %1, expected = %2)").arg(qty).arg(sizeof(float));

  QByteArray ba = mesh->signalUnit().toUtf8();
  uint stringSize = ba.size();
  qty = file.write((char*)&stringSize, sizeof(uint));
  if(qty != sizeof(uint))
    throw QString("Could not write data to file (written = %1, expected = %2)").arg(qty).arg(sizeof(uint));
  qty = file.write(ba.data(), stringSize);
  if(qty != stringSize)
    throw QString("Could not write data to file (written = %1, expected = %2)").arg(qty).arg(stringSize);

  static const uint size_vertex = sizeof(int) + sizeof(float) + sizeof(char) + sizeof(bool);
  uint header[] = { 0, (uint)S.size(), size_vertex, 0 };
  qty = file.write((char*)header, 16);
  if(qty != 16)
    throw QString("Could not write data to file (written = %1, expected = %2)").arg(qty).arg(16);

  uint saveId = 0;
  Progress progress(QString("Saving Mesh %1 in File '%2'").arg(mesh->userId()).arg(filename), S.size() * 2);
  int interval = S.size() / 100;
  if(interval < 1)
    interval = 1;
  int i = 0;
  forall(const vertex& v, S) {
    v->saveId = saveId++;
    qty = file.write((char*)&v->saveId, sizeof(uint));
    if(qty != sizeof(uint))
      throw QString("Could not write data to file (written = %1, expected = %2)").arg(qty).arg(sizeof(uint));
    Point3f pos = savedPos(v->pos, transform, stack);
    qty = file.write((char*)&pos, sizeof(Point3f));
    if(qty != sizeof(Point3f))
      throw QString("Could not write data to file (written = %1, expected = %2)").arg(qty).arg(sizeof(Point3f));
    qty = file.write((char*)&v->label, sizeof(int));
    if(qty != sizeof(int))
      throw QString("Could not write data to file (written = %1, expected = %2)").arg(qty).arg(sizeof(int));
    qty = file.write((char*)&v->signal, sizeof(float));
    if(qty != sizeof(float))
      throw QString("Could not write data to file (written = %1, expected = %2)").arg(qty).arg(sizeof(float));
    qty = file.write((char*)&v->type, sizeof(char));
    if(qty != sizeof(char))
      throw QString("Could not write data to file (written = %1, expected = %2)").arg(qty).arg(sizeof(char));
    qty = file.write((char*)&v->selected, sizeof(bool));
    if(qty != sizeof(bool))
      throw QString("Could not write data to file (written = %1, expected = %2)").arg(qty).arg(sizeof(bool));

    if((i % interval == 0)and !progress.advance(i))
      userCancel();
    ++i;
  }
  forall(const vertex& v, S) {
    qty = file.write((char*)&v->saveId, sizeof(uint));
    if(qty != sizeof(uint))
      throw QString("Could not write data to file (written = %1, expected = %2)").arg(qty).arg(sizeof(uint));
    uint val = S.valence(v);
    qty = file.write((char*)&val, sizeof(uint));
    if(qty != sizeof(uint))
      throw QString("Could not write data to file (written = %1, expected = %2)").arg(qty).arg(sizeof(uint));
    forall(const vertex& n, S.neighbors(v))
      qty = file.write((char*)&n->saveId, sizeof(uint));
    if(qty != sizeof(uint))
      throw QString("Could not write data to file (written = %1, expected = %2)").arg(qty).arg(sizeof(uint));
    if((i % interval == 0)and !progress.advance(i))
      userCancel();
    ++i;
  }
  if(!progress.advance(S.size() * 2))
    userCancel();
  file.close();
  mesh->setFile(stripCurrentDir(filename));
  SETSTATUS("Saved mesh, file:" << mesh->file() << ", vertices:" << S.size());
  return true;
}

bool MeshExport::savePLY(Mesh* mesh, const QString& filename, bool transform, bool binary)
{
  const Stack* stack = mesh->stack();

  const vvgraph& S = mesh->graph();

  size_t nb_vertices = 0u, nb_faces = 0u;
  size_t max_vertex_per_face = 3u;

  std::unordered_set<int> labels;

  if(mesh->cells()) {
    forall(const vertex& v, S) {
      if(v->label > 0)
        labels.insert(v->label);
      if(v->type == 'c') {
        v->saveId = nb_faces++;
        max_vertex_per_face = std::max(max_vertex_per_face, S.valence(v));
      } else if(v->type == 'j')
        v->saveId = nb_vertices++;
      else {
        setErrorMessage(QString("MeshExport::Error:Bad vertex type:%1").arg(v->type));
        return false;
      }
    }
  } else {
    forall(const vertex& v, S) {
      if(v->label > 0)
        labels.insert(v->label);
      v->saveId = nb_vertices++;
      forall(const vertex& n, S.neighbors(v)) {
        const vertex& m = S.nextTo(v, n);
        if(!mesh->uniqueTri(v, n, m))
          continue;
        ++nb_faces;
      }
    }
  }

  auto nb_labels = labels.size();

  Information::out << "Found " << nb_vertices << " vertices, " << nb_faces << " faces and " << nb_labels << " labels." << endl;

  Progress progress(QString("Saving Cell Mesh in File '%1'").arg(filename), nb_vertices + nb_faces + nb_labels + 2);

  PlyFile ply;

  ply.init(binary ? PlyFile::BINARY_LITTLE_ENDIAN : PlyFile::ASCII);

  // First, create elements
  auto* globalElement = ply.createElement("global", 1);
  if(not globalElement)
    return setErrorMessage("Coudnl't create 'global' element of the PLY object.");
  auto* vertexElement = ply.createElement("vertex", nb_vertices);
  if(not vertexElement)
    return setErrorMessage("Coudnl't create 'vertex' element of the PLY object.");
  auto* faceElement = ply.createElement("face", nb_faces);
  if(not faceElement)
    return setErrorMessage("Coudnl't create 'face' element of the PLY object.");
  auto* labelElement = ply.createElement("label", nb_labels);

  // Add global properties
  auto *typeProp = globalElement->createList("type", PlyFile::UINT, PlyFile::CHAR);
  if(not typeProp)
    return setErrorMessage("Couldn't create 'type' property in 'global' element of the PLY object.");
  auto *cell_meshProp = globalElement->createValue("cell_mesh", PlyFile::UCHAR);
  if(not cell_meshProp)
    return setErrorMessage("Couldn't create 'cell_mesh' property in 'global' element of the PLY object.");
  auto* orientedProp = globalElement->createValue("oriented", PlyFile::UCHAR);
  if(not orientedProp)
    return setErrorMessage("Couldn't create 'oriented' property in 'global' element of the PLY object.");
  auto* multiple_vertexProp = globalElement->createValue("multiple_vertex", PlyFile::UCHAR);
  if(not multiple_vertexProp)
    return setErrorMessage("Couldn't create 'multiple_vertex' property in 'global' element of the PLY object.");
  auto* extra_elementsProp = globalElement->createValue("extra_elements", PlyFile::UCHAR);
  if(not extra_elementsProp)
    return setErrorMessage("Couldn't create 'extra_elementsProp' property in 'global' element of the PLY object.");
  auto* length_unitProp = globalElement->createValue("length_unit", PlyFile::FLOAT);
  if(not length_unitProp)
    return setErrorMessage("Couldn't create 'length_unit' property in 'global' element of the PLY object.");

  auto* signal_unitProp = globalElement->createList("signal_unit", PlyFile::UINT, PlyFile::CHAR);
  if(not signal_unitProp)
    return setErrorMessage("Couldn't create 'signal_unit' property in 'global' element of the PLY object.");
  auto* signal_descriptionProp = globalElement->createList("signal_description", PlyFile::UINT, PlyFile::CHAR);
  if(not signal_descriptionProp)
    return setErrorMessage("Couldn't create 'signal_description' property in 'global' element of the PLY object.");
  auto* signal_lowProp = globalElement->createValue("signal_low", PlyFile::FLOAT);
  if(not signal_lowProp)
    return setErrorMessage("Couldn't create 'signal_low' property in 'global' element of the PLY object.");
  auto* signal_highProp = globalElement->createValue("signal_high", PlyFile::FLOAT);
  if(not signal_highProp)
    return setErrorMessage("Couldn't create 'signal_high' property in 'global' element of the PLY object.");

  auto* heat_descriptionProp = globalElement->createList("heat_description", PlyFile::UINT, PlyFile::CHAR);
  if(not heat_descriptionProp)
    return setErrorMessage("Couldn't create 'heat_description' property in 'global' element of the PLY object.");
  auto* heat_unitProp = globalElement->createList("heat_unit", PlyFile::UINT, PlyFile::CHAR);
  if(not heat_unitProp)
    return setErrorMessage("Couldn't create 'heat_unit' property in 'global' element of the PLY object.");
  auto* heat_lowProp = globalElement->createValue("heat_low", PlyFile::FLOAT);
  if(not heat_lowProp)
    return setErrorMessage("Couldn't create 'heat_low' property in 'global' element of the PLY object.");
  auto* heat_highProp = globalElement->createValue("heat_high", PlyFile::FLOAT);
  if(not heat_highProp)
    return setErrorMessage("Couldn't create 'heat_high' property in 'global' element of the PLY object.");

  auto* cellAxis_descriptionProp = globalElement->createList("cellAxis_description", PlyFile::UINT, PlyFile::CHAR);
  if(not cellAxis_descriptionProp)
    return setErrorMessage("Couldn't create 'cellAxis_description' property in 'global' element of the PLY object.");
  auto* cellAxis_unitProp = globalElement->createList("cellAxis_unit", PlyFile::UINT, PlyFile::CHAR);
  if(not cellAxis_unitProp)
    return setErrorMessage("Couldn't create 'cellAxis_unit' property in 'global' element of the PLY object.");
  auto* cellAxis_parentProp = globalElement->createValue("cellAxis_parent", PlyFile::UCHAR);
  if(not cellAxis_parentProp)
    return setErrorMessage("Couldn't create 'cellAxis_parent' property in 'global' element of the PLY object.");

  // Add vertex properties
  auto* vertex_signalProp = vertexElement->createValue("signal", PlyFile::FLOAT);
  if(not vertex_signalProp)
    return setErrorMessage("Couldn't create 'signal' property in 'vertex' element of the PLY object.");
  auto* vertex_xProp = vertexElement->createValue("x", PlyFile::FLOAT);
  if(not vertex_xProp)
    return setErrorMessage("Couldn't create 'x' property in 'vertex' element of the PLY object.");
  auto* vertex_yProp = vertexElement->createValue("y", PlyFile::FLOAT);
  if(not vertex_yProp)
    return setErrorMessage("Couldn't create 'y' property in 'vertex' element of the PLY object.");
  auto* vertex_zProp = vertexElement->createValue("z", PlyFile::FLOAT);
  if(not vertex_zProp)
    return setErrorMessage("Couldn't create 'z' property in 'vertex' element of the PLY object.");
  auto* vertex_nxProp = vertexElement->createValue("nx", PlyFile::FLOAT);
  if(not vertex_nxProp)
    return setErrorMessage("Couldn't create 'nx' property in 'vertex' element of the PLY object.");
  auto* vertex_nyProp = vertexElement->createValue("ny", PlyFile::FLOAT);
  if(not vertex_nyProp)
    return setErrorMessage("Couldn't create 'ny' property in 'vertex' element of the PLY object.");
  auto* vertex_nzProp = vertexElement->createValue("nz", PlyFile::FLOAT);
  if(not vertex_nzProp)
    return setErrorMessage("Couldn't create 'nz' property in 'vertex' element of the PLY object.");
  auto* vertex_selProp = vertexElement->createValue("selected", PlyFile::UCHAR);
  if(not vertex_selProp)
    return setErrorMessage("Couldn't create 'selected' property in 'vertex' element of the PLY object.");
  PlyFile::Property *vertex_labelProp = nullptr;
  if(not mesh->cells()) {
    vertex_labelProp = vertexElement->createValue("label", PlyFile::INT);
    if(not vertex_labelProp)
      return setErrorMessage("Couldn't create 'label' property in 'vertex' element of the PLY object.");
  }

  // Add faces properties
  auto *face_vertex_indexProp = faceElement->createList("vertex_index",
                                                        PlyFile::unsignedIntType(max_vertex_per_face),
                                                        PlyFile::UINT);
  if(not face_vertex_indexProp)
    return setErrorMessage("Couldn't create 'vertex_index' property in 'face' element of the PLY object.");

  auto* face_labelProp = faceElement->createValue("label", PlyFile::INT);
  if(not face_labelProp)
    return setErrorMessage("Couldn't create 'label' property in 'face' element of the PLY object.");

  // Add optional properties for cell mesh only
  PlyFile::Property *face_areaProp = nullptr, *face_signalProp = nullptr,
                    *face_xProp = nullptr, *face_yProp = nullptr, *face_zProp = nullptr,
                    *face_nxProp = nullptr, *face_nyProp = nullptr, *face_nzProp = nullptr;
  if(mesh->cells()) {
    face_signalProp = faceElement->createValue("signal", PlyFile::FLOAT);
    if(not face_signalProp)
      return setErrorMessage("Couldn't create 'signal' property in 'face' element of the PLY object.");
    face_areaProp = faceElement->createValue("area", PlyFile::FLOAT);
    if(not face_areaProp)
      return setErrorMessage("Couldn't create 'area' property in 'face' element of the PLY object.");
    face_xProp = faceElement->createValue("x", PlyFile::FLOAT);
    if(not face_xProp)
      return setErrorMessage("Couldn't create 'x' property in 'face' element of the PLY object.");
    face_yProp = faceElement->createValue("y", PlyFile::FLOAT);
    if(not face_yProp)
      return setErrorMessage("Couldn't create 'y' property in 'face' element of the PLY object.");
    face_zProp = faceElement->createValue("z", PlyFile::FLOAT);
    if(not face_zProp)
      return setErrorMessage("Couldn't create 'z' property in 'face' element of the PLY object.");
    face_nxProp = faceElement->createValue("nx", PlyFile::FLOAT);
    if(not face_nxProp)
      return setErrorMessage("Couldn't create 'nx' property in 'face' element of the PLY object.");
    face_nyProp = faceElement->createValue("ny", PlyFile::FLOAT);
    if(not face_nyProp)
      return setErrorMessage("Couldn't create 'ny' property in 'face' element of the PLY object.");
    face_nzProp = faceElement->createValue("nz", PlyFile::FLOAT);
    if(not face_nzProp)
      return setErrorMessage("Couldn't create 'nz' property in 'face' element of the PLY object.");
  }

  // Add labels property
  auto label_labelProp = labelElement->createValue("label", PlyFile::INT);
  if(not label_labelProp)
    return setErrorMessage("Couldn't create 'label' property in 'label' element of the PLY object.");

  PlyFile::Property *label_heatProp = nullptr, *label_parentProp = nullptr,
                    *label_xProp = nullptr, *label_yProp = nullptr, *label_zProp = nullptr,
                    *label_xVisProp = nullptr, *label_yVisProp = nullptr, *label_zVisProp = nullptr,
                    *label_nxProp = nullptr, *label_nyProp = nullptr, *label_nzProp = nullptr,
                    *label_nxVisProp = nullptr, *label_nyVisProp = nullptr, *label_nzVisProp = nullptr,
                    *label_cellAxis_ev1XProp = nullptr, *label_cellAxis_ev1YProp = nullptr, *label_cellAxis_ev1ZProp = nullptr,
                    *label_cellAxis_ev2XProp = nullptr, *label_cellAxis_ev2YProp = nullptr, *label_cellAxis_ev2ZProp = nullptr,
                    *label_cellAxis_evals1Prop = nullptr, *label_cellAxis_evals2Prop = nullptr, *label_cellAxis_evals3Prop = nullptr;
  if(not mesh->labelHeat().empty()) {
    label_heatProp = labelElement->createValue("heat", PlyFile::FLOAT);
    if(not label_heatProp)
      return setErrorMessage("Couldn't create 'heat' property in 'label' element of the PLY object.");
  }
  if(not mesh->labelCenter().empty()) {
    label_xProp = labelElement->createValue("x", PlyFile::FLOAT);
    if(not label_xProp)
      return setErrorMessage("Couldn't create 'x' property in 'label' element of the PLY object.");
    label_yProp = labelElement->createValue("y", PlyFile::FLOAT);
    if(not label_yProp)
      return setErrorMessage("Couldn't create 'y' property in 'label' element of the PLY object.");
    label_zProp = labelElement->createValue("z", PlyFile::FLOAT);
    if(not label_zProp)
      return setErrorMessage("Couldn't create 'z' property in 'label' element of the PLY object.");
  }
  if(not mesh->labelCenterVis().empty()) {
    label_xVisProp = labelElement->createValue("xVis", PlyFile::FLOAT);
    if(not label_xVisProp)
      return setErrorMessage("Couldn't create 'xVis' property in 'label' element of the PLY object.");
    label_yVisProp = labelElement->createValue("yVis", PlyFile::FLOAT);
    if(not label_yVisProp)
      return setErrorMessage("Couldn't create 'yVis' property in 'label' element of the PLY object.");
    label_zVisProp = labelElement->createValue("zVis", PlyFile::FLOAT);
    if(not label_zVisProp)
      return setErrorMessage("Couldn't create 'zVis' property in 'label' element of the PLY object.");
  }
  if(not mesh->labelNormal().empty()) {
    label_nxProp = labelElement->createValue("nx", PlyFile::FLOAT);
    if(not label_nxProp)
      return setErrorMessage("Couldn't create 'nx' property in 'label' element of the PLY object.");
    label_nyProp = labelElement->createValue("ny", PlyFile::FLOAT);
    if(not label_nyProp)
      return setErrorMessage("Couldn't create 'ny' property in 'label' element of the PLY object.");
    label_nzProp = labelElement->createValue("nz", PlyFile::FLOAT);
    if(not label_nzProp)
      return setErrorMessage("Couldn't create 'nz' property in 'label' element of the PLY object.");
  }
  if(not mesh->labelNormalVis().empty()) {
    label_nxVisProp = labelElement->createValue("nxVis", PlyFile::FLOAT);
    if(not label_nxVisProp)
      return setErrorMessage("Couldn't create 'nxVis' property in 'label' element of the PLY object.");
    label_nyVisProp = labelElement->createValue("nyVis", PlyFile::FLOAT);
    if(not label_nyVisProp)
      return setErrorMessage("Couldn't create 'nyVis' property in 'label' element of the PLY object.");
    label_nzVisProp = labelElement->createValue("nzVis", PlyFile::FLOAT);
    if(not label_nzVisProp)
      return setErrorMessage("Couldn't create 'nzVis' property in 'label' element of the PLY object.");
  }
  if(not mesh->parentLabelMap().empty()) {
    label_parentProp = labelElement->createValue("parent", PlyFile::INT);
    if(not label_parentProp)
      return setErrorMessage("Couldn't create 'parent' property in 'label' element of the PLY object.");
  }
  if(not mesh->cellAxis().empty()) {
    label_cellAxis_ev1XProp = labelElement->createValue("ev1X", PlyFile::FLOAT);
    if(not label_cellAxis_ev1XProp)
      return setErrorMessage("Couldn't create 'ev1X' property in 'label' element of the PLY object.");
    label_cellAxis_ev1YProp = labelElement->createValue("ev1Y", PlyFile::FLOAT);
    if(not label_cellAxis_ev1YProp)
      return setErrorMessage("Couldn't create 'ev1Y' property in 'label' element of the PLY object.");
    label_cellAxis_ev1ZProp = labelElement->createValue("ev1Z", PlyFile::FLOAT);
    if(not label_cellAxis_ev1ZProp)
      return setErrorMessage("Couldn't create 'ev1Z' property in 'label' element of the PLY object.");

    label_cellAxis_ev2XProp = labelElement->createValue("ev2X", PlyFile::FLOAT);
    if(not label_cellAxis_ev2XProp)
      return setErrorMessage("Couldn't create 'ev2X' property in 'label' element of the PLY object.");
    label_cellAxis_ev2YProp = labelElement->createValue("ev2Y", PlyFile::FLOAT);
    if(not label_cellAxis_ev2YProp)
      return setErrorMessage("Couldn't create 'ev2Y' property in 'label' element of the PLY object.");
    label_cellAxis_ev2ZProp = labelElement->createValue("ev2Z", PlyFile::FLOAT);
    if(not label_cellAxis_ev2ZProp)
      return setErrorMessage("Couldn't create 'ev2Z' property in 'label' element of the PLY object.");

    label_cellAxis_evals1Prop = labelElement->createValue("evals1", PlyFile::FLOAT);
    if(not label_cellAxis_evals1Prop)
      return setErrorMessage("Couldn't create 'evals1' property in 'label' element of the PLY object.");
    label_cellAxis_evals2Prop = labelElement->createValue("evals2", PlyFile::FLOAT);
    if(not label_cellAxis_evals2Prop)
      return setErrorMessage("Couldn't create 'evals2' property in 'label' element of the PLY object.");
    label_cellAxis_evals3Prop = labelElement->createValue("evals3", PlyFile::FLOAT);
    if(not label_cellAxis_evals3Prop)
      return setErrorMessage("Couldn't create 'evals3' property in 'label' element of the PLY object.");
  }

  ply.allocate();

  Information::out << "Memory allocated ... setting up global properties" << endl;

  // Set global properties
  {
    auto* type = typeProp->list<char>();
    if(not type)
      return setErrorMessage("Couldn't extract value for 'type' property in 'global' element of the PLY object.");
    std::string ts = "cell_complex";
    (*type)[0].resize(ts.size());
    std::copy(begin(ts), end(ts), begin((*type)[0]));

    auto* cell_mesh = cell_meshProp->value<uint8_t>();
    if(not cell_mesh)
      return setErrorMessage("Couldn't extract value for 'cell_mesh' property in 'global' element of the PLY object.");
    (*cell_mesh)[0] = mesh->cells();

    auto* oriented = orientedProp->value<uint8_t>();
    if(not oriented)
      return setErrorMessage("Couldn't extract value for 'oriented' property in 'global' element of the PLY object.");
    (*oriented)[0] = 1;

    auto* multiple_vertex = multiple_vertexProp->value<uint8_t>();
    if(not multiple_vertex)
      return setErrorMessage("Couldn't extract value for 'multiple_vertex' property in 'global' element of the PLY object.");
    (*multiple_vertex)[0] = 0;

    auto* extra_elements = extra_elementsProp->value<uint8_t>();
    if(not extra_elements)
      return setErrorMessage("Couldn't extract value for 'extra_elements' property in 'global' element of the PLY object.");
    (*extra_elements)[0] = 0;

    auto* length_unit = length_unitProp->value<float>();
    if(not length_unit)
      return setErrorMessage("Couldn't extract value for 'length_unit' property in 'global' element of the PLY object.");
    (*length_unit)[0] = 1e-6;

    auto* signal_unit = signal_unitProp->list<char>();
    if(not signal_unit)
      return setErrorMessage("Couldn't extract value for 'signal_unit' property in 'global' element of the PLY object.");
    QByteArray su_str = mesh->signalUnit().toUtf8();
    (*signal_unit)[0].resize(su_str.size());
    std::copy(begin(su_str), end(su_str), begin((*signal_unit)[0]));

    auto* signal_description = signal_descriptionProp->list<char>();
    if(not signal_description)
      return setErrorMessage("Couldn't extract value for 'signal_description' property in 'global' element of the PLY object.");
    QByteArray sd_str = mesh->signalDesc().toCSV().toUtf8();
    (*signal_description)[0].resize(sd_str.size());
    std::copy(begin(sd_str), end(sd_str), begin((*signal_description)[0]));

    auto* signal_low = signal_lowProp->value<float>();
    if(not signal_low)
      return setErrorMessage("Couldn't extract value for 'signal_low' property in 'global' element of the PLY object.");
    (*signal_low)[0] = mesh->signalBounds()[0];

    auto* signal_high = signal_highProp->value<float>();
    if(not signal_high)
      return setErrorMessage("Couldn't extract value for 'signal_high' property in 'global' element of the PLY object.");
    (*signal_high)[0] = mesh->signalBounds()[1];

    auto* heat_unit = heat_unitProp->list<char>();
    if(not heat_unit)
      return setErrorMessage("Couldn't extract value for 'heat_unit' property in 'global' element of the PLY object.");
    QByteArray hu_str = mesh->heatMapUnit().toUtf8();
    (*heat_unit)[0].resize(hu_str.size());
    std::copy(begin(hu_str), end(hu_str), begin((*heat_unit)[0]));

    auto* heat_description = heat_descriptionProp->list<char>();
    if(not heat_description)
      return setErrorMessage("Couldn't extract value for 'heat_description' property in 'global' element of the PLY object.");
    QByteArray hd_str = mesh->heatMapDesc().toCSV().toUtf8();
    (*heat_description)[0].resize(hd_str.size());
    std::copy(begin(hd_str), end(hd_str), begin((*heat_description)[0]));

    auto* heat_low = heat_lowProp->value<float>();
    if(not heat_low)
      return setErrorMessage("Couldn't extract value for 'heat_low' property in 'global' element of the PLY object.");
    (*heat_low)[0] = mesh->heatMapBounds()[0];

    auto* heat_high = heat_highProp->value<float>();
    if(not heat_high)
      return setErrorMessage("Couldn't extract value for 'heat_high' property in 'global' element of the PLY object.");
    (*heat_high)[0] = mesh->heatMapBounds()[1];

    auto* cellAxis_description = cellAxis_descriptionProp->list<char>();
    if(not cellAxis_description)
      return setErrorMessage("Couldn't create value for 'cellAxis_description' property in 'global' element of the PLY object.");
    QByteArray cad_str = mesh->cellAxisDesc().toCSV().toUtf8();
    (*cellAxis_description)[0].resize(cad_str.size());
    std::copy(begin(cad_str), end(cad_str), begin((*cellAxis_description)[0]));

    auto* cellAxis_unit = cellAxis_unitProp->list<char>();
    if(not cellAxis_unit)
      return setErrorMessage("Couldn't create 'cellAxis_unit' property in 'global' element of the PLY object.");
    QByteArray cau_str = mesh->cellAxisUnit().toUtf8();
    (*cellAxis_unit)[0].resize(cau_str.size());
    std::copy(begin(cau_str), end(cau_str), begin((*cellAxis_unit)[0]));

    auto* cellAxis_parent = cellAxis_parentProp->value<uint8_t>();
    if(not cellAxis_parent)
      return setErrorMessage("Couldn't create 'cellAxis_parent' property in 'global' element of the PLY object.");
    (*cellAxis_parent)[0] = mesh->isParentAxis() ? 1 : 0;
  }

  Information::out << "... extracting common arrays" << endl;

  // Extract common arrays

  auto* vertex_x = vertex_xProp->value<float>();
  if(not vertex_x)
    return setErrorMessage("Couldn't extract value for 'x' property in 'vertex' element of the PLY object.");

  auto* vertex_y = vertex_yProp->value<float>();
  if(not vertex_y)
    return setErrorMessage("Couldn't extract value for 'y' property in 'vertex' element of the PLY object.");

  auto* vertex_z = vertex_zProp->value<float>();
  if(not vertex_z)
    return setErrorMessage("Couldn't extract value for 'z' property in 'vertex' element of the PLY object.");

  auto* vertex_nx = vertex_nxProp->value<float>();
  if(not vertex_nx)
    return setErrorMessage("Couldn't extract value for 'nx' property in 'vertex' element of the PLY object.");

  auto* vertex_ny = vertex_nyProp->value<float>();
  if(not vertex_ny)
    return setErrorMessage("Couldn't extract value for 'ny' property in 'vertex' element of the PLY object.");

  auto* vertex_nz = vertex_nzProp->value<float>();
  if(not vertex_nz)
    return setErrorMessage("Couldn't extract value for 'nz' property in 'vertex' element of the PLY object.");

  auto* vertex_signal = vertex_signalProp->value<float>();
  if(not vertex_signal)
    return setErrorMessage("Couldn't extract value for 'signal' property in 'vertex' element of the PLY object.");

  auto* vertex_sel = vertex_selProp->value<uint8_t>();
  if(not vertex_sel)
    return setErrorMessage("Couldn't extract value for 'selected' property in 'vertex' element of the PLY object.");

  auto* face_vertex_index = face_vertex_indexProp->list<uint32_t>();
  if(not face_vertex_index)
    return setErrorMessage("Couldn't extract value for 'vertex_index' property in 'face' element of the PLY object.");

  auto* face_label = face_labelProp->value<int32_t>();
  if(not face_label)
    return setErrorMessage("Couldn't extract value for 'label' property in 'face' element of the PLY object.");

  size_t interval = (nb_vertices + nb_faces + 1) / 100;
  size_t k = 0; // progress counter

  // Now, save the values
  if(mesh->cells()) {
    Information::out << "... extracting cell mesh specific arrays" << endl;

    // Get attributes unique to cell meshes
    auto* face_signal = face_signalProp->value<float>();
    if(not face_signal)
      return setErrorMessage("Couldn't extract value for 'signal' property in 'face' element of the PLY object.");

    auto* face_area = face_areaProp->value<float>();
    if(not face_area)
      return setErrorMessage("Couldn't extract value for 'area' property in 'face' element of the PLY object.");

    auto* face_x = face_xProp->value<float>();
    if(not face_x)
      return setErrorMessage("Couldn't extract value for 'x' property in 'face' element of the PLY object.");

    auto* face_y = face_yProp->value<float>();
    if(not face_y)
      return setErrorMessage("Couldn't extract value for 'y' property in 'face' element of the PLY object.");

    auto* face_z = face_zProp->value<float>();
    if(not face_z)
      return setErrorMessage("Couldn't extract value for 'z' property in 'face' element of the PLY object.");

    auto* face_nx = face_nxProp->value<float>();
    if(not face_nx)
      return setErrorMessage("Couldn't extract value for 'nx' property in 'face' element of the PLY object.");

    auto* face_ny = face_nyProp->value<float>();
    if(not face_ny)
      return setErrorMessage("Couldn't extract value for 'ny' property in 'face' element of the PLY object.");

    auto* face_nz = face_nzProp->value<float>();
    if(not face_nz)
      return setErrorMessage("Couldn't extract value for 'nz' property in 'face' element of the PLY object.");

    Information::out << "... storing data" << endl;

    for(const vertex& v: S) {
      if((++k % interval) == 0 and not progress.advance(k))
        userCancel();
      Point3f pos = savedPos(v->pos, transform, stack);
      Point3f normal = savedNormal(v->nrml, transform, stack);
      if(v->type == 'c') {
        // This is a face
        auto face_id = v->saveId;
        auto& face = (*face_vertex_index)[face_id];
        face.clear();
        face.reserve(S.valence(v));
        for(const vertex& n: S.neighbors(v))
          face.push_back(n->saveId);
        (*face_x)[face_id] = pos.x();
        (*face_y)[face_id] = pos.y();
        (*face_z)[face_id] = pos.z();
        (*face_nx)[face_id] = normal.x();
        (*face_ny)[face_id] = normal.y();
        (*face_nz)[face_id] = normal.z();
        (*face_label)[face_id] = v->label;
        (*face_area)[face_id] = v->area;
        (*face_signal)[face_id] = v->signal;
      } else {
        auto vid = v->saveId;
        (*vertex_x)[vid] = pos.x();
        (*vertex_y)[vid] = pos.y();
        (*vertex_z)[vid] = pos.z();
        (*vertex_nx)[vid] = normal.x();
        (*vertex_ny)[vid] = normal.y();
        (*vertex_nz)[vid] = normal.z();
        (*vertex_signal)[vid] = v->signal;
        (*vertex_sel)[vid] = v->selected;
      }
    }
  } else {
    Information::out << "... extracting triangular mesh specific arrays" << endl;

    // Get attributes unique to triangle meshes
    auto* vertex_label = vertex_labelProp->value<int32_t>();
    if(not vertex_label)
      return setErrorMessage("Couldn't extract value for 'label' property in 'vertex' element of the PLY object.");

    Information::out << "... storing vertices" << endl;

    // First store vertices
    for(const vertex& v : S) {
      if((++k % interval) == 0 and not progress.advance(k))
        userCancel();
      Point3f pos = savedPos(v->pos, transform, stack);
      Point3f normal = savedNormal(v->nrml, transform, stack);

      auto vid = v->saveId;

      (*vertex_label)[vid] = v->label;
      (*vertex_x)[vid] = pos.x();
      (*vertex_y)[vid] = pos.y();
      (*vertex_z)[vid] = pos.z();
      (*vertex_nx)[vid] = normal.x();
      (*vertex_ny)[vid] = normal.y();
      (*vertex_nz)[vid] = normal.z();
      (*vertex_signal)[vid] = v->signal;
      (*vertex_sel)[vid] = v->selected;
    }

    Information::out << "... storing faces" << endl;

    size_t face_id = 0;

    // Now write cells or triangles (faces)
    for(const vertex& v : S) {
      // Write fan of triangles
      for(const vertex& n : S.neighbors(v)) {
        const vertex& m = S.nextTo(v, n);
        if(!mesh->uniqueTri(v, n, m))
          continue;
        if((++k % interval) == 0 and not progress.advance(k))
          userCancel();
        auto& vertex_index = (*face_vertex_index)[face_id];
        vertex_index.resize(3);
        vertex_index[0] = v->saveId;
        vertex_index[1] = n->saveId;
        vertex_index[2] = m->saveId;
        (*face_label)[face_id] = mesh->getLabel(v, n, m);
        ++face_id;
      }
    }
  }

  // Now, add properties
  {
    auto* label = (label_labelProp ? label_labelProp->value<int>() : nullptr);
    if(label_labelProp and not label)
      return setErrorMessage("Couldn't extract value for 'label' property in 'label' element of the PLY object.");

    auto* label_heat = (label_heatProp ? label_heatProp->value<float>() : nullptr);
    if(label_heatProp and not label_heat)
      return setErrorMessage("Couldn't extract value for 'heat' property in 'label' element of the PLY object.");

    auto* label_x = (label_xProp ? label_xProp->value<float>() : nullptr);
    if(label_xProp and not label_x)
      return setErrorMessage("Couldn't extract value for 'x' property in 'label' element of the PLY object.");
    auto* label_y = (label_yProp ? label_yProp->value<float>() : nullptr);
    if(label_yProp and not label_y)
      return setErrorMessage("Couldn't extract value for 'y' property in 'label' element of the PLY object.");
    auto* label_z = (label_zProp ? label_zProp->value<float>() : nullptr);
    if(label_zProp and not label_z)
      return setErrorMessage("Couldn't extract value for 'z' property in 'label' element of the PLY object.");

    auto* label_xVis = (label_xVisProp ? label_xVisProp->value<float>() : nullptr);
    if(label_xVisProp and not label_xVis)
      return setErrorMessage("Couldn't extract value for 'xVis' property in 'label' element of the PLY object.");
    auto* label_yVis = (label_yVisProp ? label_yVisProp->value<float>() : nullptr);
    if(label_yVisProp and not label_yVis)
      return setErrorMessage("Couldn't extract value for 'yVis' property in 'label' element of the PLY object.");
    auto* label_zVis = (label_zVisProp ? label_zVisProp->value<float>() : nullptr);
    if(label_zVisProp and not label_zVis)
      return setErrorMessage("Couldn't extract value for 'zVis' property in 'label' element of the PLY object.");

    auto* label_nx = (label_nxProp ? label_nxProp->value<float>() : nullptr);
    if(label_nxProp and not label_nx)
      return setErrorMessage("Couldn't extract value for 'nx' property in 'label' element of the PLY object.");
    auto* label_ny = (label_nyProp ? label_nyProp->value<float>() : nullptr);
    if(label_nyProp and not label_ny)
      return setErrorMessage("Couldn't extract value for 'ny' property in 'label' element of the PLY object.");
    auto* label_nz = (label_nzProp ? label_nzProp->value<float>() : nullptr);
    if(label_nzProp and not label_nz)
      return setErrorMessage("Couldn't extract value for 'nz' property in 'label' element of the PLY object.");

    auto* label_nxVis = (label_nxVisProp ? label_nxVisProp->value<float>() : nullptr);
    if(label_nxVisProp and not label_nxVis)
      return setErrorMessage("Couldn't extract value for 'nxVis' property in 'label' element of the PLY object.");
    auto* label_nyVis = (label_nyVisProp ? label_nyVisProp->value<float>() : nullptr);
    if(label_nyVisProp and not label_nyVis)
      return setErrorMessage("Couldn't extract value for 'nyVis' property in 'label' element of the PLY object.");
    auto* label_nzVis = (label_nzVisProp ? label_nzVisProp->value<float>() : nullptr);
    if(label_nzVisProp and not label_nzVis)
      return setErrorMessage("Couldn't extract value for 'nzVis' property in 'label' element of the PLY object.");

    auto* label_parent = (label_parentProp ? label_parentProp->value<int>() : nullptr);
    if(label_parentProp and not label_parent)
      return setErrorMessage("Couldn't extract value for 'parent' property in 'label' element of the PLY object.");

    auto* label_cellAxis_ev1X = (label_cellAxis_ev1XProp ? label_cellAxis_ev1XProp->value<float>() : nullptr);
    if(label_cellAxis_ev1XProp and not label_cellAxis_ev1X)
      return setErrorMessage("Couldn't extract value for 'ev1X' property in 'label' element of the PLY object.");
    auto* label_cellAxis_ev1Y = (label_cellAxis_ev1YProp ? label_cellAxis_ev1YProp->value<float>() : nullptr);
    if(label_cellAxis_ev1YProp and not label_cellAxis_ev1Y)
      return setErrorMessage("Couldn't extract value for 'ev1Y' property in 'label' element of the PLY object.");
    auto* label_cellAxis_ev1Z = (label_cellAxis_ev1ZProp ? label_cellAxis_ev1ZProp->value<float>() : nullptr);
    if(label_cellAxis_ev1ZProp and not label_cellAxis_ev1Z)
      return setErrorMessage("Couldn't extract value for 'ev1Z' property in 'label' element of the PLY object.");

    auto* label_cellAxis_ev2X = (label_cellAxis_ev2XProp ? label_cellAxis_ev2XProp->value<float>() : nullptr);
    if(label_cellAxis_ev2XProp and not label_cellAxis_ev2X)
      return setErrorMessage("Couldn't extract value for 'ev2X' property in 'label' element of the PLY object.");
    auto* label_cellAxis_ev2Y = (label_cellAxis_ev2YProp ? label_cellAxis_ev2YProp->value<float>() : nullptr);
    if(label_cellAxis_ev2YProp and not label_cellAxis_ev2Y)
      return setErrorMessage("Couldn't extract value for 'ev2Y' property in 'label' element of the PLY object.");
    auto* label_cellAxis_ev2Z = (label_cellAxis_ev2ZProp ? label_cellAxis_ev2ZProp->value<float>() : nullptr);
    if(label_cellAxis_ev2ZProp and not label_cellAxis_ev2Z)
      return setErrorMessage("Couldn't extract value for 'ev2Z' property in 'label' element of the PLY object.");

    auto* label_cellAxis_evals1 = (label_cellAxis_evals1Prop ? label_cellAxis_evals1Prop->value<float>() : nullptr);
    if(label_cellAxis_evals1Prop and not label_cellAxis_evals1)
      return setErrorMessage("Couldn't extract value for 'evals1' property in 'label' element of the PLY object.");
    auto* label_cellAxis_evals2 = (label_cellAxis_evals2Prop ? label_cellAxis_evals2Prop->value<float>() : nullptr);
    if(label_cellAxis_evals2Prop and not label_cellAxis_evals2)
      return setErrorMessage("Couldn't extract value for 'evals2' property in 'label' element of the PLY object.");
    auto* label_cellAxis_evals3 = (label_cellAxis_evals3Prop ? label_cellAxis_evals3Prop->value<float>() : nullptr);
    if(label_cellAxis_evals3Prop and not label_cellAxis_evals3)
      return setErrorMessage("Couldn't extract value for 'evals3' property in 'label' element of the PLY object.");

    const auto& NaN = std::numeric_limits<float>::quiet_NaN();

    auto empty_tensor = SymmetricTensor(Point3f(), Point3f(), Point3f());

    std::copy(begin(labels), end(labels), begin(*label));
    std::sort(begin(*label), end(*label));
    for(size_t i = 0 ; i < nb_labels ; ++i) {
      if((++k % interval) == 0 and not progress.advance(k))
        userCancel();

      int lab = (*label)[i];
      if(label_heat) (*label_heat)[i] = get_default(mesh->labelHeat(), lab, NaN);
      if(label_x and label_y and label_z) {
        Point3f pos = savedPos(get_default(mesh->labelCenter(), lab, Point3f()), transform, stack);
        (*label_x)[i] = pos.x();
        (*label_y)[i] = pos.y();
        (*label_z)[i] = pos.z();
      }
      if(label_nx and label_ny and label_nz) {
        Point3f pos = savedNormal(get_default(mesh->labelNormal(), lab, Point3f()), transform, stack);
        (*label_nx)[i] = pos.x();
        (*label_ny)[i] = pos.y();
        (*label_nz)[i] = pos.z();
      }
      if(label_xVis and label_yVis and label_zVis) {
        Point3f pos = savedPos(get_default(mesh->labelCenterVis(), lab, Point3f()), transform, stack);
        (*label_xVis)[i] = pos.x();
        (*label_yVis)[i] = pos.y();
        (*label_zVis)[i] = pos.z();
      }
      if(label_nxVis and label_nyVis and label_nzVis) {
        Point3f pos = savedNormal(get_default(mesh->labelNormalVis(), lab, Point3f()), transform, stack);
        (*label_nxVis)[i] = pos.x();
        (*label_nyVis)[i] = pos.y();
        (*label_nzVis)[i] = pos.z();
      }
      if(label_parent) (*label_parent)[i] = get_default(mesh->parentLabelMap(), lab, 0);

      if(label_cellAxis_ev1X and label_cellAxis_ev1Y and label_cellAxis_ev1Z and
         label_cellAxis_ev2X and label_cellAxis_ev2Y and label_cellAxis_ev2Z and
         label_cellAxis_evals1 and label_cellAxis_evals2 and label_cellAxis_evals3) {
        auto sym = get_default(mesh->cellAxis(), lab, empty_tensor);
        if(normsq(sym.ev1()) > 0) {
          sym.ev1() = savedNormal(sym.ev1(), transform, stack);
          sym.ev2() = savedNormal(sym.ev2(), transform, stack);
        }
        (*label_cellAxis_ev1X)[i] = sym.ev1().x();
        (*label_cellAxis_ev1Y)[i] = sym.ev1().y();
        (*label_cellAxis_ev1Z)[i] = sym.ev1().z();
        (*label_cellAxis_ev2X)[i] = sym.ev2().x();
        (*label_cellAxis_ev2Y)[i] = sym.ev2().y();
        (*label_cellAxis_ev2Z)[i] = sym.ev2().z();
        (*label_cellAxis_evals1)[i] = sym.evals()[0];
        (*label_cellAxis_evals2)[i] = sym.evals()[1];
        (*label_cellAxis_evals3)[i] = sym.evals()[3];
      }
    }
  }

  if(not progress.advance(nb_vertices + nb_faces + nb_labels))
    userCancel();
  if(not ply.save(filename))
    return setErrorMessage(QString("Error saving PLY file, check terminal for error message."));

  mesh->setFile(stripCurrentDir(filename));
  SETSTATUS("Saved mesh to file:" << mesh->file() << ", vertices:" << nb_vertices << ", faces:" << nb_faces);
  return true;
}

// REGISTER_MESH_PROCESS(MeshExport);

bool MeshSave::initialize(ParmList& parms, QWidget* parent)
{
  int meshId = parms[2].toInt();
  if(!checkState().mesh(MESH_ANY, meshId))
    return false;

  QDialog dlg(parent);
  Ui_SaveMeshDialog ui;
  ui.setupUi(&dlg);

  this->ui = &ui;
  this->dlg = &dlg;

  connect(ui.SelectMeshFile, &QAbstractButton::clicked, this, &MeshSave::selectMeshFile);

  Mesh* m = mesh(meshId);
  QString filename = m->file();
  if(filename.isEmpty())
    filename = parms[0].toString();

  setMeshFile(filename);
  ui.Transform->setChecked(parmToBool(parms[1]));

  bool res = false;
  if(dlg.exec() == QDialog::Accepted) {
    parms[0] = properFile(ui.MeshFile->text());
    parms[1] = ui.Transform->isChecked();
    res = true;
  }
  this->ui = 0;
  this->dlg = 0;
  return res;
}

QString MeshSave::properFile(QString filename) const
{
  QFileInfo fi(filename);
  QString suf = fi.suffix();
  if(!suf.isEmpty())
    filename = filename.left(filename.size() - suf.size() - 1);
  return filename + ".ply";
}

void MeshSave::selectMeshFile()
{
  QString filename = ui->MeshFile->text();
  filename = QFileDialog::getSaveFileName(dlg, QString("Save mesh as ..."), filename, "PLY Binary File (*.ply)", 0,
                                          FileDialogOptions);
  if(!filename.isEmpty())
    setMeshFile(filename);
}

void MeshSave::setMeshFile(const QString& filename)
{
  if(!filename.endsWith(".ply", Qt::CaseInsensitive))
    ui->MeshFile->setText(filename + ".ply");
  else
    ui->MeshFile->setText(filename);
}

bool MeshSave::operator()(const ParmList& parms)
{
  int meshId = parms[2].toInt();
  if(!checkState().mesh(STACK_ANY, meshId))
    return false;
  Mesh* mesh = this->mesh(meshId);
  bool transform = parmToBool(parms[1]);
  bool res = (*this)(mesh, parms[0].toString(), transform);
  return res;
}

Point3f MeshSave::savedPos(Point3f pos, bool transform, const Stack* stack)
{
  if(transform)
    pos = Point3f(stack->getFrame().inverseCoordinatesOf(Vec(pos)));
  return pos;
}

bool MeshSave::operator()(Mesh* mesh, const QString& filename, bool transform)
{
  return MeshExport::savePLY(mesh, filename, transform, true);
}

// REGISTER_MESH_PROCESS(MeshSave);

bool SaveProjectFile::operator()(const ParmList& parms) {
  return (*this)(parms[0].toString());
}

bool SaveProjectFile::operator()(const QString& filename)
{
  if(filename.isEmpty()) {
    setErrorMessage("Filename is empty. Cannot save view file.");
    return false;
  }
  actingFile(filename, true);
  bool res = systemCommand(SAVE_PROJECT, ParmList() << filename);
  SETSTATUS("saving project to file " << filename);
  if(!res) {
    setErrorMessage("Error while saving project file.");
    return false;
  }
  return true;
}

bool SaveProjectFile::initialize(ParmList& parms, QWidget* parent)
{
  QString filename = parms[0].toString();
  bool choose_file = parmToBool(parms[1]);

  if(filename.isEmpty() or choose_file) {
    if(filename.isEmpty())
      filename = file();

    // Get the file name
    filename = QFileDialog::getSaveFileName(parent, QString("Select project file"), filename,
                                            "LithoGraphX Project files (*.lgxp);;All files (*.*)", 0, FileDialogOptions);

    if(filename.isEmpty())
      return false;
  }
  if(not filename.endsWith(".lgxp", Qt::CaseInsensitive))
    filename += ".lgxp";

  parms[0] = filename;
  return true;
}

// REGISTER_GLOBAL_PROCESS(SaveProjectFile);

class MeshListModel : public QAbstractTableModel {
public:
  MeshListModel(Process* p, QObject* parent = 0)
    : QAbstractTableModel(parent)
    , proc(p)
  {
    for(int i = 0; i < proc->meshCount(); ++i) {
      names << QString("Mesh%1").arg(i + 1);
      Mesh* m = proc->mesh(i);
      QString shortFile = stripCurrentDir(m->file());
      selected << not (shortFile.isEmpty());
      files << shortFile;
      transforms << false;
    }
  }

  int rowCount(const QModelIndex& parent = QModelIndex()) const
  {
    if(parent.isValid())
      return 0;
    return names.size();
  }

  int columnCount(const QModelIndex& /*parent*/ = QModelIndex()) const {
    return 4;
  }

  QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const
  {
    if(role != Qt::DisplayRole)
      return QVariant();
    if(orientation == Qt::Vertical)
      return QVariant();
    switch(section) {
    case 0:
      return "Mesh";
    case 1:
      return "Filename";
    case 2:
      return "Transformed";
    default:
      return QVariant();
    }
  }

  Qt::ItemFlags flags(const QModelIndex& index) const
  {
    if(!index.isValid())
      return Qt::ItemIsEnabled;
    int i = index.row();
    if(i < 0 or i >= names.size())
      return Qt::ItemIsEnabled;
    switch(index.column()) {
    case 0:
      return Qt::ItemIsEnabled | Qt::ItemIsUserCheckable | Qt::ItemIsSelectable;
    case 1:
      if(selected[i])
        return Qt::ItemIsEnabled | Qt::ItemIsEditable | Qt::ItemIsSelectable;
      else
        return 0;
    case 2:
      if(selected[i])
        return Qt::ItemIsEnabled | Qt::ItemIsUserCheckable | Qt::ItemIsSelectable;
      else
        return 0;
    default:
      return Qt::ItemIsEnabled;
    }
  }

  QString properFile(QString filename)
  {
    if(!filename.endsWith(".ply", Qt::CaseInsensitive))
      return filename + ".ply";
    return filename;
  }

  bool setData(const QModelIndex& index, const QVariant& value, int role = Qt::EditRole)
  {
    if(!index.isValid() or index.column() > 3)
      return false;

    int i = index.row();

    if(i < 0 or i >= names.size())
      return false;

    switch(index.column()) {
    case 0:
      if(role != Qt::CheckStateRole)
        return false;
      selected[i] = value.toBool();
      emit dataChanged(this->index(i, 0), this->index(i, 4));
      break;
    case 1:
      if(role != Qt::EditRole)
        return false;
      files[i] = properFile(value.toString());
      emit dataChanged(index, index);
      break;
    case 2:
      if(role != Qt::CheckStateRole)
        return false;
      transforms[i] = value.toBool();
      emit dataChanged(index, index);
      break;
    default:
      return false;
    }
    return true;
  }

  QVariant data(const QModelIndex& index, int role) const
  {
    if(!index.isValid() or index.row() >= names.size() or index.column() > 3)
      return QVariant();
    int i = index.row();
    switch(role) {
    case Qt::CheckStateRole:
      switch(index.column()) {
      case 0:
        return selected[i] ? Qt::Checked : Qt::Unchecked;
      case 2:
        return transforms[i] ? Qt::Checked : Qt::Unchecked;
      case 1:
      default:
        return QVariant();
      }
    case Qt::EditRole:
      if(index.column() == 1)
        return files[i];
      else
        return QVariant();
    case Qt::DisplayRole:
      if(index.column() == 0)
        return names[i];
      else if(index.column() == 1)
        return files[i];
      else
        return QVariant();
    default:
      return QVariant();
    }
  }

  void changeMesh(const QString& name, const QString& file, bool trans)
  {
    int idx = names.indexOf(name);
    if(idx >= 0) {
      files[idx] = file;
      selected[idx] = true;
      transforms[idx] = trans;
      emit dataChanged(index(idx, 0), index(idx, 4));
    }
  }

  QStringList typeList;
  QStringList names;
  QStringList files;
  QList<bool> selected, transforms;

protected:
  Process* proc;
};

class StackListModel : public QAbstractTableModel {
public:
  StackListModel(Process* p, QObject* parent = 0)
    : QAbstractTableModel(parent)
    , proc(p)
  {
    for(int i = 0; i < proc->stackCount(); ++i) {
      Stack* s = proc->stack(i);
      if(!s)
        continue;
      Store* main = s->main();
      if(main) {
        names << QString("MainStack%1").arg(i + 1);
        QString shortFile = stripCurrentDir(main->file());
        files << properFile(shortFile);
        selected << not shortFile.isEmpty();
      }
      Store* work = s->work();
      if(work) {
        names << QString("WorkStack%1").arg(i + 1);
        QString shortFile = stripCurrentDir(work->file());
        files << properFile(shortFile);
        selected << not shortFile.isEmpty();
      }
    }
  }

  QString properFile(const QString& filename)
  {
    QString result = filename;
    auto channel_idx = result.indexOf('?');
    if(channel_idx >= 0) {
      result = result.left(channel_idx);
    }
    QFileInfo fi(result);
    QString suf = fi.suffix();
    if(suf != "inr" and suf != "tif" and suf != "mgxs" and suf != ".tiff")
      result += ".tif";
    return result;
  }

  int rowCount(const QModelIndex& parent = QModelIndex()) const
  {
    if(parent.isValid())
      return 0;
    return names.size();
  }

  int columnCount(const QModelIndex& /*parent*/ = QModelIndex()) const {
    return 2;
  }

  QVariant data(const QModelIndex& index, int role) const
  {
    if(!index.isValid() or index.row() >= names.size() or index.column() > 2)
      return QVariant();
    int i = index.row();
    if(role == Qt::CheckStateRole and index.column() == 0)
      return selected[i] ? Qt::Checked : Qt::Unchecked;
    if(role == Qt::EditRole and index.column() == 1)
      return files[i];
    if(role == Qt::DisplayRole) {
      if(index.column() == 0)
        return names[i];
      if(index.column() == 1)
        return files[i];
    }
    return QVariant();
  }

  QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const
  {
    if(role != Qt::DisplayRole)
      return QVariant();
    if(orientation == Qt::Vertical)
      return QVariant();
    switch(section) {
    case 0:
      return "Stack";
    case 1:
      return "Filename";
    default:
      return QVariant();
    }
  }

  Qt::ItemFlags flags(const QModelIndex& index) const
  {
    if(!index.isValid())
      return Qt::ItemIsEnabled;
    int i = index.row();
    if(i < 0 or i >= names.size())
      return Qt::ItemIsEnabled;
    switch(index.column()) {
    case 0:
      return Qt::ItemIsEnabled | Qt::ItemIsUserCheckable | Qt::ItemIsSelectable;
    case 1:
      if(selected[i])
        return Qt::ItemIsEnabled | Qt::ItemIsEditable | Qt::ItemIsSelectable;
      else
        return 0;
    default:
      return Qt::ItemIsEnabled;
    }
  }

  bool setData(const QModelIndex& index, const QVariant& value, int role = Qt::EditRole)
  {
    if(!index.isValid() or index.column() > 1)
      return false;

    int i = index.row();

    if(i < 0 or i >= names.size())
      return false;

    if(index.column() == 0 and role == Qt::CheckStateRole) {
      selected[i] = value.toBool();
      emit dataChanged(this->index(i, 0), this->index(i, 1));
    } else if(index.column() == 1 and role == Qt::EditRole) {
      files[i] = properFile(value.toString());
      emit dataChanged(index, index);
    } else
      return false;

    return true;
  }

  void changeStack(const QString& name, const QString& file)
  {
    int idx = names.indexOf(name);
    if(idx >= 0) {
      files[idx] = file;
      selected[idx] = true;
      emit dataChanged(index(idx, 0), index(idx, 1));
    }
  }

  QStringList names;
  QStringList files;
  QList<bool> selected;

protected:
  Process* proc;
};

void SaveAll::on_selectProjectFile_clicked()
{
  QString filename = ui->projectFileName->text();
  filename = QFileDialog::getSaveFileName(dlg, QString("Select project file"), filename,
                                          "LithoGraphX Project files (*.lgxp);;All files (*.*)", 0, FileDialogOptions);
  if(!filename.isEmpty()) {
    if(!filename.endsWith(".lgxp"))
      filename += ".lgxp";
    ui->projectFileName->setText(filename);
  }
}

void SaveAll::on_projectFileName_editingFinished()
{
  validateProjectFile();
}

void SaveAll::validateProjectFile()
{
  QString filename = ui->projectFileName->text();
  if(!filename.isEmpty() and !filename.endsWith(".lgxp")) {
    filename += ".lgxp";
    ui->projectFileName->setText(filename);
  }
}

bool SaveAll::initialize(ParmList& parms, QWidget* parent)
{
  QDialog dlg(parent);
  Ui_SaveAllDlg ui;
  ui.setupUi(&dlg);
  this->dlg = &dlg;
  this->ui = &ui;

  // First, set the system
  ui.projectFileName->setText(file());
  ui.saveProjectFile->setChecked(not file().isEmpty());
  ui.compressionLevel->setValue(parms[2].toInt());

  StackListModel smodel(this);
  MeshListModel mmodel(this);
  ui.stackList->setModel(&smodel);
  ui.meshList->setModel(&mmodel);

  ui.stackList->resizeColumnToContents(0);
  ui.stackList->resizeColumnToContents(1);

  ui.meshList->resizeColumnToContents(0);
  ui.meshList->resizeColumnToContents(1);
  ui.meshList->resizeColumnToContents(2);
  ui.meshList->resizeColumnToContents(3);

  if(dlg.exec() == QDialog::Accepted) {
    validateProjectFile();
    parms[0] = "Defined";
    QStringList definition;
    if(ui.saveProjectFile->isChecked()) {
      QString fn = ui.projectFileName->text();
      definition << QString("View=%1").arg(fn);
    }
    if(ui.saveStacks->isChecked()) {
      for(int i = 0; i < smodel.names.size(); ++i) {
        if(smodel.selected[i] and not smodel.files[i].isEmpty())
          definition << QString("%1=%2").arg(smodel.names[i]).arg(smodel.files[i]);
      }
    }
    if(ui.saveMeshes->isChecked()) {
      for(int i = 0; i < mmodel.names.size(); ++i) {
        if(mmodel.selected[i] and not mmodel.files[i].isEmpty()) {
          QString type;
          QString name = (QString("%1Mesh%2").arg(mmodel.transforms[i] ? "Transformed" : "").arg(i + 1));
          definition << QString("%1=%2").arg(name).arg(mmodel.files[i]);
        }
      }
    }
    parms[1] = definition.join(",");
    parms[2] = ui.compressionLevel->value();
    return true;
  }

  return false;
}

bool SaveAll::operator()(const ParmList& parms)
{
  QRegExp type_expr("^(?:(?:(Work|Main)Stack|(?:(Transformed)?Mesh))(\\d+)|View)$");

  QList<SaveStruct> tosave;
  QString which = parms[0].toString().toLower();
  if(which == "all") {
    SaveStruct s;
    s.type = ALL;
    s.filename = parms[1].toString();
    tosave << s;
  } else if(which == "defined") {
    QStringList fields = parms[1].toString().split(",");
    Information::out << "tosave = '" << fields.join(" - ") << "'" << endl;
    for(int i = 0; i < fields.size(); ++i) {
      const QString& f = fields[i];
      int sep = f.indexOf('=');
      if(sep == -1)
        return setErrorMessage(QString("Invalid save definition: missing '=' sign in field %1").arg(i + 1));
      QString type = f.left(sep);
      QString value = f.mid(sep + 1);
      SaveStruct s;
      s.filename = value;
      if(type_expr.indexIn(type) != 0) {
        setErrorMessage(QString("The string '%1' in field %2 is not a valid type").arg(type).arg(i + 1));
        return false;
      }
      if(!type_expr.cap(1).isEmpty())       // Stack
      {
        if(type_expr.cap(1) == "Work")
          s.type = WORK_STACK;
        else
          s.type = MAIN_STACK;
        s.id = type_expr.cap(3).toInt() - 1;
      } else if(!type_expr.cap(3).isEmpty())       // Mesh
      {
        s.type = MESH;
        s.transform = !type_expr.cap(2).isEmpty();
        s.id = type_expr.cap(3).toInt() - 1;
      } else
        s.type = VIEW;
      tosave << s;
    }
  } else
    return setErrorMessage(QString("Error, unknown type '%1'").arg(which));
  bool ok;
  int cl = parms[2].toInt(&ok);
  if(not ok)
    return setErrorMessage("Error, parameter 'Compression Level' must be an integer number");
  return (*this)(tosave, cl);
}

bool SaveAll::operator()(const QList<SaveStruct>& _tosave, int compressionLevel)
{
  // Check if the VIEW is saved. If so set this as the current project file
  forall(const SaveStruct& s, _tosave) {
    if(s.type == VIEW) {
      actingFile(s.filename, true);
      break;
    }
  }
  QList<SaveStruct> tosave = _tosave;
  if(tosave.size() == 1 and tosave[0].type == ALL) {
    tosave[0].type = VIEW;
    if(tosave[0].filename.isEmpty())
      tosave[0].filename = file();
    for(int i = 0; i < stackCount(); ++i) {
      SaveStruct sm, sw;
      Stack* st = stack(i);
      sm.type = MAIN_STACK;
      sw.type = WORK_STACK;
      Store* main = st->main();
      Store* work = st->work();
      sm.id = sw.id = i;
      if(main and !main->file().isEmpty()) {
        sm.filename = main->file();
        tosave << sm;
      }
      if(work and !work->file().isEmpty()) {
        sw.filename = work->file();
        tosave << sw;
      }
    }
    for(int i = 0; i < meshCount(); ++i) {
      SaveStruct s;
      s.type = MESH;
      s.id = i;
      s.transform = false;
      tosave << s;
    }
  }
  SaveProjectFile saveProjectFile(*this);
  MeshSave saveMesh(*this);
  StackSave saveStack(*this);
  Progress progress("Saving all files ...", tosave.size());
  QString viewFilename;
  for(int i = 0; i < tosave.size(); ++i) {
    if(!progress.advance(i))
      userCancel();
    const SaveStruct& s = tosave[i];
    switch(s.type) {
    case VIEW:
      viewFilename = s.filename;
      break;
    case MAIN_STACK: {
      Stack* st = stack(s.id);
      if(!st) {
        setErrorMessage(QString("No stack numbered %1").arg(s.id));
        return false;
      }
      if(!saveStack(st, st->main(), s.filename, compressionLevel))
        return false;
    } break;
    case WORK_STACK: {
      Stack* st = stack(s.id);
      if(!st) {
        setErrorMessage(QString("No stack numbered %1").arg(s.id));
        return false;
      }
      if(!saveStack(st, st->work(), s.filename, compressionLevel))
        return false;
    } break;
    case MESH: {
      Mesh* m = mesh(s.id);
      if(!m) {
        setErrorMessage(QString("No mesh numbered %1").arg(s.id));
        return false;
      }
      if(!saveMesh(m, s.filename, s.transform))
        return false;
    } break;
    case ALL: {
      setErrorMessage("The type 'All' can only be used in isolation, with no other file specification");
      return false;
    }
    }
  }
  // It needs to be always the last thing done
  if(!viewFilename.isEmpty()) {
    if(!saveProjectFile(viewFilename))
      return false;
  }
  SETSTATUS("All saved");
  return true;
}

bool ResetAll::operator()()
{
  for(int i = 0; i < stackCount(); ++i) {
    Stack* stk = stack(i);
    if(stk) {
      stk->reset();
      stk->main()->changed();
      stk->work()->changed();
    }
  }

  for(int i = 0; i < meshCount(); ++i) {
    Mesh* m = mesh(i);
    if(m) {
      m->reset();
      m->updateAll();
    }
  }

  resetProject();

  return true;
}

// REGISTER_GLOBAL_PROCESS(SaveAll);
bool TakeSnapshot::operator()(QString filename, float overSampling, int width, int height, int quality,
                              bool expand_frustum)
{
  return takeSnapshot(filename, overSampling, width, height, quality, expand_frustum);
}
} // namespace process
} // namespace lgx
