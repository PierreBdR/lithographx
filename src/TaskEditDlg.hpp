/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef TASKEDITDLG_HPP
#define TASKEDITDLG_HPP

#include <Process.hpp>

#include <QAbstractItemModel>
#include <QList>
#include <QHash>

#include <ui_TaskEditor.h>

class QPushButton;

typedef QList<float> floatList;

Q_DECLARE_METATYPE(floatList)

struct TypedProcessDefinition : public lgx::process::BaseProcessDefinition {
  QString type;
  bool shared;
};

class TasksModel : public QAbstractItemModel {
  Q_OBJECT
public:
  typedef QList<TypedProcessDefinition> task_t;
  typedef QHash<QString, task_t> tasks_t;

  TasksModel(const tasks_t& ts, QObject* parent = 0);

  int rowCount(const QModelIndex& parent = QModelIndex()) const;
  int columnCount(const QModelIndex& parent = QModelIndex()) const;
  QVariant data(const QModelIndex& index, int role) const;
  QMap<int, QVariant> itemData(const QModelIndex& index) const;
  QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;
  Qt::ItemFlags flags(const QModelIndex& index) const;
  bool setData(const QModelIndex& index, const QVariant& value, int role = Qt::EditRole);
  bool setItemData(const QModelIndex& index, const QMap<int, QVariant>& roles);
  QModelIndex index(int row, int column, const QModelIndex& parent = QModelIndex()) const;
  QModelIndex taskIndex(int task_id) const {
    return index(task_id, 0);
  }
  QModelIndex taskIndex(const QString& task_name) const {
    return index(task_names.indexOf(task_name), 0);
  }
  QModelIndex processIndex(int task_id, int row) const {
    return index(row, 0, taskIndex(task_id));
  }

  QModelIndex parent(const QModelIndex& index) const;

  bool insertRows(int position, int rows, const QModelIndex& index = QModelIndex());
  bool removeRows(int position, int rows, const QModelIndex& index = QModelIndex());
  bool dropMimeData(const QMimeData* data, Qt::DropAction action, int row, int column, const QModelIndex& parent);
  QStringList mimeTypes() const;
  Qt::DropActions supportedDropActions() const;
  QMimeData* mimeData(const QModelIndexList& indexes) const;

  bool addTask(const QString& name);
  bool addProcess(const QString& task_name, const TypedProcessDefinition& def);
  bool deleteTask(const QString& name);
  bool renameTask(const QString& old_name, const QString& new_name);
  bool copyTask(const QString& old_name, const QString& new_name);

  void deleteItems(const QModelIndexList& lst);
  void deleteItems(QList<QList<int> > lst);

  QStringList task_names;
  QList<int> task_numbers;
  tasks_t tasks;

  bool isTask(const QModelIndex& idx) const;

  Qt::DropActions supportedDragActions() const override;
};

class TaskEditDlg : public QDialog {
  Q_OBJECT

public:
  typedef TasksModel::task_t task_t;
  typedef TasksModel::tasks_t tasks_t;
  typedef lgx::process::BaseProcessDefinition BaseProcessDefinition;

  TaskEditDlg(const tasks_t& ts, QHash<QString, BaseProcessDefinition>& stackProc,
              QHash<QString, BaseProcessDefinition>& meshProc,
              QHash<QString, BaseProcessDefinition>& globalProc, QWidget* parent = 0, Qt::WindowFlags f = 0);

  tasks_t tasks() {
    return model->tasks;
  }

  static void readTasks(const QString& filename, QHash<QString, BaseProcessDefinition>& stackProc,
                        QHash<QString, BaseProcessDefinition>& meshProc,
                        QHash<QString, BaseProcessDefinition>& globalProc, tasks_t& newTasks);
  static void readTasksFromINI(const QString& filename, QHash<QString, BaseProcessDefinition>& stackProc,
                               QHash<QString, BaseProcessDefinition>& meshProc,
                               QHash<QString, BaseProcessDefinition>& globalProc, tasks_t& newTasks);
  static void writeTask(QTextStream& pout, const QString& name, const task_t& task);
  static void writeTasks(QTextStream& pout, const tasks_t& tasks);
  static void loadTasksFromSettings(tasks_t& newTasks);
  static void saveTasksToSettings(const tasks_t& tasks);

protected:
  void setProcesses(const QString& type, QTreeWidget* widget);
  int getSelectedRow(int default_row = 0);
  QString getSelectedTaskName(const QString& op, const QString& msg);
  QString getCurrentTaskName();

protected slots:
  void on_newTask_clicked();
  void on_deleteTask_clicked();
  void on_exportTask_clicked();
  void on_importTask_clicked();
  void on_renameTask_clicked();
  void on_copyTask_clicked();
  void on_makeSharedTask_clicked();
  void on_copyTaskToLocal_clicked();
  void on_tasks_deleteItems(const QModelIndexList& lst);
  void on_tasks_activated(const QModelIndex& index);

private:
  Ui::TaskEditDlg ui;
  TasksModel* model;

  QHash<QString, BaseProcessDefinition>& stackProc;
  QHash<QString, BaseProcessDefinition>& meshProc;
  QHash<QString, BaseProcessDefinition>& globalProc;
};

#endif
