#.rst
# FindSphinx
# ----------
#
# Find Sphinx documentation generator
#
# This module sets the following result variables:
#
#     SPHINX_FOUND:   Whether sphinx was found or not
#     SPHINX_BUILD:   Path to the sphinx-build program
#     SPHINX_VERSION: Version of the sphinx installed
#
# This module also creates the following macro:
#
#     BUILD_SPHINX_DOC(target type folder) `target` is the name of the custom
#          target, `type` is a type supported by sphinx (html, latex, ...) and
#          `folder` is the location of the sphinx makefile. This also create a
#          variable called ${target}_OUTPUT containing the folder where the
#          documentation can be found.

find_program(SPHINX_BUILD sphinx-build)

if(SPHINX_BUILD)
  execute_process(COMMAND ${SPHINX_BUILD} --version
    OUTPUT_VARIABLE SPHINX_VERSION_FULL
    ERROR_VARIABLE SPHINX_VERSION_ERROR
    RESULT_VARIABLE SPHINX_VERSION_RESULT)
  if(SPHINX_VERSION_RESULT EQUAL 0)
    string(REGEX REPLACE "Sphinx \\(.*\\) ([0-9]+\\.[0-9]+\\.[0-9]+).*" "\\1"
      SPHINX_VERSION
      ${SPHINX_VERSION_FULL})

    macro(_BUILD_SPHINX_DOC target type folder env_var extra_command)
      set(${target}_OUTPUT "${folder}/build/${type}")
      if(WIN32)
        list(APPEND ${env_var} "MAKE=${CMAKE_MAKE_PROGRAM}")
        set(SPHINX_COMMAND ${CMAKE_COMMAND} -E env ${${env_var}} "make.bat" ${type})
      else()
        set(SPHINX_COMMAND ${CMAKE_MAKE_PROGRAM} ${${env_var}} ${type})
      endif()
      if("${extra_command}" STREQUAL "")
        add_custom_target(${target}
          COMMAND ${SPHINX_COMMAND}
          WORKING_DIRECTORY ${folder})
      else()
        add_custom_target(${target}
          COMMAND ${SPHINX_COMMAND}
          COMMAND ${extra_command}
          WORKING_DIRECTORY ${folder})
      endif()
    endmacro(_BUILD_SPHINX_DOC)

    macro(BUILD_SPHINX_DOC target type folder)
      set(SPHINX_ENV_VAR "SPHINXBUILD=${SPHINX_BUILD}")
      if((${type} STREQUAL "latexpdf") OR (${type} STREQUAL "latexpdfja"))
        if(${type} STREQUAL "latexpdfja")
          set(SPHINXJA "-ja")
        else()
          set(SPHINXJA "")
        endif()
        find_package(LATEX COMPONENTS XELATEX LUALATEX)
        if(LATEX_XELATEX_FOUND)
          set(SPHINX_EXTRA_COMMAND ${CMAKE_MAKE_PROGRAM} -C build/latex "PDFLATEX=${XELATEX_COMPILER}" all-pdf${SPHINXJA})
        elseif(LATEX_LUALATEX_FOUND)
          set(SPHINX_EXTRA_COMMAND ${CMAKE_MAKE_PROGRAM} -C build/latex "PDFLATEX=${LUAATEX_COMPILER}" all-pdf${SPHINXJA})
        else()
          message(ERROR "Error, compiling the PDF documentation requires xelatex or lualatex")
        endif()
        _build_sphinx_doc(${target} latex "${folder}" SPHINX_ENV_VAR "${SPHINX_EXTRA_COMMAND}")
      else()
        _build_sphinx_doc(${target} ${type} "${folder}" SPHINX_ENV_VAR "")
      endif()
    endmacro(BUILD_SPHINX_DOC)
  endif()
endif()

find_package(PackageHandleStandardArgs)
find_package_handle_standard_args(SPHINX
  REQUIRED_VARS SPHINX_BUILD SPHINX_VERSION
  VERSION_VAR SPHINX_VERSION
)

