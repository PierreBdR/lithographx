/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "Store.hpp"
#include "Stack.hpp"

#include <QFileInfo>
#include "Information.hpp"
#include <string.h>
#include "Dir.hpp"
#include <climits>

#ifdef _OPENMP
#  include <omp.h>
#endif

namespace {
constexpr size_t histogram_size = 1024;
constexpr size_t shift_to_hist_size = 6;
} // namespace

namespace lgx {
Store::Store(Stack* stack)
  : _label(false)
  , _changed_function(false)
  , _opacity(1.0f)
  , _brightness(1.0f)
  , _changed()
  , _isVisible(false)
  , _stack(stack)
  , _invalidated_histogram(false)
  , _histogram(histogram_size, 0)
{
  allocate();
}

Store::Store(const Store& copy)
  : _data(copy._data)
  , _label(copy._label)
  , _changed_function(false)
  , _opacity(copy._opacity)
  , _brightness(copy._brightness)
  , _changed()
  , _isVisible(copy._isVisible)
  , _stack(copy._stack)
  , _invalidated_histogram(copy._invalidated_histogram)
  , _histogram(copy._histogram)
{
}

Store::~Store() {
  _data.clear();
}

void Store::allocate()
{
  Point3u size = _stack->size();
  size_t s = size_t(size.x()) * size.y() * size.z();

  _data.resize(s);
}

void Store::reset()
{
  allocate();
  setFile();
  memset(_data.data(), 0, _data.size() * sizeof(ushort));
  _histogram.clear();
  _histogram.resize(histogram_size, 0);
}

void Store::copyMetaData(const Store* other)
{
  if(other != this) {
    setFile(other->file());
    setLabels(other->labels());
    setTransferFct(other->transferFct());
  }
}

void Store::setFile(const QString& file)
{
  if(file.isEmpty()) {
    _filename = file;
  } else {
    _filename = util::absoluteFilePath(file);
  }
}

void Store::resetModified()
{
  _changed_function = false;
  _changed = BoundingBox3i();
}

void Store::setStack(Stack* s)
{
  _stack = s;
}

void Store::changed()
{
  _changed = _stack->boundingBox();
  _invalidated_histogram = true;
}

void Store::changed(const BoundingBox3i& bbox)
{
  _changed |= bbox;
  _invalidated_histogram = true;
}


void swapMetaData(Store* s1, Store* s2)
{
  if(s1 == s2)
    return;
  // Labels
  bool l1 = s1->labels();
  s1->setLabels(s2->labels());
  s2->setLabels(l1);

  // Files
  QString f1 = s1->file();
  s1->setFile(s2->file());
  s2->setFile(f1);

  auto tf1 = s1->transferFct();
  auto tf2 = s2->transferFct();
  s1->setTransferFct(tf2);
  s2->setTransferFct(tf1);
}

void Store::updateHistogram(bool force)
{
  if(force or _invalidated_histogram) {
    ushort minValue = 0xFFFF;
    ushort maxValue = 0x0;
    int nb_threads = 1;
#ifdef _OPENMP
    nb_threads = omp_get_max_threads();
#endif

    std::vector<uint64_t> histograms(histogram_size*nb_threads);

#pragma omp parallel for reduction(min: minValue) \
                         reduction(max: maxValue)
    for(size_t i = 0 ; i < _data.size() ; ++i) {
      ushort s = _data[i];
      int pos = int(s) >> shift_to_hist_size;
#ifdef _OPENMP
      pos *= nb_threads;
      pos += omp_get_thread_num();
#endif
      histograms[pos]++;
      if(s < minValue) minValue = s;
      if(s > maxValue) maxValue = s;
    }
    _histogram.clear();
    if(nb_threads == 1)
      _histogram = std::move(histograms);
    else {
      _histogram.resize(histogram_size, 0);
#pragma omp parallel for
      for(size_t i = 0 ; i < histogram_size ; ++i) {
        size_t pos = i*nb_threads;
        uint64_t acc = 0;
        for(int j = 0 ; j < nb_threads ; ++j)
          acc += histograms[pos+j];
        _histogram[i] = acc;
      }
    }

    if(minValue > maxValue)
      minValue = maxValue = 0;
    _bounds[0] = minValue;
    _bounds[1] = maxValue;
  }
  _invalidated_histogram = false;
}

void Store::updateBounds(bool force)
{
  if(_invalidated_histogram or force) {
    ushort low = 0xFFFF;
    ushort high = 0x0;
#pragma omp parallel for reduction(min: low) reduction(max: high)
    for(size_t i = 0 ; i < _data.size() ; ++i) {
      if(low > _data[i]) low = _data[i];
      if(high < _data[i]) high = _data[i];
    }
    if(low > high)
      low = high = 0;
    _bounds[0] = low;
    _bounds[1] = high;
  }
}

void Store::setHistogram(const std::vector<uint64_t>& h)
{
  if(h.size() == histogram_size)
    _histogram = h;
}

void Store::setHistogram(std::vector<uint64_t>&& h)
{
  if(h.size() == histogram_size)
    _histogram = std::move(h);
}

void Store::setTransferFct(const TransferFunction& f)
{
  if(_fct != f) {
    _fct = f;
    _changed_function = true;
  }
}

void Store::autoScale(double r)
{
  // Autoscale the brightness
  updateHistogram();
  Point2d range = TransferFunction::adjustmentRange(r, histogram());
  Information::out << "Min/max transfer function: " << range << endl;
  auto fct = transferFct();
  fct.adjust(range[0], range[1]);
  setTransferFct(fct);
}

void Store::setBounds(const Point2us& b)
{
  if(b[1] < b[0])
    _bounds = Point2us(b[1], b[0]);
  else
    _bounds = b;
}

} // namespace lgx
