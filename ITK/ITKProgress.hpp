/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef ITKPROGRESS_HPP
#define ITKPROGRESS_HPP

#include <ITKConfig.hpp>

#include <Progress.hpp>
#include <itkCommand.h>
#include <itkProcessObject.h>
#include <itkEventObject.h>

namespace lgx {
namespace process {
class itkutil_EXPORT ITKProgress {
public:
  ITKProgress(const QString& text);
  ~ITKProgress();

  void setFilter(itk::ProcessObject::Pointer filter);

  template <typename FilterPointer> void setFilter(const FilterPointer& filter)
  {
    setFilter(itk::ProcessObject::Pointer(filter));
  }

  void advance(const itk::EventObject& event);

  typedef itk::ReceptorMemberCommand<ITKProgress> Receptor;
  Receptor::Pointer eventHandler;

  Progress progress;
  unsigned long eventTag;
  itk::ProcessObject::Pointer filter;
  itk::ProgressEvent progressEvent;
};
}
}

#endif // ITKPROGRESS_HPP
