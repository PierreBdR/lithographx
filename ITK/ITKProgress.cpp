/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "ITKProgress.hpp"

namespace lgx {
namespace process {

ITKProgress::ITKProgress(const QString& text)
  : progress(text, 100, false)
  , eventTag(0)
{
  eventHandler = Receptor::New();
  eventHandler->SetCallbackFunction(this, &ITKProgress::advance);
}

void ITKProgress::setFilter(itk::ProcessObject::Pointer f)
{
  if(filter.IsNotNull())
    eventHandler->RemoveAllObservers();
  filter = f;
  if(filter.IsNotNull())
    eventTag = filter->AddObserver(itk::ProgressEvent(), eventHandler);
}

ITKProgress::~ITKProgress() {
}

void ITKProgress::advance(const itk::EventObject& event)
{
  if(progressEvent.CheckEvent(&event))
    progress.advance(int(100 * filter->GetProgress()));
}
}
}
