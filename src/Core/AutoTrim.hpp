/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef AUTO_TRIM_HPP
#define AUTO_TRIM_HPP

#include <Process.hpp>

#include <Stack.hpp>

namespace lgx {
namespace process {
/**
 * \class AutoTrim AutoTrim.hpp <AutoTrim.hpp>
 *
 * Trim stack boundary box (BBox) to keep only non-empty part. A voxel is
 * considered empty if its intensity is less or equal to the threshold.
 *
 * \ingroup StackProcess
 */
class LGXCORE_EXPORT AutoTrim : public StackProcess {
public:
  AutoTrim(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const ParmList& parms) override
  {
    if(!checkState().stack(STACK_ANY))
      return false;

    return (*this)(currentStack(), currentStack()->currentStore(), parms[0].toInt());
  }

  bool operator()(Stack* stack, Store* store, int threshold);

  QString folder() const override {
    return "Canvas";
  }
  QString name() const override {
    return "AutoTrim";
  }
  QString description() const override
  {
    return "Trim stack boundary box (BBox) to keep only non-empty part.\n"
           "A voxel is considered empty if its intensity is less or equal to the threshold.";
  }
  QStringList parmNames() const override {
    return QStringList() << "Threshold";
  }
  QStringList parmDescs() const override {
    return QStringList() << "Voxels below this threshold are considered empty.";
  }
  ParmList parmDefaults() const override {
    return ParmList() << 0;
  }
  QIcon icon() const override {
    return QIcon(":/images/AutoTrim.png");
  }
};
} // namespace process
} // namespace lgx
#endif
