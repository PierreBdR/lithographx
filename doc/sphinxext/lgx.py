from __future__ import unicode_literals
from docutils.parsers.rst import Directive
from docutils import io, nodes, statemachine, utils
from docutils.parsers.rst import directives
from docutils.statemachine import StringList
from sphinx.roles import XRefRole
from cgi import escape as escape_html
import sys

if sys.version_info.major < 3:
    str = unicode

def escape_tex(s):
    s = str(s)
    s = s.replace("\\", r'\textbackslash{}')
    s = s.replace("$", r'\$')
    s = s.replace("~", r'\textasciitilde{}')
    return s

class LGXProcess(nodes.General, nodes.Element):
    pass

class LGXProcessDirective(Directive):

    required_arguments = 0
    optional_arguments = 0
    final_argument_whitespace = False
    option_spec = {'name': str,
                   'folder': str,
                   'type': str,
                   'class': directives.class_option}

    has_content = True


    def run(self):
        args = []
        for i, l in enumerate(self.content):
            fields = l.split(':', 1)
            if fields and len(fields) != 2:
                error = self.state_machine.reporter.error(
                    'Error parsing content for the "{0}" directive: all lines must be '
                    'on the form "Name: Value"'.format(self.name), nodes.literal_block(l),
                    line = self.lineno+i)
            par = fields[0].strip()
            val = fields[1].strip()
            args.append([par, val])

        if 'type' not in self.options:
            error =  self.state_machine.reporter.error(
                'Error parsing arguments for the "%s" directive: the type must be '
                'specified.' % self.name, nodes.literal_block(
                    self.block_text, self.block_text), line=self.lineno)
            return [error]

        if 'name' not in self.options:
            error =  self.state_machine.reporter.error(
                'Error parsing arguments for the "%s" directive: the name must be '
                'specified.' % self.name, nodes.literal_block(
                    self.block_text, self.block_text), line=self.lineno)
            return [error]

        if 'folder' not in self.options:
            self.options['folder'] = ''
        else:
            self.options['folder'] += '/'

        node = LGXProcess(name = self.options['name'],
                          type = self.options['type'],
                          folder = self.options['folder'],
                          args = args)

        return [node]

def visit_lgxprocess_node_html(self, node):
    lines = ['<table border="1" class="docutils">',
             '<tr><th> Process </th> ',
             ' <th> <tt>[{0}]{1}{2}</tt> </th> </tr>'.format(escape_html(node['type']),
                                                             escape_html(node['folder']),
                                                             escape_html(node['name']))]
    args = node.get('args', [])
    if args:
        lines.append('<tr> <th> Parameter </th> <th> Value </th> </tr>')
        for (par, val) in args:
            lines += ['<tr> <td> {0} </td> <td> {1} </td> </tr>'.format(escape_html(par), escape_html(val))]
    self.body += lines

def depart_lgxprocess_node_html(self, node):
    self.body += ["</table>"]

def visit_lgxprocess_node_latex(self, node):
    lines = [
        r'\begin{tabular}{ll}',
        r'\textbf{{Process}} & \texttt{{[{0}]{1}{2}}} \\'.format(escape_tex(node['type']),
                                                             escape_tex(node['folder']),
                                                             escape_tex(node['name']))]

    args = node.get('args', [])
    if args:
        lines.append(r'\textbf{Parameter} & \textbf{Value}\\')
        for (par, val) in args:
            lines.append('{0} & {1} \\\\'.format(escape_tex(par), escape_tex(val)))
    self.body += lines

def depart_lgxprocess_node_latex(self, node):
    self.body += [r"\end{tabular}"]

def visit_lgxprocess_node_text(self, node):
    self.new_state()
    self.add_text("Process: [{0}]{1}{2}".format(node['type'], node['folder'], node['name']) + self.nl)
    args = node.get('args', [])
    if args:
        col1 = []
        col2 = []
        w1 = len('Parameter')
        w2 = len('Value')
        for par, val in args:
            col1.append(par)
            w1 = max(w1, len(par))
            col2.append(val)
            w2 = max(w2, len(val))
        w1 += 1
        w2 += 1
        self.add_text("{0}+{1}".format('-'*w1, '-'*w2) + self.nl)
        self.add_text("{0}| {1}".format('Parameter'.center(w1), 'Value') + self.nl)
        self.add_text("{0}+{1}".format('-'*w1, '-'*w2) + self.nl)
        for par, val in zip(col1, col2):
            self.add_text("{0}| {1}".format(par.ljust(w1), val) + self.nl)

def depart_lgxprocess_node_text(self, node):
    self.end_state(wrap=False)

def setup(app):
    app.add_directive('lgxprocess', LGXProcessDirective)
    app.add_node(LGXProcess,
                 html=(visit_lgxprocess_node_html, depart_lgxprocess_node_html),
                 latex=(visit_lgxprocess_node_latex, depart_lgxprocess_node_latex),
                 text=(visit_lgxprocess_node_text, depart_lgxprocess_node_text))
    return {'version': '0.1'}
