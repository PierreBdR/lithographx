/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "Clip.hpp"
#include "Geometry.hpp"

namespace lgx {
Clip::Clip(int id, QObject* parent)
  : QObject(parent)
  , _enable(false)
  , _showGrid(false)
  , _width(0.5f)
  , _gridSize(1.0f)
  , _normal(0.f, 0.f, 1.f)
  , _gridSquares(3u)
  , _changed(false)
{
  computeBasis();
  _clipNo = id;
}

bool Clip::isClipped(const Point3f& p)
{
  if(!_enable and !_showGrid)
    return false;

  Point3f cp(Point3f(_frame.coordinatesOf(qglviewer::Vec(p))));

  float d = fabs(cp * _normal);
  return d > _width;
}

void Clip::computeBasis() {
  getBasisFromPlane(_normal, _xb, _yb, _zb);
}

Point4f Clip::normalFormPos() const
{
  Point4f result(normal());
  result[3] = width();
  return result;
}

Point4f Clip::normalFormNeg() const
{
  Point4f result(-normal());
  result[3] = width();
  return result;
}
} // namespace lgx
