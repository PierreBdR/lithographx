/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "MeshProcessCreation.hpp"

#include "BoundingBox.hpp"
#include "CuttingSurface.hpp"
#include "Geometry.hpp"
#include "Information.hpp"
#include "Polygonizer.hpp"
#include "Progress.hpp"
#include "SetVector.hpp"
#include "Shapes.hpp"
#include "SystemProcessLoad.hpp"
#include "Tie.hpp"
#include "Utility.hpp"

#include <algorithm>
#include <exception>
#include <memory>
#include <unordered_map>

// Number of evals per progress check for marching cubes
const int EVAL_PROGRESS = 10000;

namespace lgx {
namespace process {
typedef util::Vector<3, vertex> Vtx3;
typedef util::Vector<4, vertex> Vtx4;
typedef util::Vector<5, vertex> Vtx5;
typedef util::Vector<7, vertex> Vtx7;

bool evalCheckProgress(Progress& progress)
{
  static int count = 0;
  if(count++ >= EVAL_PROGRESS) {
    count = 0;
    if(!progress.advance(0))
      return false;
  }
  return true;
}

template <size_t n> void makeNhbd(vvgraph& S, util::Vector<n, vertex> V)
{
  S.insertEdge(V[0], V[1]);
  for(size_t i = 2; i < n; ++i)
    S.spliceAfter(V[0], V[i - 1], V[i]);
}

// 3D surface evaluation function for marching cubes
int getStackData(const Stack* stack, const Store* store, Point3f& p)
{
  Point3f pf = stack->worldToImagef(p);
  // Be careful of float->int conversion when we are below 0
  for(int i = 0; i < 3; i++)
    if(pf[i] < 0)
      pf[i] -= 1;
  int x = int(pf.x());
  int y = int(pf.y());
  int z = int(pf.z());

  // Decide which stack to use
  const HVecUS& data = store->data();
  if(!stack->boundsOK(x, y, z))
    return (0);
  else
    return (data[stack->offset(x, y, z)]);
}

// Implicit surface evaluator functions, return true if inside
bool MarchingCubeSurface::eval(Point3f p)
{
  if(!evalCheckProgress(*progress))
    userCancel();
  int d = getStackData(_stack, _store, p);
  if(d >= _threshold)
    return true;
  else
    return false;
}

bool MarchingCubeSurface::operator()(Mesh* mesh, const Store* store, float cubeSize, int threshold)
{
  // Store threshold for eval
  if(threshold <= 0)
    threshold = 1;
  _threshold = threshold;

  // Setup progress bar
  progress = std::make_unique<Progress>(QString("Marching Cubes Surface on Stack %1").arg(mesh->userId()), 0);

  // Clear mesh
  vvgraph& S = mesh->graph();
  mesh->reset();

  mesh->updateAll();
  mesh->showLabel();

  // Find bounding box
  _store = store;
  _stack = store->stack();
  const HVecUS& data = store->data();
  Point3u size = _stack->size();

  // Count pixels and create bounding region(s)
  long pixCount = 0;
  BoundingBox3u bBox;
  Progress progress(QString("Finding object bounds-%1").arg(mesh->userId()), size.z());
  const ushort* pdata = data.data();
  int i = 0;
  for(uint z = 0; z < size.z(); z++) {
    if(!progress.advance(z))
      userCancel();
    for(uint y = 0; y < size.y(); y++) {
      for(uint x = 0; x < size.x(); x++, ++pdata, ++i) {
        // For normal march only count "inside" pixels
        if(*pdata < threshold)
          continue;
        pixCount++;

        // Find bounding box
        if(pixCount == 1) {
          bBox = BoundingBox3u(Point3u(x, y, z));
        } else {
          bBox |= Point3u(x, y, z);
        }
      }
    }
  }

  // Polygonizer object
  std::vector<Point3f> vertList;
  std::vector<Point3i> triList;
  util::Polygonizer pol(*this, cubeSize, _stack->step(), vertList, triList);

  // Get bounding box in world coordinates
  BoundingBox3f bBoxWorld = _stack->imageToWorld(bBox);

  SETSTATUS("Computing segmentation for threshold " << threshold << " with bBox of: " << bBoxWorld[0] << " - "
                                                    << bBoxWorld[1] << " cube size:" << cubeSize
                                                    << " pixel count:" << pixCount);

  // Call polygonizer, catch any errors
  triList.clear();
  Point3f marchBBox[2] = { bBoxWorld.pmin(), bBoxWorld.pmax() };
  pol.march(marchBBox);

  // Points to VVe vertices
  std::vector<vertex> vertices;
  vertices.resize(vertList.size());
  for(size_t i = 0; i < vertList.size(); ++i) {
    vertex v;
    v->pos = vertList[i];
    v->saveId = i;
    v->label = 0;
    vertices[i] = v;
  }

  // Insert vertices and triangles into mesh
  if(!shape::meshFromTriangles(S, vertices, triList)) {
    setErrorMessage("Error, cannot add all the triangles");
    return false;
  }

  SETSTATUS("Triangles added:" << triList.size() << ", vertices:" << S.size());
  mesh->signalBounds() = Point2f(0, 1);

  return true;
}
REGISTER_MESH_PROCESS(MarchingCubeSurface);

bool MarchingCube3D::eval(Point3f p)
{
  if(!evalCheckProgress(*progress))
    userCancel();
  int d = getStackData(_stack, _store, p);
  if(d == _label)
    return true;
  else
    return false;
}

bool MarchingCube3D::operator()(Mesh* mesh, const Store* store, float cubeSize, int minVoxels, int smooth,
                                int singleLabel)
{
  bool single = false;
  if(singleLabel > 0)
    single = true;

  // Setup progress bar
  progress = std::make_unique<Progress>(QString("Marching Cubes 3D on Stack %1").arg(mesh->userId()), 0);

  typedef std::pair<int, int> IntIntPair;
  typedef std::map<int, int> IntIntMap;
  typedef util::Vector<2, Point3i> Point3i2;
  typedef std::pair<int, Point3i2> IntPoint3i2Pair;
  typedef std::map<int, Point3i2> IntPoint3i2Map;

  // Clear mesh
  vvgraph& S = mesh->graph();
  vvgraph D;

  // If re-marching a single label, delete the old mesh part, otherwise reset it
  if(single) {
    forall(const vertex& v, S)
      if(v->label == singleLabel)
        D.insert(v);
    mesh->getConnectedVertices(D);
    forall(const vertex& v, D)
      S.erase(v);
  } else
    mesh->reset();

  mesh->updateAll();
  mesh->showLabel();

  // Find bounding box
  _store = store;
  _stack = store->stack();
  const HVecUS& data = store->data();
  Point3u size = _stack->size();

  // Count pixels and create bounding region(s)
  if(minVoxels <= 0)
    minVoxels = 1;
  int labelCount = 0;
  std::vector<long> pixCnt(65536, 0);
  std::vector<BoundingBox3u> bBoxImg(65536, BoundingBox3u());
  const ushort* pdata = data.data();
  int i = 0;
  for(uint z = 0; z < size.z(); z++) {
    for(uint y = 0; y < size.y(); y++) {
      for(uint x = 0; x < size.x(); x++, ++pdata, ++i) {
        // Count pixels
        int label = *pdata;
        if(single and label != singleLabel)
          continue;
        pixCnt[label]++;
        if(pixCnt[label] == minVoxels)
          labelCount++;

        // Find bounding box
        if(pixCnt[label] == 1) {
          bBoxImg[label] = BoundingBox3u(Point3u(x, y, z));
        } else {
          bBoxImg[label] |= Point3u(x, y, z);
        }
      }
    }
  }

  // Polygonizer object
  std::vector<Point3f> vertList;
  std::vector<Point3i> triList;
  util::Polygonizer pol(*this, cubeSize, _stack->step(), vertList, triList);

  // Call marching cubes for each label
  int maxLabel = 0;
  int startLabel = 1;
  if(single)
    startLabel = singleLabel;
  int endLabel = 65535;
  if(single)
    endLabel = singleLabel;

  for(int label = startLabel; label <= endLabel; ++label) {
    if(pixCnt[label] < minVoxels)
      continue;
    if(label > maxLabel)
      maxLabel = label;

    // Get bounding box in world coordinates
    BoundingBox3f bBox = _stack->imageToWorld(bBoxImg[label]);
    Point3f bBoxWorld[2] = { bBox.pmin(), bBox.pmax() };

    SETSTATUS("Computing segmentation for label " << label << " with bbox of: " << bBoxWorld[0] << " - "
                                                  << bBoxWorld[1] << " cube size:" << cubeSize
                                                  << " pixels:" << pixCnt[label]);

    // Store label for eval
    _label = label;

    // Call polygonizer
    triList.clear();
    pol.march(bBoxWorld);

    // Points to VVe vertices
    std::vector<vertex> vertices;
    // For now generate new vertices each time through
    std::map<int, int> vertMap;
    std::map<int, int>::iterator vertIter;
    forall(Point3i& tri, triList) {
      for(int i = 0; i < 3; i++) {
        if((vertIter = vertMap.find(tri[i])) != vertMap.end()) {
          tri[i] = vertIter->second;
        } else {
          vertex v;
          v->pos = vertList[tri[i]];
          v->saveId = tri[i];
          v->label = label;
          vertMap[tri[i]] = vertices.size();
          tri[i] = vertices.size();
          vertices.push_back(v);
        }
      }
    }

    // Insert vertices and triangles into mesh
    if(!shape::meshFromTriangles(S, vertices, triList)) {
      setErrorMessage("Error, cannot add all the triangles");
      return false;
    }

    SETSTATUS("Label:" << label << ", triangles added:" << triList.size() << ", vertices:" << S.size());
  }

  // smooth points but keep them attached
  if(!single) {
    for(int i = 0; i < smooth; i++) {
      std::map<int, Point3f> posMap;
      std::map<int, int> cntMap;
      forall(const vertex& v, S)
        forall(const vertex& n, S.neighbors(v)) {
          posMap[v->saveId] += n->pos;
          cntMap[v->saveId]++;
        }
      forall(const vertex& v, S)
        v->pos = posMap[v->saveId] / cntMap[v->saveId];
    }
  }

  mesh->signalBounds() = Point2f(0, 1);
  // Update next label if required
  if(maxLabel > mesh->viewLabel())
    mesh->setLabel(maxLabel + 1);

  return true;
}
REGISTER_MESH_PROCESS(MarchingCube3D);

bool CuttingSurfMesh::operator()(Mesh* mesh)
{
  CuttingSurface* cutSurf = cuttingSurface();
  if(!cutSurf) {
    setErrorMessage("You need an active cutting surface to convert it into a mesh");
    return false;
  }
  std::vector<Point3f> points;
  int uSize = 0, vSize = 0;
  cutSurf->getSurfPoints(&mesh->stack()->frame(), points, uSize, vSize);
  if(points.empty() or uSize < 2 or vSize < 2)
    return true;

  // Create a new graph
  vvgraph S;

  std::vector<vertex> empty_vector(vSize, vertex(0));   // VC++ bug requires this
  std::vector<std::vector<vertex> > vtx(uSize, empty_vector);

  // Create the vertices and fill in the position
  for(int u = 0; u < uSize; u++) {
    for(int v = 0; v < vSize; v++) {
      vertex a;
      S.insert(a);
      vtx[u][v] = a;
      a->pos = points[u * vSize + v];
      a->txpos.x() = float(u) / float(uSize - 1);
      a->txpos.y() = float(v) / float(vSize - 1);
    }
  }

  // Connect neighborhoods
  for(int u = 0; u < uSize; u++)
    for(int v = 0; v < vSize; v++) {
      if(u == 0 and v == vSize - 1)       // top left corner;
        makeNhbd(S, Vtx3(vtx[u][v], vtx[u][v - 1], vtx[u + 1][v]));
      else if(u == 0 and v == 0)       // bottom left corner
        makeNhbd(S, Vtx3(vtx[u][v], vtx[u + 1][v], vtx[u][v + 1]));
      else if(u == uSize - 1 and v == 0)       // bottom right corner
        makeNhbd(S, Vtx3(vtx[u][v], vtx[u][v + 1], vtx[u - 1][v]));
      else if(u == uSize - 1 and v == vSize - 1)       // top right corner
        makeNhbd(S, Vtx3(vtx[u][v], vtx[u - 1][v], vtx[u][v - 1]));
      else if(v == vSize - 1)       // top edge
        makeNhbd(S, Vtx4(vtx[u][v], vtx[u - 1][v], vtx[u][v - 1], vtx[u + 1][v]));
      else if(u == 0)       // left edge
        makeNhbd(S, Vtx4(vtx[u][v], vtx[u][v - 1], vtx[u + 1][v], vtx[u][v + 1]));
      else if(v == 0)       // bottom edge
        makeNhbd(S, Vtx4(vtx[u][v], vtx[u + 1][v], vtx[u][v + 1], vtx[u - 1][v]));
      else if(u == uSize - 1)       // right edge
        makeNhbd(S, Vtx4(vtx[u][v], vtx[u][v + 1], vtx[u - 1][v], vtx[u][v - 1]));
      else       // Interior vertex
        makeNhbd(S, Vtx5(vtx[u][v], vtx[u + 1][v], vtx[u][v + 1], vtx[u - 1][v], vtx[u][v - 1]));
    }

  // At this point we have a quad mesh, triangulate it (nicely)
  vvgraph T = S;
  forall(const vertex& v, T)
    forall(const vertex& n, T.neighbors(v)) {
      vertex m = S.nextTo(v, n);
      vertex w = S.nextTo(m, v);
      // Unique quad found
      if(w == S.prevTo(n, v) and v > w and n > m) {
        vertex a;
        a->pos = (v->pos + w->pos + n->pos + m->pos) / 4.0f;
        a->txpos = (v->txpos + w->txpos + n->txpos + m->txpos) / 4.0f;
        S.insert(a);
        makeNhbd(S, Vtx5(a, v, n, w, m));

        S.spliceAfter(v, n, a);
        S.spliceAfter(n, w, a);
        S.spliceAfter(w, m, a);
        S.spliceAfter(m, v, a);
      }
    }

  mesh->graph().swap(S);

  mesh->signalBounds() = Point2f(0, 1);
  mesh->setCells(false);
  SETSTATUS("Mesh " << mesh->userId() << " made from cutting surface");
  mesh->updateAll();
  return true;
}
REGISTER_MESH_PROCESS(CuttingSurfMesh);

namespace {

// Return an ordered pair (a,b) such that a <= b
template <typename T> util::Vector<2, T> makePair(const T& a, const T& b)
{
  if(a <= b)
    return util::Vector<2, T>(a, b);
  return util::Vector<2, T>(b, a);
}

// Return an ordered triplet (a,b,c) such that a <= b <= c
template <typename T> util::Vector<3, T> makeTriplet(const T& a, const T& b, const T& c)
{
  if(a <= b) {
    if(b <= c)
      return util::Vector<3, T>(a, b, c);
    if(a <= c)
      return util::Vector<3, T>(a, c, b);
    return util::Vector<3, T>(c, a, b);
  }
  if(b <= c) {
    if(c <= a)
      return util::Vector<3, T>(b, c, a);
    return util::Vector<3, T>(b, a, c);
  }
  return util::Vector<3, T>(c, b, a);
}

enum CORNER_POSITION { BOTTOM_LEFT = 0, BOTTOM_RIGHT = 1, TOP_RIGHT = 2, TOP_LEFT = 3, INVALID_POS = -1 };

CORNER_POSITION turn_cw(CORNER_POSITION c)
{
  switch(c) {
  case BOTTOM_LEFT:
    return TOP_LEFT;
  case BOTTOM_RIGHT:
    return BOTTOM_LEFT;
  case TOP_RIGHT:
    return BOTTOM_RIGHT;
  case TOP_LEFT:
    return TOP_RIGHT;
  default:
    throw QString("Programming error: invalid corner position in turn_cw");
  }
}

CORNER_POSITION turn_ccw(CORNER_POSITION c)
{
  switch(c) {
  case BOTTOM_LEFT:
    return BOTTOM_RIGHT;
  case BOTTOM_RIGHT:
    return TOP_RIGHT;
  case TOP_RIGHT:
    return TOP_LEFT;
  case TOP_LEFT:
    return BOTTOM_LEFT;
  default:
    throw QString("Programming error: invalid corner position in turn_cw");
  }
}

typedef util::Vector<4, ushort> Point4us;

class StructureError : public std::exception
{
public:
  virtual void markGraph(vvgraph& S) const = 0;

  const char* what() const throw () override
  {
    return msg.data();
  }

protected:
  QByteArray msg;
};

class OppositeCornersError : public StructureError
{
public:
  OppositeCornersError(ushort cid, const Point2u& corner, const Point3f& mpos, const Point2f& dp)
    : cornerPos(mpos)
    , pixelSize(dp.x(), dp.y(), 0)
  {
    QString errorMsg("Error at position (%1,%2): the cell %3 appears in two opposite corners.");
    errorMsg = errorMsg.arg(corner.x()).arg(corner.y()).arg(cid);
    msg = errorMsg.toUtf8();
  }

  OppositeCornersError(const OppositeCornersError&) = default;
  OppositeCornersError(OppositeCornersError&&) = default;

  OppositeCornersError& operator=(const OppositeCornersError&) = default;
  OppositeCornersError& operator=(OppositeCornersError&&) = default;

  void markGraph(vvgraph& S) const override
  {
    Point3f nSize(-pixelSize.y(), pixelSize.x(), 0);
    vertex v1, v2, v3, v4;
    v1->pos = cornerPos + 2*pixelSize;
    v2->pos = cornerPos - 2*pixelSize;
    v3->pos = cornerPos + 2*nSize;
    v4->pos = cornerPos - 2*nSize;
    v1->nrml = v2->nrml = v3->nrml = v4->nrml = Point3f(0,0,1);
    v1->label = v2->label = v3->label = v4->label = -1;
    S.insert(v1);
    S.insert(v2);
    S.insert(v3);
    S.insert(v4);
    S.insertEdge(v1, v2);
    S.insertEdge(v2, v1);
    S.insertEdge(v3, v4);
    S.insertEdge(v4, v3);
  }

private:
  Point3f cornerPos;
  Point3f pixelSize;
};

class ManyEdgeError : public StructureError
{
public:
  ManyEdgeError(ushort label, const Point3f& pos, const Point3f& dp)
    : errorPos(pos)
    , pixelSize(dp.x(), dp.y(), 0)
  {
    QString msgStr = QString("Error, cell %1 has more than one edge: either it's in many parts, or it has holes.").arg(label);
    msg = msgStr.toUtf8();
  }

  void markGraph(vvgraph& S) const override
  {
    std::array<vertex, 20> vs;
    for(int i = 0 ; i < 10 ; ++i) {
      vertex vo, vi;
      Point3f dp(cos(2*M_PI*i / 10)*pixelSize.x(),
                 sin(2*M_PI*i / 10)*pixelSize.y(),
                 0);
      vo->pos = errorPos + 4*dp;
      vi->pos = errorPos + 2*dp;
      vo->label = vi->label = -1;
      vs[i] = vo;
      vs[i+10] = vi;
      S.insert(vo);
      S.insert(vi);
    }

    int prev = 9;
    for(int i = 0 ; i < 10 ; ++i) {
      S.insertEdge(vs[i], vs[prev]);
      S.insertEdge(vs[prev], vs[i]);
      S.insertEdge(vs[10+i], vs[10+prev]);
      S.insertEdge(vs[10+prev], vs[10+i]);
      prev = i;
    }
  }

private:
  Point3f errorPos;
  Point3f pixelSize;
};

class NoEdgeEndError : public StructureError
{
public:
  NoEdgeEndError(ushort c1, ushort c2,
                 const Point2u& from_, const Point2u& to_,
                 const Point3f& from_p, const Point3f& to_p,
                 const Point2f& step)
    : from(from_p)
    , to(to_p)
    , step(step)
  {

    QString msgStr("Couldn't find the end of the edge between %1 and %2, starting from (%3,%4) and arrived at "
                "(%5,%6)");
    msgStr = msgStr.arg(c1).arg(c2).arg(from_.x()).arg(from_.y()).arg(to_.x()).arg(to_.y());
    msg = msgStr.toUtf8();
  }

  void markGraph(vvgraph& S) const override
  {
    std::array<vertex, 10> c1;
    std::array<vertex, 10> c2;
    std::array<vertex, 5> line;
    for(int i = 0 ; i < 10 ; ++i) {
      vertex v1, v2;
      Point3f dp = 4*Point3f(std::cos(2*M_PI*i/10)*step.x(),
                             std::sin(2*M_PI*i/10)*step.y(),
                             0);
      v1->pos = from + dp;
      v2->pos = to + dp;
      v1->label = v2->label = -1;
      c1[i] = v1;
      c2[i] = v2;
      S.insert(v1);
      S.insert(v2);
    }

    // Make circle
    int prev = 9;
    for(int i = 0 ; i < 10 ; ++i) {
      S.insertEdge(c1[i], c1[prev]);
      S.insertEdge(c1[prev], c1[i]);
      S.insertEdge(c2[i], c2[prev]);
      S.insertEdge(c2[prev], c2[i]);
      prev = i;
    }

    // Make line between the points
    Point3f dv = to - from;
    Point3f dn(-dv.y(), dv.x(), 0);
    // Turn 30°
    Point3f p1 = dv/4 + dn/4;
    Point3f mid = dv/2 + dn/std::sqrt(3);
    Point3f p3 = 3*dv/4 + dn/4;
    line[0]->pos = from;
    line[1]->pos = p1;
    line[2]->pos = mid;
    line[3]->pos = mid;
    line[4]->pos = to;
    for(int i = 0 ; i < 5 ; ++i) {
      S.insert(line[i]);
      line[i]->label = -1;
      if(i > 0) {
        S.insertEdge(line[i], line[i-1]);
        S.insertEdge(line[i-1], line[i]);
      }
    }
  }

private:
  Point3f from, to;
  Point2f step;
};

/*
 * Read-only 2D data holder with helper methods
 *
 * The corners are named after the pixel on their top-right. So the corner (x,y) is such that:
 *
 *    (x-1,y)  |  (x,y)
 *   ----------*--------
 *   (x-1,y-1) | (x,y-1)
 *
 */
struct Data2D {

  // Structure holding an edge with its shape which cells its separate
  struct Edge {
    Point2u cells;                 // Cells the edge separate
    std::vector<vertex> shape;     // The edge itself, including end points
  };

  Data2D(const Store* store, vvgraph& S_, uint zslice, float ml, bool showErr)
    : data(store->data())
    , S(S_)
    , minLength(ml)
    , minLengthSq(ml * ml)
    , centers(65536, Point3f())
    , sizes(65536, 0u)
    , cells(65536, vertex(0))
    , cell_joints(65536)
    , showErrors(showErr)
  {
    const Stack* stk = store->stack();
    size = Point2u(stk->size());
    step = Point2f(stk->step());
    origin = stk->origin() + 0.5 * stk->step() + zslice * stk->step().z();
    offset = zslice * step.x() * step.y();
    xdir = Point3f(step.x(), 0, 0);
    ydir = Point3f(0, step.y(), 0);
    pixelArea = step.x() * step.y();
  }

  ushort operator()(int x, int y) const
  {
    if(x >= 0 and y >= 0 and x < (int)size.x() and y < (int)size.y())
      return data[y * size.x() + x + offset];
    return 0;
  }

  // Return the position at the bottom left of the (x,y) corner
  ushort bottomLeft(int x, int y) const {
    return (*this)(x - 1, y - 1);
  }
  ushort bottomLeft(Point2i pos) const {
    return bottomLeft(pos.x(), pos.y());
  }

  // Return the position at the bottom right of the (x,y) corner
  ushort bottomRight(int x, int y) const {
    return (*this)(x, y - 1);
  }
  ushort bottomRight(Point2i pos) const {
    return bottomRight(pos.x(), pos.y());
  }

  // Return the position at the top left of the (x,y) corner
  ushort topLeft(int x, int y) const {
    return (*this)(x - 1, y);
  }
  ushort topLeft(Point2i pos) const {
    return topLeft(pos.x(), pos.y());
  }

  // Return the position at the top right of the (x,y) corner
  ushort topRight(int x, int y) const {
    return (*this)(x, y);
  }
  ushort topRight(Point2i pos) const {
    return topRight(pos.x(), pos.y());
  }

  /*
   * Return the cells around a corner as:
   *
   *  3 | 2
   *  --+--
   *  0 | 1
   *
   * which correspond to the CORNER_POSITION enumeration.
   */
  Point4us cornerCells(int x, int y) const
  {
    return Point4us(bottomLeft(x, y), bottomRight(x, y), topRight(x, y), topLeft(x, y));
  }

  template <typename T> Point3f imageToWorld(T x, T y) const {
    return origin + float(x) * xdir + float(y) * ydir;
  }

  template <typename T> Point3f imageToWorld(const util::Vector<2, T>& pos) const {
    return imageToWorld(pos.x(), pos.y());
  }

  const HVecUS& data;
  vvgraph& S;
  size_t offset;
  Point2u size;
  Point2f step;
  Point3f origin;
  Point3f xdir;
  Point3f ydir;
  float minLength, minLengthSq;
  float pixelArea;
  bool showErrors;
  QList<std::shared_ptr<StructureError>> errors;

  /* Find edges in the store, starting from a given point:
   * The second parameter is the identity of the cells as:
   *
   *   3 | 2
   *   --+--
   *   0 | 1
   *
   * which correspond to the CORNER_POSITION enumeration.
   *
   * Warning: The function should only be called if there are at least three different cells around.
   */
  bool findEdges(Point2u startCorner, Point4us cells)
  {
    if((cells[TOP_RIGHT] == cells[BOTTOM_LEFT]) or (cells[TOP_LEFT] == cells[BOTTOM_RIGHT])) {
      ushort c = (cells[TOP_RIGHT] == cells[BOTTOM_LEFT] ? cells[TOP_RIGHT] : cells[TOP_LEFT]);
      if(showErrors) {
        errors << std::make_shared<OppositeCornersError>(c, startCorner, imageToWorld(startCorner), step);
        return false;
      } else
        throw OppositeCornersError(c, startCorner, imageToWorld(startCorner), step);
    }

    vertex v = joint(startCorner);
    std::vector<size_t>& known_edges = joint_edges[v];
    size_t prev_lab = cells[3];
    for(int i = 0; i < 4; ++i) {
      if(cells[i] != prev_lab) {
        cell_joints[cells[i]].insert(v);
        cell_joints[prev_lab].insert(v);

        // Check if we already know about the edge
        Point2u pc = Point2u(cells[i], prev_lab);
        Point2u opc = Point2u(prev_lab, cells[i]);
        bool found = false;
        for(size_t j = 0; j < known_edges.size(); ++j) {
          Point2u kc = edges[known_edges[j]].cells;
          if(kc == pc or kc == opc) {
            found = true;
            break;
          }
        }
        if(not found) {
          Edge e = makeEdge(startCorner, cells, CORNER_POSITION(i));
          size_t new_edge = edges.size();
          edges.push_back(e);
          known_edges.push_back(new_edge);
          joint_edges[e.shape.back()].push_back(new_edge);           // add to the other end
        }
      }
      prev_lab = cells[i];
    }
    return true;
  }

  vertex joint(Point2u pos)
  {
    std::unordered_map<Point2u, vertex>::const_iterator found = joints.find(pos);
    if(found != joints.end())
      return found->second;

    vertex v;
    v->pos = imageToWorld(Point2f(pos) - Point2f(.5, .5));
    v->nrml = Point3f(0, 0, 1);
    v->label = -1;
    v->type = 'j';
    S.insert(v);
    joints[pos] = v;
    inv_joints[v] = pos;
    return v;
  }

  // See description of the next function to know what is happening
  void moveAlong(Point2i& cur, Point4us& cells, CORNER_POSITION c)
  {
    switch(c) {
    case TOP_LEFT:
      cur.y()++;
      cells[BOTTOM_LEFT] = cells[TOP_LEFT];
      cells[BOTTOM_RIGHT] = cells[TOP_RIGHT];
      cells[TOP_LEFT] = topLeft(cur);
      cells[TOP_RIGHT] = topRight(cur);
      break;
    case TOP_RIGHT:
      cur.x()++;
      cells[TOP_LEFT] = cells[TOP_RIGHT];
      cells[BOTTOM_LEFT] = cells[BOTTOM_RIGHT];
      cells[TOP_RIGHT] = topRight(cur);
      cells[BOTTOM_RIGHT] = bottomRight(cur);
      break;
    case BOTTOM_RIGHT:
      cur.y()--;
      cells[TOP_LEFT] = cells[BOTTOM_LEFT];
      cells[TOP_RIGHT] = cells[BOTTOM_RIGHT];
      cells[BOTTOM_LEFT] = bottomLeft(cur);
      cells[BOTTOM_RIGHT] = bottomRight(cur);
      break;
    case BOTTOM_LEFT:
      cur.x()--;
      cells[TOP_RIGHT] = cells[TOP_LEFT];
      cells[BOTTOM_RIGHT] = cells[BOTTOM_LEFT];
      cells[TOP_LEFT] = topLeft(cur);
      cells[BOTTOM_LEFT] = bottomLeft(cur);
      break;
    default:
      throw QString("Programming error: invalid corner position in moveAlong");
    }
  }

  Edge makeEdge(Point2i start, Point4us cells, CORNER_POSITION cell_pos)
  {
    ushort toFollow = cells[cell_pos];
    ushort opposite = cells[turn_cw(cell_pos)];
    Edge e;
    e.cells = Point2u(toFollow, opposite);
    e.shape.push_back(joint(start));
    size_t max = size.x() * size.y();     // We can never make more iterations than that!

    /*  The idea is to follow the edge going counter-clockwise always!
     *
     *  For this, we can see that we have four cases for the movement, depending on the position of the current
     *  pixel with respect to the current corner.
     *
     *  Movement cases: The current cell is marked with 'X'.
     *  Then, if 'F' has the current label, it will be the next position.
     *  If not, and the 'S' has the current label, it will be the next position.
     *  Otherwise, the current cell is the current position.
     *
     *  The current corner is marked with '#' and the next corner with '^'
     *
     *  Top-left -- move up
     *
     *  ·-·-·
     *  |S|F|
     *  ·-^-·
     *  |X| |
     *  ·-#-·
     *
     *  Top-Right -- move right
     *
     *  ·-·-·
     *  |X|S|
     *  #-^-·
     *  | |F|
     *  ·-·-·
     *
     *  Bottom-Right -- move down
     *
     *  ·-#-·
     *  | |X|
     *  ·-^-·
     *  |F|S|
     *  ·-·-·
     *
     *  Bottom-Left -- move left
     *
     *  ·-·-·
     *  |F| |
     *  ·-^-#
     *  |S|X|
     *  ·-·-·
     *
     *  Note that:
     *   - 'S' has the same position relative to '^' than 'X' to '#'
     *   - 'F' is the next position clockwise
     *   - 'X' the next position counter-clockwise
     *   - if the cell clockwise from the new one is not of the opposite color, this is the end
     */
    Point2i prev = start;
    Point2i cur = start;
    bool foundEnd = false;
    for(size_t cnt = 0; not foundEnd and cnt < max; ++cnt) {
      moveAlong(cur, cells, cell_pos);

      if(cells[cell_pos] == toFollow) {
        CORNER_POSITION next_pos = turn_cw(cell_pos);
        if(cells[next_pos] == toFollow)
          cell_pos = next_pos;
        else if(cells[next_pos] != opposite)
          foundEnd = true;
      } else if(cells[cell_pos] != opposite)
        foundEnd = true;
      else
        cell_pos = turn_ccw(cell_pos);         // Move back to the previous position

      if(minLength >= 0 and normsq(multiply(step, Point2f(cur) - Point2f(prev))) >= minLengthSq) {
        vertex j;
        j->type = 'j';
        j->label = -1;
        j->pos = imageToWorld(Point2f(cur) - Point2f(.5, .5));
        j->nrml = Point3f(0, 0, 1);
        e.shape.push_back(j);
        prev = cur;
      }
    }

    if(!foundEnd) {
      if(showErrors)
        errors << std::make_shared<NoEdgeEndError>(toFollow, opposite, prev, cur,
                                                   imageToWorld(prev), imageToWorld(cur), step);
      else
        throw NoEdgeEndError(toFollow, opposite, prev, cur, imageToWorld(prev), imageToWorld(cur), step);
    }

    if(minLength >= 0 and normsq(multiply(step, Point2f(cur) - Point2f(prev))) < 0.25 * minLengthSq) {
      if(e.shape.size() > 1)       // never remove the first vertex!
        e.shape.pop_back();
    }

    e.shape.push_back(joint(cur));
    // Information::out << "found edge" << endl;

    // Add the inside vertices
    for(size_t i = 0; i < e.shape.size(); ++i)
      S.insert(e.shape[i]);

    return e;
  }

  // Useful data structures

  std::vector<Point3f> centers;                         // Center of the cells by label
  std::vector<size_t> sizes;                            // Size of the cells in pixles by label
  std::vector<vertex> cells;                            // Vertex representing the cells by label
  std::vector<util::set_vector<vertex> > cell_joints;   // List of joints for each cell

  std::unordered_map<Point2u, vertex> joints;                     // Vertex representing each joint
  std::unordered_map<vertex, Point2u> inv_joints;                 // Reverse the joint map
  std::unordered_map<vertex, std::vector<size_t> > joint_edges;   // Give the list of edges for a vertex
  std::unordered_map<Point2u, Point4i> oriented_cells;            // Ordered list of cells around a corner
  std::vector<Edge> edges;                                        // List of edges

private: // Preventing copy
  Data2D(const Data2D&);
  Data2D& operator=(const Data2D&);
};

Point4i oriented4Cells(int c0, int c1, int c2, int c3)
{
  if(c0 <= c1) {
    if(c0 <= c2) {
      if(c0 <= c3)
        return Point4i(c0, c1, c2, c3);
    } else if(c2 <= c3)
      return Point4i(c2, c3, c0, c1);
  } else if(c1 <= c2) {
    if(c1 <= c3)
      return Point4i(c1, c2, c3, c0);
  } else if(c2 <= c3)
    return Point4i(c2, c3, c0, c1);
  return Point4i(c3, c0, c1, c2);
}

Point4i oriented3Cells(int c0, int c1, int c2)
{
  if(c0 <= c1 and c0 <= c2)
    return Point4i(c0, c1, c2, -1);
  if(c1 <= c2)
    return Point4i(c1, c2, c0, -1);
  return Point4i(c2, c0, c1, -1);
}

void insertLabel(std::vector<ushort>& labels, ushort label)
{
  bool found = false;
  for(size_t i = 0; i < labels.size(); ++i)
    if(labels[i] == label) {
      found = true;
      break;
    }
  if(!found)
    labels.push_back(label);
}

template <typename Iterator>
bool insertInCell(vvgraph& S, vertex c, vertex j, vertex jn, Iterator first, Iterator last,
                  util::set_vector<vertex>& js)
{
  bool has_looped = false;
  if(js.count(jn)) {
    js.erase(jn);
    S.spliceAfter(c, j, jn);
  } else
    has_looped = true;
  ++first;
  --last;
  for(Iterator it = first; it != last; ++it)
    S.spliceBefore(c, jn, *it);
  return has_looped;
}

QString processErrors(const QList<std::shared_ptr<StructureError>>& errors,
                      Mesh *mesh)
{
    vvgraph S;
    QStringList errorsStr;
    for(const auto& err: errors) {
      errorsStr << (" * " + QString::fromUtf8(err->what()));
      err->markGraph(S);
    }
    std::swap(S, mesh->graph());
    mesh->updateAll();
    mesh->setShowMesh(true);
    mesh->setShowMeshLines(true);
    mesh->setShowMeshPoints(true);
    mesh->showAllMesh();
    mesh->setCells(false);
    return errorsStr.join("\n");
}

} // namespace

bool CellMeshFrom2DImage::operator()(Mesh* mesh, const Store* store, float minLength, uint zslice, bool showErrors)
{
  const Stack* stk = store->stack();
  Point3u size = stk->size();

  if(size.z() <= zslice)
    return setErrorMessage(QString("Error, requested slice %1 is a stack with %2 slices").arg(zslice).arg(size.z()));

  vvgraph S;
  Data2D data(store, S, zslice, minLength, showErrors);

  bool found_corners = false;

  // First, first the corners, edges, centers and sizes of cells
  // We iterate on the *corners* not the cells
  for(int y = 0; y <= (int)data.size.y(); ++y)
    for(int x = 0; x <= (int)data.size.x(); ++x) {
      Point4us ccells = data.cornerCells(x, y);
      ushort lab = ccells[BOTTOM_LEFT];

      if(lab > 0) {
        Point3f pos = data.imageToWorld(x - 1, y - 1);
        data.sizes[lab]++;
        data.centers[lab] += pos;
      }

      // Check if there are three or more different colors
      // Same labels have to be consecutive ...
      std::vector<ushort> cells;
      cells.push_back(lab);
      insertLabel(cells, ccells[1]);
      insertLabel(cells, ccells[2]);
      insertLabel(cells, ccells[3]);

      if(cells.size() >= 3) {
        found_corners = true;
        Point2u pos(x, y);
        if(cells.size() == 4)
          data.oriented_cells[pos] = oriented4Cells(cells[0], cells[1], cells[2], cells[3]);
        else
          data.oriented_cells[pos] = oriented3Cells(cells[0], cells[1], cells[2]);
        // Information::out << "Cells around " << pos << ": " << data.oriented_cells[pos] << endl;
        data.findEdges(Point2u(x, y), ccells);
      }
    }

  if(not found_corners)
    return setErrorMessage("Error, no corner found. The algorithm cannot continue.");

  if(showErrors and not data.errors.empty()) {
    setWarningMessage(processErrors(data.errors, mesh));
    return true;
  }

  // Now, create the cells
  for(size_t i = 1; i < 65536; ++i) {
    if(data.sizes[i] > 0) {
      data.centers[i] /= data.sizes[i];
      vertex c;
      c->type = 'c';
      c->area = data.sizes[i] * data.pixelArea;
      c->pos = data.centers[i];
      c->nrml = Point3f(0, 0, 1);
      c->label = i;

      S.insert(c);
      data.cells[i] = c;
    }
  }

  QList<std::shared_ptr<StructureError>> errors;

  // Connect the cells
  for(size_t label = 1; label < 65536; ++label) {
    if(data.sizes[label] > 0) {
      // Copy to check we go through all the vertices
      util::set_vector<vertex> js = data.cell_joints[label];
      if(js.empty()) continue; // We have no neighbor ...

      /*
       * Information::out << "Joints for cell " << label << ":";
       * for(size_t i = 0 ; i < js.size() ; ++i)
       *  Information::out << " " << js[i].num();
       * Information::out << endl;
       */

      vertex c = data.cells[label];

      vertex start = js.front();
      S.insertEdge(c, start);

      vertex cur = start;
      size_t nb_edges = js.size();
      bool has_looped = false;
      for(size_t i = 0; not has_looped and i < nb_edges; ++i) {
        // Find the next edge, counter-clockwise
        // Information::out << "  edge from " << cur.num() << endl;
        const std::vector<size_t>& es = data.joint_edges[cur];
        for(size_t j = 0; j < es.size(); ++j) {
          const Data2D::Edge& e = data.edges[es[j]];
          if(e.cells[0] == label)           // This edge is oriented with the cell
          {
            if(e.shape.front() == cur)             // The current node is the source
            {
              has_looped = insertInCell(S, c, cur, e.shape.back(), e.shape.begin(), e.shape.end(), js);
              cur = e.shape.back();
              break;
            }
          } else if(e.cells[1] == label)           // This edge is oriented against the cell
          {
            if(e.shape.back() == cur)             // The current node is the target of the edge
            {
              has_looped = insertInCell(S, c, cur, e.shape.front(), e.shape.rbegin(), e.shape.rend(), js);
              cur = e.shape.front();
              break;
            }
          }
        }
      }
      if(not js.empty()) {
        if(showErrors) {
          errors << std::make_shared<ManyEdgeError>(label, start->pos, stk->step());
        } else
          throw ManyEdgeError(label, start->pos, stk->step());
      } else if(cur != start) {
        throw QString("Error, while making cell %1, it didn't close.").arg(label);
      }
    }
  }

  if(showErrors and not errors.empty()) {
    setWarningMessage(processErrors(errors, mesh));
    return true;
  }

  // Connect the intermediate junctions (it any)
  if(minLength >= 0) {
    for(size_t i = 0; i < data.edges.size(); ++i) {
      const Data2D::Edge& e = data.edges[i];
      vertex c1 = data.cells[e.cells[0]];
      vertex c2 = data.cells[e.cells[1]];
      for(size_t j = 1; j < e.shape.size() - 1; ++j) {
        vertex pv = e.shape[j - 1];
        vertex v = e.shape[j];
        vertex nv = e.shape[j + 1];

        if(c1)
          S.insertEdge(v, c1);
        if(c2)
          S.insertEdge(v, c2);
        if(c1) {
          S.spliceAfter(v, c1, pv);
          S.spliceBefore(v, c1, nv);
        } else {
          S.spliceBefore(v, c2, pv);
          S.spliceAfter(v, c2, nv);
        }
      }
    }
  }

  // Now connect joints
  for(std::unordered_map<Point2u, vertex>::const_iterator it = data.joints.begin(); it != data.joints.end(); ++it) {
    vertex j = it->second;
    Point2u jpos = it->first;
    Point4i cells = data.oriented_cells[jpos];
    // Information::out << "Connecting joint " << j.num() << " at pos " << jpos << endl;
    // Information::out << "    cells = " << cells << endl;
    vertex prev(0);
    bool onBorder = cells[0] == 0;
    for(size_t i = (onBorder ? 1 : 0); i < (cells[3] == -1 ? 3 : 4); ++i) {
      vertex c = data.cells[cells[i]];
      if(onBorder and i == 1) {
        // Information::out << "   from border of " << cells[i] << endl;
        vertex nj = S.nextTo(c, j);
        S.insertEdge(j, nj);
        prev = nj;
      }
      // Information::out << "    connect to cell " << cells[i] << endl;
      if(prev)
        S.spliceAfter(j, prev, c);
      else
        S.insertEdge(j, c);
      vertex pj = S.prevTo(c, j);
      S.spliceAfter(j, c, pj);
      prev = pj;
    }
  }

  // At last, clean cell centers without neighbors
  std::vector<vertex> to_remove;
  for(int i = 0 ; i < 1<<16 ; ++i) {
    if(S.empty(data.cells[i]))
      S.erase(data.cells[i]);
  }

  mesh->signalBounds() = Point2f(0, 1);
  mesh->graph().swap(S);
  mesh->setCells(true);
  mesh->updateAll();
  return true;
}

REGISTER_MESH_PROCESS(CellMeshFrom2DImage);

bool MeshFromLabelledPoints::operator()(Mesh* mesh, const Store* store, float radius, QString type, uint details)
{
  mesh->reset();
  const HVecUS& data = store->data();
  const Stack* stk = store->stack();
  Progress progress("MeshFromLabelledPoints", data.size()+1);
  std::unique_ptr<shape::AbstractShape> shape;
  QString t = type.toLower();
  if(t == "cube") {
    shape.reset(new shape::Cube);
  } else if(t == "uniformsphere") {
    shape.reset(new shape::UniformSphere(details));
  } else if(t == "regularsphere") {
    shape.reset(new shape::RegularSphere(details));
  } else {
    Information::err << "Warning, unknown shape '" << type << "', using RegularSphere" << endl;
  }
  if(not progress.advance(1)) userCancel();
  size_t di = data.size() / 100;
  for(size_t i = 0 ; i < data.size() ; ++i) {
    if(i % di == 0)
      if(not progress.advance(i+1)) userCancel();
    if(data[i] > 0) {
      Point3u pos = stk->position(i);
      Point3f p = stk->imageToWorld(pos);
      if(!shape->addTo(mesh, p, radius, data[i]))
        return setErrorMessage(shape->errorString());
    }
  }
  progress.advance(data.size()+1);
  mesh->updateAll();
  return true;
}

REGISTER_MESH_PROCESS(MeshFromLabelledPoints);

} // namespace process
} // namespace lgx
