/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/


#ifndef CORE_MESHINTERACTIONSTACKPROCESS_HPP
#define CORE_MESHINTERACTIONSTACKPROCESS_HPP

#include <Process.hpp>

#include <Misc.hpp>
#include <Stack.hpp>
#include <Store.hpp>

namespace lgx {
namespace process {

///\addtogroup StackProcess
///@{

/**
 * \class Annihilate StackProcess.hpp <StackProcess.hpp>
 *
 * Delete all but a layer of the stack just "below" the mesh.
 */
class LGXCORE_EXPORT Annihilate : public StackProcess {
public:
  Annihilate(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const ParmList& parms) override
  {
    if(!checkState().store(STORE_NON_EMPTY).mesh(MESH_NON_EMPTY))
      return false;
    Store* input = currentStack()->currentStore();
    Store* output = currentStack()->work();

    bool fill = parmToBool(parms[0]);
    bool res
      = (*this)(input, output, currentMesh(), parms[2].toFloat(), parms[3].toFloat(), fill, parms[1].toUInt());
    if(res) {
      input->hide();
      output->show();
    }
    return res;
  }

  /**
   * Annihilate the stack
   * \param input Input stack
   * \param output Stack storing the result
   * \param mesh Mesh used
   * \param minDist Distance from the mesh, taken "below" (i.e. against the normal)
   * \param maxDist Distance from the mesh, taken "below" (i.e. against the normal)
   */
  bool operator()(const Store* input, Store* output, const Mesh* mesh, float minDist, float maxDist,
                  bool fill, uint fillval);

  QString name() const override {
    return "Annihilate";
  }
  QString description() const override {
    return "Keep or fill a layer near the mesh";
  }
  QString folder() const override {
    return "Mesh Interaction";
  }
  QStringList parmNames() const override
  {
    return QStringList() << "Fill"
                         << "Fill Val" << QString("Min Dist(%1)").arg(UM)
                         << QString("Max Dist(%1)").arg(UM);
  }
  QStringList parmDescs() const override
  {
    return QStringList() << "Fill the layer with specified value, or keep the original data."
                         << "Value to fill the volume with."
                         << "Minimal distance from layer to mesh."
                         << "Maximal distance from layre to mesh";
  }
  ParmList parmDefaults() const override
  {
    return ParmList() << false
                      << 30000
                      << 1.0
                      << 5.0;
  }
  ParmChoiceMap parmChoice() const override
  {
    ParmChoiceMap map;
    map[0] = booleanChoice();
    return map;
  }
  QIcon icon() const override {
    return QIcon(":/images/Annihilate.png");
  }
};

/**
 * \class ClipStack StackProcess.hpp <StackProcess.hpp>
 *
 * Apply the active clipping planes to the current stack
 */
class LGXCORE_EXPORT ClipStack : public StackProcess {
public:
  ClipStack(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const ParmList&) override
  {
    if(!checkState().store(STORE_NON_EMPTY))
      return false;
    Store* input = currentStack()->currentStore();
    Store* output = currentStack()->work();
    bool res = (*this)(input, output);
    if(res) {
      input->hide();
      output->show();
    }
    return res;
  }

  bool operator()(const Store* input, Store* output);

  QString folder() const override {
    return "Mesh Interaction";
  }
  QString name() const override {
    return "Clip Stack";
  }
  QString description() const override {
    return "Trim stack to clipping planes";
  }
  QStringList parmNames() const override {
    return QStringList();
  }
  QStringList parmDescs() const override {
    return QStringList();
  }
  QIcon icon() const override {
    return QIcon(":/images/ClipStack.png");
  }
};

/**
 * \class StackMeshProcess StackProcess.hpp <StackProcess.hpp>
 *
 * Base class for a process that either fill or erase the inside part of a mesh in a stack.
 */
class LGXCORE_EXPORT StackMeshProcess : public StackProcess {
public:
  StackMeshProcess(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const ParmList& parms, bool fill)
  {
    if(!checkState().store(STORE_NON_EMPTY).mesh(MESH_NON_EMPTY))
      return false;
    Store* input = currentStack()->currentStore();
    Store* output = currentStack()->work();
    const Mesh* mesh = currentMesh();
    uint fillValue = 0;
    if(fill)
      fillValue = parms[0].toUInt();
    bool res = (*this)(input, output, mesh, fill, fillValue);
    if(res) {
      input->hide();
      output->show();
    }
    return res;
  }

  bool operator()(const Store* input, Store* output, const Mesh* mesh, bool fill, uint fillValue);
};

/**
 * \class FillStackToMesh StackProcess.hpp <StackProcess.hpp>
 *
 * Fill the volume contained by a closed mesh with a pre-defined intensity.
 */
class LGXCORE_EXPORT FillStackToMesh : public StackMeshProcess {
public:
  FillStackToMesh(const StackProcess& process)
    : Process(process)
    , StackMeshProcess(process)
  {
  }

  using StackMeshProcess::operator();

  bool operator()(const ParmList& parms) override {
    return (*this)(parms, true);
  }

  QString name() const override {
    return "Fill Stack from Mesh";
  }
  QString folder() const override {
    return "Mesh Interaction";
  }
  QString description() const override {
    return "Fill volume contained by closed mesh";
  }
  QStringList parmNames() const override {
    return QStringList() << "Fill Value";
  }
  QStringList parmDescs() const override {
    return QStringList() << "Fill Value";
  }
  ParmList parmDefaults() const override {
    return ParmList() << 32000;
  }
  QIcon icon() const override {
    return QIcon(":/images/TrimStack.png");
  }
};

/**
 * \class TrimStackProcess StackProcess.hpp <StackProcess.hpp>
 *
 * Set to 0 any voxel not contained within the closed mesh.
 */
class LGXCORE_EXPORT TrimStackProcess : public StackMeshProcess {
public:
  TrimStackProcess(const StackProcess& process)
    : Process(process)
    , StackMeshProcess(process)
  {
  }

  using StackMeshProcess::operator();

  bool operator()(const ParmList& parms) override {
    return (*this)(parms, false);
  }

  QString folder() const override {
    return "Mesh Interaction";
  }
  QString name() const override {
    return "Trim Stack";
  }
  QString description() const override {
    return "Trim parts of stack which are not contained within closed mesh.";
  }
  QStringList parmNames() const override {
    return QStringList();
  }
  QStringList parmDescs() const override {
    return QStringList();
  }
  QIcon icon() const override {
    return QIcon(":/images/TrimStack.png");
  }
};

/**
 * \class FillStack3D StackProcess.hpp <StackProcess.hpp>
 *
 * Fill the stack with labels from a labeled 3D mesh.
 */
class LGXCORE_EXPORT FillStack3D : public StackProcess {
public:
  FillStack3D(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const ParmList& ) override
  {
    if(!checkState().store(STORE_NON_EMPTY).mesh(MESH_NON_EMPTY))
      return false;
    Store* input = currentStack()->currentStore();
    Store* output = currentStack()->work();
    const Mesh* mesh = currentMesh();
    bool res = (*this)(input, output, mesh);
    if(res) {
      input->hide();
      output->show();
      output->setLabels(true);
    }
    return res;
  }

  bool operator()(const Store* input, Store* output, const Mesh* mesh);

  QString name() const override {
    return "Fill Stack from 3D Mesh";
  }
  QString folder() const override {
    return "Mesh Interaction";
  }
  QString description() const override {
    return "Fill stack contained by labeled 3D mesh";
  }
  QStringList parmNames() const override {
    return QStringList();
  }
  QStringList parmDescs() const override {
    return QStringList();
  }
  QIcon icon() const override {
    return QIcon(":/images/FillStack3D.png");
  }
};

/**
 * \class StackRelabelFromMesh StackProcess.hpp <StackProcess.hpp>
 *
 * Change the labels of a stack to match the ones of a labeled 3D cell mesh.
 * Unknown cells (i.e. cells in the stack, not in the mesh), can be either
 * kept or deleted. If kept, they will be relabeled to not conflict with
 * existing cells.
 */
class LGXCORE_EXPORT StackRelabelFromMesh : public StackProcess {
public:
  StackRelabelFromMesh(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const ParmList& parms) override
  {
    if(!checkState().store(STORE_NON_EMPTY | STORE_LABEL).mesh(MESH_NON_EMPTY | MESH_LABEL))
      return false;
    Stack* s = currentStack();
    Store* store = s->currentStore();
    Store* output = s->work();
    const Mesh* m = currentMesh();
    if((*this)(s, store, output, m, parmToBool(parms[0]))) {
      store->hide();
      output->show();
      return true;
    }
    return false;
  }

  bool operator()(Stack* stack, const Store* store, Store* output, const Mesh* mesh,
                  bool delete_unknown);

  QString folder() const override {
    return "Mesh Interaction";
  }
  QString name() const override {
    return "Relabel From Mesh";
  }
  QString description() const override
  {
    return "Relabel a 3D stack reusing the same labels as in the stack.\n"
           "Unknown cells (i.e. cells in the stack, not in the mesh), \n"
           "can be either kept or deleted. If kept, they will be relabeled\n"
           "to not conflict with existing cells.";
  }
  QStringList parmNames() const override {
    return QStringList() << "Delete unknown";
  }
  QStringList parmDescs() const override {
    return QStringList() << "Delete unknown";
  }
  ParmList parmDefaults() const override {
    return ParmList() << true;
  }
  ParmChoiceMap parmChoice() const override
  {
    ParmChoiceMap map;
    map[0] = booleanChoice();
    return map;
  }
  QIcon icon() const override {
    return QIcon(":/images/Relabel.png");
  }
};

///@}
} // namespace process
} // namespace lgx

#endif // CORE_MESHINTERACTIONSTACKPROCESS_HPP

