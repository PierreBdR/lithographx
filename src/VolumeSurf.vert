smooth centroid out vec3 texPos;
in vec3 texCoord;

void setTexCoord()
{
  vec4 tc = gl_TextureMatrix[0] * vec4(texCoord, 1);
  texPos = tc.xyz / tc.w;
}
