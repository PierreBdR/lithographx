**************************
Contributors and Licensing
**************************

LithoGraphX is licensed under the `GPL v3
<https://www.gnu.org/licenses/gpl-3.0-standalone.html>`_. It is a fork of
`MorphoGraphX <http://www.morphographx.org>`_ version 1.0 rev. 1256.

Contributors
============

* Dr. Barbier de Reuille, Pierre (lead developer)
* Dr. Robinson, Sarah
* Dr. Burian, Agata
* Dr. Summers, Holly
* Dr. Yoshida, Saiko

Past Contributors
=================

People listed have contributed to MorphoGraphX in significant ways, and in this
way also contributed to LithoGraphX:

* Dr. Weber, Alain
* Schuepbach, Thierry
* Toriello, Gerardo
* Dr. Strauss, Soeren
* Dr. Nakayama, Naomi
* Dr. Richard Smith's team

Funding Support
===============

* Google Inc.
* SystemsX.ch
* Swiss National Science Foundation
* University of Bern, Switzerland
