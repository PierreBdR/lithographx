/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef SYSTEMPROCESSLOAD_HPP
#define SYSTEMPROCESSLOAD_HPP

#include <Process.hpp>

#include <QObject>
#include <Mesh.hpp>
#include <Misc.hpp>

#include <QDialog>

#include <memory>
#include <vector>

/**
 * \file SystemProcessLoad.hpp
 * This file contains processes defined in LithoGraphX to load data
 */

class QDialog;
class QFile;
class QIODevice;
class QPixmap;

namespace Ui
{
class LoadStackDialog;
class LoadMeshDialog;
class MultiChannelOpenDlg;
}

namespace lgx {

struct ImageInfo;

namespace process {
LGX_EXPORT QList<int> extractVersion(QIODevice& file);

/**
 * \class StackSwapBytes SystemProcessLoad.hpp <SystemProcessLoad.hpp>
 *
 * Process that swap bytes
 *
 * \ingroup StackProcess
 */
class LGX_EXPORT StackSwapBytes : public StackProcess {
public:
  StackSwapBytes(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const ParmList&) override
  {
    if(!checkState().store(STORE_NON_EMPTY))
      return false;
    Store* input = currentStack()->currentStore();
    Store* output = currentStack()->work();
    bool res = (*this)(input, output);
    if(res) {
      input->hide();
      output->show();
    }
    return res;
  }

  bool operator()(const Store* input, Store* output);

  QString name() const override {
    return "Swap Bytes";
  }
  QString description() const override {
    return "Swap the bytes of the values in the stack.";
  }
  QString folder() const override {
    return "Filters";
  }
  QStringList parmNames() const override {
    return QStringList();
  }
  QStringList parmDescs() const override {
    return QStringList();
  }
  QIcon icon() const override {
    return QIcon(":/images/SwapBytes.png");
  }
};

class LGX_EXPORT StackImportDlg : public QDialog {
  Q_OBJECT
public:
  StackImportDlg(QWidget *parent, Qt::WindowFlags f = 0);
  ~StackImportDlg();

  Point3f imageResolution();
  float brightness() const;
  QStringList imageFiles() const;

public slots:
  void setBrightness(float value);
  void clearImageFiles();

  void LoadProfile(QString filename = QString());
  void setImageSize(Point3u size);
  void setImageSize(uint x, uint y, uint z) {
    setImageSize(Point3u(x, y, z));
  }
  void setImageResolution(Point3f step);
  void setImageResolution(float x, float y, float z) {
    setImageResolution(Point3f(x, y, z));
  }

  QString loadedFile() const { return _loadedFile; }

protected slots:
  void on_AddFiles_clicked();
  void on_RemoveFiles_clicked();
  void on_LoadProfile_clicked();
  void on_SaveProfile_clicked();
  void on_FilterFiles_clicked();
  void on_SortAscending_clicked();
  void on_SortDescending_clicked();
  void on_ImageFiles_filesDropped(const QStringList& files);

private:
  std::unique_ptr<Ui::LoadStackDialog> ui;
  QString _loadedFile;
  Point3u imageSize;
};

/**
 * \class StackImport SystemProcessLoad.hpp <SystemProcessLoad.hpp>
 *
 * Import images forming a stack.
 *
 * \ingroup StackProcess
 */
class LGX_EXPORT StackImport : public StackProcess {
public:
  StackImport(const StackProcess& proc)
    : Process(proc)
    , StackProcess(proc)
  {
  }

  bool initialize(ParmList& parms, QWidget* parent) override;

  bool operator()(const ParmList& parms) override
  {
    int stackId = parms[0].toInt();
    Point3f step(parms[2].value<float>(), parms[3].value<float>(), parms[4].value<float>());
    float brightness(parms[5].value<float>());
    QString filename(parms[6].toString());

    if(!checkState().stack(STACK_ANY, stackId))
      return false;

    // Get stack and store
    Stack* stk = stack(stackId);

    Store* store = stk->store(parms[1].value<STORE>());

    bool autoscale;
    float autoscale_range = parms[7].toFloat(&autoscale);

    return (*this)(stk, store, step, brightness, filename, autoscale, autoscale_range);
  }
  /**
   * Import the stack
   * \param stack Stack that will contain the image
   * \param store Store that will contain the image
   * \param filenames Either a single text file containing all the
   * parameters, or a list of image files, one per z slice
   * \param brightness Multiplication coefficient to convert less than 16
   * bits images to 16 bits
   * \param step Size of a voxel
   */
  bool operator()(Stack* stack, Store* store, Point3f step, float brightness, QString filename, bool autoscale, float autoscale_range);

  QString folder() const override {
    return "System";
  }
  QString name() const override {
    return "Import";
  }
  QString description() const override {
    return "Import stack from a series of images";
  }
  QStringList parmNames() const override
  {
    return QStringList() << "Stack"
                         << "Store"
                         << ("X Step (" + UM + ")")
                         << ("Y Step (" + UM + ")")
                         << ("Z Step (" + UM + ")")
                         << "Brightness"
                         << "Profile File"
                         << "Autoscale";
  }
  QStringList parmDescs() const override
  {
    return QStringList() << "Stack"
                         << "Store"
                         << ("Size of a voxel along the X axis in " + UM)
                         << ("Size of a voxel along the Y axis in " + UM)
                         << ("Size of a voxel along the Z axis in " + UM)
                         << "Brightness"
                         << "Profile File"
                         << "If not empty, the transfer function will be auto scaled with the range specified (a number from 0 to 1).";
  }
  ParmList parmDefaults() const override
  {
    return ParmList() << -1
                      << ""
                      << 1.0
                      << 1.0
                      << 1.0
                      << 1.0
                      << ""
                      << 0.99;
  }
  ParmChoiceMap parmChoice() const override
  {
    ParmChoiceMap map;
    map[1] = storeChoice();
    return map;
  }
  QIcon icon() const override {
    return QIcon(":/images/open.png");
  }
};

class LGX_EXPORT MultiChannelOpenDlg : public QDialog {
  Q_OBJECT
public:
  MultiChannelOpenDlg(const ImageInfo& info, QWidget* p = 0, Qt::WindowFlags f = 0);
  ~MultiChannelOpenDlg();

  int channel() const;
  int timepoint() const;
  void setChannel(int value);
  void setTimepoint(int value);

  bool valid() const { return nb_channels * nb_timepoints > 0; }

protected slots:
  void on_channel_valueChanged(int);
  void on_timePoint_valueChanged(int);

private:
  std::unique_ptr<Ui::MultiChannelOpenDlg> ui;
  size_t nb_channels, nb_timepoints;
  std::vector<QPixmap> samples;
};

/**
 * \class StackOpen SystemProcessLoad.hpp <SystemProcessLoad.hpp>
 *
 * Open a stack in either the MGXS or INRIA formats.
 *
 * \ingroup StackProcess
 */
class LGX_EXPORT StackOpen : public StackProcess {
public:
  StackOpen(const StackProcess& proc)
    : Process(proc)
    , StackProcess(proc)
  {
  }

  bool initialize(ParmList& parms, QWidget* parent) override;
  bool operator()(const ParmList& parms) override;
  /**
   * Open the image
   * \param stack Stack that will contain the image
   * \param store Store that will contain the image
   * \param filename File containing the image.
   */
  bool operator()(Stack* stack, Store* store, QString filename, bool autoscale, float scale_range);
  QString folder() const override {
    return "System";
  }
  QString name() const override {
    return "Open";
  }
  QString description() const override {
    return "Open a stack from a known 3D image format";
  }
  QStringList parmNames() const override
  {
    return QStringList() << "Filename"
                         << "Store"
                         << "Stack number"
                         << "Choose File"
                         << "Autoscale";
  }
  QStringList parmDescs() const override
  {
    return QStringList() << "Filename"
                         << "Store"
                         << "Stack number"
                         << "If false and a filename is provided, it won't show the dialog box to choose a file"
                         << "If specified, autoscale the stack to this range. If empty, don't autoscale the stack.";
  }
  ParmList parmDefaults() const override
  {
    return ParmList() << ""
                      << "Main"
                      << 0
                      << true
                      << 0.99;
  }
  ParmChoiceMap parmChoice() const override
  {
    ParmChoiceMap map;
    map[1] = storeChoice();
    map[5] = booleanChoice();
    return map;
  }
  QIcon icon() const override {
    return QIcon(":/images/open.png");
  }
protected:
  bool loadMGXS_1_3(QIODevice& file, Stack* stack, Store* store);
  bool loadMGXS_1_2(QIODevice& file, Stack* stack, Store* store);
  bool loadMGXS_1_1(QIODevice& file, Stack* stack, Store* store);
  bool loadMGXS_1_0(QIODevice& file, Stack* stack, Store* store);
  bool loadMGXS_0(QIODevice& file, Stack* stack, Store* store);
  void centerImage();
};

/**
 * \class MeshLoad SystemProcessLoad.hpp <SystemProcessLoad.hpp>
 *
 * Import a mesh from a file of another format
 *
 * \ingroup MeshProcess
 */
class LGX_EXPORT MeshLoad : public QObject, public MeshProcess {
  Q_OBJECT

public:
  enum TypeId
  {
    TI_PLY = 0, ///< PLY files, extended
    TI_MGXM,    ///< MorphoGraphX Mesh files
    TI_VTU,     ///< VTK unstructured grid file
    TI_TEXT,     ///< Text files
    TI_KEYENCE, ///< KEYENCE JPEG images
    TI_MESH,    ///< MeshEdit mesh files
    TI_OBJ,     ///< Wavefront OBJ files
    TI_ALL      ///< All mesh files
  };

  MeshLoad(const MeshProcess& proc)
    : Process(proc)
    , QObject()
    , MeshProcess(proc)
  {
  }

  bool initialize(ParmList& parms, QWidget* parent) override;
  bool operator()(const ParmList& parms) override;
  /**
   * Import a mesh
   * \param mesh Mesh that will contain the data
   * \param filename File describing the mesh
   * \param type Type of the file. Must be one of 'Text', 'Cells', 'Keyence' or 'MeshEdit', or empty (i.e.
   * auto-detect)
   * \param transform If true, the mesh will be transformed
   * \param add If true, the mesh won't be cleared before loading the file
   *
   * \sa MeshOpen
   */
  bool operator()(Mesh* mesh, QString filename, bool transform, bool add);
  QString folder() const override {
    return "System";
  }
  QString name() const override {
    return "Load";
  }
  QString description() const override {
    return "Load a mesh from one of the known formats.";
  }
  QStringList parmNames() const override
  {
    return QStringList() << "Filename"
                         << "Transform"
                         << "Add"
                         << "Stack number"
                         << "Choose file";
  }
  QStringList parmDescs() const override
  {
    return QStringList() << "Filename"
                         << "Transform"
                         << "Add"
                         << "Stack number"
                         << "If true or filename is empty, shows a dialog box to choose the file to load.";
  }

  ParmList parmDefaults() const override
  {
    return ParmList() << ""
                      << false
                      << false
                      << 0
                      << true;
  }

  ParmChoiceMap parmChoice() const override
  {
    ParmChoiceMap map;
    map[1] = map[2] = map[4] = booleanChoice();
    return map;
  }

  QIcon icon() const override {
    return QIcon(":/images/open.png");
  }

protected slots:
  void selectMeshFile();
  void setMeshFile(const QString& filename);

protected:
  TypeId extToTypeId(QString extension) const;
  QStringList typeIdToExtensions(TypeId t) const;
  QString typeIdToName(TypeId t) const;

  bool loadText(Mesh* mesh, const QString& filename, bool transform, bool add);
  bool loadCells(Mesh* mesh, const QString& filename, bool transform, bool add);
  bool loadKeyence(Mesh* mesh, const QString& filename, bool transform, bool add);
  bool loadMeshEdit(Mesh* mesh, const QString& filename, bool transform, bool add);
  bool loadMeshVTK(Mesh* mesh, const QString& filename, bool transform, bool add);
  bool loadMeshOBJ(Mesh* mesh, const QString& filename, bool transform, bool add);
  bool loadMeshPLY(Mesh* mesh, const QString& filename, bool transform, bool add);
  bool loadMGXM(Mesh* mesh, const QString& filename, bool transform, bool add);

  bool loadMGXM_0(QIODevice& file, Mesh* mesh, bool scale, bool transform, bool add, bool has_color = true);
  bool loadMGXM_1_0(QIODevice& file, Mesh* mesh, bool& scale, bool& transform, bool add, bool has_color = true);
  bool loadMGXM_1_1(QIODevice& file, Mesh* mesh, bool& scale, bool& transform, bool add, bool has_color = true);
  bool loadMGXM_1_2(QIODevice& file, Mesh* mesh, bool& scale, bool& transform, bool add, bool has_color = true);
  bool loadMGXM_1_3(QIODevice& file, Mesh* mesh, bool& scale, bool& transform, bool add);

  void findSignalBounds(Mesh* mesh);

  QDialog* dlg;
  Ui::LoadMeshDialog* ui;
};

/**
 * \class LoadAllData SystemProcessLoad.hpp <SystemProcessLoad.hpp>
 *
 * Load all the files as specified in the various objects (filenames).
 *
 * Note that currently, it will load only the main store of the stacks as
 * only they can have any data.
 *
 * \ingroup GlobalProcess
 */
class LGX_EXPORT LoadAllData : public GlobalProcess {
public:
  LoadAllData(const GlobalProcess& proc)
    : Process(proc)
    , GlobalProcess(proc)
  {
  }

  bool operator()(const ParmList&) override;
  bool operator()();

  QString folder() const override {
    return "System";
  }
  QString name() const override {
    return "Load All";
  }
  QString description() const override
  {
    return "Load the data for all existing objects, using the filename and properties set in them.";
  }
  QStringList parmNames() const override {
    return QStringList();
  }
  QStringList parmDescs() const override {
    return QStringList();
  }
  QIcon icon() const override {
    return QIcon(":/images/open.png");
  }

protected:
  bool loadStore(Stack* stack, Store* store, QStringList& errors);
};

/**
 * \class LoadProjectFile SystemProcessLoad.hpp <SystemProcessLoad.hpp>
 * Load a view file and all the associated files.
 *
 * \ingroup GlobalProcess
 */
class LGX_EXPORT LoadProjectFile : public GlobalProcess {
public:
  LoadProjectFile(const GlobalProcess& proc)
    : Process(proc)
    , GlobalProcess(proc)
  {
  }

  bool operator()(const ParmList& parms) override;
  /**
   * Load the file \c filename
   */
  bool operator()(QString filename);

  bool initialize(ParmList& parms, QWidget* parent) override;

  QString folder() const override {
    return "System";
  }
  QString name() const override {
    return "Load Project";
  }
  QString description() const override
  {
    return "Load a project file and set all the fields and interface. Does not load the data though.";
  }
  QStringList parmNames() const override {
    return QStringList() << "Filename"
                         << "Choose File";
  }
  QStringList parmDescs() const override {
    return QStringList() << "Filename"
                         << "If false and a filename is given, the dialog box to choose a file won't be shown.";
  }
  ParmList parmDefaults() const override {
    return ParmList() << ""
                      << true;
  }
  ParmChoiceMap parmChoice() const override
  {
    ParmChoiceMap map;
    map[1] = booleanChoice();
    return map;
  }
  QIcon icon() const override {
    return QIcon(":/images/open.png");
  }
};

/**
 * \class ResetMeshProcess SystemProcessLoad.hpp <SystemProcessLoad.hpp>
 *
 * Reset a mesh
 *
 * \ingroup MeshProcess
 */
class LGX_EXPORT ResetMeshProcess : public MeshProcess {
public:
  ResetMeshProcess(const MeshProcess& proc)
    : Process(proc)
    , MeshProcess(proc)
  {
  }

  bool operator()(const ParmList& parms) override;
  bool operator()(Mesh* m);

  QString folder() const override {
    return "System";
  }
  QString name() const override {
    return "Reset";
  }
  QString description() const override {
    return "Reset a mesh, -1 for current.";
  }
  QStringList parmNames() const override {
    return QStringList() << "Mesh";
  }
  QStringList parmDescs() const override {
    return QStringList() << "Mesh";
  }
  ParmList parmDefaults() const override {
    return ParmList() << "";
  }
  QIcon icon() const override {
    return QIcon(":/images/ClearStack.png");
  }
};
} // namespace process
} // namespace lgx

#endif
