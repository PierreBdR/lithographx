/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "PluginListView.hpp"
#include "PluginConfiguration.hpp"

#include "Information.hpp"

#include <QDragMoveEvent>

namespace lgx {
namespace gui {

PluginListView::PluginListView(QWidget* parent)
  : QTreeView(parent)
{
  setDragDropMode(QAbstractItemView::InternalMove);
}

void PluginListView::dragMoveEvent(QDragMoveEvent *event)
{
  auto mod = dynamic_cast<PluginListModel*>(model());
  bool accepted = false;
  if(mod and event->dropAction() == Qt::MoveAction) {
    auto idx = indexAt(event->pos());
    if(idx.isValid()) {
      auto plugin = mod->plugin(idx);
      if(plugin->isSystem()) {
        auto r = visualRect(idx);
        auto c = r.center();
        if(event->pos().y() >= c.y()) {
          auto nextIdx = indexBelow(idx);
          if(nextIdx.isValid()) {
            auto nextPlugin = mod->plugin(nextIdx);
            if(not nextPlugin->isSystem()) {
              accepted = true;
            }
          }
        }
      } else
        accepted = true;
    }
  }
  if(accepted)
    QTreeView::dragMoveEvent(event);
  else
    event->ignore();
}

} // namespace gui
} // namespace lgx
