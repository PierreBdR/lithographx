/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef DEPENDENCY_HPP
#define DEPENDENCY_HPP

#include <LGXConfig.hpp>

#include <Version.hpp>

#include <QString>
#include <memory>

namespace lgx {

class PackageManager;

enum class Relationship : signed char {
  Lesser       = -2,
  LesserEqual  = -1,
  Equal        = 0,
  GreaterEqual = 1,
  Greater      = 2,
  Any          = 127
};

/**
 * Class representing a dependency
 */
class Dependency {
public:
  virtual ~Dependency() { }
  virtual bool check(const PackageManager& manager, bool user) const = 0;
  virtual QString toString(bool embedded=false) const = 0;

protected:
  virtual Dependency* copy() const = 0;

  template <typename T>
  friend std::unique_ptr<T> clone(const T& value);
};

struct DependCombined : public Dependency {
  virtual void addDependency(std::unique_ptr<Dependency>&& dep) = 0;
  virtual void addDependency(std::shared_ptr<Dependency> dep) = 0;
  virtual void insertDependency(int pos, std::unique_ptr<Dependency>&& dep) = 0;
  virtual void insertDependency(int pos, std::shared_ptr<Dependency> dep) = 0;
};

class DependAll : public DependCombined {
public:
  DependAll() { }
  ~DependAll() override { }

  void addDependency(std::unique_ptr<Dependency>&& dep) override;
  void addDependency(std::shared_ptr<Dependency> dep) override;
  void insertDependency(int pos, std::unique_ptr<Dependency>&& dep) override;
  void insertDependency(int pos, std::shared_ptr<Dependency> dep) override;
  QString toString(bool embedded=false) const override;

  bool check(const PackageManager& manager, bool user) const override;

protected:
  virtual DependAll* copy() const override;

private:
  std::vector<std::unique_ptr<Dependency>> dependencies;
};

class DependAny : public DependCombined {
public:
  DependAny() { }
  ~DependAny() override { }

  void addDependency(std::unique_ptr<Dependency>&& dep) override;
  void addDependency(std::shared_ptr<Dependency> dep) override;
  void insertDependency(int pos, std::unique_ptr<Dependency>&& dep) override;
  void insertDependency(int pos, std::shared_ptr<Dependency> dep) override;
  QString toString(bool embedded=false) const override;
  bool check(const PackageManager& manager, bool user) const override;

protected:
  virtual DependAny* copy() const override;

private:
  std::vector<std::unique_ptr<Dependency>> dependencies;
};

class DependVersion : public Dependency {
public:
  DependVersion(QString name,
                lgx::util::Version version,
                Relationship rel);
  ~DependVersion() override { }

  bool check(const PackageManager& manager, bool user) const override;
  QString toString(bool embedded=false) const override;

protected:
  virtual DependVersion* copy() const override;

private:
  QString _name;
  util::Version _version;
  Relationship _rel;
};

class InvalidDependencyException : public std::exception
{
public:
  InvalidDependencyException(int pos, const QString& s);
  ~InvalidDependencyException() throw() override;

  const char* what() const throw() override;

private:
  QByteArray msg;
};

class BadTokenDependencyException : public std::exception
{
public:
  BadTokenDependencyException(const QString& tokenType, const QString& tokenText, int pos, const QString& expectedTokenType);
  ~BadTokenDependencyException() throw() override;
  const char* what() const throw() override;
private:
  QByteArray msg;
};

class UnfinishedDependencyException : public std::exception
{
public:
  UnfinishedDependencyException();

  ~UnfinishedDependencyException() throw() override;
  const char* what() const throw() override;
private:
  QByteArray msg;
};

/**
 * Create a dependency from a string.
 *
 * Example of valid dependency:
 *
 *    Package1 (>= 3), Package2 ( == 2.3) | (Package3 ( < 4) , Package4)
 *
 * This means: Package1 must be version >= 3 and either we have Package2
 * version 2.3 or Package3 version strictly less than 4 and Package4.
 *
 * In any descriptions, spaces are fully optional but will separate text.
 *
 * The grammar of a dependency is defined as:
 *
 *     Relation = "<=" | ">=" | "<" | ">" | "=="
 *     Text = [^,|()<>=[:space:]]+
 *     Version = Text (such that util::lgx::Version can parse it)
 *     And = ","
 *     Or = "|"
 *     OpenBracket = "("
 *     CloseBracket = ")"
 *     Item = Text ["(" Relation Version ")"]
 *     Group = "(" (Item | OrGroup | AndGroup | Group) ")"
 *     OrGroup = (Item | Group | OrGroup) Or (Item | Group)
 *     AndGroup = (Item | Group | OrGroup | AndGroup) And (Item | Group | OrGroup)
 */
std::unique_ptr<Dependency> parseDependency(QString s);

template <typename T>
std::unique_ptr<T> clone(const T& value) {
  return std::unique_ptr<T>(value.copy());
}

template <typename T, typename T1>
std::unique_ptr<T> clone(const T1& value) {
  return std::unique_ptr<T>(static_cast<T>(value.copy()));
}

} // namespace lgx

#endif // DEPENDENCY_HPP

