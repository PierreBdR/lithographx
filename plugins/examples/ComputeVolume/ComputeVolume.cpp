#include "ComputeVolume.hpp"
#include "Information.hpp"
#include "Progress.hpp"
#include "Geometry.hpp"

#include <limits>

namespace process
{

// In this process, we will assume m is a triangular mesh made of closed cells.
// If we discover this is not the case, we will fail
bool ComputeVolume::operator()(Mesh *mesh)
{
    vvgraph& S = mesh->graph();

    IntFloatMap volume_labels;

    // Iterate over the vertices of S
    forall(const vertex& v, S)
    {
        // Iterate over the neighbors of the vertex
        forall(const vertex& n, S.neighbors(v))
        {
            const vertex& m = S.nextTo(v,n);
            if(mesh->uniqueTri(v,n,m)) // We want to go only once per triangle in the mesh
            {
                // If the labels are not the same, this means we have more than one cell on the same surface
                if(v->label == n->label and v->label == m->label)
                {
                    volume_labels[v->label] += signedTetraVolume(v->pos, n->pos, m->pos);
                }
                else
                    throw QString("Error, the mesh has more than one cell on a given continuous surface.");
            }
        }
    }

    // Go over all the volumes to find the min and max
    float vol_min = std::numeric_limits<float>::max();
    float vol_max = 0;

    forall(const IntFloatPair& vl, volume_labels)
    {
        if(vl.second < vol_min) vol_min = vl.second;
        if(vl.second > vol_max) vol_max = vl.second;
    }

    // Note: this part will change soon! Keep it separate
    // Now, renormalize the heat map and set the bounds in the mesh

    forall(const IntFloatPair& vl, volume_labels)
    {
        int label = vl.first;
        float volume = vl.second;
        volume_labels[label] = (volume - vol_min) / (vol_max - vol_min);
    }

    // Set the map bounds. A heat of 0 will be interpreted has vol_min, and a heat of 1 as vol_max.
    mesh->heatMapBounds() = Point2f(vol_min, vol_max);
    // Unit of the heat map
    mesh->heatMapUnit() = QString::fromWCharArray(L"\xb5m\xb3"); // unicode for 'mu', 'm' and 'cube'
    // Set the values
    mesh->labelHeat() = volume_labels;
    // Tell the system the triangles have changed (here, the heat map)
    mesh->updateTriangles();

    return true;
}

REGISTER_MESH_PROCESS(ComputeVolume);
}

