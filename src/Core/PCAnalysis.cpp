/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "PCAnalysis.hpp"

#include "CSVStream.hpp"
#include "CuttingSurface.hpp"
#include "Information.hpp"
#include "Misc.hpp"
#include "Progress.hpp"
#include "Shapes.hpp"
#include "EigenDecomposition.hpp"

#include <algorithm>
#include <memory>
#include <unordered_map>
#include <unordered_set>

#include <QFile>
#include <QTextStream>

namespace lgx {
namespace process {

// Note: empty namespace = content is not exported, and it's better (cleaner and more general) than the static keyword
namespace {

struct PCAnalysis_result {
  Point3f p1, p2, p3;
  Point3f ev;
  Point3f mean;

  bool valid() const {
    return normsq(ev) > 0;
  }

  operator bool() const { return valid(); }
};

struct SelectThreshold {
  SelectThreshold(ushort th)
    : threshold(th)
  {
  }

  int operator()(ushort v) const
  {
    if(v > threshold)
      return 0;
    return -1;
  }

  ushort value(int vid) const
  {
    if(vid == 1)
      return threshold;
    return 0;
  }

  int nb_values() const {
    return 1;
  }

  ushort threshold;
};

struct SelectLabel {
  SelectLabel(const std::vector<ushort>& ls)
    : labels(ls)
  {
    for(size_t i = 0; i < labels.size(); ++i)
      inv_labels[labels[i]] = i;
  }

  int operator()(ushort v) const
  {
    std::unordered_map<ushort, int>::const_iterator found = inv_labels.find(v);
    if(found != inv_labels.end())
      return found->second;
    return -1;
  }

  ushort value(int vid) const
  {
    if(vid >= 0 and vid < (int)labels.size())
      return labels[vid];
    return 0;
  }

  int nb_values() const {
    return inv_labels.size();
  }

  std::vector<ushort> labels;
  std::unordered_map<ushort, int> inv_labels;
};

template <typename Fct>
std::vector<PCAnalysis_result> analysePC(const Stack* stk, const HVecUS& data, const Fct& selection,
                                         Point3f correcting_factor)
{
  // First, compute mean
  size_t nb_values = selection.nb_values();
  std::vector<Point3f> mean(nb_values, Point3f(0, 0, 0));
  std::vector<unsigned long long> sum(nb_values, 0);
  std::vector<PCAnalysis_result> result(nb_values);

  // Compute the mean for each selected value
  Information::out << "Compute mean" << endl;
  Point3u s = stk->size();
  size_t k = 0;
  for(size_t z = 0; z < s.z(); ++z)
    for(size_t y = 0; y < s.y(); ++y)
      for(size_t x = 0; x < s.x(); ++x, ++k) {
        int vid = selection(data[k]);
        if(vid >= 0) {
          ++sum[vid];
          mean[vid] += stk->imageToWorld(Point3f(x, y, z));
        }
      }
  for(size_t vid = 0; vid < nb_values; ++vid) {
    if(sum[vid] > 0)
      mean[vid] /= double(sum[vid]);
  }
  Information::out << "Compute CC matrix" << endl;
  // Compute the cross-correlation matrix
  std::vector<Matrix3f> corr(nb_values);
  //for(auto& m: corr)
    //m = Eigen::Matrix3f::Zero();
  k = 0;
  for(size_t z = 0; z < s.z(); ++z)
    for(size_t y = 0; y < s.y(); ++y)
      for(size_t x = 0; x < s.x(); ++x, ++k) {
        int vid = selection(data[k]);
        if(vid >= 0) {
          Point3f dp = stk->imageToWorld(Point3f(x, y, z)) - mean[vid];
          corr[vid](0, 0) += dp[0] * dp[0];
          corr[vid](1, 1) += dp[1] * dp[1];
          corr[vid](2, 2) += dp[2] * dp[2];
          corr[vid](1, 0) += dp[0] * dp[1];
          corr[vid](2, 0) += dp[0] * dp[2];
          corr[vid](2, 1) += dp[1] * dp[2];
        }
      }
  Information::out << "Decompose matrices" << endl;
  // Eigen-decomposition of the matrices
  for(size_t vid = 0; vid < nb_values; ++vid) {
    if(sum[vid] == 0)
      continue;

    corr[vid](0, 1) = corr[vid](1, 0);
    corr[vid](0, 2) = corr[vid](2, 0);
    corr[vid](1, 2) = corr[vid](2, 1);
    corr[vid] /= sum[vid];

    util::EigenVecs3D dec = util::eigenDecomposition(corr[vid], util::EigenSort::Decreasing);
    if(not dec.valid()) {
      Information::err << "Couldn't decompose the matrix of cell " << vid << " into eigen vectors" << endl;
      result.clear();
      return result;
    }

    auto ev = dec.eval;

    Point3f p1(dec.evec[0]);
    Point3f p2(dec.evec[1]);
    Point3f p3(dec.evec[2]);

    if((p1 ^ p2) * p3 < 0)
      p3 = -p3;

    // Correct eigen-vector for shape-factor
    if(correcting_factor.x() > 0)
      ev = multiply(correcting_factor, map(sqrtf, ev));

    // Information::out << "Size of the various dimension = " << ev << endl;

    result[vid].p1 = p1;
    result[vid].p2 = p2;
    result[vid].p3 = p3;
    result[vid].ev = ev;
    result[vid].mean = mean[vid];
    // Information::out << "Value: " << selection.value(vid) << " -- Major axis = " << p1 << " eigenvalues = "
    // << ev << endl;
  }

  if(correcting_factor.x() == 0) {
    Information::out << "Compute regions spans" << endl;
    std::vector<Point3f> min_pos(nb_values, Point3f(FLT_MAX));
    std::vector<Point3f> max_pos(nb_values, Point3f(-FLT_MAX));
    // Find the maximum span of each label along each direction
    k = 0;
    for(size_t z = 0; z < s.z(); ++z)
      for(size_t y = 0; y < s.y(); ++y)
        for(size_t x = 0; x < s.x(); ++x, ++k) {
          int vid = selection(data[k]);
          if(vid >= 0) {
            Point3f dp = stk->imageToWorld(Point3f(x, y, z));
            const PCAnalysis_result& res = result[vid];
            double p1 = dp * res.p1;
            double p2 = dp * res.p2;
            double p3 = dp * res.p3;
            Point3f& pmin = min_pos[vid];
            Point3f& pmax = max_pos[vid];
            if(p1 < pmin.x())
              pmin.x() = p1;
            if(p1 > pmax.x())
              pmax.x() = p1;
            if(p2 < pmin.y())
              pmin.y() = p2;
            if(p2 > pmax.y())
              pmax.y() = p2;
            if(p3 < pmin.z())
              pmin.z() = p3;
            if(p3 > pmax.z())
              pmax.z() = p3;
          }
        }
    for(size_t vid = 0; vid < nb_values; ++vid)
      if(sum[vid] > 0) {
        PCAnalysis_result& res = result[vid];
        Point3f pmin = min_pos[vid];
        Point3f pmax = max_pos[vid];
        res.ev = (pmax - pmin) / 2;
        res.mean = (pmax + pmin) / 2;
        res.mean = res.mean.x() * res.p1 + res.mean.y() * res.p2 + res.mean.z() * res.p3;
      }
  } else if(correcting_factor.x() < 0) {
    float percentile = -correcting_factor.x();
    Information::out << QString("Computing the %1% of voxels").arg(percentile * 100) << endl;
    std::vector<std::vector<double> > pos_p1(nb_values);
    std::vector<std::vector<double> > pos_p2(nb_values);
    std::vector<std::vector<double> > pos_p3(nb_values);
    k = 0;
    for(size_t z = 0; z < s.z(); ++z)
      for(size_t y = 0; y < s.y(); ++y)
        for(size_t x = 0; x < s.x(); ++x, ++k) {
          int vid = selection(data[k]);
          if(vid >= 0) {
            Point3f dp = stk->imageToWorld(Point3f(x, y, z));
            const PCAnalysis_result& res = result[vid];
            double p1 = dp * res.p1;
            double p2 = dp * res.p2;
            double p3 = dp * res.p3;
            pos_p1[vid].push_back(p1);
            pos_p2[vid].push_back(p2);
            pos_p3[vid].push_back(p3);
          }
        }
    for(size_t vid = 0; vid < nb_values; ++vid)
      if(sum[vid] > 0) {
        std::vector<double>& ps1 = pos_p1[vid];
        std::vector<double>& ps2 = pos_p2[vid];
        std::vector<double>& ps3 = pos_p3[vid];

        std::sort(ps1.begin(), ps1.end());
        std::sort(ps2.begin(), ps2.end());
        std::sort(ps3.begin(), ps3.end());

        int lower = (1 - percentile) / 2 * (ps1.size() - 1);
        int upper = ps1.size() - 1 - lower;
        // Information::out << "Positions for vid = " << vid << "(" << ps1.size() << ") = [" << lower << ";"
        // << upper << "]\n";
        PCAnalysis_result& res = result[vid];
        Point3f pmin(ps1[lower], ps2[lower], ps3[lower]);
        Point3f pmax(ps1[upper], ps2[upper], ps3[upper]);
        // Information::out << " -> " << pmin << " -- " << pmax << endl;
        res.ev = (pmax - pmin) / 2;
        res.mean = (pmax + pmin) / 2;
        res.mean = res.mean.x() * res.p1 + res.mean.y() * res.p2 + res.mean.z() * res.p3;
      }
  }
  return result;
}

/*
 *void addPCACuboidShape(int lab, const PCAnalysis_result& res, Mesh* mesh, int slices)
 *{
 *  if(slices < 1)
 *    return;
 *  // Information::out << "Draw PCA for label " << lab << endl;
 *  vvgraph& S = mesh->graph();
 *  std::vector<vertex> vs(8, vertex(0));
 *  Point3f center = res.mean;
 *  for(int i = 0; i < 8; ++i) {
 *    vertex v;
 *    v->label = lab;
 *    vs[i] = v;
 *  }
 *  Point3f p1 = res.ev[0] * res.p1;
 *  Point3f p2 = res.ev[1] * res.p2;
 *  Point3f p3 = res.ev[2] * res.p3;
 *
 *  vs[0]->pos = center - p1 - p2 - p3;
 *  vs[1]->pos = center + p1 - p2 - p3;
 *  vs[2]->pos = center - p1 + p2 - p3;
 *  vs[3]->pos = center + p1 + p2 - p3;
 *  vs[4]->pos = center - p1 - p2 + p3;
 *  vs[5]->pos = center + p1 - p2 + p3;
 *  vs[6]->pos = center - p1 + p2 + p3;
 *  vs[7]->pos = center + p1 + p2 + p3;
 *
 *  std::vector<Point3i> triangles(12);
 *  triangles[0] = Point3i(0, 1, 4);   // 1
 *  triangles[1] = Point3i(1, 5, 4);
 *
 *  triangles[2] = Point3i(1, 3, 5);   // 2
 *  triangles[3] = Point3i(3, 7, 5);
 *
 *  triangles[4] = Point3i(3, 2, 7);   // 3
 *  triangles[5] = Point3i(2, 6, 7);
 *
 *  triangles[6] = Point3i(2, 0, 6);   // 4
 *  triangles[7] = Point3i(0, 4, 6);
 *
 *  triangles[8] = Point3i(2, 3, 0);   // 5
 *  triangles[9] = Point3i(3, 1, 0);
 *
 *  triangles[10] = Point3i(4, 5, 6);   // 6
 *  triangles[11] = Point3i(5, 7, 6);
 *
 *  // Information::out << "Calling meshFromTriangles" << endl;
 *  shape::meshFromTriangles(S, vs, triangles);
 *}
 *
 *void addPCACylinderShape(int lab, const PCAnalysis_result& res, Mesh* mesh, int slices)
 *{
 *  if(slices < 1)
 *    return;
 *  vvgraph& S = mesh->graph();
 *  std::vector<vertex> vs(5 * slices + 2, vertex(0));
 *  Point3f p1 = res.ev[0] * res.p1;
 *  Point3f p2 = res.ev[1] * res.p2;
 *  Point3f p3 = res.ev[2] * res.p3;
 *  Point3f center = res.mean;
 *
 *  vertex c1, c2;
 *  c1->label = c2->label = lab;
 *  c1->pos = center - p1;
 *  c2->pos = center + p1;
 *  int nc1 = 5 * slices;
 *  int nc2 = 5 * slices + 1;
 *  vs[nc1] = c1;
 *  vs[nc2] = c2;
 *
 *  float alpha = 2 * M_PI / slices;
 *  for(int i = 0; i < slices; ++i) {
 *    vertex v1, v2, v3, v4, v5;
 *    v1->label = v2->label = v3->label = v4->label = v5->label = lab;
 *
 *    Point3f v1p = Point3f(-1, cos(i * alpha), sin(i * alpha));
 *    v3->pos = v1->pos = center + v1p.x() * p1 + v1p.y() * p2 + v1p.z() * p3;
 *    Point3f v2p = Point3f(1, cos(i * alpha), sin(i * alpha));
 *    v4->pos = v2->pos = center + v2p.x() * p1 + v2p.y() * p2 + v2p.z() * p3;
 *
 *    v5->pos = center + v2p.y() * p2 + v2p.z() * p3;
 *
 *    vs[i] = v1;
 *    vs[slices + i] = v2;
 *    vs[2 * slices + i] = v3;
 *    vs[3 * slices + i] = v4;
 *    vs[4 * slices + i] = v5;
 *  }
 *
 *  std::vector<Point3i> triangles(6 * slices);
 *  int i = slices - 1;
 *  for(int i1 = 0; i1 < slices; ++i1) {
 *    int j = slices + i;
 *    int j1 = slices + i1;
 *    int k = 2 * slices + i;
 *    int k1 = 2 * slices + i1;
 *    triangles[i] = Point3i(nc1, i1, i);
 *    triangles[i + slices] = Point3i(nc2, j, j1);
 *    triangles[i + 2 * slices] = 2 * slices + Point3i(i, i1, k);
 *    triangles[i + 3 * slices] = 2 * slices + Point3i(i1, k1, k);
 *    triangles[i + 4 * slices] = 2 * slices + Point3i(k, k1, j);
 *    triangles[i + 5 * slices] = 2 * slices + Point3i(k1, j1, j);
 *    i = i1;
 *  }
 *
 *  shape::meshFromTriangles(S, vs, triangles);
 *}
 *
 *namespace {
 *struct pair_hash {
 *  int operator()(const std::pair<int, int>& p) const {
 *    return p.first ^ p.second;
 *  }
 *};
 *}
 *
 *void addPCAEllipsoidShape(int lab, const PCAnalysis_result& res, Mesh* mesh, int subdiv)
 *{
 *  static int generated_subdiv = 0;
 *  static std::vector<Point3i> triangles;
 *  static std::vector<Point3f> positions;
 *  if(generated_subdiv != subdiv) {
 *    generated_subdiv = subdiv;
 *    if(subdiv < 0) {
 *      triangles.clear();
 *      positions.clear();
 *      return;
 *    } else {
 *      positions.resize(12);
 *      triangles.resize(20);
 *      const double t = (1 + sqrt(5.)) / 2;
 *      const double s = sqrt(1 + t * t);
 *      positions[0] = Point3f(t, 1, 0) / s;
 *      positions[1] = Point3f(-t, 1, 0) / s;
 *      positions[2] = Point3f(t, -1, 0) / s;
 *      positions[3] = Point3f(-t, -1, 0) / s;
 *      positions[4] = Point3f(1, 0, t) / s;
 *      positions[5] = Point3f(1, 0, -t) / s;
 *      positions[6] = Point3f(-1, 0, t) / s;
 *      positions[7] = Point3f(-1, 0, -t) / s;
 *      positions[8] = Point3f(0, t, 1) / s;
 *      positions[9] = Point3f(0, -t, 1) / s;
 *      positions[10] = Point3f(0, t, -1) / s;
 *      positions[11] = Point3f(0, -t, -1) / s;
 *
 *      triangles[0] = Point3i(0, 8, 4);
 *      triangles[1] = Point3i(1, 10, 7);
 *      triangles[2] = Point3i(2, 9, 11);
 *      triangles[3] = Point3i(7, 3, 1);
 *      triangles[4] = Point3i(0, 5, 10);
 *      triangles[5] = Point3i(3, 9, 6);
 *      triangles[6] = Point3i(3, 11, 9);
 *      triangles[7] = Point3i(8, 6, 4);
 *      triangles[8] = Point3i(2, 4, 9);
 *      triangles[9] = Point3i(3, 7, 11);
 *      triangles[10] = Point3i(4, 2, 0);
 *      triangles[11] = Point3i(9, 4, 6);
 *      triangles[12] = Point3i(2, 11, 5);
 *      triangles[13] = Point3i(0, 10, 8);
 *      triangles[14] = Point3i(5, 0, 2);
 *      triangles[15] = Point3i(10, 5, 7);
 *      triangles[16] = Point3i(1, 6, 8);
 *      triangles[17] = Point3i(1, 8, 10);
 *      triangles[18] = Point3i(6, 1, 3);
 *      triangles[19] = Point3i(11, 7, 5);
 *
 *#define MAKE_EDGE(i1, i2) (i1 < i2 ? std::make_pair(i1, i2) : std::make_pair(i2, i1))
 *
 *      // Subdivide triangles
 *      for(size_t i = 0; i < (size_t)subdiv; ++i) {
 *        typedef std::unordered_map<std::pair<int, int>, int, pair_hash> edge_map_t;
 *        typedef edge_map_t::iterator edge_iterator;
 *        edge_map_t edges;
 *        std::vector<Point3i> new_triangles(4 * triangles.size());
 *        for(size_t j = 0; j < triangles.size(); ++j) {
 *          const Point3i tr = triangles[j];
 *          int p1 = tr[0], p2 = tr[1], p3 = tr[2];
 *          std::pair<int, int> e1 = MAKE_EDGE(p1, p2);
 *          std::pair<int, int> e2 = MAKE_EDGE(p2, p3);
 *          std::pair<int, int> e3 = MAKE_EDGE(p1, p3);
 *          int n1, n2, n3;
 *          edge_iterator found = edges.find(e1);
 *          if(found == edges.end()) {
 *            n1 = positions.size();
 *            positions.push_back(normalized(positions[p1] + positions[p2]));
 *            edges[e1] = n1;
 *          } else
 *            n1 = found->second;
 *          found = edges.find(e2);
 *          if(found == edges.end()) {
 *            n2 = positions.size();
 *            positions.push_back(normalized(positions[p2] + positions[p3]));
 *            edges[e2] = n2;
 *          } else
 *            n2 = found->second;
 *          found = edges.find(e3);
 *          if(found == edges.end()) {
 *            n3 = positions.size();
 *            positions.push_back(normalized(positions[p3] + positions[p1]));
 *            edges[e3] = n3;
 *          } else
 *            n3 = found->second;
 *          new_triangles[4 * j] = Point3i(p1, n1, n3);
 *          new_triangles[4 * j + 1] = Point3i(p2, n2, n1);
 *          new_triangles[4 * j + 2] = Point3i(p3, n3, n2);
 *          new_triangles[4 * j + 3] = Point3i(n1, n2, n3);
 *        }
 *        swap(triangles, new_triangles);
 *      }
 *
 *      Information::out << "# triangles = " << triangles.size() << endl;
 *      Information::out << "# points = " << positions.size() << endl;
 *
 *#undef MAKE_EDGE
 *    }
 *  }
 *  if(subdiv < 0)
 *    return;
 *
 *  vvgraph& S = mesh->graph();
 *  std::vector<vertex> vs(positions.size(), vertex(0));
 *  Point3f p1 = res.ev[0] * res.p1;
 *  Point3f p2 = res.ev[1] * res.p2;
 *  Point3f p3 = res.ev[2] * res.p3;
 *  Point3f center = res.mean;
 *
 *  for(size_t i = 0; i < vs.size(); ++i) {
 *    Point3f p = positions[i];
 *    vertex v;
 *    v->pos = center + p.x() * p1 + p.y() * p2 + p.z() * p3;
 *    v->label = lab;
 *    vs[i] = v;
 *  }
 *
 *  shape::meshFromTriangles(S, vs, triangles);
 *}
 */
} // namespace

bool PCAnalysis::operator()(Stack* stk, Store* store, QString filename, int threshold, QString shape,
                            Point3f correcting_factor, int slices, bool& draw_result)
{
  Information::out << "Start PCAnalysis" << endl;

  const HVecUS& data = store->data();
  // Move the stack w.r.t. the PCs

  std::vector<PCAnalysis_result> result;
  std::vector<ushort> labels;
  if(store->labels()) {
    std::unordered_set<ushort> labs;
    forall(const ushort& l, data) {
      if(l > 0)
        labs.insert(l);
    }
    labels = std::vector<ushort>(labs.begin(), labs.end());

    result = analysePC(stk, data, SelectLabel(labels), correcting_factor);
  } else
    result = analysePC(stk, data, SelectThreshold(threshold), correcting_factor);

  if(result.empty())
    return setErrorMessage("Error computing principal components. Check terminal for more details.");

  if(shape == "Cuboids" or shape == "Cylinder" or shape == "Ellipsoids") {
    Information::out << "Drawing " << shape << endl;
    draw_result = true;
    Mesh* mesh = this->mesh(stk->id());
    mesh->reset();
    std::unique_ptr<shape::AbstractShape> shape_obj;
    if(shape == "Cuboids")
      shape_obj.reset(new shape::Cube);
    else if(shape == "Cylinder")
      shape_obj.reset(new shape::Cylinder(slices));
    else if(shape == "Ellipsoids")
      shape_obj.reset(new shape::RegularSphere(slices));
    for(size_t vid = 0; vid < labels.size(); ++vid) {
      if(result[vid]) {
        auto res = result[vid];
        Point3f p1 = res.ev[0] * res.p1;
        Point3f p2 = res.ev[1] * res.p2;
        Point3f p3 = res.ev[2] * res.p3;
        shape_obj->addTo(mesh, res.mean, p1, p2, p3, labels[vid]);
      }
    }
    // Clear memory used by addPCAEllipsoidShape
    mesh->setNormals();
    mesh->updateAll();
  } else {
    if(result.size() == 1) {
      stk->trans().setPositionAndOrientation(qglviewer::Vec(0, 0, 0), qglviewer::Quaternion(0, 0, 0, 1));
      stk->setShowTrans(true);

      CuttingSurface* cf = cuttingSurface();
      qglviewer::Frame& frame = cf->frame();
      // Information::out << "Mean = " << mean << endl;

      PCAnalysis_result res = result[0];
      if(!res) {
        setErrorMessage("Error, not result found, do you have any point?");
        return false;
      }
      double mm[16] = { res.p1.x(), res.p2.x(), res.p3.x(), 0, res.p1.y(),   res.p2.y(),   res.p3.y(),   0,
                        res.p1.z(), res.p2.z(), res.p3.z(), 0, res.mean.x(), res.mean.y(), res.mean.z(), 1 };
      Matrix4d mm_gl(mm);
      Information::out << "Transform matrix = " << mm_gl << endl;
      frame.setFromMatrix(mm_gl.c_data());

      // Now, set the planes
      cf->setMode(CuttingSurface::THREE_AXIS);
      cf->setSize(Point3f(res.ev));
      cf->showGrid();
    }

    draw_result = false;
  }

  if(!filename.isEmpty()) {
    QFile f(filename);
    if(!f.open(QIODevice::WriteOnly)) {
      setErrorMessage(QString("Error, cannot open file '%1' for writing.").arg(filename));
      return false;
    }
    util::CSVStream ts(&f);
    ts << "Image file" << store->file() << eol;
    if(store->labels()) {
      ts << "Label"
         << QString("pos.X (%1)").arg(UM)
         << QString("pos.Y (%1)").arg(UM)
         << QString("pos.Z (%1)").arg(UM)
         << "e1.X" << "e1.Y" << "e1.Z"
         << "e2.X" << "e2.Y" << "e2.Z"
         << QString("r1 (%1)").arg(UM)
         << QString("r2 (%1)").arg(UM)
         << QString("r3 (%1)").arg(UM)
         << eol;

      for(size_t vid = 0; vid < labels.size(); ++vid) {
        const PCAnalysis_result& res = result[vid];
        ts << labels[vid] << res.mean.x() << res.mean.y() << res.mean.z()
           << res.p1.x() << res.p1.y() << res.p1.z() << res.p2.x() << res.p2.y()
           << res.p2.z() << res.ev[0] << res.ev[1] << res.ev[2] << eol;
      }
    } else {
      const PCAnalysis_result& res = result[0];
      ts << "Threshold" << threshold << eol;
      ts << "Radii";
      for(int i = 0; i < 3; ++i)
        ts << res.ev[i];
      ts << eol;
      ts << "Center";
      for(int i = 0; i < 3; ++i)
        ts << res.mean[i];
      ts << "Eigenvectors" << eol;
      ts << QString("X (%1)").arg(UM)
         << QString("Y (%1)").arg(UM)
         << QString("Z (%1)").arg(UM) << eol;
      ts << res.p1.x() << res.p1.y() << res.p1.z() << eol;
      ts << res.p2.x() << res.p2.y() << res.p2.z() << eol;
      ts << res.p3.x() << res.p3.y() << res.p3.z() << eol;
    }
    f.close();
    Information::out << "Written result in file " << filename << endl;
  }

  return true;
}

REGISTER_GLOBAL_PROCESS(PCAnalysis);
} // namespace process
} // namespace lgx
