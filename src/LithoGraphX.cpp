/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/
#include "cuda/CudaExport.hpp"
#include "LGXVersion.hpp"

#ifdef WIN32
// ensure nvidia and AMD recognise this is a 3D program
extern "C" {
__declspec(dllexport) DWORD NvOptimusEnablement = 0x00000001;
__declspec(dllexport) int AmdPowerXpressRequestHighPerformance = 1;
}

#  define FILE_PREFIX "file:///"
#else
#  include <dlfcn.h>
#  include <sched.h>
#  define FILE_PREFIX "file://"
#endif

#include "SystemProcess.hpp"
#include <Progress.hpp>

#include "Process.hpp"
#include "PrivateProcess.hpp"

#include "About.hpp"
#include "AutoUpdate.hpp"
#include "ColorEditorDlg.hpp"
#include "Colors.hpp"
#include "DebugDlg.hpp"
#include "Dir.hpp"
#include "Forall.hpp"
#include "Geometry.hpp"
#include "ImageData.hpp"
#include "Information.hpp"
#include "LabelEditorDlg.hpp"
#include "LibrariesDlg.hpp"
#include "LithoGraphX.hpp"
#include "PathEditorDlg.hpp"
#include "PluginManager.hpp"
#include "ProcessDocDialog.hpp"
#include "ProcessParms.hpp"
#include "ProcessThread.hpp"
#include "ProcessUtils.hpp"
#include "SettingsDlg.hpp"
#include "SystemDirs.hpp"
#include "PackageManager.hpp"
#include "PluginConfiguration.hpp"
#include "PackageConfigurationDlg.hpp"

#include "ui_EditParms.h"
#include "ui_GUI.h"

#include <QDateTime>
#include <QDesktopServices>
#include <QDragEnterEvent>
#include <QDropEvent>
#include <QFileDialog>
#include <QFileInfo>
#include <QFileSystemWatcher>
#include <QInputDialog>
#include <QList>
#include <QMessageBox>
#include <QMimeData>
#include <QMutexLocker>
#include <QPointer>
#include <QPushButton>
#include <QSettings>
#include <QTimer>
#include <QToolButton>
#include <QUrl>

#include <QKeyEvent>

#include <memory>
#include <typeinfo>

#ifdef _OPENMP
#  include <omp.h>
#endif

using namespace lgx;
using process::MeshProcess;
using process::StackProcess;
using process::GlobalProcess;

namespace {
QString modifierString(Qt::KeyboardModifiers mod)
{
  QStringList lst;
  if(mod & Qt::ShiftModifier)
    lst << "Shift";
  if(mod & Qt::ControlModifier)
    lst << "Control";
  if(mod & Qt::AltModifier)
    lst << "Alt";
  if(mod & Qt::MetaModifier)
    lst << "Meta";
  if(mod & Qt::KeypadModifier)
    lst << "Keypad";
  if(mod & Qt::GroupSwitchModifier)
    lst << "GroupSwitch";
  return lst.join(",");
}
} // namespace

enum TasksTreeRole { TaskNameRole = Qt::UserRole, ProcessPositionRole = Qt::UserRole + 1 };

QString LithoGraphX::AppDir = QString();
QLabel* LithoGraphX::ActiveStack;

LithoGraphX::~LithoGraphX() {
}

LithoGraphX::LithoGraphX(QString appdir)
  : QMainWindow()
  , _needSaving(false)
  , _processThread(0)
  , _stack1NeedsUpdate(false)
  , _stack2NeedsUpdate(false)
  , _editPathsDlg(0)
  , _labelEditorDlg(0)
{
  // StartSession();
  Information::out << introText() << endl;

  SettingsDlg::loadDefaults(this);

  AppDir = appdir;

  // First, create all children object

  _stack1 = new ImgData(0, this);
  _stack1->setObjectName("Stack1");
  _stack2 = new ImgData(1, this);
  _stack2->setObjectName("Stack2");

  // Stack update timer
  _stackUpdateTimer = new QTimer(this);
  _stackUpdateTimer->setObjectName("stackUpdateTimer");
  _stackUpdateTimer->setInterval(50);
  _stackUpdateTimer->setSingleShot(true);
  _stackUpdateTimer->setTimerType(Qt::CoarseTimer);

  // EditParms dialog
  _editParmsDialog = new QDialog(this);
  _editParmsDialog->setObjectName("editParmsDialog");
  ui_editParmsDialog.reset(new Ui_EditParmsDialog);
  ui_editParmsDialog->setupUi(_editParmsDialog);
  QPushButton* saveAs = ui_editParmsDialog->OKCancel->addButton("Save As", QDialogButtonBox::ActionRole);
  saveAs->setText("Save As");
  connect(saveAs, &QAbstractButton::clicked,
          this, &LithoGraphX::editParmsSaveAs);

  // Process parameter models
  _parmsModel = new ProcessParmsModel(this);
  _parmsModel->setObjectName("parmsModel");

  _resetParametersAct = new QAction("Reset to default", this);
  _resetParametersAct->setObjectName("resetParametersAct");

  _editTasksAct = new QAction("Edit tasks", this);
  _editTasksAct->setObjectName("editTasksAct");

  // Create all objects pre-defined in the UI file
  ui.setupUi(this);

  // setup shortcuts
  auto actions = this->findChildren<QAction*>();
  for(auto act: actions) {
    auto shrt = act->shortcut();
    if(not shrt.isEmpty()) {
      ui.Viewer->setKeyDescription(shrt[0], act->toolTip());
      act->setToolTip(QString("%1 (%2)").arg(act->toolTip()).arg(shrt.toString()));
    }
  }

  createSetupProcess();

  _stack1->init(_currentSetup->stack(0), _currentSetup->mesh(0));
  _stack2->init(_currentSetup->stack(1), _currentSetup->mesh(1));

  // Define the default LabelColors
  lgx::resetLabelColors(ImgData::LabelColors);

  // setup radio buttons
  QButtonGroup* signal1 = new QButtonGroup(this);
  signal1->addButton(ui.Stack1SurfSignal);
  signal1->addButton(ui.Stack1SurfTexture);
  signal1->addButton(ui.Stack1SurfImage);

  QButtonGroup* signal2 = new QButtonGroup(this);
  signal2->addButton(ui.Stack2SurfSignal);
  signal2->addButton(ui.Stack2SurfTexture);
  signal2->addButton(ui.Stack2SurfImage);

  Information::setMainWindow(this);

  // Set Progress dialog parent
  lgx::GlobalProgress::instance().setParent(this);

  // Connect control In slots
  connectControlsIn();

  // Set process parameter models
  ui.ProcessParameters->setModel(_parmsModel);
  ui.ProcessParameters->setItemDelegateForColumn(1, new FreeFloatDelegate(this));
  ui.ProcessParameters->horizontalHeader()->setSectionResizeMode(1, QHeaderView::Stretch);
  ui.ProcessParameters->resizeColumnToContents(0);

  ui.ProcessParameters->addAction(_resetParametersAct);

  ui.ProcessTasksCommand->addAction(_editTasksAct);

#ifndef LithoGraphX_CHECK_UPDATES
  ui.actionCheckUpdatesLGX->setVisible(false);
#else
  checkUpdateAtStart();
#endif

#ifdef THRUST_BACKEND_CUDA
  Information::out << "Starting CUDA" << endl;
  // Initialize cuda
  initGPU();
  Information::out << "CUDA started" << endl;
#endif

#ifdef _OPENMP
#  ifdef __linux__
  // Set affinity
  cpu_set_t my_set;
  CPU_ZERO(&my_set);
  int numCPUs = sysconf(_SC_NPROCESSORS_ONLN);   // Linux only?
  for(int i = 0; i < numCPUs; i++)
    CPU_SET(i, &my_set);     /* set the bit that represents core i. */
  sched_setaffinity(0, sizeof(cpu_set_t), &my_set);
  // Ask OpenMP to use all processors
  omp_set_num_threads(numCPUs);
#  endif

  omp_set_dynamic(0);

  Information::out << "OpenMP Processors: " << omp_get_num_procs() << endl;
  Information::out << "OpenMP Max threads: " << omp_get_max_threads() << endl << endl;
#endif

  Information::out << "loadProcesses" << endl;

  process::PluginManager::allocate();

  process::pluginManager()->initialize();

  scanProcessLists();
  saveProcessesParameters();
  Information::out << "Done processes / macros" << endl;

  // TODO: remove that
  lgx::PackageManager manager;

  Information::out << "Package manager loaded" << endl;
  Information::out << "packages:\n";
  for(const auto& pkg_name : manager.packages()) {
    auto pkg = manager.package(pkg_name);
    Information::out << " - " << pkg->name() << "(v. " << pkg->version().toString() << ") / "
         << pkg->dependencies()->toString() << endl;
  }

  // Restore geometry and settings
  {
    QSettings settings;
    settings.beginGroup("MainWindow");
    restoreGeometry(settings.value("Geometry").toByteArray());
    restoreState(settings.value("WindowState").toByteArray(), 1);
    settings.endGroup();

    // Load shared tasks
    TaskEditDlg::tasks_t newTasks;
    TaskEditDlg::loadTasksFromSettings(newTasks);
    updateCurrentTasks(newTasks);
    reloadTasks();
  }

  // Set up ActiveStack widget;
  ActiveStack = new QLabel;
  ui.statusBar->addPermanentWidget(ActiveStack);
  ActiveStack->setText("Main Stack 1 Active");

  Information::out << "loadControls" << endl;

  // Load data into controls (before connecting)
  loadControls();

  // Connect control In slots
  connectControlsOut();

  Information::out << "Control connected" << endl;

  _stack1->initControls(ui.Viewer);
  _stack2->initControls(ui.Viewer);

  // Start with clipping plane as manipulated frame
  ui.Viewer->setManipulatedFrame(&ui.Viewer->_c1->frame());

  // Connect plugin manager to scanProcessLists
  connect(process::pluginManager(), &process::PluginManager::processesChanged,
          this, &LithoGraphX::scanProcessLists);
}

void LithoGraphX::createSetupProcess()
{
  _currentSetup.reset(new process::SetupProcess(this));
  Stack* s1 = _currentSetup->addStack();
  s1->main()->setTransferFct(TransferFunction::scale_green());
  s1->work()->setTransferFct(TransferFunction::scale_cyan());
  Stack* s2 = _currentSetup->addStack();
  s2->main()->setTransferFct(TransferFunction::scale_red());
  s2->work()->setTransferFct(TransferFunction::scale_yellow());
  Mesh* m1 = _currentSetup->addMesh(s1);
  m1->setSurfFct(TransferFunction::scale_gray());
  m1->setHeatFct(TransferFunction::viridis());
  Mesh* m2 = _currentSetup->addMesh(s2);
  m2->setSurfFct(TransferFunction::scale_gray());
  m2->setHeatFct(TransferFunction::viridis());

  ui.Viewer->_clip1.setClip(_currentSetup->clip1());
  ui.Viewer->_clip2.setClip(_currentSetup->clip2());
  ui.Viewer->_clip3.setClip(_currentSetup->clip3());
  ui.Viewer->_c1 = _currentSetup->clip1();
  ui.Viewer->_c2 = _currentSetup->clip2();
  ui.Viewer->_c3 = _currentSetup->clip3();
  _cutSurf.cut = _currentSetup->cuttingSurface();
}

void LithoGraphX::setDebug(bool debug)
{
  if(!debug)
    ui.actionDebug_Dialog->setVisible(false);
  ui.menuFileEditPaths->setVisible(false);
}

void LithoGraphX::loadControls()
{
  // Load clipplane info into controls
  ui.Clip1Enable->setChecked(ui.Viewer->_c1->enabled());
  ui.Clip1Grid->setChecked(ui.Viewer->_c1->grid());
  ui.Clip1Width->setSliderPosition(int(2000 * log(ui.Viewer->_c1->width() * 2.0 / ui.Viewer->getSceneRadius())));
  ui.Clip2Enable->setChecked(ui.Viewer->_c2->enabled());
  ui.Clip2Grid->setChecked(ui.Viewer->_c2->grid());
  ui.Clip2Width->setSliderPosition(int(2000 * log(ui.Viewer->_c2->width() * 2.0 / ui.Viewer->getSceneRadius())));
  ui.Clip3Enable->setChecked(ui.Viewer->_c3->enabled());
  ui.Clip3Grid->setChecked(ui.Viewer->_c3->grid());
  ui.Clip3Width->setSliderPosition(int(2000 * log(ui.Viewer->_c3->width() * 2.0 / ui.Viewer->getSceneRadius())));

  ui.Clip1Width->setDefaultValue(0);
  ui.Clip2Width->setDefaultValue(0);
  ui.Clip3Width->setDefaultValue(0);

  // Load controls for Stack 1
  ui.Stack1MainShow->setChecked(_stack1->stack->main()->isVisible());
  ui.Stack1MainBright->setValue(int(_stack1->stack->main()->brightness() * 10000.0));
  ui.Stack1MainOpacity->setValue(int(_stack1->stack->main()->opacity() * 10000.0));
  ui.Stack1MainLabels->setChecked(_stack1->stack->main()->labels());
  ui.Stack1Main16Bit->setChecked(_stack1->Main16Bit);

  ui.Stack1WorkShow->setChecked(_stack1->stack->work()->isVisible());
  ui.Stack1WorkBright->setValue(int(_stack1->stack->work()->brightness() * 10000.0));
  ui.Stack1WorkOpacity->setValue(int(_stack1->stack->work()->opacity() * 10000.0));
  ui.Stack1WorkLabels->setChecked(_stack1->stack->work()->labels());
  ui.Stack1Work16Bit->setChecked(_stack1->Work16Bit);

  ui.Stack1SurfShow->setChecked(_stack1->mesh->isSurfaceVisible());
  ui.Stack1SurfBright->setValue(int(_stack1->mesh->brightness() * 10000.0));
  ui.Stack1SurfOpacity->setValue(int(_stack1->mesh->opacity() * 10000.0));
  ui.Stack1SurfBlend->setChecked(_stack1->mesh->blending());
  ui.Stack1SurfCull->setChecked(_stack1->mesh->culling());

  ui.Stack1SurfView->setCurrentIndex((int)_stack1->mesh->toShow());

  switch(_stack1->mesh->coloring()) {
  case SurfaceColor::Signal:
    ui.Stack1SurfSignal->setChecked(true);
    break;
  case SurfaceColor::Texture:
    ui.Stack1SurfTexture->setChecked(true);
    break;
  case SurfaceColor::Image:
    ui.Stack1SurfImage->setChecked(true);
    break;
  }

  ui.Stack1MeshShow->setChecked(_stack1->mesh->isMeshVisible());
  ui.Stack1MeshView->setCurrentIndex((int)_stack1->mesh->meshView());
  ui.Stack1MeshLines->setChecked(_stack1->mesh->showMeshLines());
  ui.Stack1MeshPoints->setChecked(_stack1->mesh->showMeshPoints());

  ui.Stack1CellMap->setChecked(_stack1->mesh->showMeshCellMap());
  ui.Stack1ShowTrans->setChecked(_stack1->stack->showTrans());
  ui.Stack1ShowBBox->setChecked(_stack1->stack->showBBox());
  ui.Stack1ShowScale->setChecked(_stack1->stack->showScale());
  ui.Stack1TieScales->setChecked(_stack1->stack->tieScales());
  ui.Stack1Scale_X->setValue(_stack1->stack->scale().x());
  ui.Stack1Scale_Y->setValue(_stack1->stack->scale().y());
  ui.Stack1Scale_Z->setValue(_stack1->stack->scale().z());

  ui.Stack1Scale_X->setDefaultValue(0);
  ui.Stack1Scale_Y->setDefaultValue(0);
  ui.Stack1Scale_Z->setDefaultValue(0);

  // Load controls for Stack 2
  ui.Stack2MainShow->setChecked(_stack2->stack->main()->isVisible());
  ui.Stack2MainBright->setValue(int(_stack2->stack->main()->brightness() * 10000.0f));
  ui.Stack2MainOpacity->setValue(int(_stack2->stack->main()->opacity() * 10000.0f));
  ui.Stack2MainLabels->setChecked(_stack2->stack->main()->labels());
  ui.Stack2Main16Bit->setChecked(_stack2->Main16Bit);

  ui.Stack2WorkShow->setChecked(_stack2->stack->work()->isVisible());
  ui.Stack2WorkBright->setValue(int(_stack2->stack->work()->brightness() * 10000.0f));
  ui.Stack2WorkOpacity->setValue(int(_stack2->stack->work()->opacity() * 10000.0f));
  ui.Stack2WorkLabels->setChecked(_stack2->stack->work()->labels());
  ui.Stack2Work16Bit->setChecked(_stack2->Work16Bit);

  ui.Stack2SurfShow->setChecked(_stack2->mesh->isSurfaceVisible());
  ui.Stack2SurfBright->setValue(int(_stack2->mesh->brightness() * 10000.0f));
  ui.Stack2SurfOpacity->setValue(int(_stack2->mesh->opacity() * 10000.0f));
  ui.Stack2SurfBlend->setChecked(_stack2->mesh->blending());
  ui.Stack2SurfCull->setChecked(_stack2->mesh->culling());

  ui.Stack2SurfView->setCurrentIndex((int)_stack2->mesh->toShow());

  switch(_stack2->mesh->coloring()) {
  case SurfaceColor::Signal:
    ui.Stack2SurfSignal->setChecked(true);
    break;
  case SurfaceColor::Texture:
    ui.Stack2SurfTexture->setChecked(true);
    break;
  case SurfaceColor::Image:
    ui.Stack2SurfImage->setChecked(true);
    break;
  }

  ui.Stack2MeshShow->setChecked(_stack2->mesh->isMeshVisible());
  ui.Stack2MeshView->setCurrentIndex((int)_stack2->mesh->meshView());
  ui.Stack2MeshLines->setChecked(_stack2->mesh->showMeshLines());
  ui.Stack2MeshPoints->setChecked(_stack2->mesh->showMeshPoints());

  ui.Stack2CellMap->setChecked(_stack2->mesh->showMeshCellMap());
  ui.Stack2ShowTrans->setChecked(_stack2->stack->showTrans());
  ui.Stack2ShowBBox->setChecked(_stack2->stack->showBBox());
  ui.Stack2ShowScale->setChecked(_stack2->stack->showScale());
  ui.Stack2TieScales->setChecked(_stack2->stack->tieScales());
  ui.Stack2Scale_X->setValue(_stack2->stack->scale().x());
  ui.Stack2Scale_Y->setValue(_stack2->stack->scale().y());
  ui.Stack2Scale_Z->setValue(_stack2->stack->scale().z());

  ui.Stack2Scale_X->setDefaultValue(0);
  ui.Stack2Scale_Y->setDefaultValue(0);
  ui.Stack2Scale_Z->setDefaultValue(0);

  // Load global settings
  ui.PixelEditRadius->setValue(ImgData::PixelEditRadius);
  ui.PixelEditRadius->setDefaultValue(25);

  ui.FillWorkData->setChecked(ImgData::FillWorkData);
  ui.SeedStack->setChecked(ImgData::SeedStack);
  ui.DrawCutSurf->setChecked(_currentSetup->cuttingSurface()->isVisible());
  CuttingSurface* cut = _currentSetup->cuttingSurface();
  switch(cut->mode()) {
  case CuttingSurface::THREE_AXIS:
    ui.ThreeAxis->setChecked(true);
    break;
  case CuttingSurface::PLANE:
    ui.CutSurfPlane->setChecked(true);
    break;
  case CuttingSurface::BEZIER:
    ui.CutSurfBezier->setChecked(true);
    break;
  }
  ui.CutSurfGrid->setChecked(cut->drawGrid());
  ui.CutSurfBlend->setChecked(cut->blending());
  ui.CutSurfBright->setValue(cut->brightness()*10000);
  ui.CutSurfOpacity->setValue(cut->opacity()*10000);
  ui.CutSurfPoints->setChecked(cut->showPoints());
  setCutSurfControl();

  // ui.CutSurfSizeX->setDefaultValue(0);
  // ui.CutSurfSizeY->setDefaultValue(0);
  // ui.CutSurfSizeZ->setDefaultValue(0);
}

void LithoGraphX::connectControlsIn()
{
  // Create action group for select mode
  QActionGroup* selectActionGroup = new QActionGroup(this);
  selectActionGroup->addAction(ui.actionAddNewSeed);
  selectActionGroup->addAction(ui.actionAddCurrentSeed);
  selectActionGroup->addAction(ui.actionPickLabel);
  selectActionGroup->addAction(ui.actionGrabSeed);
  selectActionGroup->addAction(ui.actionFillLabel);
  selectActionGroup->addAction(ui.actionMeshSelect);
  selectActionGroup->addAction(ui.actionLabelSelect);
  selectActionGroup->addAction(ui.actionConnectedSelect);
  selectActionGroup->addAction(ui.actionDeletePickedLabel);

  selectActionGroup->addAction(ui.actionPixelEdit);
  selectActionGroup->addAction(ui.actionPickVolLabel);
  selectActionGroup->addAction(ui.actionDeleteVolumeLabel);
  selectActionGroup->addAction(ui.actionFillVolumeLabel);
}

void LithoGraphX::connectControlsOut()
{
  setSpinningSensitivity(ui.Viewer->_spinning);
  // Connect outgoing signals from widgets

  ImgData* _stack1 = this->_stack1.data();
  ImgData* _stack2 = this->_stack2.data();

  // Connect controls for Stack 1
  connect(ui.Stack1MainBright, &QSlider::valueChanged, _stack1, &ImgData::setMainBrightness);
  connect(ui.Stack1MainOpacity, &QSlider::valueChanged, _stack1, &ImgData::setMainOpacity);
  connect(ui.Stack1MainLabels, &QAbstractButton::toggled, _stack1, &ImgData::setMainLabel);
  connect(ui.Stack1Main16Bit, &QAbstractButton::toggled, _stack1, &ImgData::setMain16Bits);
  connect(ui.Stack1MainColorMap, &QAbstractButton::clicked, _stack1, &ImgData::editMainTransferFunction);

  connect(ui.Stack1WorkBright, &QSlider::valueChanged, _stack1, &ImgData::setWorkBrightness);
  connect(ui.Stack1WorkOpacity, &QSlider::valueChanged, _stack1, &ImgData::setWorkOpacity);
  connect(ui.Stack1Work16Bit, &QAbstractButton::toggled, _stack1, &ImgData::setWork16Bits);
  connect(ui.Stack1WorkColorMap, &QAbstractButton::clicked, _stack1, &ImgData::editWorkTransferFunction);

  connect(ui.Stack1SurfBright, &QSlider::valueChanged, _stack1, &ImgData::setSurfBrightness);
  connect(ui.Stack1SurfOpacity, &QSlider::valueChanged, _stack1, &ImgData::setSurfOpacity);
  connect(ui.Stack1SurfBlend, &QAbstractButton::toggled, _stack1, &ImgData::setSurfBlend);
  connect(ui.Stack1SurfColorMap, &QAbstractButton::clicked, _stack1, &ImgData::editSurfTransferFunction);
  connect(ui.Stack1HeatColorMap, &QAbstractButton::clicked, _stack1, &ImgData::editHeatTransferFunction);
  connect(ui.Stack1SurfCull, &QAbstractButton::toggled, _stack1, &ImgData::setSurfCull);

  connect(ui.Stack1SurfView, (void (QComboBox::*)(int))&QComboBox::currentIndexChanged,
          _stack1, &ImgData::setSurfView);
  connect(ui.Stack1SurfSignal, &QAbstractButton::toggled, _stack1, &ImgData::setSurfSignal);
  connect(ui.Stack1SurfTexture, &QAbstractButton::toggled, _stack1, &ImgData::setSurfTexture);
  connect(ui.Stack1SurfImage, &QAbstractButton::toggled, _stack1, &ImgData::setSurfImage);

  connect(ui.Stack1MeshView, (void (QComboBox::*)(int))&QComboBox::currentIndexChanged,
          _stack1, &ImgData::setChangeMeshViewMode);
  connect(ui.Stack1MeshLines, &QAbstractButton::toggled, _stack1, &ImgData::setMeshLines);
  connect(ui.Stack1MeshPoints, &QAbstractButton::toggled, _stack1, &ImgData::setMeshPoints);

  connect(ui.Stack1Info, &QAbstractButton::clicked, _stack1, &ImgData::showInfo);

  connect(ui.Stack1CellMap, &QAbstractButton::toggled, _stack1, &ImgData::setCellMap);
  connect(ui.Stack1ShowTrans, &QAbstractButton::toggled, _stack1, &ImgData::setShowTrans);
  connect(ui.Stack1ShowBBox, &QAbstractButton::toggled, _stack1, &ImgData::setShowBBox);
  connect(ui.Stack1ShowScale, &QAbstractButton::toggled, _stack1, &ImgData::setShowScale);
  connect(ui.Stack1TieScales, &QAbstractButton::toggled, _stack1, &ImgData::setTieScales);
  connect(ui.Stack1Scale_X, &QSlider::valueChanged, _stack1, &ImgData::setScaleX);
  connect(ui.Stack1Scale_Y, &QSlider::valueChanged, _stack1, &ImgData::setScaleY);
  connect(ui.Stack1Scale_Z, &QSlider::valueChanged, _stack1, &ImgData::setScaleZ);

  // Connect controls for Stack 2
  connect(ui.Stack2MainBright, &QSlider::valueChanged, _stack2, &ImgData::setMainBrightness);
  connect(ui.Stack2MainOpacity, &QSlider::valueChanged, _stack2, &ImgData::setMainOpacity);
  connect(ui.Stack2MainLabels, &QAbstractButton::toggled, _stack2, &ImgData::setMainLabel);
  connect(ui.Stack2Main16Bit, &QAbstractButton::toggled, _stack2, &ImgData::setMain16Bits);
  connect(ui.Stack2MainColorMap, &QAbstractButton::clicked, _stack2, &ImgData::editMainTransferFunction);

  connect(ui.Stack2WorkBright, &QSlider::valueChanged, _stack2, &ImgData::setWorkBrightness);
  connect(ui.Stack2WorkOpacity, &QSlider::valueChanged, _stack2, &ImgData::setWorkOpacity);
  connect(ui.Stack2Work16Bit, &QAbstractButton::toggled, _stack2, &ImgData::setWork16Bits);
  connect(ui.Stack2WorkColorMap, &QAbstractButton::clicked, _stack2, &ImgData::editWorkTransferFunction);

  connect(ui.Stack2SurfBright, &QSlider::valueChanged, _stack2, &ImgData::setSurfBrightness);
  connect(ui.Stack2SurfOpacity, &QSlider::valueChanged, _stack2, &ImgData::setSurfOpacity);
  connect(ui.Stack2SurfColorMap, &QAbstractButton::clicked, _stack2, &ImgData::editSurfTransferFunction);
  connect(ui.Stack2HeatColorMap, &QAbstractButton::clicked, _stack2, &ImgData::editHeatTransferFunction);
  connect(ui.Stack2SurfBlend, &QAbstractButton::toggled, _stack2, &ImgData::setSurfBlend);
  connect(ui.Stack2SurfCull, &QAbstractButton::toggled, _stack2, &ImgData::setSurfCull);

  connect(ui.Stack2SurfView, (void (QComboBox::*)(int))&QComboBox::currentIndexChanged,
          _stack2, &ImgData::setSurfView);

  connect(ui.Stack2SurfSignal, &QAbstractButton::toggled, _stack2, &ImgData::setSurfSignal);
  connect(ui.Stack2SurfTexture, &QAbstractButton::toggled, _stack2, &ImgData::setSurfTexture);
  connect(ui.Stack2SurfImage, &QAbstractButton::toggled, _stack2, &ImgData::setSurfImage);

  connect(ui.Stack2MeshView, (void (QComboBox::*)(int))&QComboBox::currentIndexChanged,
          _stack2, &ImgData::setChangeMeshViewMode);
  connect(ui.Stack2MeshLines, &QAbstractButton::toggled, _stack2, &ImgData::setMeshLines);
  connect(ui.Stack2MeshPoints, &QAbstractButton::toggled, _stack2, &ImgData::setMeshPoints);

  connect(ui.Stack2Info, &QAbstractButton::clicked, _stack2, &ImgData::showInfo);

  connect(ui.Stack2CellMap, &QAbstractButton::toggled, _stack2, &ImgData::setCellMap);
  connect(ui.Stack2ShowTrans, &QAbstractButton::toggled, _stack2, &ImgData::setShowTrans);
  connect(ui.Stack2ShowScale, &QAbstractButton::toggled, _stack2, &ImgData::setShowScale);
  connect(ui.Stack2TieScales, &QAbstractButton::toggled, _stack2, &ImgData::setTieScales);
  connect(ui.Stack2ShowBBox, &QAbstractButton::toggled, _stack2, &ImgData::setShowBBox);
  connect(ui.Stack2Scale_X, &QSlider::valueChanged, _stack2, &ImgData::setScaleX);
  connect(ui.Stack2Scale_Y, &QSlider::valueChanged, _stack2, &ImgData::setScaleY);
  connect(ui.Stack2Scale_Z, &QSlider::valueChanged, _stack2, &ImgData::setScaleZ);

  // Volume toolbar
  connect(ui.actionLabelColor, &QAction::triggered, ui.Viewer, &LithoViewer::resetLabel);

  // View toolbar
  connect(Colors::instance(), &lgx::Colors::colorsChanged, _stack1, &ImgData::updateColors);
  connect(Colors::instance(), &lgx::Colors::colorsChanged, _stack2, &ImgData::updateColors);
  connect(Colors::instance(), &lgx::Colors::colorsChanged, ui.Viewer, &LithoViewer::update3D);

  connect(ui.actionSaveScreenshot, SIGNAL(triggered()), ui.Viewer, SLOT(saveScreenshot()));
  connect(ui.Viewer, &LithoViewer::recordingMovie, ui.actionRecordMovie, &QAction::setChecked);
  connect(ui.actionRecordMovie, &QAction::toggled, ui.Viewer, &LithoViewer::recordMovie);
  connect(ui.actionReload_shaders, &QAction::triggered, ui.Viewer, &LithoViewer::reloadShaders);

  // Object communication
  connect(_stack1, &ImgData::viewerUpdate, ui.Viewer, &LithoViewer::update3D);
  connect(_stack2, &ImgData::viewerUpdate, ui.Viewer, &LithoViewer::update3D);

  // Cutting surface controls
  connect(ui.DrawCutSurf, &QAbstractButton::toggled, &_cutSurf, &CutSurf::setVisible);
  connect(ui.ThreeAxis, &QAbstractButton::toggled, &_cutSurf, &CutSurf::selectThreeAxis);
  connect(ui.CutSurfGrid, &QAbstractButton::toggled, &_cutSurf, &CutSurf::showGrid);
  connect(ui.CutSurfPlane, &QAbstractButton::toggled, &_cutSurf, &CutSurf::selectPlane);
  connect(ui.CutSurfBezier, &QAbstractButton::toggled, &_cutSurf, &CutSurf::selectBezier);
  connect(ui.CutSurfSizeX, &QSlider::valueChanged, &_cutSurf, &CutSurf::setSizeX);
  connect(ui.CutSurfSizeY, &QSlider::valueChanged, &_cutSurf, &CutSurf::setSizeY);
  connect(ui.CutSurfSizeZ, &QSlider::valueChanged, &_cutSurf, &CutSurf::setSizeZ);
  connect(ui.CutSurfBlend, &QAbstractButton::toggled, &_cutSurf, &CutSurf::setBlend);
  connect(ui.CutSurfPoints, &QAbstractButton::toggled, &_cutSurf, &CutSurf::showPoints);
  connect(ui.CutSurfOpacity, &QSlider::valueChanged, &_cutSurf, &CutSurf::setOpacity);
  connect(ui.CutSurfBright, &QSlider::valueChanged, &_cutSurf, &CutSurf::setBrightness);
}

void LithoGraphX::readParms()
{
  // Get filename
  QString filename = projectFile();
  modified(false);

  if(!filename.isEmpty()) {
    filename = util::resolvePath(filename);

    QFileInfo fi(filename);
    if(not fi.exists()) {
      if(not filename.endsWith(".lgxp", Qt::CaseInsensitive))
        filename += ".lgxp";
    }
    fi = QFileInfo(filename);

    setWindowTitle(QString("LithoGraphX - ") + util::stripCurrentDir(filename));

    // Check that file can be read
    if(not fi.exists() or not fi.isReadable()) {
      SETSTATUS("readParms::Cannot open parms file for reading: " << projectFile());
      return;
    }
  }

  // Create parms object
  util::Parms parms(projectFile());

  // Read model parms
  parms("Main", "ZOffset", ImgData::ZOffset, .0001f);

  parms("Main", "TileCount", ImgData::TileCount, 20u);
  //parms("Main", "MaxTexSize", ImgData::MaxTexSize, Point3u(1024u, 1024u, 1024u));
  parms.optional("Main", "DrawNormals", ImgData::DrawNormals);

  parms("Main", "DrawOffset", ImgData::DrawOffset, 1.0f);
  if(ImgData::DrawOffset < 1.0f)
    ImgData::DrawOffset = 1.0f;
  parms("Main", "DrawZeroLabels", ImgData::DrawZeroLabels, 0.0f);
  parms("Main", "DrawNhbds", ImgData::DrawNhbds, 0.0f);
  parms("Main", "DeleteBadVertex", ImgData::DeleteBadVertex, false);

  ImgData::scaleBar.readParms(parms, "ScaleBar");
  ImgData::colorBar.readParms(parms, "ColorBar");

  parms("Main", "PixelEditRadius", ImgData::PixelEditRadius, 25);
  parms("Main", "PixelEditMaxPix", ImgData::PixelEditMaxPix, 1024000);

  parms("Main", "MeshPointSize", ImgData::MeshPointSize, 3.0f);
  parms("Main", "MeshLineWidth", ImgData::MeshLineWidth, 1.0f);
  parms("Main", "FillWorkData", ImgData::FillWorkData, false);
  parms("Main", "SeedStack", ImgData::SeedStack, false);

  Colors::instance()->readParms(parms, "Colors");

  // Read the viewer and stack parms
  ui.Viewer->readParms(parms, "Main");
  if(_currentSetup) {
    _currentSetup->setGlobalContrast(ui.Viewer->_globalContrast);
    _currentSetup->setGlobalBrightness(ui.Viewer->_globalBrightness);
  }
  _stack1->readParms(parms, "Stack1");
  _stack2->readParms(parms, "Stack2");
  _cutSurf.readParms(parms, "Plane");

  readProcessParms();

  parms.all("Labels", "Color", ImgData::LabelColors);
  if(ImgData::LabelColors.empty())
    lgx::resetLabelColors(ImgData::LabelColors);
  _stack1->initTex();
  _stack2->initTex();

}

static void readProcessesParams(util::Parms& parms, QString section, QHash<QString, process::BaseProcessDefinition>& defs)
{
  QString mgxVersion;
  parms("Main", "LGXVersion", mgxVersion, QString("0.0"));

  std::vector<QString> procNames;
  std::vector<uint> numParms;
  std::vector<QString> parmNames;
  std::vector<QString> parmStrings;

  // Read process parms into arrays
  if(mgxVersion >= QString("1.0")) {
    parms.all(section, "ProcessName", procNames);
    parms.all(section, "NumParms", numParms);
    parms.all(section, "ParmName", parmNames);
    parms.all(section, "ParmString", parmStrings);
  } else {
    // RSS: Old code had both "strings" and "values", as well as "Named" vs "UnNamed" paramters
    // Now all parameters are stored as strings, and all parameters have names
    std::vector<int> nbStrings;
    std::vector<int> nbValues;
    std::vector<int> nbNamedStrings;
    std::vector<int> nbNamedValues;
    std::vector<QString> strings;
    std::vector<QString> values;

    parms.all(section, "ProcessName", procNames);
    parms.all(section, "NbProcessStrings", nbStrings);
    parms.all(section, "NbProcessValues", nbValues);
    parms.all(section, "NbProcessNamedStrings", nbNamedStrings);
    parms.all(section, "NbProcessNamedValues", nbNamedValues);
    parms.all(section, "ProcessParameter", parmNames);
    parms.all(section, "ProcessString", strings);
    parms.all(section, "ProcessValue", values);

    int valuesIdx = 0;
    int stringsIdx = 0;
    for(size_t i = 0; i < procNames.size(); ++i) {
      if(i >= nbStrings.size() or i >= nbValues.size() or i >= nbNamedStrings.size()
         or i >= nbNamedValues.size()) {
        Information::out << "Error reading Process definition for process " << procNames[i] << endl;
        return;
      }
      numParms.push_back(nbNamedStrings[i] + nbNamedValues[i]);
      // Get named strings
      for(int j = 0; j < nbNamedStrings[i]; ++j) {
        if(stringsIdx >= (int)strings.size()) {
          Information::out << "Error reading String parms for process " << procNames[i] << endl;
          return;
        }
        parmStrings.push_back(strings[stringsIdx++]);
      }
      // Toss "unnamed strings"
      stringsIdx += nbStrings[i] - nbNamedStrings[i];

      // Get named values
      for(int j = 0; j < nbNamedValues[i]; ++j) {
        if(valuesIdx >= (int)values.size()) {
          Information::out << "Error reading Value parms for process " << procNames[i] << endl;
          return;
        }
        parmStrings.push_back(values[valuesIdx++]);
      }
      // Toss "unnamed values"
      valuesIdx += nbValues[i] - nbNamedValues[i];
    }
  }

  // Now process parameters
  size_t parmIdx = 0;
  for(size_t i = 0; i < procNames.size(); ++i) {
    // Check process definition
    if(i >= numParms.size()) {
      Information::out << "Error reading Process definition for process " << procNames[i] << endl;
      return;
    }

    // Get parm names and their string values and put in a map
    std::map<QString, QString> parmMap;
    for(size_t j = 0; j < numParms[i]; ++j) {
      if(parmIdx + j >= parmNames.size() or parmIdx + j >= parmStrings.size()) {
        Information::out << "Error reading Parms for process " << procNames[i] << endl;
        return;
      }
      parmMap[parmNames[parmIdx + j]] = parmStrings[parmIdx + j];
    }

    // Get process definition and fill in the parms
    if(defs.contains(procNames[i])) {
      process::BaseProcessDefinition& def = defs[procNames[i]];
      for(int j = 0; j < def.parmNames.size(); ++j)
        if(parmMap.count(def.parmNames[j]) == 1)
          def.parms[j] = parmMap[def.parmNames[j]];
        else
          Information::out << "Parameter '" << def.parmNames[j] << "' not found for process '" << procNames[i]
                           << "'" << endl;
    } else
      Information::out << "Process '" << procNames[i] << "' not found." << endl;
    parmIdx += numParms[i];
  }
}

void LithoGraphX::readProcessParms()
{
  util::Parms parms(projectFile());

  Information::out << "Reading Stack Process Parameters" << endl;
  readProcessesParams(parms, "StackProcess", _savedStackProc);

  Information::out << "Reading Mesh Process Parameters" << endl;
  readProcessesParams(parms, "MeshProcess", _savedMeshProc);

  Information::out << "Reading Global Process Parameters" << endl;
  readProcessesParams(parms, "GlobalProcess", _savedGlobalProc);

  Information::out << "Reading Tasks" << endl;

  TaskEditDlg::tasks_t newTasks;
  TaskEditDlg::readTasks(projectFile(), _savedStackProc, _savedMeshProc, _savedGlobalProc, newTasks);
  updateCurrentTasks(newTasks);
}

void LithoGraphX::updateCurrentTasks(const TaskEditDlg::tasks_t& saved_tasks)
{
  // Now, update the tasks with the saved ones, unless empty
  for(TaskEditDlg::tasks_t::const_iterator it = saved_tasks.begin(); it != saved_tasks.end(); ++it) {
    QString name = it.key();
    QList<TypedProcessDefinition> sprocs = it.value();
    bool task_exists = _tasks.contains(name);
    std::vector<bool> used;
    forall(TypedProcessDefinition& sdef, sprocs) {
      // Check the parameters are valid
      process::BaseProcessDefinition* real_def = process::getBaseProcessDefinition(sdef.type, sdef.name);
      if(real_def) {
        sdef.folder = real_def->folder;
        sdef.description = real_def->description;
        sdef.parmNames = real_def->parmNames;
        sdef.parmDescs = real_def->parmDescs;
        sdef.icon = real_def->icon;
        sdef.parmChoice = real_def->parmChoice;
      }

      int old_proc_id = -1;
      if(task_exists) {
        const QList<TypedProcessDefinition>& procs = _tasks[name];
        used.resize(procs.size(), false);
        for(int i = 0; i < procs.size(); ++i) {
          const TypedProcessDefinition& def = procs[i];
          if(not used[i] and def.name == sdef.name and def.type == sdef.type) {
            used[i] = true;
            old_proc_id = i;
            break;
          }
        }
      }
      if(not process::checkProcessParms(sdef.type, sdef.name, sdef.parms)) {
        if(old_proc_id < 0)
          process::getLastParms(sdef.type, sdef.name, sdef.parms);
        else {
          const TypedProcessDefinition& def = _tasks[name][old_proc_id];
          sdef.parms = def.parms;
        }
      }
    }
    _tasks[name] = sprocs;
  }
}

void LithoGraphX::writeProcessParams(const process::BaseProcessDefinition& def, QTextStream& pout)
{
  pout << "ProcessName: " << def.name << endl;
  pout << "NumParms: " << def.parmNames.size() << endl;
  for(int i = 0; i < def.parmNames.size(); ++i) {
    pout << "ParmName: " << def.parmNames[i] << endl;
    pout << "ParmString: " << def.parms[i].toString() << endl;
  }
}

void LithoGraphX::writeParms()
{
  // Open the file for write
  QFile file(projectFile());
  if(!file.open(QIODevice::WriteOnly | QIODevice::Text)) {
    SETSTATUS("writeParms::Cannot open parms file for writing-" << projectFile());
    return;
  }
  QTextStream pout(&file);
  pout.setCodec("UTF-8");

  pout << "// Parameters for LithoGraphX" << endl;
  pout << "[Main]" << endl;

  // Write out model parms
  pout << "LGXVersion: " << VERSION << endl;
  pout << "ClearColor: " << ImgData::ClearColor << endl;
  pout << "ZOffset: " << ImgData::ZOffset << endl;
  pout << "TileCount: " << ImgData::TileCount << endl;
  pout << "MaxTexSize: " << ImgData::MaxTexSize << endl;
  pout << endl;

  pout << "DrawNormals: " << ImgData::DrawNormals << endl;
  pout << "DrawOffset: " << ImgData::DrawOffset << endl;

  pout << "DrawZeroLabels: " << ImgData::DrawZeroLabels << endl;
  pout << "DrawNhbds: " << ImgData::DrawNhbds << endl;
  pout << "DeleteBadVertex: " << (ImgData::DeleteBadVertex ? "true" : "false") << endl;
  pout << endl;

  pout << "MeshPointSize: " << ImgData::MeshPointSize << endl;
  pout << "MeshLineWidth: " << ImgData::MeshLineWidth << endl;
  pout << "SeedStack: " << (ImgData::SeedStack ? "true" : "false") << endl;

  pout << "PixelEditRadius: " << ImgData::PixelEditRadius << endl;
  pout << "PixelEditMaxPix: " << ImgData::PixelEditMaxPix << endl;
  pout << endl;

  pout << "[Labels]" << endl;
  for(size_t i = 0; i < ImgData::LabelColors.size(); ++i)
    pout << "Color: " << ImgData::LabelColors[i] << endl;
  pout << endl;

  Colors::instance()->writeParms(pout, "Colors");

  // Write the viewer and stack parms
  ui.Viewer->writeParms(pout, "Main");
  _stack1->writeParms(pout, "Stack1");
  _stack2->writeParms(pout, "Stack2");
  _cutSurf.writeParms(pout, "Plane");

  ImgData::scaleBar.writeParms(pout, "ScaleBar");
  ImgData::colorBar.writeParms(pout, "ColorBar");

  pout << endl << "[StackProcess]" << endl;
  auto manager = process::pluginManager();
  for(const QString& name: manager->processes<StackProcess>())
    writeProcessParams(manager->processDefinition<StackProcess>(name), pout);

  pout << endl << "[MeshProcess]" << endl;
  for(const QString& name: manager->processes<MeshProcess>())
    writeProcessParams(manager->processDefinition<MeshProcess>(name), pout);

  pout << endl << "[GlobalProcess]" << endl;
  for(const QString& name: manager->processes<GlobalProcess>())
    writeProcessParams(manager->processDefinition<GlobalProcess>(name), pout);

  // Write out tasks
  TaskEditDlg::writeTasks(pout, _tasks);

  _needSaving = false;
}

void LithoGraphX::saveProject(const QString& filename, bool choose_file)
{
  auto parms = process::ParmList() << filename << choose_file;
  launchProcess("Global", "Save Project", parms);
}

void LithoGraphX::initProject()
{
  auto parms = process::ParmList() << "" << true;
  launchProcess("Global", "Load Project", parms, false, false);
}

void LithoGraphX::loadProject(const QString& filename, bool choose_file)
{
  auto parms = process::ParmList() << filename << choose_file;
  if(launchProcess("Global", "Load Project", parms))
    _needSaving = false;
}

void LithoGraphX::clearMeshSelect()
{
  _stack1->lineId = 0;
  _stack2->lineId = 0;
  _stack1->clearMeshSelect();
  _stack2->clearMeshSelect();
}

// Slots

void LithoGraphX::on_ManipulateStack1_toggled(bool on)
{
  if(on) {
    ui.Viewer->setManipulatedFrame(&_stack1->getFrame());
  }
}

void LithoGraphX::on_ManipulateStack2_toggled(bool on)
{
  if(on) {
    ui.Viewer->setManipulatedFrame(&_stack2->getFrame());
  }
}
void LithoGraphX::on_ManipulateClip1_toggled(bool on)
{
  if(on) {
    ui.Viewer->setManipulatedFrame(&ui.Viewer->_c1->frame());
  }
}
void LithoGraphX::on_ManipulateClip2_toggled(bool on)
{
  if(on) {
    ui.Viewer->setManipulatedFrame(&ui.Viewer->_c2->frame());
  }
}
void LithoGraphX::on_ManipulateClip3_toggled(bool on)
{
  if(on) {
    ui.Viewer->setManipulatedFrame(&ui.Viewer->_c3->frame());
  }
}
void LithoGraphX::on_ManipulateCutSurf_toggled(bool on)
{
  if(on) {
    ui.Viewer->setManipulatedFrame(&_cutSurf.getFrame());
  }
}

void LithoGraphX::setSpinningSensitivity(float spin)
{
  _stack1->getFrame().setSpinningSensitivity(spin);
  _stack2->getFrame().setSpinningSensitivity(spin);
  ui.Viewer->_c1->frame().setSpinningSensitivity(spin);
  ui.Viewer->_c2->frame().setSpinningSensitivity(spin);
  ui.Viewer->_c3->frame().setSpinningSensitivity(spin);
  _cutSurf.getFrame().setSpinningSensitivity(spin);
  ui.Viewer->_c1->frame().setSpinningSensitivity(spin);
}

void LithoGraphX::on_menuFileOpen_triggered()
{
  loadProject(projectFile(), true);
}

void LithoGraphX::on_menuFileSaveAll_triggered()
{
  process::ParmList parms;
  if(!process::getLastParms("Global", "Save All", parms)) {
    QMessageBox::critical(this, "Error saving all", "There is no Global process named 'Save All'");
    return;
  }
  parms[0] = "All";
  parms[1] = "";
  launchProcess("Global", "Save All", parms);
}

void LithoGraphX::on_menuFileResetAll_triggered()
{
  process::ParmList parms;
  if(!process::getLastParms("Global", "Reset All", parms)) {
    QMessageBox::critical(this, "Error reset all", "There is no Global process named 'Reset All'");
    return;
  }
  launchProcess("Global", "Reset All", parms);
}

void LithoGraphX::on_menuFileReloadAll_triggered()
{
  process::ParmList parms;
  launchProcess("Global", "Load All", parms);
}

void LithoGraphX::on_menuFileEditPaths_triggered()
{
  if(_editPathsDlg) {
    if(_editPathsDlg->isVisible())
      _editPathsDlg->close();
    else
      _editPathsDlg->show();
  } else {
    SETSTATUS("Create new edit paths dialog");
    _editPathsDlg = new PathEditorDlg(this, dynamic_cast<LGXCamera*>(ui.Viewer->camera()));
    _editPathsDlg->show();
  }
}

void LithoGraphX::on_menuFileSave_triggered()
{
  if(projectFile().isEmpty())
    saveProject(projectFile(), true);
  else
    saveProject(projectFile(), false);
}

void LithoGraphX::on_menuFileSaveAs_triggered()
{
  saveProject(projectFile(), true);
}

void LithoGraphX::on_menuExit_triggered() {
  close();
}

QString LithoGraphX::getMeshName(int num)
{
  ImgData* data = (num == 0) ? _stack1 : _stack2;
  return data->mesh->file();
}

QString LithoGraphX::getStackName(bool main, int num)
{
  ImgData* data = (num == 0) ? _stack1 : _stack2;
  if(main)
    return data->stack->main()->file();
  else
    return data->stack->work()->file();
}

void LithoGraphX::resetMesh(int stack)
{
  QMessageBox::StandardButton a = QMessageBox::question(this, "Reset Mesh", "Are you sure you want to reset the mesh?",
                                                        QMessageBox::No | QMessageBox::Yes);
  if(a == QMessageBox::No)
    return;

  process::ParmList parms;
  if(!process::getLastParms("Mesh", "Reset", parms)) {
    QMessageBox::critical(this, "Error reseting mesh", "There is no Mesh process named 'Reset'");
    return;
  }
  parms[0] = stack;
  launchProcess("Mesh", "Reset", parms, true, true);
}

void LithoGraphX::exportMesh(int stack)
{
  process::ParmList parms;
  if(!process::getLastParms("Mesh", "Export", parms)) {
    QMessageBox::critical(this, "Error exporting mesh", "There is no Mesh process named 'Export'");
    return;
  }
  QString fn = getMeshName(stack);
  if(!fn.isEmpty())
    parms[0] = fn;
  parms[3] = stack;
  launchProcess("Mesh", "Export", parms, true, true);
}

void LithoGraphX::loadMesh(int stack)
{
  process::ParmList parms;
  if(!process::getLastParms("Mesh", "Load", parms)) {
    QMessageBox::critical(this, "Error loading mesh", "There is no Mesh process named 'Load'");
    return;
  }
  QString fn = getMeshName(stack);
  if(!fn.isEmpty())
    parms[0] = fn;
  parms[3] = stack;
  launchProcess("Mesh", "Load", parms, true, true);
}

void LithoGraphX::saveMesh(int stack)
{
  process::ParmList parms;
  if(!process::getLastParms("Mesh", "Save", parms)) {
    QMessageBox::critical(this, "Error saving mesh", "There is no Mesh process named 'Save'");
    return;
  }
  QString fn = getMeshName(stack);
  if(!fn.isEmpty())
    parms[0] = fn;
  parms[2] = stack;
  launchProcess("Mesh", "Save", parms, true, true);
}

void LithoGraphX::importStackSeries(int stack, bool main)
{
  process::ParmList parms;
  if(!process::getLastParms("Stack", "Import", parms)) {
    QMessageBox::critical(this, "Error loading stack", "There is no Stack process named 'Import Stack'");
    return;
  }
  parms[0] = stack;
  parms[1] = QVariant::fromValue(main ? STORE::Main : STORE::Work);
  launchProcess("Stack", "Import", parms, true, true);
}

void LithoGraphX::importStack(int stack, bool main)
{
  process::ParmList parms;
  if(!process::getLastParms("Stack", "ITK Image Reader", parms)) {
    QMessageBox::critical(this, "Error loading stack", "There is no Stack process named 'ITK Image Reader'");
    return;
  }
  parms[1] = QVariant::fromValue(main ? STORE::Main : STORE::Work);
  parms[2] = stack;
  launchProcess("Stack", "ITK Image Reader", parms, true, true);
}

void LithoGraphX::openStack(int stack, bool main)
{
  process::ParmList parms;
  if(!process::getLastParms("Stack", "Open", parms)) {
    QMessageBox::critical(this, "Error opening stack", "There is no Stack process named 'Open Stack'");
    return;
  }
  QString fn = getStackName(main, stack);
  if(!fn.isEmpty())
    parms[0] = fn;
  parms[1] = QVariant::fromValue(main ? STORE::Main : STORE::Work);
  parms[2] = stack;
  launchProcess("Stack", "Open", parms, true, true);
}

void LithoGraphX::saveStack(int stack, bool main)
{
  // Check if stack is scaled before saving
  Stack* s = _currentSetup->stack(stack);
  if(s->showScale()) {
    QMessageBox::StandardButton a = QMessageBox::question(
        this, "Save Stack", QString("Stack %1 is scaled. Do you want to save it?").arg(stack + 1),
        QMessageBox::No | QMessageBox::Yes);
    if(a == QMessageBox::No)
      return;
  }

  process::ParmList parms;
  if(!process::getLastParms("Stack", "Save", parms)) {
    QMessageBox::critical(this, "Error saving stack", "There is no Stack process named 'Save'");
    return;
  }
  QString fn = getStackName(main, stack);
  if(!fn.isEmpty())
    parms[0] = fn;
  parms[1] = QVariant::fromValue(main ? STORE::Main : STORE::Work);
  parms[2] = stack;
  launchProcess("Stack", "Save", parms, true, true);
}

void LithoGraphX::exportStack(int stack, bool main)
{
  process::ParmList parms;
  if(!process::getLastParms("Stack", "Export", parms)) {
    QMessageBox::critical(this, "Error exporting stack", "There is no Stack process named 'Export'");
    return;
  }
  parms[1] = QVariant::fromValue(main ? STORE::Main : STORE::Work);
  parms[4] = stack;
  launchProcess("Stack", "Export", parms, true, true);
}

void LithoGraphX::on_menuStack1Import_triggered() {
  importStack(0, true);
}

void LithoGraphX::on_menuStack1Load_triggered() {
  importStackSeries(0, true);
}

void LithoGraphX::on_menuStack1Save_triggered() {
  saveStack(0, true);
}

void LithoGraphX::on_menuStack1Export_triggered() {
  exportStack(0, true);
}

void LithoGraphX::on_menuStack1Open_triggered() {
  openStack(0, true);
}

void LithoGraphX::on_menuStack1Reset_triggered()
{
  _stack1->resetStack();
  ui.Viewer->update3D();
}

void LithoGraphX::on_menuStack2Import_triggered() {
  importStack(1, true);
}

void LithoGraphX::on_menuStack2Load_triggered() {
  importStackSeries(1, true);
}

void LithoGraphX::on_menuStack2Save_triggered() {
  saveStack(1, true);
}

void LithoGraphX::on_menuStack2Export_triggered() {
  exportStack(1, true);
}

void LithoGraphX::on_menuStack2Open_triggered() {
  openStack(1, true);
}

void LithoGraphX::on_menuStack2Reset_triggered()
{
  _stack2->resetStack();
  ui.Viewer->update3D();
}

void LithoGraphX::on_menuWorkStack1Import_triggered() {
  importStack(0, false);
}

void LithoGraphX::on_menuWorkStack1Load_triggered() {
  importStackSeries(0, false);
}

void LithoGraphX::on_menuWorkStack1Save_triggered() {
  saveStack(0, false);
}

void LithoGraphX::on_menuWorkStack1Export_triggered() {
  exportStack(0, false);
}

void LithoGraphX::on_menuWorkStack1Open_triggered() {
  openStack(0, false);
}

void LithoGraphX::on_menuWorkStack2Import_triggered() {
  importStack(1, false);
}

void LithoGraphX::on_menuWorkStack2Load_triggered() {
  importStackSeries(1, false);
}

void LithoGraphX::on_menuWorkStack2Save_triggered() {
  saveStack(1, false);
}

void LithoGraphX::on_menuWorkStack2Export_triggered() {
  exportStack(1, false);
}

void LithoGraphX::on_menuWorkStack2Open_triggered() {
  openStack(1, false);
}

void LithoGraphX::on_menuMesh1Load_triggered() {
  loadMesh(0);
}

void LithoGraphX::on_menuMesh1Save_triggered() {
  saveMesh(0);
}

void LithoGraphX::on_menuMesh2Export_triggered() {
  exportMesh(1);
}

void LithoGraphX::on_menuMesh1Export_triggered() {
  exportMesh(0);
}

void LithoGraphX::on_menuMesh1Reset_triggered() {
  resetMesh(0);
}

void LithoGraphX::on_menuMesh2Load_triggered() {
  loadMesh(1);
}

void LithoGraphX::on_menuMesh2Save_triggered() {
  saveMesh(1);
}

void LithoGraphX::on_menuMesh2Reset_triggered() {
  resetMesh(1);
}

void LithoGraphX::on_actionSelectAll_triggered()
{
  process::ParmList parms;
  if(!process::getLastParms("Mesh", "Select All", parms)) {
    QMessageBox::critical(this, "Error select all", "There is no Mesh process named 'Select All'");
    return;
  }
  launchProcess("Mesh", "Select All", parms, true, true);
}

void LithoGraphX::on_actionUnselect_triggered()
{
  process::ParmList parms;
  if(!process::getLastParms("Mesh", "Unselect", parms)) {
    QMessageBox::critical(this, "Error unselect", "There is no Mesh process named 'Unselect'");
    return;
  }
  launchProcess("Mesh", "Unselect", parms, true, true);
}

void LithoGraphX::on_actionInvertSelection_triggered()
{
  process::ParmList parms;
  if(!process::getLastParms("Mesh", "Invert Selection", parms)) {
    QMessageBox::critical(this, "Error invert selection", "There is no Mesh process named 'Invert Selection'");
    return;
  }
  launchProcess("Mesh", "Invert Selection", parms, true, true);
}

void LithoGraphX::on_actionAddCurrentLabel_triggered()
{
  process::ParmList parms;
  if(!process::getLastParms("Mesh", "Select Label", parms)) {
    QMessageBox::critical(this, "Error add current label", "There is no Mesh process named 'Select Label'");
    return;
  }
  parms[0] = false;
  parms[1] = ui.Viewer->_selectedLabel;
  launchProcess("Mesh", "Select Label", parms, true, true);
}

void LithoGraphX::on_actionSelectUnlabeled_triggered()
{
  process::ParmList parms;
  if(!process::getLastParms("Mesh", "Select Unlabeled", parms)) {
    QMessageBox::critical(this, "Error add unlabeled", "There is no Mesh process named 'Select Unlabeled'");
    return;
  }
  parms[0] = false;
  launchProcess("Mesh", "Select Unlabeled", parms, true, true);
}

void LithoGraphX::on_actionRemoveCurrentLabel_triggered()
{
  process::ParmList parms;
  if(!process::getLastParms("Mesh", "Unselect Label", parms)) {
    QMessageBox::critical(this, "Error remove current label", "There is no Mesh process named 'Unselect Label'");
    return;
  }
  parms[0] = 0;
  launchProcess("Mesh", "Unselect Label", parms, true, true);
}

void LithoGraphX::on_actionChangeSeed_triggered()
{
  bool ok;
  int value = QInputDialog::getInt(this, "Change Current Seed", "New value for the seed:", ui.Viewer->_selectedLabel, 0,
                                   65535, 1, &ok);
  if(ok)
    ui.Viewer->setLabel(value);
}

void LithoGraphX::on_actionNew_Seed_triggered()
{
  ui.Viewer->setNewLabel();
}

void LithoGraphX::on_actionUserManual_triggered()
{
  QString link = FILE_PREFIX + util::docsDir().canonicalPath() + "/UserManual/html/index.html";
  QDesktopServices::openUrl(QUrl(link));
}

void LithoGraphX::on_actionOnlineUserManual_triggered()
{
  QString link = "https://lithographx.readthedocs.org";
  QDesktopServices::openUrl(QUrl(link));
}

void LithoGraphX::on_actionDoxygen_triggered()
{
  QString link = FILE_PREFIX + util::docsDir().canonicalPath() + "/DeveloperManual/index.html";
  Information::out << "Doxygen link:" << link << endl;
  QDesktopServices::openUrl(QUrl(link));
}

void LithoGraphX::on_actionQGLViewerHelp_triggered() {
  ui.Viewer->aboutQGLViewer();
}

void LithoGraphX::on_actionAbout_triggered()
{
  //QString about(aboutText());

  //QMessageBox dlg(tr("About LithoGraphX"), about, QMessageBox::Information, QMessageBox::Ok, QMessageBox::NoButton,
                  //QMessageBox::NoButton, this);
  //// Set icon
  //dlg.setIconPixmap(QPixmap(":/images/Icon256.png"));
  //dlg.setTextFormat(Qt::RichText);
  //dlg.exec();
  QMessageBox::about(this, tr("About LithoGraphX"), aboutText());
}

void LithoGraphX::on_actionLibrariesUsed_triggered()
{
  LibrariesDlg dlg(LibraryDeclaration::libraries(), this);
  dlg.exec();
}

void LithoGraphX::on_actionProcessDocs_triggered()
{
  if(not _processDocDialog)
    _processDocDialog = new ProcessDocDialog(this);
  _processDocDialog->show();
  _processDocDialog->raise();
}

void LithoGraphX::on_actionResetView_triggered()
{
  ui.Viewer->resetView();
}

void LithoGraphX::on_Viewer_deleteSelection()
{
  on_actionDeleteSelection_triggered();
}

void LithoGraphX::on_Viewer_setLabelColor(QIcon& icon)
{
  ui.actionLabelColor->setIcon(icon);
}

void LithoGraphX::on_Viewer_changeSceneRadius(float radius)
{
  resetClipControl(radius);
}

void LithoGraphX::on_actionAddNewSeed_triggered(bool on)
{
  if(on)
    ImgData::tool = ImgData::ST_ADD_NEW_SEED;
}

void LithoGraphX::on_actionAddCurrentSeed_triggered(bool on)
{
  if(on)
    ImgData::tool = ImgData::ST_ADD_CURRENT_SEED;
}

void LithoGraphX::on_actionPickLabel_triggered(bool on)
{
  if(on)
    ImgData::tool = ImgData::ST_PICK_LABEL;
}

void LithoGraphX::on_actionGrabSeed_triggered(bool on)
{
  if(on)
    ImgData::tool = ImgData::ST_GRAB_SEED;
}

void LithoGraphX::on_actionFillLabel_triggered(bool on)
{
  if(on)
    ImgData::tool = ImgData::ST_FILL_LABEL;
}

void LithoGraphX::on_actionMeshSelect_triggered(bool on)
{
  if(on)
    ImgData::tool = ImgData::ST_MESH_SELECT;
}

void LithoGraphX::on_actionLabelSelect_triggered(bool on)
{
  if(on)
    ImgData::tool = ImgData::ST_SELECT_LABEL;
}

void LithoGraphX::on_actionConnectedSelect_triggered(bool on)
{
  if(on)
    ImgData::tool = ImgData::ST_SELECT_CONNECTED;
}

void LithoGraphX::on_actionPixelEdit_triggered(bool on)
{
  if(on)
    ImgData::tool = ImgData::ST_PIXEL_EDIT;
}

void LithoGraphX::on_actionPickVolLabel_triggered(bool on)
{
  if(on)
    ImgData::tool = ImgData::ST_PICK_3D_LABEL;
}

void LithoGraphX::on_actionDeletePickedLabel_triggered(bool on)
{
  if(on)
    ImgData::tool = ImgData::ST_DELETE_LABEL;
}

void LithoGraphX::on_actionDeleteVolumeLabel_triggered(bool on)
{
  if(on)
    ImgData::tool = ImgData::ST_DELETE_3D_LABEL;
}

void LithoGraphX::on_actionFillVolumeLabel_triggered(bool on)
{
  if(on)
    ImgData::tool = ImgData::ST_FILL_3D_LABEL;
}

void LithoGraphX::on_actionEraseSelection_triggered()
{
  QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));
  if(activeMesh() == 0)
    _stack1->fillSelect(0);
  else if(activeMesh() == 1)
    _stack2->fillSelect(0);
  ui.Viewer->update2D();
  QApplication::restoreOverrideCursor();
}

void LithoGraphX::on_actionFillSelection_triggered()
{
  QApplication::setOverrideCursor(QCursor(Qt::WaitCursor));
  if(activeMesh() == 0)
    _stack1->fillSelect(ui.Viewer->_selectedLabel);
  else if(activeMesh() == 1)
    _stack2->fillSelect(ui.Viewer->_selectedLabel);
  ui.Viewer->update2D();
  QApplication::restoreOverrideCursor();
}

void LithoGraphX::on_actionDeleteSelection_triggered()
{
  process::ParmList parms;
  if(!process::getLastParms("Mesh", "Delete Selection", parms)) {
    QMessageBox::critical(this, "Error deleting mesh selection",
                          "There is no Mesh process named 'Delete Selection'");
    return;
  }
  launchProcess("Mesh", "Delete Selection", parms, true, true);
}

void LithoGraphX::on_actionColorPalette_triggered()
{
  ColorEditorDlg dlg(Colors::instance(), this);
  dlg.exec();
}

void LithoGraphX::on_actionEditLabels_triggered(bool on)
{
  if(_labelEditorDlg.isNull()) {
    this->_labelEditorDlg = new LabelEditorDlg(&ImgData::LabelColors, this);
    auto labelEditorDlg = this->_labelEditorDlg.data();
    labelEditorDlg->setCurrentLabel(ui.Viewer->_selectedLabel);
    connect(ui.Viewer, &LithoViewer::selectLabelChanged, labelEditorDlg, &LabelEditorDlg::setCurrentLabel);
    connect(labelEditorDlg, &LabelEditorDlg::update, ui.Viewer, &LithoViewer::UpdateLabels);
    connect(labelEditorDlg, &LabelEditorDlg::finished, ui.actionEditLabels, &QAction::toggle);
    connect(labelEditorDlg, &LabelEditorDlg::labelSelected, ui.Viewer, &LithoViewer::selectMeshLabel);
    connect(labelEditorDlg, &LabelEditorDlg::currentLabelChosen, ui.Viewer, &LithoViewer::setLabel);
  }
  _labelEditorDlg->setVisible(on);
}

void LithoGraphX::on_actionSwapSurfaces_triggered()
{
  if((ui.Stack1SurfShow->isChecked() and !ui.Stack2SurfShow->isChecked())
     or (!ui.Stack1SurfShow->isChecked() and ui.Stack2SurfShow->isChecked())) {
    ui.Stack1SurfShow->setChecked(not ui.Stack1SurfShow->isChecked());
    ui.Stack2SurfShow->setChecked(not ui.Stack2SurfShow->isChecked());
    if(ui.Stack1SurfShow->isChecked())
      ui.StackTabs->setCurrentIndex(0);
    else if(ui.Stack2SurfShow->isChecked())
      ui.StackTabs->setCurrentIndex(1);
  } else if((ui.Stack1MeshShow->isChecked() and !ui.Stack2MeshShow->isChecked())
            or (!ui.Stack1MeshShow->isChecked() and ui.Stack2MeshShow->isChecked())) {
    // Mesh version added for Gerardo
    ui.Stack1MeshShow->setChecked(not ui.Stack1MeshShow->isChecked());
    ui.Stack2MeshShow->setChecked(not ui.Stack2MeshShow->isChecked());
    if(ui.Stack1MeshShow->isChecked())
      ui.StackTabs->setCurrentIndex(0);
    else if(ui.Stack2MeshShow->isChecked())
      ui.StackTabs->setCurrentIndex(1);
  }
  ui.Viewer->update3D();
}

void LithoGraphX::on_actionEditParms_triggered()
{
  if(projectFile().isEmpty()) {
    QMessageBox::information(
      this, "Cannot edit parameters",
      "To edit the parameter file, you first need to create one. Save the current parameters in File->Save");
    return;
  }
  writeParms();
  QFile file(projectFile());
  if(file.open(QIODevice::ReadOnly)) {
    QTextStream stream(&file);
    stream.setCodec("UTF-8");
    ui_editParmsDialog->Parms->setPlainText(stream.readAll());
  }
  file.close();

  if(_editParmsDialog->exec() == QDialog::Accepted) {
    if(!file.open(QIODevice::WriteOnly | QIODevice::Text)) {
      SETSTATUS("EditParms::Error:Cannot open output file:" << projectFile());
      return;
    }
    QTextStream stream(&file);
    stream.setCodec("UTF-8");
    stream << ui_editParmsDialog->Parms->toPlainText();
    file.close();
    SETSTATUS("Saved parameters to file:" << projectFile());
  }
  saveProcessesParameters();
  readParms();
  loadControls();
  recallProcessesParameters();
  reloadTasks();
  ui.Viewer->update3D();
}

void LithoGraphX::editParmsSaveAs()
{
  // Be sure to grab current process parameters
  // processCommandGetParmValues(ImgData::ProcessCommandIndex);

  // Write to file
  QString filename
    = QFileDialog::getSaveFileName(this, QString("Select file to save parmaters to "), projectFile(),
                                   QString("Parameter files *.mgxv"), 0, QFileDialog::DontUseNativeDialog);
  if(filename == "")
    return;

  QFile file(filename);
  if(!file.open(QIODevice::WriteOnly | QIODevice::Text)) {
    SETSTATUS("EditParmsSaveAs::Error:Cannot open output file:" << filename);
    return;
  }
  QTextStream stream(&file);
  stream << ui_editParmsDialog->Parms->toPlainText();
  file.close();
  SETSTATUS("Saved parameters to file:" << projectFile());
}

void LithoGraphX::on_ProcessTabs_currentChanged(int tab)
{
  QString type = ui.ProcessTabs->tabText(tab);
  if(type == "Stack") {
    if(ui.ProcessStackCommand->topLevelItemCount() == 0)
      on_ProcessStackCommand_currentItemChanged(0, 0);
    else
      on_ProcessStackCommand_currentItemChanged(ui.ProcessStackCommand->currentItem(), 0);
  } else if(type == "Mesh") {
    if(ui.ProcessMeshCommand->topLevelItemCount() == 0)
      on_ProcessMeshCommand_currentItemChanged(0, 0);
    else
      on_ProcessMeshCommand_currentItemChanged(ui.ProcessMeshCommand->currentItem(), 0);
  } else if(type == "Global") {
    if(ui.ProcessGlobalCommand->topLevelItemCount() == 0)
      on_ProcessGlobalCommand_currentItemChanged(0, 0);
    else
      on_ProcessGlobalCommand_currentItemChanged(ui.ProcessGlobalCommand->currentItem(), 0);
  } else if(type == "Tasks") {
    if(ui.ProcessTasksCommand->topLevelItemCount() == 0)
      on_ProcessTasksCommand_currentItemChanged(0, 0);
    else
      on_ProcessTasksCommand_currentItemChanged(ui.ProcessTasksCommand->currentItem(), 0);
  }
  ui.ProcessParameters->resizeColumnToContents(0);
}

void LithoGraphX::on_StackTabs_currentChanged(int)
{ updateCurrentStack(); }

void LithoGraphX::on_Stack1MainShow_toggled(bool on)
{
  _stack1->showMain(on);
  updateCurrentStack();
}

void LithoGraphX::on_Stack1WorkShow_toggled(bool on)
{
  _stack1->showWork(on);
  updateCurrentStack();
}

void LithoGraphX::on_Stack1SurfShow_toggled(bool on)
{
  _stack1->showSurf(on);
  updateCurrentStack();
}

void LithoGraphX::on_Stack1MeshShow_toggled(bool on)
{
  _stack1->showMesh(on);
  updateCurrentStack();
}

void LithoGraphX::on_Stack1WorkLabels_toggled(bool on)
{
  _stack1->setWorkLabels(on);
  updateFillWorkOptions();
}

void LithoGraphX::on_Stack2MainShow_toggled(bool on)
{
  _stack2->showMain(on);
  updateCurrentStack();
}

void LithoGraphX::on_Stack2WorkShow_toggled(bool on)
{
  _stack2->showWork(on);
  updateCurrentStack();
}

void LithoGraphX::on_Stack2SurfShow_toggled(bool on)
{
  _stack2->showSurf(on);
  updateCurrentStack();
}

void LithoGraphX::on_Stack2MeshShow_toggled(bool on)
{
  _stack2->showMesh(on);
  updateCurrentStack();
}

void LithoGraphX::on_Stack2WorkLabels_toggled(bool on)
{
  _stack2->setWorkLabels(on);
  updateFillWorkOptions();
}

void LithoGraphX::updateCurrentStack()
{
  QString tabText = ui.StackTabs->tabText(ui.StackTabs->currentIndex());
  _currentSetup->setCurrentMeshId(-1);
  _currentSetup->setCurrentStackId(-1);
  if(tabText == "Stack 1") {
    _currentSetup->setCurrentStackId(0);
    _currentSetup->setCurrentMeshId(0);
    if(ui.Stack1WorkShow->isChecked()) {
      ActiveStack->setText("Work Stack 1 Active");
    } else if(ui.Stack1MainShow->isChecked()) {
      ActiveStack->setText("Main Stack 1 Active");
    } else {
      ActiveStack->setText("Stack 1 Active");
    }
  } else if(tabText == "Stack 2") {
    _currentSetup->setCurrentStackId(1);
    _currentSetup->setCurrentMeshId(1);
    if(ui.Stack2WorkShow->isChecked()) {
      ActiveStack->setText("Work Stack 2 Active");
    } else if(ui.Stack2MainShow->isChecked()) {
      ActiveStack->setText("Main Stack 2 Active");
    } else {
      ActiveStack->setText("Stack 2 Active");
    }
  } else {
    ActiveStack->setText("Error -- Invalid Stack Tab selected");
  }
  updateFillWorkOptions();
}

void LithoGraphX::on_ProcessStackCommand_currentItemChanged(QTreeWidgetItem* current, QTreeWidgetItem*)
{
  if(current == 0 or current->childCount() > 0) {
    _parmsModel->clear();
    _currentProcessName.clear();
    _currentProcessType.clear();
  } else {
    _currentProcessType = "Stack";
    _currentProcessInTasks = false;
    QString name = current->text(0);
    _currentProcessName = name;
    _parmsModel->setParms(process::pluginManager()->processDefinition<StackProcess>(name));
  }
}

void LithoGraphX::on_ProcessMeshCommand_currentItemChanged(QTreeWidgetItem* current, QTreeWidgetItem*)
{
  if(current == 0 or current->childCount() > 0) {
    _parmsModel->clear();
    _currentProcessName.clear();
    _currentProcessType.clear();
  } else {
    _currentProcessType = "Mesh";
    _currentProcessInTasks = false;
    QString name = current->text(0);
    _currentProcessName = name;
    _parmsModel->setParms(process::pluginManager()->processDefinition<MeshProcess>(name));
  }
}

void LithoGraphX::on_ProcessGlobalCommand_currentItemChanged(QTreeWidgetItem* current, QTreeWidgetItem*)
{
  if(current == 0 or current->childCount() > 0) {
    _parmsModel->clear();
    _currentProcessName.clear();
    _currentProcessType.clear();
  } else {
    _currentProcessType = "Global";
    _currentProcessInTasks = false;
    QString name = current->text(0);
    _currentProcessName = name;
    _parmsModel->setParms(process::pluginManager()->processDefinition<GlobalProcess>(name));
  }
}

void LithoGraphX::on_ProcessStackFilter_textChanged(const QString& text)
{
  process_util::filterProcesses(ui.ProcessStackCommand, text);
}

void LithoGraphX::on_ProcessMeshFilter_textChanged(const QString& text)
{
  process_util::filterProcesses(ui.ProcessMeshCommand, text);
}

void LithoGraphX::on_ProcessGlobalFilter_textChanged(const QString& text)
{
  process_util::filterProcesses(ui.ProcessGlobalCommand, text);
}

void LithoGraphX::on_ProcessTasksCommand_currentItemChanged(QTreeWidgetItem* current, QTreeWidgetItem*)
{
  if(current == 0) {
    _parmsModel->clear();
    _currentProcessName.clear();
    _currentProcessType.clear();
  } else {
    bool found = false;
    if(current->flags() & Qt::ItemIsEnabled) {
      _currentProcessName = current->text(0);
      _currentProcessType = current->text(1);
      _currentProcessInTasks = true;
      QString task = current->data(0, TaskNameRole).toString();
      int proc_num = current->data(0, ProcessPositionRole).toInt();
      if(_tasks.contains(task)) {
        const QList<TypedProcessDefinition>& defs = _tasks[task];
        if(proc_num < defs.size()) {
          const TypedProcessDefinition& def = defs[proc_num];
          _parmsModel->setParms(def);
          found = true;
        }
      }
    }
    if(!found) {
      _parmsModel->clear();
      _currentProcessName.clear();
      _currentProcessType.clear();
    }
  }
}

void LithoGraphX::on_ProcessCommandGo_clicked()
{
  if(_currentProcessName.isEmpty()) {
    QMessageBox::warning(this, "Process Command", "Please, select a process before trying to launch it");
    return;
  }
  QString processType = _currentProcessType;
  process::ParmList parms = _parmsModel->parms();
  if(launchProcess(processType, _currentProcessName, parms)) {
    _parmsModel->setParms(parms);
  }
}

bool LithoGraphX::launchProcess(const QString& processType, const QString& processName, process::ParmList& parms,
                                 bool useGUI, bool saveDefaults)
{
  if(_processThread) {
    // QMessageBox::critical(this, QString("Error starting process %1.%2").arg(processType).arg(processName),
    //      QString("The process '%1' is already running. Please wait for the process to finish before starting
    //      another one").arg(_currentProcess->name()));
    return false;
  }
  size_t expectedParms = std::numeric_limits<size_t>::max();
  if(!process::checkProcessParms(processType, processName, parms, &expectedParms)) {
    if(expectedParms == std::numeric_limits<size_t>::max())
      QMessageBox::critical(this, "Error starting process",
                            QString("There is no %1 process named '%2'").arg(processType).arg(processName));
    else
      QMessageBox::critical(this, "Error starting process",
                            QString("The arguments provided for the process [%3]%4 are not valid: expected %1 "
                                    "parameters, but %2 were provided")
                            .arg(expectedParms)
                            .arg(parms.size())
                            .arg(processType)
                            .arg(processName));
    return false;
  }

  updateCurrentStack();
  _currentSetup->resetModified();

  try {
    _currentProcess = _currentSetup->makeProcess(processType, processName);
  } catch(std::exception&) {
    QMessageBox::critical(this, "Error finding process",
                          QString("There is no %1 process named '%2'").arg(processType.toLower()).arg(processName));
    return false;
  }

  // Call the process initialize()
  if(useGUI) {
    try {
      if(not _currentProcess->initialize(parms, this)) {
        if(_currentProcess->errorMessage().isEmpty())
          SETSTATUS("Cancelled during initialize");
        else
          QMessageBox::critical(this, "Error during process configuration", _currentProcess->errorMessage());
        return false;
      }
      bool valid;
      if(saveDefaults)
        valid = process::saveDefaultParms(processType, processName, parms);
      else
        valid = process::checkProcessParms(processType, processName, parms);
      if(not valid) {
        QMessageBox::critical(this, "Error during process configuration",
                              "The configuration process didn't produce a valid set of parameters");
        return false;
      }
    }
    catch(const QString& str) {
      QMessageBox::critical(this, "Error during process configuration", str);
      return false;
    }
    catch(const std::string& str) {
      QMessageBox::critical(this, "Error during process configuration", QString::fromStdString(str));
      return false;
    }
    catch(const std::exception& ex) {
      QMessageBox::critical(this, "Error during process configuration", QString::fromLocal8Bit(ex.what()));
      return false;
    }
    catch(...) {
      QMessageBox::critical(this, "Error during process configuration", "Unknown exception during configuration");
      return false;
    }
  }

  // Save the current command in LithoGraphX.py, but only if it already exists
  if(QFile::exists("LithoGraphX.py")) {
    QFile file("LithoGraphX.py");
    if(file.open(QIODevice::WriteOnly | QIODevice::Append | QIODevice::Text)) {
      QTextStream ss(&file);
      _currentProcess->p->currentPythonCall = _currentProcess->pythonCall(parms);
      ss << _currentProcess->p->currentPythonCall << endl;
      file.close();
    } else
      Information::err << "Warning, cannot open LithoGraphX.py for writing" << endl;
  } else
    Information::err << "Warning, no LithoGraphX.py file in current folder" << endl;

  // Free cuda memory
  freeMemGPU();

  lgx::GlobalProgress::useModeless();
  lgx::GlobalProgress::instance().start(QString("Running %1.%2 ...").arg(processType).arg(processName), 0, false);
  _processThread = new ProcessThread(_currentProcess.get(), parms, this);
  connect(_processThread.data(), &ProcessThread::finished, this, &LithoGraphX::processCommandFinished);
  _currentProcessTime.start();
  _processThread->start();
  // this->setDisabled(true);
  ui.ControlsTab->setDisabled(true);
  ui.menubar->setDisabled(true);
  ui.volumeToolbar->setDisabled(true);
  ui.miscToolbar->setDisabled(true);
  ui.meshToolbar->setDisabled(true);
  return true;
}

void LithoGraphX::processCommandFinished()
{
  // Reserve cuda Memory
  holdMemGPU();

  int msec = _currentProcessTime.elapsed();
  QString nm = _currentProcess->name();
  nm.replace(" ", "_");
  Information::out << _currentProcess->type() << "." << nm << " Process running time: " << (msec / 1000.0) << " s."
                   << endl;

  // this->setEnabled(true);
  ui.ControlsTab->setEnabled(true);
  ui.menubar->setEnabled(true);
  ui.volumeToolbar->setEnabled(true);
  ui.miscToolbar->setEnabled(true);
  ui.meshToolbar->setEnabled(true);
  bool success = _processThread->exitStatus();
  if(success) {
    if(!_processThread->warningMessage().isEmpty())
      QMessageBox::information(this, QString("Warning running process %1").arg(_currentProcess->name()),
                               _processThread->warningMessage());
    // updateStateFromProcess(_currentProcess);
  } else {
    lgx::GlobalProgress::clear();
    QMessageBox::critical(this, QString("Error running process %1").arg(_currentProcess->name()),
                          _processThread->errorMessage());
  }
  // Moved to here, if a process partially completes, we still may need to update things.
  updateStateFromProcess(_currentProcess.get());
  ui.Viewer->update2D();
  cleanProcess();
  lgx::GlobalProgress::clear();
  _currentSetup->p->currentPythonCall.clear();
  if(success)
    emit processFinished();
  else
    emit processFailed();
}

void LithoGraphX::cleanProcess()
{
  lgx::GlobalProgress::useModal();
  delete _processThread.data();
  _currentProcess.reset();
}

void LithoGraphX::updateStateFromProcess(process::Process* proc)
{
  QMutexLocker locker(&proc->p->lock);
  QTime updateTime;
  updateTime.start();
  // First, check the stores and stacks
  Store* mainStore1 = 0, *mainStore2 = 0, *workStore1 = 0, *workStore2 = 0;
  Mesh* mesh1 = 0, *mesh2 = 0;
  Stack* stack1 = 0, *stack2 = 0;
  for(int i = 0; i < proc->stackCount(); ++i) {
    Stack* stack = proc->stack(i);
    if(stack->id() == 0) {
      stack1 = stack;
      mainStore1 = stack->main();
      workStore1 = stack->work();
    } else if(stack->id() == 1) {
      stack2 = stack;
      mainStore2 = stack->main();
      workStore2 = stack->work();
    }
  }
  for(int i = 0; i < proc->meshCount(); ++i) {
    Mesh* mesh = proc->mesh(i);
    if(mesh->id() == 0)
      mesh1 = mesh;
    else if(mesh->id() == 1)
      mesh2 = mesh;
  }
  if(stack1) {
    _stack1->stack = stack1;
    _stack1->updateStackSize();
    ui.Stack1Scale_X->setValue(_stack1->toSliderScale(stack1->scale().x()));
    ui.Stack1Scale_Y->setValue(_stack1->toSliderScale(stack1->scale().y()));
    ui.Stack1Scale_Z->setValue(_stack1->toSliderScale(stack1->scale().z()));

    if(mainStore1->data().size() != stack1->storeSize()) {
      mainStore1->reset();
      mainStore1->changed();
    }
    if(mainStore1->wasChanged()) {
      mainStore1->updateHistogram();
      _stack1->reloadMainTex(mainStore1->changedBBox());
    }
    if(mainStore1->transferFunctionChanged()) {
      _stack1->updateMainColorMap();
    }

    if(workStore1->data().size() != stack1->storeSize()) {
      SETSTATUS("Warning, work store 1 is not of the right size: deleting");
      workStore1->reset();
      workStore1->changed();
    }
    if(workStore1->wasChanged()) {
      workStore1->updateHistogram();
      _stack1->reloadWorkTex(workStore1->changedBBox());
    }
    if(workStore1->transferFunctionChanged()) {
      _stack1->updateWorkColorMap();
    }

    ui.Stack1MainShow->setChecked(mainStore1->isVisible());
    ui.Stack1WorkShow->setChecked(workStore1->isVisible());

    ui.Stack1ShowTrans->setChecked(stack1->showTrans());
    ui.Stack1ShowBBox->setChecked(stack1->showBBox());
    ui.Stack1ShowScale->setChecked(stack1->showScale());
    ui.Stack1TieScales->setChecked(stack1->tieScales());

    ui.Stack1MainLabels->setChecked(mainStore1->labels());
    ui.Stack1WorkLabels->setChecked(workStore1->labels());

    ui.Stack1MainBright->setValue(mainStore1->brightness() * 10000);
    ui.Stack1MainOpacity->setValue(mainStore1->opacity() * 10000);
    ui.Stack1WorkBright->setValue(workStore1->brightness() * 10000);
    ui.Stack1WorkOpacity->setValue(workStore1->opacity() * 10000);
  }

  if(stack2) {
    _stack2->stack = stack2;
    _stack2->updateStackSize();
    ui.Stack2Scale_X->setValue(_stack2->toSliderScale(stack2->scale().x()));
    ui.Stack2Scale_Y->setValue(_stack1->toSliderScale(stack2->scale().y()));
    ui.Stack2Scale_Z->setValue(_stack1->toSliderScale(stack2->scale().z()));

    if(mainStore2->data().size() != stack2->storeSize()) {
      SETSTATUS("Warning, main store 2 is not of the right size: deleting");
      mainStore2->reset();
      mainStore2->changed();
    }

    if(workStore2->data().size() != stack2->storeSize()) {
      SETSTATUS("Warning, work store 2 is not of the right size: deleting");
      workStore2->reset();
      workStore2->changed();
    }

    if(mainStore2->wasChanged()) {
      mainStore2->updateHistogram();
      _stack2->reloadMainTex(mainStore2->changedBBox());
    }
    if(mainStore2->transferFunctionChanged()) {
      _stack2->updateMainColorMap();
    }

    if(workStore2->wasChanged()) {
      workStore2->updateHistogram();
      _stack2->reloadWorkTex(workStore2->changedBBox());
    }
    if(workStore2->transferFunctionChanged()) {
      _stack2->updateWorkColorMap();
    }

    ui.Stack2MainShow->setChecked(mainStore2->isVisible());
    ui.Stack2WorkShow->setChecked(workStore2->isVisible());

    ui.Stack2ShowTrans->setChecked(stack2->showTrans());
    ui.Stack2ShowBBox->setChecked(stack2->showBBox());
    ui.Stack2ShowScale->setChecked(stack2->showScale());
    ui.Stack2TieScales->setChecked(stack2->tieScales());

    ui.Stack2MainLabels->setChecked(mainStore2->labels());
    ui.Stack2WorkLabels->setChecked(workStore2->labels());

    ui.Stack2MainBright->setValue(mainStore2->brightness() * 10000);
    ui.Stack2MainOpacity->setValue(mainStore2->opacity() * 10000);
    ui.Stack2WorkBright->setValue(workStore2->brightness() * 10000);
    ui.Stack2WorkOpacity->setValue(workStore2->opacity() * 10000);
  }

  // Then the meshes
  if(mesh1) {
    _stack1->mesh = mesh1;
    if(mesh1->changes() & ImgData::RELOAD_VBO) {
      _stack1->chkGraph();
      _stack1->fillVBOs();
      _stack1->updateSelection();
    } else {
      if(mesh1->changes() & ImgData::RELOAD_POS)
        _stack1->updatePos();
      if(mesh1->changes() & ImgData::RELOAD_LINES)
        _stack1->updateLines();
      if(mesh1->changes() & ImgData::RELOAD_TRIS)
        _stack1->updateTris();
      if(mesh1->changes() & ImgData::UPDATE_SELECTION)
        _stack1->updateSelection();
    }

    if(mesh1->hasImgTex() and not mesh1->imgTex().isNull()) {
      _stack1->loadImgTex(mesh1->imgTex());
    }

    if(mesh1->surfFctChanged())
      _stack1->updateSurfColorMap();
    if(mesh1->heatFctChanged())
      _stack1->updateHeatColorMap();
    ui.Stack1SurfCull->setChecked(mesh1->culling());
    ui.Stack1SurfBlend->setChecked(mesh1->blending());

    ui.Stack1MeshShow->setChecked(mesh1->isMeshVisible());
    ui.Stack1SurfShow->setChecked(mesh1->isSurfaceVisible());
    ui.Stack1SurfView->setCurrentIndex((int)mesh1->toShow());
    switch(mesh1->coloring()) {
    case SurfaceColor::Signal:
      ui.Stack1SurfSignal->setChecked(true);
      break;
    case SurfaceColor::Texture:
      ui.Stack1SurfTexture->setChecked(true);
      break;
    case SurfaceColor::Image:
      ui.Stack1SurfImage->setChecked(true);
      break;
    }

    ui.Stack1MeshView->setCurrentIndex((int)mesh1->meshView());

    ui.Stack1MeshLines->setChecked(mesh1->showMeshLines());
    ui.Stack1MeshPoints->setChecked(mesh1->showMeshPoints());
    ui.Stack1CellMap->setChecked(mesh1->showMeshCellMap());

    ui.Stack1SurfBright->setValue(mesh1->brightness() * 10000);
    ui.Stack1SurfOpacity->setValue(mesh1->opacity() * 10000);
  }

  if(mesh2) {
    _stack2->mesh = mesh2;
    if(mesh2->changes() & ImgData::RELOAD_VBO) {
      _stack2->chkGraph();
      _stack2->fillVBOs();
      _stack2->updateSelection();
    } else {
      if(mesh2->changes() & ImgData::RELOAD_POS)
        _stack2->updatePos();
      if(mesh2->changes() & ImgData::RELOAD_LINES)
        _stack2->updateLines();
      if(mesh2->changes() & ImgData::RELOAD_TRIS)
        _stack2->updateTris();
      if(mesh2->changes() & ImgData::UPDATE_SELECTION)
        _stack2->updateSelection();
    }

    if(mesh2->hasImgTex() and not mesh2->imgTex().isNull()) {
      _stack2->loadImgTex(mesh2->imgTex());
    }

    if(mesh2->surfFctChanged())
      _stack2->updateSurfColorMap();
    if(mesh2->heatFctChanged())
      _stack2->updateHeatColorMap();
    ui.Stack2SurfCull->setChecked(mesh2->culling());
    ui.Stack2SurfBlend->setChecked(mesh2->blending());

    ui.Stack2MeshShow->setChecked(mesh2->isMeshVisible());
    ui.Stack2SurfShow->setChecked(mesh2->isSurfaceVisible());
    ui.Stack2SurfView->setCurrentIndex((int)mesh2->toShow());
    switch(mesh2->coloring()) {
    case SurfaceColor::Signal:
      ui.Stack2SurfSignal->setChecked(true);
      break;
    case SurfaceColor::Texture:
      ui.Stack2SurfTexture->setChecked(true);
      break;
    case SurfaceColor::Image:
      ui.Stack2SurfImage->setChecked(true);
      break;
    }
    ui.Stack2MeshView->setCurrentIndex((int)mesh2->meshView());

    ui.Stack2MeshLines->setChecked(mesh2->showMeshLines());
    ui.Stack2MeshPoints->setChecked(mesh2->showMeshPoints());
    ui.Stack2CellMap->setChecked(mesh2->showMeshCellMap());

    ui.Stack2SurfBright->setValue(mesh2->brightness() * 10000);
    ui.Stack2SurfOpacity->setValue(mesh2->opacity() * 10000);
  }

  // Update from cutting surface
  CuttingSurface* cut = _currentSetup->cuttingSurface();
  ui.DrawCutSurf->setChecked(cut->isVisible());
  switch(cut->mode()) {
  case CuttingSurface::THREE_AXIS:
    ui.ThreeAxis->setChecked(true);
    break;
  case CuttingSurface::PLANE:
    ui.CutSurfPlane->setChecked(true);
    break;
  case CuttingSurface::BEZIER:
    ui.CutSurfBezier->setChecked(true);
    break;
  }

  // Find the bounding box of all images
  BoundingBox3f bbox(Point3f(FLT_MAX, FLT_MAX, FLT_MAX), Point3f(-FLT_MAX, -FLT_MAX, -FLT_MAX));
  forall(Stack* stack, proc->stacks()) {
    Point3f worldSize = multiply(Point3f(stack->size()), stack->step());
    bbox |= worldSize;
  }
  forall(Mesh* mesh, proc->meshes()) {
    if(!mesh->empty()) {
      const BoundingBox3f& b = mesh->boundingBox();
      bbox |= b;
    }
  }
  Point3f rbox;
  if(bbox[0].x() > bbox[1].x())
    rbox = Point3f(0.f);
  else
    rbox = max(fabs(bbox[0]), fabs(bbox[1]));
  ui.Viewer->setSceneBoundingBox(rbox);
  _cutSurf.setSceneBoundingBox(rbox);
  ui.CutSurfGrid->setChecked(cut->drawGrid());
  ui.CutSurfBlend->setChecked(cut->blending());
  ui.CutSurfBright->setValue(cut->brightness()*10000);
  ui.CutSurfOpacity->setValue(cut->opacity()*10000);
  ui.CutSurfPoints->setChecked(cut->showPoints());
  setCutSurfControl();

  // Update global brightness and contrast
  ui.Viewer->setProperty("globalContrast", proc->globalContrast());
  ui.Viewer->setProperty("globalBrightness", proc->globalBrightness());

  resetClipControl(ui.Viewer->getSceneRadius());

  setWindowTitle("LithoGraphX - " + _currentSetup->actingFile());

  _currentSetup->resetModified();
  updateCurrentStack();
  int msec = updateTime.elapsed();
  Information::out << "updateStateFromProcess() running time: " << (msec / 1000.) << " s." << endl;
  ui.Viewer->setLabel(proc->selectedLabel());
}

const QString& LithoGraphX::projectFile() const {
  return _currentSetup->file();
}

void LithoGraphX::setProjectFile(const QString& f)
{
  QString filename = f;
  if(!filename.isEmpty())
    filename = util::absoluteFilePath(filename);
  _currentSetup->setFile(filename);
}

void LithoGraphX::processSystemCommand(process::Process* proc, process::SystemCommand command,
                                       const process::ParmList& parms)
{
  switch(command) {
  case process::LOAD_PROJECT: {
    if(parms.size() != 1)
      break;
    setProjectFile(parms[0].toString());

    // Save the process parameters
    saveProcessesParameters();

    // Load the project
    readParms();

    recallProcessesParameters();
    loadControls();
    reloadTasks();

    ui.Viewer->update3D();
    // ui.Viewer->showEntireScene();

    proc->p->success = true;
  } break;
  case process::SAVE_PROJECT: {
    if(parms.size() != 1)
      break;
    setProjectFile(parms[0].toString());
    saveSettings();

    updateStateFromProcess(proc);

    // Save the process parameters
    saveProcessesParameters();

    // Load the project
    writeParms();

    ui.Viewer->update2D();
    proc->p->success = true;
  } break;
  case process::UPDATE_STRUCTURE: {
    updateStateFromProcess(proc);
    proc->p->success = true;
  } break;
  case process::SET_CURRENT_STACK: {
    lgx::STORE which_store = process::stringToStore(parms[0].toString());
    int id = parms[1].toInt();
    if(id < ui.StackTabs->count()) {
      proc->p->current_stack = id;
      proc->p->current_mesh = id;
      auto stk = proc->p->_stacks[id];
      stk->setCurrent(which_store);
      ui.StackTabs->setCurrentIndex(id);
      switch(id) {
      case 0:
        ui.Stack1MainShow->setChecked(stk->main()->isVisible());
        ui.Stack1WorkShow->setChecked(stk->work()->isVisible());
        break;
      case 1:
        ui.Stack2MainShow->setChecked(stk->main()->isVisible());
        ui.Stack2WorkShow->setChecked(stk->work()->isVisible());
        break;
      }
      proc->p->success = true;
    }
  } break;
  case process::RESET_PROJECT:
    setProjectFile(QString());
    proc->p->success = true;
    break;
  case process::TAKE_SNAPSHOT: {
    updateStateFromProcess(proc);
    ui.Viewer->update3D();
    // Force event processing
    QCoreApplication::flush();
    QString filename = parms[0].toString();
    bool expand_frustum = process::parmToBool(parms[1]);
    int w = parms[2].toInt();
    int h = parms[3].toInt();
    if(w == 0)
      w = ui.Viewer->width();
    if(h == 0)
      h = ui.Viewer->height();
    float over = parms[5].toFloat();
    Information::out << "w = " << w << endl << "h = " << h << endl;
    QString error;
    proc->p->success = ui.Viewer->saveImageSnapshot(filename, QSize(w, h), over, expand_frustum, false, &error);
    if(!proc->p->success)
      proc->setErrorMessage(error);
  }
  default:
    break;
  }
  proc->p->updated.wakeAll();
}

bool LithoGraphX::canAutoOpen(const QString& filename) const
{
  static QStringList recognisedSuffix =
    QStringList() << "mgxs"
                  << "mgxv"
                  << "mgxm"
                  << "lgxs"
                  << "lgxm"
                  << "lgxp"
                  << "lgxpb"
                  << "lgxps"
                  << "fct"
                  << "inr"
                  << "jpg"
                  << "jpeg"
                  << "png"
                  << "pnm"
                  << "sgi"
                  << "tif"
                  << "tiff"
                  << "ply"
                  << "mesh"
                  << "vtu"
                  << "lif"
                  << "lsm"
                  << "py";

  QFileInfo fi(filename);
  QString ext = fi.suffix().toLower();
  return (fi.exists() and fi.isReadable() and recognisedSuffix.contains(ext)) or fi.isDir();
}

/*
 *void LithoGraphX::dragEnterEvent(QDragEnterEvent* event)
 *{
 *
 *  //if(DEBUG)
 *    Information::out << "dragEnterEvent" << endl
 *                     << "  keyboardModifiers = " << modifierString(QGuiApplication::keyboardModifiers()) << endl
 *                     << "  dropAction = " << event->proposedAction() << endl;
 *
 *  event->setDropAction(Qt::CopyAction);
 *
 *  const QMimeData* mime = event->mimeData();
 *  Information::out << "formats: " << mime->formats().join(", ") << endl;
 *  if(mime->hasUrls()) {
 *    QList<QUrl> urls = mime->urls();
 *    if(urls.size() == 1) {
 *      QUrl url = urls[0];
 *      DEBUG_OUTPUT("Url dragged: " << url.toString() << endl);
 *      QFileInfo fi(url.toLocalFile());
 *      QString ext = fi.suffix().toLower();
 *      if(fi.exists() and fi.isReadable() and recognisedSuffix.contains(ext)) {
 *        event->accept();
 *        return;
 *      } else {
 *        DEBUG_OUTPUT("Wrong type of files" << endl);
 *      }
 *    } else {
 *      DEBUG_OUTPUT("Too many files" << endl);
 *    }
 *  } else {
 *    DEBUG_OUTPUT("No urls" << endl);
 *  }
 *  event->ignore();
 *}
 */

void LithoGraphX::autoOpen(const QString& filename, int stack, bool main)
{
  disconnect(this, SIGNAL(processFinished()), this, SLOT(autoOpen()));
  Information::out << "Dropping file " << filename << endl;
  QString pth = (filename.isEmpty() ? _loadFilename : filename);
  QFileInfo fi(pth);
  QString ext = fi.suffix().toLower();
  if(ext == "mgxv" or ext == "lgxp") {
    loadProject(pth, false);
  } else {
    DEBUG_OUTPUT("Filename:" << pth << endl);
    process::ParmList parms;
    static const QStringList load_mesh = { "ply", "vtu", "mesh", "obj", "mgxm", "txt" };
    static const QStringList image_itk = { "lif", "lsm" };
    static const QStringList stack_open = { "mgxs", "inr", "tif", "tiff", "png", "jpg", "jpeg", "pnm", "sgi" };
    static const QStringList pkg_ext = { "lgxpb", "lgxps" };
    if(load_mesh.contains(ext)) {
      parms << pth
            << false
            << false
            << stack
            << false;
      launchProcess("Mesh", "Load", parms, false);
    } else if(stack_open.contains(ext)) {
      if(!process::getLastParms("Stack", "Open", parms)) {
        QMessageBox::critical(this, "Error opening stack", "There is no Stack process named 'Open Stack'");
        return;
      }
      parms[0] = pth;
      parms[1] = (main ? "Main" : "Work");
      parms[2] = QString::number(stack);
      parms[3] = "No";
      launchProcess("Stack", "Open", parms, true);
    } else if(image_itk.contains(ext)) {
      parms << pth << (main ? "Main" : "Work") << QString::number(stack) << "No" << "Yes";
      launchProcess("Stack", "ITK Image Reader", parms, true, true);
    } else if(ext == "fct") {
      ui.Viewer->loadFile(pth, (stack == 1), not main);
    } else if(ext == "py") {
      parms << pth << "No";
      launchProcess("Global", "Python Script", parms, true);
    } else if(pkg_ext.contains(ext) or fi.isDir()) {
      showPackageManager();
      emit installPackage(pth);
    }
  }
}

void LithoGraphX::showEvent(QShowEvent* e)
{
  QMainWindow::showEvent(e);
  // And show the splash screen
  {
    QSettings settings;
    settings.beginGroup("MainWindow");
    auto lastViewVer = settings.value("ViewedSplash", "").toString();
    settings.setValue("ViewedSplash", lgxVersion.toString());
    settings.endGroup();
    if(lastViewVer.isEmpty() or lgxVersion > util::Version(lastViewVer))
      on_actionAbout_triggered();
  }
}

void LithoGraphX::closeEvent(QCloseEvent* e)
{
  if(_needSaving) {
    QMessageBox::StandardButton a = QMessageBox::question(
        this, "Closing with unsaved informations",
        "You haven't saved the current state of LithoGraphX. Do you want to quit without saving?",
        QMessageBox::No | QMessageBox::Yes);
    if(a == QMessageBox::No) {
      e->ignore();
      return;
    }
  }
  ui.Viewer->_quitting = true;
  saveSettings();

  // Unload the processes by hand

  ui.ProcessStackCommand->clear();
  ui.ProcessMeshCommand->clear();
  ui.ProcessGlobalCommand->clear();

  /*
   *forall(const auto& l, _loadedLibs) {
   *  if(l->isLoaded()) {
   *    if(DEBUG)
   *      Information::out << "Unloading library '" << l->fileName() << "'" << endl;
   *    l->unload();
   *    if(l->isLoaded())
   *      SETSTATUS("Failed to unload library '" << l->fileName() << "': " << l->errorString());
   *  }
   *}
   *_loadedLibs.clear();
   *process::unregisterSystemProcesses();
   */

  process::pluginManager()->finalize();

  _currentSetup.reset();

  /*
     if(!process::stackProcessFactories().empty())
     {
     size_t nb_left_processes = process::stackProcessFactories().size();
     Information::out << "Error, " << nb_left_processes << " stack process factories non empty when unloading
     everything" << endl;
     process::stackProcessFactories().clear();
     }

     if(!process::meshProcessFactories().empty())
     {
     size_t nb_left_processes = process::meshProcessFactories().size();
     Information::out << "Error, " << nb_left_processes << " mesh process factories non empty when unloading
     everything" << endl;
     process::meshProcessFactories().clear();
     }

     if(!process::globalProcessFactories().empty())
     {
     size_t nb_left_processes = process::globalProcessFactories().size();
     Information::out << "Error, " << nb_left_processes << " global process factories non empty when unloading
     everything" << endl;
     process::globalProcessFactories().clear();
     }
   */

  QMainWindow::closeEvent(e);
}

void LithoGraphX::saveSettings()
{
  QSettings settings;
  settings.beginGroup("MainWindow");
  settings.setValue("Geometry", saveGeometry());
  settings.setValue("WindowState", saveState(1));
  TaskEditDlg::saveTasksToSettings(_tasks);
  settings.endGroup();
}

void LithoGraphX::on_Stack1_stackUnloaded() {
  ui.Stack1Size->setText("Not loaded");
  ui.Stack1Resolution->setText("");
}

void LithoGraphX::on_Stack2_stackUnloaded() {
  ui.Stack2Size->setText("Not loaded");
  ui.Stack2Resolution->setText("");
}

void LithoGraphX::on_Stack1_changeSize(const Point3u& size, const Point3f& step, const Point3f& )
{
  QString msg;
  if(size_t(size.x()) * size.y() * size.z() != 0) {
    msg = QString("%1x%2x%3").arg(size.x()).arg(size.y()).arg(size.z());
    ui.Stack1Size->setText(msg);
    msg = QString("%1x%2x%3").arg(step.x()).arg(step.y()).arg(step.z());
    msg += UM;
    ui.Stack1Resolution->setText(msg);
  } else
    on_Stack1_stackUnloaded();
}

void LithoGraphX::on_Stack1_updateSliderScale()
{
  ui.Stack1Scale_X->setValue(_stack1->toSliderScale(_stack1->stack->scale().x()));
  ui.Stack1Scale_Y->setValue(_stack1->toSliderScale(_stack1->stack->scale().y()));
  ui.Stack1Scale_Z->setValue(_stack1->toSliderScale(_stack1->stack->scale().z()));
}

void LithoGraphX::on_Stack2_changeSize(const Point3u& size, const Point3f& step, const Point3f& )
{
  QString msg;
  if(size_t(size.x()) * size.y() * size.z() != 0) {
    msg = QString("%1x%2x%3").arg(size.x()).arg(size.y()).arg(size.z());
    ui.Stack2Size->setText(msg);
    msg = QString("%1x%2x%3").arg(step.x()).arg(step.y()).arg(step.z());
    msg += UM;
    ui.Stack2Resolution->setText(msg);
  } else
    on_Stack2_stackUnloaded();
}

void LithoGraphX::on_Stack2_updateSliderScale()
{
  ui.Stack2Scale_X->setValue(_stack2->toSliderScale(_stack2->stack->scale().x()));
  ui.Stack2Scale_Y->setValue(_stack2->toSliderScale(_stack2->stack->scale().y()));
  ui.Stack2Scale_Z->setValue(_stack2->toSliderScale(_stack2->stack->scale().z()));
}

void LithoGraphX::modified(bool on)
{
  if(on != _needSaving)
    _needSaving = on;
}

namespace {
void createItem(const process::BaseProcessDefinition& def, QTreeWidget* tree, QHash<QString, QTreeWidgetItem*>& folders)
{
  QStringList desc;
  desc << def.name;
  QTreeWidgetItem* item = new QTreeWidgetItem(desc);
  item->setFlags(Qt::ItemIsEnabled | Qt::ItemNeverHasChildren | Qt::ItemIsSelectable);
  item->setToolTip(0, def.description);
  item->setIcon(0, def.icon);
  if(def.folder.isEmpty())
    tree->addTopLevelItem(item);
  else {
    QString folder_s = def.folder;
    QTreeWidgetItem* folder = process_util::getFolder(folder_s, folders, tree);
    folder->addChild(item);
  }
}
} // namespace

template <typename P>
void updateDefinitions(typename process::Registration<P>::processFactory factory,
                       const QHash<QString, process::BaseProcessDefinition>& oldProcDefs)
{
  // SETSTATUS("updateDefinitions(...)");
  process::SetupProcess setup;
  // SETSTATUS("  Creating process");
  auto proc = (*factory)(setup);
  auto name = proc->name();
  name.replace('_', " ");

  int defined = proc->parmNames().size();
  int provided = proc->parmDefaults().size();
  if(defined != provided) {
    Information::err << "Error for process " << name
                     << ": parameters doesn't match the number of defaults. This process won't be used." << endl;
    return;
  }

  process::ParmList newParms;
  if(oldProcDefs.contains(name)) {
    process::BaseProcessDefinition oldDef = oldProcDefs.value(name);
    if(oldProcDefs.contains(name) and oldDef.parmNames.size() == defined) {
      newParms = oldDef.parms;
    } else {
      newParms = proc->parmDefaults();
    }
  } else {
    newParms = proc->parmDefaults();
  }

  process::pluginManager()->processDefinition<P>(name).parms = newParms;
  // SETSTATUS("end of updateDefinitions(...)");
}

namespace {
// Perform a deep copy of the strings, and don't copy the pointers
template <typename P>
void duplicateDefinitions(QHash<QString, process::BaseProcessDefinition>& savedProcDefs)
{
  auto manager = process::pluginManager();
  for(const QString& name : manager->processes<P>()) {
    process::ProcessDefinition<P> newDef;
    const process::ProcessDefinition<P>& def = manager->processDefinition<P>(name);
    newDef.name = def.name;
    // newDef.nbNamedStrings = def.nbNamedStrings;
    // newDef.nbNamedValues = def.nbNamedValues;
    newDef.parmNames = def.parmNames;
    newDef.parms = def.parms;
    savedProcDefs[newDef.name] = newDef;
  }
}
} // namespace

void LithoGraphX::saveProcessesParameters()
{
  duplicateDefinitions<StackProcess>(_savedStackProc);
  duplicateDefinitions<MeshProcess>(_savedMeshProc);
  duplicateDefinitions<GlobalProcess>(_savedGlobalProc);
}

void LithoGraphX::recallProcessesParameters()
{
  auto manager = process::pluginManager();
  for(const QString& procName: manager->processes<StackProcess>()) {
    const auto& def = manager->processDefinition<StackProcess>(procName);
    updateDefinitions<StackProcess>(def.factory, _savedStackProc);
  }
  for(const QString& procName: manager->processes<MeshProcess>()) {
    const auto& def = manager->processDefinition<MeshProcess>(procName);
    updateDefinitions<MeshProcess>(def.factory, _savedMeshProc);
  }
  for(const QString& procName: manager->processes<GlobalProcess>()) {
    const auto& def = manager->processDefinition<GlobalProcess>(procName);
    updateDefinitions<GlobalProcess>(def.factory, _savedGlobalProc);
  }

  QString processType = _currentProcessType;
  try {
    const auto& def = manager->baseProcessDefinition(processType, _currentProcessName);
    _parmsModel->setParms(def);
  } catch(process::NoSuchProcess&) {
    // pass
  }
}

void LithoGraphX::scanProcessLists()
{
  ui.ProcessStackCommand->clear();
  ui.ProcessMeshCommand->clear();
  ui.ProcessGlobalCommand->clear();

  // Save the current state of things, duplicating everything to never
  // reference a constant of the dll
  saveProcessesParameters();

  auto manager = process::pluginManager();

  // list of folders
  QHash<QString, QTreeWidgetItem*> stackFolders, meshFolders, globalFolders;

  // then create processes
  for(const QString& procName: manager->processes<StackProcess>()) {
    const auto& def = manager->processDefinition<StackProcess>(procName);
    updateDefinitions<StackProcess>(def.factory, _savedStackProc);
    createItem(def, ui.ProcessStackCommand, stackFolders);
  }

  SETSTATUS("Generated " << manager->nbProcesses<StackProcess>() << " stack processes from "
                         << manager->nbRegisteredProcesses<StackProcess>() << " registered");
  ui.ProcessStackCommand->sortItems(0, Qt::AscendingOrder);
  ui.ProcessStackCommand->resizeColumnToContents(0);
  QTreeWidgetItem* item = ui.ProcessStackCommand->topLevelItem(0);
  if(item)
    ui.ProcessStackCommand->setCurrentItem(item);

  // SETSTATUS("Loading mesh processes");

  for(const QString& procName: manager->processes<MeshProcess>()) {
    const auto& def = manager->processDefinition<MeshProcess>(procName);
    updateDefinitions<MeshProcess>(def.factory, _savedMeshProc);
    createItem(def, ui.ProcessMeshCommand, meshFolders);
  }
  SETSTATUS("Generated " << manager->nbProcesses<MeshProcess>() << " stack processes from "
                         << manager->nbRegisteredProcesses<MeshProcess>() << " registered");
  ui.ProcessMeshCommand->sortItems(0, Qt::AscendingOrder);
  ui.ProcessMeshCommand->resizeColumnToContents(0);
  item = ui.ProcessMeshCommand->topLevelItem(0);
  if(item)
    ui.ProcessMeshCommand->setCurrentItem(item);

  // SETSTATUS("Loading global processes");

  for(const QString& procName: manager->processes<GlobalProcess>()) {
    const auto& def = manager->processDefinition<GlobalProcess>(procName);
    updateDefinitions<GlobalProcess>(def.factory, _savedGlobalProc);
    createItem(def, ui.ProcessGlobalCommand, globalFolders);
  }
  SETSTATUS("Generated " << manager->nbProcesses<GlobalProcess>() << " stack processes from "
                         << manager->nbRegisteredProcesses<GlobalProcess>() << " registered");
  ui.ProcessGlobalCommand->sortItems(0, Qt::AscendingOrder);
  ui.ProcessGlobalCommand->resizeColumnToContents(0);
  item = ui.ProcessGlobalCommand->topLevelItem(0);
  if(item)
    ui.ProcessGlobalCommand->setCurrentItem(item);

  if(ui.ProcessStackCommand->topLevelItemCount() > 0 and ui.ProcessStackCommand->topLevelItem(0)->childCount() == 0)
    on_ProcessStackCommand_currentItemChanged(ui.ProcessStackCommand->topLevelItem(0), 0);

  reloadTasks();
}

void LithoGraphX::reloadProcesses()
{
  if(!_currentProcessName.isEmpty()) {
    _currentProcessName.clear();
    _parmsModel->clear();
  }

  //auto manager = process::pluginManager();

  //manager->unloadProcesses();

  //manager->loadProcesses();

  scanProcessLists();
}

void LithoGraphX::on_parmsModel_valuesChanged()
{
  if(_currentProcessName.isEmpty())
    return;
  auto manager = process::pluginManager();
  process::BaseProcessDefinition* def = 0;
  if(_currentProcessInTasks) {
    QTreeWidgetItem* current = ui.ProcessTasksCommand->currentItem();
    if(current and current->flags() & Qt::ItemIsEnabled) {
      QString task = current->data(0, TaskNameRole).toString();
      int proc_num = current->data(0, ProcessPositionRole).toInt();
      if(_tasks.contains(task)) {
        QList<TypedProcessDefinition>& defs = _tasks[task];
        if(proc_num < defs.size())
          def = &defs[proc_num];
      }
    }
  } else if(not _currentProcessName.isEmpty()) {
    def = &manager->baseProcessDefinition(_currentProcessType, _currentProcessName);
  }
  if(def)
    def->parms = _parmsModel->parms();
}

void LithoGraphX::on_stackUpdateTimer_timeout()
{
  if(_stack1NeedsUpdate) {
    _stack1->updateImgData();
    _stack1NeedsUpdate = false;
  }
  if(_stack2NeedsUpdate) {
    _stack2->updateImgData();
    _stack2NeedsUpdate = false;
  }
  ui.Viewer->update3D();
}

void LithoGraphX::on_Stack1_needsUpdate()
{
  _stack1NeedsUpdate = true;
  _stackUpdateTimer->start();
}

void LithoGraphX::on_Stack2_needsUpdate()
{
  _stack2NeedsUpdate = true;
  _stackUpdateTimer->start();
}

bool LithoGraphX::event(QEvent* e)
{
  ProcessUpdateViewEvent* ve = dynamic_cast<ProcessUpdateViewEvent*>(e);
  if(ve) {
    ui.Viewer->update3D();
    return true;
  }
  Information::Event* ie = dynamic_cast<Information::Event*>(e);
  if(ie) {
    Information::Print(ie->message);
    ie->accept();
    return true;
  }
  ProcessSystemCommand* se = dynamic_cast<ProcessSystemCommand*>(e);
  if(se) {
    processSystemCommand(se->process, se->command, se->parms);
    return true;
  }
  if(QThread::currentThread() != thread()) {
    Information::out << "Received event from wrong thread: 0x"
                     << QString::number(quintptr(QThread::currentThread()), 16) << endl;
    Information::out << "Message type = " << e->type() << endl;
    return false;
  } else
    return QMainWindow::event(e);
}

int LithoGraphX::activeStack() const
{
  int active = ui.StackTabs->currentIndex();
  if((active == 0 and (ui.Stack1MainShow->isChecked() or ui.Stack1WorkShow->isChecked()))
     or (active == 1 and (ui.Stack2MainShow->isChecked() or ui.Stack2WorkShow->isChecked())))
    return active;
  return -1;
}

int LithoGraphX::activeMesh() const
{
  int active = ui.StackTabs->currentIndex();
  if((active == 0 and (ui.Stack1MeshShow->isChecked() or ui.Stack1SurfShow->isChecked()))
     or (active == 1 and (ui.Stack2MeshShow->isChecked() or ui.Stack2SurfShow->isChecked())))
    return active;
  return -1;
}

void LithoGraphX::resetDefaultParameters()
{
  if(!_currentProcessName.isEmpty()) {
    QString type = _currentProcessType;
    process::ParmList parms;
    QList<float> values;
    if(process::getDefaultParms(type, _currentProcessName, parms)) {
      _parmsModel->setParms(parms);
    }
  }
}

void LithoGraphX::reloadTasks()
{
  ui.ProcessTasksCommand->clear();
  QStringList ts = _tasks.keys();
  ts.sort();
  forall(const QString& t, ts) {
    QList<TypedProcessDefinition>& defs = _tasks[t];
    QTreeWidgetItem* task_item = new QTreeWidgetItem(QStringList() << t);
    int proc_num = 0;
    forall(TypedProcessDefinition& def, defs) {
      QTreeWidgetItem* item = new QTreeWidgetItem(QStringList() << def.name << def.type);
      item->setToolTip(0, def.description);
      item->setIcon(0, def.icon);
      item->setData(0, TaskNameRole, t);
      item->setData(0, ProcessPositionRole, proc_num);
      if(process::validProcessName(def.type, def.name)) {
        if(!process::checkProcessParms(def.type, def.name, def.parms)) {
          process::getLastParms(def.type, def.name, def.parms);
        }
      } else
        item->setFlags(item->flags() & ~Qt::ItemIsEnabled);
      task_item->addChild(item);
      ++proc_num;
    }
    ui.ProcessTasksCommand->addTopLevelItem(task_item);
    ui.ProcessTasksCommand->expandItem(task_item);
  }
}

void LithoGraphX::editTasks()
{
  TaskEditDlg* dlg = new TaskEditDlg(_tasks, _savedStackProc, _savedMeshProc, _savedGlobalProc, this);
  if(dlg->exec() == QDialog::Accepted) {
    // Deep copy to duplicate all strings
    const TaskEditDlg::tasks_t& ts = dlg->tasks();
    _tasks.clear();
    updateCurrentTasks(ts);
    reloadTasks();
    TaskEditDlg::saveTasksToSettings(_tasks);
  }
}

void LithoGraphX::on_actionDebug_Dialog_triggered()
{
  static QPointer<DebugDlg> dlg = 0;
  if(dlg.isNull())
    dlg = new DebugDlg(ui.Viewer, this);
  dlg->show();
}

void LithoGraphX::setCutSurfControl()
{
  float sceneRadius = _cutSurf.getSceneRadius();
  if(sceneRadius == 0.0f)
    sceneRadius = 1.0f;
  ui.CutSurfSizeX->setSliderPosition(int(log(_cutSurf.cut->size().x() * 2.0 / sceneRadius) * 2000));
  ui.CutSurfSizeY->setSliderPosition(int(log(_cutSurf.cut->size().y() * 2.0 / sceneRadius) * 2000));
  ui.CutSurfSizeZ->setSliderPosition(int(log(_cutSurf.cut->size().z() * 2.0 / sceneRadius) * 2000));
}

void LithoGraphX::resetClipControl(float sceneRadius)
{
  if(sceneRadius == 0.0f)
    sceneRadius = 1.0f;
  ui.Clip1Width->setSliderPosition(int(log(_currentSetup->clip1()->width() * 2.0 / sceneRadius) * 2000));
  ui.Clip2Width->setSliderPosition(int(log(_currentSetup->clip2()->width() * 2.0 / sceneRadius) * 2000));
  ui.Clip3Width->setSliderPosition(int(log(_currentSetup->clip3()->width() * 2.0 / sceneRadius) * 2000));
}

void LithoGraphX::on_CutSurfReset_clicked()
{
  _cutSurf.reset(ui.Viewer->getSceneRadius());
  setCutSurfControl();
}

const process::Process* LithoGraphX::globalProcess() const {
  return _currentSetup.get();
}

process::Process* LithoGraphX::globalProcess() {
  return _currentSetup.get();
}

void LithoGraphX::on_Viewer_selectLabelChanged(int lab) {
  _currentSetup->setSelectedLabel(lab);
  if(lab <= 0)
    ui.actionLabelColor->setToolTip("No label selected");
  else
    ui.actionLabelColor->setToolTip(QString("Label %1").arg(lab));
}

void LithoGraphX::keyPressEvent(QKeyEvent *event)
{
  auto mod = QGuiApplication::keyboardModifiers();
  if(DEBUG)
    Information::out << "keyPressEvent " << event->key() << " " << modifierString(mod) << endl;
  QMainWindow::keyPressEvent(event);
}

void LithoGraphX::on_actionUserMacroFolder_triggered()
{
  QDir dir = util::userMacroDir(true);
  if(dir.exists()) {
    QString link = FILE_PREFIX + dir.absolutePath();
    QDesktopServices::openUrl(QUrl(link));
  } else
    QMessageBox::warning(this, "User Macro Folder", QString("Cannot find or create user macro folder:\n%1").arg(dir.absolutePath()));
}

void LithoGraphX::on_actionSample_Plugins_triggered()
{
  QDir dir = util::docsDir();
  if(dir.cd("samples")) {
    QString link = FILE_PREFIX + dir.absolutePath();
    QDesktopServices::openUrl(QUrl(link));
  } else
    QMessageBox::warning(this, "Sample Plugins",
                         QString("Cannot find sample plugins folder:\n%1").arg(dir.absoluteFilePath("samples")));
}

void LithoGraphX::on_actionSystemMacroFolder_triggered()
{
  QDir dir = util::macroDirs()[0];
  if(dir.exists()) {
    QString link = FILE_PREFIX + dir.absolutePath();
    QDesktopServices::openUrl(QUrl(link));
  } else
    QMessageBox::warning(this, "System Macro Folder", QString("Cannot find system macro folder:\n%1").arg(dir.absolutePath()));
}

void LithoGraphX::on_actionUserProcessFolder_triggered()
{
  QDir dir = util::userProcessesDir(true);
  if(dir.exists()) {
    QString link = FILE_PREFIX + dir.absolutePath();
    QDesktopServices::openUrl(QUrl(link));
  } else
    QMessageBox::warning(this, "User Process Folder", QString("Cannot find or create user process folder:\n%1").arg(dir.absolutePath()));
}

void LithoGraphX::on_actionSystemProcessFolder_triggered()
{
  QDir dir = util::processesDirs()[0];
  if(dir.exists()) {
    QString link = FILE_PREFIX + dir.absolutePath();
    QDesktopServices::openUrl(QUrl(link));
  } else
    QMessageBox::warning(this, "System Process Folder", QString("Cannot find system process folder:\n%1").arg(dir.absolutePath()));
}

void LithoGraphX::on_actionReloadMacros_triggered()
{
  auto manager = process::pluginManager();
  manager->refreshAll();
  scanProcessLists();
}

void LithoGraphX::on_actionSelectStack1_triggered()
{
  ui.StackTabs->setCurrentWidget(ui.Stack1Tab);
}

void LithoGraphX::on_actionSelectStack2_triggered()
{
  ui.StackTabs->setCurrentWidget(ui.Stack2Tab);
}

void LithoGraphX::on_Clip1Enable_toggled(bool on)
{ ui.Viewer->enableClip(ui.Viewer->_c1, on); }
void LithoGraphX::on_Clip2Enable_toggled(bool on)
{ ui.Viewer->enableClip(ui.Viewer->_c2, on); }
void LithoGraphX::on_Clip3Enable_toggled(bool on)
{ ui.Viewer->enableClip(ui.Viewer->_c3, on); }

void LithoGraphX::on_Clip1Grid_toggled(bool on)
{ ui.Viewer->showClipGrid(ui.Viewer->_c1, on); }
void LithoGraphX::on_Clip2Grid_toggled(bool on)
{ ui.Viewer->showClipGrid(ui.Viewer->_c2, on); }
void LithoGraphX::on_Clip3Grid_toggled(bool on)
{ ui.Viewer->showClipGrid(ui.Viewer->_c3, on); }

void LithoGraphX::on_Clip1Width_valueChanged(int value)
{ ui.Viewer->setClipWidth(ui.Viewer->_c1, value); }
void LithoGraphX::on_Clip2Width_valueChanged(int value)
{ ui.Viewer->setClipWidth(ui.Viewer->_c2, value); }
void LithoGraphX::on_Clip3Width_valueChanged(int value)
{ ui.Viewer->setClipWidth(ui.Viewer->_c3, value); }

void LithoGraphX::on_PixelEditRadius_valueChanged(int value)
{ lgx::ImgData::PixelEditRadius = value; }

void LithoGraphX::updateFillWorkOptions()
{
  auto stk = _currentSetup->currentStack();
  auto work = stk->work();
  if(work and stk->currentStore() == work) {
    ui.FillWorkData->setEnabled(true);
    if(ui.FillWorkData->isChecked()) {
      bool has_labels = stk->work()->labels();
      ui.SeedStack->setEnabled(has_labels);
      ui.FillWorkValue->setDisabled(has_labels);
    } else {
      ui.SeedStack->setEnabled(false);
      ui.FillWorkValue->setEnabled(false);
    }
  } else
    ui.FillWorkData->setEnabled(false);
}

void LithoGraphX::on_FillWorkData_toggled(bool on)
{
  lgx::ImgData::FillWorkData = on;
  updateFillWorkOptions();
}

void LithoGraphX::on_SeedStack_toggled(bool on)
{ ImgData::SeedStack = on; }

void LithoGraphX::on_FillWorkValue_valueChanged(int value)
{
  if(value < 0) value = 0;
  else if(value > 65535) value = 65535;
  lgx::ImgData::FillWorkValue = (ushort)value;
}

void LithoGraphX::on_actionSettings_triggered(bool on)
{
  if(_settingsDlg.isNull()) {
    _settingsDlg = new SettingsDlg(ui.Viewer);
    auto settingsDlg = _settingsDlg.data();
    connect(settingsDlg, &SettingsDlg::update2DDrawing, ui.Viewer, &LithoViewer::update2D);
    connect(settingsDlg, &SettingsDlg::update3DDrawing, this, &LithoGraphX::updateDefaultTex16Bits);
    connect(settingsDlg, &SettingsDlg::update3DDrawing, ui.Viewer, &LithoViewer::updateTextures);
    connect(settingsDlg, &SettingsDlg::update3DDrawing, ui.Viewer, &LithoViewer::update3D);
    connect(settingsDlg, &SettingsDlg::finished, ui.actionSettings, &QAction::toggle);
  }
  _settingsDlg->setVisible(on);
}

LithoViewer* LithoGraphX::viewer()
{
  return ui.Viewer;
}

void LithoGraphX::updateDefaultTex16Bits()
{
  if(_stack1->stack->empty()) {
    ui.Stack1Main16Bit->setChecked(ImgData::defaultTex16Bits);
    ui.Stack1Work16Bit->setChecked(ImgData::defaultTex16Bits);
  }
  if(_stack2->stack->empty()) {
    ui.Stack2Main16Bit->setChecked(ImgData::defaultTex16Bits);
    ui.Stack2Work16Bit->setChecked(ImgData::defaultTex16Bits);
  }
}

void LithoGraphX::on_actionManagePlugin_triggered()
{
  static QPointer<gui::PluginConfiguration> dlg = nullptr;
  if(not dlg) dlg = new gui::PluginConfiguration(this);
  dlg->show();
}

void LithoGraphX::on_actionManagePackages_triggered()
{
  showPackageManager();
}

void LithoGraphX::showPackageManager()
{
  static QPointer<gui::PackageConfigurationDlg> dlg = nullptr;
  if(not dlg) dlg = new gui::PackageConfigurationDlg(this);
  dlg->show();
}

#ifdef LithoGraphX_CHECK_UPDATES
void LithoGraphX::on_actionCheckUpdatesLGX_triggered()
{
  Information::out << "Checking for updates" << endl;
  CheckVersionDlg checkDlg(this);
  connect(&checkDlg, &CheckVersionDlg::newVersionAvailable, this, &LithoGraphX::hasNewVersion);
  if(checkDlg.exec() == QDialog::Accepted)
    close();
}

void LithoGraphX::checkUpdateAtStart()
{
  QSettings settings;
  // Check if we should check .. no more than once a day!
  auto doCheck = settings.value("AutoUpdate/CheckUpdate", true).toBool();
  if(doCheck) {
    bool ok;
    auto checkInterval = settings.value("AutoUpdate/CheckInterval", 1).toUInt(&ok);
    if(not ok)
      checkInterval = 1;
    auto value = settings.value("AutoUpdate/LastCheck");
    if(value.canConvert<QDateTime>()) {
      auto last_check = value.toDateTime();
      auto now = QDateTime::currentDateTime();
      if(last_check.msecsTo(now) < checkInterval*24*3600*1000) {
        QString version_string = settings.value("AutoUpdate/LatestVersion").toString();
        auto lastVersion = util::Version(version_string);
        if(lastVersion and lastVersion > lgxVersion)
          hasNewVersion();
        return;
      }
    }
    // Check if we need to update
    CheckVersionDlg checkDlg(this, true);
    connect(&checkDlg, &CheckVersionDlg::newVersionAvailable, this, &LithoGraphX::hasNewVersion);
    if(checkDlg.exec() == QDialog::Accepted)
      close();
  }
}

void LithoGraphX::hasNewVersion()
{
  ui.actionCheckUpdatesLGX->setIcon(QIcon(":/images/Warning.png"));
}

#endif // LithoGraphX_CHECK_UPDATES
