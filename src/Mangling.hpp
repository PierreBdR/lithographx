/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef MANGLING_H
#define MANGLING_H

#include <LGXConfig.hpp>

#include <string>

class QString;

namespace lgx {
namespace util {
/**
 * Demangle all the words in the string.
 *
 * \returns The string with all symbols demangled.
 */
LGX_EXPORT QString qdemangle(QString s);
/**
 * Demangle all the words in the string.
 *
 * \returns The string with all symbols demangled.
 */
LGX_EXPORT QString qdemangle(const char* s);
/**
 * Demangle all the words in the string.
 *
 * \returns The string with all symbols demangled.
 */
LGX_EXPORT QString qdemangle(std::string s);
/**
 * Demangle all the words in the string.
 *
 * \returns The string with all symbols demangled.
 */
LGX_EXPORT std::string demangle(std::string s);
} // namespace util
} // namespace lgx
#endif
