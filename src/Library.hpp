/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef LIBRARY_HPP
#define LIBRARY_HPP

#include <LGXConfig.hpp>

#include <QString>

class LGX_EXPORT Library {
public:
#if defined(WIN32) || defined(WIN64)
  typedef HINSTANCE handle_t;
#else
  typedef void* handle_t;
#endif

  Library(QString path);
  Library(const Library&) = default;

  Library& operator=(const Library&) = default;

#ifndef _MSC_VER
  Library(Library&&) = default;
  Library& operator=(Library&&) = default;
#endif

  QString fileName() const {
    return _filename;
  }
  void setFileName(const QString& fn) {
    _filename = fn;
  }

  bool load();
  bool unload();

  bool isLoaded() const {
    return (bool)_handle;
  }

  QString errorMessage() const {
    return _errorMessage;
  }

  static bool isLibrary(QString path);

protected:
  QString _filename, _errorMessage;
  handle_t _handle;
};

#endif
