/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "ColorEditorDlg.hpp"
#include "Colors.hpp"

#include <QColorDialog>

ColorEditorDlg::ColorEditorDlg(lgx::Colors* colors, QWidget* parent, Qt::WindowFlags f)
  : QDialog(parent, f)
  , model(colors)
{
  ui.setupUi(this);
  model->backupColors();
  ui.colorsView->setModel(model);

  int row = model->rowCount();
  for(int i = 0; i < row; ++i) {
    const QModelIndex& index = model->index(i, 0);
    ui.colorsView->setExpanded(index, true);
    // int srow = model->rowCount(index);
    // for(int j = 0 ; j < srow ; ++j)
    //{
    // const QModelIndex& idx = model->index(i, 0, index);
    // ui.colorsView->setItemsExpandable(idx, false);
    //}
  }
  ui.colorsView->resizeColumnToContents(0);
}

void ColorEditorDlg::on_colorsView_doubleClicked(const QModelIndex& idx)
{
  QColor col = model->color(idx);
  if(col.isValid()) {
    col = QColorDialog::getColor(col, this, "Change color");
    if(col.isValid()) {
      model->setColor(idx, col);
    }
  }
}

void ColorEditorDlg::on_buttonBox_clicked(QAbstractButton* btn)
{
  switch(ui.buttonBox->buttonRole(btn)) {
  case QDialogButtonBox::RejectRole:
    model->restoreColors();
    break;
  case QDialogButtonBox::ApplyRole:
  case QDialogButtonBox::AcceptRole:
    model->backupColors();
    break;
  default:
    break;
  }
}
