smooth centroid out vec2 texPos;
in vec3 texCoord;

void main()
{
  texPos = (gl_TextureMatrix[0]*vec4(texCoord, 1)).xy;
  gl_Position = ftransform();
  gl_FrontColor = gl_Color;
}
