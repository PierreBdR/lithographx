The Packaging System
====================

Packages are simply folders, possibly compressed. There are two kind of
packages:

#. Source packages needs to be compiled but are independent from the operating
   system.
#. Binary packages can be directly used but are usually dependent on an exact
   version of LithoGraphX and operating system.

Preparing a Binary Package
--------------------------

A binary package is simpler to prepare and is useful to distribute macros that
don't need compilation. The structure is important, the top-level should contain
a file called ``description.ini`` and up to three folders, looking like this:

.. code-block:: text

    description.ini
    macros/
    processes/
        include/
            PackageName/
    lib/

The ``description.ini`` file should be following this template:

.. code-block:: ini

    [Package]
    Name = [Package name]
    Version = major.minor.patch
    Description = "Description of the package"
    Dependencies = ...
    OS = [OS]

The "Dependencies" and "OS" description are optional. If they are not there, it
means there is no constraints.

The other folders should contains specific files:

``macros``
    This is were the macro needs to be put. This is also where any Python
    package you need should be placed.
``lib``
    This is were support libraries should be placed.
``packages``
    This is were compiled plug-ins need to be placed. In the ``include`` folder,
    the header files for the support libraries should be placed.

There shouldn't be any files outside these folder or the installation will fail.

Preparing a Source Package
--------------------------

Unlike a binary package, the structure of a source package is a lot more free.
The only constraint is the presence of two files at the root of the package:

.. code-block:: text

    description.ini
    CMakeLists.txt

The ``description.ini`` file has the same structure as for the binary package.
``CMakeLists.txt`` should describe how to compile and install a binary package
using CPack with a ZIP generator. Although you are free to implement this as you
want, it is recommended to include the LithoGraphX CMake package:

.. toctree::

    cmake_package

A typical CMake file would follow the following template:

.. code-block:: cmake

    find_package(LithoGraphX REQUIRED)
    init_lgx_package()

    lgx_add_plugin(${LithoGraphX_PACKAGE_NAME} source1.cpp source2.cpp ...)

    lgx_package()

Or, if a support library is compiled:

.. code-block:: cmake

    find_package(LithoGraphX REQUIRED)
    init_lgx_package()

    lgx_add_library(${LithoGraphX_PACKAGE_NAME}_lib SHARED
        source1.cpp source2.cpp ...
        PUBLIC_HEADER header1.gpp header2.hpp ...)

    lgx_add_plugin(${LithoGraphX_PACKAGE_NAME} source_impl.cpp)
    target_link_library(${LithoGraphX_PACKAGE_NAME} ${LithoGraphX_PACKAGE_NAME}_util)

    lgx_package()


