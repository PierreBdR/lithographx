/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef QALGORITHMS_HPP
#define QALGORITHMS_HPP

#include <QList>
#include <QRunnable>
#include <QSet>
#include <QThreadPool>

namespace lgx {
namespace util {

/**
 * Take two lists and return a list with the unique elements of the first one ordered like the second one.
 *
 * \param toSort List of elements to be sorted
 * \param sorted Order of elements
 *
 * The elements of \c toSort not in \c sorted will be added at the end in the same order they appear in \c toSort.
 *
 * \note This requires the function qHash to be defined for the type T
 */
template <typename T>
QList<T> sortLike(const QList<T>& toSort, const QList<T>& sorted)
{
  QList<T> result;
  QSet<T> elements;
  for(const T& t: toSort)
    elements.insert(t);
  for(const T& t: sorted) {
    auto found = elements.find(t);
    if(found != elements.end()) {
      result << t;
      elements.erase(found);
    }
  }
  for(const T& t: toSort) {
    auto found = elements.find(t);
    if(found != elements.end()) {
      result << t;
      elements.erase(found);
    }
  }
  return result;
}

} // namespace lgx
} // namespace util

#endif // QALGORITHMS_HPP

