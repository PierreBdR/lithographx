/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef EIGENDECOMPOSITION_HPP
#define EIGENDECOMPOSITION_HPP

#include <LGXConfig.hpp>

#include <Geometry.hpp>
#include <array>

namespace lgx {
namespace util {

/**
 * Result of a 2D eigen-values decomposition
 */
struct LGX_EXPORT EigenVecs2D
{
  /// List of eigen vectors
  std::array<Point2f,2> evec;
  /// Eigen values corresponding to the different eigen vectors
  Point2f eval;

  /**
   * True if the structure is a valid decomposition.
   *
   * This means, the eigen vectors have norm 1 and are orthogonal to each other.
   */
  bool valid(double epsilon=1e-6) const;
};

/**
 * Result of a 2D eigen-values decomposition
 */
struct LGX_EXPORT EigenVecs3D
{
  /// Matrix whose rows are the eigen vectors
  std::array<Point3f,3> evec;
  /// Eigen values corresponding to the different eigen vectors
  Point3f eval;

  /**
   * True if the structure is a valid decomposition.
   *
   * This means, the eigen vectors have norm 1 and are orthogonal to each other.
   */
  bool valid(double epsilon=1e-6) const;
};

/// Ways of sorted the eigen values
enum class EigenSort
{
  NoSort,        ///< No sorting
  Decreasing,    ///< Sort the values from largest to smallest
  Increasing,    ///< Sort the values from smallest to largest
  DecreasingAbs, ///< Sort from largest to smallest absolute value
  IncreasingAbs  ///< Sort from smallest to largest absolute value
};

/**
 * 2D eigen value decompositions
 */
LGX_EXPORT EigenVecs2D eigenDecomposition(const Matrix2f& m, EigenSort sorting = EigenSort::NoSort);

/**
 * 3D eigen value decompositions
 */
LGX_EXPORT EigenVecs3D eigenDecomposition(const Matrix3f& m, EigenSort sorting = EigenSort::NoSort);

/**
 * Sort the eigen values
 */
LGX_EXPORT void sort(EigenVecs2D& dec, EigenSort sorting);

/**
 * Sort the eigen values
 */
LGX_EXPORT void sort(EigenVecs3D& dec, EigenSort sorting);

} // namespace util
} // namespace lgx

#endif // EIGENDECOMPOSITION_HPP

