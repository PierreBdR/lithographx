/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef CUDA_HPP
#define CUDA_HPP

#define COMPILE_CUDA
#include <LGXConfig.hpp>

#include <cuda/CudaGlobal.hpp>
#include <Geometry.hpp>

#include <cfloat>
#include <ctime>
#include <exception>
#include <iostream>
#include <set>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <time.h>
#include <vector>

#include <Thrust.hpp>

#ifdef THRUST_BACKEND_CUDA
#  include <cuda_runtime.h>
#  include <device_functions.h>
#  include <thrust/system/cuda/experimental/pinned_allocator.h>
#endif

#include <thrust/copy.h>
#include <thrust/device_ptr.h>
#include <thrust/device_vector.h>
#include <thrust/functional.h>
#include <thrust/host_vector.h>
#include <thrust/reduce.h>
#include <thrust/sequence.h>
#include <thrust/transform.h>
#include <thrust/version.h>

#ifdef WIN32
#  include <windows.h>
#else
#  include <unistd.h>
#endif

namespace lgx {

// Progress bar
#if ((defined(WIN32) || defined(WIN64)) && defined(MINGW))
static bool progAdvance(int step) {
  return true;
}
static void progStart(const std::string& msg, int steps) {
}
static void progSetMsg(const std::string& msg) {
}
static bool progCancelled() {
  return false;
}
static void progStop() {
}
#else
extern bool progAdvance(int step);
extern void progStart(const std::string& msg, int steps);
extern void progSetMsg(const std::string& msg);
extern bool progCancelled();
extern void progStop();
#endif

/**
 * \namespace cuda
 *
 * Namespace containing any typedefs, classes and functions for cuda
 */
namespace cuda {

typedef unsigned char uchar;
typedef unsigned int uint;
typedef unsigned short ushort;
typedef thrust::host_vector<float> HVecF;
typedef thrust::host_vector<uint> HVecU;
typedef thrust::host_vector<uchar> HVecUC;
typedef thrust::host_vector<ushort> HVecUS;
typedef thrust::host_vector<Point2i> HVec2I;
typedef thrust::host_vector<Point3u> HVec3U;
typedef thrust::host_vector<Point3f> HVec3F;
typedef thrust::host_vector<Point4f> HVec4F;
typedef thrust::host_vector<Point6f> HVec6F;
typedef thrust::host_vector<Matrix4f> HVec4MF;
typedef thrust::device_vector<float> DVecF;
typedef thrust::device_vector<uint> DVecU;
typedef thrust::device_vector<ushort> DVecUS;
typedef thrust::device_vector<Point2i> DVec2I;
typedef thrust::device_vector<Point3u> DVec3U;
typedef thrust::device_vector<Point2f> DVec2F;
typedef thrust::device_vector<Point3f> DVec3F;
typedef thrust::device_vector<Point4f> DVec4F;
typedef thrust::device_vector<Point6f> DVec6F;
typedef thrust::device_vector<Matrix4f> DVec4MF;

// This requires changing after thrust is updated everywhere
#if THRUST_VERSION >= 100600
typedef thrust::counting_iterator<int, thrust::device_system_tag> DCountIter;
#else
typedef thrust::counting_iterator<int, thrust::device_space_tag> DCountIter;
#endif

// Dimensions
enum DimEnum { X, Y, Z, XY, YZ, XZ, XYZ };

// Check for Cuda errors
int checkCudaError(const std::string& msg);
uint getThreadCount(uint threadPos, uint totThreads);
int errMsg(const std::string& s);
size_t userMem();
#define MByte size_t(1024 * 1024)

// Get a device pointer
template <typename T> T* devP(thrust::device_vector<T>& DVec) {
  return (&DVec[0]).get();
}

template <typename T> const T* devP(const thrust::device_vector<T>& DVec) {
  return (&DVec[0]).get();
}

#ifdef THRUST_BACKEND_CUDA
// Get a device pointer to pinned host memory
template <typename T> T* devHP(thrust::host_vector<T, thrust::cuda::experimental::pinned_allocator<T> >& DVec)
{
  T* result;
  cudaHostGetDevicePointer(&result, (void*)(&DVec[0]), 0);
  return (result);
}

template <typename T> const T* devHP(const thrust::host_vector<T, thrust::cuda::experimental::pinned_allocator<T> >& DVec)
{
  const T* result;
  cudaHostGetDevicePointer(&result, (void*)(&DVec[0]), 0);
  return (result);
}
#endif

// Find a 1D Gaussian mask (normalized to sum up to 1)
inline void getGaussianMask1D(float step, float sigma, HVecF& mask, uint& radius)
{
  // Find mask
  float sigmaSqr = sigma * sigma;
  float rad = .5f + 3.0f * sigma / step;
  radius = (rad <= 0 ? 0 : int(rad));
  if(radius <= 0)
    return;

  float normalize = 0;
  mask.resize(2 * radius + 1);
  int j = 0;
  for(uint u = 0; u <= 2*radius; ++u) {
    float x = (float(u) - radius) * step;
    double alpha = exp(-(x * x) / (2.0 * sigmaSqr));
    float val = alpha / (2.0f * M_PI * sigma);
    mask[j++] = val;
    normalize += val;
  }
  if(normalize <= 0)
    return;
  for(size_t i = 0; i < mask.size(); ++i)
    mask[i] /= normalize;
}

// Normalize 1D Mask
inline void normalizeMaskScale(HVecF& mask)
{
  if(mask.empty())
    return;

  float normalize = 0;
  for(size_t i = 0; i < mask.size(); ++i)
    normalize += mask[i];

  if(normalize > 0)
    for(size_t i = 0; i < mask.size(); ++i)
      mask[i] /= normalize;
}

// Go from image coordinates to world coordinates (centers of voxels)
__device__ __host__ inline Point3f imageToWorld(const Point3i& img, const Point3f& step, const Point3f& shift)
{
  return multiply(Point3f(img) + Point3f(.5f, .5f, .5f), step) + shift;
}

__device__ __host__ inline Point3f imageToWorld(const Point3u& img, const Point3f& step, const Point3f& shift)
{
  return multiply(Point3f(img) + Point3f(.5f, .5f, .5f), step) + shift;
}

// Go from world coordinates to image coordinates
__device__ __host__ inline Point3i worldToImage(const Point3f& wrld, const Point3f& step, const Point3f& shift)
{
  return Point3i((wrld - shift) / step - Point3f(.5f, .5f, .5f));
}

// Compute offset in stack
__device__ __host__ inline size_t offset(size_t x, size_t y, size_t z, size_t xsz, size_t ysz)
{
  size_t result = z;
  result *= ysz;
  result += y;
  result *= xsz;
  result += x;
  return result;
}

__device__ __host__ inline size_t offset(size_t x, size_t y, size_t z, const Point3u& size)
{
  size_t result = z;
  result *= size.y();
  result += y;
  result *= size.x();
  result += x;
  return result;
}

__device__ __host__ inline size_t offset(const Point3u p, const Point3u& size)
{
  size_t result = p.z();
  result *= size.y();
  result += p.y();
  result *= size.x();
  result += p.x();
  return result;
}
__device__ __host__ inline void getXY(size_t idx, const Point3u& size, int& x, int& y)
{
  x = idx % size.x();
  y = idx / size.x();
}

__device__ __host__ inline void getXZ(size_t idx, const Point3u& size, int& x, int& z)
{
  x = idx % size.x();
  z = idx / size.x();
}

__device__ __host__ inline void getYZ(size_t idx, const Point3u& size, int& y, int& z)
{
  y = idx % size.y();
  z = idx / size.y();
}

__device__ __host__ inline void getXYZ(size_t idx, const Point3u& size, int& x, int& y, int& z)
{
  x = idx % size.x();
  y = idx / size.x() % size.y();
  z = idx / size.x() / size.y();
}

// Compute stride
__device__ __host__ inline Point3u getStride(const Point3u& size)
{
  return (Point3u(1, size.x(), size.x() * size.y()));
}

// Clip point to clipping planes
__device__ __host__ inline bool clipPoint(const Point3f& wp, const Point3u& clipDo, const Point4f* pn)
{
  bool clip = false;
  Point4f p4 = Point4f(wp.x(), wp.y(), wp.z(), 1.0);
  for(int i = 0; i < 3; i++)
    if(clipDo[i]) {
      // Test if inside clipping planes
      for(int k = 0; k < 2; k++)
        if(pn[i * 2 + k] * p4 < 0)
          clip = true;
    }
  return (clip);
}

// --- Cuda kernels ---

// Average
struct pixAverage {
  int acc;
  int count;

  __device__ __host__ void init() {
    acc = count = 0;
  }

  __device__ __host__ void data(int val, int)
  {
    acc += val;
    count++;
  }

  __device__ __host__ ushort result() {
    return ushort(acc / count);
  }
};
typedef struct pixAverage AverageKernel;

// Dilate
struct pixDilate {
  int acc;

  __device__ __host__ void init() {
    acc = 0;
  }

  __device__ __host__ void data(int val, int)
  {
    if(val > acc)
      acc = val;
  }

  __device__ __host__ ushort result() {
    return ushort(acc);
  }
};
typedef struct pixDilate DilateKernel;

// Erode
struct pixErodeLabels {
  int acc;

  __device__ __host__ void init() {
    acc = -1;
  }

  __device__ __host__ void data(int val, int)
  {
    if(acc < 0)
      acc = val;
    else if(val != acc)
      acc = 0;
  }

  __device__ __host__ ushort result() {
    return ushort(acc);
  }
};
typedef struct pixErodeLabels ErodeLabelsKernel;

// Erode
struct pixErode {
  int acc;

  __device__ __host__ void init() {
    acc = 65536;
  }

  __device__ __host__ void data(int val, int)
  {
    if(val < acc)
      acc = val;
  }

  __device__ __host__ ushort result() {
    return ushort(acc);
  }
};
typedef struct pixErode ErodeKernel;

// Generic mask
struct pixMask {
  float acc;
  const float* mask;

  pixMask(const float* _mask)
    : mask(_mask)
  {
  }

  __device__ __host__ void init() {
    acc = 0;
  }

  __device__ __host__ void data(int val, int pos) {
    acc += val * mask[pos];
  }

  __device__ __host__ ushort result()
  {
    if(acc <= 0)
      return 0;
    else if(acc >= 65535.0)
      return 65535;
    else
      return ushort(acc + 0.5);
  }
};
typedef struct pixMask MaskKernel;

// Separable operation data along X axis
template <typename opT, typename T> struct sepIterX {
  opT pixOP;
  uint radius;

  Point3u size;
  Point3u stride;
  T* Src;
  T* Dst;

  sepIterX(opT _pixOP, uint _radius, thrust::device_vector<T>& _Src, thrust::device_vector<T>& _Dst)
    : pixOP(_pixOP)
    , radius(_radius)
    , Src(devP(_Src))
    , Dst(devP(_Dst))
  {
  }

  void setSizes(const Point3u&, const Point3u& _size)
  {
    size = _size;
    stride = getStride(size);
  }

  __device__ __host__ void operator()(uint idx)
  {
    int x, y;
    getXY(idx, size, x, y);

    int xs = (x < radius ? 0 : x - radius), xe = x + radius + 1;
    if(xs < 0)
      xs = 0;
    if(xe > size.x())
      xe = size.x();
    size_t zIdx = offset(x, y, 0, size);
    int maskinit = radius - (x - xs);     // Calculate index into mask

    for(int z = 0; z < size.z(); ++z, zIdx += stride.z()) {
      pixOP.init();
      size_t xIdx = offset(xs, y, z, size);
      int maskpos = maskinit;
      for(int xi = xs; xi < xe; ++xi, xIdx += stride.x())
        pixOP.data(Src[xIdx], maskpos++);
      Dst[zIdx] = pixOP.result();
    }
  }
};

// Separable operation along Y axis
template <typename opT, typename T> struct sepIterY {
  opT pixOP;
  int radius;

  Point3u size;
  Point3u stride;
  T* Src;
  T* Dst;

  sepIterY(opT _pixOP, int _radius, thrust::device_vector<T>& _Src, thrust::device_vector<T>& _Dst)
    : pixOP(_pixOP)
    , radius(_radius)
    , Src(devP(_Src))
    , Dst(devP(_Dst))
  {
  }

  void setSizes(const Point3u&, const Point3u& _size)
  {
    size = _size;
    stride = getStride(size);
  }

  __device__ __host__ void operator()(uint idx)
  {
    int x, y;
    getXY(idx, size, x, y);

    int ys = y - radius, ye = y + radius + 1;
    if(ys < 0)
      ys = 0;
    if(ye > size.y())
      ye = size.y();
    size_t zIdx = offset(x, y, 0, size);
    int maskinit = radius - (y - ys);     // Calculate index into mask

    for(int z = 0; z < size.z(); ++z, zIdx += stride.z()) {
      pixOP.init();
      int maskpos = maskinit;
      size_t yIdx = offset(x, ys, z, size);
      for(int yi = ys; yi < ye; ++yi, yIdx += stride.y())
        pixOP.data(Src[yIdx], maskpos++);
      Dst[zIdx] = pixOP.result();
    }
  }
};

// Separable operation along Z axis, uses XZ slices
// template <typename opT, typename T>
// struct sepIterZ
//{
//  opT pixOP;
//  int radius;
//
//  Point3u size;
//  Point3u stride;
//  T *Src;
//  T *Dst;
//
//  sepIterZ(opT _pixOP, int _radius, thrust::device_vector<T> *_Src, thrust::device_vector<T> *_Dst)
//    : pixOP(_pixOP), radius(_radius), Src(devP(*_Src)), Dst(devP(*_Dst)) {}
//
//  void setSizes(const Point3u &base, const Point3u &_size)
//  {
//    size = _size;
//    stride = getStride(size);
//  }
//
//  __device__ __host__
//  void operator()(uint idx)
//  {
//    int x, z;
//    getXZ(idx, size, x, z);
//
//    int zs = z - radius, ze = z + radius + 1;
//
//    if(zs < 0) zs = 0;
//    if(ze > size.z()) ze = size.z();
//    size_t yIdx = offset(x,0,z,size);
//    int maskinit = radius - (z - zs);          // Calculate index into mask
//
//    for(int y = 0; y < size.y(); ++y, yIdx += stride.y()) {
//      pixOP.init();
//      int maskpos = maskinit;
//      size_t zIdx = offset(x,y,zs,size);
//      for(int zi = zs; zi < ze; ++zi, zIdx += stride.z())
//        pixOP.data(Src[zIdx], maskpos++);
//      Dst[yIdx] = pixOP.result();
//    }
//  }
//};

// Separable operation along Z axis with XY-slices
template <typename opT, typename T> struct sepIterZ {
  opT pixOP;
  int radius;

  Point3u size;
  Point3u stride;
  T* Src;
  T* Dst;

  sepIterZ(opT _pixOP, int _radius, thrust::device_vector<T>& _Src, thrust::device_vector<T>& _Dst)
    : pixOP(_pixOP)
    , radius(_radius)
    , Src(devP(_Src))
    , Dst(devP(_Dst))
  {
  }

  void setSizes(const Point3u&, const Point3u& _size)
  {
    size = _size;
    stride = getStride(size);
  }

  __device__ __host__ void operator()(uint idx)
  {
    int x, y;
    getXY(idx, size, x, y);

    size_t zOff = offset(x, y, 0, size);

    for(int z = 0; z < size.z(); ++z, zOff += stride.z()) {
      int zs = z - radius, ze = z + radius + 1;
      if(zs < 0)
        zs = 0;
      if(ze > size.z())
        ze = size.z();

      pixOP.init();
      int maskpos = radius - (z - zs);       // Calculate index into mask

      size_t zIdx = offset(x, y, zs, size);
      for(int zi = zs; zi < ze; ++zi, zIdx += stride.z())
        pixOP.data(Src[zIdx], maskpos++);
      Dst[zOff] = pixOP.result();
    }
  }
};

// Flip an image
template <typename T> void flipImage(DimEnum dim, Point3u imgSize, Point3u& flipImgSize, const T& src, T& dst)
{
  if(dim == XY) {
    flipImgSize = Point3u(imgSize.y(), imgSize.x(), imgSize.z());
    size_t idx = 0;
    for(int z = 0; z < imgSize.z(); ++z)
      for(int y = 0; y < imgSize.y(); ++y)
        for(int x = 0; x < imgSize.x(); ++x)
          dst[offset(y, x, z, flipImgSize)] = src[idx++];
  } else if(dim == YZ) {
    flipImgSize = Point3u(imgSize.x(), imgSize.z(), imgSize.y());
    size_t idx = 0;
    for(int z = 0; z < imgSize.z(); ++z)
      for(int y = 0; y < imgSize.y(); ++y)
        for(int x = 0; x < imgSize.x(); ++x)
          dst[offset(x, z, y, flipImgSize)] = src[idx++];
  } else if(dim == XZ) {
    flipImgSize = Point3u(imgSize.z(), imgSize.y(), imgSize.x());
    size_t idx = 0;
    for(int z = 0; z < imgSize.z(); ++z)
      for(int y = 0; y < imgSize.y(); ++y)
        for(int x = 0; x < imgSize.x(); ++x)
          dst[offset(z, y, x, flipImgSize)] = src[idx++];
  } else
    std::cout << "Error in flipImage: unknown dimension" << std::endl;
}

// Calculate threads
inline uint getTotalThreads(DimEnum dim, const Point3u size)
{
  switch(dim) {
  case X:
    return size.x();
  case Y:
    return size.y();
  case Z:
    return size.z();
  case XY:
    return size.x() * size.y();
  case XZ:
    return size.x() * size.z();
  case YZ:
    return size.y() * size.z();
  case XYZ:
    return size.x() * size.y() * size.z();
  }
  return 0;
}

// Check bounds
inline int checkBounds(const Point3u& imgSize, const Point3u& base, const Point3u& size)
{
  if(base.x() + size.x() > imgSize.x() || base.y() + size.y() > imgSize.y() || base.z() + size.z() > imgSize.z())
    return errMsg("checkbounds:size > image size");

  return (0);
}

// Divide stack into sections to limit memory usage.
inline uint getStep(uint pos, uint maxpos, size_t stride, size_t szof, size_t mem)
{
  if(pos >= maxpos)
    return (0);

  // Check parms
  if(stride * szof == 0)
    throw(std::string("getStep: Bad stride (=0)"));

  // See how many slices can be done, do in chunks to limit memory usage.
  uint step = mem / (stride * szof);
  if(step == 0)
    throw(std::string("getStep: Can't process enough data, try increasing CudaHoldMem"));

  if(pos + step > maxpos)
    step = maxpos - pos;

  return (step);
}

#ifdef THRUST_BACKEND_CUDA
// Copy sections of stack data to device,
template <typename T>
int copyToDevice(const Point3u& imgSize, const Point3u& base, const Point3u& size, const thrust::host_vector<T>& Hvec,
                 thrust::device_vector<T>& Dvec)
{
  // Sync threads
  cudaThreadSynchronize();
  checkCudaError("copyToDevice::Error in initialization.");

  // Check bounds
  if(checkBounds(imgSize, base, size) > 0)
    return 1;

  // Check host vector big enough
  if(Hvec.size() < (size_t(base.z()) + size.z()) * imgSize.y() * imgSize.x())
    throw(std::string("copyToDevice:Host vector too small"));

  // Check device vector big enough
  if(Dvec.size() < size_t(size.x()) * size.y() * size.z())
    throw(std::string("copyToDevice:Device vector too small"));

  if(base.x() == 0 && base.y() == 0 && size.x() == imgSize.x() && size.y() == imgSize.y())
    // If src && dest have same xy geometry, copy directly to device
    thrust::copy(Hvec.begin() + size_t(base.z()) * imgSize.x() * imgSize.y(),
                 Hvec.begin() + size_t(base.z() + size.z()) * imgSize.x() * imgSize.y(), Dvec.begin());

  else {
    // If not, use a temp host variable
    thrust::host_vector<ushort> Htmp(size.x() * size.y() * size.z());
    size_t dstIdx = 0;
    for(int z = base.z(); z < base.z() + size.z(); ++z)
      for(int y = base.y(); y < base.y() + size.y(); ++y) {
        size_t srcIdx = offset(base.x(), y, z, imgSize);
        for(int x = base.x(); x < base.x() + size.x(); ++x)
          Htmp[dstIdx++] = Hvec[srcIdx++];
      }
    thrust::copy(Htmp.begin(), Htmp.end(), Dvec.begin());
  }
  checkCudaError("copyToDevice:Error copying to device.");
  return 0;
}

// Copy sections of stack data from device,
template <typename T>
int copyToHost(const Point3u& imgSize, const Point3u& base, const Point3u& size, const thrust::device_vector<T>& Dvec,
               thrust::host_vector<T>& Hvec)
{
  // Sync threads
  cudaThreadSynchronize();
  checkCudaError("copyToHost::Error in initialization.");

  // Check bounds
  if(checkBounds(imgSize, base, size))
    return 1;

  // Check host vector big enough
  if(Hvec.size() < (size_t(base.z()) + size.z()) * imgSize.y() * imgSize.x())
    throw(std::string("copyToHost:Host vector too small"));

  // Check device vector size
  if(Dvec.size() < size_t(size.x()) * size.y() * size.z())
    throw(std::string("copyToHost:Device vector too small"));

  if(base.x() == 0 && base.y() == 0 && size.x() == imgSize.x() && size.y() == imgSize.y())
    // If src && dest have same xy geometry, copy directly to host
    thrust::copy(Dvec.begin(), Dvec.begin() + size_t(size.z()) * size.x() * size.y(),
                 Hvec.begin() + size_t(base.z()) * imgSize.x() * imgSize.y());
  else {
    // If not, use a temp host variable
    thrust::host_vector<ushort> Htmp(size_t(size.x()) * size.y() * size.z());
    thrust::copy(Dvec.begin(), Dvec.begin() + Htmp.size(), Htmp.begin());
    size_t srcIdx = 0;
    for(int z = base.z(); z < base.z() + size.z(); ++z)
      for(int y = base.y(); y < base.y() + size.y(); ++y) {
        size_t dstIdx = offset(base.x(), y, z, imgSize);
        for(int x = base.x(); x < base.x() + size.x(); ++x)
          Hvec[dstIdx++] = Htmp[srcIdx++];
      }
  }
  checkCudaError("copyToHost::Error copying fdrom device to host.");
  return 0;
}
#endif

// Class to process generic command using XY blocks with padding
class processOP {
public:
  processOP(Point3u _imgSize, int _pad, size_t _sizeOf)
    : sizeOf(_sizeOf)
    , imgSize(_imgSize)
    , pad(_pad)
    , baseZ(0)
  {
    mem = userMem();     // Set at start and don't change
    calcSize();
    passesReq = (int)ceil(float(imgSize.z()) / sizeZ);
    if(passesReq > 1)
      std::cout << "Blocking data to GPU, "
                << int(1.1 * sizeOf * imgSize.x() * imgSize.y() * imgSize.z() / (1024 * 1024)) << " MB required, "
                << mem / MByte << " MB available (" << sizeZ << " slices)" << std::endl;
  }

  template <typename T> void alloc(T& Dvec)
  {
    // Setup device storage
    size_t size = imgSize.x() * imgSize.y() * padSizeZ;
    if(Dvec.size() != size) {
      Dvec.resize(size);
      checkCudaError("processOp.alloc:Error allocating device vector.");
    }
  }

  template <typename HT, typename DT> void write(const HT& Hvec, DT& Dvec)
  {
    // Copy source data to device
    alloc(Dvec);
    thrust::copy(Hvec.begin() + padBaseZ * imgSize.x() * imgSize.y(),
                 Hvec.begin() + (padBaseZ + padSizeZ) * imgSize.x() * imgSize.y(), Dvec.begin());
    checkCudaError("processOp.write:Error copying data to device.");
  }

  // Launch threads to process a command that has the device vectors already allocated
  template <typename T> bool launch(T op, DimEnum dim)
  {
    // Set up operator pointers to data
    op.setSizes(Point3u(0, 0, 0), Point3u(imgSize.x(), imgSize.y(), padSizeZ));
    uint maxThreads = 0;
    uint totThreads = getTotalThreads(dim, Point3u(imgSize.x(), imgSize.y(), padSizeZ));
    for(uint threadPos = 0; threadPos < totThreads; ) {
      // Calculate threads
      uint threads = getThreadCount(threadPos, totThreads);
      if(maxThreads < threads)
        maxThreads = threads;
      // Launch threads
      DCountIter first(threadPos), last(threadPos + threads);
      thrust::for_each(first, last, op);
      checkCudaError("processOp.launchDeviceOP:launch failure.");
      if(!progAdvance(-1))
        return errMsg("Operation canceled") != 0; // VC++ complains about converting int to bool
      threadPos += threads;
    }
    return false;
  }

  template <typename HT, typename DT> void read(const DT& Dvec, HT& Hvec)
  {
    // Copy results back to host, only works if x and y are not padded.
    thrust::copy(Dvec.begin() + (baseZ - padBaseZ) * imgSize.x() * imgSize.y(),
                 Dvec.begin() + (baseZ - padBaseZ + sizeZ) * imgSize.x() * imgSize.y(),
                 Hvec.begin() + baseZ * imgSize.x() * imgSize.y());
    checkCudaError("processOp.read:Error copying data to host.");
  }

  int next()
  {
    baseZ += sizeZ;
    calcSize();
    return imgSize.z() - baseZ;
  }

  void rewind()
  {
    baseZ = 0;
    calcSize();
  }

  int passes() {
    return passesReq;
  }

private:
  int calcSize()
  {
    // Finished, nothing to do
    if(baseZ >= imgSize.z())
      return 0;

    // Calculate padding at the base
    padBaseZ = baseZ;
    padBaseZ -= pad;
    if(padBaseZ < 0)
      padBaseZ = 0;

    // Calcaulate how much we can do.
    padSizeZ = getStep(padBaseZ, imgSize.z(), imgSize.x() * imgSize.y(), sizeOf, mem);

    // Adjust size for padding
    sizeZ = padSizeZ;
    if(padBaseZ + padSizeZ < imgSize.z())
      sizeZ -= pad;
    sizeZ -= baseZ - padBaseZ;

    // Not enough memory for one pass
    if(sizeZ <= 0)
      throw(std::string("processOP.calcSize: Can't process enough data, try increasing cudaHoldMem"));

    return sizeZ;
  }

  size_t sizeOf;
  size_t mem;
  Point3u imgSize;
  int pad;
  int baseZ;
  int sizeZ;
  int padBaseZ;
  int padSizeZ;
  int passesReq;
};

// Process separable operators
template <typename opT, typename T>
int processSepOP(opT opx, opT opy, opT opz, const Point3u& imgSize, const Point3u& radius,
                 const thrust::host_vector<T>& srcData, thrust::host_vector<T>& dstData)
{
  if(radius.x() == 0 && radius.y() == 0 && radius.z() == 0) {
    dstData = srcData;
    return 0;
  }
  thrust::host_vector<T> Hsrc(srcData);

  // Loop over sections of XY slices
  processOP proc(imgSize, radius.z(), sizeof(T) * 2);
  thrust::device_vector<T> DVec1, DVec2;
  thrust::device_vector<T>* Dsrc = &DVec1, *Ddst = &DVec2;
  do {
    proc.write(Hsrc, *Dsrc);
    proc.alloc(DVec2);

    if(radius.x() > 0) {
      if(proc.launch(sepIterX<opT, T>(opx, radius.x(), *Dsrc, *Ddst), XY))
        return 1;
      std::swap(Dsrc, Ddst);
    }
    if(radius.y() > 0) {
      if(proc.launch(sepIterY<opT, T>(opy, radius.y(), *Dsrc, *Ddst), XY))
        return 1;
      std::swap(Dsrc, Ddst);
    }
    if(radius.z() > 0) {
      if(proc.launch(sepIterZ<opT, T>(opz, radius.z(), *Dsrc, *Ddst), XY))
        return 1;
      std::swap(Dsrc, Ddst);
    }

    proc.read(*Dsrc, dstData);
  } while(proc.next());

  return (0);
}
} // namespace cuda
} // namespace lgx
#endif
