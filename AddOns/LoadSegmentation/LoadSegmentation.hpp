/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef LOADSEGMENTATION_HPP
#define LOADSEGMENTATION_HPP

#include <Process.hpp>
#include <Stack.hpp>
#include <Store.hpp>
#include <Misc.hpp>

namespace lgx {
namespace process {
// Gamma filter on mesh signal, inherit from mesh process
class LoadSegmentation : public StackProcess {
public:
  LoadSegmentation(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool initialize(ParmList& parms, QWidget* parent) override;

  // Standard call interface for all processes
  bool operator()(const ParmList& parms) override
  {
    Store* output = currentStack()->currentStore();

    auto fields = util::splitFields(parms[1].toString());
    std::vector<double> factors(fields.size());
    bool ok;
    for(int i = 0 ; i < factors.size() ; ++i) {
      factors[i] = fields[i].toDouble(&ok);
      if(not ok)
        return setErrorMessage("Error, the 'Channel factors' must be a list of floating point values.");
    }

    bool res = (*this)(currentStack(), output, parms[0].toString(), factors);
    if(res) {
      output->show();
      output->setLabels(true);
    }
    return res;
  }

  // Call interface with parameters used
  bool operator()(Stack *stk, Store* output, QString filename, const std::vector<double>& factors);

  // Plug-in folder
  QString folder() const override {
    return "System";
  }
  // Plug-in name
  QString name() const override {
    return "Load Segmented Image";
  }
  // Plug-in long description
  QString description() const override {
    return "Load an image and create a labeled one.\n"
           "If the image is multi-channel, it will combine them as described by the channels factors.\n"
           "For the computation: all channels are loaded, then for each voxel, the values are converted to float, "
           "combined and converted back to unsigned 16 bits values.";
  }
  // List of parameter names
  QStringList parmNames() const override {
    return QStringList() << "Filename"
                         << "Channels Factors";
  }
  // List of parameter long descriptions
  QStringList parmDescs() const override {
    return QStringList() << "File with segmentation."
                         << "Multiplication factors for each channels. Any non-specified channel will be set to 0.\n"
                            "The default '1 256 1' use all channels, assuming that either blue or red are used, not both.";
  }
  // List of parameter default values
  ParmList parmDefaults() const override {
    return ParmList() << "" << "1 256 1";
  }
  // Plug-in icon
  QIcon icon() const override {
    return QIcon(":/images/open.png");
  }
};
}
}
#endif
