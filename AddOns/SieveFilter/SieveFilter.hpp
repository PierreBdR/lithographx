/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef SIEVE_FILTER_HPP
#define SIEVE_FILTER_HPP

#include <Process.hpp>
#include <Stack.hpp>
#include <Store.hpp>
#include <Misc.hpp>

#include "Sieve.hpp"

namespace lgx {
class Progress;

namespace process {

// Gamma filter on mesh signal, inherit from mesh process
class SieveFilter : public StackProcess {
public:
  SieveFilter(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  // Standard call interface for all processes
  bool operator()(const ParmList& parms) override
  {
    if(!checkState().store(STORE_NON_EMPTY))
      return false;
    Store* input = currentStack()->currentStore();
    Store* output = currentStack()->work();
    bool ok;
    float size = parms[1].toFloat(&ok);
    if(not ok or size < 0)
      return setErrorMessage("Error, size parameter must be a positive number.");
    bool res = (*this)(input, output, parms[0].toString(), size, stringToBool(parms[2].toString()));
    if(res) {
      input->hide();
      output->show();
    }
    return res;
  }

  // Call interface with parameters used
  bool operator()(const Store* input, Store* output, const QString type_str, float size, bool fully_connected);

  // Plug-in folder
  QString folder() const override {
    return "Morphology";
  }
  // Plug-in name
  QString name() const override {
    return "Sieve Filter";
  }
  // Plug-in long description
  QString description() const override {
    return "Implementation of the sieve filter.\n"
           "A sieve recursively remove extrema, constructing regions of "
           "increasing size as it goes and stopping when the target area is "
           "reached. In the end, each minimum region gets the value of its maximum value pixel "
           "while each maximum region gets the value of its minimum pixel.\n"
           "There are three basic type of filtering:\n"
           " - Opening: process maxima only\n"
           " - Closing: process minima only\n"
           " - Median: process both minima and maxima at the same time\n"
           "In addition 'OpenClose' perform an opening followed by a closing and 'CloseOpen' does the opposite.\n"
           "\n"
           "Reference:\n"
           "Bangham, J. A.; Harvey, R.; Ling, P. D. & Aldridge, R. V. Nonlinear "
           "scale-space from n-dimensional sieves Computer Vision — ECCV'96, "
           "4th European Conference on Computer Vision Cambridge, "
           "Springer Science + Business Media, 1996, 1, 187–198";
  }
  // List of parameter names
  QStringList parmNames() const override {
    return QStringList() << "Type"
                         << QString("Size (%1/%2)").arg(UM2).arg(UM3)
                         << "Fully Connected";
  }
  // List of parameter long descriptions
  QStringList parmDescs() const override {
    return QStringList() << "Type of filte, must be one of: Median, Opening, Closing, OpenClose or CloseOpen"
                         << "Volume or area of the filter, depending on the dimension of the image"
                         << "If true, voxels are neighbors even if they shared only an edge or point, "
                            "if false, only voxels sharing a face are neighbors.";
  }
  // List of parameter default values
  ParmList parmDefaults() const override {
    return ParmList() << "Median"
                      << 1
                      << false;
  }
  // Choices for the parameters
  ParmChoiceMap parmChoice() const override {
    ParmChoiceMap map;
    map[0] = QStringList() << "Median" << "Opening" << "Closing" << "OpenClose" << "CloseOpen";
    map[2] = booleanChoice();
    return map;
  }
  // Plug-in icon
  QIcon icon() const override {
    return QIcon(":/images/Sieve.png");
  }

private:
  template <typename Sieve>
  bool applySieve(Sieve& sieve, HVecUS& output, sieve::FILTER_TYPE filter, intptr_t int_area, Progress& progress);

};
}
}
#endif
