uniform sampler2D front;
uniform sampler2D back;

smooth centroid in vec2 texPos;

vec4 combineColor(vec4 back, vec4 front) // in premultiplied!
{
  vec4 res;
  res.a = back.a*front.a;
  res.rgb = front.a*back.rgb + front.rgb;
  return res;
}

void main()
{
  vec4 front_col = premulColor(texture(front, texPos));
  vec4 back_col = premulColor(texture(back, texPos));

  vec4 col = combineColor(back_col, front_col);

  gl_FragColor = normalColor(col);
}

