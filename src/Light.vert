smooth centroid out vec3 nor;
smooth centroid out vec3 lightPos;

void light()
{
  nor = gl_NormalMatrix * gl_Normal;
  lightPos = gl_Vertex.xyz / gl_Vertex.w;
}
