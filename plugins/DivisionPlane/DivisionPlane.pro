MGX_LIBS=$$system(MorphoGraphX --libs)
MGX_INCLUDE=$$system(MorphoGraphX --include)
MGX_PROCESSES=$$system(MorphoGraphX --process)
CUDA_INCLUDE=/usr/local/cuda/include
CIMG_INCLUDE=

TEMPLATE = lib

TARGET=DivisionPlane

DEFINES += cimg_display=0
SOURCES += DivisionPlane.cpp
HEADERS += DivisionPlane.hpp
RESOURCES = DivisionPlane.qrc

CONFIG += qt release plugin
QT += xml opengl

INCLUDEPATH += $$MGX_INCLUDE $$MGX_PROCESSES/include $$CUDA_INCLUDE $$CIMG_INCLUDE
macx: {
LIBS += -F$$MGX_LIBS -framework mgx
} else {
LIBS += -L$$MGX_LIBS -lmgx
}

LIBS += -lgsl -lgslcblas

target.path = $$MGX_PROCESSES
INSTALLS = target


