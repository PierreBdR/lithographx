<tagfile>
  <compound kind="class">
    <name>AxisPlaneConstraint</name>
    <filename>classqglviewer_1_1AxisPlaneConstraint.html</filename>
  </compound>
  <compound kind="class">
    <name>qglviewer::AxisPlaneConstraint</name>
    <base>qglviewer::Constraint</base>
    <base>Constraint</base>
    <filename>classqglviewer_1_1AxisPlaneConstraint.html</filename>
    <member kind="function">
      <name>AxisPlaneConstraint::Type</name>
      <anchor>1d1cfd8ffb84e947f82999c682b666a7</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <name>REE</name>
      <anchor>1d1cfd8ffb84e947f82999c682b666a7cc62d1576546f3245237e1b232d838b6</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <name>XIS</name>
      <anchor>1d1cfd8ffb84e947f82999c682b666a71ad785f7d0b0b3a5a52cdd4385785a6b</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <name>LANE</name>
      <anchor>1d1cfd8ffb84e947f82999c682b666a767572d1492c84d8d417b61e864f13f24</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <name>ORBIDDEN</name>
      <anchor>1d1cfd8ffb84e947f82999c682b666a74b4068e636cd02a6e87e8d3920383d67</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <name>AxisPlaneConstraint::AxisPlaneConstraint</name>
      <anchor>1049b4e70e2fc0d46b4cfaf93d167515</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <name>AxisPlaneConstraint::~AxisPlaneConstraint</name>
      <anchor>88334a0bc0770afe6bd3af525f70ad9f</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <name>AxisPlaneConstraint::constrainTranslation</name>
      <anchor>b7d724965c6765209f47c1abe7f7b7b4</anchor>
      <arglist>(Vec &amp;translation, Frame *const frame)</arglist>
    </member>
    <member kind="function">
      <name>AxisPlaneConstraint::setTranslationConstraint</name>
      <anchor>722c30f7303a078681fbf516499579de</anchor>
      <arglist>(Type type, const Vec &amp;direction)</arglist>
    </member>
    <member kind="function">
      <name>AxisPlaneConstraint::setTranslationConstraintType</name>
      <anchor>0cee03d4db5722e992c20f042601eaa5</anchor>
      <arglist>(Type type)</arglist>
    </member>
    <member kind="function">
      <name>AxisPlaneConstraint::setTranslationConstraintDirection</name>
      <anchor>2b676c088af158a52724bbbab15d1a65</anchor>
      <arglist>(const Vec &amp;direction)</arglist>
    </member>
    <member kind="function">
      <name>AxisPlaneConstraint::translationConstraintType</name>
      <anchor>12ad675910ffa86fbabefff2cd6a594a</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>AxisPlaneConstraint::translationConstraintDirection</name>
      <anchor>431b0d36842122de9db3b4e4f00b8d7c</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>AxisPlaneConstraint::constrainRotation</name>
      <anchor>5de5f38e75b58476b7926171dba4b31b</anchor>
      <arglist>(Quaternion &amp;rotation, Frame *const frame)</arglist>
    </member>
    <member kind="function">
      <name>AxisPlaneConstraint::setRotationConstraint</name>
      <anchor>116f0f394030f165b031287c1e1cf5ea</anchor>
      <arglist>(Type type, const Vec &amp;direction)</arglist>
    </member>
    <member kind="function">
      <name>AxisPlaneConstraint::setRotationConstraintType</name>
      <anchor>7c0dc83d6a770742719c8d3ffa67c4e5</anchor>
      <arglist>(Type type)</arglist>
    </member>
    <member kind="function">
      <name>AxisPlaneConstraint::setRotationConstraintDirection</name>
      <anchor>7cb6055005e69d6edac8fbb0d14dd0fa</anchor>
      <arglist>(const Vec &amp;direction)</arglist>
    </member>
    <member kind="function">
      <name>AxisPlaneConstraint::rotationConstraintType</name>
      <anchor>61a05b9e18948b2b787e355c90b7b85f</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>AxisPlaneConstraint::rotationConstraintDirection</name>
      <anchor>f982989dbeb2e0e63c85eecc09bd88d7</anchor>
      <arglist>() const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>CameraConstraint</name>
    <filename>classqglviewer_1_1CameraConstraint.html</filename>
  </compound>
  <compound kind="class">
    <name>qglviewer::CameraConstraint</name>
    <base>qglviewer::AxisPlaneConstraint</base>
    <base>AxisPlaneConstraint</base>
    <filename>classqglviewer_1_1CameraConstraint.html</filename>
    <member kind="function">
      <name>CameraConstraint::CameraConstraint</name>
      <anchor>38cba0f718402b8cd5ab74d36d7666b4</anchor>
      <arglist>(const Camera *const camera)</arglist>
    </member>
    <member kind="function">
      <name>CameraConstraint::~CameraConstraint</name>
      <anchor>b8570e4f91974b4bd5441297ab123cdb</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <name>CameraConstraint::constrainTranslation</name>
      <anchor>dc695bfbc605b5631be663b28a4ea9f6</anchor>
      <arglist>(Vec &amp;translation, Frame *const frame)</arglist>
    </member>
    <member kind="function">
      <name>CameraConstraint::constrainRotation</name>
      <anchor>71c099a2c356f715f8bf34052875cd25</anchor>
      <arglist>(Quaternion &amp;rotation, Frame *const frame)</arglist>
    </member>
    <member kind="function">
      <name>CameraConstraint::camera</name>
      <anchor>f5bc7c79df6e4f2f8290775c4fd8d47c</anchor>
      <arglist>() const </arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Camera</name>
    <filename>classqglviewer_1_1Camera.html</filename>
  </compound>
  <compound kind="class">
    <name>qglviewer::Camera</name>
    <filename>classqglviewer_1_1Camera.html</filename>
    <member kind="function">
      <name>Camera::Type</name>
      <anchor>1d1cfd8ffb84e947f82999c682b666a7</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <name>ERSPECTIVE</name>
      <anchor>1d1cfd8ffb84e947f82999c682b666a72c5d7801888c03752f28943ac85d805f</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <name>RTHOGRAPHIC</name>
      <anchor>1d1cfd8ffb84e947f82999c682b666a7e7bf29f117630a30ba5ffc75b33ac624</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <name>Camera::Camera</name>
      <anchor>a3f3efcb2fcc75de885df29041103cd2</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <name>Camera::~Camera</name>
      <anchor>b921e886e6f14e117eb8099ccb0a3775</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <name>Camera::Camera</name>
      <anchor>43d24da01076c4cea5f3dbde85e8482c</anchor>
      <arglist>(const Camera &amp;camera)</arglist>
    </member>
    <member kind="function">
      <name>Camera::operator=</name>
      <anchor>0f87e84ad3356493738d4fc8331c7362</anchor>
      <arglist>(const Camera &amp;camera)</arglist>
    </member>
    <member kind="function">
      <name>Camera::position</name>
      <anchor>bc38a05a597e07c9ff525122682adc50</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>Camera::upVector</name>
      <anchor>9cd2e746e7379b08fbaeea0ced76e1d7</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>Camera::viewDirection</name>
      <anchor>ac10e453c166209b4e6c14c0266651c7</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>Camera::rightVector</name>
      <anchor>99f766869c7cc9faaf9a5337db6f223c</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>Camera::orientation</name>
      <anchor>691af39b54669fad8b4e73599de22094</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>Camera::setFromModelViewMatrix</name>
      <anchor>b21e4c76205431bdeba645a5f258dce9</anchor>
      <arglist>(const GLdouble *const modelViewMatrix)</arglist>
    </member>
    <member kind="function">
      <name>Camera::setFromProjectionMatrix</name>
      <anchor>c49280735bce44665a4eabb166f10fcd</anchor>
      <arglist>(const float matrix[12])</arglist>
    </member>
    <member kind="function">
      <name>Camera::setPosition</name>
      <anchor>212edb01759f542f356c5d62e4a3f821</anchor>
      <arglist>(const Vec &amp;pos)</arglist>
    </member>
    <member kind="function">
      <name>Camera::setOrientation</name>
      <anchor>3862c502f5afc626af2ff582390bc868</anchor>
      <arglist>(const Quaternion &amp;q)</arglist>
    </member>
    <member kind="function">
      <name>Camera::setOrientation</name>
      <anchor>1842836742cf1ed8a33f32a863a5a05e</anchor>
      <arglist>(float theta, float phi)</arglist>
    </member>
    <member kind="function">
      <name>Camera::setUpVector</name>
      <anchor>b442b71a46297223ae12b163653eeb7e</anchor>
      <arglist>(const Vec &amp;up, bool noMove=true)</arglist>
    </member>
    <member kind="function">
      <name>Camera::setViewDirection</name>
      <anchor>066c4058970a008d0d0a8ff442d117f6</anchor>
      <arglist>(const Vec &amp;direction)</arglist>
    </member>
    <member kind="function">
      <name>Camera::lookAt</name>
      <anchor>afe147ffa75738c296c729d9b5026446</anchor>
      <arglist>(const Vec &amp;target)</arglist>
    </member>
    <member kind="function">
      <name>Camera::showEntireScene</name>
      <anchor>da8dd7d2346ebf46ed1f9822a8418df2</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <name>Camera::fitSphere</name>
      <anchor>424fbe98af0ca295c692d8d4ae73ceec</anchor>
      <arglist>(const Vec &amp;center, float radius)</arglist>
    </member>
    <member kind="function">
      <name>Camera::fitBoundingBox</name>
      <anchor>65a284702aab36f853d59ce6c7a082b9</anchor>
      <arglist>(const Vec &amp;min, const Vec &amp;max)</arglist>
    </member>
    <member kind="function">
      <name>Camera::fitScreenRegion</name>
      <anchor>c49a71148d1d501310026f6f6f76d471</anchor>
      <arglist>(const QRect &amp;rectangle)</arglist>
    </member>
    <member kind="function">
      <name>Camera::centerScene</name>
      <anchor>bf37eb8d64d09f93771b42b95cad00f6</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <name>Camera::interpolateToZoomOnPixel</name>
      <anchor>9e2b0c2d37f9176629d737faf431b51c</anchor>
      <arglist>(const QPoint &amp;pixel)</arglist>
    </member>
    <member kind="function">
      <name>Camera::interpolateToFitScene</name>
      <anchor>8a6201a1c2deca60a917e5351e549bb0</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <name>Camera::interpolateTo</name>
      <anchor>37dee575e0fdf56909e1779e20383e97</anchor>
      <arglist>(const Frame &amp;fr, float duration)</arglist>
    </member>
    <member kind="function">
      <name>Camera::type</name>
      <anchor>fbd0fa31db28593e9669c3c56711c0a7</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>Camera::fieldOfView</name>
      <anchor>89f1a2e62f7edf51de2d1c077ea5d330</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>Camera::horizontalFieldOfView</name>
      <anchor>957cf1049788f7aba3dd16f20f565960</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>Camera::aspectRatio</name>
      <anchor>915589f4d93e15d110444ed9b3464fa1</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>Camera::screenWidth</name>
      <anchor>80386c5943505b915246563262825d8e</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>Camera::screenHeight</name>
      <anchor>9fb925ffbf82e93898ae6db366a8d794</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>Camera::getViewport</name>
      <anchor>5034f21055f864c1ade7d86fec209348</anchor>
      <arglist>(GLint viewport[4]) const </arglist>
    </member>
    <member kind="function">
      <name>Camera::pixelGLRatio</name>
      <anchor>27f2dfd157643704c48630515a7f811f</anchor>
      <arglist>(const Vec &amp;position) const </arglist>
    </member>
    <member kind="function">
      <name>Camera::zNearCoefficient</name>
      <anchor>4c29c26071ddbe8512d478511e04a93e</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>Camera::zClippingCoefficient</name>
      <anchor>cd07c1b9464b935ad21bb38b7c27afca</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>Camera::zNear</name>
      <anchor>419a57556a6681c3a0489c847d687ea5</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>Camera::zFar</name>
      <anchor>a7461df81c1ea0384d4c64723eb7b949</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>Camera::getOrthoWidthHeight</name>
      <anchor>ffe84ed52b08d5d1d3dfdb87fe9242fa</anchor>
      <arglist>(GLdouble &amp;halfWidth, GLdouble &amp;halfHeight) const </arglist>
    </member>
    <member kind="function">
      <name>Camera::getFrustumPlanesCoefficients</name>
      <anchor>1450258d117908a71908a72184136e41</anchor>
      <arglist>(GLdouble coef[6][4]) const </arglist>
    </member>
    <member kind="function">
      <name>Camera::setType</name>
      <anchor>608e58a2f9fb7e497f91662a6e9ae4cc</anchor>
      <arglist>(Type type)</arglist>
    </member>
    <member kind="function">
      <name>Camera::setFieldOfView</name>
      <anchor>524f0183a127bb004defe3c2f7614e4c</anchor>
      <arglist>(float fov)</arglist>
    </member>
    <member kind="function">
      <name>Camera::setHorizontalFieldOfView</name>
      <anchor>017ace2ed5f67b6aeaef5c9a47b5f014</anchor>
      <arglist>(float hfov)</arglist>
    </member>
    <member kind="function">
      <name>Camera::setFOVToFitScene</name>
      <anchor>3394c425f95c5649b1e2e532dbd97fa7</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <name>Camera::setAspectRatio</name>
      <anchor>5b191e9b12b704a05302b5d922792e9c</anchor>
      <arglist>(float aspect)</arglist>
    </member>
    <member kind="function">
      <name>Camera::setScreenWidthAndHeight</name>
      <anchor>504c573cdd26be8b717a6da9fdb41812</anchor>
      <arglist>(int width, int height)</arglist>
    </member>
    <member kind="function">
      <name>Camera::setZNearCoefficient</name>
      <anchor>1d478610f928ecc4597c56d677d908b5</anchor>
      <arglist>(float coef)</arglist>
    </member>
    <member kind="function">
      <name>Camera::setZClippingCoefficient</name>
      <anchor>62ab18396bd8c212ec7b90dc156b59d7</anchor>
      <arglist>(float coef)</arglist>
    </member>
    <member kind="function">
      <name>Camera::sceneRadius</name>
      <anchor>58c17044cc4a601c6b446bf5e83513f9</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>Camera::sceneCenter</name>
      <anchor>87bccbfd40649310f3da369af02d50b1</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>Camera::distanceToSceneCenter</name>
      <anchor>253932bd8634348f9c189ab4c9b280b5</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>Camera::setSceneRadius</name>
      <anchor>33c70507fea15c3ea49a561b743ed822</anchor>
      <arglist>(float radius)</arglist>
    </member>
    <member kind="function">
      <name>Camera::setSceneCenter</name>
      <anchor>9185df6d9e8fbc108f83c5355f5e7b33</anchor>
      <arglist>(const Vec &amp;center)</arglist>
    </member>
    <member kind="function">
      <name>Camera::setSceneCenterFromPixel</name>
      <anchor>338c3360223f5fb20e2248208d2f32bf</anchor>
      <arglist>(const QPoint &amp;pixel)</arglist>
    </member>
    <member kind="function">
      <name>Camera::setSceneBoundingBox</name>
      <anchor>f3a1c2682cba52d6339d2f565ea1d0ee</anchor>
      <arglist>(const Vec &amp;min, const Vec &amp;max)</arglist>
    </member>
    <member kind="function">
      <name>Camera::setRevolveAroundPoint</name>
      <anchor>546bc081d6116ff848e6fc25a2329e08</anchor>
      <arglist>(const Vec &amp;rap)</arglist>
    </member>
    <member kind="function">
      <name>Camera::setRevolveAroundPointFromPixel</name>
      <anchor>1710d7aa9fc8dd96f88bba9a1ae3eed7</anchor>
      <arglist>(const QPoint &amp;pixel)</arglist>
    </member>
    <member kind="function">
      <name>Camera::revolveAroundPoint</name>
      <anchor>fd168442e2a05bd41881936fb722b5fe</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>Camera::frame</name>
      <anchor>d367db656b03fe0bc87b021801d66b75</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>Camera::setFrame</name>
      <anchor>809bad30c861f4f6f74228fb59340f90</anchor>
      <arglist>(ManipulatedCameraFrame *const mcf)</arglist>
    </member>
    <member kind="function">
      <name>Camera::keyFrameInterpolator</name>
      <anchor>bfbef567a6bbb9163b31c6f46238e4de</anchor>
      <arglist>(int i) const </arglist>
    </member>
    <member kind="function">
      <name>Camera::setKeyFrameInterpolator</name>
      <anchor>049b63c8c9ef48e24446ce44a3bd32b5</anchor>
      <arglist>(int i, KeyFrameInterpolator *const kfi)</arglist>
    </member>
    <member kind="function">
      <name>Camera::addKeyFrameToPath</name>
      <anchor>804ee001a41c3ddc33948447fc555cec</anchor>
      <arglist>(int i)</arglist>
    </member>
    <member kind="function">
      <name>Camera::playPath</name>
      <anchor>4eb47bb1cf02f806f1f355f63b445818</anchor>
      <arglist>(int i)</arglist>
    </member>
    <member kind="function">
      <name>Camera::deletePath</name>
      <anchor>8a75fa050d365ba249e8dcd439670aac</anchor>
      <arglist>(int i)</arglist>
    </member>
    <member kind="function">
      <name>Camera::resetPath</name>
      <anchor>e0329e473f00bb56f1e9949b8423ecbf</anchor>
      <arglist>(int i)</arglist>
    </member>
    <member kind="function">
      <name>Camera::drawAllPaths</name>
      <anchor>eea4caff561e6b1d8fe4b3d8efe4ae87</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <name>Camera::loadProjectionMatrix</name>
      <anchor>98a0679a22f005bbd8cc19756507cc9a</anchor>
      <arglist>(bool reset=true) const </arglist>
    </member>
    <member kind="function">
      <name>Camera::loadModelViewMatrix</name>
      <anchor>81053f822008b76bff7b1a41dceedf53</anchor>
      <arglist>(bool reset=true) const </arglist>
    </member>
    <member kind="function">
      <name>Camera::computeProjectionMatrix</name>
      <anchor>e51b6c486fe6448453369174bbea8055</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>Camera::computeModelViewMatrix</name>
      <anchor>0dad4baab6008e5a94e8a2fa83ec0f05</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>Camera::loadProjectionMatrixStereo</name>
      <anchor>79dac3c1bcb983c9025710b333f063a3</anchor>
      <arglist>(bool leftBuffer=true) const </arglist>
    </member>
    <member kind="function">
      <name>Camera::loadModelViewMatrixStereo</name>
      <anchor>47c0f19a566d045a2872b44014be8392</anchor>
      <arglist>(bool leftBuffer=true) const </arglist>
    </member>
    <member kind="function">
      <name>Camera::getProjectionMatrix</name>
      <anchor>97ff5cf330a6c24d6d49308aedd638bc</anchor>
      <arglist>(GLdouble m[16]) const </arglist>
    </member>
    <member kind="function">
      <name>Camera::getModelViewMatrix</name>
      <anchor>a1d631d6e368a7aa14656b7511787786</anchor>
      <arglist>(GLdouble m[16]) const </arglist>
    </member>
    <member kind="function">
      <name>Camera::getModelViewProjectionMatrix</name>
      <anchor>deb5e39686b9fcb3db090dff84c13be4</anchor>
      <arglist>(GLdouble m[16]) const </arglist>
    </member>
    <member kind="function">
      <name>Camera::draw</name>
      <anchor>1636e20e6910ded1c9a5860ba91f397e</anchor>
      <arglist>(bool drawFarPlane=true, float scale=1.0) const </arglist>
    </member>
    <member kind="function">
      <name>Camera::cameraCoordinatesOf</name>
      <anchor>5dd8eca926558c252d7552d85079880a</anchor>
      <arglist>(const Vec &amp;src) const </arglist>
    </member>
    <member kind="function">
      <name>Camera::worldCoordinatesOf</name>
      <anchor>123ad9bda6d715b5370650c2514896ab</anchor>
      <arglist>(const Vec &amp;src) const </arglist>
    </member>
    <member kind="function">
      <name>Camera::getCameraCoordinatesOf</name>
      <anchor>badd060aa34d51a940575b58d256dca4</anchor>
      <arglist>(const float src[3], float res[3]) const </arglist>
    </member>
    <member kind="function">
      <name>Camera::getWorldCoordinatesOf</name>
      <anchor>e1b0a45b7cd3a071c4ef88ed608511fd</anchor>
      <arglist>(const float src[3], float res[3]) const </arglist>
    </member>
    <member kind="function">
      <name>Camera::projectedCoordinatesOf</name>
      <anchor>c4dc649d17bd2ae8664a7f4fdd50360f</anchor>
      <arglist>(const Vec &amp;src, const Frame *frame=NULL) const </arglist>
    </member>
    <member kind="function">
      <name>Camera::unprojectedCoordinatesOf</name>
      <anchor>42577d3077e22b4726d78b3db3bba50d</anchor>
      <arglist>(const Vec &amp;src, const Frame *frame=NULL) const </arglist>
    </member>
    <member kind="function">
      <name>Camera::getProjectedCoordinatesOf</name>
      <anchor>e167e3b955401e11bbbf0fc25f52e3ee</anchor>
      <arglist>(const float src[3], float res[3], const Frame *frame=NULL) const </arglist>
    </member>
    <member kind="function">
      <name>Camera::getUnprojectedCoordinatesOf</name>
      <anchor>222deedc6fa32ab78d0a338eaa312f1f</anchor>
      <arglist>(const float src[3], float res[3], const Frame *frame=NULL) const </arglist>
    </member>
    <member kind="function">
      <name>Camera::convertClickToLine</name>
      <anchor>706d401cffa41e0e30864e59007f005f</anchor>
      <arglist>(const QPoint &amp;pixel, Vec &amp;orig, Vec &amp;dir) const </arglist>
    </member>
    <member kind="function">
      <name>Camera::pointUnderPixel</name>
      <anchor>90459767f782c80086ad7cff90a40b5a</anchor>
      <arglist>(const QPoint &amp;pixel, bool &amp;found) const </arglist>
    </member>
    <member kind="function">
      <name>Camera::flySpeed</name>
      <anchor>c1758b72dab0895b9340fa833e62b802</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>Camera::setFlySpeed</name>
      <anchor>02765efeaae1ef24f9d899f0d1d9ca09</anchor>
      <arglist>(float speed)</arglist>
    </member>
    <member kind="function">
      <name>Camera::IODistance</name>
      <anchor>d36e74de9c4020f951ea1a04e53a192d</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>Camera::physicalDistanceToScreen</name>
      <anchor>2cbb7d182c56732a6129936c5dd19d64</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>Camera::physicalScreenWidth</name>
      <anchor>8418799b4eeb22cbc1a157d488dcf09c</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>Camera::focusDistance</name>
      <anchor>f201ce62f669c8944a276a2615951379</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>Camera::setIODistance</name>
      <anchor>6cd81fe74a9dfe5c7124d00341ad0234</anchor>
      <arglist>(float distance)</arglist>
    </member>
    <member kind="function">
      <name>Camera::setPhysicalDistanceToScreen</name>
      <anchor>a88668546aa531dc0702d1b005904bcf</anchor>
      <arglist>(float distance)</arglist>
    </member>
    <member kind="function">
      <name>Camera::setPhysicalScreenWidth</name>
      <anchor>34911cda1cc1bc13336024c844ff4401</anchor>
      <arglist>(float width)</arglist>
    </member>
    <member kind="function">
      <name>Camera::setFocusDistance</name>
      <anchor>863ffb0284b534d5c57ac64a98e5e49a</anchor>
      <arglist>(float distance)</arglist>
    </member>
    <member kind="function">
      <name>Camera::domElement</name>
      <anchor>48e0e2dd26cd96418c8b889ceabe80f6</anchor>
      <arglist>(const QString &amp;name, QDomDocument &amp;document) const </arglist>
    </member>
    <member kind="function">
      <name>Camera::initFromDOMElement</name>
      <anchor>cd13d2ddeca530cb9f26ead47f7d25d3</anchor>
      <arglist>(const QDomElement &amp;element)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Constraint</name>
    <filename>classqglviewer_1_1Constraint.html</filename>
  </compound>
  <compound kind="class">
    <name>qglviewer::Constraint</name>
    <filename>classqglviewer_1_1Constraint.html</filename>
    <member kind="function">
      <name>Constraint::~Constraint</name>
      <anchor>65f2b59f5bc1435bf439482d885b0c0a</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <name>Constraint::constrainTranslation</name>
      <anchor>b7d724965c6765209f47c1abe7f7b7b4</anchor>
      <arglist>(Vec &amp;translation, Frame *const frame)</arglist>
    </member>
    <member kind="function">
      <name>Constraint::constrainRotation</name>
      <anchor>5de5f38e75b58476b7926171dba4b31b</anchor>
      <arglist>(Quaternion &amp;rotation, Frame *const frame)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Frame</name>
    <filename>classqglviewer_1_1Frame.html</filename>
  </compound>
  <compound kind="class">
    <name>qglviewer::Frame</name>
    <filename>classqglviewer_1_1Frame.html</filename>
    <member kind="function">
      <name>Frame::Frame</name>
      <anchor>b71e6ee46f0c2593266f9a62d9c5e029</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <name>Frame::~Frame</name>
      <anchor>e0c994a30d9a018000fe8ad66ff0a86d</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <name>Frame::Frame</name>
      <anchor>7864fb955cec11fe78c3b2bb81230516</anchor>
      <arglist>(const Frame &amp;frame)</arglist>
    </member>
    <member kind="function">
      <name>Frame::Frame</name>
      <anchor>2f649a1218291aa3776ce08d0a2879b1</anchor>
      <arglist>(const Vec &amp;position, const Quaternion &amp;orientation)</arglist>
    </member>
    <member kind="function">
      <name>Frame::operator=</name>
      <anchor>eb0f5ceffbb62f990b89b200d6ce728c</anchor>
      <arglist>(const Frame &amp;frame)</arglist>
    </member>
    <member kind="function">
      <name>Frame::modified</name>
      <anchor>b6722375e68e03d151762ebfc83c5459</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <name>Frame::interpolated</name>
      <anchor>b55c2a4f1732b90057fae4b6037399de</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <name>Frame::setPosition</name>
      <anchor>24f5da9b3f4bf4e71dfad7a74a932e52</anchor>
      <arglist>(const Vec &amp;position)</arglist>
    </member>
    <member kind="function">
      <name>Frame::setPosition</name>
      <anchor>ff7308b903966afc6c87b5cd766a9083</anchor>
      <arglist>(float x, float y, float z)</arglist>
    </member>
    <member kind="function">
      <name>Frame::setPositionWithConstraint</name>
      <anchor>5081403cb47f1324edae5278d5438a38</anchor>
      <arglist>(Vec &amp;position)</arglist>
    </member>
    <member kind="function">
      <name>Frame::setOrientation</name>
      <anchor>6733704520780505759efdef38a68b8b</anchor>
      <arglist>(const Quaternion &amp;orientation)</arglist>
    </member>
    <member kind="function">
      <name>Frame::setOrientation</name>
      <anchor>e3ac2bf6dcfe21252d40325789503ce6</anchor>
      <arglist>(double q0, double q1, double q2, double q3)</arglist>
    </member>
    <member kind="function">
      <name>Frame::setOrientationWithConstraint</name>
      <anchor>8b69106a268d78daf27d32854d455da4</anchor>
      <arglist>(Quaternion &amp;orientation)</arglist>
    </member>
    <member kind="function">
      <name>Frame::setPositionAndOrientation</name>
      <anchor>47912d16be51eb85d68d3dcb4a4fb087</anchor>
      <arglist>(const Vec &amp;position, const Quaternion &amp;orientation)</arglist>
    </member>
    <member kind="function">
      <name>Frame::setPositionAndOrientationWithConstraint</name>
      <anchor>8501c5582c1aa2a82a929ce36439ab3a</anchor>
      <arglist>(Vec &amp;position, Quaternion &amp;orientation)</arglist>
    </member>
    <member kind="function">
      <name>Frame::position</name>
      <anchor>bc38a05a597e07c9ff525122682adc50</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>Frame::orientation</name>
      <anchor>691af39b54669fad8b4e73599de22094</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>Frame::getPosition</name>
      <anchor>b3ca600424074ccaf5ee078c8d3149c3</anchor>
      <arglist>(float &amp;x, float &amp;y, float &amp;z) const </arglist>
    </member>
    <member kind="function">
      <name>Frame::getOrientation</name>
      <anchor>c47c3b7fd6023734ba40249bea1fd253</anchor>
      <arglist>(double &amp;q0, double &amp;q1, double &amp;q2, double &amp;q3) const </arglist>
    </member>
    <member kind="function">
      <name>Frame::setTranslation</name>
      <anchor>e5495cd791858225ba7c85ce39329704</anchor>
      <arglist>(const Vec &amp;translation)</arglist>
    </member>
    <member kind="function">
      <name>Frame::setTranslation</name>
      <anchor>eafb612e3ad0d6a81de91884eb4bdef1</anchor>
      <arglist>(float x, float y, float z)</arglist>
    </member>
    <member kind="function">
      <name>Frame::setTranslationWithConstraint</name>
      <anchor>7f3395d24d164a7af9a8b99e4e6bafc0</anchor>
      <arglist>(Vec &amp;translation)</arglist>
    </member>
    <member kind="function">
      <name>Frame::setRotation</name>
      <anchor>857aacdb88a574ec4a363335f6152a1e</anchor>
      <arglist>(const Quaternion &amp;rotation)</arglist>
    </member>
    <member kind="function">
      <name>Frame::setRotation</name>
      <anchor>26c462ecae655fa82c4d2efa42f9cd2e</anchor>
      <arglist>(double q0, double q1, double q2, double q3)</arglist>
    </member>
    <member kind="function">
      <name>Frame::setRotationWithConstraint</name>
      <anchor>9d4ab1388820d002d44304c5463fbfbd</anchor>
      <arglist>(Quaternion &amp;rotation)</arglist>
    </member>
    <member kind="function">
      <name>Frame::setTranslationAndRotation</name>
      <anchor>0fbfd129a52b36b6753cc1589ff87a48</anchor>
      <arglist>(const Vec &amp;translation, const Quaternion &amp;rotation)</arglist>
    </member>
    <member kind="function">
      <name>Frame::setTranslationAndRotationWithConstraint</name>
      <anchor>3a08480e8de4a36536ff95e0f05303db</anchor>
      <arglist>(Vec &amp;translation, Quaternion &amp;rotation)</arglist>
    </member>
    <member kind="function">
      <name>Frame::translation</name>
      <anchor>388a5f38140104323aa51fc207fde642</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>Frame::rotation</name>
      <anchor>19ee9243c60f412dfc4eac3dcf45a5a1</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>Frame::getTranslation</name>
      <anchor>39804059e5c4e694a6cb13b71347da97</anchor>
      <arglist>(float &amp;x, float &amp;y, float &amp;z) const </arglist>
    </member>
    <member kind="function">
      <name>Frame::getRotation</name>
      <anchor>475a20f772ff87508b673a586fc50bbb</anchor>
      <arglist>(double &amp;q0, double &amp;q1, double &amp;q2, double &amp;q3) const </arglist>
    </member>
    <member kind="function">
      <name>Frame::referenceFrame</name>
      <anchor>9794b079db5f492e8804631d3d23baec</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>Frame::setReferenceFrame</name>
      <anchor>bca9e0c2e40957fffeee4a52139facd8</anchor>
      <arglist>(const Frame *const refFrame)</arglist>
    </member>
    <member kind="function">
      <name>Frame::settingAsReferenceFrameWillCreateALoop</name>
      <anchor>a4891a91c825effc65b73f1329ae49c7</anchor>
      <arglist>(const Frame *const frame)</arglist>
    </member>
    <member kind="function">
      <name>Frame::translate</name>
      <anchor>4cb0cf5ff79e1acf0755befdaceb6f4a</anchor>
      <arglist>(Vec &amp;t)</arglist>
    </member>
    <member kind="function">
      <name>Frame::translate</name>
      <anchor>c774331eeae8d29acc94f5653b2f2c3b</anchor>
      <arglist>(const Vec &amp;t)</arglist>
    </member>
    <member kind="function">
      <name>Frame::translate</name>
      <anchor>9081ed23141a5980f028b21d58fa3290</anchor>
      <arglist>(float x, float y, float z)</arglist>
    </member>
    <member kind="function">
      <name>Frame::translate</name>
      <anchor>2ae644799a164d7519769ba393565c5d</anchor>
      <arglist>(float &amp;x, float &amp;y, float &amp;z)</arglist>
    </member>
    <member kind="function">
      <name>Frame::rotate</name>
      <anchor>38dffd6ee0ad7e395b49082b2b94de33</anchor>
      <arglist>(Quaternion &amp;q)</arglist>
    </member>
    <member kind="function">
      <name>Frame::rotate</name>
      <anchor>c754417fa609300f980fcba5405c9989</anchor>
      <arglist>(const Quaternion &amp;q)</arglist>
    </member>
    <member kind="function">
      <name>Frame::rotate</name>
      <anchor>7fe03bbf807b109c1edb849193d03aa8</anchor>
      <arglist>(double q0, double q1, double q2, double q3)</arglist>
    </member>
    <member kind="function">
      <name>Frame::rotate</name>
      <anchor>b15e32161293dce94ae3df3bc3a7b0ad</anchor>
      <arglist>(double &amp;q0, double &amp;q1, double &amp;q2, double &amp;q3)</arglist>
    </member>
    <member kind="function">
      <name>Frame::rotateAroundPoint</name>
      <anchor>85f48572c1298bc3192f111e8b587bca</anchor>
      <arglist>(Quaternion &amp;rotation, const Vec &amp;point)</arglist>
    </member>
    <member kind="function">
      <name>Frame::rotateAroundPoint</name>
      <anchor>5f87bae05096c99fb3b30cf5bb52c891</anchor>
      <arglist>(const Quaternion &amp;rotation, const Vec &amp;point)</arglist>
    </member>
    <member kind="function">
      <name>Frame::alignWithFrame</name>
      <anchor>352fc1d1f806653677eabcdb8a5eb898</anchor>
      <arglist>(const Frame *const frame, bool move=false, float threshold=0.85f)</arglist>
    </member>
    <member kind="function">
      <name>Frame::projectOnLine</name>
      <anchor>f97ae9e790033879dfcb9837a7312255</anchor>
      <arglist>(const Vec &amp;origin, const Vec &amp;direction)</arglist>
    </member>
    <member kind="function">
      <name>Frame::coordinatesOf</name>
      <anchor>c706b338b08d9d7b511fd84d0b6ecf96</anchor>
      <arglist>(const Vec &amp;src) const </arglist>
    </member>
    <member kind="function">
      <name>Frame::inverseCoordinatesOf</name>
      <anchor>5bcb22a3f2e11e42e7469602b75b3c1e</anchor>
      <arglist>(const Vec &amp;src) const </arglist>
    </member>
    <member kind="function">
      <name>Frame::localCoordinatesOf</name>
      <anchor>2b113688a0cab6c439dfbf598fd45e70</anchor>
      <arglist>(const Vec &amp;src) const </arglist>
    </member>
    <member kind="function">
      <name>Frame::localInverseCoordinatesOf</name>
      <anchor>4e6ac1c504a2f70fdbc0e7383c1aa7c7</anchor>
      <arglist>(const Vec &amp;src) const </arglist>
    </member>
    <member kind="function">
      <name>Frame::coordinatesOfIn</name>
      <anchor>8c3c1e66b248a0f29e304e6c786b9391</anchor>
      <arglist>(const Vec &amp;src, const Frame *const in) const </arglist>
    </member>
    <member kind="function">
      <name>Frame::coordinatesOfFrom</name>
      <anchor>adad46a46365f6977e4aac6dc18318ce</anchor>
      <arglist>(const Vec &amp;src, const Frame *const from) const </arglist>
    </member>
    <member kind="function">
      <name>Frame::getCoordinatesOf</name>
      <anchor>3718dc9cc825c674c8da3576b1448764</anchor>
      <arglist>(const float src[3], float res[3]) const </arglist>
    </member>
    <member kind="function">
      <name>Frame::getInverseCoordinatesOf</name>
      <anchor>ede4243bfe48e6d870a2417bbfc711a8</anchor>
      <arglist>(const float src[3], float res[3]) const </arglist>
    </member>
    <member kind="function">
      <name>Frame::getLocalCoordinatesOf</name>
      <anchor>be456b2ee906b9def0d2ec0c949a9497</anchor>
      <arglist>(const float src[3], float res[3]) const </arglist>
    </member>
    <member kind="function">
      <name>Frame::getLocalInverseCoordinatesOf</name>
      <anchor>dbd3252d2089f077e23e78345b3723c2</anchor>
      <arglist>(const float src[3], float res[3]) const </arglist>
    </member>
    <member kind="function">
      <name>Frame::getCoordinatesOfIn</name>
      <anchor>f2748e8dd6a4edad38d195132a8c8b30</anchor>
      <arglist>(const float src[3], float res[3], const Frame *const in) const </arglist>
    </member>
    <member kind="function">
      <name>Frame::getCoordinatesOfFrom</name>
      <anchor>36a2d7b27ab27bcff37ab9cbfab20a17</anchor>
      <arglist>(const float src[3], float res[3], const Frame *const from) const </arglist>
    </member>
    <member kind="function">
      <name>Frame::transformOf</name>
      <anchor>05a3e18419f02427366a95b1e299f12e</anchor>
      <arglist>(const Vec &amp;src) const </arglist>
    </member>
    <member kind="function">
      <name>Frame::inverseTransformOf</name>
      <anchor>ba2c6c46c1825a81f09d22322b03d9b5</anchor>
      <arglist>(const Vec &amp;src) const </arglist>
    </member>
    <member kind="function">
      <name>Frame::localTransformOf</name>
      <anchor>37239448835f46771b9598a31b964cf2</anchor>
      <arglist>(const Vec &amp;src) const </arglist>
    </member>
    <member kind="function">
      <name>Frame::localInverseTransformOf</name>
      <anchor>d36cf320ff7cf6c8f9a2ac527c924f9e</anchor>
      <arglist>(const Vec &amp;src) const </arglist>
    </member>
    <member kind="function">
      <name>Frame::transformOfIn</name>
      <anchor>01636f93c09d6fd9fca90cb671afdda0</anchor>
      <arglist>(const Vec &amp;src, const Frame *const in) const </arglist>
    </member>
    <member kind="function">
      <name>Frame::transformOfFrom</name>
      <anchor>3416ca5a53c1f2d932fca1906ec1c6c7</anchor>
      <arglist>(const Vec &amp;src, const Frame *const from) const </arglist>
    </member>
    <member kind="function">
      <name>Frame::getTransformOf</name>
      <anchor>ac70b883f8e3635d88356cb08b1abae3</anchor>
      <arglist>(const float src[3], float res[3]) const </arglist>
    </member>
    <member kind="function">
      <name>Frame::getInverseTransformOf</name>
      <anchor>6a2eb1467ca31f7db460a2d6b7166a07</anchor>
      <arglist>(const float src[3], float res[3]) const </arglist>
    </member>
    <member kind="function">
      <name>Frame::getLocalTransformOf</name>
      <anchor>6e4aeb1d5aafee31442cc3c82a6ee215</anchor>
      <arglist>(const float src[3], float res[3]) const </arglist>
    </member>
    <member kind="function">
      <name>Frame::getLocalInverseTransformOf</name>
      <anchor>62b201feca5e0ebecc03a71b9566d97c</anchor>
      <arglist>(const float src[3], float res[3]) const </arglist>
    </member>
    <member kind="function">
      <name>Frame::getTransformOfIn</name>
      <anchor>7d36c3c2d4b004b3e5d296aa58541f9f</anchor>
      <arglist>(const float src[3], float res[3], const Frame *const in) const </arglist>
    </member>
    <member kind="function">
      <name>Frame::getTransformOfFrom</name>
      <anchor>4eed4b2770a62e5ebb32d3dcee642c52</anchor>
      <arglist>(const float src[3], float res[3], const Frame *const from) const </arglist>
    </member>
    <member kind="function">
      <name>Frame::constraint</name>
      <anchor>0974dfe336ed84a20adb8c6cdf8dedd9</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>Frame::setConstraint</name>
      <anchor>dffa44cd173714b29f2441848a39c224</anchor>
      <arglist>(Constraint *const constraint)</arglist>
    </member>
    <member kind="function">
      <name>Frame::matrix</name>
      <anchor>a12123cf45f68b9f7fe0526b70ab0047</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>Frame::getMatrix</name>
      <anchor>4915640a54a6b48a99cbdd0cd42fec48</anchor>
      <arglist>(GLdouble m[4][4]) const </arglist>
    </member>
    <member kind="function">
      <name>Frame::getMatrix</name>
      <anchor>15f0dc7596dc78437154302466ac3c0a</anchor>
      <arglist>(GLdouble m[16]) const </arglist>
    </member>
    <member kind="function">
      <name>Frame::worldMatrix</name>
      <anchor>39aa0648db05006e2b2f22ac5d971141</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>Frame::getWorldMatrix</name>
      <anchor>45358fae434b3912ce81577dc7cf4fc9</anchor>
      <arglist>(GLdouble m[4][4]) const </arglist>
    </member>
    <member kind="function">
      <name>Frame::getWorldMatrix</name>
      <anchor>c808850371649942726dd474aba29ead</anchor>
      <arglist>(GLdouble m[16]) const </arglist>
    </member>
    <member kind="function">
      <name>Frame::setFromMatrix</name>
      <anchor>5408d08ab204157ec29f555b6813f9e4</anchor>
      <arglist>(const GLdouble m[4][4])</arglist>
    </member>
    <member kind="function">
      <name>Frame::setFromMatrix</name>
      <anchor>0c4d51f142f43235e30198e7b8abc626</anchor>
      <arglist>(const GLdouble m[16])</arglist>
    </member>
    <member kind="function">
      <name>Frame::inverse</name>
      <anchor>4534c3188c217feb8d21465d4cefd4ad</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>Frame::worldInverse</name>
      <anchor>37d4da8cfd297273e7bb55538debaa3e</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>Frame::domElement</name>
      <anchor>48e0e2dd26cd96418c8b889ceabe80f6</anchor>
      <arglist>(const QString &amp;name, QDomDocument &amp;document) const </arglist>
    </member>
    <member kind="function">
      <name>Frame::initFromDOMElement</name>
      <anchor>cd13d2ddeca530cb9f26ead47f7d25d3</anchor>
      <arglist>(const QDomElement &amp;element)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>KeyFrameInterpolator</name>
    <filename>classqglviewer_1_1KeyFrameInterpolator.html</filename>
  </compound>
  <compound kind="class">
    <name>qglviewer::KeyFrameInterpolator</name>
    <filename>classqglviewer_1_1KeyFrameInterpolator.html</filename>
    <member kind="function">
      <name>KeyFrameInterpolator::KeyFrameInterpolator</name>
      <anchor>2a78bc183af3ac92802cbe605e2a878e</anchor>
      <arglist>(Frame *fr=NULL)</arglist>
    </member>
    <member kind="function">
      <name>KeyFrameInterpolator::~KeyFrameInterpolator</name>
      <anchor>110b875f9265a30f7a520a3603362f95</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <name>KeyFrameInterpolator::interpolated</name>
      <anchor>b55c2a4f1732b90057fae4b6037399de</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <name>KeyFrameInterpolator::endReached</name>
      <anchor>b4010a17bf77b9940b120ee8ed9a0271</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <name>KeyFrameInterpolator::addKeyFrame</name>
      <anchor>44ac54529e675a2157067c9d205d9622</anchor>
      <arglist>(const Frame &amp;frame)</arglist>
    </member>
    <member kind="function">
      <name>KeyFrameInterpolator::addKeyFrame</name>
      <anchor>379af0370e27c513c4d9091bff272b40</anchor>
      <arglist>(const Frame &amp;frame, float time)</arglist>
    </member>
    <member kind="function">
      <name>KeyFrameInterpolator::addKeyFrame</name>
      <anchor>23d3166003e0355b718f34a3e6c92a1b</anchor>
      <arglist>(const Frame *const frame)</arglist>
    </member>
    <member kind="function">
      <name>KeyFrameInterpolator::addKeyFrame</name>
      <anchor>a821392c6e108d5c7814317b8c3cd47f</anchor>
      <arglist>(const Frame *const frame, float time)</arglist>
    </member>
    <member kind="function">
      <name>KeyFrameInterpolator::deletePath</name>
      <anchor>e343912505ce83d62bea580a83c7bc34</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <name>KeyFrameInterpolator::frame</name>
      <anchor>5426b68b2b1bb6ad8dc0007914412b4f</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>KeyFrameInterpolator::setFrame</name>
      <anchor>1e16bb31ee6240a9f0e3a6824f4bef0d</anchor>
      <arglist>(Frame *const frame)</arglist>
    </member>
    <member kind="function">
      <name>KeyFrameInterpolator::keyFrame</name>
      <anchor>2437eecf340817ad1a3f86c822b111e8</anchor>
      <arglist>(int index) const </arglist>
    </member>
    <member kind="function">
      <name>KeyFrameInterpolator::keyFrameTime</name>
      <anchor>976ec792d48ccd7e53b55bb91b49d473</anchor>
      <arglist>(int index) const </arglist>
    </member>
    <member kind="function">
      <name>KeyFrameInterpolator::numberOfKeyFrames</name>
      <anchor>ba3744250d9cd01ec848f81151a62273</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>KeyFrameInterpolator::duration</name>
      <anchor>80c858ec25677a47d066e0900f4e1980</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>KeyFrameInterpolator::firstTime</name>
      <anchor>5335f8bedcb11c4e9cc06cbbab838477</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>KeyFrameInterpolator::lastTime</name>
      <anchor>3e953e6c813baa461389c132c9509e30</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>KeyFrameInterpolator::interpolationTime</name>
      <anchor>58326b7948e78c1d2861ca659492207a</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>KeyFrameInterpolator::interpolationSpeed</name>
      <anchor>7e468c84c27d896e341563c83e102aad</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>KeyFrameInterpolator::interpolationPeriod</name>
      <anchor>21bf6165ea3a6be2fd854e9be5105b1e</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>KeyFrameInterpolator::loopInterpolation</name>
      <anchor>906c17cf6c1d51a54c7d3b9b4c9cbd45</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>KeyFrameInterpolator::setInterpolationTime</name>
      <anchor>b860af88ea46ecedb2a648157bb68dc3</anchor>
      <arglist>(float time)</arglist>
    </member>
    <member kind="function">
      <name>KeyFrameInterpolator::setInterpolationSpeed</name>
      <anchor>3cc64e95e1b8dfda110bcf3d033ecf2d</anchor>
      <arglist>(float speed)</arglist>
    </member>
    <member kind="function">
      <name>KeyFrameInterpolator::setInterpolationPeriod</name>
      <anchor>9763e647346a8bee885517d9985173fd</anchor>
      <arglist>(int period)</arglist>
    </member>
    <member kind="function">
      <name>KeyFrameInterpolator::setLoopInterpolation</name>
      <anchor>31a491ac2ac016298cbd1c66f07be6dd</anchor>
      <arglist>(bool loop=true)</arglist>
    </member>
    <member kind="function">
      <name>KeyFrameInterpolator::interpolationIsStarted</name>
      <anchor>8cc0fa56ba7b6da71226cc191cc18e70</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>KeyFrameInterpolator::startInterpolation</name>
      <anchor>3c0098b2307d04e904c1f64f505e5819</anchor>
      <arglist>(int period=-1)</arglist>
    </member>
    <member kind="function">
      <name>KeyFrameInterpolator::stopInterpolation</name>
      <anchor>16f0910299ae07cede3396a14ec01f4b</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <name>KeyFrameInterpolator::resetInterpolation</name>
      <anchor>0bfc602dc2fcaca40cc49195bba7ba74</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <name>KeyFrameInterpolator::toggleInterpolation</name>
      <anchor>cf744bd8ce09e5433f829f63ba310eef</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <name>KeyFrameInterpolator::interpolateAtTime</name>
      <anchor>d5393783e1768b6d688a8c49ddea56ae</anchor>
      <arglist>(float time)</arglist>
    </member>
    <member kind="function">
      <name>KeyFrameInterpolator::drawPath</name>
      <anchor>ca0ce46b39ad4093450019d77fd247f2</anchor>
      <arglist>(int mask=1, int nbFrames=6, float scale=1.0f)</arglist>
    </member>
    <member kind="function">
      <name>KeyFrameInterpolator::domElement</name>
      <anchor>48e0e2dd26cd96418c8b889ceabe80f6</anchor>
      <arglist>(const QString &amp;name, QDomDocument &amp;document) const </arglist>
    </member>
    <member kind="function">
      <name>KeyFrameInterpolator::initFromDOMElement</name>
      <anchor>cd13d2ddeca530cb9f26ead47f7d25d3</anchor>
      <arglist>(const QDomElement &amp;element)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>LocalConstraint</name>
    <filename>classqglviewer_1_1LocalConstraint.html</filename>
  </compound>
  <compound kind="class">
    <name>qglviewer::LocalConstraint</name>
    <base>qglviewer::AxisPlaneConstraint</base>
    <base>AxisPlaneConstraint</base>
    <filename>classqglviewer_1_1LocalConstraint.html</filename>
    <member kind="function">
      <name>LocalConstraint::~LocalConstraint</name>
      <anchor>774eacfea1ced7b56a66f125e737557c</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <name>LocalConstraint::constrainTranslation</name>
      <anchor>dc695bfbc605b5631be663b28a4ea9f6</anchor>
      <arglist>(Vec &amp;translation, Frame *const frame)</arglist>
    </member>
    <member kind="function">
      <name>LocalConstraint::constrainRotation</name>
      <anchor>71c099a2c356f715f8bf34052875cd25</anchor>
      <arglist>(Quaternion &amp;rotation, Frame *const frame)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>ManipulatedCameraFrame</name>
    <filename>classqglviewer_1_1ManipulatedCameraFrame.html</filename>
  </compound>
  <compound kind="class">
    <name>qglviewer::ManipulatedCameraFrame</name>
    <base>qglviewer::ManipulatedFrame</base>
    <base>ManipulatedFrame</base>
    <filename>classqglviewer_1_1ManipulatedCameraFrame.html</filename>
    <member kind="function">
      <name>ManipulatedCameraFrame::ManipulatedCameraFrame</name>
      <anchor>8eb72b24950cc4c47c35c4fefc3dfa14</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <name>ManipulatedCameraFrame::~ManipulatedCameraFrame</name>
      <anchor>5f187b4d7822ab9602aee6dc27e723a6</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <name>ManipulatedCameraFrame::ManipulatedCameraFrame</name>
      <anchor>1a319246151bdd8f5ca4d9e1824abcc2</anchor>
      <arglist>(const ManipulatedCameraFrame &amp;mcf)</arglist>
    </member>
    <member kind="function">
      <name>ManipulatedCameraFrame::operator=</name>
      <anchor>51ccf29ba558394424cf08ce21bd4e73</anchor>
      <arglist>(const ManipulatedCameraFrame &amp;mcf)</arglist>
    </member>
    <member kind="function">
      <name>ManipulatedCameraFrame::revolveAroundPoint</name>
      <anchor>fd168442e2a05bd41881936fb722b5fe</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>ManipulatedCameraFrame::setRevolveAroundPoint</name>
      <anchor>2d752bdc1dc20e5892ab79b5f3cabcf6</anchor>
      <arglist>(const Vec &amp;revolveAroundPoint)</arglist>
    </member>
    <member kind="function">
      <name>ManipulatedCameraFrame::setFlySpeed</name>
      <anchor>02765efeaae1ef24f9d899f0d1d9ca09</anchor>
      <arglist>(float speed)</arglist>
    </member>
    <member kind="function">
      <name>ManipulatedCameraFrame::setFlyUpVector</name>
      <anchor>9c750a49810dc2bad64b60156fa0e7d6</anchor>
      <arglist>(const Vec &amp;up)</arglist>
    </member>
    <member kind="function">
      <name>ManipulatedCameraFrame::flySpeed</name>
      <anchor>c1758b72dab0895b9340fa833e62b802</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>ManipulatedCameraFrame::flyUpVector</name>
      <anchor>e853dc0ded87ad0c2d49030f2439dffe</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>ManipulatedCameraFrame::mouseReleaseEvent</name>
      <anchor>ec95b0f05a05c1cdfc940ef4621d5db3</anchor>
      <arglist>(QMouseEvent *const event, Camera *const camera)</arglist>
    </member>
    <member kind="function">
      <name>ManipulatedCameraFrame::mouseMoveEvent</name>
      <anchor>609d287c3721aff697068efa3dfc4b4c</anchor>
      <arglist>(QMouseEvent *const event, Camera *const camera)</arglist>
    </member>
    <member kind="function">
      <name>ManipulatedCameraFrame::wheelEvent</name>
      <anchor>e5e5914dbdcba274fc9f58c558ba6a36</anchor>
      <arglist>(QWheelEvent *const event, Camera *const camera)</arglist>
    </member>
    <member kind="function">
      <name>ManipulatedCameraFrame::spin</name>
      <anchor>f87828117d0582d0031f563933f97bfd</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <name>ManipulatedCameraFrame::domElement</name>
      <anchor>48e0e2dd26cd96418c8b889ceabe80f6</anchor>
      <arglist>(const QString &amp;name, QDomDocument &amp;document) const </arglist>
    </member>
    <member kind="function">
      <name>ManipulatedCameraFrame::initFromDOMElement</name>
      <anchor>cd13d2ddeca530cb9f26ead47f7d25d3</anchor>
      <arglist>(const QDomElement &amp;element)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>ManipulatedFrame</name>
    <filename>classqglviewer_1_1ManipulatedFrame.html</filename>
  </compound>
  <compound kind="class">
    <name>qglviewer::ManipulatedFrame</name>
    <base>qglviewer::Frame</base>
    <base>qglviewer::MouseGrabber</base>
    <base>Frame</base>
    <base>MouseGrabber</base>
    <filename>classqglviewer_1_1ManipulatedFrame.html</filename>
    <member kind="function">
      <name>ManipulatedFrame::ManipulatedFrame</name>
      <anchor>1e9e849ecaeee6226bfb615d7a74a091</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <name>ManipulatedFrame::~ManipulatedFrame</name>
      <anchor>e2ffc5f93122c6628e80dc254977c4cf</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <name>ManipulatedFrame::ManipulatedFrame</name>
      <anchor>9b1534a226c056993bc9ed594de9cc7e</anchor>
      <arglist>(const ManipulatedFrame &amp;mf)</arglist>
    </member>
    <member kind="function">
      <name>ManipulatedFrame::operator=</name>
      <anchor>d6d13eba8611869fe824da2dab500f50</anchor>
      <arglist>(const ManipulatedFrame &amp;mf)</arglist>
    </member>
    <member kind="function">
      <name>ManipulatedFrame::manipulated</name>
      <anchor>841d6d550acbe6e9103c32ac3b75c0c2</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <name>ManipulatedFrame::spun</name>
      <anchor>8e3a52ff9f948f42269cab711bb92b5c</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <name>ManipulatedFrame::setRotationSensitivity</name>
      <anchor>239ba71eaf212e169fe31918ed2d9c11</anchor>
      <arglist>(float sensitivity)</arglist>
    </member>
    <member kind="function">
      <name>ManipulatedFrame::setTranslationSensitivity</name>
      <anchor>c0cdf6be7c1443e2e984012490569871</anchor>
      <arglist>(float sensitivity)</arglist>
    </member>
    <member kind="function">
      <name>ManipulatedFrame::setSpinningSensitivity</name>
      <anchor>a319d217b34f0a861e30d90b78a5c39b</anchor>
      <arglist>(float sensitivity)</arglist>
    </member>
    <member kind="function">
      <name>ManipulatedFrame::setWheelSensitivity</name>
      <anchor>fd6cb097279239492a3401893d317499</anchor>
      <arglist>(float sensitivity)</arglist>
    </member>
    <member kind="function">
      <name>ManipulatedFrame::rotationSensitivity</name>
      <anchor>4573c3414e63c10dec06a3064d37532f</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>ManipulatedFrame::translationSensitivity</name>
      <anchor>28fd6b0ca560be97bb899e1a7e3c821a</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>ManipulatedFrame::spinningSensitivity</name>
      <anchor>1025480797d5dd046d6d717de59ffbca</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>ManipulatedFrame::wheelSensitivity</name>
      <anchor>d9e7fc4134c9733e8cfecf8bf80dbd44</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>ManipulatedFrame::isSpinning</name>
      <anchor>975dfbf6b972b7128520b30a4ffe11dd</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>ManipulatedFrame::spinningQuaternion</name>
      <anchor>7b8b14da64c84fed1cbdb8d98191d276</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>ManipulatedFrame::setSpinningQuaternion</name>
      <anchor>aa23ed0375ee472156f439eaee070ec9</anchor>
      <arglist>(const Quaternion &amp;spinningQuaternion)</arglist>
    </member>
    <member kind="function">
      <name>ManipulatedFrame::startSpinning</name>
      <anchor>9942b6eab3fb197805f7af7e0edfdccf</anchor>
      <arglist>(int updateInterval)</arglist>
    </member>
    <member kind="function">
      <name>ManipulatedFrame::stopSpinning</name>
      <anchor>9dc6e4b2c4c4a3ae195dfc39bb2ce8ac</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <name>ManipulatedFrame::spin</name>
      <anchor>f87828117d0582d0031f563933f97bfd</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <name>ManipulatedFrame::mousePressEvent</name>
      <anchor>9b4cce9d77e0495202100a8f1055f1f3</anchor>
      <arglist>(QMouseEvent *const event, Camera *const camera)</arglist>
    </member>
    <member kind="function">
      <name>ManipulatedFrame::mouseMoveEvent</name>
      <anchor>609d287c3721aff697068efa3dfc4b4c</anchor>
      <arglist>(QMouseEvent *const event, Camera *const camera)</arglist>
    </member>
    <member kind="function">
      <name>ManipulatedFrame::mouseReleaseEvent</name>
      <anchor>ec95b0f05a05c1cdfc940ef4621d5db3</anchor>
      <arglist>(QMouseEvent *const event, Camera *const camera)</arglist>
    </member>
    <member kind="function">
      <name>ManipulatedFrame::mouseDoubleClickEvent</name>
      <anchor>866619719201540ace1f05a1f8b1f156</anchor>
      <arglist>(QMouseEvent *const event, Camera *const camera)</arglist>
    </member>
    <member kind="function">
      <name>ManipulatedFrame::wheelEvent</name>
      <anchor>e5e5914dbdcba274fc9f58c558ba6a36</anchor>
      <arglist>(QWheelEvent *const event, Camera *const camera)</arglist>
    </member>
    <member kind="function">
      <name>ManipulatedFrame::isManipulated</name>
      <anchor>032c48e2e7cdced01dea0cb7eca14022</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>ManipulatedFrame::checkIfGrabsMouse</name>
      <anchor>be537c0091ddf3c907ca0e32861d701d</anchor>
      <arglist>(int x, int y, const Camera *const camera)</arglist>
    </member>
    <member kind="function">
      <name>ManipulatedFrame::domElement</name>
      <anchor>48e0e2dd26cd96418c8b889ceabe80f6</anchor>
      <arglist>(const QString &amp;name, QDomDocument &amp;document) const </arglist>
    </member>
    <member kind="function">
      <name>ManipulatedFrame::initFromDOMElement</name>
      <anchor>cd13d2ddeca530cb9f26ead47f7d25d3</anchor>
      <arglist>(const QDomElement &amp;element)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>MouseGrabber</name>
    <filename>classqglviewer_1_1MouseGrabber.html</filename>
  </compound>
  <compound kind="class">
    <name>qglviewer::MouseGrabber</name>
    <filename>classqglviewer_1_1MouseGrabber.html</filename>
    <member kind="function">
      <name>MouseGrabber::MouseGrabber</name>
      <anchor>eed1d659951e2bc8dbe0ffa3850c97e6</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <name>MouseGrabber::~MouseGrabber</name>
      <anchor>acf14b5ce34121bce9fe5b75c54d666e</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <name>MouseGrabber::checkIfGrabsMouse</name>
      <anchor>6110636d4e031373ecebd42c6ea838ea</anchor>
      <arglist>(int x, int y, const Camera *const camera)=0</arglist>
    </member>
    <member kind="function">
      <name>MouseGrabber::grabsMouse</name>
      <anchor>4eb7e14d035ae255b77ac1711aef039e</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>MouseGrabber::setGrabsMouse</name>
      <anchor>9ecea0a9f8ce1b2d92bbd1dd6bdcd4f9</anchor>
      <arglist>(bool grabs)</arglist>
    </member>
    <member kind="function">
      <name>MouseGrabber::MouseGrabberPool</name>
      <anchor>e4eac355da73462080fe66243be5a286</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <name>MouseGrabber::isInMouseGrabberPool</name>
      <anchor>958abc8c9ae9ea339f9765fddfc7dbdd</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>MouseGrabber::addInMouseGrabberPool</name>
      <anchor>4ef00d9d2abb7b331a3c333649f6ff82</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <name>MouseGrabber::removeFromMouseGrabberPool</name>
      <anchor>4ac2261aafd5f48f2d90c989cdd69369</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <name>MouseGrabber::clearMouseGrabberPool</name>
      <anchor>23548e9ef41cf38913f6c642509a81ec</anchor>
      <arglist>(bool autoDelete=false)</arglist>
    </member>
    <member kind="function">
      <name>MouseGrabber::mousePressEvent</name>
      <anchor>56df172a6eff56ecc1a99d2e7d548d31</anchor>
      <arglist>(QMouseEvent *const event, Camera *const camera)</arglist>
    </member>
    <member kind="function">
      <name>MouseGrabber::mouseDoubleClickEvent</name>
      <anchor>7c092390bad31d7f73ea474667159859</anchor>
      <arglist>(QMouseEvent *const event, Camera *const camera)</arglist>
    </member>
    <member kind="function">
      <name>MouseGrabber::mouseReleaseEvent</name>
      <anchor>2e07bf5f7c6c96f40e7f1d1fdb031b0c</anchor>
      <arglist>(QMouseEvent *const event, Camera *const camera)</arglist>
    </member>
    <member kind="function">
      <name>MouseGrabber::mouseMoveEvent</name>
      <anchor>6e2baf9735a27e4c8a928518cc273d5c</anchor>
      <arglist>(QMouseEvent *const event, Camera *const camera)</arglist>
    </member>
    <member kind="function">
      <name>MouseGrabber::wheelEvent</name>
      <anchor>07a7d880d107f0b532ef779b29884e08</anchor>
      <arglist>(QWheelEvent *const event, Camera *const camera)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Quaternion</name>
    <filename>classqglviewer_1_1Quaternion.html</filename>
  </compound>
  <compound kind="class">
    <name>qglviewer::Quaternion</name>
    <filename>classqglviewer_1_1Quaternion.html</filename>
    <member kind="function">
      <name>Quaternion::Quaternion</name>
      <anchor>65ed15cc19af958b5933b5c522f10e66</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <name>Quaternion::Quaternion</name>
      <anchor>d0347716e801bcdeab909493817edc85</anchor>
      <arglist>(const Vec &amp;axis, double angle)</arglist>
    </member>
    <member kind="function">
      <name>Quaternion::Quaternion</name>
      <anchor>1b60be34a715145efc3b91e6dfba1634</anchor>
      <arglist>(const Vec &amp;from, const Vec &amp;to)</arglist>
    </member>
    <member kind="function">
      <name>Quaternion::Quaternion</name>
      <anchor>81ba24ffd95778f3ca4e51b2b9922f45</anchor>
      <arglist>(double q0, double q1, double q2, double q3)</arglist>
    </member>
    <member kind="function">
      <name>Quaternion::Quaternion</name>
      <anchor>71a4d1a3b760854468ff270a982e5f59</anchor>
      <arglist>(const Quaternion &amp;Q)</arglist>
    </member>
    <member kind="function">
      <name>Quaternion::Quaternion</name>
      <anchor>87faf5efc96c9b5af85a611985b6618f</anchor>
      <arglist>(const QDomElement &amp;element)</arglist>
    </member>
    <member kind="function">
      <name>Quaternion::operator=</name>
      <anchor>d20a1310f5fac88d5e00fa055e09fe72</anchor>
      <arglist>(const Quaternion &amp;Q)</arglist>
    </member>
    <member kind="function">
      <name>Quaternion::setAxisAngle</name>
      <anchor>d6bd25e80a18f6bd38ae5c4e5337b53b</anchor>
      <arglist>(const Vec &amp;axis, double angle)</arglist>
    </member>
    <member kind="function">
      <name>Quaternion::setValue</name>
      <anchor>be7dd94e4fb48e0c8e0d994cac84064a</anchor>
      <arglist>(double q0, double q1, double q2, double q3)</arglist>
    </member>
    <member kind="function">
      <name>Quaternion::setFromRotationMatrix</name>
      <anchor>1880f20e8080ee670b6a88ed0c7d69ac</anchor>
      <arglist>(const double m[3][3])</arglist>
    </member>
    <member kind="function">
      <name>Quaternion::setFromRotatedBasis</name>
      <anchor>760617f082c950633b5642d2f60b2dd9</anchor>
      <arglist>(const Vec &amp;X, const Vec &amp;Y, const Vec &amp;Z)</arglist>
    </member>
    <member kind="function">
      <name>Quaternion::axis</name>
      <anchor>63f3f585fd25e9cb32700a26d54f8ee4</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>Quaternion::angle</name>
      <anchor>39bde10d142ce0cbc9614988ca0a0e59</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>Quaternion::getAxisAngle</name>
      <anchor>46b7892fe36e140862b9c71e5cfe8292</anchor>
      <arglist>(Vec &amp;axis, float &amp;angle) const </arglist>
    </member>
    <member kind="function">
      <name>Quaternion::operator[]</name>
      <anchor>605b34d1fc87b2ccaf49a45419c2e46f</anchor>
      <arglist>(int i) const </arglist>
    </member>
    <member kind="function">
      <name>Quaternion::operator[]</name>
      <anchor>f1a8c9c4769161714a5a1a2c7fad446b</anchor>
      <arglist>(int i)</arglist>
    </member>
    <member kind="function">
      <name>Quaternion::operator*=</name>
      <anchor>0caef8e6e702101a45fdc0af3920c49d</anchor>
      <arglist>(const Quaternion &amp;q)</arglist>
    </member>
    <member kind="function">
      <name>Quaternion::rotate</name>
      <anchor>5e2421069d88e576334e10f634625815</anchor>
      <arglist>(const Vec &amp;v) const </arglist>
    </member>
    <member kind="function">
      <name>Quaternion::inverseRotate</name>
      <anchor>74d79bc1afcc02fdbfe0eb390c3546f5</anchor>
      <arglist>(const Vec &amp;v) const </arglist>
    </member>
    <member kind="function">
      <name>Quaternion::inverse</name>
      <anchor>c378ebc684d691dc25336b895f24a82e</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>Quaternion::invert</name>
      <anchor>7fa1616cc61c19a5efcc863c950f7f30</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <name>Quaternion::negate</name>
      <anchor>bcdb1512395327f8236a4f4a4d4ff648</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <name>Quaternion::normalize</name>
      <anchor>05826e509c686f39baaec4656f1a7231</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <name>Quaternion::normalized</name>
      <anchor>e347ecf6e0a78618e8fd1a3b9107df32</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>Quaternion::matrix</name>
      <anchor>a12123cf45f68b9f7fe0526b70ab0047</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>Quaternion::getMatrix</name>
      <anchor>4915640a54a6b48a99cbdd0cd42fec48</anchor>
      <arglist>(GLdouble m[4][4]) const </arglist>
    </member>
    <member kind="function">
      <name>Quaternion::getMatrix</name>
      <anchor>15f0dc7596dc78437154302466ac3c0a</anchor>
      <arglist>(GLdouble m[16]) const </arglist>
    </member>
    <member kind="function">
      <name>Quaternion::getRotationMatrix</name>
      <anchor>ad37aa66da74be6fb8d9d66f059f0872</anchor>
      <arglist>(float m[3][3]) const </arglist>
    </member>
    <member kind="function">
      <name>Quaternion::inverseMatrix</name>
      <anchor>32992f152b7377da431c69f15384ed22</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>Quaternion::getInverseMatrix</name>
      <anchor>ee97ea226e31ac805a2245e7c73dac7b</anchor>
      <arglist>(GLdouble m[4][4]) const </arglist>
    </member>
    <member kind="function">
      <name>Quaternion::getInverseMatrix</name>
      <anchor>315cf039df621a101a953df20acc7155</anchor>
      <arglist>(GLdouble m[16]) const </arglist>
    </member>
    <member kind="function">
      <name>Quaternion::getInverseRotationMatrix</name>
      <anchor>e1455acb1cbf59befe65295734d48c4a</anchor>
      <arglist>(float m[3][3]) const </arglist>
    </member>
    <member kind="function">
      <name>Quaternion::slerp</name>
      <anchor>755201c9a752fd892a3656a517b190e4</anchor>
      <arglist>(const Quaternion &amp;a, const Quaternion &amp;b, float t, bool allowFlip=true)</arglist>
    </member>
    <member kind="function">
      <name>Quaternion::squad</name>
      <anchor>4b218b205d88a4dd50c8de38522b5812</anchor>
      <arglist>(const Quaternion &amp;a, const Quaternion &amp;tgA, const Quaternion &amp;tgB, const Quaternion &amp;b, float t)</arglist>
    </member>
    <member kind="function">
      <name>Quaternion::dot</name>
      <anchor>80d06247e39abf2980e56d2fe8c4bb83</anchor>
      <arglist>(const Quaternion &amp;a, const Quaternion &amp;b)</arglist>
    </member>
    <member kind="function">
      <name>Quaternion::log</name>
      <anchor>4db67fbb8171a5e781b56404112cf848</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <name>Quaternion::exp</name>
      <anchor>5c546a33cc0c65f24b7e9c48e5069ba7</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <name>Quaternion::lnDif</name>
      <anchor>f4c74176967acca6e3947977351e1c68</anchor>
      <arglist>(const Quaternion &amp;a, const Quaternion &amp;b)</arglist>
    </member>
    <member kind="function">
      <name>Quaternion::squadTangent</name>
      <anchor>2a27d43a68da6c7c5243fe81e5a9d005</anchor>
      <arglist>(const Quaternion &amp;before, const Quaternion &amp;center, const Quaternion &amp;after)</arglist>
    </member>
    <member kind="function">
      <name>Quaternion::randomQuaternion</name>
      <anchor>87d5e98d958fdc63100979fcff1d9976</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <name>Quaternion::domElement</name>
      <anchor>48e0e2dd26cd96418c8b889ceabe80f6</anchor>
      <arglist>(const QString &amp;name, QDomDocument &amp;document) const </arglist>
    </member>
    <member kind="function">
      <name>Quaternion::initFromDOMElement</name>
      <anchor>cd13d2ddeca530cb9f26ead47f7d25d3</anchor>
      <arglist>(const QDomElement &amp;element)</arglist>
    </member>
    <member kind="function">
      <name>Quaternion::operator&lt;&lt;</name>
      <anchor>16d5f2f021103b05006b0c58fbd48796</anchor>
      <arglist>(std::ostream &amp;o, const qglviewer::Vec &amp;)</arglist>
    </member>
    <member kind="function">
      <name>Quaternion::operator*</name>
      <anchor>76b3ffdb188246ff6559069cb3f5b919</anchor>
      <arglist>(const Quaternion &amp;a, const Quaternion &amp;b)</arglist>
    </member>
    <member kind="function">
      <name>Quaternion::operator*</name>
      <anchor>19ce6efe5ef2744c8293e8ba1a39b9e4</anchor>
      <arglist>(const Quaternion &amp;q, const Vec &amp;v)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>Vec</name>
    <filename>classqglviewer_1_1Vec.html</filename>
  </compound>
  <compound kind="class">
    <name>qglviewer::Vec</name>
    <filename>classqglviewer_1_1Vec.html</filename>
    <member kind="function">
      <name>Vec::Vec</name>
      <anchor>82cf7e1c93ee9188fefb25b86fc6c5b0</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <name>Vec::Vec</name>
      <anchor>85c7b6c44bcde7ebe227b2744651008d</anchor>
      <arglist>(float X, float Y, float Z)</arglist>
    </member>
    <member kind="function">
      <name>Vec::Vec</name>
      <anchor>4d1a636ad977799255e695ac5f870b2d</anchor>
      <arglist>(const C &amp;c)</arglist>
    </member>
    <member kind="function">
      <name>Vec::Vec</name>
      <anchor>e789c3b0b8e39895b96f417a36db4b8a</anchor>
      <arglist>(const QDomElement &amp;element)</arglist>
    </member>
    <member kind="function">
      <name>Vec::operator=</name>
      <anchor>9d105072fa3c0fc1e66c162ba083fee8</anchor>
      <arglist>(const Vec &amp;v)</arglist>
    </member>
    <member kind="function">
      <name>Vec::setValue</name>
      <anchor>19369167561ad33986ea8b903a35cd4e</anchor>
      <arglist>(float X, float Y, float Z)</arglist>
    </member>
    <member kind="function">
      <name>Vec::operator[]</name>
      <anchor>0c34e903011e152d5ac3f2a867b56373</anchor>
      <arglist>(int i) const </arglist>
    </member>
    <member kind="function">
      <name>Vec::operator[]</name>
      <anchor>1dc1be6e3b00fd36f7b33a77f5acb849</anchor>
      <arglist>(int i)</arglist>
    </member>
    <member kind="function">
      <name>Vec::operator const float *</name>
      <anchor>54795c57f1d998af73b630d786de2932</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>Vec::operator float *</name>
      <anchor>122685d2407dde6ec6eee564a69eb3aa</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <name>Vec::operator+=</name>
      <anchor>3be0b4bc904d39b75329eae40177240e</anchor>
      <arglist>(const Vec &amp;a)</arglist>
    </member>
    <member kind="function">
      <name>Vec::operator-=</name>
      <anchor>fa0c39c7dfe9db8b942f01f330925571</anchor>
      <arglist>(const Vec &amp;a)</arglist>
    </member>
    <member kind="function">
      <name>Vec::operator*=</name>
      <anchor>394741965f9db42d6c88855f9f3ba1b7</anchor>
      <arglist>(float k)</arglist>
    </member>
    <member kind="function">
      <name>Vec::operator/=</name>
      <anchor>cf001831145b0a8f861009e59486c58e</anchor>
      <arglist>(float k)</arglist>
    </member>
    <member kind="function">
      <name>Vec::orthogonalVec</name>
      <anchor>30d49f3d3753a7111fc67cdde253a43d</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>Vec::squaredNorm</name>
      <anchor>097f0ca0bd6613a8efa839e13cdcba5e</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>Vec::norm</name>
      <anchor>3d53aed01800d847b8ce126d243c6166</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>Vec::normalize</name>
      <anchor>b5068053e7758f065a8725c40ab39194</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <name>Vec::unit</name>
      <anchor>7f3923a61bb70c53263379de9783d265</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>Vec::projectOnAxis</name>
      <anchor>8c89e6f39afb5bd92e4cae4ffa6c3221</anchor>
      <arglist>(const Vec &amp;direction)</arglist>
    </member>
    <member kind="function">
      <name>Vec::projectOnPlane</name>
      <anchor>6ed4feca0d9a299bdc68632eb7db51b9</anchor>
      <arglist>(const Vec &amp;normal)</arglist>
    </member>
    <member kind="function">
      <name>Vec::domElement</name>
      <anchor>48e0e2dd26cd96418c8b889ceabe80f6</anchor>
      <arglist>(const QString &amp;name, QDomDocument &amp;document) const </arglist>
    </member>
    <member kind="function">
      <name>Vec::initFromDOMElement</name>
      <anchor>cd13d2ddeca530cb9f26ead47f7d25d3</anchor>
      <arglist>(const QDomElement &amp;element)</arglist>
    </member>
    <member kind="function">
      <name>Vec::operator&lt;&lt;</name>
      <anchor>16d5f2f021103b05006b0c58fbd48796</anchor>
      <arglist>(std::ostream &amp;o, const qglviewer::Vec &amp;)</arglist>
    </member>
    <member kind="function">
      <name>Vec::operator+</name>
      <anchor>bc7a7829beaefabef78bbf1aa8aa69f7</anchor>
      <arglist>(const Vec &amp;a, const Vec &amp;b)</arglist>
    </member>
    <member kind="function">
      <name>Vec::operator-</name>
      <anchor>84bd051b693ee420203ae5947ac38bdc</anchor>
      <arglist>(const Vec &amp;a, const Vec &amp;b)</arglist>
    </member>
    <member kind="function">
      <name>Vec::operator-</name>
      <anchor>780b6fbbee93774c986eb645dc5c6373</anchor>
      <arglist>(const Vec &amp;a)</arglist>
    </member>
    <member kind="function">
      <name>Vec::operator*</name>
      <anchor>4b30062b33ff5835991e9a629dbfb042</anchor>
      <arglist>(const Vec &amp;a, float k)</arglist>
    </member>
    <member kind="function">
      <name>Vec::operator*</name>
      <anchor>e60fbf5f6445e402c70b640253bc90c0</anchor>
      <arglist>(float k, const Vec &amp;a)</arglist>
    </member>
    <member kind="function">
      <name>Vec::operator/</name>
      <anchor>f55c8ddbc588825a76a31e26c476769c</anchor>
      <arglist>(const Vec &amp;a, float k)</arglist>
    </member>
    <member kind="function">
      <name>Vec::operator!=</name>
      <anchor>a694f4dad59f93a8b8364d1b9cbdfd54</anchor>
      <arglist>(const Vec &amp;a, const Vec &amp;b)</arglist>
    </member>
    <member kind="function">
      <name>Vec::operator==</name>
      <anchor>a36a8f02bb5bd8f5ab8532a79ceb4e9f</anchor>
      <arglist>(const Vec &amp;a, const Vec &amp;b)</arglist>
    </member>
    <member kind="function">
      <name>Vec::operator*</name>
      <anchor>09c5bc39a12b3d8e3881229f0e817758</anchor>
      <arglist>(const Vec &amp;a, const Vec &amp;b)</arglist>
    </member>
    <member kind="function">
      <name>Vec::operator^</name>
      <anchor>f521e6619361cc97fff70ae3115e0317</anchor>
      <arglist>(const Vec &amp;a, const Vec &amp;b)</arglist>
    </member>
    <member kind="function">
      <name>Vec::cross</name>
      <anchor>835244f47bc3744aed547f6ae814e13e</anchor>
      <arglist>(const Vec &amp;a, const Vec &amp;b)</arglist>
    </member>
    <member kind="function">
      <name>Vec::x</name>
      <anchor>d0da36b2558901e21e7a30f6c227a45e</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <name>Vec::y</name>
      <anchor>a4f0d3eebc3c443f9be81bf48561a217</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <name>Vec::z</name>
      <anchor>f73583b1e980b0aa03f9884812e9fd4d</anchor>
      <arglist></arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>WorldConstraint</name>
    <filename>classqglviewer_1_1WorldConstraint.html</filename>
  </compound>
  <compound kind="class">
    <name>qglviewer::WorldConstraint</name>
    <base>qglviewer::AxisPlaneConstraint</base>
    <base>AxisPlaneConstraint</base>
    <filename>classqglviewer_1_1WorldConstraint.html</filename>
    <member kind="function">
      <name>WorldConstraint::~WorldConstraint</name>
      <anchor>150934716deed3fda4d4121307d17fd0</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <name>WorldConstraint::constrainTranslation</name>
      <anchor>dc695bfbc605b5631be663b28a4ea9f6</anchor>
      <arglist>(Vec &amp;translation, Frame *const frame)</arglist>
    </member>
    <member kind="function">
      <name>WorldConstraint::constrainRotation</name>
      <anchor>71c099a2c356f715f8bf34052875cd25</anchor>
      <arglist>(Quaternion &amp;rotation, Frame *const frame)</arglist>
    </member>
  </compound>
  <compound kind="class">
    <name>QGLViewer</name>
    <filename>classQGLViewer.html</filename>
    <member kind="function">
      <name>KeyboardAction</name>
      <anchor>7a90ec0b49f9586addb5eed9026077c1</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <name>RAW_AXIS</name>
      <anchor>7a90ec0b49f9586addb5eed9026077c1a9c213a1cf39290bfcad5d6813d2395d</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <name>RAW_GRID</name>
      <anchor>7a90ec0b49f9586addb5eed9026077c19891606ac8b160f15d3e705f7d192604</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <name>ISPLAY_FPS</name>
      <anchor>7a90ec0b49f9586addb5eed9026077c17522d8401eb437769071ba3b1562ca97</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <name>NABLE_TEXT</name>
      <anchor>7a90ec0b49f9586addb5eed9026077c1f12e793187e1edaf1e236818225b9e0e</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <name>XIT_VIEWER</name>
      <anchor>7a90ec0b49f9586addb5eed9026077c17ff2639b181c08e5d9196a0303a72cd1</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <name>AVE_SCREENSHOT</name>
      <anchor>7a90ec0b49f9586addb5eed9026077c11e9b410aa72809cf30d86b2d34ee7239</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <name>AMERA_MODE</name>
      <anchor>7a90ec0b49f9586addb5eed9026077c191b759170cb0389695a3c219a9a69073</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <name>ULL_SCREEN</name>
      <anchor>7a90ec0b49f9586addb5eed9026077c14750f7f8fc87e44b233c6186713f8e59</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <name>TEREO</name>
      <anchor>7a90ec0b49f9586addb5eed9026077c1a0cd9874b7ec35409aa4ef363b818a4e</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <name>NIMATION</name>
      <anchor>7a90ec0b49f9586addb5eed9026077c1f3b49771c99e24d1407f9fc662fc7a6f</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <name>ELP</name>
      <anchor>7a90ec0b49f9586addb5eed9026077c19f5cb747b2e1f0ea781d2b1f2a5b4824</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <name>DIT_CAMERA</name>
      <anchor>7a90ec0b49f9586addb5eed9026077c197ed373cfcaeadc41c6975357bbc17df</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <name>OVE_CAMERA_LEFT</name>
      <anchor>7a90ec0b49f9586addb5eed9026077c1c71e3cca6e8031a8c05944d15f257b30</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <name>OVE_CAMERA_RIGHT</name>
      <anchor>7a90ec0b49f9586addb5eed9026077c1dee5dcac0e4f4dfe9190a769f3575a63</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <name>OVE_CAMERA_UP</name>
      <anchor>7a90ec0b49f9586addb5eed9026077c1eaca3cc65bb13383b55ff6704f80adeb</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <name>OVE_CAMERA_DOWN</name>
      <anchor>7a90ec0b49f9586addb5eed9026077c1e8ffd7bbd8e032bf43298331a6525274</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <name>NCREASE_FLYSPEED</name>
      <anchor>7a90ec0b49f9586addb5eed9026077c10f5233365123b2f88633907040a95a5a</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <name>ECREASE_FLYSPEED</name>
      <anchor>7a90ec0b49f9586addb5eed9026077c1595e91c7270892a31306f01e105c1dd8</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <name>MouseHandler</name>
      <anchor>5b90ab220b7700ca28db5ecf3217325d</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <name>AMERA</name>
      <anchor>5b90ab220b7700ca28db5ecf3217325dda31f516cdf218b68b790fb31e8a6956</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <name>RAME</name>
      <anchor>5b90ab220b7700ca28db5ecf3217325d200c1bcf1eaa8635daa3cbb5fdd2ebb6</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <name>ClickAction</name>
      <anchor>85fe75121d351785616b75b2c5661d8f</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <name>O_CLICK_ACTION</name>
      <anchor>85fe75121d351785616b75b2c5661d8fed60c81bb5edd0570ff12ac8a0e2b604</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <name>OOM_ON_PIXEL</name>
      <anchor>85fe75121d351785616b75b2c5661d8fc7b18b21c4c8f1eeb5d54bf1b7919db4</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <name>OOM_TO_FIT</name>
      <anchor>85fe75121d351785616b75b2c5661d8fb1efbb77356f16254fd4a62e1236b531</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <name>ELECT</name>
      <anchor>85fe75121d351785616b75b2c5661d8f1697a91b22c2369eb2ba427c2d193329</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <name>AP_FROM_PIXEL</name>
      <anchor>85fe75121d351785616b75b2c5661d8f6423101303db857a4217e8e66606128a</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <name>AP_IS_CENTER</name>
      <anchor>85fe75121d351785616b75b2c5661d8f131d4a2b38607d5d753c4fe19884a9cc</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <name>ENTER_FRAME</name>
      <anchor>85fe75121d351785616b75b2c5661d8fbf4ad7098f468bfaf170fd5e16902929</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <name>ENTER_SCENE</name>
      <anchor>85fe75121d351785616b75b2c5661d8f23a1d829d84b71f5aa5a0e19385e8ce7</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <name>HOW_ENTIRE_SCENE</name>
      <anchor>85fe75121d351785616b75b2c5661d8f3f717d1605f3ca83254beb93ea399ddc</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <name>LIGN_FRAME</name>
      <anchor>85fe75121d351785616b75b2c5661d8f3d318f59bc81979e3922c7e716085304</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <name>LIGN_CAMERA</name>
      <anchor>85fe75121d351785616b75b2c5661d8f35685e5c7e681c3c0eb079e4f132a82a</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <name>MouseAction</name>
      <anchor>ded669cb17515ea2b5971496f9aef875</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <name>O_MOUSE_ACTION</name>
      <anchor>ded669cb17515ea2b5971496f9aef8753b20d5f27f63af5ea6e5b6af1112ecf8</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <name>OTATE</name>
      <anchor>ded669cb17515ea2b5971496f9aef8753dcfe0046eb5876e287dbf0914819b16</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <name>OOM</name>
      <anchor>ded669cb17515ea2b5971496f9aef875604adefe799fe794cab6b76ed1108201</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <name>RANSLATE</name>
      <anchor>ded669cb17515ea2b5971496f9aef875bc6501410409b0638909b580970b35f7</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <name>OVE_FORWARD</name>
      <anchor>ded669cb17515ea2b5971496f9aef87599906f0ddded6cfdab57271cd33e308c</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <name>OOK_AROUND</name>
      <anchor>ded669cb17515ea2b5971496f9aef87521fa52d8ef1574dce79cab9ddbb6cd73</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <name>OVE_BACKWARD</name>
      <anchor>ded669cb17515ea2b5971496f9aef875b3313fc5887b62fd14b36f1d67903e08</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <name>CREEN_ROTATE</name>
      <anchor>ded669cb17515ea2b5971496f9aef875410b0fa7f49e7eedd6d739db37c67209</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <name>OLL</name>
      <anchor>ded669cb17515ea2b5971496f9aef8752eeb9fef8a6a516fa6437a44a6efbd52</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <name>RIVE</name>
      <anchor>ded669cb17515ea2b5971496f9aef875f7b6d6d8e5e14633d388ef9cc7a941b7</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <name>CREEN_TRANSLATE</name>
      <anchor>ded669cb17515ea2b5971496f9aef8753de224b064ad81a76d8739cf288543a3</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <name>OOM_ON_REGION</name>
      <anchor>ded669cb17515ea2b5971496f9aef875fbac98d470c69690e178ff5ab9ad504d</anchor>
      <arglist></arglist>
    </member>
    <member kind="function">
      <name>QGLViewer</name>
      <anchor>1a30c8c6e5166bc2eb82d599351d7712</anchor>
      <arglist>(QWidget *parent=0, const QGLWidget *shareWidget=0, Qt::WFlags flags=0)</arglist>
    </member>
    <member kind="function">
      <name>QGLViewer</name>
      <anchor>f38a060fc322d6381e28245b2a05a880</anchor>
      <arglist>(QGLContext *context, QWidget *parent=0, const QGLWidget *shareWidget=0, Qt::WFlags flags=0)</arglist>
    </member>
    <member kind="function">
      <name>QGLViewer</name>
      <anchor>172fc2a1bb06b7811576a6eaff46ec90</anchor>
      <arglist>(const QGLFormat &amp;format, QWidget *parent=0, const QGLWidget *shareWidget=0, Qt::WFlags flags=0)</arglist>
    </member>
    <member kind="function">
      <name>~QGLViewer</name>
      <anchor>8c90239e64b7a43473a189d5da865ac7</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <name>axisIsDrawn</name>
      <anchor>7d38e6f11078e886f7978525def15797</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>gridIsDrawn</name>
      <anchor>ee18c56a8321a60771b085a5fe798ee7</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>FPSIsDisplayed</name>
      <anchor>4b8985b86aca5584d9869c8ac868984a</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>textIsEnabled</name>
      <anchor>87896d67f84ddb458e1e5ab326db2631</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>cameraIsEdited</name>
      <anchor>1bf2817fb27f0ad326e3db75aeb46af7</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>setAxisIsDrawn</name>
      <anchor>5be8cff3702c1a130f8a17330737a887</anchor>
      <arglist>(bool draw=true)</arglist>
    </member>
    <member kind="function">
      <name>setGridIsDrawn</name>
      <anchor>e9dd114195dfdf82e23b5754b7c161de</anchor>
      <arglist>(bool draw=true)</arglist>
    </member>
    <member kind="function">
      <name>setFPSIsDisplayed</name>
      <anchor>ad24c89e014de3ea16f071c3bc18f4db</anchor>
      <arglist>(bool display=true)</arglist>
    </member>
    <member kind="function">
      <name>setTextIsEnabled</name>
      <anchor>73d76caa402acd217e504d0bcd13e421</anchor>
      <arglist>(bool enable=true)</arglist>
    </member>
    <member kind="function">
      <name>setCameraIsEdited</name>
      <anchor>521c83b1d745b37331932b4d7b976d41</anchor>
      <arglist>(bool edit=true)</arglist>
    </member>
    <member kind="function">
      <name>toggleAxisIsDrawn</name>
      <anchor>cf2ccb8a346c04a5c7da87da0e8e601f</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <name>toggleGridIsDrawn</name>
      <anchor>8dfaa8be71152ae881b9347235ccc6a1</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <name>toggleFPSIsDisplayed</name>
      <anchor>b8a88948237894dca2b7b57a67226d66</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <name>toggleTextIsEnabled</name>
      <anchor>b92b2b41db85e4347675b0bc453366d7</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <name>toggleCameraIsEdited</name>
      <anchor>3a53b30eacfccf0825b808977d634936</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <name>backgroundColor</name>
      <anchor>7ddf68dcfb09cc5a991a06d91cb4cc5b</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>foregroundColor</name>
      <anchor>a2f726def3615050a9c816c0ca32171d</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>setBackgroundColor</name>
      <anchor>70d4e21190fdc47edd88f078dd2037cb</anchor>
      <arglist>(const QColor &amp;color)</arglist>
    </member>
    <member kind="function">
      <name>setForegroundColor</name>
      <anchor>7d986b1944dc5a190e509835e7c79eec</anchor>
      <arglist>(const QColor &amp;color)</arglist>
    </member>
    <member kind="function">
      <name>sceneRadius</name>
      <anchor>58c17044cc4a601c6b446bf5e83513f9</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>sceneCenter</name>
      <anchor>5a0503bc22a710f96bfd779eaf538bab</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>setSceneRadius</name>
      <anchor>ef99f105486d457f0199fcc35181fa83</anchor>
      <arglist>(float radius)</arglist>
    </member>
    <member kind="function">
      <name>setSceneCenter</name>
      <anchor>6712acd05f9d518d6babfe96e537d06d</anchor>
      <arglist>(const qglviewer::Vec &amp;center)</arglist>
    </member>
    <member kind="function">
      <name>setSceneBoundingBox</name>
      <anchor>a8339148d876e633c27e5df826f06c9c</anchor>
      <arglist>(const qglviewer::Vec &amp;min, const qglviewer::Vec &amp;max)</arglist>
    </member>
    <member kind="function">
      <name>showEntireScene</name>
      <anchor>da8dd7d2346ebf46ed1f9822a8418df2</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <name>camera</name>
      <anchor>27a9e97573822d296b48e1c408b74042</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>manipulatedFrame</name>
      <anchor>ba8c9c519574192fb7197bdbad8049db</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>setCamera</name>
      <anchor>9c45e1d2c4777de28664d3db952b7585</anchor>
      <arglist>(qglviewer::Camera *const camera)</arglist>
    </member>
    <member kind="function">
      <name>setManipulatedFrame</name>
      <anchor>c6964ec1ebb2f42464313e0c43e767a3</anchor>
      <arglist>(qglviewer::ManipulatedFrame *frame)</arglist>
    </member>
    <member kind="function">
      <name>mouseGrabber</name>
      <anchor>6c834adafd727025b63741dd27cb6925</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>setMouseGrabberIsEnabled</name>
      <anchor>3f0b956f948f469c095ff3c2c1b73494</anchor>
      <arglist>(const qglviewer::MouseGrabber *const mouseGrabber, bool enabled=true)</arglist>
    </member>
    <member kind="function">
      <name>mouseGrabberIsEnabled</name>
      <anchor>5fe9b0a1fcd39a5cb132ffdc86f2574f</anchor>
      <arglist>(const qglviewer::MouseGrabber *const mouseGrabber)</arglist>
    </member>
    <member kind="function">
      <name>setMouseGrabber</name>
      <anchor>993285ef8a16ccbdc5d57a1264212712</anchor>
      <arglist>(qglviewer::MouseGrabber *mouseGrabber)</arglist>
    </member>
    <member kind="function">
      <name>aspectRatio</name>
      <anchor>915589f4d93e15d110444ed9b3464fa1</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>currentFPS</name>
      <anchor>6fde4d85dfc5338aa237ba1eb505e975</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <name>isFullScreen</name>
      <anchor>129c439f36bb669672148192abc8ffed</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>displaysInStereo</name>
      <anchor>2fc4c62e317a0f64c2a943ed11faa337</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>sizeHint</name>
      <anchor>d8288a17cf54658f1ce1c0db9e97dc8a</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>setFullScreen</name>
      <anchor>7e021e15180ba348e90c955bdd28b1a4</anchor>
      <arglist>(bool fullScreen=true)</arglist>
    </member>
    <member kind="function">
      <name>setStereoDisplay</name>
      <anchor>fe362ba5c3851a93f23fb833b2479f08</anchor>
      <arglist>(bool stereo=true)</arglist>
    </member>
    <member kind="function">
      <name>toggleFullScreen</name>
      <anchor>a5b47397e4ad7c2bb9573e0d186170e0</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <name>toggleStereoDisplay</name>
      <anchor>4e0ebf98eacbcbad3b094e26d9e35886</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <name>toggleCameraMode</name>
      <anchor>17ea00dd30fb78086cf7e22bc2f10695</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <name>drawArrow</name>
      <anchor>14fc47f313bbb65c38d2a8ae754215e0</anchor>
      <arglist>(float length=1.0f, float radius=-1.0f, int nbSubdivisions=12)</arglist>
    </member>
    <member kind="function">
      <name>drawArrow</name>
      <anchor>27edb1331c7bf373d126487e9547969f</anchor>
      <arglist>(const qglviewer::Vec &amp;from, const qglviewer::Vec &amp;to, float radius=-1.0f, int nbSubdivisions=12)</arglist>
    </member>
    <member kind="function">
      <name>drawAxis</name>
      <anchor>f18c0661b9a86e6b07ae344e05979c4c</anchor>
      <arglist>(float length=1.0f)</arglist>
    </member>
    <member kind="function">
      <name>drawGrid</name>
      <anchor>d4a4d99fabe53083099c70439bc3564d</anchor>
      <arglist>(float size=1.0f, int nbSubdivisions=10)</arglist>
    </member>
    <member kind="function">
      <name>startScreenCoordinatesSystem</name>
      <anchor>5825ac26bdef13ae5ddd021e318aaf15</anchor>
      <arglist>(bool upward=false) const </arglist>
    </member>
    <member kind="function">
      <name>stopScreenCoordinatesSystem</name>
      <anchor>9cff22af974391604bff7f91df789138</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>drawText</name>
      <anchor>d604ec747b161c869877fcb647a3c775</anchor>
      <arglist>(int x, int y, const QString &amp;text, const QFont &amp;fnt=QFont())</arglist>
    </member>
    <member kind="function">
      <name>displayMessage</name>
      <anchor>61336516f9771ac6aef90875f848add4</anchor>
      <arglist>(const QString &amp;message, int delay=2000)</arglist>
    </member>
    <member kind="function">
      <name>drawLight</name>
      <anchor>2a3b971fe826a90efaffcb7c68fdcc53</anchor>
      <arglist>(GLenum light, float scale=1.0f) const </arglist>
    </member>
    <member kind="function">
      <name>width</name>
      <anchor>369399896761e31ae71db57fdd0ba431</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>height</name>
      <anchor>e26bcfe2f33f5873dbdfb6948cf1f59f</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>updateGL</name>
      <anchor>e12b7378efbffabc24a133ca1deb19ae</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <name>convertToGLFormat</name>
      <anchor>75679575b3b071cddce2a56c77e1bd68</anchor>
      <arglist>(const QImage &amp;image)</arglist>
    </member>
    <member kind="function">
      <name>qglColor</name>
      <anchor>8911f3aec33c17d1eba4390436b5c868</anchor>
      <arglist>(const QColor &amp;color) const </arglist>
    </member>
    <member kind="function">
      <name>qglClearColor</name>
      <anchor>88b363f05e720484cde551d5f798e69b</anchor>
      <arglist>(const QColor &amp;color) const </arglist>
    </member>
    <member kind="function">
      <name>isValid</name>
      <anchor>ac1b70a2ed67ead038c4d3f5ac4d8a81</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>isSharing</name>
      <anchor>514d24ec3ec6c94657ef302a2b3fa74a</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>makeCurrent</name>
      <anchor>1436277e13026c94601bbe37a2f1d262</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <name>hasMouseTracking</name>
      <anchor>47c0968a61bcd23c491817631e4ec953</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>resize</name>
      <anchor>d1185e99a9efec124b7a9bede84a6cf1</anchor>
      <arglist>(int width, int height)</arglist>
    </member>
    <member kind="function">
      <name>setMouseTracking</name>
      <anchor>cf0a4ee197fe91b8c07dd74cad1aafaa</anchor>
      <arglist>(bool enable)</arglist>
    </member>
    <member kind="function">
      <name>autoBufferSwap</name>
      <anchor>36faca915c37548a53ab04f297bb5c17</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>setAutoBufferSwap</name>
      <anchor>d95e0ffd255bcc1fb69c9213d8d8b017</anchor>
      <arglist>(bool on)</arglist>
    </member>
    <member kind="function">
      <name>snapshotFileName</name>
      <anchor>00f2094711c7349fe1f74b6fc0b5530f</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>snapshotFormat</name>
      <anchor>bbb1add55632dced395e2f1b78ef491c</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>snapshotCounter</name>
      <anchor>4de825bb96eba2c5f6a55facb9dc4cef</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>snapshotQuality</name>
      <anchor>50685165e76c57dc87dea3c84b7e5be0</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <name>saveSnapshot</name>
      <anchor>1cf2ffb973b096b249dc7e90327a2a8e</anchor>
      <arglist>(bool automatic=true, bool overwrite=false)</arglist>
    </member>
    <member kind="function">
      <name>saveSnapshot</name>
      <anchor>82544567565cfa0b750cb24ac1aa8358</anchor>
      <arglist>(const QString &amp;fileName, bool overwrite=false)</arglist>
    </member>
    <member kind="function">
      <name>setSnapshotFileName</name>
      <anchor>a15be68d137eca013942eae69e40e0ee</anchor>
      <arglist>(const QString &amp;name)</arglist>
    </member>
    <member kind="function">
      <name>setSnapshotFormat</name>
      <anchor>82043998256d08163f79ceee83278e14</anchor>
      <arglist>(const QString &amp;format)</arglist>
    </member>
    <member kind="function">
      <name>setSnapshotCounter</name>
      <anchor>feb307bbd1a56cdfb3749f699d4af03c</anchor>
      <arglist>(int counter)</arglist>
    </member>
    <member kind="function">
      <name>setSnapshotQuality</name>
      <anchor>b64b04b76b1f35ee1b3f07fa747dc9a0</anchor>
      <arglist>(int quality)</arglist>
    </member>
    <member kind="function">
      <name>openSnapshotFormatDialog</name>
      <anchor>c9c409fd034bc5ab42aacfa4c23fe55c</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <name>bufferTextureId</name>
      <anchor>6435e0a64e14d04dce25e524051f8d69</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>bufferTextureMaxU</name>
      <anchor>c60a0831696a80344fd04b2fba039f48</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>bufferTextureMaxV</name>
      <anchor>8d3ecfdb46f8971e46a0ab0f52c5bbf7</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>copyBufferToTexture</name>
      <anchor>eb1721bfb1c032ae68665808bb2f4453</anchor>
      <arglist>(GLint internalFormat, GLenum format=GL_NONE)</arglist>
    </member>
    <member kind="function">
      <name>animationIsStarted</name>
      <anchor>d865668850fb0aa249e79f21d2e9d40e</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>animationPeriod</name>
      <anchor>700d9398d4293d9274766efa8b17917e</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>setAnimationPeriod</name>
      <anchor>a37d4e0afe6a47e8f4f828ed41072176</anchor>
      <arglist>(int period)</arglist>
    </member>
    <member kind="function">
      <name>startAnimation</name>
      <anchor>d5344a5f702678f309fafa0c699b2cf3</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <name>stopAnimation</name>
      <anchor>84c1367b486680bcf22987540e217cfb</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <name>animate</name>
      <anchor>64465ac69c7fe9f4f8519a57501c76c2</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <name>toggleAnimation</name>
      <anchor>67be4f5d4065852e7f46a2fc6197c070</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <name>viewerInitialized</name>
      <anchor>252b68caec768d882a3fa78ecd1499db</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <name>drawNeeded</name>
      <anchor>7a712ca70a0b1c22af51363b786fc86e</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <name>drawFinished</name>
      <anchor>fc74e28548768da157f2fe75bced2803</anchor>
      <arglist>(bool automatic)</arglist>
    </member>
    <member kind="function">
      <name>animateNeeded</name>
      <anchor>841503c97db5a51e33f8a7e56d4ca006</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <name>helpRequired</name>
      <anchor>64f461121859dc0c19e7af2d413935e0</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <name>axisIsDrawnChanged</name>
      <anchor>541cdbec67d0c5895cd6c77c01b0f89e</anchor>
      <arglist>(bool drawn)</arglist>
    </member>
    <member kind="function">
      <name>gridIsDrawnChanged</name>
      <anchor>02d191cc46db491d9807266fe62b6178</anchor>
      <arglist>(bool drawn)</arglist>
    </member>
    <member kind="function">
      <name>FPSIsDisplayedChanged</name>
      <anchor>4b005fb3bda4582ce4ab7aeda6692699</anchor>
      <arglist>(bool displayed)</arglist>
    </member>
    <member kind="function">
      <name>textIsEnabledChanged</name>
      <anchor>c0b30c0de291a24af638e8c1d6171943</anchor>
      <arglist>(bool enabled)</arglist>
    </member>
    <member kind="function">
      <name>cameraIsEditedChanged</name>
      <anchor>38968d2f050efa14869c2e4de416b7b4</anchor>
      <arglist>(bool edited)</arglist>
    </member>
    <member kind="function">
      <name>stereoChanged</name>
      <anchor>9d7df8ab7c351e32da3c72b19c4585c0</anchor>
      <arglist>(bool on)</arglist>
    </member>
    <member kind="function">
      <name>pointSelected</name>
      <anchor>b78f96913c9aede4854b0efccf53f983</anchor>
      <arglist>(const QMouseEvent *e)</arglist>
    </member>
    <member kind="function">
      <name>mouseGrabberChanged</name>
      <anchor>959ca6df1731d57aa692af99abcd28d1</anchor>
      <arglist>(qglviewer::MouseGrabber *mouseGrabber)</arglist>
    </member>
    <member kind="function">
      <name>helpString</name>
      <anchor>38ddb3cdf15e24de824a2d7a170ec915</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>mouseString</name>
      <anchor>6d66b99a4c5a38ef0072f350b055201b</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>keyboardString</name>
      <anchor>0e20e13c1170d50b46b6fe2a49377690</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>help</name>
      <anchor>97ee70a8770dc30d06c744b24eb2fcfc</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <name>aboutQGLViewer</name>
      <anchor>f08b8ca0f43910754ecd5d314e3febf0</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <name>helpWidget</name>
      <anchor>f3af989be04f1d45b6ff3f748c2e9d4a</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <name>resizeGL</name>
      <anchor>3efe88f982dbec7825725dd954991139</anchor>
      <arglist>(int width, int height)</arglist>
    </member>
    <member kind="function">
      <name>initializeGL</name>
      <anchor>2d3d45239c78255c23a70ca558b4d4f1</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <name>init</name>
      <anchor>9339772ec5ac9fa929938109207f2863</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <name>paintGL</name>
      <anchor>c5cbfafb28ef4c0474ae96437294f547</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <name>preDraw</name>
      <anchor>9c6b3ec107b4f010cf1fcd8c51ca92e4</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <name>preDrawStereo</name>
      <anchor>43df4754781c2aaf3236d676401cec59</anchor>
      <arglist>(bool leftBuffer=true)</arglist>
    </member>
    <member kind="function">
      <name>draw</name>
      <anchor>bc45d04e5f5ce1fbd68f920fcdb2d0e0</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <name>fastDraw</name>
      <anchor>8b6601997fe7a83e7cd041104d4b21d2</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <name>postDraw</name>
      <anchor>24f5dce22199f5eea71f034cae6ae4fa</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <name>mousePressEvent</name>
      <anchor>991f0a076bd76a1ee5bda0df7fa474f4</anchor>
      <arglist>(QMouseEvent *)</arglist>
    </member>
    <member kind="function">
      <name>mouseMoveEvent</name>
      <anchor>88e672693c2cfdbaf9af942a58a8e1dd</anchor>
      <arglist>(QMouseEvent *)</arglist>
    </member>
    <member kind="function">
      <name>mouseReleaseEvent</name>
      <anchor>158642bef03883cc4157b8b40e1aa0ea</anchor>
      <arglist>(QMouseEvent *)</arglist>
    </member>
    <member kind="function">
      <name>mouseDoubleClickEvent</name>
      <anchor>a11ba8137b62942cede01c57aade3073</anchor>
      <arglist>(QMouseEvent *)</arglist>
    </member>
    <member kind="function">
      <name>wheelEvent</name>
      <anchor>bc61c05ed30a94d66ab715c718532c03</anchor>
      <arglist>(QWheelEvent *)</arglist>
    </member>
    <member kind="function">
      <name>keyPressEvent</name>
      <anchor>2cc4c898ca007c7cc0ebb7791aa3e5b3</anchor>
      <arglist>(QKeyEvent *)</arglist>
    </member>
    <member kind="function">
      <name>timerEvent</name>
      <anchor>cd355cb527aec563bbefc75edc9deffd</anchor>
      <arglist>(QTimerEvent *)</arglist>
    </member>
    <member kind="function">
      <name>closeEvent</name>
      <anchor>3fb8c90e5c48e6ccc09f9125aa86943e</anchor>
      <arglist>(QCloseEvent *)</arglist>
    </member>
    <member kind="function">
      <name>selectedName</name>
      <anchor>0015a8bc683c3a1483c5ad638550bdde</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>selectBufferSize</name>
      <anchor>13e131ca92b021fb8946a9af0b0c269d</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>selectRegionWidth</name>
      <anchor>16ad2d80aa574d32ae8237f56cfd7e06</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>selectRegionHeight</name>
      <anchor>db0b15577ca9bcabc99c78601cfce59f</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>selectBuffer</name>
      <anchor>87015f560fd54b358d185482ae82eec3</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <name>select</name>
      <anchor>3b20e4da96e6d8c038a08c7d34d182c6</anchor>
      <arglist>(const QMouseEvent *event)</arglist>
    </member>
    <member kind="function">
      <name>select</name>
      <anchor>f2ed82c063635ce439d9c73223e20fd6</anchor>
      <arglist>(const QPoint &amp;point)</arglist>
    </member>
    <member kind="function">
      <name>setSelectBufferSize</name>
      <anchor>e8af6dc4a89202211b764647caf3d1f3</anchor>
      <arglist>(int size)</arglist>
    </member>
    <member kind="function">
      <name>setSelectRegionWidth</name>
      <anchor>0bfc48a72feefc6c90bd187280853911</anchor>
      <arglist>(int width)</arglist>
    </member>
    <member kind="function">
      <name>setSelectRegionHeight</name>
      <anchor>e830757057c41db506410fd3c332d7dd</anchor>
      <arglist>(int height)</arglist>
    </member>
    <member kind="function">
      <name>setSelectedName</name>
      <anchor>eaa1327270d0bcdf991874e7d741afd5</anchor>
      <arglist>(int id)</arglist>
    </member>
    <member kind="function">
      <name>beginSelection</name>
      <anchor>f0a48cc50f194926bad38d4924162116</anchor>
      <arglist>(const QPoint &amp;point)</arglist>
    </member>
    <member kind="function">
      <name>drawWithNames</name>
      <anchor>528b238068a87472df8ac3a5b2481c55</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <name>endSelection</name>
      <anchor>0d164809a99bbe6ff2fc0dee33fe0e91</anchor>
      <arglist>(const QPoint &amp;point)</arglist>
    </member>
    <member kind="function">
      <name>postSelection</name>
      <anchor>3dc0d3b212e04bcafd63e9c3eb214a6c</anchor>
      <arglist>(const QPoint &amp;point)</arglist>
    </member>
    <member kind="function">
      <name>shortcut</name>
      <anchor>b6dd5b47ea7d89e1f86c05c58d25c11d</anchor>
      <arglist>(KeyboardAction action) const </arglist>
    </member>
    <member kind="function">
      <name>pathKey</name>
      <anchor>dfe21e109b45f8c79f2c360bf35fb835</anchor>
      <arglist>(int index) const </arglist>
    </member>
    <member kind="function">
      <name>addKeyFrameKeyboardModifiers</name>
      <anchor>7c91f5b77f92266cff3fe4ef99847b93</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>playPathKeyboardModifiers</name>
      <anchor>02cb4ab51746a7bb14d1fab077e94ea7</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>setShortcut</name>
      <anchor>910dc7fa27de6f4d8e9e8ed7fa7a1667</anchor>
      <arglist>(KeyboardAction action, int key)</arglist>
    </member>
    <member kind="function">
      <name>setKeyDescription</name>
      <anchor>9e97204faca448653fad94a37024a488</anchor>
      <arglist>(int key, QString description)</arglist>
    </member>
    <member kind="function">
      <name>setPathKey</name>
      <anchor>da7e68536b6593f8a8bde9a746c99c13</anchor>
      <arglist>(int key, int index=0)</arglist>
    </member>
    <member kind="function">
      <name>setPlayPathKeyboardModifiers</name>
      <anchor>7c941d66cf641971ef0bb99e998290dc</anchor>
      <arglist>(Qt::KeyboardModifiers modifiers)</arglist>
    </member>
    <member kind="function">
      <name>setAddKeyFrameKeyboardModifiers</name>
      <anchor>a9a5108680fffd096a3e086a3b8b45d4</anchor>
      <arglist>(Qt::KeyboardModifiers modifiers)</arglist>
    </member>
    <member kind="function">
      <name>mouseAction</name>
      <anchor>3bc7b6c4d3ca76c0faecfdbfd2ede69a</anchor>
      <arglist>(int state) const </arglist>
    </member>
    <member kind="function">
      <name>mouseHandler</name>
      <anchor>3d400c0d0a5824e70d36b01f9b7211c1</anchor>
      <arglist>(int state) const </arglist>
    </member>
    <member kind="function">
      <name>mouseButtonState</name>
      <anchor>5e9d4c07afc73f5c8a0926641e1428ac</anchor>
      <arglist>(MouseHandler handler, MouseAction action, bool withConstraint=true) const </arglist>
    </member>
    <member kind="function">
      <name>clickAction</name>
      <anchor>77651b87caa33aaa234457b1ef784e2a</anchor>
      <arglist>(int state, bool doubleClick, Qt::MouseButtons buttonsBefore) const </arglist>
    </member>
    <member kind="function">
      <name>getClickButtonState</name>
      <anchor>0fbf5d9d33985397f382ee2706c5898b</anchor>
      <arglist>(ClickAction action, int &amp;state, bool &amp;doubleClick, Qt::MouseButtons &amp;buttonsBefore) const </arglist>
    </member>
    <member kind="function">
      <name>wheelAction</name>
      <anchor>aeb802e1db589dad89db8300e82454f4</anchor>
      <arglist>(Qt::KeyboardModifiers modifiers) const </arglist>
    </member>
    <member kind="function">
      <name>wheelHandler</name>
      <anchor>24751ec23d337eb0fb9851e8cb140502</anchor>
      <arglist>(Qt::KeyboardModifiers modifiers) const </arglist>
    </member>
    <member kind="function">
      <name>wheelButtonState</name>
      <anchor>253566e1950c2c0cfe69bdb6c42258d6</anchor>
      <arglist>(MouseHandler handler, MouseAction action, bool withConstraint=true) const </arglist>
    </member>
    <member kind="function">
      <name>setMouseBinding</name>
      <anchor>218f2279fd64d5841b5ae2fc67d74274</anchor>
      <arglist>(int state, MouseHandler handler, MouseAction action, bool withConstraint=true)</arglist>
    </member>
    <member kind="function">
      <name>setMouseBinding</name>
      <anchor>3225418600cc93fb968e11ce195bee2d</anchor>
      <arglist>(int state, ClickAction action, bool doubleClick=false, Qt::MouseButtons buttonsBefore=Qt::NoButton)</arglist>
    </member>
    <member kind="function">
      <name>setMouseBindingDescription</name>
      <anchor>2e1e005bde20f65b7269e2dd4e978306</anchor>
      <arglist>(int state, QString description, bool doubleClick=false, Qt::MouseButtons buttonsBefore=Qt::NoButton)</arglist>
    </member>
    <member kind="function">
      <name>setWheelBinding</name>
      <anchor>4198b3b668c342e39752bade04f78c64</anchor>
      <arglist>(Qt::KeyboardModifiers modifiers, MouseHandler handler, MouseAction action, bool withConstraint=true)</arglist>
    </member>
    <member kind="function">
      <name>setHandlerKeyboardModifiers</name>
      <anchor>0f709bcb22461a4e9df78de2821b9c80</anchor>
      <arglist>(MouseHandler handler, Qt::KeyboardModifiers modifiers)</arglist>
    </member>
    <member kind="function">
      <name>stateFileName</name>
      <anchor>62b572fd9b9afecb2ac055a24ca9dfa0</anchor>
      <arglist>() const </arglist>
    </member>
    <member kind="function">
      <name>domElement</name>
      <anchor>48e0e2dd26cd96418c8b889ceabe80f6</anchor>
      <arglist>(const QString &amp;name, QDomDocument &amp;document) const </arglist>
    </member>
    <member kind="function">
      <name>initFromDOMElement</name>
      <anchor>cd13d2ddeca530cb9f26ead47f7d25d3</anchor>
      <arglist>(const QDomElement &amp;element)</arglist>
    </member>
    <member kind="function">
      <name>saveStateToFile</name>
      <anchor>aec9168a5f41b3b7fa4211523535ceaa</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <name>restoreStateFromFile</name>
      <anchor>212f6b7b669463c2151688f629d72a81</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <name>setStateFileName</name>
      <anchor>68737cce3d7301701bca6e4270a5a34e</anchor>
      <arglist>(const QString &amp;name)</arglist>
    </member>
    <member kind="function">
      <name>QGLViewerPool</name>
      <anchor>32d7a506084c50fd97a24f94d9ca4864</anchor>
      <arglist>()</arglist>
    </member>
    <member kind="function">
      <name>QGLViewerIndex</name>
      <anchor>9570ddcbaab08bce6f121e69db4fb903</anchor>
      <arglist>(const QGLViewer *const viewer)</arglist>
    </member>
  </compound>
</tagfile>
