/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include <Process.hpp>

#include <Dir.hpp>
#include <Image.hpp>
#include <Information.hpp>
#include <Misc.hpp>

#include <QDir>

namespace lgx {
namespace process {

namespace {

class CreateTestImages : public GlobalProcess {
public:
  CreateTestImages(const GlobalProcess& process)
    : Process(process)
    , GlobalProcess(process)
  { }

  bool operator()(const ParmList& parms)
  {
    auto pth = parms[0].toString();
    auto dir = (pth.isEmpty() ? util::currentDir() : QDir(util::resolvePath(parms[0].toString())));
    Image5D image;
    image.allocate(Point5u{5,5,5,3,5});
    Information::out << "Image strides = " << image.strides << endl;

    auto total_size = size_t(image.size[0])*image.size[1]*image.size[2]*image.size[3]*image.size[4];
    Information::out << "total_size = " << total_size << endl;

    for(uint k = 0 ; k < total_size ; ++k)
      image[k] = ushort(k*30);

    Information::out << "Max value = " << ushort(total_size-1)*30 << endl;

    if(not saveTIFFImage(dir.absoluteFilePath("tiff5d.tif"), image))
      return setErrorMessage("Error saving image 'tiff5d.tif'. Check terminal for details.");

    auto image3d = image.channel(1).timepoint(1);
    Information::out << "Image 3D strides = " << image3d.strides << endl;
    if(not saveTIFFImage(dir.absoluteFilePath("tiff3d.tif"), image3d))
      return setErrorMessage("Error saving image 'tiff3d.tif'. Check terminal for details.");

    auto image2d = image3d.plane(3);
    for(size_t y = 0 ; y < image2d.size.y() ; ++y) {
      for(size_t x = 0 ; x < image2d.size.x() ; ++x) {
        Information::out << QString::number(image2d[x+image2d.strides.y()*y]).rightJustified(6, ' ');
      }
      Information::out << endl;
    }

    Image5D colored = image.timepoint(0);
    if(not saveImage(dir.absoluteFilePath("multi.png"), colored, "CImg Auto", 2))
      return setErrorMessage("Error saving image 'multi.png'. Check terminal for details.");
    return true;
  }

  // Plug-in folder
  QString folder() const override {
    return "Tests";
  }
  // Plug-in name
  QString name() const override {
    return "Create Test Images";
  }
  // Plug-in long description
  QString description() const override {
    return "Create a series of test images.";
  }
  // List of parameter names
  QStringList parmNames() const override {
    return QStringList() << "Folder";
  }
  // List of parameter long descriptions
  QStringList parmDescs() const override {
    return QStringList() << "Folder containing the images.";
  }
  // List of parameter default values
  ParmList parmDefaults() const override {
    return ParmList() << "";
  }
};

class LoadTestImages : public GlobalProcess {
public:
  LoadTestImages(const GlobalProcess& process)
    : Process(process)
    , GlobalProcess(process)
  { }

  bool operator()(const ParmList& parms)
  {
    auto pth = parms[0].toString();
    auto dir = (pth.isEmpty() ? util::currentDir() : QDir(util::resolvePath(parms[0].toString())));
    Image5D image;

    Information::out << "=== Testing loadImage with 5D TIFF file ===" << endl;
    if(not loadImage(dir.absoluteFilePath("tiff5d.tif"), image, -1, -1))
      return setErrorMessage("Error loading image 'tiff5d.tif'. Check terminal for details.");

    Information::out << "Loading 'tiff5d.tif'" << endl
                     << "    - Size = " << image.size << endl
                     << "    - Strides = " << image.strides << endl;

    auto total_size = image.nbVoxels();
    Information::out << "total_size = " << total_size << endl;

    for(uint k = 0 ; k < total_size ; ++k)
      if(image[k] != ushort(k*30))
        return setErrorMessage(QString("Error loading 'tiff5d.tif' on position %1, value is %2 instead of %3")
                               .arg(k).arg(image[k]).arg(ushort(k*30)));

    Image5D image3d;
    if(not loadImage("tiff3d.tif", image3d))
      return setErrorMessage("Error loading image 'tiff3d.tif'. Check terminal for details.");

    Information::out << "Image 3D strides = " << image3d.strides << endl;

    auto view = image.channel(1).timepoint(1);
    for(uint k = 0 ; k < view.size.x()*view.size.y()*view.size.z() ; ++k)
      if(view[k] != image3d[k])
        return setErrorMessage(QString("Error, 'tiff3d.tif' doesn't match channel 1 timepoint 1 of image 'tiff5d.tif'"));

    image3d.free();
    Information::out << "=== Testing loadSamples with 5D TIFF file ===" << endl;
    if(not loadSamples(dir.absoluteFilePath("tiff5d.tif"), image3d))
      return setErrorMessage("Error, cannot load samples from 'tiff5d.tif'");

    auto expected = Point5u{image.size.x(), image.size.y(), image.size[3]*image.size[4], 1, 1};
    if(image3d.size != expected)
      return setErrorMessage(QString("Error, expected size: %1, real size: %2.").arg(toQString(expected)).arg(toQString(image3d.size)));

    view = image.plane(image.size.z() / 2);
    for(uint t = 0, z = 0 ; t < image.size[4] ; ++t)
      for(uint c = 0 ; c < image.size[3] ; ++c, ++z) {
        ushort* p = &view[t*image.strides[4] + c*image.strides[3]];
        ushort *p3d = &image3d[z*image.strides.z()];

        for(uint k = 0 ; k < image.size.x()*image.size.y() ; ++k) {
          if(p[k] != p3d[k])
            return setErrorMessage(QString("Sample %1 not equal to plane %2 of channel %3, time point %4.")
                                   .arg(z).arg(image.size.z()/2).arg(c).arg(t));
        }
      }

    return true;
  }

  // Plug-in folder
  QString folder() const override {
    return "Tests";
  }
  // Plug-in name
  QString name() const override {
    return "Load Test Images";
  }
  // Plug-in long description
  QString description() const override {
    return "Load and test the images generated by the 'Create Test Images' process.";
  }
  // List of parameter names
  QStringList parmNames() const override {
    return QStringList() << "Folder";
  }
  // List of parameter long descriptions
  QStringList parmDescs() const override {
    return QStringList() << "Folder containing the images.";
  }
  // List of parameter default values
  ParmList parmDefaults() const override {
    return ParmList() << "";
  }
};

REGISTER_GLOBAL_PROCESS(CreateTestImages);
REGISTER_GLOBAL_PROCESS(LoadTestImages);

}

} // namespace lgx
} // namespace process

