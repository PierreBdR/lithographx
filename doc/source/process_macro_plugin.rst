Processes, Macros, Plugins and Packages
=======================================

The extension mechanism of LithoGraphX relies on these four notions:

    Processes
        A process implements an algorithm and is what users will see in the end.
        Within LithoGraphX, each process is an object that can be configured
        with some user interaction and execute the processing without any
        interaction.
    Plugins
        A Plugin is a single file registering one or more processes for use in
        LithoGraphX. Plugins can be enabled or disabled and if more than one
        plugin declares the same process, only the first one will be used.
        Plugins are typically written in C++.
    Macros
        A macro is a particular plugin that uses a macro language instead of
        a compiled one. These are different from standard plugins in that they
        rely on a Macro language being being defined to make the interface
        with the C++.
    Packages
        Packages are a distributable set of plugins and macros. Packages can
        contain the sources or binaries. Source packages need to be compiled,
        while binary packages are restricted to a given version of LithoGraphX
        and OS.



