/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef STORE_HPP
#define STORE_HPP

#include <LGXConfig.hpp>

#include <Geometry.hpp>
#include <Thrust.hpp>
#include <thrust/host_vector.h>
#include <TransferFunction.hpp>

namespace lgx {

typedef util::Vector<2, ushort> Point2us;

namespace process {
class SetupProcess;
} // namespace process

#if !defined(_MSC_VER) || !defined(Q_OS_WIN32) // otherwise, a bug in VC++ prevents using hash maps
typedef unsigned short ushort;
#endif

typedef thrust::host_vector<ushort> HVecUS;

class Stack;
/**
 * \class Store Store.hpp <Store.hpp>
 *
 * The Store class holds the actual 3D data and properties specific to it.
 *
 * \ingroup ProcessUtils
 */
class LGX_EXPORT Store {
  friend class process::SetupProcess;

public:
  /**
   * Create an empty store
   */
  Store(Stack* stack);

  /**
   * Copy the store, and attach it to the same stack
   */
  Store(const Store& copy);

  /**
   * Delete the data attached to the store
   */
  ~Store();

  /**
   * Actual 3D data store linearly in a host vector.
   *
   * The data is always store as an unsigned 16 bits integer in a host vector (i.e. from the thrust library)
   */
  HVecUS& data() {
    return _data;
  }
  /**
   * Actual 3D data store linearly in a host vector.
   *
   * The data is always store as an unsigned 16 bits integer in a host vector (i.e. from the thrust library)
   */
  const HVecUS& data() const {
    return _data;
  }
  /**
   * Returns true if the data is to be interpreted as labels rather than intensities
   */
  bool labels() const {
    return _label;
  }
  /**
   * Change the interpretation of the volume as labels
   */
  void setLabels(bool val) {
    _label = val;
  }
  /**
   * Opacity used to render the volume
   */
  float opacity() const {
    return _opacity;
  }
  /**
   * Changed the opacity of the volume
   */
  void setOpacity(float f)
  {
    if(f < 0)
      _opacity = 0;
    else if(f > 1)
      _opacity = 1;
    else
      _opacity = f;
  }
  /**
   * Global brightness used to render the volume
   */
  float brightness() const {
    return _brightness;
  }
  /**
   * Change the brightness of the volume
   */
  void setBrightness(float f)
  {
    if(f < 0)
      _brightness = 0;
    else if(f > 1)
      _brightness = 1;
    else
      _brightness = f;
  }

  /**
   * Retrieve the transfer function used to render the volume
   */
  TransferFunction transferFct() const {
    return _fct;
  }
  /**
   * Change the transfer function used to render the volume
   */
  void setTransferFct(const TransferFunction& f);

  /**
   * Returns true if the transfer function has been changed within this process
   */
  bool transferFunctionChanged() const {
    return _changed_function;
  }

  /**
   * A process that changes the 3D data needs to call this method.
   *
   * This indicate the GUI that the data changed and need reloading in the graphics card.
   */
  void changed();

  /**
   * A process that changed a range in the 3D data needs to call this method.
   *
   * This indicate the GUI that the data changed in this range and need reloading. Note that many calls to this will
   * assume the union of the bounding boxes have changed.
   */
  void changed(const BoundingBox3i& bbox);

  /**
   * Returns true if the 3D data has been changed during this process.
   */
  bool wasChanged() const {
    return _changed;
  }

  /**
   * Returns the current bounding box for the changes
   */
  const BoundingBox3i& changedBBox() const {
    return _changed;
  }

  /**
   * Ask the user interface to show this store
   */
  void show() {
    _isVisible = true;
  }
  /**
   * Ask the user interface to hide this store
   */
  void hide() {
    _isVisible = false;
  }
  /**
   * Is the store currently visible
   */
  bool isVisible() const {
    return _isVisible;
  }

  /**
   * Returns a constant pointer on the stack holding this store
   */
  const Stack* stack() const {
    return _stack;
  }

  /**
   * Change the stack this store is attached to
   */
  void setStack(Stack* s);

  /**
   * Makes sure the memory for the store is allocated.
   *
   * \note before the process starts, the memory is already allocated. It is useful only if you change the size of the
   * data by hand.
   */
  void allocate();
  /**
   * Reset the memory of the store.
   *
   * In the end, the store has the correct size and is filled with zeros.
   */
  void reset();

  /**
   * Returns the file corresponding to this store.
   */
  const QString& file() const {
    return _filename;
  }
  /**
   * Set which file corresponds to this store.
   */
  void setFile(const QString& f = QString());

  /**
   * Returns the size (in number of elements) of the store
   */
  size_t size() const {
    return _data.size();
  }
  /**
   * True if the store is of size 0
   */
  bool empty() const {
    return _data.empty();
  }

  /**
   * Copy the metadata from another store
   *
   * In the end, the states will be the same (currently labels, file name and transfer function)
   */
  void copyMetaData(const Store* other);

  /**
   * Update the histogram and the bounds on the voxels intensities
   *
   * \param force If true, the histogram will be updated even if the image hasn't been marked as changed.
   */
  void updateHistogram(bool force = false);

  /**
   * Update the bounds of the voxel intensities
   */
  void updateBounds(bool force = false);

  /**
   * Get the current histogram
   */
  const std::vector<uint64_t>& histogram() const
  {
    return _histogram;
  }

  /**
   * Set the histogram.
   *
   * \param h must be a vector with as many values as the current histogram (default: 1024)
   */
  void setHistogram(const std::vector<uint64_t>& h);

  /**
   * Set the histogram.
   *
   * \param h must be a vector with as many values as the current histogram (default: 1024)
   */
  void setHistogram(std::vector<uint64_t>&& h);

  /**
   * Get the bounds on the voxel intensities
   */
  const Point2us& bounds() const
  {
    return _bounds;
  }

  /**
   * Set the bounds for voxel intensities
   *
   * \param b Bounds, the first value must be smaller than the second
   */
  void setBounds(const Point2us& b);

  /**
   * Auto-scale the store using the histogram.
   *
   * \note The histogram will be updated if needed
   */
  void autoScale(double range);

protected:
  void resetModified();

  HVecUS _data;
  bool _label;
  bool _changed_function;
  float _opacity;
  float _brightness;
  BoundingBox3i _changed;
  TransferFunction _fct;
  bool _isVisible;
  QString _filename;
  const Stack* _stack;
  bool _invalidated_histogram;
  std::vector<uint64_t> _histogram;
  Point2us _bounds;
};

void LGX_EXPORT swapMetaData(Store* s1, Store* s2);
} // namespace lgx
#endif // STORE_HPP
