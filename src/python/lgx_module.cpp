/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "LGXPython.hpp"

#include "lgx_module.hpp"

#include "Process.hpp"
#include "Defer.hpp"

#include <structmember.h>
#include <frameobject.h>
#include <sip.h>

#include <stack>
#include <cassert>
#include <memory>

namespace lgx {
namespace process {
namespace python {

SafePyObject::SafePyObject(PyObject *obj)
  : _ptr(obj)
{ }

SafePyObject::SafePyObject(SafePyObject&& other)
  : _ptr(other._ptr)
{
  other._ptr = nullptr;
}

SafePyObject::~SafePyObject()
{
  Py_XDECREF(_ptr);
  _ptr = nullptr;
}

SafePyObject& SafePyObject::operator=(SafePyObject&& other)
{
  Py_XDECREF(_ptr);
  _ptr = other._ptr;
  other._ptr = nullptr;
  return *this;
}

void SafePyObject::acquire() {
  if(_ptr)
    Py_INCREF(_ptr);
}

void SafePyObject::free() {
  Py_XDECREF(_ptr);
  _ptr = nullptr;
}

PyObject* SafePyObject::release() {
  PyObject* value = _ptr;
  _ptr = nullptr;
  return value;
}

QVariant getVariant(PyObject *obj)
{
  // First, check if obj is already a variant
  PyTypeObject* qvariant_typedef = PythonInterpreter::convertSipTypeToPyTypeObject(PythonInterpreter::sipQVariantType());
  int isQVar = PyObject_IsInstance(obj, (PyObject*)qvariant_typedef);
  if(isQVar == 1) {
    int err = 0;
    auto* v = PythonInterpreter::convertToSipType<QVariant>(obj, PythonInterpreter::sipQVariantType(),
                                                            nullptr, SIP_NOT_NONE, nullptr, &err);
    if(err)
      return getString(obj);
    return *v;
  }
  if(PyFloat_Check(obj))
    return PyFloat_AS_DOUBLE(obj);
  if(PyBool_Check(obj))
    return obj == Py_True;
  if(PyLong_Check(obj))
  {
    int overflow;
    qlonglong res = PyLong_AsLongLongAndOverflow(obj, &overflow);
    if(not overflow)
      return res;
  }
#ifndef IS_PY3K
  if(PyInt_Check(obj)) {
    long res = PyInt_AS_LONG(obj);
    return QVariant::fromValue(res);
  }
#endif
  return getString(obj);
}

SafePyObject makePyObject(const QVariant& v)
{
  switch((QMetaType::Type)v.type()) {
    case QMetaType::Bool:
      return (v.value<bool>() ? Py_True : Py_False);
    case QMetaType::Int:
    case QMetaType::Short:
#ifdef IS_PY3K
    case QMetaType::LongLong:
      return PyLong_FromLongLong(v.value<qlonglong>());
#else
      return PyInt_FromLong(v.value<long>());
#endif

    case QMetaType::UInt:
    case QMetaType::UShort:
#ifdef IS_PY3K
    case QMetaType::ULongLong:
      return PyLong_FromUnsignedLongLong(v.value<qulonglong>());
#else
      return PyInt_FromSsize_t(v.value<Py_ssize_t>());
#endif
    case QMetaType::Double:
      return PyFloat_FromDouble(v.value<double>());
    default:
      return makeString(v.toString());
  }
}

SafePyObject makePyVariant(const QVariant& v)
{
  auto* pv = new QVariant(v);
  auto sipobj = PythonInterpreter::convertFromSipType(pv, PythonInterpreter::sipQVariantType(), Py_None);
  SafePyObject siptype = PyObject_Type(sipobj);
  SafePyObject siptype_str = PyObject_Str(siptype);
  return sipobj;
}

QString getString(PyObject* obj)
{
#ifdef IS_PY3K
  if(PyUnicode_Check(obj))
    return QString::fromUtf8(PyUnicode_AsUTF8(obj));
  SafePyObject pystr = PyObject_Str(obj);
  return QString::fromUtf8(PyUnicode_AsUTF8(pystr));
#else
  if(PyString_Check(obj))
    return QString::fromLocal8Bit(PyString_AsString(obj));
  else if(PyUnicode_Check(obj)) {
    SafePyObject pystr = PyUnicode_AsUTF8String(obj);
    if(not pystr) {
      Information::err << "Error, couldn't convert unicode string to UTF-8 string" << endl;
      return QString();
    }
    return QString::fromUtf8(PyString_AsString(pystr));
  } else {
    SafePyObject pystr = PyObject_Str(obj);
    return QString::fromUtf8(PyString_AsString(pystr));
  }
#endif
}

SafePyObject makeString(const QString& s)
{
#ifdef IS_PY3K
  QByteArray ba = s.toUtf8();
  const char err[] = "ignore";
  return PyUnicode_DecodeUTF8(ba.data(), ba.size(), err);
#else
  QByteArray ba = s.toLocal8Bit();
  return PyString_FromString(ba.data());
#endif
}

QString getTraceBack(PyObject* tb, int limit)
{
  QString err;
  PyTracebackObject* tb1 = (PyTracebackObject*)tb;
  int depth = 0;
  while(tb1) {
    depth++;
    tb1 = tb1->tb_next;
  }
  tb1 = (PyTracebackObject*)tb;
  while(tb1 != NULL) {
    if(depth <= limit) {
#ifdef IS_PY3K
      QString fn = QString::fromUtf8(PyUnicode_AsUTF8(tb1->tb_frame->f_code->co_filename));
      QString co = QString::fromUtf8(PyUnicode_AsUTF8(tb1->tb_frame->f_code->co_name));
#else
      const char* fn = PyString_AsString(tb1->tb_frame->f_code->co_filename);
      const char* co = PyString_AsString(tb1->tb_frame->f_code->co_name);
#endif
      int lo = tb1->tb_lineno;
      err += QString("  File \"%1\", line %2, in %3\n").arg(fn).arg(lo).arg(co);
    }
    depth--;
    tb1 = tb1->tb_next;
  }
  return err;
}

QString getError()
{
  PyObject* exc, *val, *tb;
  PyErr_Fetch(&exc, &val, &tb);
  if(!exc)
    return "Unknown error in Python code";
  else {
    // Now, get the exception string
    QString err;
    PyErr_NormalizeException(&exc, &val, &tb);
    if(tb and tb != Py_None) {
      err += "Traceback (most recent call last):\n";
      err += getTraceBack(tb, 10);
    }
    SafePyObject str = PyObject_Str(val);
    if(str) {
      SafePyObject exc_str = PyObject_GetAttrString(exc, "__name__");
      QString s_exc = python::getString(exc_str);
      QString s_val = python::getString(str);
      QString msg = err + QString("%1: %2").arg(s_exc).arg(s_val);
      return msg;
    } else
      return "Unknown exception in Python code";
  }
}

std::stack<Process*> active_process;

ProcessRunner::ProcessRunner()
{ }

ProcessRunner::ProcessRunner(const QString& type, const QString& name)
  : _type(type)
  , _name(name)
{ }

ProcessRunner::~ProcessRunner()
{ }

bool ProcessRunner::operator()(const ParmList& args)
{
  if(active_process.empty())
    throw ProcessError("No active process.");
  Process *base_proc = active_process.top();
  auto new_proc = std::unique_ptr<Process>(base_proc->makeProcess(_type, _name));
  try {
    if(not (*new_proc)(args)) {
      throw ProcessError(new_proc->errorMessage());
    }
  } catch(std::string s) {
    throw ProcessError(s.data());
  } catch(QString s) {
    throw ProcessError(s.toUtf8().data());
  } catch(std::exception&) {
    throw;
  } catch(...) {
    throw ProcessError("Unknown C++ exception.");
  }
  return true;
}

QString ProcessRunner::errorMessage()
{
  if(active_process.empty())
    return "No active process";
  return active_process.top()->errorMessage();
}

void ProcessRunner::registerBaseProcess(Process *proc)
{
  active_process.push(proc);
}

void ProcessRunner::popBaseProcess()
{
  assert(not active_process.empty());
  active_process.pop();
}

ProcessMaker::ProcessMaker()
{ }

ProcessMaker::ProcessMaker(const QString& type)
  : _type(type)
{ }

ProcessMaker::~ProcessMaker() { }

QStringList ProcessMaker::processes()
{
  auto result = listProcesses(_type);
  result.replaceInStrings(" ", "_").sort(Qt::CaseInsensitive);
  return result;
}

ProcessRunner ProcessMaker::__getattr__(const QString& name) throw (lgx::process::NoSuchProcess)
{
  QString real_name = name;
  real_name.replace("_", " ");
  auto* proc = getBaseProcessDefinition(_type, real_name);
  if(not proc)
    throw NoSuchProcess(_type, real_name);
  return ProcessRunner(_type, real_name);
}

PyObject* mapQWidgetToSelf(QWidget* w)
{
  return PythonInterpreter::convertFromSipType(w, PythonInterpreter::sipQWidgetType(), nullptr);
}

SafePy<PyListObject> pyParmList(const ParmList& parms)
{
    auto list = SafePy<PyListObject>(PyList_New(parms.size()));
    if(not list) {
      auto error = QString("Couldn't create the python list to hold %1 parameters").arg(parms.size());
      PyErr_SetString(PyExc_RuntimeError, error.toUtf8().data());
      return 0;
    }
    for(size_t i = 0 ; i < parms.size() ; ++i) {
      auto v = parms[i];
      auto pyObject = makePyObject(v);
      if(not pyObject) {
        auto error = QString("Couldn't create object for item %1 of the list.").arg(i);
        PyErr_SetString(PyExc_ValueError, error.toUtf8().data());
        return 0;
      }
      PyList_SET_ITEM(list, i, pyObject.release());
    }

    return list.release();
}

} // namespace python
} // namespace process
} // namespace lgx
