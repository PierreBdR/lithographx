/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "ITKSink.hpp"
#include <itkVTKImageIO.h>

namespace lgx {
namespace process {

namespace {

template <size_t dim>
bool itkVTKWriter(const Store* input, QString filename, Process* process)
{
  auto converter = UImageConverter<dim>::New();

  converter->SetStore(input);

  converter->Update();

  auto img = converter->GetOutput();

  typedef itk::ImageFileWriter<UImageType<dim>> WriterType;

  auto filter = WriterType::New();

  QByteArray ba = filename.toLocal8Bit();
  filter->SetFileName(ba.data());

  itk::VTKImageIO::Pointer ImageIO = itk::VTKImageIO::New();

  filter->SetImageIO(ImageIO);

  filter->SetInput(img);

  filter->Write();

  return true;
}

} // namespace

bool ITKVTKWriter::operator()(const Store* input, QString filename)
{
  const Stack* stack = input->stack();
  if(stack->is2D())
    return itkVTKWriter<2>(input, filename, this);
  return itkVTKWriter<3>(input, filename, this);
}

REGISTER_STACK_PROCESS(ITKVTKWriter);
}
}
