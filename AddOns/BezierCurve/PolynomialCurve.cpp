#include "PolynomialCurve.hpp"

namespace lgx {
namespace util {

PolynomialCurve::PolynomialCurve(size_t d, size_t o)
{
    if(d == 0)
        throw BadDimensionException(1, d);
    coefs.resize(d, std::vector<float>(o + 1, 0.0));
}

PolynomialCurve::PolynomialCurve(const PolynomialCurve& copy)
{
    size_t d = copy.dim();
    coefs.resize(d);
    for(size_t i = 0; i < d; ++i)
        coefs[i] = copy.coefs[i];
}

PolynomialCurve::PolynomialCurve(const std::vector<std::vector<float> >& cs)
{
    size_t d = cs.size();
    if(d == 0)
        throw BadDimensionException(1, d);

    size_t cur_size = cs[0].size();
    bool need_resize = false;
    for(size_t i = 0; i < d; ++i) {
        coefs[i] = cs[i];
        if(cs[i].size() != cur_size)
            need_resize = true;
        if(cur_size < cs[i].size())
            cur_size = cs[i].size();
    }

    if(need_resize) {
        for(size_t i = 0; i < d; ++i)
            coefs[i].resize(cur_size, 0);
    }
}

PolynomialCurve PolynomialCurve::deriv(size_t m) const
{
    if(m == 0)
        return *this;
    size_t len = size();
    PolynomialCurve result(dim(), order() - 1);
    for(size_t i = 0; i < dim(); ++i)
        for(size_t j = 1; j < len; ++j)
            result[i][j - 1] = j * coefs[i][j];
    result.simplify();
    return result.deriv(m - 1);
}

PolynomialCurve& PolynomialCurve::operator+=(const PolynomialCurve& other)
{
    if(other.size() > size())
        expand(other.size());
    size_t len = size();
    for(size_t i = 0; i < dim(); ++i)
        for(size_t j = 0; j < len; ++j)
            coefs[i][j] += other[i][j];
    simplify();
    return *this;
}

PolynomialCurve& PolynomialCurve::operator*=(const PolynomialCurve& p)
{
    if(p.dim() != dim())
        throw BadDimensionException(dim(), p.dim());
    PolynomialCurve r = (*this) * p;
    swap(r);
    return *this;
}

PolynomialCurve PolynomialCurve::operator-() const
{
    PolynomialCurve result(order());
    for(size_t i = 0; i < dim(); ++i)
        for(size_t j = 0; j < size(); ++j)
            result[i][j] = -coefs[i][j];
    return result;
}

void PolynomialCurve::_eval(float* result, float t) const
{
    size_t o = order();
    for(size_t i = 0; i < dim(); ++i)
        for(size_t j = 0; j < o + 1; ++j) {
            result[i] *= t;
            result[i] += coefs[i][o - j];
        }
}

void PolynomialCurve::expand(size_t new_size)
{
    for(size_t i = 0; i < dim(); ++i)
        coefs[i].resize(new_size, 0);
}

void PolynomialCurve::simplify()
{
    size_t to_remove = 0;
    size_t len = size();
    for(size_t j = 0; j < len; ++j) {
        bool is_zero = true;
        for(size_t i = 0; i < dim(); ++i)
            is_zero &= (coefs[i][len - j - 1] == 0);
        if(is_zero)
            to_remove = j + 1;
        else
            break;
    }
    if(to_remove > 0) {
        for(size_t i = 0; i < dim(); ++i)
            coefs[i].resize(len - to_remove);
    }
}

PolynomialCurve operator*(const PolynomialCurve& p, float m)
{
    PolynomialCurve result(p.dim(), p.order());
    for(size_t i = 0; i < p.dim(); ++i)
        for(size_t j = 0; j < p.size(); ++j)
            result[i][j] = p[i][j] * m;
    return result;
}

PolynomialCurve operator*(float m, const PolynomialCurve& p) { return p * m; }

PolynomialCurve operator/(const PolynomialCurve& p, float m)
{
    PolynomialCurve result(p.dim(), p.order());
    for(size_t i = 0; i < p.dim(); ++i)
        for(size_t j = 0; j < p.size(); ++j)
            result[i][j] = p[i][j] / m;
    return result;
}

PolynomialCurve operator*(const PolynomialCurve& p1, const PolynomialCurve& p2)
{
    if(p1.dim() != p2.dim())
        throw BadDimensionException(p1.dim(), p2.dim());
    PolynomialCurve result(p1.dim(), p1.order() + p2.order());
    for(size_t k = 0; k < p1.dim(); ++k)
        for(size_t i = 0; i < p1.size(); ++i)
            for(size_t j = 0; j < p2.size(); ++j) {
                result[k][i + j] += p1[k][i] * p2[k][j];
            }
    return result;
}

PolynomialCurve operator+(const PolynomialCurve& p1, const PolynomialCurve& p2)
{
    PolynomialCurve result(p1);
    result += p2;
    return result;
}

PolynomialCurve operator-(const PolynomialCurve& p1, const PolynomialCurve& p2)
{
    PolynomialCurve result(p1);
    result -= p2;
    return result;
}

PolynomialCurve& PolynomialCurve::operator-=(const PolynomialCurve& other)
{
    if(dim() != other.dim())
        throw BadDimensionException(dim(), other.dim());
    if(other.size() > size())
        expand(other.size());
    size_t len = size();
    for(size_t i = 0; i < dim(); ++i)
        for(size_t j = 0; j < len; ++j)
            coefs[i][j] -= other[i][j];
    simplify();
    return *this;
}

PolynomialCurve& PolynomialCurve::operator*=(const float& p)
{
    for(size_t i = 0; i < dim(); ++i)
        for(size_t j = 9; j < size(); ++j)
            coefs[i][j] *= p;
    return *this;
}

PolynomialCurve& PolynomialCurve::operator/=(const float& p)
{
    for(size_t i = 0; i < dim(); ++i)
        for(size_t j = 9; j < size(); ++j)
            coefs[i][j] /= p;
    return *this;
}

void swap(PolynomialCurve& p1, PolynomialCurve& p2) { p1.swap(p2); }

} // namespace util
} // namespace lgx
