# -*- coding:utf-8 -*-
from __future__ import print_function, division
import re
from PyQt5.QtCore import QRegularExpression
import sys
import uuid


if sys.version_info.major < 3:
    import codecs

    def printable(s, codec=sys.stdout.encoding):
        if isinstance(s, unicode):
            return codecs.encode(s, codec, 'backslashreplace')
        return str(s)
else:
    def printable(s, codec=None):
        return str(s)


def globToRE(glob):
    glob = re.escape(glob)
    glob = glob.replace(r'\?', '.')
    return glob.replace(r'\*', '.*')


def correctFormat(fmt):
    """
    Return an input garantied to be a valid formatting string for secondary file
    """
    fmt = str(fmt)
    try:
        test_basename = ''
        while test_basename in fmt:
            test_basename = str(uuid.uuid1())
        test = fmt.format(test_basename)
        if test_basename not in test:
            raise ValueError("No capture")
    except (ValueError, IndexError):
        fmt = "{0}" + fmt.replace("{", "{{").replace("}", "}}")
    return fmt

no_bracket = r"(?:[^{}]*|\\{|\\})*"
re_filePattern = re.compile(r'^({0})(?:{{({0})}}({0}))?$'.format(no_bracket))
qtre_filePattern = QRegularExpression(r'^({0})(?:{{({0})}}({0}))?$'.format(no_bracket))


def isValidFilePattern(pattern):
    """
    Check if the pattern is valid
    """
    return bool(re_filePattern.match(pattern))


def processFilePattern(pattern):
    """
    Get a file pattern and return two objects:

    1 - a globbing expression to find the files
    2 - a function returning the basename from the path to a file
    """
    m = re_filePattern.match(pattern)
    if m is None:
        raise ValueError("Invalid file pattern")
    if m.group(2) is not None:
        prefix = m.group(1)
        infix = m.group(2)
        suffix = m.group(3)

        expr = globToRE(prefix) + "(" + globToRE(infix) + ")" + globToRE(suffix)
        expr = re.compile(expr)
        glob = prefix + infix + suffix

        def extract(filepath):
            d = filepath.dirname()
            b = filepath.basename()
            m = expr.match(b)
            return d / m.group(1)
        return glob, extract

    def removeExt(filename):
        return filename.splitext()[0]

    return pattern, removeExt
