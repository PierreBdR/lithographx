#ifndef ATTRIBUTES_HPP
#define ATTRIBUTES_HPP

#include <LGXConfig.hpp>

#include <Vector.hpp>
#include <Matrix.hpp>
#include <MemberIterator.hpp>
#include <Information.hpp>
#include <Utility.hpp>
#include <Geometry.hpp>

#include <QString>

#include <stdint.h>
#include <exception>
#include <limits>
#include <type_traits>
#include <utility>
#include <typeinfo>
#include <unordered_map>

namespace lgx {
namespace util {

template <typename T>
struct attribute_traits
{
  typedef T value_type;
  typedef T element_type;
};

// Remove consts
template <typename T>
struct attribute_traits<const T>
{
  typedef typename attribute_traits<T>::value_type value_type;
  typedef const value_type element_type;
};

template <size_t N, typename T>
struct attribute_traits<Vector<N,T> >
{
  typedef T value_type;
  typedef T element_type;
};

template <size_t N, size_t M, typename T>
struct attribute_traits<Matrix<N,M,T> >
{
  typedef T value_type;
  typedef T element_type;
};

class PrivateAttrArray;
class AttributesSet;


class AttrBounds;

/**
 * Base array that manages an array or known-size tuples
 *
 * You should not use this outside attributes
 */
class AttrArray
{
  friend class AttributesSet;
public:
  typedef size_t size_type;
  typedef char value_type; // Char has to be 1 byte!
  typedef char* pointer;
  typedef const char* const_pointer;
  typedef char& reference;
  typedef const char& const_reference;
  typedef char* iterator;
  typedef const char* const_iterator;
  typedef ptrdiff_t difference_type;

  /**
   * Returns the maximum possible size of an array
   */
  static inline size_type max_size()
  {
    return std::numeric_limits<size_type>::max();
  }

  /**
   * Create a new empty array
   *
   * If element_size is 0, it leads to an invalid array, that you should not try to use!
   */
  AttrArray(size_type element_size = 0);

  /**
   * Create a new empty array
   */
  AttrArray(size_type count,
            size_type element_size);

  /**
   * Creates a shallow copy of the attribute
   */
  AttrArray(const AttrArray& copy);

  /**
   * Move an array to another one
   */
  AttrArray(AttrArray&& other);

  /**
   * Returns a full copy of the current array
   */
  AttrArray copy() const;

  /**
   * Returns the size, in bytes, of one element
   */
  size_type element_size() const { return _properties->element_size; }

  /**
   * True if the array has no element
   */
  bool empty() const;

  /**
   * True if the array is valid (i.e. the element size is not 0)
   */
  virtual bool valid() const { return _properties->element_size != 0; }

  /**
   * Shallow copy of the data
   */
  AttrArray& operator=(const AttrArray& copy);

  /**
   * Move assignment operator
   */
  AttrArray& operator=(AttrArray&& copy);

  /**
   * Destroy the array
   */
  virtual ~AttrArray();

  /**
   * Copy the content of other in this. If the element size are incompatible, fails and returns false.
   */
  bool set(const AttrArray& other);

  /**
   * Get a pointer to the last value
   */
  const_pointer back() const { return data() + (_properties->element_size * (_properties->count-1)); }
  /**
   * Get a pointer to the last value
   */
  pointer back() { return data() + (_properties->element_size * (_properties->count-1)); }

  /**
   * Get a pointer to the last value
   */
  const_pointer front() const { return data(); }
  /**
   * Get a pointer to the last value
   */
  pointer front() { return data(); }

  /**
   * Get a raw pointer on the data
   */
  pointer data() { return _properties->_data.get(); }
  /**
   * Get a raw pointer on the data
   */
  const_pointer data() const { return _properties->_data.get(); }

  /**
   * Iterator on the bytes
   */
  iterator begin() { return data(); }

  /**
   * Iterator on the bytes
   */
  iterator end() { return data() + (_properties->element_size*_properties->count); }

  /**
   * Iterator on the bytes
   */
  const_iterator begin() const { return data(); }

  /**
   * Iterator on the bytes
   */
  const_iterator end() const { return data() + (_properties->element_size*_properties->count); }

  /**
   * Return a pointer to the ith element
   */
  pointer operator[](size_type i) { return data() + i*_properties->element_size; }
  /**
   * Return a pointer to the ith element
   */
  const_pointer operator[](size_type i) const { return data() + i*_properties->element_size; }

  /**
   * Return the number of elements in the array
   */
  size_type size() const { return _properties->count; }
  /**
   * Return the number of elements allocated for this array
   */
  size_type capacity() const { return _properties->reserved; }

  /**
   * Return the size in byte of the array
   */
  size_type byteSize() const { return size() * _properties->element_size; }

  /**
   * Indicate the array has been modified
   */
  void modified();

  /**
   * Tell if the array has been modified
   */
  bool wasModified();

  /**
   * Return the bounds for the array
   */
  AttrBounds bounds() const;

  /**
   * Change the bounds of the array
   * \returns true if the size of the bounds was compatible with this array
   */
  bool setBounds(const AttrBounds& bounds);

  pointer min() { return _properties->_min.get(); }
  pointer max() { return _properties->_max.get(); }
  const_pointer min() const { return _properties->_min.get(); }
  const_pointer max() const { return _properties->_max.get(); }

protected:
  /**
   * Make sure that much is allocated.
   *
   * This will never shrink the allocation.
   */
  void reserve(size_type size);
  /**
   * Make sure that exactly that much memory is allocated.
   *
   * If size is smaller than the currently allocated memory, then it will reallocate.
   */
  void resize(size_type size);
  /**
   * Add a value at the end.
   */
  pointer push_back(size_type n = 1);
  /**
   * Remove the last value from the array
   */
  void pop_back(size_type n = 1);

  /**
   * Reset the modified state
   */
  void resetModified();

private:

  /**
   * Make sure this amount is the memory allocated.
   *
   * It MUST be greater than count (and it's not tested!)
   */
  void reserve_(size_type size);

  /**
   * Method for the initial allocation
   */
  void allocate(size_type count, size_type element_size);

  /**
   * Release the current array
   */
  void release();

  /**
   * Increase reference counting
   */
  void acquire();

  typedef std::unique_ptr<value_type[]> array_pointer;

  struct properties_t
  {
    properties_t(size_type cnt, size_type es)
      : count(cnt)
      , element_size(es)
      , reserved((cnt == 0 ? 1 : cnt))
      , modified(false)
      , _data(nullptr)
      , _min(nullptr)
      , _max(nullptr)
      , reference_count(1)
    { }

    size_type count;
    size_type element_size;
    size_type reserved;
    bool modified;
    array_pointer _data;
    array_pointer _min;
    array_pointer _max;
    size_type reference_count;
  } *_properties;

};

inline AttrArray::iterator begin(AttrArray& arr) { return arr.begin(); }
inline AttrArray::iterator end(AttrArray& arr) { return arr.end(); }

inline AttrArray::const_iterator begin(const AttrArray& arr) { return arr.begin(); }
inline AttrArray::const_iterator end(const AttrArray& arr) { return arr.end(); }

class AttrBounds
{
public:
  typedef AttrArray::size_type size_type;
  typedef AttrArray::value_type value_type;
  typedef AttrArray::pointer pointer;

  AttrBounds(size_type element_size);

  AttrBounds(size_type element_size,
             pointer min,
             pointer max);

  AttrBounds(const AttrBounds& copy);

  AttrBounds(AttrBounds&& copy)
    : _element_size(copy._element_size)
    , _min(std::move(copy._min))
    , _max(std::move(copy._max))
  { }

  void swap(AttrBounds& other)
  {
    std::swap(_element_size, other._element_size);
    std::swap(_min, other._min);
    std::swap(_max, other._max);
  }

  pointer min() const { return _min.get(); }
  pointer max() const { return _max.get(); }
  size_type element_size() const { return _element_size; }

private:
  typedef std::unique_ptr<value_type[]> array_type;
  size_type _element_size;
  array_type _min, _max;
};

struct bad_array_element_size : public std::exception
{
  const char* what() const throw()
  {
    return "Error, trying to work with arrays of incompatible element size.";
  }
};

struct bad_array_type_size : public std::exception
{
  const char* what() const throw()
  {
    return "Error, trying to create a TupleArray (or TupleBounds) from an AttrArray (resp. AttrBounds) of wrong element size";
  }
};

struct bad_array_tuple_size : public std::exception
{
  const char* what() const throw()
  {
    return "Error, trying to use tuple-based object of the wrong size";
  }
};

template <typename T>
class TupleBounds : public AttrBounds
{
public:
  typedef std::numeric_limits<T> limits;
  typedef AttrArray::size_type size_type;

  typedef T value_type;
  typedef T* pointer;
  typedef T& reference;

  TupleBounds(size_type ts)
    : AttrBounds(ts*sizeof(T))
    , tuple_size(ts)
  {
    reset();
  }

  TupleBounds(const AttrBounds& b)
    : AttrBounds(b)
    , tuple_size(b.element_size() / sizeof(T))
  {
    if(tuple_size*sizeof(T) != b.element_size())
      throw bad_array_type_size();
  }

  TupleBounds(const TupleBounds& copy)
    : AttrBounds(copy)
    , tuple_size(copy.tuple_size)
  { }

  TupleBounds(TupleBounds&& other)
    : AttrBounds(std::move(other))
    , tuple_size(other.tuple_size)
  { }

  template <typename T1>
  TupleBounds(const TupleBounds<T1>& copy)
    : AttrBounds(copy.tupleSize()*sizeof(T))
    , tuple_size(copy.tupleSize())
  {
    for(size_type i = 0 ; i < tuple_size ; ++i)
    {
      min(i) = (T)copy.min(i);
      max(i) = (T)copy.max(i);
    }
  }

  /**
   * Add a point to the tuple
   */
  TupleBounds& operator|=(pointer p)
  {
    for(size_type i = 0 ; i < tuple_size ; ++i)
    {
      if(p[i] < min(i)) min(i) = p[i];
      if(p[i] > max(i)) max(i) = p[i];
    }
    return *this;
  }

  /**
   * Union between two tuple bounds
   */
  template <typename T1>
  TupleBounds& operator|=(const TupleBounds<T1>& other)
  {
    if(other.tupleSize() != tupleSize()) throw bad_array_tuple_size();
    for(size_type i = 0 ; i < tuple_size ; ++i)
    {
      if(other.min(i) < min(i)) min(i) = other.min(i);
      if(other.max(i) > max(i)) max(i) = other.max(i);
    }
    return *this;
  }

  /**
   * Intersection between two tuple bounds
   */
  template <typename T1>
  TupleBounds& operator&=(const TupleBounds<T1>& other)
  {
    if(other.tupleSize() != tupleSize()) throw bad_array_tuple_size();
    for(size_type i = 0 ; i < tuple_size ; ++i)
    {
      if(other.min(i) > min(i)) min(i) = other.min(i);
      if(other.max(i) < max(i)) max(i) = other.max(i);
    }
    return *this;
  }

  template <typename T1>
  friend auto operator|(const TupleBounds& b1, const TupleBounds<T1>& b2) -> TupleBounds<decltype(T()+T1())>
  {
    typedef decltype(T()+T1()) result_type;
    typedef TupleBounds<result_type> result_t;
    result_t result(b1);
    result |= b2;
    return result;
  }

  template <typename T1>
  friend auto operator&(const TupleBounds& b1, const TupleBounds<T1>& b2) -> TupleBounds<decltype(T()+T1())>
  {
    typedef decltype(T()+T1()) result_type;
    typedef TupleBounds<result_type> result_t;
    result_t result(b1);
    result &= b2;
    return result;
  }

  void reset(size_type i)
  {
    min()[i] = limits::max();
    max()[i] = limits::lowest();
  }

  void reset()
  {
    for(size_type i = 0 ; i < tuple_size ; ++i)
      reset(i);
  }

  pointer min() const { return reinterpret_cast<pointer>(AttrBounds::min()); }
  pointer max() const { return reinterpret_cast<pointer>(AttrBounds::max()); }
  reference min(size_type i) const { return min()[i]; }
  reference max(size_type i) const { return max()[i]; }

  size_type tupleSize() const { return tuple_size; }

private:
  size_type tuple_size;
};

/**
 * \class tuple_iterator attributes.hpp <attributes.hpp>
 *
 * Iterator on a TupleArray.
 *
 * \note As tuple arrays are always on simple types, there is no operator->() method.
 */
template <typename T>
struct tuple_iterator
{
  typedef AttrArray::size_type size_type;
  typedef T value_type;
  typedef T* pointer;
  typedef T* reference;
  typedef AttrArray::difference_type difference_type;
  typedef std::random_access_iterator_tag iterator_category;

  tuple_iterator(size_type ts = size_type())
    : tuple_size(ts)
  { }

  tuple_iterator(T* p, size_type ts)
    : ptr(p)
    , tuple_size(ts)
  { }

  tuple_iterator(const tuple_iterator&) = default;
  tuple_iterator(tuple_iterator&&) = default;

  tuple_iterator& operator=(const tuple_iterator&) = default;
  tuple_iterator& operator=(tuple_iterator&&) = default;

  size_type tupleSize() const { return tuple_size; }

  //@{
  ///\name Forward iterators requirements

  bool operator==(const tuple_iterator& other) const
  {
    return ptr == other.ptr;
  }

  bool operator!=(const tuple_iterator& other) const
  {
    return ptr != other.ptr;
  }

  reference operator*() const { return ptr; }

  tuple_iterator& operator++() { ptr += tuple_size; return *this; }
  tuple_iterator operator++(int) { tuple_iterator copy(*this); ++(*this); return copy; }
  //@}

  //@{
  ///\name Bidirectional iterators requirements

  tuple_iterator& operator--() { ptr -= tuple_size; return *this; }
  tuple_iterator operator--(int) { tuple_iterator copy(*this); --(*this); return copy; }
  //@}

  //@{
  ///\name Random access iterators requirements

  tuple_iterator& operator+=(difference_type d) { ptr += d*tuple_size; return *this; }
  tuple_iterator& operator-=(difference_type d) { ptr -= d*tuple_size; return *this; }

  difference_type operator-(const tuple_iterator& other) const
  {
    difference_type diff = other.ptr - ptr;
    return diff / tuple_size;
  }

  reference operator[](difference_type d) const { return ptr + d*tuple_size; }
  bool operator>(const tuple_iterator& other) const { return ptr > other.ptr; }
  bool operator<(const tuple_iterator& other) const { return ptr < other.ptr; }
  bool operator>=(const tuple_iterator& other) const { return ptr >= other.ptr; }
  bool operator<=(const tuple_iterator& other) const { return ptr <= other.ptr; }
  //@}

private:
  T* ptr;
  difference_type tuple_size;
};

template <typename T>
tuple_iterator<T> operator-(tuple_iterator<T> it, typename tuple_iterator<T>::difference_type d)
{
  it -= d;
  return it;
}

template <typename T>
tuple_iterator<T> operator+(tuple_iterator<T> it, typename tuple_iterator<T>::difference_type d)
{
  it += d;
  return it;
}

template <typename T>
tuple_iterator<T> operator+(typename tuple_iterator<T>::difference_type d, tuple_iterator<T> it)
{
  it += d;
  return it;
}

template <typename T>
struct TupleArray : public AttrArray
{
  friend class AttributesSet;

  typedef AttrArray base;

  typedef T value_type;

  typedef const T const_value_type;
  typedef typename std::remove_const<T>::type variable_value_type;

  typedef TupleArray<const_value_type> const_array;
  typedef TupleArray<variable_value_type> variable_array;

  typedef T& reference;
  typedef const T& const_reference;

  typedef T* pointer;
  typedef const T* const_pointer;

  typedef tuple_iterator<T> iterator;
  typedef tuple_iterator<const T> const_iterator;

  typedef T& element_reference;
  typedef const T& const_element_reference;

  typedef base::size_type size_type;
  typedef base::difference_type difference_type;

  /**
   * Create an empty typed array
   */
  TupleArray(size_type tuple_size = 0)
    : AttrArray(sizeof(T)*tuple_size)
    , _tuple_size(tuple_size)
  { }

  /**
   * Create an empty typed array
   */
  TupleArray(size_type count, size_type tuple_size)
    : AttrArray(count, sizeof(T)*tuple_size)
    , _tuple_size(tuple_size)
  { }

  /**
   * Create a typed array, from a non-type one.
   * \param ts number of elements in the tuple
   * \param array Base-array
   */
  explicit TupleArray(const AttrArray& array)
    : AttrArray(array)
    , _tuple_size(array.element_size()/sizeof(T))
  {
    assert(array.element_size()/sizeof(T) == _tuple_size);
    if(array.element_size() != sizeof(T)*_tuple_size)
    {
      Information::out << "Pb, _tuple_size = " << _tuple_size << endl;
      Information::out << "    sizeof(T) = " << sizeof(T) << endl;
      Information::out << "    typeif(T).name() = " << typeid(T).name() << endl;
      Information::out << "    element_size = " << array.element_size() << endl;
      throw bad_array_type_size();
    }
  }

  /**
   * Create a shallow copy of the other array
   */
  TupleArray(const TupleArray<const_value_type>& array)
    : AttrArray(array)
    , _tuple_size(array._tuple_size)
  {
    static_assert(std::is_const<T>::value, "You cannot copy from a constant array to a non-constant one.");
  }

  /**
   * Create a shallow copy of the other array
   */
  TupleArray(const TupleArray<variable_value_type>& array)
    : AttrArray(array)
    , _tuple_size(array._tuple_size)
  { }

  TupleArray(TupleArray<const_value_type>&& array)
    : AttrArray(std::move(array))
    , _tuple_size(array._tuple_size)
  {
    static_assert(std::is_const<T>::value, "You cannot copy from a constant array to a non-constant one.");
  }

  TupleArray(TupleArray<variable_value_type>&& array)
    : AttrArray(std::move(array))
    , _tuple_size(array._tuple_size)
  {
  }

  /**
   * Shallow copy of array
   */
  TupleArray& operator=(const TupleArray<const_value_type>& copy)
  {
    static_assert(std::is_const<T>::value, "You cannot copy from a constant array to a non-constant one.");
    AttrArray::operator=(copy);
    _tuple_size = copy._tuple_size;
    return *this;
  }

  /**
   * Shallow copy of array
   */
  TupleArray& operator=(const TupleArray<variable_value_type>& copy)
  {
    AttrArray::operator=(copy);
    _tuple_size = copy._tuple_size;
    return *this;
  }

  /**
   * Shallow copy of array
   */
  TupleArray& operator=(TupleArray<const_value_type>&& copy)
  {
    static_assert(std::is_const<T>::value, "You cannot copy from a constant array to a non-constant one.");
    AttrArray::operator=(std::move(copy));
    _tuple_size = copy._tuple_size;
    return *this;
  }

  /**
   * Shallow copy of array
   */
  TupleArray& operator=(TupleArray<variable_value_type>&& copy)
  {
    AttrArray::operator=(std::move(copy));
    _tuple_size = copy._tuple_size;
    return *this;
  }

  /**
   * Get a pointer to the last tuple of the array
   */
  const_pointer back() const { return reinterpret_cast<const_pointer>(AttrArray::back()); }
  /**
   * Get a pointer to the last tuple of the array
   */
  pointer back() { return reinterpret_cast<pointer>(AttrArray::back()); }

  /**
   * Get a pointer to the first tuple of the array
   */
  const_pointer front() const { return reinterpret_cast<const_pointer>(AttrArray::front()); }
  /**
   * Get a pointer to the first tuple of the array
   */
  pointer front() { return reinterpret_cast<pointer>(AttrArray::front()); }

  /**
   * Get a pointer to the first tuple of the array
   */
  const_pointer data() const { return reinterpret_cast<const_pointer>(AttrArray::data()); }
  /**
   * Get a pointer to the first tuple of the array
   */
  pointer data() { return reinterpret_cast<pointer>(AttrArray::data()); }

  /**
   * Iterator on the tuples
   */
  iterator begin() { return iterator(data(), _tuple_size); }
  /**
   * Iterator on the tuples
   */
  const_iterator begin() const { return const_iterator(data(), _tuple_size); }
  /**
   * Iterator on the tuples
   */
  const_iterator cbegin() const { return const_iterator(data(), _tuple_size); }

  /**
   * Iterator on the tuples
   */
  iterator end() { return iterator(data()+size(), _tuple_size); }
  /**
   * Iterator on the tuples
   */
  const_iterator end() const { return const_iterator(data()+size(), _tuple_size); }
  /**
   * Iterator on the tuples
   */
  const_iterator cend() const { return const_iterator(data()+size(), _tuple_size); }

  /**
   * Return a pointer on the beginning of a tuple
   */
  pointer operator[](size_type i) { return begin()[i]; }
  /**
   * Return a pointer on the beginning of a tuple
   */
  const_pointer operator[](size_type i) const { return begin()[i]; }

  /**
   * Return an element in a tuple
   */
  element_reference operator()(size_type row, size_type col)
  {
    return (*this)[row][col];
  }

  /**
   * Return an element in a tuple
   */
  const_element_reference operator()(size_type row, size_type col) const
  {
    return (*this)[row][col];
  }

  size_type tupleSize() const { return _tuple_size; }

  pointer min() { return reinterpret_cast<pointer>(base::min()); }
  pointer max() { return reinterpret_cast<pointer>(base::max()); }
  const_pointer min() const { return reinterpret_cast<const_pointer>(base::min()); }
  const_pointer max() const { return reinterpret_cast<const_pointer>(base::max()); }

  reference min(size_type i) { return min()[i]; }
  reference max(size_type i) { return max()[i]; }
  const_reference min(size_type i) const { return min()[i]; }
  const_reference max(size_type i) const { return max()[i]; }

  /**
   * Return the bounds for the array
   */
  TupleBounds<T> bounds() const
  {
    return TupleBounds<T>(base::bounds());
  }

  /**
   * Change the bounds of the array
   * \returns true if the size of the bounds was compatible with this array
   */
  bool setBounds(const TupleBounds<T>& bounds)
  {
    return base::setBounds(bounds);
  }

  /**
   * Re-calculate the bounds for this array
   */
  void updateBounds()
  {
    TupleBounds<T> bounds(_tuple_size);
    for(size_type i = 0 ; i < size() ; ++i)
      bounds |= (*this)[i];
    setBounds(bounds);
  }


  protected:

  /**
   * Set the underlying array
   *
   * \returns true on success
   */
  bool set(size_type tuple_size,
           AttrArray& array)
  {
    if(array.element_size() != sizeof(T)*tuple_size)
      return false;
    _tuple_size = tuple_size;
    AttrArray::set(array);
  }

  /**
   * Add elements at the end of the array
   */
  void push_back(const T& value, size_type n = 1)
  {
    pointer ptr = reinterpret_cast<pointer>(push_back(n));
    for(size_t i = 0 ; i < _tuple_size*n ; ++i)
      ptr[i] = value;
  }

  private:
  size_type _tuple_size;
};

template <typename T>
AttrArray::iterator begin(AttrArray& arr)
{
  return arr.begin();
}
template <typename T>
AttrArray::iterator end(AttrArray& arr)
{
  return arr.end();
}

template <typename T>
AttrArray::const_iterator begin(const AttrArray& arr)
{
  return arr.begin();
}
template <typename T>
AttrArray::const_iterator end(const AttrArray& arr)
{
  return arr.end();
}

/**
 * Array returning a Vector of fixed size
 */
template <typename T>
struct TypedArray : public AttrArray
{
  friend class AttributesSet;
  typedef AttrArray base;

  typedef T value_type;

  typedef const T const_value_type;
  typedef typename std::remove_const<T>::type variable_value_type;

  typedef TypedArray<const_value_type> const_array;
  typedef TypedArray<variable_value_type> variable_array;

  typedef value_type& reference;
  typedef const value_type& const_reference;

  typedef value_type* pointer;
  typedef const value_type* const_pointer;

  typedef pointer iterator;
  typedef const_pointer const_iterator;

  typedef base::size_type size_type;
  typedef base::difference_type difference_type;

  typedef typename attribute_traits<value_type>::element_type element_type;
  typedef const element_type const_element_type;
  typedef typename std::remove_const<element_type>::type variable_element_type;

  /**
   * Create an empty typed array
   */
  TypedArray()
    : AttrArray()
  { }

  /**
   * Create a typed array, from a non-type one.
   * \param ts number of elements in the tuple
   * \param array Base-array
   */
  explicit TypedArray(const AttrArray& array)
    : AttrArray(array)
  {
    if(array.element_size() != sizeof(value_type))
      throw bad_array_type_size();
  }

  TypedArray(const TypedArray<const_value_type>& copy)
    : AttrArray(copy)
  {
    static_assert(std::is_const<T>::value, "You cannot copy from a constant array to a non-constant one.");
  }

  TypedArray(const TypedArray<variable_value_type>& copy)
    : AttrArray(copy)
  {
  }

  TypedArray(TypedArray<const_value_type>&& copy)
    : AttrArray(std::move(copy))
  {
    static_assert(std::is_const<T>::value, "You cannot copy from a constant array to a non-constant one.");
  }

  TypedArray(TypedArray<variable_value_type>&& copy)
    : AttrArray(std::move(copy))
  {
  }

  /**
   * Create a shallow copy of the other array
   */
  TypedArray(const TupleArray<const_element_type>& array)
    : AttrArray(array)
  {
    static_assert(std::is_const<T>::value, "You cannot copy from a constant array to a non-constant one.");
    if(array.element_size() != sizeof(value_type))
      throw bad_array_type_size();
  }

  /**
   * Create a shallow copy of the other array
   */
  TypedArray(const TupleArray<variable_element_type>& array)
    : AttrArray(array)
  {
    if(array.element_size() != sizeof(value_type))
      throw bad_array_type_size();
  }

  /**
   * Move another array to this one
   */
  TypedArray(TupleArray<const_element_type>&& array)
    : AttrArray(std::move(array))
  {
    static_assert(std::is_const<T>::value, "You cannot copy from a constant array to a non-constant one.");
    if(array.element_size() != sizeof(value_type))
      throw bad_array_type_size();
  }

  /**
   * Move another array to this one
   */
  TypedArray(TupleArray<variable_element_type>&& array)
    : AttrArray(std::move(array))
  {
    if(array.element_size() != sizeof(value_type))
      throw bad_array_type_size();
  }

  TypedArray& operator=(const TypedArray<const_value_type>& copy)
  {
    static_assert(std::is_const<T>::value, "You cannot copy from a constant array to a non-constant one.");
    AttrArray::operator=(copy);
    return *this;
  }

  TypedArray& operator=(const TypedArray<variable_value_type>& copy)
  {
    AttrArray::operator=(copy);
    return *this;
  }

  TypedArray& operator=(const TupleArray<const_element_type>& copy)
  {
    static_assert(std::is_const<T>::value, "You cannot copy from a constant array to a non-constant one.");
    if(copy.element_size() != sizeof(value_type))
      throw bad_array_type_size();
    AttrArray::operator=(copy);
    return *this;
  }

  TypedArray& operator=(const TupleArray<variable_element_type>& copy)
  {
    if(copy.element_size() != sizeof(value_type))
      throw bad_array_type_size();
    AttrArray::operator=(copy);
    return *this;
  }

  TypedArray& operator=(TypedArray<const_value_type>&& copy)
  {
    static_assert(std::is_const<T>::value, "You cannot copy from a constant array to a non-constant one.");
    AttrArray::operator=(std::move(copy));
    return *this;
  }

  TypedArray& operator=(TypedArray<variable_value_type>&& copy)
  {
    AttrArray::operator=(std::move(copy));
    return *this;
  }

  TypedArray& operator=(TupleArray<const_element_type>&& copy)
  {
    static_assert(std::is_const<T>::value, "You cannot copy from a constant array to a non-constant one.");
    if(copy.element_size() != sizeof(value_type))
      throw bad_array_type_size();
    AttrArray::operator=(std::move(copy));
    return *this;
  }

  TypedArray& operator=(TupleArray<variable_element_type>&& copy)
  {
    if(copy.element_size() != sizeof(value_type))
      throw bad_array_type_size();
    AttrArray::operator=(std::move(copy));
    return *this;
  }

  /**
   * Get a pointer to the last tuple of the array
   */
  const_reference back() const { return reinterpret_cast<const_reference>(*AttrArray::back()); }
  /**
   * Get a pointer to the last tuple of the array
   */
  reference back() { return reinterpret_cast<reference>(*AttrArray::back()); }

  /**
   * Get a pointer to the last tuple of the array
   */
  const_reference front() const { return reinterpret_cast<const_reference>(*AttrArray::front()); }
  /**
   * Get a pointer to the last tuple of the array
   */
  reference front() { return reinterpret_cast<reference>(*AttrArray::front()); }

  /**
   * Get a pointer to the first tuple of the array
   */
  const_pointer data() const { return reinterpret_cast<const_pointer>(AttrArray::data()); }
  /**
   * Get a pointer to the first tuple of the array
   */
  pointer data() { return reinterpret_cast<pointer>(AttrArray::data()); }

  /**
   * Iterator on the tuples
   */
  iterator begin() { return reinterpret_cast<iterator>(AttrArray::begin()); }
  /**
   * Iterator on the tuples
   */
  const_iterator begin() const { return reinterpret_cast<const_iterator>(AttrArray::begin()); }

  /**
   * Iterator on the tuples
   */
  iterator end() { return reinterpret_cast<iterator>(AttrArray::end()); }
  /**
   * Iterator on the tuples
   */
  const_iterator end() const { return reinterpret_cast<const_iterator>(AttrArray::end()); }

  /**
   * Return a pointer on the beginning of a tuple
   */
  reference operator[](size_type i) { return reinterpret_cast<reference>(*AttrArray::operator[](i)); }
  /**
   * Return a pointer on the beginning of a tuple
   */
  const_reference operator[](size_type i) const { return reinterpret_cast<const_reference>(*AttrArray::operator[](i)); }

  reference min() { return *reinterpret_cast<pointer>(base::min()); }
  reference max() { return *reinterpret_cast<pointer>(base::max()); }
  const_reference min() const { return *reinterpret_cast<const_pointer>(base::min()); }
  const_reference max() const { return *reinterpret_cast<const_pointer>(base::max()); }

  protected:
  /**
   * Set the underlying array
   *
   * \returns true on success
   */
  bool set(AttrArray& array)
  {
    if(array.element_size() != sizeof(value_type))
      return false;
    AttrArray::set(array);
  }

  /**
   * Add elements at the end of the array
   */
  void push_back(const value_type& value, size_type n = 1)
  {
    pointer ptr = reinterpret_cast<pointer>(push_back(n));
    for(size_t i = 0 ; i < n ; ++i)
      ptr[i] = value;
  }
};

template <typename Key, typename Array>
struct IndexedArray : public Array
{
  public:
    typedef Array base;
    typedef typename base::value_type value_type;
    typedef typename base::reference reference;
    typedef typename base::const_reference const_reference;
    typedef typename base::pointer pointer;
    typedef typename base::const_pointer const_pointer;
    typedef typename base::size_type size_type;
    typedef typename base::difference_type difference_type;
    typedef typename base::iterator iterator;
    typedef typename base::const_iterator const_iterator;

    typedef typename base::const_array const_base;
    typedef typename base::variable_array variable_base;

    typedef Key key_type;

    typedef std::unordered_map<key_type,size_type> map_type;
    typedef typename map_type::const_iterator map_iterator;

    IndexedArray(const map_type* m = 0)
      : base()
      , map(m)
    { }

    IndexedArray(size_type tuple_size, base& copy, const map_type* m = 0)
      : base(copy)
      , map(m)
    { }

    IndexedArray(const const_base& copy, const map_type* m = 0)
      : base(copy)
      , map(m)
    { }

    IndexedArray(const_base&& copy, const map_type* m = 0)
      : base(std::move(copy))
      , map(m)
    { }

    IndexedArray(const variable_base& copy, const map_type* m = 0)
      : base(copy)
      , map(m)
    { }

    IndexedArray(variable_base&& copy, const map_type* m = 0)
      : base(std::move(copy))
      , map(m)
    { }

    IndexedArray(const AttrArray& copy, const map_type* m = 0)
      : base(copy)
      , map(m)
    { }

    IndexedArray(AttrArray&& copy, const map_type* m = 0)
      : base(std::move(copy))
      , map(m)
    { }

    IndexedArray(const IndexedArray& copy)
      : base(copy)
      , map(copy.map)
    { }

    IndexedArray(IndexedArray&& copy)
      : base(std::move(copy))
      , map(copy.map)
    { }

    template <typename A>
    IndexedArray(const IndexedArray<Key,A>& copy)
      : base(copy)
      , map(copy.getMap())
    { }

    template <typename A>
    IndexedArray(IndexedArray<Key,A>&& copy)
      : base(std::move(copy))
      , map(copy.getMap())
    { }

    IndexedArray& operator=(const IndexedArray& array)
    {
      base::operator=(array);
      map = array.map;
      return *this;
    }

    IndexedArray& operator=(IndexedArray&& array)
    {
      base::operator=(std::move(array));
      map = array.map;
      return *this;
    }

    template <typename A>
    IndexedArray& operator=(const IndexedArray<Key,A>& array)
    {
      base::operator=(array);
      map = array.getMap();
      return *this;
    }

    template <typename A>
    IndexedArray& operator=(IndexedArray<Key,A>&& array)
    {
      base::operator=(std::move(array));
      map = array.getMap();
      return *this;
    }

    void setMap(const map_type* m)
    { map = m; }

    const map_type* getMap() const { return map; }

    const_reference get(const key_type& key) const
    {
      map_iterator found = map->find(key);
      if(found == map->end())
        return 0;
      return (*this)[found->second];
    }

    /**
     * Return a tuple for this key
     */
    reference get(const key_type& key)
    {
      map_iterator found = map->find(key);
      if(found == map->end())
        return *this->end();
      return (*this)[found->second];
    }

    /**
     * Returns the index of a key.
     *
     * In case of failure, returns the maximum size possible for any array (i.e. AttrArray::max_size())
     */
    size_type index(const key_type& key) const
    {
      map_iterator found = map->find(key);
      if(found == map->end())
        return AttrArray::max_size();
      return found->second;
    }

    friend reference operator->*(const key_type& key, IndexedArray& array)
    {
      return array.get(key);
    }

    friend const_reference operator->*(const key_type& key, const IndexedArray& array)
    {
      return array.get(key);
    }

  private:
    const map_type *map;
};

/**
 * Base class for an interpolator of type T
 */
class Interpolator
{
  public:
    typedef AttrArray::size_type size_type;

    virtual ~Interpolator() {}
    virtual void set(AttrArray& array) = 0;
    virtual std::unique_ptr<Interpolator> copy() const = 0;

    virtual void operator()(size_type out_tuple) = 0;
    virtual void operator()(size_type out_tuple, const std::vector<size_type>& /*in_tuples*/)
    {
      (*this)(out_tuple);
    }

    virtual void operator()(size_type out_tuple, const std::vector<size_type>& in_tuples, const std::vector<double>& /*weights*/)
    {
      (*this)(out_tuple, in_tuples);
    }
};

/**
 * Constant interpolator
 */
template <typename T>
class ConstantInterpolator : public Interpolator
{
  public:
    typedef typename TupleArray<T>::size_type size_type;
    typedef typename TupleArray<T>::pointer pointer;

    void set(AttrArray& a)
    {
      array = (TupleArray<T>)a;
    }

    ConstantInterpolator(const T& val)
      : value(val)
    { }

    ConstantInterpolator(const ConstantInterpolator& ) = default;
    ConstantInterpolator(ConstantInterpolator&& ) = default;

    virtual std::unique_ptr<Interpolator> copy() const { return std::make_unique<ConstantInterpolator>(*this); }

    virtual void operator()(size_type out_id)
    {
      size_type s = array.tupleSize();
      pointer out_tuple = array[out_id];
      for(size_type i = 0 ; i < s ; ++i)
        out_tuple[i] = value;
    }

  protected:
    TupleArray<T> array;
    T value;
};

/**
 * Average interpolator
 */
template <typename T>
class AverageInterpolator : public ConstantInterpolator<T>
{
  public:
    typedef typename TupleArray<T>::size_type size_type;
    typedef typename TupleArray<T>::pointer pointer;

    AverageInterpolator(const T& val)
      : ConstantInterpolator<T>(val)
    { }

    AverageInterpolator(const AverageInterpolator& ) = default;
    AverageInterpolator(AverageInterpolator&& ) = default;

    std::unique_ptr<Interpolator> copy() const { return std::make_unique<AverageInterpolator>(*this); }

    virtual void operator()(size_type out_tuple, const std::vector<size_type>& in_tuples)
    {
      size_type s = this->array.tupleSize();
      pointer out = this->array[out_tuple];
      for(size_type j = 0 ; j < s ; ++j)
        out[j] = T();
      for(size_type i = 0 ; i < in_tuples.size() ; ++i)
      {
        T* in_tuple = this->array[in_tuples[i]];
        for(size_type j = 0 ; j < s ; ++j)
          out[j] += in_tuple[j];
      }
      for(size_type j = 0 ; j < s ; ++j)
        out[j] /= in_tuples.size();
    }
};

/**
 * Weighted Average interpolator
 */
template <typename T>
class WeightedAverageInterpolator : public AverageInterpolator<T>
{
  public:
    typedef typename TupleArray<T>::size_type size_type;
    typedef typename TupleArray<T>::pointer pointer;

    WeightedAverageInterpolator(const T& val)
      : AverageInterpolator<T>(val)
    { }

    WeightedAverageInterpolator(const WeightedAverageInterpolator& ) = default;
    WeightedAverageInterpolator(WeightedAverageInterpolator&& ) = default;


    std::unique_ptr<Interpolator> copy() const { return std::make_unique<WeightedAverageInterpolator>(*this); }

    virtual void operator()(size_type out_tuple, const std::vector<size_type>& in_tuples, const std::vector<double>& in_weights)
    {
      size_type s = this->array.tupleSize();
      pointer out = this->array[out_tuple];
      double sum = 0;
      for(size_type j = 0 ; j < s ; ++j)
      {
        out[j] = T();
      }
      for(size_type i = 0 ; i < in_tuples.size() ; ++i)
      {
        double w = in_weights[i];
        sum += w;
        T* in_tuple = this->array[in_tuples[i]];
        for(size_type j = 0 ; j < s ; ++j)
          out[j] += T(w*in_tuple[j]);
      }
      for(size_type j = 0 ; j < s ; ++j)
        out[j] /= sum;
    }
};


/**
 * Interpolator for normalized data (i.e. 3D vectors of size always 1)
 */
class NormalizedInterpolator : public Interpolator
{
  public:
  NormalizedInterpolator(const Point3f& def)
    : value(def)
  { }

  NormalizedInterpolator(const NormalizedInterpolator&) = default;
  NormalizedInterpolator(NormalizedInterpolator&&)= default;

  std::unique_ptr<Interpolator> copy() const { return std::make_unique<NormalizedInterpolator>(*this); }

  virtual ~NormalizedInterpolator() {}

  virtual void set(AttrArray& a)
  {
    array = (TypedArray<Point3f>)a;
  }

  virtual void operator()(size_type out_tuple)
  {
    array[out_tuple] = value;
  }

  virtual void operator()(size_type out_tuple, const std::vector<size_type>& in_tuples)
  {
    Point3f result;
    for(size_type i = 0 ; i < in_tuples.size() ; ++i)
      result += array[in_tuples[i]];
    array[out_tuple] = normalized(result);
  }

  virtual void operator()(size_type out_tuple, const std::vector<size_type>& in_tuples, const std::vector<double>& weights)
  {
    Point3f result;
    for(size_type i = 0 ; i < in_tuples.size() ; ++i)
      result += weights[i]*array[in_tuples[i]];
    array[out_tuple] = normalized(result);
  }

  TypedArray<Point3f> array;
  Point3f value;
};

/**
 * Store a set of attributes
 */
class AttributesSet
{
  public:
    typedef AttrArray::size_type size_type;

    /**
     * Underlying type for the attributes
     */
    enum AttributeType
    {
      CHAR = 0,
      INT8,
      UINT8,
      INT16,
      UINT16,
      INT32,
      UINT32,
      UNIT,
      DOUBLE,
      NB_ATTR_TYPES,
      FLOAT=UNIT // Alias
    };

    /**
     * Sizes of the different types
     */
    static const size_type typeSizes[NB_ATTR_TYPES];

    /**
     * Conversion from AttributeType to OpenGL type
     */
    static const unsigned int openGLTypes[NB_ATTR_TYPES];

    /**
     * Type of an attribute description
     */
    struct Attribute
    {
      Attribute()
        : interpolator(0)
      { }

      Attribute(const Attribute& copy)
        : name(copy.name)
        , type(copy.type)
        , interpolator(copy.interpolator)
        , array(copy.array)
        , unit(copy.unit)
      { }

      /// Name of the attribute
      QString name;
      /// Type stored
      AttributeType type;
      /// Interpolator
      std::shared_ptr<Interpolator> interpolator;
      /// Actual array with the data
      AttrArray array;
      /// Unit used
      QString unit;

      /// Is the attribute valid
      bool valid() const
      {
        return array.valid();
      }

      /// How many tuples?
      size_type tupleSize() const
      {
        return array.element_size() / AttributesSet::typeSizes[type];
      }

      void updateBounds();
    };

    /**
     * Check if the type \c type correspond to the C++ type T
     */
    template <typename T>
    bool validType(const T& /* value */, AttributeType /*type*/) const
    {
      return false;
    }

    bool validType(const char&, AttributeType type) const
    {
      return type == CHAR;
    }

    bool validType(const int8_t&, AttributeType type) const
    {
      return type == INT8;
    }

    bool validType(const uint8_t&, AttributeType type) const
    {
      return type == UINT8;
    }

    bool validType(const int16_t&, AttributeType type) const
    {
      return type == INT16;
    }

    bool validType(const uint16_t&, AttributeType type) const
    {
      return type == UINT16;
    }

    bool validType(const int32_t&, AttributeType type) const
    {
      return type == INT32;
    }

    bool validType(const uint32_t&, AttributeType type) const
    {
      return type == UINT32;
    }

    bool validType(const float&, AttributeType type) const
    {
      return type == UNIT;
    }

    AttributesSet();
    AttributesSet(const AttributesSet& copy);
    AttributesSet(AttributesSet&& copy);
    ~AttributesSet();

    typedef std::unordered_map<QString, Attribute> attribute_map_t;

    typedef SelectMemberIterator<attribute_map_t::iterator, Attribute, &attribute_map_t::value_type::second> iterator;

    typedef SelectMemberIterator<attribute_map_t::const_iterator, const Attribute, &attribute_map_t::value_type::second> const_iterator;

    AttributesSet& operator=(const AttributesSet& copy);
    AttributesSet& operator=(AttributesSet&& copy);

    /**
     * Add an unitless attribute.
     *
     * \return if the attribute has been added, false if it already existed (or another attribute with the same name
     * exists)
     */
    bool add(QString name, size_type tuple_size, AttributeType type, std::shared_ptr<Interpolator> interpolator);
    /**
     * Add a floating point attribute with a unit
     *
     * \return true if the attribute has been added, false if it already existed (or another attribute with the same name
     * exists)
     */
    bool add(QString name, size_type tuple_size, QString unit, std::shared_ptr<Interpolator> interpolator);
    /**
     * Add an unitless attribute with the data already prepared.
     *
     * \return if the attribute has been added, false if it already existed (or another attribute with the same name
     * exists)
     */
    bool add(QString name, const AttrArray& array, AttributeType type, std::shared_ptr<Interpolator> interpolator);
    /**
     * Add a floating point attribute with a unit
     *
     * \return true if the attribute has been added, false if it already existed (or another attribute with the same name
     * exists)
     */
    bool add(QString name, const AttrArray& array, QString unit, std::shared_ptr<Interpolator> interpolator);

    /**
     * Remove and attribute, and returns true if it was there
     */
    bool erase(QString name);

    /**
     * Return true if the attribute exists
     */
    bool contains(QString name);

    /**
     * Return true if the attribute exists and can be converted into the correct type
     */
    template <typename T>
    bool hasTyped(QString name)
    {
      typedef typename attribute_traits<T>::value_type element_type;
      Attribute attr = AttributesSet::attribute(name);
      if(attr.type == UNIT)
        return attr.valid() and validUnit(element_type(), attr.unit) and sizeof(T) == attr.array.element_size();
      else
        return attr.valid() and validType(element_type(), attr.type) and sizeof(T) == attr.array.element_size();
    }

    /**
     * Return true if the attribute exists and can be converted into the correct tuple
     */
    template <typename T>
    bool hasTuple(QString name)
    {
      Attribute attr = AttributesSet::attribute(name);
      return attr.valid() and validType(T(), attr.type) and attr.tupleSize()*sizeof(T) = attr.array.element_size();
    }

    /**
     * Retrieve an attribute.
     *
     * If the attribute doesn't exist, returns an empty array
     */
    Attribute attribute(QString name) const;

    /**
     * Retrieve an attribute.
     *
     * \see AttributesSet::attribute(QString)
     */
    Attribute operator[](QString name) const { return attribute(name); }

    /**
     * Retrieve the attribute type
     *
     *  If the attribute doesn't exist, the method return NB_ATTR_TYPES
     */
    AttributeType attributeType(QString name) const;

    /**
     * Return the number of elements to give attributes to
     */
    size_type elementCount() const { return _count; }

    /**
     * Update the bounds of an attribute
     *
     * \returns true if the attribute was found.
     */
    bool updateBounds(QString name);

    /**
     * Consolidate all the attributes
     *
     * Returns true if succeeded. Success requires the reordering array to be the same size as the attribute list.
     *
     * \param reordering This parameter gives the correspondance between the tuples before and after. \code new_data[i] =
     * old_data[reordering[i]] \endcode
     */
    bool consolidate(const std::vector<size_type>& reordering);

    /**
     * Start iteration on the attributes
     */
    iterator begin() { return iterator(attribute_map.begin()); }
    /**
     * Start iteration on the attributes
     */
    const_iterator begin() const { return const_iterator(attribute_map.begin()); }
    /**
     * End iteration on the attributes
     */
    iterator end() { return iterator(attribute_map.end()); }
    /**
     * End iteration on the attributes
     */
    const_iterator end() const { return const_iterator(attribute_map.end()); }

    /**
     * Returns true if consolidation is needed
     */
    bool needsConsolidation() const;

    /**
     * Number of attributes
     */
    size_type size() const { return attribute_map.size(); }

    /**
     * Number of elements in each attributes
     */
    size_type count() const { return _count; }

    /**
     * Reset the attributes (i.e. remove all attributes and all elements);
     */
    void reset();

    /**
     * Reserve enough memory for that many elements
     */
    void reserve(size_type count);

    /**
     * Return the list of attribute names
     */
    QStringList names() const;

    /**
     * Return the list of attribute names for a given type
     */
    QStringList names(AttributeType type) const;

    /**
     * Return the list of attribute names for a given type
     */
    QStringList names(const QList<AttributeType>& type) const;

  protected:
    /**
     * Add an element and returns its position
     */
    size_type addElement();

    /**
     * Add a whole bunch of elements, return the index of the first one
     */
    size_type addElements(size_type n);

    /**
     * Remove an element
     */
    void removeElement() { _count--; }

    /**
     * Remove a whole bunch of elements
     */
    void removeElements(size_type n) { _count -= n; }

  private:
    attribute_map_t attribute_map;
    size_type _count;
    size_type _array_size;
};


/**
 * Store a set of attributes, indexed by a key
 */
template <typename Key>
class IndexedAttributesSet : public AttributesSet
{
  public:
    /// Type for sizes
    typedef AttrArray::size_type size_type;
    /// Type of a key
    typedef Key key_type;
    /// Type of the map between the key and their indices
    typedef std::unordered_map<key_type,size_type> key_map_type;

    /// Iterators on the keys
    typedef SelectMemberIterator<typename key_map_type::const_iterator, const key_type,
                                 &key_map_type::value_type::first> key_iterator;

    /// Range on the keys
    typedef std::pair<key_iterator,key_iterator> key_range;

    /// Map of the attributes
    typedef AttributesSet::attribute_map_t attribute_map_t;

    /// Iterators on the attributes
    typedef AttributesSet::iterator iterator;

    /// Constant iterator on the attributes
    typedef AttributesSet::const_iterator const_iterator;

    /// Type of an attribute's description
    typedef AttributesSet::Attribute Attribute;

    /// Enumeration of the underlying types of an attribute
    typedef AttributesSet::AttributeType AttributeType;

    /// Type of the tuple arrays
    template <typename T>
    struct tuple_array_t
    {
      typedef IndexedArray<Key,TupleArray<T> > type;
    };

    /// Type of the typed arrays
    template <typename T>
    struct typed_array_t
    {
      typedef IndexedArray<Key,TypedArray<T> > type;
    };

    IndexedAttributesSet()
      : AttributesSet()
    { }

    IndexedAttributesSet(const IndexedAttributesSet& ) = default;
    IndexedAttributesSet(IndexedAttributesSet&& ) = default;

    IndexedAttributesSet& operator=(const IndexedAttributesSet&) = default;
    IndexedAttributesSet& operator=(IndexedAttributesSet&&) = default;

    /**
     * Returns the index of a key.
     *
     * In case of failure, returns the maximum size possible for any array (i.e. AttrArray::max_size())
     */
    size_type index(const Key& k) const
    {
      typename key_map_type::const_iterator found = key_map.find(k);
      if(found == key_map.end())
        return AttrArray::max_size();
      return found->second;
    }

    /**
     * Return the list of the keys
     */
    key_range keys() const
    {
      return std::make_pair(key_iterator(key_map.begin()),
                            key_iterator(key_map.end()));
    }

    /**
     * Consolidate the attributes
     *
     * This will take the container of keys and make sure all the attributes are ordered in the same manner.
     * Each key must be present exactly once in the container.
     */
    template <typename Container>
    bool consolidate(const Container& new_keys)
    {
      std::vector<size_type> reordering;
      size_type idx = 0;
      for(const Key& key: new_keys)
      {
        typename key_map_type::const_iterator found = key_map.find(key);
        if(found != key_map.end())
          reordering.push_back(found->second);
        else
          reordering.push_back(AttrArray::max_size());
        ++idx;
      }
      bool result = AttributesSet::consolidate(reordering);
      // Update only if success
      if(result)
      {
        idx = 0;
        key_map.clear();
        for(const Key& key: new_keys)
          key_map[key] = idx++;
      }
      return result;
    }

    /**
     * Consolidate using directly a reordering array
     */
    bool consolidate(const std::vector<size_type>& reordering)
    {
      return AttributesSet::consolidate(reordering);
    }

    /**
     * Get the attribute \c name as array of tuples of type \c T
     */
    template <typename T>
    typename tuple_array_t<T>::type attr(QString name)
    {
      typedef typename tuple_array_t<T>::type result_type;
      Attribute attr = AttributesSet::attribute(name);
      if(attr.valid() and validType(T(), attr.type))
      {
        return result_type(attr.tupleSize(), attr.array, &key_map);
      }
      return result_type();
    }

    /**
     * Get the attribute \c name as array of tuples of type \c T
     */
    template <typename T>
    typename tuple_array_t<const T>::type attr(QString name) const
    {
      typedef typename tuple_array_t<const T>::type result_type;
      Attribute attr = AttributesSet::attribute(name);
      if(attr.valid() and validType(T(), attr.type))
      {
        return result_type(attr.tupleSize(), attr.array, &key_map);
      }
      return result_type();
    }

    /**
     * Get the attribute \c name as an array of 32 bits integers tuples
     */
    typename tuple_array_t<char>::type attrc(QString name)
    {
      return attr<char>(name);
    }

    /**
     * Get the attribute \c name as an array of 32 bits integers tuples
     */
    typename tuple_array_t<int8_t>::type attrb(QString name)
    {
      return attr<int8_t>(name);
    }

    /**
     * Get the attribute \c name as an array of 32 bits unsigned integers tuples
     */
    typename tuple_array_t<uint8_t>::type attrub(QString name)
    {
      return attr<uint8_t>(name);
    }

    /**
     * Get the attribute \c name as an array of 32 bits integers tuples
     */
    typename tuple_array_t<int16_t>::type attrs(QString name)
    {
      return attr<int16_t>(name);
    }

    /**
     * Get the attribute \c name as an array of 32 bits unsigned integers tuples
     */
    typename tuple_array_t<uint16_t>::type attrus(QString name)
    {
      return attr<uint16_t>(name);
    }

    /**
     * Get the attribute \c name as an array of 32 bits integers tuples
     */
    typename tuple_array_t<int32_t>::type attri(QString name)
    {
      return attr<int32_t>(name);
    }

    /**
     * Get the attribute \c name as an array of 32 bits unsigned integers tuples
     */
    typename tuple_array_t<uint32_t>::type attru(QString name)
    {
      return attr<uint32_t>(name);
    }

    /**
     * Get the attribute \c name as an array of 32 bits floats tuples
     */
    typename tuple_array_t<float>::type attrf(QString name)
    {
      return attr<float>(name);
    }

    /**
     * Get the attribute \c name as an array of 8 bits unsigned integers tuples
     */
    typename tuple_array_t<const char>::type attrc(QString name) const
    {
      return attr<const char>(name);
    }

    /**
     * Get the attribute \c name as an array of 8 bits unsigned integers tuples
     */
    typename tuple_array_t<const uint8_t>::type attrub(QString name) const
    {
      return attr<const uint8_t>(name);
    }

    /**
     * Get the attribute \c name as an array of 8 bits integers tuples
     */
    typename tuple_array_t<const int8_t>::type attrb(QString name) const
    {
      return attr<const int8_t>(name);
    }

    /**
     * Get the attribute \c name as an array of 16 bits unsigned integers tuples
     */
    typename tuple_array_t<const uint16_t>::type attrus(QString name) const
    {
      return attr<const uint16_t>(name);
    }

    /**
     * Get the attribute \c name as an array of 16 bits integers tuples
     */
    typename tuple_array_t<const int16_t>::type attrs(QString name) const
    {
      return attr<const int16_t>(name);
    }

    /**
     * Get the attribute \c name as an array of 32 bits unsigned integers tuples
     */
    typename tuple_array_t<const uint32_t>::type attru(QString name) const
    {
      return attr<const uint32_t>(name);
    }

    /**
     * Get the attribute \c name as an array of 32 bits integers tuples
     */
    typename tuple_array_t<const int32_t>::type attri(QString name) const
    {
      return attr<const int32_t>(name);
    }

    /**
     * Get the attribute \c name as an array of 32 bits floats tuples
     */
    typename tuple_array_t<const float>::type attrf(QString name) const
    {
      return attr<const float>(name);
    }

    /**
     * Get the attribute \c name as an array of \c T
     */
    template <typename T>
    typename typed_array_t<T>::type typed(QString name)
    {
      typedef typename typed_array_t<T>::type result_type;
      typedef typename attribute_traits<T>::value_type element_type;
      Attribute attr = AttributesSet::attribute(name);
      if((attr.type == UNIT and (attr.valid() and validUnit(element_type(), attr.unit) and sizeof(T) == attr.array.element_size())) or
         (attr.type != UNIT and (attr.valid() and validType(element_type(), attr.type) and sizeof(T) == attr.array.element_size())))
        return result_type(attr.array, &key_map);
      return result_type();
    }

    /**
     * Get the attribute \c name as an array of \c T
     */
    template <typename T>
    typename typed_array_t<const T>::type typed(QString name) const
    {
      typedef typename typed_array_t<const T>::type result_type;
      typedef typename attribute_traits<T>::value_type element_type;
      Attribute attr = AttributesSet::attribute(name);
      if((attr.type == UNIT and (attr.valid() and validUnit(element_type(), attr.unit) and sizeof(T) == attr.array.element_size())) or
         (attr.type != UNIT and (attr.valid() and validType(element_type(), attr.type) and sizeof(T) == attr.array.element_size())))
        return result_type(attr.array, &key_map);
      return result_type();
    }

    /**
     * Insert a new key.
     *
     * The values for the attribute should be calculated from the keys \c ins
     */
    bool insert(const Key& k, const std::vector<Key>& ins)
    {
      std::vector<size_type> ins_idx(ins.size());
      typename key_map_type::const_iterator found;
      for(size_type i = 0 ; i < ins.size() ; ++i)
      {
        found = key_map.find(ins[i]);
        if(found == key_map.end()) return false;
        ins_idx[i] = found->second;
      }
      return insert(k, ins_idx);
    }

    /**
     * Insert a new key.
     *
     * The values for the attribute should be calculated from the keys at position \c ins
     */
    bool insert(const Key& k, const std::vector<size_type>& ins)
    {
      typename key_map_type::iterator found = key_map.find(k);
      if(found == key_map.end())
      {
        size_type idx = addElement(ins);
        key_map[k] = idx;
        return true;
      }
      return false;
    }

    /**
     * Insert a new key.
     *
     * The values for the attribute should be calculated from the keys at position \c ins using weights \c ws
     */
    bool insert(const Key& k, const std::vector<Key>& ins, const std::vector<double>& ws)
    {
      std::vector<size_type> ins_idx(ins.size());
      typename key_map_type::const_iterator found;
      for(size_type i = 0 ; i < ins.size() ; ++i)
      {
        found = key_map.find(ins[i]);
        if(found == key_map.end()) return false;
        ins_idx[i] = found->second;
      }
      return insert(k, ins_idx, ws);
    }

    /**
     * Insert a new key.
     *
     * The values for the attribute should be calculated from the keys at position \c ins using weights \c ws
     */
    bool insert(const Key& k, const std::vector<size_type>& ins, const std::vector<double>& ws)
    {
      typename key_map_type::iterator found = key_map.find(k);
      if(found == key_map.end())
      {
        size_type idx = addElement(ins, ws);
        key_map[k] = idx;
        return true;
      }
      return false;
    }

    /**
     * Insert a new key.
     *
     * The attributes will get default values.
     */
    bool insert(const Key& k)
    {
      typename key_map_type::iterator found = key_map.find(k);
      if(found == key_map.end())
      {
        size_type idx = addElement();
        key_map[k] = idx;
        return true;
      }
      return false;
    }

    /**
     * Insert a set of elements. The container will be iterated twice upon!
     *
     * Each element will be default-initialized.
     *
     * \returns the number of elements actually inserted
     */
    template <typename Container>
    size_type insert(const Container& ks)
    {
      size_type n = 0;
      std::vector<bool> to_insert;
      for(const Key& k: ks)
      {
        typename key_map_type::iterator found = key_map.find(k);
        if(found == key_map.end())
        {
          to_insert.push_back(true);
          ++n;
        }
        else
          to_insert.push_back(false);
      }
      if(n > 0)
      {
        size_type idx = addElements(n);
        size_type i = 0;
        for(const Key* k: ks)
          if(to_insert[i]) key_map[k] = idx++;
      }
      return n;
    }

    /**
     * Remove a key
     */
    bool remove(const Key& k)
    {
      typename key_map_type::iterator found = key_map.find(k);
      if(found != key_map.end())
      {
        key_map.erase(found);
        removeElement();
        return true;
      }
      return false;
    }

    /**
     * Remove a set of keys.
     *
     * \returns the actual number of keys removed
     */
    template <typename Container>
    size_type remove(const Container& ks)
    {
      size_type n = 0;
      for(const Key& k: ks)
      {
        typename key_map_type::iterator found = key_map.find(k);
        if(found != key_map.end())
        {
          key_map.erase(found);
          ++n;
        }
      }
      removeElements(n);
      return n;
    }

  protected:
    key_map_type key_map;
};

} // namespace util
} // namespace lgx

#endif // ATTRIBUTES_HPP

