/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef STACKPROCESSFILTERS_HPP
#define STACKPROCESSFILTERS_HPP

#include <Process.hpp>

#include <cuda/CudaExport.hpp>
#include <Misc.hpp>
#include <Stack.hpp>
#include <Store.hpp>

namespace lgx {
namespace process {

///\addtogroup StackProcess
///@{
/**
 * \class AverageStack StackProcessFilters.hpp <StackProcessFilters.hpp>
 *
 * Compute the local average of each voxel of the stack.
 *
 * More precisely, this module perform the convolution of the stack with the
 * square kernel of given radius.
 */
class LGXCORE_EXPORT AverageStack : public StackProcess {
public:
  AverageStack(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const ParmList& parms) override
  {
    if(!checkState().store(STORE_NON_EMPTY))
      return false;
    Store* input = currentStack()->currentStore();
    Store* output = currentStack()->work();
    Point3i r(parms[0].toInt(), parms[1].toInt(), parms[2].toInt());
    bool res = (*this)(input, output, r, parms[3].toUInt());
    if(res) {
      input->hide();
      output->show();
    }
    return res;
  }

  bool operator()(const Store* input, Store* output, Point3i radius, uint steps);

  QString name() const override {
    return "Average";
  }
  QString description() const override {
    return "Average stack data with a square filter.";
  }
  QString folder() const override {
    return "Filters";
  }
  QStringList parmNames() const override
  {
    return QStringList() << "X Radius"
                         << "Y Radius"
                         << "Z Radius"
                         << "Steps";
  }
  QStringList parmDescs() const override
  {
    return QStringList() << "X Radius"
                         << "Y Radius"
                         << "Z Radius"
                         << "Steps";
  }
  ParmList parmDefaults() const override
  {
    return ParmList() << 1
                      << 1
                      << 1
                      << 1;
  }
  QIcon icon() const override {
    return QIcon(":/images/AvgPix.png");
  }
};

/**
 * \class BrightenStack StackProcessFilters.hpp <StackProcessFilters.hpp>
 *
 * Change the global luminosity of the stack. The value of each voxel will be
 * multiplied by the brightening constant. So a value > 1 will brighten the
 * stack, while a value < 1 will darken the stack
 *
 * Useful examples:
 *  - to convert a stack from 12bits to 16bits, use 16;
 *  - to convert a stack from 8bits to 16bits, use 256.
 */
class LGXCORE_EXPORT BrightenStack : public StackProcess {
public:
  BrightenStack(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const ParmList& parms) override
  {
    if(!checkState().store(STORE_NON_EMPTY))
      return false;
    Stack* s = currentStack();
    Store* input = s->currentStore();
    Store* output = s->work();
    bool res = (*this)(input, output, parms[0].toFloat());
    if(res) {
      input->hide();
      output->show();
    }
    return res;
  }
  bool operator()(const Store* input, Store* output, float brightness);

  QString folder() const override {
    return "Filters";
  }
  QString name() const override {
    return "Brighten Darken";
  }
  QString description() const override
  {
    return "Brighten or Darken stack.\n"
           "A value > 1 will brighten the stack, < 1 will darken it."
           "To convert from 12bits to 16bits stack, use 16."
           "To convert from 8bits to 16bits stack, use 256.";
  }
  QStringList parmNames() const override {
    return QStringList() << "Amount";
  }
  QStringList parmDescs() const override {
    return QStringList() << "Amount to multiply voxels";
  }
  ParmList parmDefaults() const override {
    return ParmList() << 2.0;
  }
  QIcon icon() const override {
    return QIcon(":/images/Brightness.png");
  }
};

/**
 * \class BinarizeStack StackProcessFilters.hpp <StackProcessFilters.hpp>
 *
 * Binarize the stack: set the intensity of any voxel greater than the
 * threshold to 65535 and all other voxels to 0.
 */
class LGXCORE_EXPORT BinarizeStack : public StackProcess {
public:
  BinarizeStack(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const ParmList& parms) override
  {
    if(!checkState().store(STORE_NON_EMPTY))
      return false;
    Stack* s = currentStack();
    Store* input = s->currentStore();
    Store* output = s->work();
    bool res = (*this)(input, output, ushort(parms[0].toUInt()));
    if(res) {
      input->hide();
      output->show();
    }
    return res;
  }
  bool operator()(const Store* input, Store* output, ushort threshold);

  QString folder() const override {
    return "Filters";
  }
  QString name() const override {
    return "Binarize";
  }
  QString description() const override
  {
    return "Transform the stack to binary (65535 or 0).\n"
           "All voxels with an intensity greater than the threshold to 65535, while to others are set to 0.";
  }
  QStringList parmNames() const override {
    return QStringList() << "Threshold";
  }
  QStringList parmDescs() const override {
    return QStringList() << "Voxels above this threshold will be assigned 1.";
  }
  ParmList parmDefaults() const override {
    return ParmList() << 5000;
  }
  QIcon icon() const override {
    return QIcon(":/images/Brightness.png");
  }
};

/**
 * \class ColorGradient StackProcessFilters.hpp <StackProcessFilters.hpp>
 *
 * Compute the gradient of the image in the Z axis of the image.
 */
class LGXCORE_EXPORT ColorGradient : public StackProcess {
public:
  ColorGradient(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const ParmList& parms) override
  {
    if(!checkState().store(STORE_NON_EMPTY))
      return false;
    Stack* s = currentStack();
    Store* input = s->currentStore();
    Store* output = s->work();
    bool res = (*this)(input, output, parms[0].toFloat());
    if(res) {
      input->hide();
      output->show();
    }
    return res;
  }
  bool operator()(const Store* input, Store* output, float colorGradDiv);

  QString folder() const override {
    return "Filters";
  }
  QString name() const override {
    return "Color Gradient";
  }
  QString description() const override {
    return "Compute color gradient in Z direction";
  }
  QStringList parmNames() const override {
    return QStringList() << "Z Divisor";
  }
  QStringList parmDescs() const override {
    return QStringList() << "Factor by which the gradient is divided by.";
  }
  ParmList parmDefaults() const override {
    return ParmList() << 5.0;
  }
  QIcon icon() const override {
    return QIcon(":/images/ColorGrad.png");
  }
};

/**
 * \class InvertStack StackProcessFilters.hpp <StackProcessFilters.hpp>
 *
 * Invert the intensity of all voxels in the stack.
 */
class LGXCORE_EXPORT InvertStack : public StackProcess {
public:
  InvertStack(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const ParmList&) override
  {
    if(!checkState().store(STORE_NON_EMPTY))
      return false;
    Stack* s = currentStack();
    Store* input = s->currentStore();
    Store* output = s->work();
    bool res = (*this)(input, output);
    if(res) {
      input->hide();
      output->show();
    }
    return res;
  }
  bool operator()(const Store* input, Store* output);

  QString folder() const override {
    return "Filters";
  }
  QString name() const override {
    return "Invert";
  }
  QString description() const override {
    return "Invert the stack";
  }
  QStringList parmNames() const override {
    return QStringList();
  }
  QStringList parmDescs() const override {
    return QStringList();
  }
  QIcon icon() const override {
    return QIcon(":/images/Invert.png");
  }
};

/**
 * \class FilterStack StackProcessFilters.hpp <StackProcessFilters.hpp>
 *
 * Clip the intensity of the stack to the interval [Low Threshold, High Threshold].
 */
class LGXCORE_EXPORT FilterStack : public StackProcess {
public:
  FilterStack(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const ParmList& parms) override
  {
    if(!checkState().store(STORE_NON_EMPTY))
      return false;
    Stack* s = currentStack();
    Store* input = s->currentStore();
    Store* output = s->work();
    bool res = (*this)(input, output, parms[0].toUInt(), parms[1].toUInt());
    if(res) {
      input->hide();
      output->show();
    }
    return res;
  }
  bool operator()(const Store* input, Store* output, uint lowFilter, uint highFilter);

  QString folder() const override {
    return "Filters";
  }
  QString name() const override {
    return "Trim high/low values";
  }
  QString description() const override
  {
    return "Clip the voxel intensities to the interval [Low Threshold, High Threshold].";
  }
  QStringList parmNames() const override
  {
    return QStringList() << "Low Threshold"
                         << "High Threshold";
  }
  QStringList parmDescs() const override
  {
    return QStringList() << "Lower bound"
                         << "Upper bound";
  }
  ParmList parmDefaults() const override
  {
    return ParmList() << 1000
                      << 0;
  }
  QIcon icon() const override {
    return QIcon(":/images/Filter.png");
  }
};

/**
 * \class GaussianBlurStack StackProcessFilters.hpp <StackProcessFilters.hpp>
 *
 * Blur the stack using a Gaussian kernel or specified radius.
 *
 * The radius given correspond to the standard deviation of the Gaussian
 * kernel along the various axes. The kernel itself will be 3 time as big.
 */
class LGXCORE_EXPORT GaussianBlurStack : public StackProcess {
public:
  GaussianBlurStack(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const ParmList& parms) override;

  bool operator()(const Store* input, Store* output, Point3f sigma);

  QString folder() const override {
    return "Filters";
  }
  QString name() const override {
    return "Gaussian Blur Stack";
  }
  QString description() const override {
    return "Blur the stack, radius = 3 x Sigma";
  }
  QStringList parmNames() const override
  {
    return QStringList() << QString("X Sigma (%1)").arg(UM) << QString("Y Sigma (%1)").arg(UM)
                         << QString("Z Sigma (%1)").arg(UM);
  }
  QStringList parmDescs() const override
  {
    return QStringList() << QString("X Sigma (%1)").arg(UM) << QString("Y Sigma (%1)").arg(UM)
                         << QString("Z Sigma (%1)").arg(UM);
  }
  ParmList parmDefaults() const override
  {
    return ParmList() << 0.25
                      << 0.25
                      << 0.25;
  }
  QIcon icon() const override {
    return QIcon(":/images/Blur.png");
  }
};

/**
 * \class SharpenStack StackProcessFilters.hpp <StackProcessFilters.hpp>
 *
 * Apply an "unsharp" filter to the stack.
 *
 * The unsharp filter consists in subtracting from the image the blured
 * version of itself. The parameters of the filter correspond to the blurring
 * and a multiplicating factor for the blured image.
 */
class LGXCORE_EXPORT SharpenStack : public StackProcess {
public:
  SharpenStack(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const ParmList& parms) override
  {
    if(!checkState().store(STORE_NON_EMPTY))
      return false;
    Stack* s = currentStack();
    Store* input = s->currentStore();
    Store* output = s->work();
    Point3f sigma(parms[0].toFloat(), parms[1].toFloat(), parms[2].toFloat());
    bool res = (*this)(input, output, sigma, parms[3].toFloat());
    if(res) {
      input->hide();
      output->show();
    }
    return res;
  }
  bool operator()(const Store* input, Store* output, const Point3f sigma, const float amount);

  QString folder() const override {
    return "Filters";
  }
  QString name() const override {
    return "Sharpen Stack";
  }
  QString description() const override {
    return "Sharpen the stack, radius = 3 x Sigma";
  }
  QStringList parmNames() const override
  {
    return QStringList() << QString("X Sigma (%1)").arg(UM) << QString("Y Sigma (%1)").arg(UM)
                         << QString("Z Sigma (%1)").arg(UM) << "Amount";
  }
  QStringList parmDescs() const override
  {
    return QStringList() << QString("X Sigma (%1)").arg(UM) << QString("Y Sigma (%1)").arg(UM)
                         << QString("Z Sigma (%1)").arg(UM) << "Amount";
  }
  ParmList parmDefaults() const override
  {
    return ParmList() << 0.2
                      << 0.2
                      << 1.0
                      << 1.0;
  }
  QIcon icon() const override {
    return QIcon(":/images/Sharpen.png");
  }
};

/**
 * \class ApplyKernelStack StackProcessFilters.hpp <StackProcessFilters.hpp>
 *
 * Apply a user-defined convolution kernel to the current image.
 *
 * The kernel must be separable, so only 1D kernels for the X, Y and Z axes
 * are specified.
 */
class LGXCORE_EXPORT ApplyKernelStack : public StackProcess {
public:
  ApplyKernelStack(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const ParmList& parms) override
  {
    if(!checkState().store(STORE_NON_EMPTY))
      return false;
    Stack* s = currentStack();
    Store* input = s->currentStore();
    Store* output = s->work();
    HVecF kernels[3];
    bool ok;
    QString fields[3] = { "x", "y", "z" };
    for(int i = 0; i < 3; i++) {
      QString str = QString(parms[i].toString()).trimmed();
      QStringList field_values = str.split(" ", QString::SkipEmptyParts);
      kernels[i].resize(field_values.size());
      for(size_t j = 0; j < (size_t)field_values.size(); ++j) {
        kernels[i][j] = field_values[j].toFloat(&ok);
        if(not ok)
          return setErrorMessage(
            QString("Value %1 of field %2 if not a valid number.").arg(j).arg(fields[i]));
      }
    }

    bool res = (*this)(input, output, kernels[0], kernels[1], kernels[2]);
    if(res) {
      input->hide();
      output->show();
    }
    return res;
  }
  bool operator()(const Store* input, Store* output, const HVecF& kernelX, const HVecF& kernelY,
                  const HVecF& kernelZ);

  QString folder() const override {
    return "Filters";
  }
  QString name() const override {
    return "Apply Separable Kernel";
  }
  QString description() const override {
    return "Kernel must have odd number of values";
  }
  QStringList parmNames() const override
  {
    return QStringList() << "X Kernel"
                         << "Y Kernel"
                         << "Z Kernel";
  }
  QStringList parmDescs() const override
  {
    return QStringList() << "X Kernel"
                         << "Y Kernel"
                         << "Z Kernel";
  }
  ParmList parmDefaults() const override
  {
    return ParmList() << "-.1 1.2 -.1"
                      << "-.1 1.2 -.1"
                      << "-.1 1.2 -.1";
  }
  QIcon icon() const override {
    return QIcon(":/images/Kernel.png");
  }
};

/**
 * \class NormalizeStack StackProcessFilters.hpp <StackProcessFilters.hpp>
 *
 * Normalize the intensity of the stack.
 *
 * This process first dilates, then blur the image to obtain a local maximum
 * voxel intensity. The value of the current voxel is then divided by this
 * local maximum.
 */
class LGXCORE_EXPORT NormalizeStack : public StackProcess {
public:
  NormalizeStack(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const ParmList& parms) override
  {
    if(!checkState().store(STORE_NON_EMPTY))
      return false;
    Stack* s = currentStack();
    Store* input = s->currentStore();
    Store* output = s->work();
    Point3f radius(parms[0].toFloat(), parms[1].toFloat(), parms[2].toFloat());
    Point3f sigma(parms[3].toFloat(), parms[4].toFloat(), parms[5].toFloat());
    bool res = (*this)(input, output, radius, sigma, parms[6].toUInt(), parms[7].toFloat());
    if(res) {
      input->hide();
      output->show();
    }
    return res;
  }
  bool operator()(const Store* input, Store* output, Point3f radius, Point3f sigma, uint threshold, float blurFactor);

  QString folder() const override {
    return "Filters";
  }
  QString name() const override {
    return "Normalize Stack";
  }
  QString description() const override {
    return "Normalize the stack";
  }
  QStringList parmNames() const override
  {
    return QStringList() << QString("X Radius (%1)").arg(UM) << QString("Y Radius (%1)").arg(UM)
                         << QString("Z Radius (%1)").arg(UM) << QString("X Sigma (%1)").arg(UM)
                         << QString("Y Sigma (%1)").arg(UM) << QString("Z Sigma (%1)").arg(UM) << "Threshold"
                         << "Blur factor";
  }
  QStringList parmDescs() const override
  {
    return QStringList() << QString("X Radius (%1) for the locality of normalization").arg(UM)
                         << QString("Y Radius (%1) for the locality of normalization").arg(UM)
                         << QString("Z Radius (%1) for the locality of normalization").arg(UM)
                         << QString("X Sigma (%1) for pre-blur").arg(UM)
                         << QString("Y Sigma (%1) for pre-blur").arg(UM)
                         << QString("Z Sigma (%1) for pre-blur").arg(UM)
                         << "Threshold under which pixels are cleared considered as background"
                         << "Relative contribution of blurred vs unblurred dilation";
  }
  ParmList parmDefaults() const override
  {
    return ParmList() << 5.0
                      << 5.0
                      << 5.0
                      << 0.5
                      << 0.5
                      << 0.5
                      << 10000
                      << 0.7;
  }
  QIcon icon() const override {
    return QIcon(":/images/Normalize.png");
  }
};

/**
 * \class CImgGaussianBlurStack StackProcessFilters.hpp <StackProcessFilters.hpp>
 *
 * CImg implementation of the gaussian blur
 */
class LGXCORE_EXPORT CImgGaussianBlurStack : public StackProcess {
public:
  CImgGaussianBlurStack(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const ParmList& parms) override
  {
    if(!checkState().store(STORE_NON_EMPTY))
      return false;
    Stack* s = currentStack();
    Store* input = s->currentStore();
    Store* output = s->work();
    bool res = (*this)(input, output, parms[0].toUInt());
    if(res) {
      input->hide();
      output->show();
    }
    return res;
  }

  bool operator()(const Store* input, Store* output, uint radius);

  QString folder() const override {
    return "CImage";
  }
  QString name() const override {
    return "Cimg Gaussian Blur";
  }
  QString description() const override {
    return "Cimg Gaussian Blur";
  }
  QStringList parmNames() const override {
    return QStringList() << "Radius";
  }
  QStringList parmDescs() const override {
    return QStringList() << "Radius";
  }
  ParmList parmDefaults() const override {
    return ParmList() << "5";
  }
  QIcon icon() const override {
    return QIcon(":/images/Blur.png");
  }
};

/**
 * \class CImgLaplaceStack StackProcessFilters.hpp <StackProcessFilters.hpp>
 *
 * Compute the Laplacian of the stack (using CImg).
 */
class LGXCORE_EXPORT CImgLaplaceStack : public StackProcess {
public:
  CImgLaplaceStack(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const ParmList&) override
  {
    if(!checkState().store(STORE_NON_EMPTY))
      return false;
    Stack* s = currentStack();
    Store* input = s->currentStore();
    Store* output = s->work();
    bool res = (*this)(input, output);
    if(res) {
      input->hide();
      output->show();
    }
    return res;
  }
  bool operator()(const Store* input, Store* output);

  QString folder() const override {
    return "CImage";
  }
  QString name() const override {
    return "CImg Laplace Transform";
  }
  QString description() const override {
    return "CImg Laplace transform of stack";
  }
  QStringList parmNames() const override {
    return QStringList();
  }
  QStringList parmDescs() const override {
    return QStringList();
  }
  QIcon icon() const override {
    return QIcon(":/images/Laplace.png");
  }
};

class LGXCORE_EXPORT NormalizeStackVariance : public StackProcess {
public:
  NormalizeStackVariance(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const ParmList& parms) override
  {
    if(!checkState().store(STORE_NON_EMPTY))
      return false;
    Stack* s = currentStack();
    Store* input = s->currentStore();
    Store* output = s->work();
    Point3f radius = util::stringToPoint3f(parms[0].toString());
    if(radius[0] != radius[0])
      return setErrorMessage("Error, invalid radius");
    float min_mean, mean_var;
    bool ok;
    min_mean = parms[2].toFloat(&ok);
    if(!ok)
      return setErrorMessage("Error, argument 'Min Mean' must be a valid number");
    mean_var = parms[3].toFloat(&ok);
    if(!ok)
      return setErrorMessage("Error, argument 'Mean Var' must be a valid number");
    bool res = (*this)(input, output, radius, parmToBool(parms[1]), min_mean, mean_var);
    if(res) {
      input->hide();
      output->show();
    }
    return res;
  }
  bool operator()(const Store* input, Store* output, Point3f radius, bool norm_mean, float min_mean, float mean_var);

  QString folder() const override {
    return "Filters";
  }
  QString name() const override {
    return "Normalize Stack Variance and Mean.";
  }
  QString description() const override
  {
    return "Normalize the stack variance. "
           "If a radius is negative, the corresponding metric is not normalized.\n"
           "Also, the radii can be specified as single numbers, or three element vectors.";
  }
  QStringList parmNames() const override
  {
    return QStringList() << "Radius (" + UM + ")"
                         << "Normalize mean"
                         << "Min mean"
                         << "Min Variance";
  }
  QStringList parmDescs() const override
  {
    return QStringList() << "Radius used to compute the local mean and variance"
                         << "If true, the output stack will have normalized mean as well as variance"
                         << "If the local mean is less than this value, the voxel is untouched"
                         << "If the local variance is less than this value, the voxel is untouched";
  }
  ParmList parmDefaults() const override
  {
    return ParmList() << 1
                      << true
                      << 100
                      << 0.001;
  }
  ParmChoiceMap parmChoice() const override
  {
    ParmChoiceMap map;
    map[1] = booleanChoice();
    return map;
  }
  QIcon icon() const override {
    return QIcon(":/images/Normalize.png");
  }
};

class LGXCORE_EXPORT FloodFillStack : public StackProcess {
public:
  FloodFillStack(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const ParmList& parms) override
  {
    if(!checkState().store(STORE_NON_EMPTY))
      return false;
    Stack* s = currentStack();
    Store* input = s->currentStore();
    Store* output = s->work();
    bool ok;
    Point3i start = util::stringToPoint3u(parms[0].toString(), &ok);
    if(not ok)
      return setErrorMessage("Error, invalid starting position");
    uint v = parms[1].toUInt(&ok);
    if(not ok or v > 0xFFFF)
      return setErrorMessage("The 'New Value' parameter must be an integer between 0 and 65535.");
    uint threshold = parms[2].toUInt(&ok);
    if(not ok or threshold > 0xFFFF)
      return setErrorMessage("The 'Threshold' parameter must be an integer between 0 and 65535.");
    bool res = (*this)(input, output, start, v, threshold);
    if(res) {
      input->hide();
      output->show();
    }
    return res;
  }

  bool operator()(const Store* input, Store* output, Point3u start, ushort new_value, ushort threshold);

  QString folder() const override {
    return "Filters";
  }
  QString name() const override {
    return "Flood Fill";
  }
  QString description() const override
  {
    return "Flood fill the stack from a given point.";
  }
  QStringList parmNames() const override
  {
    return QStringList() << "Start position"
                         << "New value"
                         << "Threshold";
  }
  QStringList parmDescs() const override
  {
    return QStringList() << "Position starting the flood fill."
                         << "Value to replace the value"
                         << "For non-labeled stacks, threshold blah blah.";
  }
  ParmList parmDefaults() const override
  {
    return ParmList() << "0 0 0"
                      << 0xFFFF
                      << 1024;
  }
  QIcon icon() const override {
    return QIcon(":/images/FillLabel3D.png");
  }
};

///@}
} // namespace process
} // namespace lgx

#endif // STACKPROCESSFILTERS_HPP
