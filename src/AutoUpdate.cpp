/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "AutoUpdate.hpp"

#include <Parms.hpp>
#include <Information.hpp>
#include <LGXVersion.hpp>

#include <QCryptographicHash>
#include <QDesktopServices>
#include <QDialogButtonBox>
#include <QDir>
#include <QMessageBox>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QPushButton>
#include <QSettings>
#include <QStandardPaths>
#include <QTemporaryFile>
#include <QThread>
#include <QRegularExpression>

//QUrl latestINIUrl = QUrl("http://updates.lithographx.com/latest_tmp.ini");
QUrl latestINIUrl = QUrl("http://updates.lithographx.com/latest.ini");
//QUrl latestINIUrl = QUrl("http://sourceforge.net/projects/lithographx/files/latest.ini");
//QUrl latestINIUrl = QUrl("https://bitbucket.org/PierreBdR/lithographx/downloads/latest.ini");

INIVersion parseINIVersion(QIODevice* dev)
{
  INIVersion result;
  auto content = lgx::util::iniFile(dev, 4);

  QString version_string;
  if(content(LithoGraphX_OS, "Version", version_string)) {
    result.version = lgx::util::Version(version_string);
    if(not result.version)
      return {};
    QString url_s;
    if(not content(LithoGraphX_OS, "Url", url_s))
      return {};
    result.url = QUrl(url_s);
    QString md5_s;
    if(not content(LithoGraphX_OS, "md5sum", md5_s))
      return {};
    result.md5sum = QByteArray::fromHex(md5_s.toLocal8Bit());
  } else
    lgx::Information::out << "No section named " LithoGraphX_OS << endl;
  return result;
}

CheckVersionDlg::CheckVersionDlg(QWidget* parent, bool ac)
  : QDialog(parent)
  , autoclose(ac)
{
  ui.setupUi(this);
}

int CheckVersionDlg::exec()
{
  network = new QNetworkAccessManager(this);
  connect(network.data(), &QNetworkAccessManager::finished, this, &CheckVersionDlg::answerReceived);
  connect(this, &CheckVersionDlg::fileDownloaded, this, &CheckVersionDlg::receivedLatestIni);
  retrieveUrl(latestINIUrl);
  return QDialog::exec();
}

void CheckVersionDlg::retrieveUrl(const QUrl& url)
{
  QUrl real_url = url;
  auto req = QNetworkRequest(real_url);
  req.setRawHeader("User-Agent", "LithoGraphX");
  cur_reply = network->get(req);
  connect(cur_reply.data(), &QNetworkReply::downloadProgress, this, &CheckVersionDlg::downloadProgress);
}

void CheckVersionDlg::reject()
{
  if(cur_reply and not cur_reply->isFinished()) {
    cur_reply->abort();
  }
  QDialog::reject();
}

void CheckVersionDlg::answerReceived(QNetworkReply* reply)
{
  // Check for redirection
  auto status = reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
  DEBUG_OUTPUT("Received content of '" << reply->url().toString() << "'\n");
  DEBUG_OUTPUT("HTTP STATUS CODE: " << status << endl);
  bool need_redir = false;
  QUrl new_url;
  auto redir = reply->attribute(QNetworkRequest::RedirectionTargetAttribute);
  if(redir.canConvert<QUrl>()) {
    need_redir = true;
    new_url = reply->url().resolved(redir.value<QUrl>());
  } else if(reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt() == 307 or
            reply->rawHeaderList().contains("Location")) {
    need_redir = true;
    new_url = reply->url().resolved(reply->header(QNetworkRequest::LocationHeader).toString());
  }

  if(need_redir) {
    reply->deleteLater();
    lgx::Information::out << "Redirection toward " << new_url.toString() << endl;
    QThread::msleep(500);
    retrieveUrl(new_url);
  }
  else
    emit fileDownloaded(reply);
}

void CheckVersionDlg::receivedLatestIni(QNetworkReply* reply)
{
  disconnect(this, &CheckVersionDlg::fileDownloaded, this, &CheckVersionDlg::receivedLatestIni);
  auto version = parseINIVersion(reply);
  reply->deleteLater();
  latestVersion = version.version;
  versionUrl = version.url;
  md5sum = version.md5sum;
  ui.buttonBox->setStandardButtons(QDialogButtonBox::Close);
  if(latestVersion) { // save the last check
    QSettings settings;
    // Check if we should check .. no more than once a day!
    settings.beginGroup("AutoUpdate");
    settings.setValue("LastCheck", QDateTime::currentDateTime());
    settings.setValue("LatestVersion", latestVersion.toString());
    settings.endGroup();
  }
  if(latestVersion and latestVersion > lgxVersion) {
    emit newVersionAvailable();
    ui.label->setText(QString("Latest version: %1").arg(latestVersion.toString()));
    ui.buttonBox->setStandardButtons(QDialogButtonBox::Close | QDialogButtonBox::Apply);
    auto download_btn = ui.buttonBox->button(QDialogButtonBox::Apply);
    download_btn->setText("Download ...");
  } else {
    if(autoclose) close();
    else {
      if(latestVersion)
        ui.label->setText("You already have the latest version.");
      else
        ui.label->setText(QString("Couldn't find the next version"));
    }
  }
  ui.progressBar->setMaximum(1);
  ui.progressBar->setValue(1);
}

void CheckVersionDlg::downloadProgress(qint64 received, qint64 total)
{
  ui.progressBar->setMaximum(total);
  ui.progressBar->setValue(received);
}

void CheckVersionDlg::on_buttonBox_clicked(QAbstractButton* btn)
{
  switch(ui.buttonBox->buttonRole(btn)) {
    case QDialogButtonBox::ApplyRole: {
      connect(this, &CheckVersionDlg::fileDownloaded, this, &CheckVersionDlg::receivedNewVersion);
      ui.label->setText(QString("Downloading LithoGraphX version %1 for %2").arg(latestVersion.toString()).arg(LithoGraphX_OS));
      auto download_btn = ui.buttonBox->button(QDialogButtonBox::Apply);
      download_btn->setEnabled(false);
      ui.progressBar->setMaximum(0);
      ui.progressBar->setValue(-1);
      retrieveUrl(versionUrl);
      return;
    }
    default:
      break;
  }
}

void CheckVersionDlg::accept()
{
  QString filename = QDir::toNativeSeparators(versionUrl.toLocalFile());
  if(not QDesktopServices::openUrl(versionUrl))
    QMessageBox::critical(this, "Error starting installer",
                          QString("Failed to start installer '%1'").arg(filename));
  else
    QDialog::accept();
}

void CheckVersionDlg::receivedNewVersion(QNetworkReply* reply)
{
  disconnect(this, &CheckVersionDlg::fileDownloaded, this, &CheckVersionDlg::receivedNewVersion);
  ui.progressBar->setMaximum(1);
  ui.progressBar->setValue(1);
  // Saving files
  QStringList tempDirs = QStandardPaths::standardLocations(QStandardPaths::TempLocation);
  QString fileName = versionUrl.fileName();
  QFile file;
  for(QString tempDir: tempDirs) {
    QDir dir(tempDir);
    auto filePath = dir.filePath(fileName);
    file.setFileName(filePath);
    if(file.open(QIODevice::ReadWrite | QIODevice::Truncate))
      break;
  }
  reply->deleteLater();
  if(not file.isOpen()) {
    QMessageBox::critical(this, "Error downloading LithoGraphX", QString("Cannot create file '%1' in temporary folder.").arg(fileName));
    return;
  }
  versionUrl = QUrl::fromLocalFile(file.fileName());
  auto data = reply->readAll();
  file.write(data);
  // Check md5
  QCryptographicHash hash(QCryptographicHash::Md5);
  hash.addData(data);
  auto res = hash.result();
  if(res == md5sum) {
    QString filename = QDir::toNativeSeparators(file.fileName());
    ui.label->setText(QString("Success! Click 'Install' to launch the updated.\n"
                              "Close LithoGraphX before uninstalling it.\n"
                              "The installer can be found there: %1").arg(filename));
    ui.buttonBox->setStandardButtons(QDialogButtonBox::Close | QDialogButtonBox::Yes);
    auto install_btn = ui.buttonBox->button(QDialogButtonBox::Yes);
    install_btn->setText("Install ...");
  } else {
    ui.label->setText("Checksum failed. You can try to restart the download.");
    auto download_btn = ui.buttonBox->button(QDialogButtonBox::Apply);
    download_btn->setEnabled(true);
  }
}

