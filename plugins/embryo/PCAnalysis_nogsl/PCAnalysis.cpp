#include "PCAnalysis.hpp"
#include "Information.hpp"
#include "Progress.hpp"
/*#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_eigen.h>*/
#include "unorderedmap.hpp"
#include "unorderedset.hpp"
#include <algorithm>
#include <complex>

#include <QFile>
#include <QTextStream>

namespace process
{

// Note: empty namespace = content is not exported, and it's better (cleaner and more general) than the static keyword
namespace
{

struct PCAnalysis_result
{
    Point3f p1, p2, p3;
    Point3f ev;
    Point3f mean;

    bool valid() const
    {
        return normsq(ev) > 0;
    }

    operator bool() const
    {
        return valid();
    }
};

struct SelectThreshold
{
    SelectThreshold(ushort th)
        : threshold(th)
    { }

    int operator()(ushort v) const
    {
        if(v > threshold) return 0;
        return -1;
    }

    ushort value(int vid) const
    {
        if(vid == 1) return threshold;
        return 0;
    }

    int nb_values() const
    {
        return 1;
    }

    ushort threshold;
};

struct SelectLabel
{
    SelectLabel(const std::vector<ushort>& ls)
        : labels(ls)
    {
        for(size_t i = 0 ; i < labels.size() ; ++i)
            inv_labels[labels[i]] = i;
    }

    int operator()(ushort v) const
    {
        std::unordered_map<ushort, int>::const_iterator found = inv_labels.find(v);
        if(found != inv_labels.end())
            return found->second;
        return -1;
    }

    ushort value(int vid) const
    {
        if(vid >= 0 and vid < (int)labels.size()) return labels[vid];
        return 0;
    }

    int nb_values() const
    {
        return inv_labels.size();
    }

    std::vector<ushort> labels;
    std::unordered_map<ushort, int> inv_labels;
};

template <typename Fct>
std::vector<PCAnalysis_result> analysePC(const Stack* stk, const HVecUS& data, const Fct& selection,
                                         Point3f correcting_factor)
{
    // First, compute mean
    size_t nb_values = selection.nb_values();
    std::vector<Point3d> mean(nb_values, Point3f(0,0,0));
    std::vector<unsigned long long> sum(nb_values, 0);
    std::vector<PCAnalysis_result> result(nb_values);

    // Compute the mean for each selected value
    Information::out << "Compute mean" << endl;
    Point3u s = stk->size();
    size_t k = 0;
    for(size_t z = 0 ; z < s.z() ; ++z)
        for(size_t y = 0 ; y < s.y() ; ++y)
            for(size_t x = 0 ; x < s.x() ; ++x, ++k)
            {
                int vid = selection(data[k]);
                if(vid >= 0)
                {
                    ++sum[vid];
                    mean[vid] += stk->imageToWorld(Point3d(x,y,z));
                }
            }
    for(size_t vid = 0 ; vid < nb_values ; ++vid)
    {
        if(sum[vid] > 0)
            mean[vid] /= double(sum[vid]);
    }
    Information::out << "Compute CC matrix" << endl;
    // Compute the cross-correlation matrix
    std::vector<Matrix3f> corr(nb_values);
    k = 0;
    for(size_t z = 0 ; z < s.z() ; ++z)
        for(size_t y = 0 ; y < s.y() ; ++y)
            for(size_t x = 0 ; x < s.x() ; ++x, ++k)
            {
                int vid = selection(data[k]);
                if(vid >= 0)
                {
                    Point3d dp = stk->imageToWorld(Point3d(x,y,z)) - mean[vid];
                    corr[vid](0,0) += dp[0]*dp[0];
                    corr[vid](1,1) += dp[1]*dp[1];
                    corr[vid](2,2) += dp[2]*dp[2];
                    corr[vid](1,0) += dp[0]*dp[1];
                    corr[vid](2,0) += dp[0]*dp[2];
                    corr[vid](2,1) += dp[1]*dp[2];
                }
            }
    /*
    gsl_matrix *mat = gsl_matrix_alloc(3, 3);
    gsl_vector *eval = gsl_vector_alloc(3);
    gsl_matrix *evec = gsl_matrix_alloc(3,3);
    gsl_eigen_symmv_workspace *w = gsl_eigen_symmv_alloc(3);*/
    Information::out << "Decompose matrices" << endl;
    // Eigen-decomposition of the matrices
    for(size_t vid = 0 ; vid < nb_values ; ++vid)
    {
        if(sum[vid] == 0) continue;

        Matrix3d& cr = corr[vid];

        cr(0,1) = cr(1,0);
        cr(0,2) = cr(2,0);
        cr(1,2) = cr(2,1);
        cr /= sum[vid];

        //double a = -1;
        double b = cr(0,0) + cr(1,1) + cr(2,2);
        double c = cr(0,1)*cr(0,1) + cr(0,2)*cr(0,2) + cr(1,2)*cr(1,2) - cr(0,0)*cr(1,1) - cr(0,0)*cr(2,2) - cr(1,1)*cr(2,2);
        double d = 2*cr(0,1)*cr(0,2)*cr(1,2) + cr(0,0)*cr(1,1)*cr(2,2) - cr(0,0)*cr(1,2)*cr(1,2) - cr(1,1)*cr(0,2)*cr(0,2) - cr(2,2)*cr(0,1)*cr(0,1);

        double delta = -18*b*c*d -4*b*b*b*d + b*b*c*c + 4*c*c*c -27*d*d;
        if(delta < 0)
            Information::out << "Error, delta < 0 for vid = " << vid << ". delta = " << delta << endl;
        else
        {
            // First, find eigenvalues
            std::complex<double> exp1(delta, 0);
            exp1 = sqrt(exp1);

            std::complex<double> exp2 = (2*b*b*b + 9*b*c + 27*d + exp1)/2;
            exp2 = pow(exp2, 1./3);
            std::complex<double> exp3 = (2*b*b*b + 9*b*c + 27*d - exp1)/2;
            exp3 = pow(exp3, 1./3);

            std::complex<double> f1(-1./6, -sqrt(3.)/6);
            std::complex<double> f2(-1./6, sqrt(3.)/6);

            double x1 = (b/3 + 1/3*exp2 + 1/3*exp3).real();
            double x2 = (b/3 + f1*exp2 + f2*exp3).real();
            double x3 = (b/3 + f2*exp2 + f1*exp3).real();

            if(x1 >= x2 and x1 >= x3)
            {
                if(x2 >= x3)
                    ev = Point3d(x1, x2, x3);
                else
                    ev = Point3d(x1, x3, x2);
            }
            else if(x2 >= x1 and x2 >= x3)
            {
                if(x1 >= x3)
                    ev = Point3d(x2, x1, x3);
                else
                    ev = Point3d(x2, x3, x1);
            }
            else
            {
                if(x1 >= x2)
                    ev = Point3d(x3, x1, x2);
                else
                    ev = Point3d(x3, x2, x1);
            }

            // Second, find eigenvectors using gaussian-elimination

            for(int i = 0 ; i < 3 ; ++i)
            {
                Matrix3d eq = cr - ev[i]*Matrix3d::identity();

            }


            /*
        for(int i = 0 ; i < 3 ; ++i)
            for(int j = 0 ; j < 3 ; ++j)
                gsl_matrix_set(mat, i, j, corr[vid](i,j));
        gsl_eigen_symmv(mat, eval, evec, w);
        gsl_eigen_symmv_sort(eval, evec, GSL_EIGEN_SORT_VAL_DESC);

        Point3d p1(gsl_matrix_get(evec, 0, 0),
                   gsl_matrix_get(evec, 1, 0),
                   gsl_matrix_get(evec, 2, 0));
        Point3d p2(gsl_matrix_get(evec, 0, 1),
                   gsl_matrix_get(evec, 1, 1),
                   gsl_matrix_get(evec, 2, 1));
        Point3d p3(gsl_matrix_get(evec, 0, 2),
                   gsl_matrix_get(evec, 1, 2),
                   gsl_matrix_get(evec, 2, 2));
        Point3d ev(gsl_vector_get(eval, 0),
                   gsl_vector_get(eval, 1),
                   gsl_vector_get(eval, 2));
                   */
            if((p1^p2)*p3 < 0)
                p3 = -p3;

            // Correct eigen-vector for shape-factor
            if(correcting_factor.x() > 0)
                ev = multiply(Point3d(correcting_factor), map(sqrt,ev));
        }

        //Information::out << "Size of the various dimension = " << ev << endl;

        result[vid].p1 = p1;
        result[vid].p2 = p2;
        result[vid].p3 = p3;
        result[vid].ev = ev;
        result[vid].mean = mean[vid];
        //Information::out << "Value: " << selection.value(vid) << " -- Major axis = " << p1 << " eigenvalues = " << ev << endl;
    }
    /*gsl_eigen_symmv_free(w);
    gsl_vector_free(eval);
    gsl_matrix_free(evec);
    gsl_matrix_free(mat);*/

    if(correcting_factor.x() == 0)
    {
        Information::out << "Compute regions spans" << endl;
        std::vector<Point3d> min_pos(nb_values, Point3f(FLT_MAX));
        std::vector<Point3d> max_pos(nb_values, Point3f(-FLT_MAX));
        // Find the maximum span of each label along each direction
        k = 0;
        for(size_t z = 0 ; z < s.z() ; ++z)
            for(size_t y = 0 ; y < s.y() ; ++y)
                for(size_t x = 0 ; x < s.x() ; ++x, ++k)
                {
                    int vid = selection(data[k]);
                    if(vid >= 0)
                    {
                        Point3d dp = stk->imageToWorld(Point3d(x,y,z));
                        const PCAnalysis_result& res = result[vid];
                        double p1 = dp * res.p1;
                        double p2 = dp * res.p2;
                        double p3 = dp * res.p3;
                        Point3d& pmin = min_pos[vid];
                        Point3d& pmax = max_pos[vid];
                        if(p1 < pmin.x()) pmin.x() = p1;
                        if(p1 > pmax.x()) pmax.x() = p1;
                        if(p2 < pmin.y()) pmin.y() = p2;
                        if(p2 > pmax.y()) pmax.y() = p2;
                        if(p3 < pmin.z()) pmin.z() = p3;
                        if(p3 > pmax.z()) pmax.z() = p3;
                    }
                }
        for(size_t vid = 0 ; vid < nb_values ; ++vid) if(sum[vid] > 0)
        {
            PCAnalysis_result& res = result[vid];
            Point3d pmin = min_pos[vid];
            Point3d pmax = max_pos[vid];
            res.ev = (pmax-pmin)/2;
            res.mean = (pmax+pmin)/2;
            res.mean = res.mean.x()*res.p1 + res.mean.y()*res.p2 + res.mean.z()*res.p3;
        }
    }
    else if(correcting_factor.x() < 0)
    {
        float percentile = -correcting_factor.x();
        Information::out << QString("Computing the %1% of voxels").arg(percentile*100) << endl;
        std::vector<std::vector<double> > pos_p1(nb_values);
        std::vector<std::vector<double> > pos_p2(nb_values);
        std::vector<std::vector<double> > pos_p3(nb_values);
        k = 0;
        for(size_t z = 0 ; z < s.z() ; ++z)
            for(size_t y = 0 ; y < s.y() ; ++y)
                for(size_t x = 0 ; x < s.x() ; ++x, ++k)
                {
                    int vid = selection(data[k]);
                    if(vid >= 0)
                    {
                        Point3d dp = stk->imageToWorld(Point3d(x,y,z));
                        const PCAnalysis_result& res = result[vid];
                        double p1 = dp * res.p1;
                        double p2 = dp * res.p2;
                        double p3 = dp * res.p3;
                        pos_p1[vid].push_back(p1);
                        pos_p2[vid].push_back(p2);
                        pos_p3[vid].push_back(p3);
                    }
                }
        for(size_t vid = 0 ; vid < nb_values ; ++vid) if(sum[vid] > 0)
        {
            std::vector<double>& ps1 = pos_p1[vid];
            std::vector<double>& ps2 = pos_p2[vid];
            std::vector<double>& ps3 = pos_p3[vid];

            std::sort(ps1.begin(), ps1.end());
            std::sort(ps2.begin(), ps2.end());
            std::sort(ps3.begin(), ps3.end());

            int lower = (1-percentile)/2*(ps1.size()-1);
            int upper = ps1.size() - 1 - lower;
            //Information::out << "Positions for vid = " << vid << "(" << ps1.size() << ") = [" << lower << ";" << upper << "]\n";
            PCAnalysis_result& res = result[vid];
            Point3d pmin(ps1[lower], ps2[lower], ps3[lower]);
            Point3d pmax(ps1[upper], ps2[upper], ps3[upper]);
            //Information::out << " -> " << pmin << " -- " << pmax << endl;
            res.ev = (pmax-pmin)/2;
            res.mean = (pmax+pmin)/2;
            res.mean = res.mean.x()*res.p1 + res.mean.y()*res.p2 + res.mean.z()*res.p3;
        }
    }
    return result;
}

void addPCACuboidShape(int lab, const PCAnalysis_result& res, Mesh *mesh, int /*slices*/)
{
    //Information::out << "Draw PCA for label " << lab << endl;
    vvgraph& S = mesh->graph();
    std::vector<vertex> vs(8, vertex(0));
    Point3f center = res.mean;
    for(int i = 0 ; i < 8 ; ++i)
    {
        vertex v;
        v->label = lab;
        vs[i] = v;
    }
    Point3f p1 = res.ev[0] * res.p1;
    Point3f p2 = res.ev[1] * res.p2;
    Point3f p3 = res.ev[2] * res.p3;

    vs[0]->pos = center - p1 - p2 - p3;
    vs[1]->pos = center + p1 - p2 - p3;
    vs[2]->pos = center - p1 + p2 - p3;
    vs[3]->pos = center + p1 + p2 - p3;
    vs[4]->pos = center - p1 - p2 + p3;
    vs[5]->pos = center + p1 - p2 + p3;
    vs[6]->pos = center - p1 + p2 + p3;
    vs[7]->pos = center + p1 + p2 + p3;

    std::vector<Point3i> triangles(12);
    triangles[ 0] = Point3i(0,1,4); // 1
    triangles[ 1] = Point3i(1,5,4);

    triangles[ 2] = Point3i(1,3,5); // 2
    triangles[ 3] = Point3i(3,7,5);

    triangles[ 4] = Point3i(3,2,7); // 3
    triangles[ 5] = Point3i(2,6,7);

    triangles[ 6] = Point3i(2,0,6); // 4
    triangles[ 7] = Point3i(0,4,6);

    triangles[ 8] = Point3i(2,3,0); // 5
    triangles[ 9] = Point3i(3,1,0);

    triangles[10] = Point3i(4,5,6); // 6
    triangles[11] = Point3i(5,7,6);

    //Information::out << "Calling meshFromTriangles" << endl;
    meshFromTriangles(S, vs, triangles);
}

void addPCACylinderShape(int lab, const PCAnalysis_result& res, Mesh *mesh, int slices)
{
    vvgraph& S = mesh->graph();
    std::vector<vertex> vs(5*slices+2, vertex(0));
    Point3f p1 = res.ev[0] * res.p1;
    Point3f p2 = res.ev[1] * res.p2;
    Point3f p3 = res.ev[2] * res.p3;
    Point3f center = res.mean;

    vertex c1, c2;
    c1->label = c2->label = lab;
    c1->pos = center - p1;
    c2->pos = center + p1;
    int nc1 = 5*slices;
    int nc2 = 5*slices+1;
    vs[nc1] = c1;
    vs[nc2] = c2;

    float alpha = 2*M_PI/slices;
    for(int i = 0 ; i < slices ; ++i)
    {
        vertex v1, v2, v3, v4, v5;
        v1->label = v2->label = v3->label = v4->label = v5->label = lab;

        Point3f v1p = Point3f(-1, cos(i*alpha), sin(i*alpha));
        v3->pos = v1->pos = center + v1p.x() * p1 + v1p.y() * p2 + v1p.z() * p3;
        Point3f v2p = Point3f(1, cos(i*alpha), sin(i*alpha));
        v4->pos = v2->pos = center + v2p.x() * p1 + v2p.y() * p2 + v2p.z() * p3;

        v5->pos = center + v2p.y() * p2 + v2p.z() * p3;

        vs[i] = v1;
        vs[slices+i] = v2;
        vs[2*slices+i] = v3;
        vs[3*slices+i] = v4;
        vs[4*slices+i] = v5;
    }

    std::vector<Point3i> triangles(6*slices);
    int i = slices-1;
    for(int i1 = 0 ; i1 < slices ; ++i1)
    {
        int j  = slices+i;
        int j1 = slices+i1;
        int k  = 2*slices+i;
        int k1 = 2*slices+i1;
        triangles[i] = Point3i(nc1, i1, i);
        triangles[i+slices] = Point3i(nc2, j, j1);
        triangles[i+2*slices] = 2*slices+Point3i(i,i1,k);
        triangles[i+3*slices] = 2*slices+Point3i(i1,k1,k);
        triangles[i+4*slices] = 2*slices+Point3i(k,k1,j);
        triangles[i+5*slices] = 2*slices+Point3i(k1,j1,j);
        i = i1;
    }

    meshFromTriangles(S, vs, triangles);
}

}

bool PCAnalysis::operator()(Stack* stk, Store* store, QString filename, int threshold, QString shape, Point3f correcting_factor, int slices, bool& draw_result)
{
    Information::out << "Start PCAnalysis" << endl;

    const HVecUS& data = store->data();
    // Move the stack w.r.t. the PCs

    stk->trans().setPositionAndOrientation(qglviewer::Vec(0,0,0), qglviewer::Quaternion(0,0,0,1));
    stk->setShowTrans(true);

    std::vector<PCAnalysis_result> result;
    std::vector<ushort> labels;
    if(store->labels())
    {
        std::unordered_set<ushort> labs;
        forall(const ushort& l, data)
        {
            if(l > 0) labs.insert(l);
        }
        labels = std::vector<ushort>(labs.begin(), labs.end());

        result = analysePC(stk, data, SelectLabel(labels), correcting_factor);
    }
    else
        result = analysePC(stk, data, SelectThreshold(threshold), correcting_factor);

    if(result.size() == 1)
    {
        CuttingSurface *cf = cuttingSurface();
        qglviewer::Frame& frame = cf->frame();
        //Information::out << "Mean = " << mean << endl;

        PCAnalysis_result res = result[0];
        if(!res)
        {
            setErrorMessage("Error, not result found, do you have any point?");
            return false;
        }
        double mm[16] = { res.p1.x(), res.p2.x(), res.p3.x(), 0,
                          res.p1.y(), res.p2.y(), res.p3.y(), 0,
                          res.p1.z(), res.p2.z(), res.p3.z(), 0,
                          res.mean.x(), res.mean.y(), res.mean.z(), 1};
        Matrix4d mm_gl(mm);
        Information::out << "Transform matrix = " << mm_gl << endl;
        frame.setFromMatrix(mm_gl.c_data());

        // Now, set the planes
        cf->setMode(CuttingSurface::THREE_AXIS);
        cf->setSize(Point3f(res.ev));
        cf->showGrid();
    }

    if(shape == "Cuboids" or shape == "Cylinder")
    {
        Information::out << "Drawing " << shape << endl;
        draw_result = true;
        Mesh *mesh = this->mesh(stk->id());
        mesh->reset();
        void (*shape_fct)(int, const PCAnalysis_result&, Mesh *,int) = 0;
        if(shape == "Cuboids")
            shape_fct = addPCACuboidShape;
        else if(shape == "Cylinder")
            shape_fct = addPCACylinderShape;
        for(size_t vid = 0 ; vid < labels.size() ; ++vid)
        {
            if(result[vid])
            {
                shape_fct(labels[vid], result[vid], mesh, slices);
            }
        }
        mesh->setNormals();
        mesh->updateAll();
    }
    else
        draw_result = false;

    if(!filename.isEmpty())
    {
        QFile f(filename);
        if(!f.open(QIODevice::WriteOnly))
        {
            setErrorMessage(QString("Error, cannot open file '%1' for writing.").arg(filename));
            return false;
        }
        QTextStream ts(&f);
        ts << "Image file," << store->file() << endl;
        if(store->labels())
        {
            ts << "Label, pos.X ("+micrometers+"), pos.Y ("+micrometers+"), pos.Z ("+micrometers+"), e1.X, e1.Y, e1.Z, e2.X, e2.Y, e2.Z, r1 ("+micrometers+"), r2 ("+micrometers+"), r3 ("+micrometers+")" << endl;
            for(size_t vid = 0 ; vid < labels.size() ; ++vid)
            {
                const PCAnalysis_result& res = result[vid];
                ts << labels[vid] << ", "
                   << res.mean.x() << ", " << res.mean.y() << ", " << res.mean.z() << ", "
                   << res.p1.x() << ", " << res.p1.y() << ", " << res.p1.z() << ", "
                   << res.p2.x() << ", " << res.p2.y() << ", " << res.p2.z() << ", "
                   << res.ev[0] << ", " << res.ev[1] << ", " << res.ev[2] << endl;
            }
        }
        else
        {
            const PCAnalysis_result& res = result[0];
            ts << "Threshold," << threshold << endl;
            ts << "Radii";
            for(int i = 0 ; i < 3 ; ++i)
                ts << "," << res.ev[i];
            ts << endl;
            ts << "Center";
            for(int i = 0 ; i < 3 ; ++i)
                ts << "," << res.mean[i];
            ts << "Eigenvectors" << endl;
            ts << ("X ("+micrometers+"),Y ("+micrometers+"),Z ("+micrometers+")") << endl;
            ts << res.p1.x() << "," << res.p1.y() << "," << res.p1.z() << endl;
            ts << res.p2.x() << "," << res.p2.y() << "," << res.p2.z() << endl;
            ts << res.p3.x() << "," << res.p3.y() << "," << res.p3.z() << endl;
            ts << endl;
        }
        f.close();
        Information::out << "Written result in file " << filename << endl;
    }

    return true;
}

REGISTER_GLOBAL_PROCESS(PCAnalysis);
}


