/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef MESH_HPP
#define MESH_HPP

/**
 * \file Mesh.hpp
 *
 * This files contains the definition of a mesh for the Process API
 */

#include <LGXConfig.hpp>

#include <cuda/CudaExport.hpp>
#include <Description.hpp>
#include <EdgeData.hpp>
#include <Geometry.hpp>
#include <Stack.hpp>
#include <SymmetricTensor.hpp>
#include <TransferFunction.hpp>
#include <VertexData.hpp>
#include <VVGraph.hpp>

#include <map>
#ifdef _OPENMP
#  include <omp.h>
#endif
#include <QImage>
#include <QString>
#include <set>
#include <string.h>
#include <typeinfo>
#include <unordered_map>
#include <unordered_set>

namespace lgx {

#ifdef _MSC_VER
template class LGX_EXPORT graph::VVGraph<VertexData, EdgeData>;
template class LGX_EXPORT graph::Vertex<VertexData, std::allocator<VertexData> >;
template class LGX_EXPORT graph::Edge<EdgeData>;
#endif

/**
 * Type of the VV graph holding the actual mesh
 * \ingroup ProcessUtils
 */
typedef graph::VVGraph<VertexData, EdgeData> vvgraph;

/// Type of a vertex
typedef vvgraph::vertex_t vertex;

/// Type of an edge
typedef vvgraph::edge_t edge;

#if !defined(_MSC_VER) || !defined(Q_OS_WIN32) // otherwise, a bug in VC++ prevents using hash maps
typedef unsigned short ushort;
typedef unsigned int uint;
typedef unsigned long ulong;
#endif

typedef util::Color<float> Colorf;

typedef util::Vector<3, bool> Point3b;

/// Map of an integer to a 3D point
typedef std::unordered_map<int, Point3f> IntPoint3fMap;
/// Element in IntPoint3fMap
typedef std::pair<int, Point3f> IntPoint3fPair;
/// Map of an integer to a float
typedef std::unordered_map<int, float> IntFloatMap;
/// Element in IntFloatMap
typedef std::pair<int, float> IntFloatPair;
/// Map of an integer to another one
typedef std::unordered_map<int, int> IntIntMap;
/// Element in IntIntMap
typedef std::pair<int, int> IntIntPair;
/// Map of an integer to a host vector of 3 unsigned integers
typedef std::unordered_map<int, HVec3U> IntHVec3uMap;
/// Element in IntHVec3UMap
typedef std::pair<int, HVec3U> IntHVec3uPair;
/// Set of vertex ids
typedef std::unordered_set<ulong> VIdSet;
/// Set of vertices
typedef std::unordered_set<vertex> VtxSet;
/// Vector of vertices
typedef std::vector<vertex> VtxVec;
/// Vector of floats
typedef std::vector<float> FloatVec;
/// Vector of vvgraphs
typedef std::vector<vvgraph> VVGraphVec;
/// Integer, VVGraph pair
typedef std::pair<int, vvgraph> IntVVGraphPair;
/// Map of an integer to a vvgraph
typedef std::unordered_map<int, vvgraph> IntVVGraphMap;
/// Map of an integer to a set of vertex ids
typedef std::unordered_map<int, VIdSet> IntVIdSetMap;
/// Element in IntVIdSetMap
typedef std::pair<int, VIdSet> IntVIdSetPair;
/// Set of integers
typedef std::unordered_set<int> IntSet;
/// Vector of integers
typedef std::vector<int> IntVec;
/// Element in IntIntSetMap
typedef std::pair<int, IntSet> IntIntSetPair;
/// Map of an integer to a set of integers
typedef std::unordered_map<int, IntSet> IntIntSetMap;
/// Map of a vertex to an integer
typedef std::unordered_map<vertex, int> VIntMap;
/// Element in IntIntVIdSetMap
typedef std::pair<IntIntPair, VIdSet> IntIntVIdSetPair;
/// Element in IntIntFloatMap
typedef std::pair<IntIntPair, float> IntIntFloatPair;
/// Map of a pair of integers to a float
typedef std::unordered_map<IntIntPair, float> IntIntFloatMap;
/// Map of a pair of integers to a set of vertex ids
typedef std::unordered_map<IntIntPair, VIdSet> IntIntVIdSetMap;
/// Map of an integer to a 3x3 matrix
typedef std::unordered_map<int, Matrix3f> IntMatrix3x3fMap;
/// Element in IntMatrix3x3fMap
typedef std::pair<int, Matrix3f> IntMatrix3x3fPair;
/// Map an integer to a symmetric tensor
typedef std::unordered_map<int, util::SymmetricTensor> IntSymTensorMap;
/// Element in IntSymTensorMap
typedef std::pair<int, util::SymmetricTensor> IntSymTensorPair;
/// Map of vertex to vertex
typedef std::unordered_map<vertex, vertex> VertexVertexMap;

struct AttachedData {
  virtual ~AttachedData() {
  }
  virtual void* pointer(const std::type_info& type) const = 0;
  virtual void deletePointer() = 0;

  template <typename T>
  T* pointer() {
    return reinterpret_cast<T*>(this->pointer(typeid(T)));
  }
};

template <typename T>
struct TypedAttachedData : public AttachedData {
  TypedAttachedData(T* ptr)
    : _pointer(ptr)
  {
  }

  void* pointer(const std::type_info& type) const
  {
    if(typeid(T) != type)
      return 0;
    return _pointer;
  }

  T* _pointer;
};

template <typename T> struct AttachedStaticObject : public TypedAttachedData<T> {
  AttachedStaticObject(T* ptr)
    : TypedAttachedData<T>(ptr)
  {
  }

  void deletePointer()
  {
    // Nothing to do as the object is not to be deleted
  }
};

template <typename T> struct AttachedObject : public TypedAttachedData<T> {
  AttachedObject(T* ptr)
    : TypedAttachedData<T>(ptr)
  {
  }

  void deletePointer()
  {
    delete this->_pointer;
    this->_pointer = 0;
  }
};

template <typename T> struct AttachedArray : public TypedAttachedData<T> {
  AttachedArray(T* ptr)
    : TypedAttachedData<T>(ptr)
  {
  }

  void deletePointer()
  {
    delete[] this->_pointer;
    this->_pointer = 0;
  }
};
} // namespace lgx

namespace std {
#ifdef HASH_NEED_TR1
namespace tr1 {
#endif

template <typename T1, typename T2> struct hash<std::pair<T1, T2> > {
  size_t operator()(const std::pair<T1, T2>& v) const
  {
    std::hash<T1> h1;
    std::hash<T2> h2;
    return h1(v.first) ^ h2(v.second);
  }
};

#ifdef HASH_NEED_TR1
} // namespace tr1
#endif

} // namespace std

namespace lgx {

namespace process {
class SetupProcess;
}

/**
 * Enumeration of the possible visualisations for the surface
 * \ingroup ProcessUtils
 */
enum class SurfaceView {
  Normal, ///< Show the color option
  Label,  ///< Show the label of the triangles
  Heat,   ///< Show the current heat map
  Parents ///< Show the parent label
};

/**
 * Enumeration of the possible visualisations for the Mesh
 * \ingroup ProcessUtils
 */
enum class MeshView {
  All,     ///< Show the whole mesh
  Border,  ///< Show only the mesh border
  Cell,    ///< Show only the cell borders (i.e. interface between labels)
  Selected ///< Show only the selected part of the mesh
};

/**
 * Enumeration of the coloring modes possible to display a normal surface
 * \ingroup ProcessUtils
 */
enum class SurfaceColor {
  Signal,  ///< Color using the signal stored on the vertices
  Texture, ///< Color using the 3D texture of the active stack
  Image    ///< Color using the texture stored with the mesh
};

/**
 * \class Mesh Mesh.hpp <Mesh.hpp>
 *
 * This class holds the actual mesh as a VV Graph and all sort of properties
 * for it, including visualization ones.
 *
 * Note that, in LithoGraphX, the user sees the surface, or the mesh. The
 * surface is continuous, when the mesh is the actual connectivity of the
 * vertices.
 *
 * \ingroup ProcessUtils
 */
class LGX_EXPORT Mesh {
  friend class process::SetupProcess;

public:
  /**
   * Create an empty mesh attached to the stack
   */
  Mesh(const Stack* stack);

  /**
   * Create an empty mesh attached to the stack
   */
  Mesh(int id, const Stack* stack);

  /**
   * Copy the mesh
   */
  Mesh(const Mesh& copy);

  /**
   * Mesh destructor
   */
  ~Mesh();

  //@{
  ///\name Global properties
  /// Properties for both mesh and surface

  /**
   * Id of the current mesh
   *
   * This is the same id used in the Process:mesh(int) method.
   */
  int id() const {
    return _id;
  }

  /**
   * Change the id of the mesh.
   *
   * Please do not use this method for meshes attached to a process.
   */
  void setId(int i) {
    _id = i;
  }

  /**
   * Id as seen by the user
   *
   * This is, typically id()+1
   */
  int userId() const {
    return _id + 1;
  }

  /**
   * Returns the stack associated to this mesh
   */
  const Stack* stack() const {
    return _stack;
  }

  // For now, a mesh is always with the same stack
  void setStack(const Stack* s);

  /**
   * Name of the file
   *
   * This name correspond usually to the name of the file during the last
   * load/save and correspond to this mesh.
   */
  const QString& file() const {
    return _file;
  }
  /**
   * Change the name of the file.
   *
   * This name correspond usually to the name of the file during the last
   * load/save and correspond to this mesh. Processes that create a new mesh
   * should typically either set the file to an empty string, to make it
   * clear this mesh has never been saved.
   */
  void setFile(const QString& file = QString());

  /**
   * Returns true if the mesh is scaled
   */
  bool scaled() const {
    return _scaled;
  }
  /**
   * Set if the mesh is scaled
   */
  void setScaled(bool on = true) {
    _scaled = on;
  }

  /**
   * Returns true if the mesh is transformed
   */
  bool transformed() const {
    return _transformed;
  }
  /**
   * Set if the mesh is transformed
   */
  void setTransformed(bool on = true) {
    _transformed = on;
  }

  /**
   * Returns true if the bounding box is shown
   */
  bool showBBox() const {
    return _showBBox;
  }

  /**
   * Set if the bounding box is shown
   */
  void setShowBBox(bool on = true) {
    _showBBox = on;
  }

  /**
   * Returns the current label, without modifying it
   */
  int viewLabel() const {
    return _currLabel;
  }
  /**
   * Sets the current label
   */
  void setLabel(int l)
  {
    changed_label = true;
    _currLabel = l;
  }
  /**
   * Increment the current label and return
   */
  int nextLabel()
  {
    changed_label = true;
    return ++_currLabel;
  }
  /**
   * Returns true if the current label has been changed during this process
   */
  bool labelChanged() const {
    return changed_label;
  }

  /**
   * Reference on the map from each label to a label in a different mesh.
   */
  IntIntMap& parentLabelMap() {
    return _parentLabel;
  }
  /**
   * Const reference on the map from each label to a label in a different mesh.
   */
  const IntIntMap& parentLabelMap() const {
    return _parentLabel;
  }
  /**
   * Update ParentLabel map for label to parent_label when grabbing seed.
   * \author Gerardo Tauriello
   */
  // void mapParentLabel(int label, int parent_label) {_parentLabel[label] = parent_label};
  // HACK
  //_labelHeat[label] = 1;
  // heatMapUnit() = "ON/OFF";
  // heatMapBounds() = Point2f(0, 1);

  /**
   * Returns true if the mesh has a cell structure
   *
   * This means that some vertexes correspond to cell centers and other the
   * junctions.
   */
  bool cells() const {
    return _cells;
  }
  /**
   * Set if the mesh has a cell structure
   *
   * This means that some vertexes correspond to cell centers and other the
   * junctions.
   */
  void setCells(bool value = true) {
    _cells = value;
  }

  /**
   * Reset the whole mesh and its properties.
   *
   * In the end, the mesh is empty, so doesn't contain cells, texture and is neither scaled nor transformed.
   *
   * The next label stays untouched, as it is important to keep coordination between different meshes/stacks.
   */
  void reset();

  //@}

  //@{
  ///\name Structure

  /**
   * Get the VV graph representing the topology of the mesh
   */
  vvgraph& graph() {
    return S;
  }
  /**
   * Get the VV graph representing the topology of the mesh
   */
  const vvgraph& graph() const {
    return S;
  }
  /**
   * True if the mesh is empty (i.e. the graph is empty)
   */
  bool empty() const {
    return S.empty();
  }
  /**
   * Returns the number of vertices in the mesh
   */
  size_t size() const {
    return S.size();
  }

  /**
   * Returns the label of the triangle (v1,v2,v3)
   */
  int getLabel(vertex v1, vertex v2, vertex v3, bool useParentLabel = true) const;

  /**
   * Returns true if (v,n,m) correspond to a canonical triangle.
   *
   * This means it returns true only for one of all the possible triangles
   * made from v, n and m (i.e. (v,n,m), (v,m,n), ...).
   *
   * It also makes sure the triangle is consistent (in the case of vertices with only two neighbors)
   */
  bool uniqueTri(const vertex& v, const vertex& n, const vertex& m) const
  {
    if(v <= n or v <= m or n == m or !S.edge(v, n) or !S.edge(v, m) or !S.edge(n, m))
      return false;
    else if(S.valence(v) == 2)
      return S.nextTo(n, m) == v and S.nextTo(m, v) == n;
    else
      return true;
  }

  /**
   * Update the normal of the vertex \c v in the mesh
   *
   * Returns false if the normal cannot be computed
   */
  bool setNormal(vertex v) {
    return setNormal(S, v);
  }

  /**
   * Update the normal of the vertex \c v in \c S
   *
   * Returns false if the normal cannot be computed
   */
  static bool setNormal(vvgraph& S, vertex v);

  /**
   * Update all the normals of the mesh
   *
   * Returns false if any normal cannot be computed
   */
  bool setNormals();

  /**
   * Reference on the map from label to a position.
   *
   * This position will be intepreted as the center of the label, and used to
   * draw the label number on the mesh or growth directions on cellular mesh.
   *
   */
  IntPoint3fMap& labelCenter() {
    return _labelCenter;
  }
  /**
   * Returns the map from label to position
   */
  const IntPoint3fMap& labelCenter() const {
    return _labelCenter;
  }

  /**
   * Reference on the map from label to a normal.
   *
   * This vector will be intepreted as the normal of the label, and used to
   * draw the cell axis (e.g. principal directions of growth, MT orientation...).
   *
   */
  IntPoint3fMap& labelNormal() {
    return _labelNormal;
  }
  /**
   * Returns the map from label to normal
   */
  const IntPoint3fMap& labelNormal() const {
    return _labelNormal;
  }
  /**
   * Reference on the map from label to a position.
   *
   * This position will be intepreted as the center of the label, and used to
   * draw growth directions on a non-cellular mesh.
   *
   */
  IntPoint3fMap& labelCenterVis() {
    return _labelCenterVis;
  }
  /**
   * Returns the map from label to position
   */
  const IntPoint3fMap& labelCenterVis() const {
    return _labelCenterVis;
  }

  /**
   * Reference on the map from label to a normal.
   *
   * This vector will be intepreted as the normal of the label, and used to
   * draw the cell axis (e.g. principal directions of growth, MT orientation...).
   *
   */
  IntPoint3fMap& labelNormalVis() {
    return _labelNormalVis;
  }
  /**
   * Returns the map from label to position
   */
  const IntPoint3fMap& labelNormalVis() const {
    return _labelNormalVis;
  }

  /**
   * Reference on the map from parent label to a position.
   *
   * This position will be intepreted as the center of the parent label, and used to
   * draw the parent label number on the mesh or growth directions on cellular mesh.
   *
   */
  IntPoint3fMap& parentCenter() {
    return _parentCenter;
  }
  /**
   * Returns the map from parent label to position
   */
  const IntPoint3fMap& parentCenter() const {
    return _parentCenter;
  }

  /**
   * Reference on the map from parent label to a normal.
   *
   * This vector will be intepreted as the normal of the parent label, and used to
   * draw the cell axis (e.g. principal directions of growth, MT orientation...).
   *
   */
  IntPoint3fMap& parentNormal() {
    return _parentNormal;
  }
  /**
   * Returns the map from parent label to normal
   */
  const IntPoint3fMap& parentNormal() const {
    return _parentNormal;
  }
  /**
   * Reference on the map from parent label to a position.
   *
   * This position will be intepreted as the center of the parent label, and used to
   * draw growth directions on a non-cellular mesh.
   *
   */
  IntPoint3fMap& parentCenterVis() {
    return _parentCenterVis;
  }
  /**
   * Returns the map from parent label to position
   */
  const IntPoint3fMap& parentCenterVis() const {
    return _parentCenterVis;
  }

  /**
   * Reference on the map from parent label to a normal.
   *
   * This vector will be intepreted as the normal of the parent label, and used to
   * draw the cell axis (e.g. principal directions of growth, MT orientation...).
   *
   */
  IntPoint3fMap& parentNormalVis() {
    return _parentNormalVis;
  }
  /**
   * Returns the map from parent label to position
   */
  const IntPoint3fMap& parentNormalVis() const {
    return _parentNormalVis;
  }

  /**
   * Reference on the map from labels to the set of neighbor labels.
   *
   * A label is neighbors of another ones if they share at least an edge of the mesh.
   *
   * \note You shouldn't need to modify this structure directly. You should
   * instead call Mesh::updateWallGeometry()
   */
  IntIntSetMap& labelNeighbors() {
    return _labelNeighbors;
  }
  /**
   * Returns the map from labels to the set of neighbors labels.
   */
  const IntIntSetMap& labelNeighbors() const {
    return _labelNeighbors;
  }
  /**
   * Reference on the map from each wall to the list of vertexes in the wall.
   *
   * A wall is identified as the interface between two labels, and as such, is given as a pair of labels. Note that
   * there is a difference between the wall (i,j) and (j,i), as the first label is considered as the interior of the
   * wall, and used as such when calculating the signal along a wall.
   *
   * \note You shouldn't need to modify this structure directly. You should
   * instead call Mesh::updateWallGeometry()
   */
  IntIntVIdSetMap& wallVId() {
    return _wallVId;
  }
  /**
   * Returns the map from each wall to the list of vertexes in the wall.
   */
  const IntIntVIdSetMap& wallVId() const {
    return _wallVId;
  }
  /**
   * Reference on the map from each wall to the length of the wall.
   *
   * The length is calculated as the sum of the edges making the wall. This means the number returned is will depend
   * on the discretization of the surface.
   *
   * \note You shouldn't need to modify this structure directly. You should
   * instead call Mesh::updateWallGeometry()
   */
  IntIntFloatMap& wallGeom() {
    return _wallGeom;
  }
  /**
   * Reference on the map from each wall to the length of the wall.
   */
  const IntIntFloatMap& wallGeom() const {
    return _wallGeom;
  }

  /**
   * Update labelNeighbors, wallVId and wallGeom to reflect current data structure
   * \param borderSize Distance from the wall up to which the triangles are considered part of the wall. This is
   * important mostly for signal intensity.
   */
  void updateWallGeometry(float borderSize);

  /**
   * True if (v,n,m) is on the border of the \c label.
   *
   * \c rp will be set to the wall the triangle is on, if any.
   */
  bool isBordTriangle(int label, vertex v, vertex n, vertex m, IntIntPair& rp) const;

  /**
   * Mark all triangles closer than borderSize from a border as being
   * a border triangle
   */
  void markBorder(float borderSize);

  /**
   * Search for the vertex closest to position of cell center
   * fills in labelCenter, labelNormal, labelCenterVis
   */
  void updateCentersNormals();

  /**
   * Prepare the axis drawing structures
   */
  void prepareAxisDrawing(float scaleAxisLength, bool (*predicate)(float), Colorf colorTrue, Colorf colorFalse,
                          const Point3b& showAxis = Point3b(true, true, true));
  void prepareAxisDrawing(float scaleAxisLength, Colorf colorV1, Colorf colorV2, Colorf colorV3,
                          const Point3b& showAxis = Point3b(true, true, true));

  //@}

  //@{
  ///\name Surface methods
  /**
   * Return the transfer function used to draw the surface in normal mode
   */
  TransferFunction surfFct() const {
    return _surf_fct;
  }
  /**
   * Change the transfer function used to draw the surface in normal mode
   */
  void setSurfFct(const TransferFunction& f)
  {
    if(_surf_fct != f) {
      _surf_fct = f;
      changed_surf_function = true;
    }
  }
  /**
   * Returns true if the normal transfer function has been changed during the
   * current process.
   */
  bool surfFctChanged() const {
    return changed_surf_function;
  }

  /**
   * Return the transfer function used to draw the surface in heat mode
   */
  TransferFunction heatFct() const {
    return _heat_fct;
  }
  /**
   * Change the transfer function used to draw the surface in heat mode
   */
  void setHeatFct(const TransferFunction& f)
  {
    if(_heat_fct != f) {
      _heat_fct = f;
      changed_heat_function = true;
    }
  }
  /**
   * Returns true if the heat transfer function has been changed during the
   * current process.
   */
  bool heatFctChanged() const {
    return changed_heat_function;
  }

  /**
   * Get the current opacity level of the surface
   */
  float opacity() const {
    return _opacity;
  }
  /**
   * Change the current opactity level of the surface
   */
  void setOpacity(float f)
  {
    if(f < 0)
      _opacity = 0;
    else if(f > 1)
      _opacity = 1;
    else
      _opacity = f;
  }
  /**
   * Get the current brightness of the surface
   */
  float brightness() const {
    return _brightness;
  }
  /**
   * Change the current brightness of the surface
   */
  void setBrightness(float f)
  {
    if(f < 0)
      _brightness = 0;
    else if(f > 1)
      _brightness = 1;
    else
      _brightness = f;
  }

  /**
   * True if the surface is currently visible to the user
   */
  bool isSurfaceVisible() const {
    return _isSurfaceVisible;
  }
  /**
   * Show the surface to the user
   */
  void showSurface() {
    _isSurfaceVisible = true;
  }
  /**
   * Hide the surface from the user
   */
  void hideSurface() {
    _isSurfaceVisible = false;
  }

  /**
   * Show the heat map of the surface, if any
   */
  void showNormal() {
    _show = SurfaceView::Normal;
  }
  /**
   * Show the labels of the surface triangles
   */
  void showLabel() {
    _show = SurfaceView::Label;
  }
  /**
   * Show the color of the surface
   */
  void showHeat() {
    _show = SurfaceView::Heat;
  }
  /**
   * Show the color of the surface
   */
  void showParents() {
    _show = SurfaceView::Parents;
  }

  /**
   * Change the view mode
   */
  void show(SurfaceView view) {
    _show = view;
  }

  /**
   * Set the current color to be the signal (i.e. intensity) on each triangle
   */
  void showSignal() {
    _color = SurfaceColor::Signal;
  }
  /**
   * Set the current color to be the 3D texture at the position of the point.
   *
   * This represent the surface as a cut through the volume. The 3D texture
   * selected correspond to the current one for the stack attached to this
   * mesh.
   */
  void showTexture() {
    _color = SurfaceColor::Texture;
  }

  /**
   * Set the current color to be the texture attached to the mesh.
   */
  void showImage() {
    _color = SurfaceColor::Image;
  }

  /**
   * Change the color mode
   */
  void show(SurfaceColor color) {
    _color = color;
  }

  /**
   * Returns if the back surface is culled
   */
  bool culling() const {
    return _culling;
  }
  /**
   * Cull the back surface
   */
  void setCulling(bool cul) {
    _culling = cul;
  }

  /**
   * Return if the surface is rendered blended or not
   */
  bool blending() const {
    return _blending;
  }
  /**
   * Set the blending attribute
   */
  void setBlending(bool b) {
    _blending = b;
  }

  /**
   * Returns the current visualisation for the surface
   */
  SurfaceView toShow() const {
    return _show;
  }
  /**
   * Returns the current coloring for the surface
   */
  SurfaceColor coloring() const {
    return _color;
  }

  /**
   * True if the surface has a texture attached to it
   */
  bool hasImgTex() const {
    return _imgtex;
  }
  /**
   * Get the current texture attached to the surface
   */
  const QImage& imgTex() const {
    return _image;
  }
  /**
   * Set the texture attached to the surface
   */
  void setImgTex(const QImage& img)
  {
    _imgtex = true;
    _image = img;
  }
  /**
   * Remove any texture attached to the surface
   */
  void clearImgTex()
  {
    _imgtex = false;
    _image = QImage();
  }

  //@}

  //@{
  ///\name Mesh methods
  /**
   * Is the mesh currently visible to the user
   */
  bool isMeshVisible() const {
    return _isMeshVisible;
  }
  /**
   * Make the mesh visible to the user
   */
  void showMesh() {
    _isMeshVisible = true;
  }
  /**
   * Hide the mesh from the user
   */
  void hideMesh() {
    _isMeshVisible = false;
  }

  /**
   * Return the current view mode for the mesh
   */
  MeshView meshView() const {
    return _meshView;
  }

  /**
   * Set the mesh view to all
   */
  void showAllMesh() {
    _meshView = MeshView::All;
  }
  /**
   * Set the mesh view to cell border only
   */
  void showCellMesh() {
    _meshView = MeshView::Cell;
  }
  /**
   * Set the mesh view to mesh border only
   */
  void showBorderMesh() {
    _meshView = MeshView::Border;
  }
  /**
   * Set the mesh view to selected mesh only
   */
  void showSelectedMesh() {
    _meshView = MeshView::Selected;
  }

  /**
   * Change the mesh view mode
   */
  void show(MeshView view) {
    _meshView = view;
  }

  /**
   * Are mesh lines visible
   */
  bool showMeshLines() const {
    return _showMeshLines;
  }
  /**
   * Are mesh points visible
   */
  bool showMeshPoints() const {
    return _showMeshPoints;
  }
  /**
   * Are cell numbers visible
   */
  bool showMeshCellMap() const {
    return _showMeshCellMap;
  }

  /**
   * Set if the mesh visible at all
   */
  void setShowMesh(bool show) {
    _isMeshVisible = show;
  }
  /**
   * Set if the mesh lines are visible
   */
  void setShowMeshLines(bool show) {
    _showMeshLines = show;
  }
  /**
   * Set if the mesh points are visible
   */
  void setShowMeshPoints(bool show) {
    _showMeshPoints = show;
  }
  /**
   * Set if the cell numbers are visible
   */
  void setShowMeshCellMap(bool show) {
    _showMeshCellMap = show;
  }

  /**
   * Returns a vector with the list of selected vertices.
   */
  std::vector<vertex> selectedVertices() const;

  /**
   * Returns a vector with the list of active vertices.
   *
   * These are either the selected vertices, or all of them if no vertex was selected.
   */
  std::vector<vertex> activeVertices() const;

  /**
   * Starting with a set of vertices, extend to connected region
   */
  void getConnectedVertices(vvgraph& vSet) const;

  /**
   * Get a list of connected regions (3D cells)
   */
  int getConnectedRegions(const vvgraph& S, VVGraphVec& cellVId, VIntMap& vtxCell) const;

  /**
   * Get the neighborhood of a vertex
   */
  static VtxSet getNeighborhood(const vvgraph& S, const vertex& v, float radius);

  /**
   * Correct selection
   *
   * Make sure the vertices are "correctly" selected. This means that:
   *  * border vertices will be selected if all their non-border neighbors are
   *  * border vertices will be selected if they don't have a non-border neighbors and at least one border neighbor is
   *  selected
   *  * if the selection is inclusive, border vertices will be selected if at least one non-border neighbor is
   *  selected
   */
  void correctSelection(bool inclusive);

  //@}

  //@{
  ///\name Updating the graphical representation
  /**
   * To be called when the mesh structure changed
   */
  void updateAll();
  /**
   * To be called if properties of the triangles changed
   */
  void updateTriangles();
  /**
   * To be called if properties of the lines changed
   */
  void updateLines();
  /**
   * To be called if the position of the points changed
   */
  void updatePositions();
  /**
   * To be called if the selection of the points changed
   */
  void updateSelection();
  /** Returns what changed in the current process
   */
  int changes() const {
    return _changed;
  }
  //@}

  //@{
  /**
   * \name Heat Map methods
   * The heat map associate a heat (from 0 to 1) to a label or a wall.
   *
   * As the heat is always from 0 to 1, the bounds of the heat map have to be
   * specified separatly for presentation purposes and correspond to the
   * actual value associated to 0 and 1.
   */

  /**
   * Clear the current heat map
   */
  void clearHeatmap();

  /** Reference on the map from label to intensity
   */
  IntFloatMap& labelHeat() {
    return _labelHeat;
  }
  /** Returns the map from label to intensity
   */
  const IntFloatMap& labelHeat() const {
    return _labelHeat;
  }
  /** Reference on the map from wall (i.e. pair of vertex id) to intensity
   */
  IntIntFloatMap& wallHeat() {
    return _wallHeat;
  }
  /** Returns the map from wall (i.e. pair of vertex id) to intensity
   */
  const IntIntFloatMap& wallHeat() const {
    return _wallHeat;
  }
  /**
   * Name of the current heat map
   */
  Description& heatMapDesc() {
    return _heatMapDesc;
  }
  /**
   * Name of the current heat map
   */
  const Description& heatMapDesc() const {
    return _heatMapDesc;
  }
  /**
   * Reference on the unit used for the current heat map.
   *
   * It is shown on the color bar.
   */
  QString& heatMapUnit() {
    return _heatMapUnit;
  }
  /// Returns the unit used for the current heat map
  const QString& heatMapUnit() const {
    return _heatMapUnit;
  }
  /// Reference on the upper and lower bounds for the heat.
  Point2f& heatMapBounds() {
    return _heatMapBounds;
  }
  /// Return the upper and lower bounds for the heat.
  const Point2f& heatMapBounds() const {
    return _heatMapBounds;
  }

  /**
   * Return the mapping of tensors for each cell
   *
   * The tensors are encoded as three vectors: ev1, ev2, evals.
   * Where ev1 and ev2 are the two first eigenvectors and evals is the vector with the three eigenvalues.
   */
  IntSymTensorMap& cellAxis() {
    return _cellAxis;
  }

  /**
   * Returns the map from label to cells axis (array of 3 Point3f), scaled for visualisation
   */
  IntMatrix3x3fMap& cellAxisScaled() {
    return _cellAxisScaled;
  }

  /**
   * Returns the map from label to cells axis colors (array of 3 Point3f)
   */
  IntMatrix3x3fMap& cellAxisColor() {
    return _cellAxisColor;
  }

  /** Returns the cell axis width
   */
  float& cellAxisWidth() {
    return _cellAxisWidth;
  }
  /** Returns a reference to the cell axis z-offset for drawing
   */
  float& cellAxisOffset() {
    return _cellAxisOffset;
  }
  /**
   * Return the type of the current cell axis.
   *
   * If an empty string, there is no cell axis.
   */
  const Description& cellAxisDesc() const {
    return _cellAxisDesc;
  }
  /**
   * Return the type of the current cell axis.
   *
   * If an empty string, there is no cell axis.
   */
  Description& cellAxisDesc() {
    return _cellAxisDesc;
  }
  /**
   * Returns true if the cell axis are for the parents labels
   */
  bool isParentAxis() const {
    return _cellAxisParents;
  }
  /**
   * Returns true if the cell axis are for the cells
   */
  bool isCellAxis() const {
    return not _cellAxisParents;
  }
  /**
   * Tell that the axis are for the parents
   */
  void setParentAxis() {
    _cellAxisParents = true;
  }
  /**
   * Tell that the axis are for the cells
   */
  void setCellAxis() {
    _cellAxisParents = false;
  }

  /**
   * Unit for the length of the cell axis
   */
  const QString& cellAxisUnit() const {
    return _cellAxisUnit;
  }
  /**
   * Unit for the length of the cell axis
   */
  QString& cellAxisUnit() {
    return _cellAxisUnit;
  }
  /**
   * Remove any cell axis
   */
  void clearCellAxis();
  /** Returns a reference to the map of correspondence between vertices in mesh1 and mesh2 (store vertices positions)
   */
  IntMatrix3x3fMap& vvCorrespondance() {
    return _VVCorrespondance;
  }
  /** Returns a boolean for visualization of lines between corresponding vertices
   */
  bool& showVVCorrespondance() {
    return _showVVCorrespondance;
  }

  //@}

  //@{
  /**
   * \name Signal methods
   * The signal spans from 0 to 1, but may correspond to any range of values.
   * These methods specifies the range and the units.
   */

  /**
   * Name of the signal
   */
  Description& signalDesc() {
    return _signalDesc;
  }
  const Description& signalDesc() const {
    return _signalDesc;
  }

  /**
   * Reference on the unit used for the signal.
   *
   * It is shown on the color bar.
   */
  QString& signalUnit() {
    return _signalUnit;
  }
  /// Returns the unit used for the signal
  const QString& signalUnit() const {
    return _signalUnit;
  }
  /// Reference on the upper and lower bounds for the signal.
  Point2f& signalBounds() {
    return _signalBounds;
  }
  /// Return the upper and lower bounds for the signal.
  const Point2f& signalBounds() const {
    return _signalBounds;
  }
  //@}

  /**
   * Returns true if the visualization of this Mesh requires a colorbar
   */
  bool needsColorbar() const
  {
    return (isSurfaceVisible() and ((toShow() == SurfaceView::Heat and (_labelHeat.size() > 1 or _wallHeat.size() > 1)) or
                                    (toShow() == SurfaceView::Normal and coloring() == SurfaceColor::Signal)));
  }

  /**
   * Get the current bounding box of the mesh
   */
  const BoundingBox3f& boundingBox() const {
    return _bbox;
  }
  /**
   * Get the (modifiable) current bounding box of the mesh
   */
  BoundingBox3f& boundingBox() {
    return _bbox;
  }
  /**
   * Change the bounding box containing the mesh
   */
  void setBoundingBox(const BoundingBox3f& bbox) {
    _bbox = bbox;
  }
  /// Compute the current bounding box
  void updateBBox();

  //@{
  ///\name Attached data

  /**
   * This method attach the object 'obj' as name 'name'.
   *
   * The object will be deleted when the object is detached.
   *
   * If an object with the same name already exists, it is replaced.
   */
  template <typename T> void attachObject(QString name, T* obj)
  {
    return _attachObject(name, new AttachedObject<T>(obj));
  }

  /**
   * This method attach the object 'obj' as name 'name'.
   *
   * The object's lifetime is assumed to be as long as necessary and it will not be deleted by the system.
   *
   * If an object with the same name already exists, it is replaced.
   */
  template <typename T> void attachStaticObject(QString name, T* obj)
  {
    return _attachObject(name, new AttachedStaticObject<T>(obj));
  }

  /**
   * This method attach the array 'obj' as name 'name'.
   *
   * The array will be deleted when the object is detached.
   *
   * If an object with the same name already exists, it is replaced.
   */
  template <typename T> void attachArray(QString name, T* obj)
  {
    return _attachObject(name, new AttachedArray<T>(obj));
  }

  template <typename T> T* attachment(QString name)
  {
    AttachedData* data = _attached.value(name, 0);
    if(data)
      return data->pointer<T>();
    return 0;
  }

  bool deleteAttached(QString name);
  void deleteAllAttachments();

  //@}

  /**
   * Copy tsignal into signal
   */
  void finalizeTSignal();

private:
  void resetModified();
  void _attachObject(QString name, AttachedData* data);
  void* _getObject(QString name, const char* type);

  vvgraph S;
  TransferFunction _surf_fct, _heat_fct;
  IntFloatMap _labelHeat;
  IntIntFloatMap _wallHeat;
  IntPoint3fMap _labelCenter;
  IntPoint3fMap _labelCenterVis;
  IntPoint3fMap _labelNormal;
  IntPoint3fMap _labelNormalVis;
  IntPoint3fMap _parentCenter;
  IntPoint3fMap _parentCenterVis;
  IntPoint3fMap _parentNormal;
  IntPoint3fMap _parentNormalVis;
  IntSymTensorMap _cellAxis;
  IntMatrix3x3fMap _cellAxisScaled;
  IntMatrix3x3fMap _cellAxisColor;
  float _cellAxisWidth;
  float _cellAxisOffset;
  bool _cellAxisParents;
  Description _cellAxisDesc;
  QString _cellAxisUnit;
  IntMatrix3x3fMap _VVCorrespondance;
  bool _showVVCorrespondance;
  IntIntSetMap _labelNeighbors;
  IntIntVIdSetMap _wallVId;
  IntIntFloatMap _wallGeom;
  Description _signalDesc, _heatMapDesc;
  QString _signalUnit, _heatMapUnit;
  Point2f _signalBounds, _heatMapBounds;
  bool changed_surf_function, changed_heat_function;
  bool changed_label;
  int _changed;
  SurfaceView _show;
  SurfaceColor _color;
  bool _culling, _blending;
  bool _isSurfaceVisible, _isMeshVisible;
  int _currLabel;
  IntIntMap _parentLabel;
  float _opacity, _brightness;
  MeshView _meshView;
  bool _showMeshLines, _showMeshPoints, _showMeshCellMap;
  bool _cells, _imgtex;
  QString _file;
  bool _scaled;
  bool _transformed;
  bool _showBBox;
  int _id;
  const Stack* _stack;
  QImage _image;
  QHash<QString, AttachedData*> _attached;

  BoundingBox3f _bbox;

  void init();
};

/**
 * Test if a number of greater or equal to 0
 *
 * Helper function for prepareAxisDrawing
 *
 * \relates Mesh
 */
LGX_EXPORT bool isPositive(float a);
} // namespace lgx

#endif // MESH_HPP
