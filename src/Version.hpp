/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef VERSION_HPP
#define VERSION_HPP

#include <LGXConfig.hpp>

#include <QString>

// Linux defines thoses ...
#ifdef major
#  undef major
#endif

#ifdef minor
#  undef minor
#endif

namespace lgx {
namespace util {

struct LGX_EXPORT Version
{
  /**
   * Creates an invalid version number
   */
  Version();

  /**
   * Initialized constructor
   */
  constexpr Version(int maj, int min, int pat)
    : major(maj)
    , minor(min)
    , patch(pat)
  { }

  /**
   * Parse a string into a version.
   *
   * If the string is not a valid version, an invalid version object is created.
   */
  explicit Version(QString version);

  Version(const Version&) = default;
  Version(Version&&) = default;

  Version& operator=(const Version&) = default;
  Version& operator=(Version&&) = default;

  /**
   * Create a string representation of the version
   */
  QString toString() const;

  /**
   * Creates an integer 0xMMmmPP
   * where MM is the major number, mm the minor and PP the patch
   *
   * If any of the major, minor or patch is negative of greater than 255 "FF" will be used.
   */
  constexpr uint32_t toInt() const {
    return (((major > 255 ? 0xff : uint32_t(major)) << 16) +
            ((minor < 0 or minor > 255 ? 0xff : uint32_t(minor)) << 8) +
            (patch < 0 or patch > 255 ? 0xff : uint32_t(patch)));
  }

  /**
   * Create a version from an integer
   *
   * \see toInt
   */
  static Version fromInt(uint32_t ver);

  /**
   * Version's major
   */
  int major = -1;
  /**
   * Version's minor (< 0 if not specified)
   */
  int minor = -1;
  /**
   * Version's patch (< 0 if not specified)
   */
  int patch = -1;

  /**
   * Test if the version is valid.
   *
   * To be valid, none of major, minor and patch can be greater or equal to 255.
   * major must be positive, and if patch is positive, then minor must also be.
   */
  bool valid() const;
  /**
   * Equivalent to valid()
   *
   * \see valid
   */
  explicit operator bool() const;

  /**
   * Static invalid version
   */
  static const Version invalid;
};

LGX_EXPORT bool operator==(const Version& v1, const Version& v2);
LGX_EXPORT bool operator!=(const Version& v1, const Version& v2);
LGX_EXPORT bool operator>(const Version& v1, const Version& v2);
LGX_EXPORT bool operator<(const Version& v1, const Version& v2);
LGX_EXPORT bool operator>=(const Version& v1, const Version& v2);
LGX_EXPORT bool operator<=(const Version& v1, const Version& v2);

} // namespace lgx
} // namespace util

#endif // VERSION_HPP

