/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef MESH_PROCESS_LINEAGE_HPP
#define MESH_PROCESS_LINEAGE_HPP

#include <Process.hpp>

#include <Mesh.hpp>

namespace lgx {
namespace process {
///\addtogroup MeshProcess
///@{
/**
 * \class SaveParents MeshProcessLineage.hpp <MeshProcessLineage.hpp>
 *
 * Save mapping from labels to parents
 */
class LGXCORE_EXPORT SaveParents : public MeshProcess {
public:
  SaveParents(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  bool initialize(ParmList& parms, QWidget* parent) override;

  bool operator()(const ParmList& parms) override
  {
    if(!checkState().mesh(MESH_ANY))
      return false;
    Mesh* m = currentMesh();
    return (*this)(m, parms[0].toString());
  }

  bool operator()(Mesh* mesh, const QString& filename);

  QString folder() const override {
    return "Lineage Tracking";
  }
  QString name() const override {
    return "Save Parents";
  }
  QString description() const override {
    return "Save map of labels to parents labels to a file";
  }
  QStringList parmNames() const override {
    return QStringList() << "Filename";
  }
  QStringList parmDescs() const override {
    return QStringList() << "Path to spreadsheet file.";
  }
  ParmList parmDefaults() const override {
    return ParmList() << "";
  }
  QIcon icon() const override {
    return QIcon(":/images/ParentsSave.png");
  }
};

/**
 * \class LoadParents MeshProcessLineage.hpp <MeshProcessLineage.hpp>
 *
 * Load mapping from labels to parents
 */
class LGXCORE_EXPORT LoadParents : public MeshProcess {
public:
  LoadParents(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  bool initialize(ParmList& parms, QWidget* parent) override;

  bool operator()(const ParmList& parms) override
  {
    if(!checkState().mesh(MESH_ANY))
      return false;
    Mesh* m = currentMesh();
    bool res = (*this)(m, parms[0].toString(), parms[0].toBool());
    if(res)
      m->showParents();
    return res;
  }

  bool operator()(Mesh* mesh, const QString& filename, bool markDaughters);

  QString folder() const override {
    return "Lineage Tracking";
  }
  QString name() const override {
    return "Load Parents";
  }
  QString description() const override {
    return "Load map of labels to parents from a file";
  }
  QStringList parmNames() const override {
    return QStringList() << "Filename" << "Mark daughter cells";
  }
  QStringList parmDescs() const override {
    return QStringList() << "Path to spreadsheet file."
                         << "If true, the heat map will be set to 1 if the cell has a parent, 0 otherwise.";
  }
  ParmList parmDefaults() const override {
    return ParmList() << "" << true;
  }
  QIcon icon() const override {
    return QIcon(":/images/ParentsOpen.png");
  }
};

/**
 * \class ClearParents MeshProcessLineage.hpp <MeshProcessLineage.hpp>
 *
 * Clear label to parent mapping
 */
class LGXCORE_EXPORT ClearParents : public MeshProcess {
public:
  ClearParents(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  bool operator()(const ParmList& ) override
  {
    if(!checkState().mesh(MESH_ANY))
      return false;
    Mesh* m = currentMesh();
    return (*this)(m);
  }

  bool operator()(Mesh* mesh);

  QString folder() const override {
    return "Lineage Tracking";
  }
  QString name() const override {
    return "Clear Parents";
  }
  QString description() const override {
    return "Clear mapping from parents to labels";
  }
  QStringList parmNames() const override {
    return QStringList();
  }
  QStringList parmDescs() const override {
    return QStringList();
  }
  QIcon icon() const override {
    return QIcon(":/images/ParentsClear.png");
  }
};

/**
 * \class CorrectParents MeshProcessLineage.hpp <MeshProcessLineage.hpp>
 *
 * Take out non-existing labels from parent map.
 */
class LGXCORE_EXPORT CorrectParents : public MeshProcess {
public:
  CorrectParents(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  bool operator()(const ParmList& ) override
  {
    if(!checkState().mesh(MESH_NON_EMPTY, 0).mesh(MESH_NON_EMPTY, 1))
      return false;
    Mesh* mesh1, *mesh2;
    if(currentMesh() == mesh(0)) {
      mesh2 = mesh(0);
      mesh1 = mesh(1);
    } else if(currentMesh() == mesh(1)) {
      mesh2 = mesh(1);
      mesh1 = mesh(0);
    } else
      return false;

    bool res = (*this)(mesh1, mesh2);
    return res;
  }

  bool operator()(Mesh* mesh1, Mesh* mesh2);

  QString folder() const override {
    return "Lineage Tracking";
  }
  QString name() const override {
    return "Correct Parents";
  }
  QString description() const override {
    return "Take out non-existing labels from parent map.";
  }
  QStringList parmNames() const override {
    return QStringList();
  }
  QStringList parmDescs() const override {
    return QStringList();
  }
  QIcon icon() const override {
    return QIcon(":/images/ParentsCheck.png");
  }
};

/**
 * \class HeatMapDaughterCells MeshProcessLineage.hpp <MeshProcessLineage.hpp>
 *
 * Compute the heap map that shows how many daughter cells a parent cell has.
 */
class HeatMapDaughterCells : public MeshProcess {
public:
  HeatMapDaughterCells(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  bool operator()(const ParmList& ) override
  {
    if(!checkState().mesh(MESH_NON_EMPTY) or currentMesh()->toShow() != SurfaceView::Parents) {
      setErrorMessage("The current mesh must have show parents selected.");
      return false;
    }
    Mesh* m = currentMesh();

    bool res = (*this)(m);
    if(res)
      m->showHeat();
    return res;
  }

  bool operator()(Mesh* m);

  QString folder() const override {
    return "Lineage Tracking";
  }
  QString name() const override {
    return "Heat Map Daughter Cells";
  }
  QString description() const override {
    return "Compute the heat map that shows how may daughter cells a parent cell has.";
  }
  QStringList parmNames() const override {
    return QStringList();
  }
  QStringList parmDescs() const override {
    return QStringList();
  }
  ParmList parmDefaults() const override {
    return ParmList();
  }
  QIcon icon() const override {
    return QIcon(":/images/HeatMapDaughterCells.png");
  }
};

/**
 * \class CopyParentsToLabels MeshProcessLineage.hpp <MeshProcessLineage.hpp>
 *
 * Copy parents to labels, and clear parent table.
 */
class CopyParentsToLabels : public MeshProcess {
public:
  CopyParentsToLabels(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  bool operator()(const ParmList& ) override
  {

    if(!checkState().mesh(MESH_NON_EMPTY) or currentMesh()->toShow() != SurfaceView::Parents) {
      setErrorMessage("The current mesh must have show parents selected.");
      return false;
    }
    Mesh* m = currentMesh();

    bool res = (*this)(m);
    if(res)
      m->showLabel();
    return res;
  }

  bool operator()(Mesh* m);

  QString folder() const override {
    return "Lineage Tracking";
  }
  QString name() const override {
    return "Copy Parents to Labels";
  }
  QString description() const override {
    return "Copy parents to labels, and clear parent table.";
  }
  QStringList parmNames() const override {
    return QStringList();
  }
  QStringList parmDescs() const override {
    return QStringList();
  }
  ParmList parmDefaults() const override {
    return ParmList();
  }
  QIcon icon() const override {
    return QIcon(":/images/ParentsCopyToLabel.png");
  }
};

/**
 * \class AddLineageToTable MeshProcessLineage.hpp <MeshProcessLineage.hpp>
 *
 * Add the lineage information to a CSV file generated from another LithoGraphX process
 */
class AddLineageToTable : public MeshProcess {
public:
  AddLineageToTable(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  bool initialize(ParmList& parms, QWidget* parent) override;

  bool operator()(const ParmList& parms) override
  {
    Mesh* m = currentMesh();

    bool ok;
    size_t headerLine = parms[4].toUInt(&ok);
    if(not ok or headerLine < 1)
      headerLine = 0;

    bool res = (*this)(m, parms[0].toString(), parms[1].toString(),
                       parms[2].toString(), parms[3].toString(), headerLine,
                       parmToBool(parms[5]));
    if(res)
      m->showLabel();
    return res;
  }

  bool operator()(Mesh* m, const QString& inputFile, const QString& outputFile,
                  const QString& cellColumn, const QString& parentColumn,
                  size_t headerLine, bool removeNoLineage);

  QString folder() const override {
    return "Lineage Tracking";
  }
  QString name() const override {
    return "Add Lineage to Table";
  }
  QString description() const override {
    return "Add lineage information to a table saved into a CSV file.\n"
           "If the selected column already exists, it will be over-written.";
  }
  QStringList parmNames() const override {
    return QStringList() << "Filename"
                         << "New filename"
                         << "Cell Column"
                         << "Added Column"
                         << "Header line"
                         << "Remove cells without lineage";
  }
  QStringList parmDescs() const override {
    return QStringList() << "File containing the table to update"
                         << "File to write to, if empty, overwrite the input file"
                         << "Column containing cell id information. Can be a number or a label.\n"
                            "Note: if some columns are named with numbers, they will be selected in priority."
                         << "Name of the column to add"
                         << "If not empty, this should be the line containing the header (starting from 1).\n"
                            "If empty, the process will try to guess where the header is by finding the "
                            "first 10 consecutive lines with the same number of cells. If the file doesn't have 10 "
                            "such lines, the largest block will be used"
                         << "If true, the final file won't include any cell that doesn't have a lineage.\n"
                            "If false, those cells will be given '0' as a parent.";
  }
  ParmList parmDefaults() const override {
    return ParmList() << ""
                      << ""
                      << 0
                      << "Parent"
                      << ""
                      << false;
  }
  QIcon icon() const override {
    return QIcon(":/images/ParentsCopyToLabel.png");
  }
};

///@}
} // namespace process
} // namespace lgx

#endif
