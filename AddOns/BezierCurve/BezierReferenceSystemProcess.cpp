#include "BezierReferenceSystemProcess.hpp"

#include "PolynomialCurve.hpp"

#include <Assert.hpp>
#include <Information.hpp>
#include <Mesh.hpp>
#include <Misc.hpp>
#include <Progress.hpp>

#include <complex>
#include <gsl/gsl_blas.h>
#include <gsl/gsl_integration.h>
#include <gsl/gsl_matrix_float.h>

//#define DEBUG_BEZIERAXIS_OUTPUT

namespace lgx {
namespace process {
namespace {
size_t comb(size_t n, size_t k)
{
    size_t acc = 1;
    if(n - k < k)
        k = n - k;
    for(size_t j = 0; j < k; ++j)
        acc *= n - j;
    for(size_t j = 1; j < k; ++j)
        acc /= j + 1;
    return acc;
}

/**
 * Return the coefficients of the Bézier polynomial.
 *
 * The matrix is returned such that, for the matrix of control points C of shape (order+1, d)
 * the coefficients of the polynomials for each dimension can be obtained with:
 *
 * B(t) = M*C * [1, t, t^2, ..., t^n]
 *
 * The matrix M is given by:
 *
 * M(j, i) = (-1)^(j-i) C(n, i) C(n-i, j-i)    , if j ≥ i
 *           0                                 , otherwise
 *
 * The matrix is returned in row-major order
 */
std::vector<float> bezier_coefs(uint order)
{
    uint n = order + 1;
    std::vector<float> result(n * n);
    for(uint i = 0; i < n; ++i) {
        float head = 1;
        for(uint j = 0; j < n - i; ++j) {
            result[(i + j) * n + i] = head * comb(n - 1, i) * comb(n - i - 1, j);
            head *= -1;
        }
    }
    return result;
}

util::PolynomialCurve computeCurve(const std::vector<Point3f>& ctrlPts)
{
    size_t nb_pts = ctrlPts.size();

#ifdef DEBUG_BEZIERAXIS_OUTPUT
    Information::out << "Ctrl Pts:\n";
    for(size_t i = 0; i < nb_pts; ++i)
        Information::out << i << ": " << ctrlPts[i] << "\n";
    Information::out << "======\n";
#endif // DEBUG_BEZIERAXIS_OUTPUT

    std::vector<float> coefs = bezier_coefs(nb_pts - 1);
    std::vector<Point3f> poly_coefs(nb_pts);
    gsl_matrix_float_view C = gsl_matrix_float_view_array(&coefs[0], nb_pts, nb_pts);
    gsl_matrix_float_const_view P = gsl_matrix_float_const_view_array(&ctrlPts[0][0], nb_pts, 3);

    gsl_matrix_float_view M = gsl_matrix_float_view_array(&poly_coefs[0][0], nb_pts, 3);

    gsl_blas_sgemm(CblasNoTrans, CblasNoTrans, 1.0f, &C.matrix, &P.matrix, 0.f, &M.matrix);

#ifdef DEBUG_BEZIERAXIS_OUTPUT
    Information::out << "Result (#pts: " << nb_pts << "):\n";
    for(size_t i = 0; i < nb_pts; ++i)
        Information::out << i << ": " << poly_coefs[i] << "\n";
    Information::out << "======\n";
#endif // DEBUG_BEZIERAXIS_OUTPUT

    return util::PolynomialCurve(poly_coefs);
}

float bezierProject(const util::PolynomialCurve& curve, const util::PolynomialCurve& deriv, const Point3f p)
{
    util::PolynomialCurve pol = curve - p;
    pol *= deriv;

#ifdef DEBUG_BEZIERAXIS_OUTPUT
    Information::out << "Polynomial Curve:";
    for(size_t i = 0; i < pol.size(); ++i) {
        for(size_t j = 0; j < 3; ++j)
            Information::out << " " << pol[j][i];
        Information::out << "\n";
    }
    Information::out << "======\n";
#endif // DEBUG_BEZIERAXIS_OUTPUT

    std::vector<double> p1d(pol.size(), 0.);
    for(size_t i = 0; i < 3; ++i)
        for(size_t j = 0; j < pol.size(); ++j)
            p1d[j] += pol[i][j];

#ifdef DEBUG_BEZIERAXIS_OUTPUT
    Information::out << "Polynomial:";
    for(size_t i = 0; i < p1d.size(); ++i)
        Information::out << " " << p1d[i];
    Information::out << "\n";
#endif // DEBUG_BEZIERAXIS_OUTPUT

    std::vector<std::complex<double> > result(p1d.size() - 1);

    gsl_poly_complex_workspace* workspace = gsl_poly_complex_workspace_alloc(pol.size());
    gsl_poly_complex_solve(&p1d[0], p1d.size(), workspace, reinterpret_cast<double*>(&result[0]));
    gsl_poly_complex_workspace_free(workspace);

#ifdef DEBUG_BEZIERAXIS_OUTPUT
    Information::out << "Roots:\n";
    for(size_t i = 0; i < result.size(); ++i)
        Information::out << " - " << result[i].real() << " + " << result[i].imag() << "j\n";
#endif // DEBUG_BEZIERAXIS_OUTPUT

    std::vector<double> real_roots;
    for(size_t i = 0; i < result.size(); ++i)
        if(result[i].imag() == 0 and result[i].real() >= 0 and result[i].real() <= 1)
            real_roots.push_back(result[i].real());

    float solution = 0;
    float best_length = FLT_MAX;
    for(size_t i = 0; i < real_roots.size(); ++i) {
        Point3f r = curve.eval<3>(real_roots[i]);
        float l = norm(r - p);
        if(l < best_length) {
            best_length = l;
            solution = real_roots[i];
        }
    }

    float dist0 = norm(curve.eval<3>(0) - p);
    float dist1 = norm(curve.eval<3>(1) - p);
    float dist_end = (dist0 < dist1 ? dist0 : dist1);
    float best_end = (dist0 < dist1 ? 0 : 1);

    if(real_roots.empty())
        solution = best_end;
    else if(dist_end < best_length) {
        // Check if this is plausible
        // It isn't be the closest end is very different from the best position
        if(fabs(solution - best_end) > .1)
            solution = best_end;
    }

    return solution;
}

typedef std::unordered_map<int, Point3f> int_point_map_t;
typedef std::unordered_map<int, float> int_float_map_t;

int_point_map_t findCells(Mesh* mesh)
{
    int_point_map_t cell_center;
    int_float_map_t cell_size;

    const vvgraph& S = mesh->graph();

    for(size_t i = 0; i < S.size(); ++i) {
        vertex v = S[i];
        if(v->label > 0) {
            cell_center[v->label] += v->pos;
            cell_size[v->label] += 1;
        }
    }

    for(int_point_map_t::iterator it = cell_center.begin(); it != cell_center.end(); ++it) {
        it->second /= cell_size[it->first];
    }

    return cell_center;
}

double size_deriv(double t, void* params)
{
    util::PolynomialCurve* deriv = reinterpret_cast<util::PolynomialCurve*>(params);
    Point3f p = deriv->eval<3>(t);
    return norm(p);
}

float length(util::PolynomialCurve& deriv, float t)
{
    double result, error;

    gsl_function F;
    F.function = &size_deriv;
    F.params = &deriv;

    gsl_integration_workspace* w = gsl_integration_workspace_alloc(1000);
    gsl_integration_qags(&F, 0, t, 0, 1e-6, 1000, w, &result, &error);
    gsl_integration_workspace_free(w);

    return result;
}

Point3f tangent(util::PolynomialCurve& deriv, float t)
{
    Point3f p = deriv.eval<3>(t);
    return normalized(p);
}

Point3f curvature(util::PolynomialCurve& deriv, float t, float dt = 1e-5)
{
    float t1 = std::max(0.f, t - dt);
    float t2 = std::min(1.f, t + dt);
    Point3f p1 = tangent(deriv, t1);
    Point3f p2 = tangent(deriv, t2);
    return (p2 - p1) / (t2 - t1);
}
} // namespace

BezierReferenceSystemProcess::DisplayType BezierReferenceSystemProcess::parseDisplay(const QString& name)
{
    QString low = name.toLower();
    if(low == "length")
        return DT_LENGTH;
    if(low == "normal")
        return DT_NORMAL;
    if(low == "binormal")
        return DT_BINORM;
    if(low == "radius")
        return DT_RADIUS;
    if(low == "none")
        return DT_NONE;
    Information::err << "Warning, unknown display type '" << name << "', 'none' will be used" << endl;
    return DT_NONE;
}

QString BezierReferenceSystemProcess::displayName(DisplayType display)
{
    switch(display) {
    case DT_LENGTH:
        return "Length";
    case DT_NORMAL:
        return "Normal";
    case DT_BINORM:
        return "Binormal";
    case DT_RADIUS:
        return "Radius";
    case DT_NONE:
        return "none";
    }
    return "none";
}

bool BezierReferenceSystemProcess::operator()(
    Mesh* mesh, CuttingSurface* cut, const QString& output, DisplayType display)
{
    // First, extract the cells and their centers
    std::unordered_map<int, Point3f> cell_center = findCells(mesh);

    // Then, extract the Bezier curve
    uint nb_pts = cut->bezPoints();
#ifdef DEBUG_BEZIERAXIS_OUTPUT
    Information::out << "nb_pts = " << nb_pts << endl;
#endif // DEBUG_BEZIERAXIS_OUTPUT

    std::vector<Point3f> points(nb_pts);

    // Test where is the line. By default: evvery nb_pts, otherwise, consecutive
    const std::vector<Point3f>& bezier_pts = cut->bezierV();
    bool is_consecutive = bezier_pts[0] == bezier_pts[nb_pts];

    for(uint i = 0; i < nb_pts; ++i)
        points[i] = (is_consecutive ? bezier_pts[i] : bezier_pts[i * nb_pts]);

    util::PolynomialCurve curve = computeCurve(points);
    util::PolynomialCurve deriv = curve.deriv();

    // For each cell, find its projection
    int_float_map_t projection;
    int_point_map_t new_ref;
    for(int_point_map_t::const_iterator it = cell_center.begin(); it != cell_center.end(); ++it) {
        int lab = it->first;
        const Point3f& p = it->second;
        float b = bezierProject(curve, deriv, it->second);
        const Point3f& proj = curve.eval<3>(b);
        const Point3f& t = tangent(deriv, b);
        const Point3f& n = normalized(curvature(deriv, b));
        const Point3f& bn = t ^ n;
        float l = length(deriv, b);

        projection[lab] = b;
        new_ref[lab] = Point3f(l, n * (p - proj), bn * (p - proj));
    }


    if(not output.isEmpty()) {
        QFile file(output);
        if(not file.open(QIODevice::WriteOnly))
            return setErrorMessage(QString("Error, cannot option file '%1' for writing").arg(output));
        QTextStream ts(&file);
        ts << QString("Label,CenterX (%1),CenterY (%1),CenterZ (%1),Bezier,Length (%1),Normal (%1),Binormal "
                      "(%1),Radius (%1),Angle\r\n").arg(UM);
        for(int_point_map_t::const_iterator it = cell_center.begin(); it != cell_center.end(); ++it) {
            int lab = it->first;
            const Point3f& p = it->second;
            float b = projection[lab];
            const Point3f& nr = new_ref[lab];
            QStringList fields = QStringList()
                << QString::number(lab) << QString::number(p.x()) << QString::number(p.y()) << QString::number(p.z())
                << QString::number(b) << QString::number(nr.x()) << QString::number(nr.y()) << QString::number(nr.z())
                << QString::number(norm(Point2f(nr.y(), nr.z()))) << QString::number(atan2(nr.z(), nr.y()));
            ts << fields.join(",") << "\r\n";
            ;
        }
    }

    if(display != DT_NONE) {
        mesh->clearHeatmap();
        IntFloatMap& heat = mesh->labelHeat();

        float max_val = -FLT_MAX;
        float min_val = FLT_MAX;

        for(int_point_map_t::const_iterator it = new_ref.begin(); it != new_ref.end(); ++it) {
            int lab = it->first;
            const Point3f& p = it->second;
            float val;
            switch(display) {
            case DT_RADIUS:
                val = norm(Point2f(p.y(), p.z()));
                break;
            case DT_LENGTH:
                val = p.x();
                break;
            case DT_NORMAL:
                val = p.y();
                break;
            case DT_BINORM:
                val = p.z();
                break;
            case DT_ANGLE:
                val = atan2(p.z(), p.y());
                break;
            case DT_NONE:
                throw QString("Programming error, this should never be here!");
            }
            heat[lab] = val;
            if(val > max_val)
                max_val = val;
            if(val < min_val)
                min_val = val;
        }

        mesh->showHeat();
        mesh->heatMapBounds() = Point2f(min_val, max_val);
        mesh->heatMapUnit() = UM;
        QString desc = displayName(display);
        mesh->heatMapDesc() = Description(desc);
        mesh->updateTriangles();
    }

    return true;
}

} // namespace process
} // namespace lgx
