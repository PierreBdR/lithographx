MGX_LIBS=$$system(MorphoGraphX --libs)
MGX_INCLUDE=$$system(MorphoGraphX --include)
MGX_PROCESSES=$$system(MorphoGraphX --process)
CUDA_INCLUDE=/usr/local/cuda/include
CIMG_INCLUDE=

TEMPLATE = lib

TARGET=CellCurvature

DEFINES += cimg_display=0
SOURCES += CellCurvature.cpp
HEADERS += CellCurvature.hpp
RESOURCES = CellCurvature.qrc

CONFIG += qt plugin
QT += xml opengl

INCLUDEPATH += $$MGX_INCLUDE $$MGX_PROCESSES/include $$CUDA_INCLUDE $$CIMG_INCLUDE
macx: {
LIBS += -F$$MGX_LIBS -framework mgx
} else {
LIBS += -L$$MGX_LIBS -lmgx
}

target.path = $$MGX_PROCESSES
INSTALLS = target


