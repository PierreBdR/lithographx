if defined LGX_PYTHON (
    rem Remove trailing backslash from LGX_PYTHON, if any
    if "!LGX_PYTHON:~-1!"=="\" SET LGX_PYTHON=!LGX_PYTHON:~,-1!
    set PATH="!LGX_PYTHON!";"!LGX_PYTHON!\Scripts";!PATH!
)
conda install path.py scikit-learn
pause
