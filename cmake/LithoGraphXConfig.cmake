#.rst
# LithoGraphXConfig
# -----------------
#
# Find LithoGraphX
#
# This module sets the following result variables:
#
#     LithoGraphX_FOUND
#         Whether LithoGraphX was found or not
#     LithoGraphX_VERSION
#         Version of the installed LithoGraphX
#     LithoGRaphX_PROCESS_VERSION
#         Version of the process subsystem
#     LithoGraphX_PROGRAM
#         Path to the LithoGraphX program
#     LithoGraphX_lgxPack
#         Path to the lgxPack program
#     LithoGraphX_LIBRARIES
#         List of libraries to link to LithoGraphX
#     LithoGraphX_OS
#         Name of the operating system LithoGraphX has been compiled for
#     LithoGraphX_BIN_DIR
#         Folder containing the LithoGraphX program
#     LithoGraphX_USER_PROCESS_DIR
#         Folder containing user-installed plugins
#     LithoGraphX_PROCESS_DIR
#         Folder containing system-installed plugins
#     LithoGraphX_USER_MACRO_DIR
#         Folder containing user-installed macros
#     LithoGraphX_MACRO_DIR
#         Folder containing system-installed macros
#     LithoGraphX_INCLUDE_DIR
#         Main LithoGraphX include directory
#     LithoGraphX_USER_LIB_DIR
#         Folder containing user-installed libraries
#     LithoGraphX_LIB_DIR
#         Folder containing system-installed libraries
#     LithoGraphX_USER_PACKAGES_DIR
#         Folder containing the definition of user packages
#     LithoGraphX_PACKAGES_DIR
#         Folder containing the definition of system packages
#
# Then, for each requested module, the following variables will be defined:
#
#     LithoGraphX_${Module}_FOUND: whether the module has been found or not
#     LithoGraphX_${Module}_VERSION: Version of the module
#     LithoGraphX_${Module}_LIBRARIES: Libraries to link against
#     LithoGraphX_${Module}_INCLUDE_DIRS: Folders containing the include files
#     LithoGraphX_${Module}_LIB_DIR: Folder containing the libraries
#     LithoGraphX_${Module}_LOCATION: Location of the file ${module}.cmake
#     LithoGraphX_${Module}_INSTALL_TYPE: 'user', 'system' or 'builtin' depending on how the module was installed.
#         If 'builint', the INCLUDE_DIRS, LIB_DIR and LOCATION are not defined.
#
# Also, for each module, a target ``LithoGraphX::${Module}`` is created to use
# in TARGET_LINK_LIBRARIES() which will set all dependent libraries, include
# directories, ...
#
# This module also creates the following macro:
#
#     INIT_LGX_PACKAGE()
#       This function analyses the description.ini and creates the following
#       variables and call the PROJECT function with the package name as
#       argument:
#
#           LithoGraphX_PACKAGE_NAME
#               Name of the package
#           ${LithoGraphX_PACKAGE_NAME}_VERSION
#               Version of the package, which MUST be in the form MAJOR.MINOR.PATCH
#           ${LithoGraphX_PACKAGE_NAME}_DESCRIPTION
#               Description of the package
#           ${LithoGraphX_PACKAGE_NAME}_DEPENDENCIES
#               Dependencies of the package
#
#       You can also set these variables by hand and call PROJECT() yourself.
#
#     LGX_PACKAGE()
#       This function creates the files needed by the package and setup CPack variables.
#       Two files may be created:
#
#         1. ``description.ini`` is always created using the variables created by INIT_LGX_PACKAGE()
#         2. ``${LithoGraphX_PACKAGE_NAME}.cmake`` is created if the package
#             exports at least one library and contains the definition to link
#             against it. It uses as a model the file ``Package.cmake`` found
#             either in the current folder or in the ``cmake`` directory of
#             LithoGraphX. The following variables can be used to influence the generated cmake module:
#
#                 LithoGraphX_${package}_EXTRA_INCLUDE_DIRS
#                     List of directories containing useful include dirs (probably from dependencies)
#                 LithoGraphX_${package}_EXTRA_LIBS
#                     List of libraries to link against, in addition to the ones defined in the package.
#                 ${package}_HAS_LIBS
#                     This is set automatically by LGX_ADD_LIBRARY, but if set
#                     to false, no CMake module will be generated. You can use
#                     this to prevent users from linking to your module.
#
#     LGX_FIND_PACKAGE(...)
#       Calls FIND_PACKAGE(...) but record the call to repeat it in the module.
#
#     LGX_ADD_PLUGIN(target source1 [source2 ...])
#       Add a plugin, compiled from source. It will be installed at the proper place.
#
#     LGX_ADD_MACRO(macro1 [macro2 ...] [STRIP_DIR dir])
#       Add a set of macros to the macro folder. If ``STRIP_DIR`` is specified,
#       all files should be in the given folder, which will not be created in
#       the macro folder.
#
#     LGX_ADD_LIBRARY(target [SHARED|STATIC] [MAIN] source1 [source2 ...] [PUBLIC_HEADER header1 [header2 ...]])
#       Add a library linked to LithoGraphX and installed in the library folder.
#       If no library is marked as MAIN, the last specified one will be the main one.
#
#     LGX_DESCRIPTION(field output_var required)
#       Read the given field from the description.ini file in the local folder
#       and store the result in output_var. If required is TRUE and the field
#       is not present or empty, this raises an error.
#
# Note: All paths must use forward slash, indenpendent from the system, so a
# windows path may look like: "C:/Program Files/LithoGraphX/bin"
#

cmake_minimum_required(VERSION 2.8.12)
cmake_policy(VERSION 2.8.12)

set(LithoGraphX_VERSION_STRING @LithoGraphX_VERSION@ CACHE INTERNAL "")
set(LithoGraphX_PROCESS_VERSION @LithoGraphX_PROCESS_VERSION@ CACHE INTERNAL "")
set(LithoGraphX_LIBRARIES LithoGraphX CACHE INTERNAL "")
set(LithoGraphX_HAS_CUDA @THRUST_BACKEND_CUDA@ CACHE INTERNAL "")
set(LithoGraphX_OS @LithoGraphX_OS@@LithoGraphX_OS_RELEASE@@LithoGraphX_CUDA@ CACHE INTERNAL "")
set(THRUST_BACKEND @THRUST_BACKEND@ CACHE INTERNAL "")

# First, try to find Python 3
find_package(PythonInterp 3)
if(NOT PYTHONINTERP_FOUND)
  find_package(PythonInterp)
endif()

get_filename_component(_LOCAL_PATH "${CMAKE_CURRENT_LIST_FILE}" PATH) # PREFIX/lib/cmake/LithoGraphX

get_filename_component(_IMPORT_PREFIX "${_LOCAL_PATH}" PATH) # PREFIX/lib/cmake
get_filename_component(_IMPORT_PREFIX "${_IMPORT_PREFIX}" PATH) # PREFIX/lib
get_filename_component(_IMPORT_PREFIX "${_IMPORT_PREFIX}" PATH) # PREFIX

list(APPEND LithoGraphX_SEARCH_PATHS
  ${_IMPORT_PREFIX}/bin
  @CMAKE_INSTALL_PREFIX/bin)

find_program(LithoGraphX_PROGRAM LithoGraphX)
if(NOT LithoGraphX_PROGRAM)
  find_program(LithoGraphX_PROGRAM LithoGraphX PATHS ${LithoGraphX_SEARCH_PATHS})
endif()

if(LithoGraphX_PROGRAM)
  get_filename_component(LithoGraphX_PROGRAM_PATH "${LithoGraphX_PROGRAM}" PATH)
  list(INSERT LithoGraphX_SEARCH_PATHS 0
    ${LithoGraphX_PROGRAM_PATH})

  set(LithoGraphX_lgxDescription ${_LOCAL_PATH}/lgxDescription.py)

  find_program(LithoGraphX_lgxPack lgxPack)
  if(NOT LithoGraphX_lgxPack)
    find_program(LithoGraphX_lgxPack lgxPack PATHS ${LithoGraphX_SEARCH_PATHS})
  endif()
endif()

if(NOT LithoGraphX_PROGRAM OR NOT LithoGraphX_lgxDescription OR NOT LithoGraphX_lgxPack)
  message(FATAL_ERROR "Cannot find LithoGraphX applications.")
endif()

execute_process(COMMAND "${LithoGraphX_PROGRAM}" --user-process OUTPUT_VARIABLE LithoGraphX_USER_PROCESS_DIR
  OUTPUT_STRIP_TRAILING_WHITESPACE ERROR_QUIET)
execute_process(COMMAND "${LithoGraphX_PROGRAM}" --process OUTPUT_VARIABLE LithoGraphX_PROCESS_DIR
  OUTPUT_STRIP_TRAILING_WHITESPACE ERROR_QUIET)
execute_process(COMMAND "${LithoGraphX_PROGRAM}" --user-macros OUTPUT_VARIABLE LithoGraphX_USER_MACRO_DIR
  OUTPUT_STRIP_TRAILING_WHITESPACE ERROR_QUIET)
execute_process(COMMAND "${LithoGraphX_PROGRAM}" --macros OUTPUT_VARIABLE LithoGraphX_MACRO_DIR
  OUTPUT_STRIP_TRAILING_WHITESPACE ERROR_QUIET)
execute_process(COMMAND "${LithoGraphX_PROGRAM}" --include OUTPUT_VARIABLE LithoGraphX_INCLUDE_DIR
  OUTPUT_STRIP_TRAILING_WHITESPACE ERROR_QUIET)
execute_process(COMMAND "${LithoGraphX_PROGRAM}" --user-libs OUTPUT_VARIABLE LithoGraphX_USER_LIB_DIR
  OUTPUT_STRIP_TRAILING_WHITESPACE ERROR_QUIET)
execute_process(COMMAND "${LithoGraphX_PROGRAM}" --libs OUTPUT_VARIABLE LithoGraphX_LIB_DIR
  OUTPUT_STRIP_TRAILING_WHITESPACE ERROR_QUIET)
execute_process(COMMAND "${LithoGraphX_PROGRAM}" --packages OUTPUT_VARIABLE LithoGraphX_PACKAGES_DIR
  OUTPUT_STRIP_TRAILING_WHITESPACE ERROR_QUIET)
execute_process(COMMAND "${LithoGraphX_PROGRAM}" --user-packages OUTPUT_VARIABLE LithoGraphX_USER_PACKAGES_DIR
  OUTPUT_STRIP_TRAILING_WHITESPACE ERROR_QUIET)

get_filename_component(LithoGraphX_BIN_DIR ${LithoGraphX_PROGRAM} PATH)

list(APPEND CMAKE_MODULE_PATH
  ${_LOCAL_PATH}
  ${LithoGraphX_LIB_DIR}/cmake)

message(STATUS "CMAKE_MODULE_PATH = ${CMAKE_MODULE_PATH}")

list(APPEND CMAKE_INCLUDE_PATH ${LithoGraphX_INCLUDE_DIR})
list(APPEND CMAKE_INCLUDE_PATH ${LithoGraphX_PROCESS_DIR}/include)
list(APPEND CMAKE_LIBRARY_PATH ${LithoGraphX_LIB_DIR})
list(APPEND CMAKE_LIBRARY_PATH ${LithoGraphX_PROCESS_DIR}/lib)
list(APPEND CMAKE_LIBRARY_PATH ${LithoGraphX_BIN_DIR})

find_package(Qt5 REQUIRED COMPONENTS Core Widgets Xml OpenGL PrintSupport)
find_package(OpenMP)
find_package(Eigen REQUIRED)
find_package(Thrust REQUIRED)
include(CMakeDependentOption)

set(Qt5_libraries Qt5::Core Qt5::Widgets Qt5::OpenGL Qt5::Xml Qt5::PrintSupport)
set(Qt5_includes)
foreach(lib ${Qt5_libraries})
  list(APPEND Qt5_includes $<TARGET_PROPERTY:${lib},INTERFACE_INCLUDE_DIRECTORIES$<ANGLE-R>)
endforeach()

macro(LGX_FIND_MODULE module required)
  if(NOT DEFINED LithoGraphX_${module}_FOUND)
    set(LithoGraphX_REQUESTED_${module} TRUE)
    if(LithoGraphX_HAS_${module})
      set(LithoGraphX_${module}_FOUND)
      set(LithoGraphX_${module}_INSTALL_TYPE builtin)
      set(LithoGraphX_${module}_VERSION @LithoGraphX_VERSION@)
      set(LithoGraphX_${module}_LIBRARIES LithoGraphX::${module})
    else()
      execute_process(COMMAND ${LithoGraphX_lgxPack} --installed ${module}
        OUTPUT_VARIABLE LithoGraphX_${module}_INSTALL_TYPE
        RESULT_VARIABLE LithoGraphX_${module}_INSTALL_TYPE_RESULT
        OUTPUT_STRIP_TRAILING_WHITESPACE
        ERROR_QUIET)
      set(LithoGraphX_${module}_INSTALL_TYPE "${LithoGraphX_${module}_INSTALL_TYPE}")
      if(LithoGraphX_${module}_INSTALL_TYPE_RESULT EQUAL 0)
        message(STATUS "${module}_INSTALL_TYPE = '${LithoGraphX_${module}_INSTALL_TYPE}'")
        if("${LithoGraphX_${module}_INSTALL_TYPE}" STREQUAL "user")
          set(LithoGraphX_${module}_MODULE_DIR "${LithoGraphX_USER_LIB_DIR}/cmake/LithoGraphX" CACHE STRING "Location of the CMake module")
          message(STATUS "User install for ${module}")
        else()
          set(LithoGraphX_${module}_MODULE_DIR "${LithoGraphX_LIB_DIR}/cmake/LithoGraphX" CACHE STRING "Location of the CMake module")
          message(STATUS "System install for ${module}")
        endif()
        message(STATUS "${module} MODULE_DIR = '${LithoGraphX_${module}_MODULE_DIR}'")
        set(LithoGraphX_${module}_LOCATION "${LithoGraphX_${module}_MODULE_DIR}/${module}.cmake" CACHE STRING "Path to the CMake module of ${module}.")
        if(EXISTS "${LithoGraphX_${module}_LOCATION}")
          set(LithoGraphX_${module}_FOUND TRUE)
          include(${LithoGraphX_${module}_LOCATION})
          if(LithoGraphX_${module}_FOUND)
            list(APPEND LithoGraphX_PACKAGES_FOUND ${module})
          endif()
        endif()
      endif()
    endif()
  endif()
  #if(NOT LithoGraphX_${module}_FOUND)
    #if(${required})
      #message(FATAL_ERROR "Cannot find required component ${module}")
    #else()
      #message(WARNING "Cannot find optional component ${module}")
    #endif()
  #endif()
endmacro(LGX_FIND_MODULE)

message(STATUS "LithoGraphX components: ${LithoGraphX_FIND_COMPONENTS}")

foreach(module ${LithoGraphX_FIND_COMPONENTS})
  lgx_find_module(${module} ${LithoGraphX_FIND_REQUIRED_${module}})
  #if(NOT LithoGraphX_${module}_FOUND)
    #message(FATAL_ERROR "This installation of LithoGraphX doesn't have any component called ${module}")
  #endif()
endforeach()

set(LithoGraphX_REQUESTED_COMPONENTS ${LithoGraphX_FIND_COMPONENTS} CACHE INTERNAL "")

if(NOT CMAKE_BUILD_TYPE AND NOT CMAKE_CONFIGURATION_TYPES)
  message(STATUS "Setting build type to '@CMAKE_BUILD_TYPE@' as none was specified.")
  set(CMAKE_BUILD_TYPE @CMAKE_BUILD_TYPE@ CACHE STRING "Choose the type of build: Debug Release MinSizeRel or RelWithDebInfo." FORCE)
  # Set the possible values of build type for cmake-gui
  set_property(CACHE CMAKE_BUILD_TYPE PROPERTY STRINGS "Debug" "Release"
    "MinSizeRel" "RelWithDebInfo")
endif()

option(LithoGraphX_MAKE_PACKAGE "If checked, a binary package will be created for later installation." ON)
cmake_dependent_option(LithoGraphX_USER_INSTALL "If checked, the plugin will be installed in the user folder instead of the system one" OFF
  "NOT LithoGraphX_MAKE_PACKAGE" OFF)

if(NOT TARGET LGXViewer)
  add_library(LGXViewer SHARED IMPORTED)
  set(LithoGraphX_LGXViewer_DIRECTORIES ${LithoGraphX_INCLUDE_DIR})
  set_property(TARGET LGXViewer PROPERTY
    INTERFACE_INCLUDE_DIRECTORIES ${LithoGraphX_LGXViewer_DIRECTORIES})

  $<$<NOT:$<BOOL:@WIN32@>>:
  set_property(TARGET LGXViewer PROPERTY
    IMPORTED_SONAME ${LithoGraphX_LIB_DIR}/$<TARGET_LINKER_FILE_NAME:LGXViewer>)>
  $<$<BOOL:@WIN32@>:
  set_property(TARGET LGXViewer PROPERTY
    IMPORTED_IMPLIB ${LithoGraphX_LIB_DIR}/$<TARGET_LINKER_FILE_NAME:LGXViewer>)>

  set_property(TARGET LGXViewer PROPERTY
    IMPORTED_LOCATION ${LithoGraphX_LIB_DIR}/$<TARGET_FILE_NAME:LGXViewer>)
  set_property(TARGET LGXViewer PROPERTY
    INTERFACE_LINK_LIBRARIES ${Qt5_libraries})
  set_property(TARGET LGXViewer PROPERTY
    INTERFACE_SYSTEM_INCLUDE_DIRECTORIES ${Qt5_includes})
endif()

set(LithoGraphX_SHARED_DIR $<$<BOOL:@WIN32@>:${LithoGraphX_BIN_DIR}>$<$<NOT:$<BOOL:@WIN32@>>:${LithoGraphX_LIB_DIR}>)

if(NOT TARGET LithoGraphX)
  add_library(LithoGraphX SHARED IMPORTED)
  set(LithoGraphX_USE_OPENMP @USE_OPENMP@)
  set(LithoGraphX_INCLUDE_DIRECTORIES
    ${LithoGraphX_INCLUDE_DIR})
  if(EXISTS "${LithoGraphX_PROCESS_DIR}/include")
    list(APPEND LithoGraphX_INCLUDE_DIRECTORIES ${LithoGraphX_PROCESS_DIR}/include)
  endif()
  if(EXISTS "${LithoGraphX_USER_PROCESS_DIR}/include")
    list(APPEND LithoGraphX_INCLUDE_DIRECTORIES "${LithoGraphX_USER_PROCESS_DIR}/include")
  endif()
  set(lgx_libraries ${Qt5_libraries})
  set(LithoGraphX_SYSTEM_INCLUDE_DIRECTORIES
    ${Eigen_INCLUDE_DIR}
    ${Qt5_includes}
    ${THRUST_INCLUDE_DIR})
  set(LithoGraphX_COMPILE_OPTIONS -std=c++11)
  if(LithoGraphX_USE_OPENMP)
    list(APPEND LithoGraphX_COMPILE_OPTIONS ${OpenMP_CXX_FLAGS})
  endif()
  set_property(TARGET LithoGraphX PROPERTY
    INTERFACE_INCLUDE_DIRECTORIES ${LithoGraphX_INCLUDE_DIRECTORIES} ${LithoGraphX_SYSTEM_INCLUDE_DIRECTORIES})
  set_property(TARGET LithoGraphX PROPERTY
    INTERFACE_SYSTEM_INCLUDE_DIRECTORIES ${LithoGraphX_SYSTEM_INCLUDE_DIRECTORIES})
  set_property(TARGET LithoGraphX PROPERTY
    INTERFACE_COMPILE_OPTIONS ${LithoGraphX_COMPILE_OPTIONS})

  $<$<BOOL:@WIN32@>:
  set_property(TARGET LithoGraphX PROPERTY
    IMPORTED_IMPLIB ${LithoGraphX_LIB_DIR}/$<TARGET_LINKER_FILE_NAME:LithoGraphX>)>
  $<$<NOT:$<BOOL:@WIN32@>>:
  set_property(TARGET LithoGraphX PROPERTY
    IMPORTED_SONAME ${LithoGraphX_LIB_DIR}/$<TARGET_SONAME_FILE_NAME:LithoGraphX>)>

  set_property(TARGET LithoGraphX PROPERTY
    IMPORTED_LOCATION ${LithoGraphX_SHARED_DIR}/$<TARGET_FILE_NAME:LithoGraphX>)
  set_property(TARGET LithoGraphX PROPERTY
    INTERFACE_LINK_LIBRARIES ${lgx_libraries} LGXViewer)
  set_property(TARGET LithoGraphX PROPERTY INTERFACE_POSITION_INDEPENDENT_CODE ON)
endif()

$<$<BOOL:@THRUST_BACKEND_CUDA@>:
if(LithoGraphX_REQUESTED_CUDA)
  set(THRUST_BACKEND "CUDA" CACHE INTERNAL "")
  if(NOT TARGET LithoGraphX::CUDA)
      add_library(LithoGraphX::CUDA SHARED IMPORTED)
      $<$<BOOL:@MINGW@>:
        set_property(TARGET LithoGraphX::CUDA PROPERTY
            IMPORTED_IMPLIB ${LithoGraphX_LIB_DIR}/lgx_cuda.lib)
        set_property(TARGET LithoGraphX::CUDA PROPERTY
            IMPORTED_LOCATION ${LithoGraphX_SHARED_DIR}/lgx_cuda.dll)
        set_property(TARGET LithoGraphX::CUDA PROPERTY INTERFACE_POSITION_INDEPENDENT_CODE ON)
      >
      set_property(TARGET LithoGraphX::CUDA PROPERTY
          INTERFACE_INCLUDE_DIRECTORIES ${CUDA_INCLUDE_DIRS})
      set_property(TARGET LithoGraphX::CUDA PROPERTY
          INTERFACE_LINK_LIBRARIES ${CUDA_LIBRARIES})
  endif()
else()
  find_package(OpenMP)
  if(OPENMP_FOUND)
    set(THRUST_BACKEND "OpenMP" CACHE INTERNAL "")
  else()
    set(THRUST_BACKEND "CPU" CACHE INTERNAL "")
  endif()
endif()
update_thrust_backend()
>

if(THRUST_BACKEND_CUDA)
  add_definitions(-DTHRUST_BACKEND_CUDA)
elseif(THRUST_BACKEND_OMP)
  add_definitions(-DTHRUST_BACKEND_OMP)
elseif(THRUST_BACKEND_CPP)
  add_definitions(-DTHRUST_BACKEND_CPP)
elseif(THRUST_BACKEND_TBB)
  add_definitions(-DTHRUST_BACKEND_TBB)
endif()

macro(LGX_DESCRIPTION which name required)
  execute_process(COMMAND ${PYTHON_EXECUTABLE} ${LithoGraphX_lgxDescription} --field=${which} "${CMAKE_CURRENT_SOURCE_DIR}/description.ini"
    OUTPUT_VARIABLE ${name}
    ERROR_VARIABLE lgxDesc_ERROR
    RESULT_VARIABLE lgxDesc_RESULT
    OUTPUT_STRIP_TRAILING_WHITESPACE)

  if(NOT lgxDesc_RESULT EQUAL 0)
    message(SEND_ERROR "Error, cannot run 'lgxDescription --field=${which}':\n${lgxDesc_ERROR}")
  elseif(${required} AND (${name} STREQUAL ""))
    message(SEND_ERROR "${which} is not defined in description file.")
  endif()
endmacro(LGX_DESCRIPTION)

macro(INIT_LGX_PACKAGE)
  # Read the description.ini file
  lgx_description(Name LithoGraphX_PACKAGE_NAME TRUE)
  project(${LithoGraphX_PACKAGE_NAME} CXX)
  lgx_description(Version ${LithoGraphX_PACKAGE_NAME}_VERSION TRUE)
  lgx_description(Description ${LithoGraphX_PACKAGE_NAME}_DESCRIPTION TRUE)
  lgx_description(Dependencies ${LithoGraphX_PACKAGE_NAME}_DEPENDENCIES FALSE)
endmacro(INIT_LGX_PACKAGE)

macro(LGX_PACKAGE)
  if("${LithoGraphX_PACKAGE_NAME}" STREQUAL "")
    message(FATAL_ERROR "The package name hasn't been defined. You should use the init_lgx_package() function.")
  endif()

  string(REPLACE "\\" "\\\\" ${LithoGraphX_PACKAGE_NAME}_DESCRIPTION "${${LithoGraphX_PACKAGE_NAME}_DESCRIPTION}")
  string(REPLACE "\"" "\\\"" ${LithoGraphX_PACKAGE_NAME}_DESCRIPTION "${${LithoGraphX_PACKAGE_NAME}_DESCRIPTION}")
  string(REPLACE "\n" "\\n" ${LithoGraphX_PACKAGE_NAME}_DESCRIPTION "${${LithoGraphX_PACKAGE_NAME}_DESCRIPTION}")
  string(REPLACE "\t" "\\t" ${LithoGraphX_PACKAGE_NAME}_DESCRIPTION "${${LithoGraphX_PACKAGE_NAME}_DESCRIPTION}")

  if(LithoGraphX_MAKE_PACKAGE)

    foreach(package ${LithoGraphX_PACKAGES_FOUND})
      list(APPEND ${LithoGraphX_PACKAGE_NAME}_DEPENDENCIES "${package} (== ${LithoGraphX_${package}_VERSION})")
    endforeach(package)

    string(REPLACE ";" " " LithoGraphX_${LithoGraphX_PACKAGE_NAME}_COMPONENTS "${LithoGraphX_REQUESTED_COMPONENTS}")

    string(REPLACE ";" ", " ${LithoGraphX_PACKAGE_NAME}_DEPENDENCIES "${${LithoGraphX_PACKAGE_NAME}_DEPENDENCIES}")
    if(NOT ("${${LithoGraphX_PACKAGE_NAME}_DEPENDENCIES}" STREQUAL ""))
      set(${LithoGraphX_PACKAGE_NAME}_DEPENDENCIES ", ${${LithoGraphX_PACKAGE_NAME}_DEPENDENCIES}")
    endif()

    configure_file("${LithoGraphX_PACKAGES_DIR}/description.ini.cmake" "${CMAKE_CURRENT_BINARY_DIR}/description.ini")
    set(${LithoGraphX_PACKAGE_NAME}_DESCRIPTION_FILE "${CMAKE_CURRENT_BINARY_DIR}/description.ini")
    install(FILES ${${LithoGraphX_PACKAGE_NAME}_DESCRIPTION_FILE}
      COMPONENT runtime
      DESTINATION .)

    # If the package has libs, generate the module
    if(${LithoGraphX_PACKAGE_NAME}_HAS_LIBS)
      # First, check a main lib has been defined
      if(NOT DEFINED ${LithoGraphX_PACKAGE_NAME}_MAIN_LIB)
        set(${LithoGraphX_PACKAGE_NAME}_MAIN_LIB ${${LithoGraphX_PACKAGE_NAME}_LAST_LIB})
        set(${LithoGraphX_PACKAGE_NAME}_LIB_TYPE ${${LithoGraphX_PACKAGE_NAME}_LAST_LIB_TYPE})
        list(REMOVE_AT ${LithoGraphX_PACKAGE_NAME}_SECONDARY_LIBRARIES -1)
      endif()
      foreach(elmt ${${LithoGraphX_PACKAGE_NAME}_SECONDARY_LIBRARIES})
        list(APPEND tmp_list
          "$<TARGET_LINKER_FILE_NAME:${elmt}$<ANGLE-R>")
      endforeach()
      set(${LithoGraphX_PACKAGE_NAME}_SECONDARY_LIBRARIES ${tmp_list})

      # Find Package.cmake
      find_file(LithoGraphX_${LithoGraphX_PACKAGE_NAME}_PACKAGE_FILE
        Package.cmake
        PATHS ${CMAKE_SOURCE_DIR} ${_LOCAL_PATH}
        NO_DEFAULT_PATH)

      if(NOT LithoGraphX_${LithoGraphX_PACKAGE_NAME}_PACKAGE_FILE)
        message(FATAL_ERROR "Cannot find file 'Package.cmake'. Check your installation.")
      endif()

      configure_file("${LithoGraphX_${LithoGraphX_PACKAGE_NAME}_PACKAGE_FILE}"
        "${CMAKE_CURRENT_BINARY_DIR}/Package.cmake")
      file(GENERATE OUTPUT "${CMAKE_CURRENT_BINARY_DIR}/${LithoGraphX_PACKAGE_NAME}.cmake"
        INPUT "${CMAKE_CURRENT_BINARY_DIR}/Package.cmake")
      install(FILES "${CMAKE_CURRENT_BINARY_DIR}/${LithoGraphX_PACKAGE_NAME}.cmake"
        COMPONENT runtime
        DESTINATION lib/cmake/LithoGraphX)
    endif()

    set(CPACK_PACKAGE_NAME "${LithoGraphX_PACKAGE_NAME}")
    string(REPLACE "." ";" split_version ${${LithoGraphX_PACKAGE_NAME}_VERSION})
    list(LENGTH split_version version_size)
    if(version_size EQUAL 3)
      list(GET split_version 0 CPACK_PACKAGE_VERSION_MAJOR)
      list(GET split_version 1 CPACK_PACKAGE_VERSION_MINOR)
      list(GET split_version 2 CPACK_PACKAGE_VERSION_PATCH)
      if(NOT ((CPACK_PACKAGE_VERSION_MAJOR GREATER 0) AND (CPACK_PACKAGE_VERSION_MAJOR LESS 255)))
        message(SEND_ERROR "Error, version's major must be between 1 and 254 inclusive.
        Current value: ${CPACK_PACKAGE_VERSION_MAJOR}")
      endif()
      if(NOT ((CPACK_PACKAGE_VERSION_MINOR GREATER -1) AND (CPACK_PACKAGE_VERSION_MINOR LESS 255)))
        message(SEND_ERROR "Error, version's minor must be between 0 and 254 inclusive.
        Current value: ${CPACK_PACKAGE_VERSION_MINOR}")
      endif()
      if(NOT ((CPACK_PACKAGE_VERSION_PATCH GREATER -1) AND (CPACK_PACKAGE_VERSION_PATCH LESS 255)))
        message(SEND_ERROR "Error, version's patch must be between 0 and 254 inclusive.
        Current value: ${CPACK_PACKAGE_VERSION_PATCH}")
      endif()
    else()
      message(SEND_ERROR "Error, the version must be written as MAJOR.MINOR.PATCH (e.g. 1.2.3)")
    endif()
    set(CPACK_PACKAGE_VERSION ${${LithoGraphX_PACKAGE_NAME}_VERSION})

    include(CPack)
  endif()
endmacro()

macro(LGX_ADD_LIBRARY target type)
  if("${LithoGraphX_PACKAGE_NAME}" STREQUAL "")
    message(FATAL_ERROR "The package name hasn't been defined. You should start the CMakeLists with init_lgx_package().")
  endif()

  set(${LithoGraphX_PACKAGE_NAME}_HAS_LIBS TRUE)

  set(current_var ${target}_sources)
  set(is_main FALSE)
  foreach(value ${ARGN})
    if("${value}" STREQUAL "PUBLIC_HEADER")
      set(current_var ${target}_headers)
    elseif("${value}" STREQUAL "MAIN")
      set(is_main TRUE)
    else()
      list(APPEND ${current_var} ${value})
    endif()
  endforeach()
  if(is_main)
    if(DEFINED ${LithoGraphX_PACKAGE_NAME}_MAIN_LIB})
      list(APPEND ${LithoGraphX_PACKAGE_NAME}_SECONDARY_LIBRARIES
        ${LithoGraphX_PACKAGE_NAME}_MAIN_LIB)
    endif()
    set(${LithoGraphX_PACKAGE_NAME}_MAIN_LIB ${target})
    set(${LithoGraphX_PACKAGE_NAME}_LIB_TYPE ${type})
  else()
    list(APPEND ${LithoGraphX_PACKAGE_NAME}_SECONDARY_LIBRARIES
      ${target})
  endif()
  set(${LithoGraphX_PACKAGE_NAME}_LAST_LIB_TYPE ${type})
  set(${LithoGraphX_PACKAGE_NAME}_LAST_LIB ${target})

  add_library(${target} ${type} ${${target}_sources} ${${target}_headers})
  target_link_libraries(${target} LithoGraphX)
  if(LithoGraphX_USE_OPENMP)
    set_property(TARGET ${target} APPEND PROPERTY COMPILE_FLAGS ${OpenMP_CXX_FLAGS})
    set_property(TARGET ${target} APPEND PROPERTY LINK_FLAGS ${OpenMP_CXX_FLAGS})
  endif()
  set(LIB_INSTALL_DIR lib)
  set(INCLUDE_INSTALL_DIR include)
  set(PROCESS_INSTALL_DIR processes)
  if(NOT LithoGraphX_MAKE_PACKAGE)
    if(LithoGraphX_USER_INSTALL)
      set(LIB_INSTALL_DIR ${LithoGraphX_USER_LIB_DIR})
      set(PROCESS_INSTALL_DIR ${LithoGraphX_USER_PROCESS_DIR})
    else()
      set(LIB_INSTALL_DIR ${LithoGraphX_LIB_DIR})
      set(PROCESS_INSTALL_DIR ${LithoGraphX_PROCESS_DIR})
    endif()
  endif()
  install(TARGETS ${target}
    COMPONENT runtime
    DESTINATION ${LIB_INSTALL_DIR})
  install(FILES ${${target}_headers}
    COMPONENT runtime
    DESTINATION ${PROCESS_INSTALL_DIR}/include/${LithoGraphX_PACKAGE_NAME})
endmacro(LGX_ADD_LIBRARY)

macro(LGX_FIND_PACKAGE)
  if("${LithoGraphX_PACKAGE_NAME}" STREQUAL "")
    message(FATAL_ERROR "The package name hasn't been defined. You should start the CMakeLists with init_lgx_package().")
  endif()

  find_package(${ARGN})
  string(REPLACE ";" " " package_args "${ARGN}")
  list(APPEND ${LithoGraphX_PACKAGE_NAME}_FIND_PACKAGES
    "find_package(${package_args})")
endmacro(LGX_FIND_PACKAGE)

macro(LGX_ADD_PLUGIN plugin_name)
  add_library(${plugin_name} MODULE ${ARGN})
  target_link_libraries(${plugin_name} LithoGraphX)
  set_property(TARGET ${plugin_name} PROPERTY PREFIX "")
  if(LithoGraphX_USE_OPENMP)
    set_property(TARGET ${plugin_name} APPEND PROPERTY COMPILE_FLAGS ${OpenMP_CXX_FLAGS})
    set_property(TARGET ${plugin_name} APPEND PROPERTY LINK_FLAGS ${OpenMP_CXX_FLAGS})
  endif()
  set(PROCESS_INSTALL_DIR processes)
  if(NOT LithoGraphX_MAKE_PACKAGE)
    if(LithoGraphX_USER_INSTALL)
      set(PROCESS_INSTALL_DIR ${LithoGraphX_USER_PROCESS_DIR})
    else()
      set(PROCESS_INSTALL_DIR ${LithoGraphX_PROCESS_DIR})
    endif()
  endif()
  install(TARGETS ${plugin_name}
    COMPONENT runtime
    DESTINATION ${PROCESS_INSTALL_DIR})
endmacro(LGX_ADD_PLUGIN)

macro(LGX_ADD_MACRO)
  set(current_var macro_files)
  set(macro_stripped FALSE)
  set(macro_base_dir "")
  foreach(arg ${ARGV})
    if("${arg}" STREQUAL "STRIP_DIR")
      set(macro_stripped TRUE)
      set(current_var macro_base_dir)
    else()
      list(APPEND ${current_var} "${arg}")
    endif()
  endforeach(arg)

  if(macro_stripped)
    list(LENGTH macro_base_dir macro_base_dir_size)

    if(NOT macro_base_dir_size EQUAL 1)
      message(SEND_ERROR "Error, you must specify exactly one folder after STRIP_DIR")
    endif()

    string(LENGTH "${macro_base_dir}" macro_base_dir_size)
    math(EXPR mbd_last_char_pos ${macro_base_dir_size}-1)
    string(SUBSTRING macro_base_dir ${mbd_last_char_pos} 1 mbd_last_char)

    if(NOT mbd_last_char STREQUAL "/")
      set(macro_base_dir "${macro_base_dir}/")
      string(LENGTH "${macro_base_dir}" macro_base_dir_size)
    endif()

    foreach(arg ${macro_files})
      string(SUBSTRING "${arg}" 0 ${macro_base_dir_size} arg_prefix)
      if(NOT "${arg_prefix}" STREQUAL "${macro_base_dir}")
        message(SEND_ERROR "Error, macro '${arg}' is not in the striped folder '${macro_base_dir}' -- prefix = ${arg_prefix}.")
      endif()
    endforeach()
  endif()

  set(MACRO_INSTALL_DIR macros)
  if(NOT LithoGraphX_MAKE_PACKAGE)
    if(LithoGraphX_USER_INSTALL)
      set(MACRO_INSTALL_DIR ${LithoGraphX_USER_MACRO_DIR})
    else()
      set(MACRO_INSTALL_DIR ${LithoGraphX_MACRO_DIR})
    endif()
  endif()

  foreach(arg ${macro_files})
    if(macro_stripped)
      string(SUBSTRING "${arg}" ${macro_base_dir_size} -1 stripped_arg)
    else()
      set(stripped_arg "${arg}")
    endif()
    get_filename_component(arg_dir ${stripped_arg} PATH)
    get_filename_component(dest_dir "${arg_dir}" ABSOLUTE BASE_DIR ${MACRO_INSTALL_DIR})
    get_filename_component(arg_name ${stripped_arg} NAME)
    install(FILES ${arg} COMPONENT runtime DESTINATION ${dest_dir})
  endforeach()
endmacro(LGX_ADD_MACRO)

mark_as_advanced(
  LithoGraphX_lgxDescription
  LithoGraphX_lgxPack
  )

find_package(PackageHandleStandardArgs)
find_package_handle_standard_args(LithoGraphX
  REQUIRED_VARS LithoGraphX_PROGRAM LithoGraphX_lgxDescription LithoGraphX_lgxPack
  VERSION_VAR LithoGraphX_VERSION_STRING
  HANDLE_COMPONENTS
)
