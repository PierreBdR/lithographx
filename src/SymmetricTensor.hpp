/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef SYMMETRIC_TENSOR_HPP
#define SYMMETRIC_TENSOR_HPP

#include <QTextStream>
#include <Geometry.hpp>

namespace lgx {
namespace util {

class LGX_EXPORT SymmetricTensor {
public:
  SymmetricTensor()
    : _ev1(1, 0, 0)
    , _ev2(0, 1, 0)
    , _evals(1, 1, 1)
  {
  }

  SymmetricTensor(const Point3f& ev1, const Point3f& ev2, const Point3f& evals)
    : _ev1(ev1)
    , _ev2(ev2)
    , _evals(evals)
  {
  }

  SymmetricTensor(const SymmetricTensor& copy)
    : _ev1(copy._ev1)
    , _ev2(copy._ev2)
    , _evals(copy._evals)
  {
  }

  SymmetricTensor& operator=(const SymmetricTensor& other)
  {
    _ev1 = other._ev1;
    _ev2 = other._ev2;
    _evals = other._evals;
    return *this;
  }

  Point3f& ev1() {
    return _ev1;
  }
  Point3f& ev2() {
    return _ev2;
  }
  const Point3f& ev1() const {
    return _ev1;
  }
  const Point3f& ev2() const {
    return _ev2;
  }
  Point3f ev3() const {
    return _ev1 ^ _ev2;
  }
  const Point3f& evals() const {
    return _evals;
  }
  Point3f& evals() {
    return _evals;
  }

protected:
  Point3f _ev1, _ev2;
  Point3f _evals;
};

QTextStream& operator<<(QTextStream& ts, const SymmetricTensor& tensor);
} // namespace util
} // namespace lgx
#endif
