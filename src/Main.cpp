/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "Dir.hpp"
#include "Information.hpp"
#include "LGXVersion.hpp"
#include "LithoGraphX.hpp"
#include "Random.hpp"
#include "SystemDirs.hpp"
#include "Thrust.hpp"
#include "PackageArchive.hpp"
#include "PackageManager.hpp"

#include <QApplication>
#include <QString>
#include <QGLFormat>
#include <QDir>
#include <QFileInfo>
#include <QMetaType>
#include <QTimer>
#include <QMessageBox>
#include <QStringBuilder>
#include <QtGlobal>

using lgx::Information::out;

enum class ToDo
{
  Run = 0,
  AllMacrosDirs,
  AllPackagesDirs,
  AllProcessesDirs,
  AppDir,
  IncludeDir,
  LibsDir,
  SystemMacrosDir,
  SystemPackagesDir,
  SystemProcessesDir,
  UserLibsDir,
  UserMacrosDir,
  UserPackagesDir,
  UserProcessesDir,
#ifdef WIN32
  SetBuilder,
  Builder,
#endif
  Version,
  OS,
  Help,
};

void usage()
{
  out << "Usage: LithoGraphX OPTION [FILE]\n"
         "    --all-macros    - Print all the directories searched for processes and exit\n"
         "    --all-packages  - Print all the directories searched for packages and exit\n"
         "    --all-process   - Print all the directories searched for processes and exit\n"
         "    --debug         - Launch LithoGraphX in debug mode\n"
         "    --dir           - Print the application directory and exit\n"
         "    --include       - Print the include directory and exit\n"
         "    --libs          - Print the lib directory and exit\n"
         "    --macros        - Print the system macro directory and exit\n"
         "    --packages      - Print the system package directory and exit\n"
         "    --process       - Print the system process directory and exit\n"
         "    --user-libs     - Print the user lib directory and exit\n"
         "    --user-macros   - Print the user macro directory and exit\n"
         "    --user-packages - Print the user package directory and exit\n"
         "    --user-process  - Print the user process directory and exit\n"
         "    --version       - Display the version and revision and exit\n"
         "    --OS            - Display the operating system this version of LithoGraphX has been compiled for\n"
#ifdef WIN32
         "    --set-builder   - Change the script used to set the builder environment variables.\n"
         "    --builder       - Display the path to the builder's script.\n"
#endif

         "    --help |-h     - Print this help" << endl;
}

int main(int argc, char** argv)
{
  lgx::Information::out << "Starting " LGX_APPNAME " " VERSION << endl;
  QCoreApplication::setOrganizationDomain(LGX_DOMAINNAME);
  QCoreApplication::setApplicationName(LGX_APPNAME);
  QCoreApplication::setApplicationVersion(VERSION);

  bool is_debug = false;
  ToDo toDo = ToDo::Run;
#if defined(WIN32) || defined(WIN64)
  bool associate_files = false;
#endif

  auto todo = [&toDo](ToDo other) -> void {
    if(toDo != ToDo::Run) {
      lgx::Information::err << "Error, only one option can be specified on the command line." << endl;
      ::exit(2);
    }
    toDo = other;
  };

  QString filename;
  // QStringList args = app->arguments();
  for(int i = 1; i < argc; ++i) {
    QString arg = QString::fromLocal8Bit(argv[i]);
    if(arg == "--debug")
      is_debug = true;
    else if((arg == "--help")or (arg == "-h")) {
      todo(ToDo::Help);
    } else if(arg == "--dir") {
      todo(ToDo::AppDir);
    } else if(arg == "--process") {
      todo(ToDo::SystemProcessesDir);
    } else if(arg == "--user-process") {
      todo(ToDo::UserProcessesDir);
    } else if(arg == "--all-process") {
      todo(ToDo::AllProcessesDirs);
    } else if(arg == "--packages") {
      todo(ToDo::SystemPackagesDir);
    } else if(arg == "--user-packages") {
      todo(ToDo::UserPackagesDir);
    } else if(arg == "--all-packages") {
      todo(ToDo::AllPackagesDirs);
    } else if(arg == "--macros") {
      todo(ToDo::SystemMacrosDir);
    } else if(arg == "--user-libs") {
      todo(ToDo::UserLibsDir);
    } else if(arg == "--user-macros") {
      todo(ToDo::UserMacrosDir);
    } else if(arg == "--all-macros") {
      todo(ToDo::AllMacrosDirs);
    } else if(arg == "--include") {
      todo(ToDo::IncludeDir);
    } else if(arg == "--libs") {
      todo(ToDo::LibsDir);
#ifdef WIN32
    } else if(arg == "--set-builder") {
      todo(ToDo::SetBuilder);
    } else if(arg == "--builder") {
      todo(ToDo::Builder);
#endif
    } else if(arg == "--version") {
      todo(ToDo::Version);
    } else if(arg == "--OS") {
      todo(ToDo::OS);
    } else if(arg.startsWith("--")) {
      lgx::Information::err << "Unknown option: '" << arg << "'" << endl;
      usage();
      return 2;
    } else if(filename.isEmpty()) {
      filename = arg;
    }
  }

  QCoreApplication* app = 0;
  switch(toDo) {
    case ToDo::Run:
      if(not filename.isEmpty())
        filename = lgx::util::absoluteFilePath(filename);
      lgx::Information::out << "Creating QApplication" << endl;
      app = new QApplication(argc, argv);
      break;
#ifdef WIN32
    case ToDo::SetBuilder:
      if(filename.isEmpty()) {
        lgx::Information::err << "Error, you must specify the path to a batch script." << endl;
        return 1;
      }
      filename = lgx::util::absoluteFilePath(filename);
    case ToDo::Builder:
#endif
    case ToDo::AllMacrosDirs:
    case ToDo::AllPackagesDirs:
    case ToDo::AllProcessesDirs:
    case ToDo::AppDir:
    case ToDo::Help:
    case ToDo::IncludeDir:
    case ToDo::LibsDir:
    case ToDo::SystemMacrosDir:
    case ToDo::SystemPackagesDir:
    case ToDo::SystemProcessesDir:
    case ToDo::UserLibsDir:
    case ToDo::UserMacrosDir:
    case ToDo::UserPackagesDir:
    case ToDo::UserProcessesDir:
    case ToDo::Version:
    case ToDo::OS:
      if(not filename.isEmpty()) {
        lgx::Information::err << "Error, you cannot specify a filename with this option." << endl;
        return 2;
      }
      app = new QCoreApplication(argc, argv);
      break;
  }

  switch(toDo) {
    case ToDo::Help:
      usage();
      return 0;
    case ToDo::AppDir:
      out << QCoreApplication::applicationDirPath() << endl;
      return 0;
    case ToDo::SystemProcessesDir:
      out << lgx::util::systemProcessesDir().absolutePath() << endl;
      return 0;
    case ToDo::UserProcessesDir:
      out << lgx::util::userProcessesDir(true).absolutePath() << endl;
      return 0;
    case ToDo::AllProcessesDirs: {
        QList<QDir> dirs = lgx::util::processesDirs();
        forall(const QDir& dir, dirs) {
          out << dir.absolutePath() << endl;
        }
      }
      return 0;
    case ToDo::SystemMacrosDir:
      out << lgx::util::systemMacroDir().absolutePath() << endl;
      return 0;
    case ToDo::UserMacrosDir:
      out << lgx::util::userMacroDir(true).absolutePath() << endl;
      return 0;
    case ToDo::AllMacrosDirs: {
        QList<QDir> dirs = lgx::util::macroDirs();
        forall(const QDir& dir, dirs) {
          out << dir.absolutePath() << endl;
        }
      }
      return 0;
    case ToDo::SystemPackagesDir:
      out << lgx::util::systemPackagesDir().absolutePath() << endl;
      return 0;
    case ToDo::UserPackagesDir:
      out << lgx::util::userPackagesDir(true).absolutePath() << endl;
      return 0;
    case ToDo::AllPackagesDirs: {
        QList<QDir> dirs = lgx::util::packagesDirs();
        forall(const QDir& dir, dirs) {
          out << dir.absolutePath() << endl;
        }
      }
      return 0;
    case ToDo::IncludeDir:
      out << lgx::util::includesDir().absolutePath() << endl;
      return 0;
    case ToDo::LibsDir:
      out << lgx::util::libsDir().absolutePath() << endl;
      return 0;
    case ToDo::UserLibsDir:
      out << lgx::util::userLibsDir().absolutePath() << endl;
      return 0;
    case ToDo::Version:
      out << lgxVersion.toString() << endl;
      return 0;
    case ToDo::OS:
      out << LithoGraphX_OS << endl;
      return 0;
#ifdef WIN32
    case ToDo::Builder:
      out << lgx::findLGXBuilder() << endl;
      return 0;
    case ToDo::SetBuilder:
      if(lgx::setBuilderEnvironment(filename))
        return 0;
      return 1;
#endif
    case ToDo::Run:
      {
        QString currdir = QFileInfo(filename).dir().path();
        if(currdir.length() > 0)
          lgx::util::setCurrentPath(currdir);
      }
      break; // Nothing to do, we just go on
  }

#ifdef WIN32
  {
    auto kernel32 = LoadLibrary("kernel32.dll");
    auto AddDllDirectory = (DLL_DIRECTORY_COOKIE  WINAPI (*)(_In_ PCWSTR))GetProcAddress(kernel32, "AddDllDirectory");
    auto SetDefaultDllDirectories = (BOOL WINAPI (*)(_In_ DWORD)) GetProcAddress(kernel32, "SetDefaultDllDirectories");
    if(not AddDllDirectory or not SetDefaultDllDirectories) {
        lgx::Information::err << "Error, cannot find functions AddDllDirectory and SetDefaultDllDirectories." << endl;
    } else {
        lgx::Information::err << "Adding processes libs path to search path." << endl;
        auto sysLib = lgx::util::libsDir().absolutePath().toStdWString();
        auto sysProcLib = lgx::util::systemProcessesDir().absoluteFilePath("lib").toStdWString();
        auto userLib = lgx::util::userLibsDir().absolutePath().toStdWString();
        //SetDefaultDllDirectories(LOAD_LIBRARY_SEARCH_USER_DIRS | LOAD_LIBRARY_SEARCH_APPLICATION_DIR |
        //                         LOAD_LIBRARY_SEARCH_DEFAULT_DIRS | LOAD_LIBRARY_SEARCH_SYSTEM32);
        AddDllDirectory(sysLib.c_str());
        AddDllDirectory(sysProcLib.c_str());
        AddDllDirectory(userLib.c_str());
    }
  }
#endif

  // Unfortunately, once you turn multisampling on, it seems to be impossible to turn it off.
  // This screws up selection by color.
  QGLFormat format = QGLFormat::defaultFormat();
  format.setSampleBuffers(false);
  QGLFormat::setDefaultFormat(format);

  // Register extra meta type
  qRegisterMetaType<floatList>("floatList");
  qRegisterMetaTypeStreamOperators<floatList>("floatList");

  lgx::DEBUG = is_debug;
  out << "Welcome to LithoGraphX!" << endl << endl;
  out << "Thrust host evaluation: " << THRUST_TAG << endl << endl;

  // Create the user processes and macros dirs
  lgx::util::userProcessesDir(true);
  lgx::util::userMacroDir(true);

// Add path for ITK Loci tools import
#ifdef UNIX // This is UNIX-only code!!
  QDir dir = resourcesDir();
  out << "Loci tools ITK plugin (bf-itk): ";
  if(dir.cd("bf-itk")) {
    out << "found" << endl;
    QString dirName(QDir::toNativeSeparators(dir.canonicalPath()));
    QString path(getenv("ITK_AUTOLOAD_PATH"));
    if(path.length() == 0)
      setenv("ITK_AUTOLOAD_PATH", dirName.toStdString().data(), 1);
    else
#  if defined(WIN32) || defined(WIN64)
      setenv("ITK_AUTOLOAD_PATH", (dirName + ";" + path).toStdString().data(), 1);
#  else
      setenv("ITK_AUTOLOAD_PATH", (dirName + ":" + path).toStdString().data(), 1);
#  endif
  } else
    out << "not found" << endl;
#endif

  {
      // Update the path to include the processes folder
      QList<QDir> procDirs = lgx::util::processesDirs();
      QStringList procPaths;
      for(const QDir& dir: procDirs) {
          procPaths << QDir::toNativeSeparators(dir.absoluteFilePath("lib"));
      }
#ifdef WIN32
      QString path_sep = ";";
#else
      QString path_sep = ":";
#endif
      //lgx::Information::out << "Adding to PATH:" << procPaths.join(path_sep) << endl;
      auto env = QProcessEnvironment::systemEnvironment();
      QString path = procPaths.join(path_sep) + path_sep + env.value("PATH");
      //lgx::Information::out << "New PATH:\n" << path << endl;
#ifdef WIN32
      SetEnvironmentVariable("PATH", path.toUtf8().data());
#else
      env.insert("PATH", path);
#endif
  }

  // Initialize random numbers generator
  lgx::util::sran_time();

  QPointer<LithoGraphX> gui = new LithoGraphX(QCoreApplication::applicationDirPath());
  QObject::connect(gui, SIGNAL(destroyed()), app, SLOT(quit()));
  gui->setDebug(is_debug);
  gui->show();
  if(filename.toLower().endsWith(".lgxp") or filename.toLower().endsWith(".mgxv"))
    gui->loadProject(filename, false);
  else {
    gui->setLoadFile(filename);
    QObject::connect(gui, SIGNAL(processFinished()), gui, SLOT(autoOpen()));
    gui->initProject();
  }

  // Program a show for when the GUI is ready
  // QTimer::singleShot(0, gui, SLOT(show()));
  int result = app->exec();
  delete app;
  if(result == 0)
    out << "Bye bye!" << endl;
  else
    out << "Oops, didn't intend to quit?" << endl;
  return result;
}
