/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef __FUNCTION_HPP__
#define __FUNCTION_HPP__
/**
 * \file Function.hpp
 *
 * Defines the Function class
 */

#include <LGXConfig.hpp>

#include <string>
#include <vector>
#include <Vector.hpp>

namespace lgx {
namespace util {
/**
 * \class Function Function.hpp <Function.hpp>
 * \brief A utility class for functions.
 *
 * This class is a function object that encapsulates functions
 * in the VLAB function formats (original and fver 1 1).
 */
class Function {
public:
  Function();
  Function(std::string filename);
  Function(const Function& copy);
  Function& operator=(const Function&);
  double operator()(double x);
  const Vector<2, double>& getMax() const;
  const Vector<2, double>& getMin() const;
  void reread();
  void setSamples(size_t n);
  bool error();
  /**
   * Scale the x axis by s
   * \param s Scaling factor to apply to the axis
   */
  void scaleX(double s) {
    scaling_x = 1 / s;
  }
  /**
   * Scale the y axis by s
   * \param s Scaling factor to apply to the axis
   */
  void scaleY(double s) {
    scaling_y = s;
  }
  /// Get the current x scaling
  double scaleX() const {
    return 1 / scaling_x;
  }
  /// Get the current y scaling
  double scaleY() const {
    return scaling_y;
  }
  /**
   * Shift the x axis by s
   * \param s Shift of the axis
   *
   * Note that the shift happens after the scaling, so it should be written
   * in the scaled reference system.
   */
  void shiftX(double s) {
    shift_x = -s;
  }
  /**
   * Shift the y axis by s
   * \param s Shift of the axis
   *
   * Note that the shift happens after the scaling, so it should be written
   * in the scaled reference system.
   */
  void shiftY(double s) {
    shift_y = s;
  }
  /// Get the current x axis shift
  double shiftX() const {
    return -shift_x;
  }
  /// Get the current y axis shift
  double shiftY() const {
    return shift_y;
  }
  void normalizeY(bool shift = true);
  void normalizeX(bool shift = true);

private:
  std::string filename;
  std::vector<Vector<2, double> > pts;
  Vector<2, double> max;
  Vector<2, double> min;
  unsigned int samples;
  double scaling_x, scaling_y;
  double shift_x, shift_y;

  Vector<2, double> P(double x) const;
  double N(int, int, double) const;
  double Nk1(int, double) const;
  double Nkt(int, int, double) const;
  int Uk(int) const;
  double getVal(double x) const;   // computes the actual value for a given x

  struct CacheVal {
    bool valid;
    double val;
  };
  bool cache_valid;              // whether the cache vector is resized properly or not
  std::vector<CacheVal> cache;   // stores the cached values
  void init();                   // should be executed by every constructor
  bool error_occured;            // when loading this indicates whether there was an error
};
} // namespace util
} // namespace lgx

#endif
