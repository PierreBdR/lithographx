/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "CellAtlas.hpp"
#include <math.h>
#include "Geometry.hpp"
#include <limits>
#include "Progress.hpp"

#include <QFileDialog>

using namespace std;

namespace mgx
{
namespace process
{
// typedefs of cellgraph3d
typedef Vector<3, float> Vec3f;
typedef Vector<3, int> Vec3i;
typedef std::map<int, Vec3f> IntVec3fMap;
typedef std::map<Vec3f, int> Vec3fIntMap;
typedef std::pair<Vec3f, int> Vec3fIntPair;
typedef std::map<Vec3i, IntSet> Vec3iIntSetMap;
typedef std::pair<Vec3i, IntSet> Vec3iIntSetPair;
typedef std::map<int, double > IntDoubleMap;

// Put triangle in standard form
// (from the compute 3d graph plugin)
Vec3i triIndex(Vec3i t)
{
  if(t.x() > t.y())
    std::swap(t.x(), t.y());
  if(t.y() > t.z())
    std::swap(t.y(), t.z());
  if(t.x() > t.y())
    std::swap(t.x(), t.y());
  return t;
}

// Get vertex index, combine positions closer that the tolerance
// (from the compute 3d graph plugin)
int vIndex(const Vec3f &pos, Vec3fIntMap &vMap, float tolerance)
{
  if(vMap.count(pos) > 0)
    return vMap[pos];
  else if(vMap.empty() or tolerance <= 0)
    return (vMap[pos] = vMap.size());
  else {
    Vec3f p = pos;
    p.x() -= tolerance;
    Vec3fIntMap::iterator it = vMap.lower_bound(p);
    if(it == vMap.end())
      --it;
    while(it != vMap.end() and it->first.x() - pos.x() <= tolerance) {
      if(norm(pos - it->first) <= tolerance)
        return it->second;
      ++it;
    }
    return (vMap[pos] = vMap.size());
  }
}

// create the neighbourhood graph (copied from the compute 3d graph plugin)
bool neighbourhoodGraph(Mesh *m1, const vvgraph& S, /*VVGraphVec& cellVertex, VIntMap& vertexCell, */double tolerance, RootCellProcessing& rootDataBox)
{
  Progress progress("Analyze Cells 3D - Create neighbourhood graph", 0);

  labelDataMap cellWallArea;

  Vec3fIntMap vMap;
  Vec3iIntSetMap triCell; // This is set of cells for each triangle
  forall(const vertex& v, S) {
    forall(const vertex& n, S.neighbors(v)) {
      if(!progress.advance(1)) return false;
      const vertex& m = S.nextTo(v,n);
      if(!m1->uniqueTri(v,n,m)) // Only once per triangle in the mesh
        continue;
      // If the labels are not the same, this means we have more than one label on the same cell
      if(v->label != n->label or v->label != m->label)
        throw QString("Error, the mesh has more than one label on a cell.");
      int cell = v->label;//vertexCell[v];

      //float vol = signedTetraVolume(v->pos, n->pos, m->pos);

      cellWallArea[cell]+=triangleArea(v->pos,n->pos, m->pos);
      // Add the cell to the triangle map, points with similar positions will get the same index
      triCell[triIndex(Vec3i(vIndex(v->pos, vMap, tolerance), vIndex(n->pos, vMap, tolerance),
                             vIndex(m->pos, vMap, tolerance)))].insert(cell);
    }
  }

  // Generate opposite map, vertex indices to positions
  IntVec3fMap pMap;
  forall(const Vec3fIntPair &pr, vMap)
    pMap[pr.second] = pr.first;

  // Now caluclate wall areas from triangle list
  Vec2iFloatMap wallArea;
  forall(const Vec3iIntSetPair &pr, triCell) {
    if(pr.second.size() > 2) {
      Information::out << "Error, triangle belongs to more than 2 cells:";
      forall(int cell, pr.second)
        Information::out << " " << cell;
      Information::out << " Area:" << triangleArea(pMap[pr.first.x()],pMap[pr.first.y()], pMap[pr.first.z()]) << endl;
      continue;

      //throw QString("Error, triangle belongs to more that 2 cells.");
    } else if(pr.second.size() != 2)
      continue;

    IntSet::const_iterator cell1 = pr.second.begin();
    IntSet::const_iterator cell2 = cell1;
    cell2++;
    float area = triangleArea(pMap[pr.first.x()],pMap[pr.first.y()], pMap[pr.first.z()]);
    wallArea[Vec2i(*cell1, *cell2)] += area;
    wallArea[Vec2i(*cell2, *cell1)] += area;
  }
  //std::cout << "wallArea size:" << wallArea.size() << std::endl;

  rootDataBox.rootData.wallArea = wallArea;
  rootDataBox.rootData.cellWallArea = cellWallArea;

  return true;
}

// plugin to analyze the cell properties
bool AnalyzeCells::operator()(const Stack *s1, const Stack *s2, Mesh *m1, Mesh *m2, RootCellProcessing& rcp, double minVolume)
{
  Progress progress("Analyze Cells 3D - Preparation", 0);

  // meshes
  const vvgraph& segmentedMesh = m1->graph();
  const vvgraph& surfaceMesh = m2->graph();

  VVGraphVec cellVertex;
  VIntMap vertexCell;

  int selectedLabel1 = 0;

  // get selected cells
  progress.restart("Analyze Cells 3D - Find Selected Cell", 0);
  forall(const vertex &v, segmentedMesh) {
    if(!progress.advance(1)) userCancel();
    if(v->selected){
      selectedLabel1 = v->label;
    }
  }
  cout << "Selected Cell: " << selectedLabel1 << endl;
  if(selectedLabel1 == 0){
    setErrorMessage("Warning: no first cell selected");
    return false;
  }

  // prepare data structures
  //////////////////////////
  progress.restart("Analyze Cells 3D - Interpolate Bezier", 0);
  m1->getConnectedRegions(segmentedMesh, cellVertex, vertexCell);


  // Bezier
  CuttingSurface* cutSurf = cuttingSurface();

  Matrix4d rotMatrixS1, rotMatrixCS;
  s1->getFrame().getMatrix(rotMatrixS1.data());
  cutSurf->frame().getMatrix(rotMatrixCS.data());

  ////////// this part was included after many hours of trying to fix the rotations
  Matrix4d mGLTot = transpose(inverse(rotMatrixS1)) * transpose(rotMatrixCS);

  // create bezier vector and its derivative
  bezierMap bMap,diffbMap;
  double bezStart = 0.0;
  double bezEnd = 1.0;
  int dataPointsBezier = 200; // number of intermediate points
  double stepSize = (double)(bezEnd-bezStart)/(double)(dataPointsBezier-1);

  for(int i=0; i<dataPointsBezier; i++){
    double u = bezStart+i*stepSize;
    Point3d p = cutSurf->evalCoord(0,u);
    p = multMatrix4Point3(mGLTot,p); // correct rotations
    bMap[i] = p;
  }
  diffbMap[0] = bMap[1] - bMap[0];
  diffbMap[dataPointsBezier-1] = bMap[dataPointsBezier-1] - bMap[dataPointsBezier-2];
  for(int i=1; i<dataPointsBezier-1; i++){
    diffbMap[i] = (bMap[i+1] - bMap[i-1])/(double)(2);
  }

  //// input parameter ////
  /////////////////////////

  // label of last rootcap cell -> comes from parameter

  // label of first cell -> comes from selection

  int maxLabel = 0;
  int minLabel = 1E9;
  int numberVerticesSegmentedMesh = 0;

  // determine number of nodes (vertices) and labels
  forall(const vertex &v, segmentedMesh) {
    numberVerticesSegmentedMesh++;
    if(v->label>maxLabel)
      maxLabel = v->label;
    if(v->label<minLabel)
      minLabel = v->label;
  }

  //ColumnVector uniqueLabels(maxLabel);
  std::vector<int> countLabels(maxLabel);
  std::vector<int> uniqueLabels;

  // fill above data structures
  progress.restart("Analyze Cells 3D - Find Cells", 0);
  forall(const vertex &v, segmentedMesh) {
    if(!progress.advance(1)) userCancel();
    if(countLabels[v->label-1] != 1) {
      uniqueLabels.push_back(v->label);
      countLabels[v->label-1] = 1;
    }

  }
  rcp.rootData.numCells = uniqueLabels.size();
  rcp.rootData.uniqueLabels = uniqueLabels;

  labelVertexMap lvMap;
  vMap vertexMap;

  // create label - vertex map
  progress.restart("Analyze Cells 3D - Create label/vertex map", 0);
  forall(const vertex &v, segmentedMesh) {
    if(!progress.advance(1)) userCancel();
    lvMap[v->label].push_back(v);
  }

  // analyze all cells

  RootCellAnalyzing rca;

  if(!rca.analyzeEmbryo(uniqueLabels, segmentedMesh, surfaceMesh, bMap, diffbMap, lvMap, rcp, selectedLabel1, minVolume))
    userCancel();
  if(!neighbourhoodGraph(m1, segmentedMesh, /*cellVertex, vertexCell, */0.0001, rcp))
    userCancel();
  progress.restart("Analyze Cells 3D - Finalize", 0);
  analyzed = true;
  return true;
}
REGISTER_MESH_PROCESS(AnalyzeCells);

// function to automatically assign the colours in the heatmap
QRgb calcRGB(double maxValue, double currentValue)
{
  QRgb value;
  int r = 0, b = 0, g = 0, a = 0;
  int red = 0, green = 0, blue = 0;

  if(currentValue<maxValue/5.0){
    blue = (currentValue)/maxValue*5.0*255.0;
  } else if(currentValue<maxValue*2.0/5.0){
    blue = 255;
    green = (currentValue-maxValue*1.0/5.0)/maxValue*5.0*255.0;
  } else if(currentValue<maxValue*3.0/5.0){
    blue = (maxValue*3.0/5.0-currentValue)/maxValue*5*255.0;
    green = 255;
  } else if(currentValue<maxValue*4.0/5.0){
    green = 255;
    red = (currentValue-maxValue*3.0/5.0)/maxValue*5.0*255.0;
  } else {
    green = (maxValue-currentValue)/maxValue*5.0*255.0;
    red = 255;
  }
  // assure the range from 0 to 255
  r = std::max(0,std::min(255,red));
  g = std::max(0,std::min(255,green));
  b = std::max(0,std::min(255,blue));
  a = 255;

  value = qRgba(r, g, b, a);
  return value;
}

// draw a cross according to the parameters (used for generating the heatmap)
void drawCross(QImage& image, int x, int y, int size, int crossSize, QRgb color)
{
  for(int i = -crossSize; i <= crossSize; i++) {
    image.setPixel(std::min(std::max(0,x+i),size-1), y, color);
    image.setPixel(x, std::min(std::max(0,y+i),size-1), color);
  }
}

// draw a circle according to the parameters (used for generating the heatmap)
void drawCircle(QImage& image, int x, int y, int size, QRgb color)
{
  int circleSize = 6;
  Vec2d currentPoint(x,y);
  for(int i = -circleSize; i <= circleSize; i++) {
    for(int j = -circleSize; j <= circleSize; j++) {
      Vec2d testPoint(i+x,j+y);
      if((int)sqrt(i*i+j*j)==circleSize)
        image.setPixel(std::min(std::max(0,x+i),size-1), std::min(std::max(0,y+j),size-1), color);
    }
  }
}

// set a number to a label
void setLabelProperties(QLabel* label, int x, int y, int number, bool reset)
{
  int xOffset = 10+10;
  int yOffset = 100+7;
  int xSize = 10;
  int ySize = 10;

  label->setGeometry(QRect(x+xOffset, y+yOffset, xSize, ySize));
  if(!reset)
    label->setText(QString::number(number+1));
  else
    label->setText("");
  label->setStyleSheet("QLabel { color : rgb(120, 0, 120); }");

}

// find the label that has to be changed
void drawNumber(int x, int y, int label, int number)
{

  if(label == 0){
    setLabelProperties(ui.labelCluster1, x, y, number,false);
  } else if(label == 1){
    setLabelProperties(ui.labelCluster2, x, y, number,false);
  } else if(label == 2){
    setLabelProperties(ui.labelCluster3, x, y, number,false);
  } else if(label == 3){
    setLabelProperties(ui.labelCluster4, x, y, number,false);
  } else if(label == 4){
    setLabelProperties(ui.labelCluster5, x, y, number,false);
  } else if(label == 5){
    setLabelProperties(ui.labelCluster6, x, y, number,false);
  } else if(label == 6){
    setLabelProperties(ui.labelCluster7, x, y, number,false);
  } else if(label == 7){
    setLabelProperties(ui.labelCluster8, x, y, number,false);
  } else if(label == 8){
    setLabelProperties(ui.labelCluster9, x, y, number,false);
  } else if(label == 9){
    setLabelProperties(ui.labelCluster10, x, y, number,false);
  } else if(label == 10){
    setLabelProperties(ui.labelCluster11, x, y, number,false);
  } else if(label == 11){
    setLabelProperties(ui.labelCluster12, x, y, number,false);
  } else if(label == 12){
    setLabelProperties(ui.labelCluster13, x, y, number,false);
  } else if(label == 13){
    setLabelProperties(ui.labelCluster14, x, y, number,false);
  } else if(label == 14){
    setLabelProperties(ui.labelCluster15, x, y, number,false);
  } else if(label == 15){
    setLabelProperties(ui.labelCluster16, x, y, number,false);
  } else if(label == 16){
    setLabelProperties(ui.labelCluster17, x, y, number,false);
  } else if(label == 17){
    setLabelProperties(ui.labelCluster18, x, y, number,false);
  } else if(label == 18){
    setLabelProperties(ui.labelCluster19, x, y, number,false);
  } else if(label == 19){
    setLabelProperties(ui.labelCluster20, x, y, number,false);
  }

}

// reset the label number
void resetNumber(int label)
{

  if(label == 0){
    setLabelProperties(ui.labelCluster1, 0, 0, 0,true);
  } else if(label == 1){
    setLabelProperties(ui.labelCluster2, 0, 0, 0,true);
  } else if(label == 2){
    setLabelProperties(ui.labelCluster3, 0, 0, 0,true);
  } else if(label == 3){
    setLabelProperties(ui.labelCluster4, 0, 0, 0,true);
  } else if(label == 4){
    setLabelProperties(ui.labelCluster5, 0, 0, 0,true);
  } else if(label == 5){
    setLabelProperties(ui.labelCluster6, 0, 0, 0,true);
  } else if(label == 6){
    setLabelProperties(ui.labelCluster7, 0, 0, 0,true);
  } else if(label == 7){
    setLabelProperties(ui.labelCluster8, 0, 0, 0,true);
  } else if(label == 8){
    setLabelProperties(ui.labelCluster9, 0, 0, 0,true);
  } else if(label == 9){
    setLabelProperties(ui.labelCluster10, 0, 0, 0,true);
  } else if(label == 10){
    setLabelProperties(ui.labelCluster11, 0, 0, 0,true);
  } else if(label == 11){
    setLabelProperties(ui.labelCluster12, 0, 0, 0,true);
  } else if(label == 12){
    setLabelProperties(ui.labelCluster13, 0, 0, 0,true);
  } else if(label == 13){
    setLabelProperties(ui.labelCluster14, 0, 0, 0,true);
  } else if(label == 14){
    setLabelProperties(ui.labelCluster15, 0, 0, 0,true);
  } else if(label == 15){
    setLabelProperties(ui.labelCluster16, 0, 0, 0,true);
  } else if(label == 16){
    setLabelProperties(ui.labelCluster17, 0, 0, 0,true);
  } else if(label == 17){
    setLabelProperties(ui.labelCluster18, 0, 0, 0,true);
  } else if(label == 18){
    setLabelProperties(ui.labelCluster19, 0, 0, 0,true);
  } else if(label == 19){
    setLabelProperties(ui.labelCluster20, 0, 0, 0,true);
  }

}

// create the qimage with the heatmap
QImage DataToQImage( heatMapDataStructure& body, bool drawPoints )
{
  // init variables
  int size = body.gridSize;
  cellMorphLandscape data = body.heatMap;
  double maxValue = body.high;
  maximaVector maxVec = body.maximaHeatMapAll;
  maximaVector& maxVec2 = body.maximaHeatMapSelected;
  cellMorphLandscape points = body.points;
  IntIntMap labels = body.maximaHeatMapSelectedLabel;

  // init image and colours
  int factor = 3;
  QImage image( size*factor, size*factor, QImage::Format_ARGB32 );
  QRgb value;
  QRgb white = qRgba(255,255,255,255);
  QRgb grey1 = qRgba(170,170,170,255);
  //QRgb grey2 = qRgba(85,85,85,255);
  QRgb purple = qRgba(120,0,120,255);

  int yOffset = size*factor-1;

  // draw the heatmap and (if option is checked) small points for the cells
  for(int i = 0; i < size; i++) {
    for(int j = 0; j < size; j++) {
      for(int k = 0; k < factor; k++) {
        for(int l = 0; l < factor; l++) {
          value = calcRGB(maxValue,data[i][j]);
          image.setPixel(i*factor+k, yOffset-(j*factor+l), value);
        }
      }
      //if(drawPoints && points[i][j] > 2){
      //  image.setPixel(i*factor+factor/2, j*factor+factor/2, white);
      //} else if(drawPoints && points[i][j] > 1){
      //  image.setPixel(i*factor+factor/2, j*factor+factor/2, grey1);
      //} else
      if(drawPoints && points[i][j] > 0){
        image.setPixel(i*factor+factor/2, yOffset-j*factor-factor/2, grey1);
      }
    }
  }

  // draw a small cross on all heatmap maxima
  int maxSize = maxVec.size();
  for(int i = 0; i < maxSize; i++) {
    Vec2d currentPoint = maxVec[i];
    drawCross(image, currentPoint[0]*factor+factor/2, yOffset-currentPoint[1]*factor-factor/2, size*factor, 2, white);
  }

  for(int i = 0; i < 20; i++) {
    resetNumber(i);
  }

  // draw the user defined cluster center with circle and big cross
  maxSize = maxVec2.size();
  for(int i = 0; i < maxSize; i++) {
    Vec2d currentPoint = maxVec2[i];
    drawCross(image, currentPoint[0]*factor+factor/2, yOffset-currentPoint[1]*factor-factor/2, size*factor, 4, purple);
    drawCircle(image, currentPoint[0]*factor+factor/2, yOffset-currentPoint[1]*factor-factor/2, size*factor, purple);

    drawNumber(currentPoint[0]*factor+factor/2-2, yOffset-currentPoint[1]*factor-factor/2+2, i, labels[i]);
  }

  return image;
}

// checks whether there exists a maximum in maxVec close to the pos coordinates
// writes the result in maxIdx
bool chosenMaxExistNearby(maximaVector& maxVec, Vec2d pos, int& maxIdx)
{
  double minDis = 1E20;
  int maxVecSize = maxVec.size();

  for(int i = 0; i < maxVecSize; i++) {
    double dis = norm(pos - maxVec[i]);
    if(dis < minDis){
      minDis = dis;
      maxIdx = i;
    }
  }

  if(minDis < 10){
    return true;
  } else {
    maxIdx = -1;
    return false;
  }
}

// generate the heatmap image
void CellAtlas::setImage()
{

  QImage image;

  // check which root part is active
  if(ui.rootPartCmb->currentIndex() == 0){
    // generate image
    ui.xDimCmb->setCurrentIndex(rcp.mainBody.chosenX-1);
    ui.yDimCmb->setCurrentIndex(rcp.mainBody.chosenY-1);
    image = DataToQImage( rcp.mainBody, ui.showCellsCheckBox->isChecked() );
  } else {
    // generate image
    ui.xDimCmb->setCurrentIndex(rcp.radicle.chosenX-1);
    ui.yDimCmb->setCurrentIndex(rcp.radicle.chosenY-1);
    image = DataToQImage( rcp.radicle, ui.showCellsCheckBox->isChecked() );
  }
  // set image to image area
  ui.imageHolder->setPixmap(QPixmap::fromImage(image));
  ui.imageHolder->setFocus();
}

// upon double click: delete if there is a cluster nearby
void CellAtlas::setDeletePosition(const QPoint& p)
{
  //cout << "Mouse double click" << p.x() << "   " << p.y() << endl;
  Vec2d currentPoint;
  currentPoint[0] = p.x()/3.0;
  currentPoint[1] = (160.0*3.0-1-p.y())/3.0;
  int idx;
  if(ui.rootPartCmb->currentIndex() == 0){ // main
    // check whether there already is a maximum at the clicked position
    if(chosenMaxExistNearby(rcp.mainBody.maximaHeatMapSelected, currentPoint, idx)){
      // yes : delete this maximum
      rcp.delIdx(rcp.mainBody, idx);
    }
  } else { // radicle
    if(chosenMaxExistNearby(rcp.radicle.maximaHeatMapSelected, currentPoint, idx)){
      // yes : delete this maximum
      rcp.delIdx(rcp.radicle, idx);
    }
  }

  setImage();
}

// upon mouse click: if a maximum is nearby: activate the maximum (for drag and drop)
// if no maximum is nearby create a new one
void CellAtlas::setPosition(const QPoint& p)
{
  Vec2d currentPoint;
  currentPoint[0] = p.x()/3.0;
  currentPoint[1] = (160.0*3.0-1-p.y())/3.0;
  if(ui.rootPartCmb->currentIndex() == 0){ // main
    int maxVecSize = rcp.mainBody.maximaHeatMapSelected.size();
    int idx;
    // check whether there already is a maximum at the clicked position
    if(chosenMaxExistNearby(rcp.mainBody.maximaHeatMapSelected, currentPoint, idx)){
      // yes : activate this maximum
      rcp.mainBody.activatedMaximum = idx;
    } else {
      // no : create a new maximum
      rcp.mainBody.maximaHeatMapSelected[maxVecSize] = currentPoint;
      rcp.mainBody.maximaHeatMapSelectedLabel[maxVecSize] = 0;
      rcp.mainBody.activatedMaximum = -1;
      setImage();
    }

  } else { // radicle

    int maxVecSize = rcp.radicle.maximaHeatMapSelected.size();
    int idx;
    // check whether there already is a maximum at the clicked position
    if(chosenMaxExistNearby(rcp.radicle.maximaHeatMapSelected, currentPoint, idx)){
      // yes : activate this maximum
      rcp.radicle.activatedMaximum = idx;
    } else {
      // no : create a new maximum
      rcp.radicle.maximaHeatMapSelected[maxVecSize] = currentPoint;
      rcp.radicle.maximaHeatMapSelectedLabel[maxVecSize] = 0;
      rcp.radicle.activatedMaximum = -1;
      setImage();
    }

  }

}

// upon mouse release: check whether there is an activated maximum (=nearby maximum) and move it to the new position
void CellAtlas::setReleasePosition(const QPoint& p)
{
  Vec2d currentPoint;
  currentPoint[0] = p.x()/3.0;
  currentPoint[1] = (160.0*3.0-1-p.y())/3.0;


  if(ui.rootPartCmb->currentIndex() == 0){ // main
    if(rcp.mainBody.activatedMaximum != -1){    // check if there is an activated maximum
      //yes : move it to the new position
      rcp.mainBody.maximaHeatMapSelected[rcp.mainBody.activatedMaximum] = currentPoint;
    }
  } else { // rootcap
    if(rcp.radicle.activatedMaximum != -1){    // check if there is an activated maximum
      //yes : move it to the new position
      rcp.radicle.maximaHeatMapSelected[rcp.radicle.activatedMaximum] = currentPoint;
    }
  }
  setImage();

  rcp.mainBody.activatedMaximum = -1;
  rcp.radicle.activatedMaximum = -1;

}

// automatically assign the highest maxima
void CellAtlas::setAutoCluster()
{
  if(ui.rootPartCmb->currentIndex() == 0){ // main
    rcp.setAutoCluster(ui.spinBoxAutoClustering->value(), true);
  } else {
    rcp.setAutoCluster(ui.spinBoxAutoClustering->value(), false);
  }
  setImage();
  ui.imageHolder->setFocus();
}

// upon keypress: check whether there is a cluster nearby and if yes: assign the label to it
void CellAtlas::setClusterLabel(QString label){

  if(label == "1" || label == "2" || label == "3" || label == "4" || label == "5" || label == "6" || label == "7" || label == "8" || label == "9"){
    int labelNr = label.toInt() - 1;
    int idx;
    maximaVector maxVec;
    if(ui.rootPartCmb->currentIndex() == 0){ // main
      //cout << " ggg" << endl;
      maxVec = rcp.mainBody.maximaHeatMapSelected;
      if(chosenMaxExistNearby(rcp.mainBody.maximaHeatMapSelected, mousePos, idx)){
        // assign label to cluster
        rcp.mainBody.maximaHeatMapSelectedLabel[idx] = labelNr;
      }
    } else { // rootcap
      maxVec = rcp.radicle.maximaHeatMapSelected;
      if(chosenMaxExistNearby(rcp.radicle.maximaHeatMapSelected, mousePos, idx)){
        // assign label to cluster
        rcp.radicle.maximaHeatMapSelectedLabel[idx] = labelNr;
      }
    }
  } else {
    cout << "Enter a number between 1 and 9" << endl;
  }

  setImage();

}

// update the position of the mouse within the heatmap window
void CellAtlas::setMousePosition(const QPoint& p){

  Vec2d currentPoint;
  currentPoint[0] = p.x()/3.0;
  currentPoint[1] = (160.0*3.0-1-p.y())/3.0;
  mousePos = currentPoint;
  //cout << " input2  " << currentPoint << endl;

}

// change the Y coord of the heatmap and redraw with the new data
void CellAtlas::changeHeatmapX(QString stringX)
{
  if(ui.rootPartCmb->currentIndex() == 0){ // main
    rcp.setHeatmapX(rcp.mainBody, stringX);
  } else { // root
    rcp.setHeatmapX(rcp.radicle, stringX);
  }

  rcp.geometryBasedLabelling();
  setImage();
}


// change the Y coord of the heatmap and redraw with the new data
void CellAtlas::changeHeatmap(QString stringY)
{
  if(ui.rootPartCmb->currentIndex() == 0){ // main
    rcp.setHeatmapY(rcp.mainBody, stringY);
  } else { // root
    rcp.setHeatmapY(rcp.radicle, stringY);
  }

  rcp.geometryBasedLabelling();
  setImage();
}

void updateParentLabels(Mesh *m1)
{
  // update parent labels with the generated ones
  m1->parentLabelMap().clear();

  for(int i=0; i<rcp.rootData.numCells; i++){
    int currentLabel = rcp.rootData.uniqueLabels[i];
    m1->parentLabelMap()[currentLabel] = rcp.cellLabel[currentLabel];
  }

  m1->showParent();
  m1->updateTriangles();
}

// changes the sigma parameter of the heatmap and redraws the heatmap
void CellAtlas::changeSigma(double sigma)
{

  if(ui.rootPartCmb->currentIndex() == 0){ // main
    rcp.mainBody.sigma = sigma;

  } else { // radicle
    rcp.radicle.sigma = sigma;
  }

  rcp.geometryBasedLabelling();
  setImage();

}

// switch the heatmap view from main body to radicle or back
void CellAtlas::changeRootPart()
{

  if(ui.rootPartCmb->currentIndex() == 0){ // main
    ui.spinBoxSigma->setValue(rcp.mainBody.sigma);

  } else { // radicle
    ui.spinBoxSigma->setValue(rcp.radicle.sigma);
  }

  setImage();

}

void CellAtlas::resetCluster()
{
  // reset all preselected clusters

  if(ui.rootPartCmb->currentIndex() == 0){ // main
    rcp.resetHeat(rcp.mainBody);

  } else { // radicle
    rcp.resetHeat(rcp.radicle);
  }
  rcp.geometryBasedLabelling();
  setImage();

}

void CellAtlas::setPreCluster()
{
  // take all currently selected clusters, look for their nearest maximum, register them as preselected and remove them from the heatmap

  if(ui.rootPartCmb->currentIndex() == 0){ // main
    rcp.preselectCells(rcp.mainBody);

  } else { // radicle
    rcp.preselectCells(rcp.radicle);
  }
  rcp.geometryBasedLabelling();
  setImage();

}


bool CellAtlas::initialize(ParmList& parms, QWidget* parent)
{
  //Progress progress("Assign Cell Types", 0);

  //bool hasRootCap = false;
  bool hasRootCap = stringToBool(parms[0].toString());

  //if(strings[0] == "Yes"){
  //if(QString::compare(strings[0],"Yes")==0){
  //  hasRootCap = true;
  //}



  if(!analyzed && !loaded){
    // do nothing and return error message in operator
  } else {

    Mesh *m1 = mesh(0);
    const vvgraph& segmentedMesh = m1->graph();

    int selectedLabel1 = 0;
    int selectedLabel2 = 0;

    // get selected cells
    forall(const vertex &v, segmentedMesh) {
      if(v->selected && selectedLabel1 == 0){
        selectedLabel1 = v->label;
      }
      if(v->selected && selectedLabel1!=v->label){
        selectedLabel2 = v->label;
      }
    }
    cout << "Selected Cells: " << selectedLabel1 << "  " << selectedLabel2 << endl;
    if(selectedLabel1 == 0){
      // do nothing and return error message in operator
      parms[0] = "error";
    } else {

      dlg = new QDialog(parent);
      ui.setupUi(dlg);
      this->_dlg = dlg;

      if(!hasRootCap){
        ui.rootPartCmb->setEnabled(false);
      } else {
        ui.rootPartCmb->setEnabled(true);
      }
      connect(ui.imageHolder, SIGNAL(mousePressed( const QPoint& )), this, SLOT(setPosition(const QPoint&)));
      connect(ui.imageHolder, SIGNAL(mouseReleased( const QPoint& )), this, SLOT(setReleasePosition(const QPoint&)));
      connect(ui.buttonAutoClustering, SIGNAL(released()), this, SLOT(setAutoCluster()));
      connect(ui.preselectButton, SIGNAL(released()), this, SLOT(setPreCluster()));
      connect(ui.imageHolder, SIGNAL(mouseDouble( const QPoint& )), this, SLOT(setDeletePosition(const QPoint&)));
      connect(ui.showCellsCheckBox, SIGNAL(stateChanged(int)), this, SLOT(setImage()));
      connect(ui.rootPartCmb, SIGNAL(currentIndexChanged(QString)), this, SLOT(changeRootPart()));
      connect(ui.imageHolder, SIGNAL(keyPressed( const QString& )), this, SLOT(setClusterLabel(QString)));
      connect(ui.imageHolder, SIGNAL(mouseMove( const QPoint& )), this, SLOT(setMousePosition(const QPoint&)));
      connect(ui.yDimCmb, SIGNAL(currentIndexChanged(QString)), this, SLOT(changeHeatmap(QString)));
      connect(ui.xDimCmb, SIGNAL(currentIndexChanged(QString)), this, SLOT(changeHeatmapX(QString)));
      connect(ui.spinBoxSigma, SIGNAL(valueChanged(double)), this, SLOT(changeSigma(double)));
      connect(ui.buttonReset, SIGNAL(released()), this, SLOT(resetCluster()));

      rcp.setParameter(hasRootCap, selectedLabel1, selectedLabel2);
      rcp.assignRootRegions();
      rcp.geometryBasedLabelling();
      setImage();

      cout << " main cells  " << rcp.mainBody.cellCounter << " root cells  " << rcp.radicle.cellCounter << " col cells " << rcp.columella.cellCounter << endl;

      if(dlg->exec() == QDialog::Accepted)
      {
        //relabel
        rcp.geometryBasedLabelling();
        // update Labels
        updateParentLabels(m1);// = true;
      } else {
        // do nothing
      }
    }
  }
  return true;
}

bool CellAtlas::operator()(QString rootCap)
{
  if((!analyzed && !loaded) || rootCap == "error"){
    return setErrorMessage("Warning: no data present or no cells selected. Please load existing cell data or analyze the root first and select a first cell and if necessary a last root cap cell.");
  }

  return true;
}

REGISTER_MESH_PROCESS(CellAtlas);

bool SelectBadCells::operator()(Mesh *m1)
{
  // select all cells that are labelled as bad (=too small, no centroid or no intersect found)
  forall(const vertex& v, m1->graph())
  {
    if(rcp.rootData.cellBad[v->label]) v->selected = true;
    else v->selected = false;
  }

  m1->correctSelection(true);
  m1->updateSelection();

  return true;
}
REGISTER_MESH_PROCESS(SelectBadCells);

// file dialogue for save data
bool SaveCellData::initialize(ParmList& parms, QWidget* parent)
{
  QString filename = parms[0].toString();
  if(filename.isEmpty())
    filename = QFileDialog::getSaveFileName(0, "Choose spreadsheet file to save", QDir::currentPath(), "CSV files (*.csv)");
  if(filename.isEmpty())
    return false;
  if(!filename.endsWith(".csv", Qt::CaseInsensitive))
    filename += ".csv";
  parms[0] = filename;
  return true;
}


bool SaveCellData::operator()(QString filename)
{
  //cout << "file   " << filename.toLocal8Bit().constData() << endl;

  // file for root cell data
  QFile file(filename);
  if(!file.open(QIODevice::WriteOnly))
  {
    setErrorMessage(QString("File '%1' cannot be opened for writing").arg(filename));
    return false;
  }
  QTextStream out(&file);

  out << "Cell label, Cell type, Associated Cortical Cell,Arclength, Radius, Longitudinal length, Radial length, Circumferential length, Cell Volume, Cell Wall Area" << endl;

  Mesh *m1 = mesh(0);

  for(int i = 0; i<rcp.rootData.numCells; i++){
    int currentLabel = rcp.rootData.uniqueLabels[i];
    out << currentLabel << "," << m1->parentLabelMap()[currentLabel] << "," << rcp.rootData.associatedCorticalCell[currentLabel] << "," << rcp.rootData.arclengths[currentLabel] << "," << rcp.rootData.scaledRadialDis[currentLabel] << "," << rcp.rootData.lengthLong[currentLabel] << "," << rcp.rootData.lengthRad[currentLabel] << "," << rcp.rootData.lengthCirc[currentLabel] << "," << rcp.rootData.cellVolumes[currentLabel] << "," << rcp.rootData.cellWallArea[currentLabel] << endl;
  }


  // file for selected cluster positions and labels
  filename.remove(".csv", Qt::CaseInsensitive);
  filename += "_config";
  filename += ".csv";

  QFile file2(filename);
  if(!file2.open(QIODevice::WriteOnly))
  {
    setErrorMessage(QString("File '%1' cannot be opened for writing").arg(filename));
    return false;
  }
  QTextStream out2(&file2);

  out2 << "x, y" << endl;

  int lengthMain = rcp.mainBody.maximaHeatMapSelected.size();
  int lengthRoot = rcp.radicle.maximaHeatMapSelected.size();

  //out2 << "-1" << "," << lengthMain << endl;
  out2 << -1*rcp.mainBody.chosenX << "," << rcp.mainBody.chosenY << "," << rcp.rootData.labelFirstCell << endl;

  for(int i = 0; i<lengthMain; i++){
    out2 << rcp.mainBody.maximaHeatMapSelected[i][0] << "," << rcp.mainBody.maximaHeatMapSelected[i][1] << "," << rcp.mainBody.maximaHeatMapSelectedLabel[i] << endl;
  }

  //out2 << "-1" << "," << lengthRoot << endl;
  out2 << -1*rcp.radicle.chosenX << "," << rcp.radicle.chosenY << "," << rcp.rootData.labelLastRootCapCell << endl;

  for(int i = 0; i<lengthRoot; i++){
    out2 << rcp.radicle.maximaHeatMapSelected[i][0] << "," << rcp.radicle.maximaHeatMapSelected[i][1] << "," << rcp.radicle.maximaHeatMapSelectedLabel[i] << endl;
  }

  // file for neighbourhood clusters
  filename.remove("_config.csv", Qt::CaseInsensitive);
  filename += "_neighbourhood";
  filename += ".csv";

  QFile file3(filename);
  if(!file3.open(QIODevice::WriteOnly))
  {
    setErrorMessage(QString("File '%1' cannot be opened for writing").arg(filename));
    return false;
  }
  QTextStream out3(&file3);

  out3 << "label, neighbour, wall area" << endl;

  forall(const Vec2iFloatPair &pr, rcp.rootData.wallArea){
    out3 << pr.first.x() << "," << pr.first.y() << "," << pr.second << endl;
  }

  return true;
}
REGISTER_MESH_PROCESS(SaveCellData);

bool LoadCellData::initialize(ParmList& parms, QWidget* parent)
{
  QString filename = parms[0].toString();
  if(filename.isEmpty())
    filename = QFileDialog::getOpenFileName(0, "Choose spreadsheet file to load", QDir::currentPath(), "CSV files (*.csv)");
  if(filename.isEmpty())
    return false;
  if(!filename.endsWith(".csv", Qt::CaseInsensitive))
    filename += ".csv";
  parms[0] = filename;
  return true;
}


bool LoadCellData::operator()(QString filename)
{

  // open file
  QFile file(filename);
  if(!file.open(QIODevice::ReadOnly))
  {
    setErrorMessage(QString("File '%1' cannot be opened for reading").arg(filename));
    return false;
  }
  QTextStream ss(&file);
  QString line = ss.readLine();
  QStringList fields = line.split(",");

  // variables to be filled
  int counter = 0;
  std::vector<int> uniqueLabels;
  labelDataMap parents, scaledRadialDis, arclengths;
  labelDataMap lengthRad, lengthLong, lengthCirc, vol, wall;
  IntIntMap assocCC;

  bool ok;
  //cout << "start" << endl;
  while(ss.status() == QTextStream::Ok)
  {
    fields = ss.readLine().split(",");
    if(fields[0].isEmpty()) break;

    int label = fields[0].toInt(&ok);
    int parent = fields[1].toInt(&ok);

    //cout << "lll  " << label << " parent" << parent << " long " << cellLengthLong << " rad " << cellLengthRad << " circ " << cellLengthCirc << endl;
    uniqueLabels.push_back(label);
    assocCC[label] = fields[2].toInt(&ok);
    arclengths[label] = fields[3].toDouble(&ok);
    scaledRadialDis[label] = fields[4].toDouble(&ok);
    lengthLong[label] = fields[5].toDouble(&ok);
    lengthRad[label] = fields[6].toDouble(&ok);
    lengthCirc[label] = fields[7].toDouble(&ok);
    vol[label] = fields[8].toDouble(&ok);
    wall[label] = fields[9].toDouble(&ok);
    parents[label] = parent;
    counter++;
  }

  // fill rootData structure
  rcp.rootData.associatedCorticalCell = assocCC;
  rcp.rootData.scaledRadialDis = scaledRadialDis;
  rcp.rootData.arclengths = arclengths;
  rcp.rootData.lengthLong = lengthLong;
  rcp.rootData.lengthRad = lengthRad;
  rcp.rootData.lengthCirc = lengthCirc;
  rcp.rootData.cellWallArea = wall;
  rcp.rootData.uniqueLabels = uniqueLabels;
  rcp.rootData.numCells = counter;
  rcp.rootData.cellVolumes = vol;
  rcp.cellLabel = parents;



  // file for selected clusters
  filename.remove(".csv", Qt::CaseInsensitive);
  filename += "_config";
  filename += ".csv";


  QFile file2(filename);
  if(!file2.open(QIODevice::ReadOnly))
  {
    setErrorMessage(QString("File '%1' cannot be opened for reading").arg(filename));
    return false;
  }

  QTextStream ss2(&file2);
  line = ss2.readLine();
  fields = line.split(",");

  counter = 0;
  maximaVector maxVecRootCap, maxVecMainBody;
  IntIntMap labelRadicle, labelMain;
  int xDimRootCap = 0, xDimMainBody = 0;
  int yDimRootCap = 0, yDimMainBody = 0;
  bool root = false;
  int labelFirst=0, labelRoot=0;

  while(ss2.status() == QTextStream::Ok)
  {
    fields = ss2.readLine().split(",");
    if(fields[0].isEmpty()) break;
    double xCoord = fields[0].toDouble(&ok);
    double yCoord = fields[1].toDouble(&ok);
    int number = fields[2].toInt(&ok);
    if(!root){ // main
      if(xCoord < 0){ // break point
        if(yDimMainBody>0){
          root = true;
          xDimRootCap = -1*xCoord;
          yDimRootCap = yCoord;
          labelRoot = fields[2].toDouble(&ok);
          counter = 0;
        } else {
          xDimMainBody = -1*xCoord;
          yDimMainBody = yCoord;
          labelFirst = fields[2].toDouble(&ok);
        }
      } else {
        maxVecMainBody[counter][0] = xCoord;
        maxVecMainBody[counter][1] = yCoord;
        labelMain[counter] = number;
        counter++;
      }
    } else { // root cap
      maxVecRootCap[counter][0] = xCoord;
      maxVecRootCap[counter][1] = yCoord;
      labelRadicle[counter] = number;
      counter++;
    }
  }

  rcp.mainBody.maximaHeatMapSelected = maxVecMainBody;
  rcp.mainBody.maximaHeatMapSelectedLabel = labelMain;
  rcp.mainBody.chosenX = xDimMainBody;
  rcp.mainBody.chosenY = yDimMainBody;
  rcp.radicle.maximaHeatMapSelected = maxVecRootCap;
  rcp.radicle.maximaHeatMapSelectedLabel = labelRadicle;
  rcp.radicle.chosenX = xDimRootCap;
  rcp.radicle.chosenY = yDimRootCap;

  // file for neighbourhood
  filename.remove("_config.csv", Qt::CaseInsensitive);
  filename += "_neighbourhood";
  filename += ".csv";

  QFile file3(filename);
  if(!file3.open(QIODevice::ReadOnly))
  {
    setErrorMessage(QString("File '%1' cannot be opened for reading").arg(filename));
    return false;
  }

  QTextStream ss3(&file3);
  line = ss3.readLine();
  fields = line.split(",");

  Vec2iFloatMap wallArea;
  while(ss3.status() == QTextStream::Ok){
    fields = ss3.readLine().split(",");
    if(fields[0].isEmpty()) break;
    int neigh1 = fields[0].toInt(&ok);
    int neigh2 = fields[1].toInt(&ok);
    double area = fields[2].toDouble(&ok);

    wallArea[Vec2i(neigh1, neigh2)] = area;
    wallArea[Vec2i(neigh2, neigh1)] = area;

  }

  rcp.rootData.wallArea = wallArea;


  // selected first and last radicle cell
  Mesh *m1 = mesh(0);
  forall(const vertex& v, m1->graph())
  {
    if(v->label == labelRoot || v->label == labelFirst) v->selected = true;
    else v->selected = false;
  }

  m1->correctSelection(true);
  m1->updateSelection();

  //rootData.rootData.labelFirstCell;
  //rootData.rootData.labelLastRootCapCell;

  // set parent labels
  updateParentLabels(m1);

  loaded = true;
  return true;
}
REGISTER_MESH_PROCESS(LoadCellData);

bool DisplayCellData::operator()(QString choice)
{

  Mesh *m1 = mesh(0);

  //int option = -1;
  labelDataMap heatMap;

  if(choice == "Cell Type"){
    //option = 1;
  } else if(choice == "Arclengths"){
    //option = 2;
    heatMap = rcp.rootData.arclengths;
  } else if(choice == "Radial"){
    //option = 3;
    heatMap = rcp.rootData.scaledRadialDis;
  } else if(choice == "Longitudinal Cell Length"){
    //option = 4;
    heatMap = rcp.rootData.lengthLong;
  } else if(choice == "Radial Cell Length"){
    //option = 5;
    heatMap = rcp.rootData.lengthRad;
  } else if(choice == "Circumferential Cell Length"){
    //option = 6;
    heatMap = rcp.rootData.lengthCirc;
  } else if(choice == "Volume"){
    //option = 6;
    heatMap = rcp.rootData.cellVolumes;
  } else if(choice == "Circumferential Angle"){
    //option = 6;
    heatMap = rcp.rootData.azimuthal;
  } else if(choice == "Cell Wall Area"){
    //option = 6;
    heatMap = rcp.rootData.cellWallArea;
  } else if(choice == "Associated Cortical Cell"){
    //option = 6;
    for(int i = 0; i<rcp.rootData.numCells; i++){
      int currentLabel = rcp.rootData.uniqueLabels[i];
      heatMap[currentLabel] = rcp.rootData.associatedCorticalCell[currentLabel];
    }
  } else {
    //option = -1;
    //warning << QString("Invalid type '%1'").arg(outputType);
  }

  double maxValue, minValue;
  rcp.calcMinMax(heatMap, minValue, maxValue);
  //cout << " min max " << minValue << "  " << maxValue << endl;


  float delta = maxValue - minValue;
  m1->wallHeat().clear();
  m1->labelHeat().clear();
  for(int i = 0; i<rcp.rootData.numCells; i++){
    int currentLabel = rcp.rootData.uniqueLabels[i];
    m1->labelHeat()[currentLabel] = (heatMap[currentLabel] - minValue)/delta;
  }
  //m1->heatMapUnit() = unit;
  m1->heatMapBounds() = Point2f(minValue, maxValue);
  m1->updateTriangles();
  m1->showHeat();
  SETSTATUS(QString("Loaded heat map with values ranging from %1 to %2").arg(minValue).arg(maxValue));


  //LoadHeatMap lhm(*this);
  //lhm(m1, filename, option, 1);


  return true;
}
REGISTER_MESH_PROCESS(DisplayCellData);


bool TopologicalCheck::operator()(QString selection, double threshVol, double threshWallArea, int rootCapLabel, int airBubbleLabel, int vascLabel, int errors)
{
  bool selectedArea = stringToBool(selection);
  Mesh *m1 = mesh(0);

  // 11 for parent labels 0 to 10
  typedef Vector<11, double> Vecc;
  typedef Vector<11, bool> VeccBool;
  typedef Vector<11, Vecc> Matt;
  typedef Vector<11, VeccBool> MattBool;

  Vecc nrOfParentLabels;
  Vecc avgVol;
  MattBool valid;
  Matt nrOfParentConnections;

  labelBoolMap mislabelled;

  intToVec cellConnections;
  intToVec cellWallAreas;

  // create a connection list
  forall(const Vec2iFloatPair &pr, rcp.rootData.wallArea){
    if(pr.second > threshWallArea && rcp.rootData.cellVolumes[pr.first.x()] > threshVol && rcp.rootData.cellVolumes[pr.first.y()] > threshVol){
      cellConnections[pr.first.x()].push_back(pr.first.y());
      cellWallAreas[pr.first.x()].push_back(pr.second);
    }
  }

  labelBoolMap cellSelected;

  // check for selection
  const vvgraph& segmentedMesh = m1->graph();
  forall(const vertex& v, segmentedMesh) {
    if(v->selected || !selectedArea)
      cellSelected[v->label] = true;
  }


  double avgNeigh=0, zeroNeigh=0;
  // check whether a cell has neighbours of a wrong label
  for(int i = 0; i<rcp.rootData.numCells; i++){
    int currentLabel = rcp.rootData.uniqueLabels[i];
    if(cellSelected[currentLabel]){
      int currentNeighbors = cellConnections[currentLabel].size();
      avgNeigh+=currentNeighbors;
      if(currentNeighbors==0) zeroNeigh++;
      mislabelled[currentLabel] = false;
      int currentParent = m1->parentLabelMap()[currentLabel];
      nrOfParentLabels[currentParent]++;
      avgVol[currentParent]+=rcp.rootData.cellVolumes[currentLabel];

      for(int j = 0; j<currentNeighbors; j++){
        // fill frequency matrix
        int currentNeighborParent = m1->parentLabelMap()[cellConnections[currentLabel][j]];
        nrOfParentConnections[currentParent][currentNeighborParent]++;
      }
    }
  }


  // find the two highest connection values and mark them as labelled
  for(int i = 0; i<11; i++){
    double high1=0, high2=0;
    double high1pos=0, high2pos=0;
    if(i!=airBubbleLabel && i!=10 && i!=0){ // ignore airbubbles, columella and unassigned
      for(int j = 0; j<11; j++){
        valid[i][j] = false;
        if(nrOfParentLabels[i]>0){
          double nr = (double)nrOfParentConnections[i][j]/(double)nrOfParentLabels[i];
          if(i!=j && j!=airBubbleLabel && j!=10 && j!=0){ // ignore same layer, airbubbles, columella and unassigned
            if(nr>high1) {
              high2 = high1;
              high2pos = high1pos;
              high1 = nr;
              high1pos = j;
            } else if(nr>high2) {
              high2 = nr;
              high2pos = j;
            }
          }
        }
      }
      valid[i][high1pos] = true;
      if(i!=rootCapLabel && i!=vascLabel) // only one valid connection for rootcap and vasculature
        valid[i][high2pos] = true;
    }

  }

  for(int i = 0; i<rcp.rootData.numCells; i++){
    int currentLabel = rcp.rootData.uniqueLabels[i];
    if(cellSelected[currentLabel]){
      int currentNeighbors = cellConnections[currentLabel].size();
      int currentParent = m1->parentLabelMap()[currentLabel];
      int errorCount = 0;
      for(int j = 0; j<currentNeighbors; j++){
        int currentNeighborParent = m1->parentLabelMap()[cellConnections[currentLabel][j]];
        if(valid[currentParent][currentNeighborParent] || currentParent==10 || currentNeighborParent==10 || currentParent==airBubbleLabel || currentNeighborParent==airBubbleLabel || currentParent==currentNeighborParent){
          // perfect
        } else { // error detected
          errorCount++;
        }
        if(errorCount>=errors){
          mislabelled[currentLabel] = true;
        }
      }
    }
  }

  //cout << " Out of " << rcp.rootData.numCells << " Cells, " << rcp.rootData.numCells-zeroNeigh << " have more than 0 neighbours with an average number of neighbours of " << avgNeigh/((double)rcp.rootData.numCells-zeroNeigh)<< endl;
  //for(int i = 0; i<10; i++){
  // avgVol[i]/=nrOfParentLabels[i];
  //}

  cout << "Allowed Connections:" << endl;
  for(int i = 0; i<11; i++){
    if(nrOfParentLabels[i]>0 && i!=airBubbleLabel && i!=10 && i!=0){
      cout << "Parent Label " << i << " with";
      for(int j = 0; j<11; j++){
        //cout << (double)nrOfParentConnections[i][j]/(double)nrOfParentLabels[i] << "  ";
        if(valid[i][j])
          cout << " " << j << " ";
      }
      cout << " " << endl;
    }

  }

  int counterMis = 0;

  for(int i = 0; i<rcp.rootData.numCells; i++){
    int currentLabel = rcp.rootData.uniqueLabels[i];
    if(mislabelled[currentLabel]){
      counterMis++;
    }
  }

  cout << "Selected " << counterMis << " mislabelled cells" << endl;
  rcp.rootData.mislabelled = mislabelled;

  // select all wrong cells
  forall(const vertex& v, m1->graph())
  {
    if(mislabelled[v->label]){
      v->selected = true;
    }
    else v->selected = false;
  }

  m1->correctSelection(true);
  m1->updateSelection();


  return true;
}
REGISTER_MESH_PROCESS(TopologicalCheck);

// Collapse Bezier points into a line
bool CollapseBezier::operator()()
{
  // get current Bezier points
  CuttingSurface* cutSurf = cuttingSurface();
  uint bezPoints = cutSurf->bezPoints();
  std::vector<Point3f>& bezVec = cutSurf->bezierV();

  // collapse them into a line
  int counter = 0;
  forall(Point3f &bezPoint, bezVec){
    bezPoint = bezVec[counter];
    counter++;
    if(counter>=bezPoints)
      counter = 0;
  }

  return true;
}

REGISTER_MESH_PROCESS(CollapseBezier);

// new december 2014/ january 2015
// Data Analysis Tool
bool DataAnalysis::initialize(ParmList& parms, QWidget* )
{
  QString filename = parms[0].toString();
  if(filename.isEmpty())
    filename = QFileDialog::getExistingDirectory(0, "Choose folder for control", QDir::currentPath(), QFileDialog::ShowDirsOnly);
  parms[0] = filename;

  QString filename2 = parms[1].toString();
  if(filename2.isEmpty())
    filename2 = QFileDialog::getExistingDirectory(0, "Choose folder for treatment", QDir::currentPath(), QFileDialog::ShowDirsOnly);
  parms[1] = filename2;

  QString filename3 = parms[2].toString();
  if(filename3.isEmpty())
    filename3 = QFileDialog::getExistingDirectory(0, "Choose folder for output", QDir::currentPath(), QFileDialog::ShowDirsOnly);
  parms[2] = filename3;

  return true;
}

bool DataAnalysis::operator()(QString folderControl, QString folderTreatment, QString folderOutput, QString outputFileType, QString avg, double window)
{

  DataAnalyzer da;
  //bool gusBool = stringToBool(gus);
  bool avgBool = stringToBool(avg);

  bool treatment = true;
  if(outputFileType == "Control")
    treatment = false;

  // everything happens in the DataAnalyzer class
  da.analyze(folderControl, folderTreatment, folderOutput, treatment, avgBool, window);

  return true;
}

REGISTER_MESH_PROCESS(DataAnalysis);

bool DataAnalysisGUS::initialize(ParmList& parms, QWidget* parent)
{
  QString filename = parms[0].toString();
  if(filename.isEmpty())
    filename = QFileDialog::getExistingDirectory(0, "Choose folder for input", QDir::currentPath(), QFileDialog::ShowDirsOnly);
  parms[0] = filename;
  /*
     QString &filename3 = parms[2];
     if(filename3.isEmpty())
     filename3 = QFileDialog::getExistingDirectory(0, "Choose folder for output", QDir::currentPath(), QFileDialog::ShowDirsOnly);
     parms[2] = filename3;
     */
  bool mergeBool = stringToBool(parms[2].toString());
  if(mergeBool){
    QString filename4 = parms[3].toString();
    if(filename4.isEmpty())
      filename4 = QFileDialog::getOpenFileName(0, "Choose output file", QDir::currentPath(), "CSV files (*.csv)");
    parms[3] = filename4;
  }

  return true;
}

bool DataAnalysisGUS::operator()(Mesh *m1, QString folderInput, QString gusFileExt,/* QString folderOutput, */QString mergeWithFile, QString fileToMerge, QString avg, double window, QString upperFilter, double upperFilterValue, QString lowerFilter, double lowerFilterValue)
{

  /*cout << "1  " << folderInput.toLocal8Bit().constData() << endl;
    cout << "1  " << folderOutput.toLocal8Bit().constData() << endl;
    cout << "1  " << gusFileExt.toLocal8Bit().constData() << endl;
    cout << "1  " << mergeWithFile.toLocal8Bit().constData() << endl;
    cout << "1  " << fileToMerge.toLocal8Bit().constData() << endl;
    cout << "1  " << upperFilter.toLocal8Bit().constData() << endl;*/

  DataAnalyzer da;
  bool mergeBool = stringToBool(mergeWithFile);
  bool avgBool = stringToBool(avg);

  IntDoubleMap GUSInfo;
  // everything important happens in the DataAnalyzer class
  da.analyzeGUS(folderInput, /*folderOutput,*/ gusFileExt, mergeBool, fileToMerge, avgBool, window, upperFilter, upperFilterValue, lowerFilter, lowerFilterValue, GUSInfo);

  // now update the heatmap
  m1->labelHeat().clear();
  IntDoubleMap::iterator it;

  double minVal = 1E20, maxVal = -1E20;

  for (it = GUSInfo.begin(); it != GUSInfo.end(); ++it){
    if(it->second > maxVal) maxVal = it->second;
    if(it->second < minVal) minVal = it->second;
  }
  //cout << "max: " << maxVal << ", min: " << minVal << endl;

  for (it = GUSInfo.begin(); it != GUSInfo.end(); ++it){
    //if(it->second >=1 && it->second <=4)
    m1->labelHeat()[it->first] = (it->second - minVal)/(maxVal - minVal);
    //double a = it->first;
    //double b = it->second;
    //cout << "label: " << a << ", value: " << b << endl;
  }

  m1->heatMapBounds() = Point2f(minVal, maxVal);
  m1->heatMapUnit() = QString("GUS");
  m1->showHeat();
  m1->updateTriangles();


  return true;
}

REGISTER_MESH_PROCESS(DataAnalysisGUS);



// Assign the Columella cells to rootcap and (inner) columella
bool AssignColumella::operator()(int labelRootCap, int labelCol)//, int labelVasc)
{

  Mesh *m1 = mesh(0);

  rcp.reassignColumella(labelRootCap, labelCol);//, labelVasc);

  updateParentLabels(m1);

  return true;
}

REGISTER_MESH_PROCESS(AssignColumella);

bool AssignCorticalCells::operator()(int labelCort)
{

  if(!analyzed){
    return setErrorMessage("Warning: no analyzed data present. Please run Analyze Cells 3D plugin first.");
  }

  // check whether there are some cortical cells
  bool existCortCells = false;
  for(int i = 0; i<rcp.rootData.numCells; i++){
    int currentLabel = rcp.rootData.uniqueLabels[i];
    if(rcp.cellLabel[currentLabel] == labelCort){
      existCortCells = true;
    }
  }

  if(!existCortCells){
    return setErrorMessage("Warning: no cortical cells found. Please make sure you have clustered the root and specified the correct label of the cortical cells");
  }

  // get bezier in high resolution and minmax of the arclengths
  double minS, maxS;
  rcp.assignCortCellsGetMinMax(labelCort, minS, maxS);

  std::vector<double> sSmooth;
  int lengthS = 1000;
  double stepS = (maxS-minS)/((double)lengthS-1.0);

  for(int i = 0; i<lengthS; i++){
    sSmooth.push_back(minS+stepS*i);
  }

  bezierMap bMapS,diffbMapS;
  double bezStart = 0.0;
  double bezEnd = 1.0;
  double stepSize = (double)(bezEnd-bezStart)/(double)(lengthS-1);

  CuttingSurface* cutSurf = cuttingSurface();

  for(int i=0; i<lengthS; i++){
    double u = bezStart+i*stepSize;
    Point3f p = cutSurf->evalCoord(0,u);
    bMapS[i] = p;
  }
  diffbMapS[0] = bMapS[1] - bMapS[0];
  diffbMapS[lengthS-1] = bMapS[lengthS-1] - bMapS[lengthS-2];

  for(int i=1; i<lengthS; i++){
    diffbMapS[i] = (bMapS[i+1] - bMapS[i-1])/(double)(2);
  }

  // run the main function
  rcp.assignCortCells(labelCort, sSmooth, bMapS, lengthS);

  return true;
}

REGISTER_MESH_PROCESS(AssignCorticalCells);

bool ExamineBadVasculature::operator()(int labelVasc, int labelAirBubble, int labelEndo, int methodAvg)
{

  labelDataMap landscape1D;
  int sizeLandsc = 160;
  double minC=1E20, maxC=-1E20;

  // go through all mislabelled cells and find vasc
  for(int i = 0; i<rcp.rootData.numCells; i++){
    int currentLabel = rcp.rootData.uniqueLabels[i];
    if(rcp.rootData.mislabelled[currentLabel] && rcp.cellLabel[currentLabel] == labelVasc){
      if(minC > rcp.rootData.lengthCirc[currentLabel]) minC = rcp.rootData.lengthCirc[currentLabel];
      if(maxC < rcp.rootData.lengthCirc[currentLabel]) maxC = rcp.rootData.lengthCirc[currentLabel];
    }
  }

  double sigma = 1.5*3;
  double stepX = 1.0/(double)(sizeLandsc-1.0);
  for(int j = 0; j<rcp.rootData.numCells; j++){
    int currentLabel = rcp.rootData.uniqueLabels[j];
    if(rcp.rootData.mislabelled[currentLabel] && rcp.cellLabel[currentLabel] == labelVasc){

      // generate the gaussian function

      // calc center of gaussian
      int binC = rcp.interpolateArrayIndex(rcp.rootData.lengthCirc[currentLabel],minC,maxC,sizeLandsc-1);

      // cut off radius around the center
      int radius = std::max(10.0,sigma*2.0);
      for(int i = -radius; i<=radius; i++){
        // calc gauss
        if(binC+i>=0 && binC+i<sizeLandsc){
          double xValue = rcp.interpolateArrayIndex(rcp.rootData.lengthCirc[currentLabel]+i*stepX,minC,maxC,sizeLandsc-1);
          landscape1D[binC+i] += rcp.gauss1D(xValue, binC, sigma);
        }
      }
    }
  }

  // check for minima in that function
  std::vector<int> minVec;

  // check beginning and end
  if(landscape1D[0] < landscape1D[1])  minVec.push_back(0);
  if(landscape1D[sizeLandsc-1] < landscape1D[sizeLandsc-2])  minVec.push_back(sizeLandsc-1);

  // check everything else
  for(int j = 1; j<sizeLandsc-1; j++){
    if(landscape1D[j] < landscape1D[j+1] && landscape1D[j] > landscape1D[j-1])  minVec.push_back(j);
  }

  // more than 2 minima: take the lowest as a separation point:
  // big cells are now endo, small cells are now bubbles (=matlab procedure)
  if(minVec.size() > 2){
    double min = 1E20;
    int minIdx = 0;
    for(int j = 0; j<minVec.size(); j++){
      //cout << "min  " << minVec[j] << "  " << landscape1D[minVec[j]] << endl;
      if(min > landscape1D[minVec[j]]){
        min = landscape1D[minVec[j]];
        minIdx = j;
      }
    }

    double separationPoint = minC + minVec[minIdx]*(maxC-minC)/(double)(sizeLandsc-1.0);

    double avgCirc = 0;
    int counter = 0;

    for(int j = 0; j<rcp.rootData.numCells; j++){
      int currentLabel = rcp.rootData.uniqueLabels[j];
      if(!rcp.rootData.mislabelled[currentLabel] && rcp.cellLabel[currentLabel] == labelEndo){
        counter++;
        avgCirc+=rcp.rootData.lengthCirc[currentLabel];
      }
    }
    avgCirc/=counter;

    // matlab procedure doesnt work very well -> new simple method
    if(methodAvg==1){
      separationPoint = avgCirc/2.0;
    }
    for(int j = 0; j<rcp.rootData.numCells; j++){
      int currentLabel = rcp.rootData.uniqueLabels[j];
      if(rcp.rootData.mislabelled[currentLabel] && rcp.cellLabel[currentLabel] == labelVasc){
        if(rcp.rootData.lengthCirc[currentLabel]>separationPoint){
          rcp.cellLabel[currentLabel] = labelEndo;
          cout << "relabelled cell " << currentLabel << " to Endo" << endl;
        } else {
          rcp.cellLabel[currentLabel] = labelAirBubble;
          cout << "relabelled cell " << currentLabel << " to Air Bubble" << endl;
        }
      }
    }
  }

  updateParentLabels(mesh(0));
  return true;
}

REGISTER_MESH_PROCESS(ExamineBadVasculature);

}
}
