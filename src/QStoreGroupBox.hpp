/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef QSTOREGROUPBOX_HPP
#define QSTOREGROUPBOX_HPP

#include <LGXConfig.hpp>

#include <QGroupBox>

class LithoGraphX;

namespace lgx {
namespace gui {

class QStoreGroupBox : public QGroupBox
{
public:
  QStoreGroupBox(QWidget* parent = 0);
  QStoreGroupBox(const QString& title, QWidget* parent = 0);

protected:
  LithoGraphX* mainWindow();

  void dragEnterEvent(QDragEnterEvent* event) override;
  void dropEvent(QDropEvent* event) override;
};

} // namespace lgx
} // namespace gui

#endif // QSTOREGROUPBOX_HPP

