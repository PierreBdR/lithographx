/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "Progress.hpp"
#ifndef WIN32
#  include <unistd.h>
#endif
#include <QEvent>
#include <QMutexLocker>
#include <QProgressDialog>
#include <QPushButton>
#include <QTextStream>
#include <QTimer>

#include <stdio.h>

#define SHOW_TIMER 2000

namespace {
class ProgressDialog : public QProgressDialog
{
public:
  ProgressDialog(const QString& message, int minimum, int maximum, QWidget *parent)
    : QProgressDialog(message, QString(), minimum, maximum, parent)
  { }

protected:
  void closeEvent(QCloseEvent *e) { e->ignore(); }
};
}

namespace lgx {
struct StartProgressEvent : public QEvent {
  StartProgressEvent(const QString& msg, int steps, bool allow_cancel)
    : QEvent(QEvent::User)
    , msg(msg)
    , steps(steps)
    , allow_cancel(allow_cancel)
  {
  }

  QString msg;
  int steps;
  bool allow_cancel;
};

struct AdvanceProgressEvent : public QEvent {
  AdvanceProgressEvent(int i)
    : QEvent(QEvent::User)
    , step(i)
  {
  }

  int step;
};

struct SetMaximumProgressEvent : public QEvent {
  SetMaximumProgressEvent(int n)
    : QEvent(QEvent::User)
    , steps(n)
  {
  }

  int steps;
};

struct SetTextProgressEvent : public QEvent {
  SetTextProgressEvent(const QString& txt)
    : QEvent(QEvent::User)
    , text(txt)
  {
  }

  QString text;
};

struct StopProgressEvent : public QEvent {
  StopProgressEvent()
    : QEvent(QEvent::User)
  {
  }
};

GlobalProgress* GlobalProgress::_instance = 0;

GlobalProgress& GlobalProgress::instance()
{
  if(_instance == 0)
    _instance = new GlobalProgress();
  return *_instance;
}

// Progress dialog class

GlobalProgress::GlobalProgress()
  : parent(0)
  , prg(0)
  , ismodal(true)
  , _canceled(false)
  , access(QMutex::Recursive)
  , showTimer(new QTimer(this))
{
  showTimer->setInterval(SHOW_TIMER);
  showTimer->setSingleShot(true);
  connect(showTimer, &QTimer::timeout, this, &GlobalProgress::showDialog);
}

GlobalProgress::~GlobalProgress()
{
  if(prg) {
    delete prg;
  }
  prg = 0;
}

void GlobalProgress::useModal()
{
  GlobalProgress& p = instance();
  if(p.prg and !p.ismodal) {
    disconnect(p.prg, &QProgressDialog::canceled, &p, &GlobalProgress::wasCanceled);
  }
  p.ismodal = true;
}

void GlobalProgress::useModeless()
{
  GlobalProgress& p = instance();
  if(p.prg and p.ismodal) {
    connect(p.prg, &QProgressDialog::canceled, &p, &GlobalProgress::wasCanceled);
  }
  p.ismodal = false;
}

void GlobalProgress::setParent(QWidget* _parent) {
  parent = _parent;
}

void GlobalProgress::wasCanceled() {
  _canceled = true;
}

void GlobalProgress::createDialog(const QString& msg, int steps, int value, bool allow_cancel)
{
  if(ismodal) {
    if(prg) {
      delete prg;
      prg = 0;
    }
    prg = new ProgressDialog(msg, 0, steps, parent);
    cancel = new QPushButton("Cancel");
    prg->setCancelButton(cancel);
    if(!allow_cancel)
      cancel->setDisabled(true);
  } else {
    if(prg) {
      if(allow_cancel)
        cancel->setEnabled(true);
      else
        cancel->setDisabled(true);
      prg->setMaximum(steps);
      prg->setLabelText(msg);
    } else {
      if(prg)
        delete prg;
      prg = new ProgressDialog(msg, 0, steps, parent);
      disconnect(prg, &QProgressDialog::canceled, prg, &QProgressDialog::cancel);
      cancel = new QPushButton("Cancel");
      prg->setCancelButton(cancel);
      if(!allow_cancel)
        cancel->setDisabled(true);
      connect(prg, &QProgressDialog::canceled, this, &GlobalProgress::wasCanceled);
    }
  }
  prg->setEnabled(true);
  _canceled = false;
  prg->setMinimumDuration(1000);
  prg->setAutoReset(false);
  prg->setAutoClose(false);
  prg->setValue(value);
  if(!showTimer->isActive() and !prg->isVisible())
    showTimer->start();
  // prg->show();
}

void GlobalProgress::showDialog()
{
  if(prg)
    prg->show();
}

void GlobalProgress::start(const QString& msg, int steps, bool allow_cancel)
{
  if(thread() != QThread::currentThread()) {
    QCoreApplication::postEvent(this, new StartProgressEvent(msg, steps, allow_cancel));
    return;
  }
  QMutexLocker lock(&access);
  createDialog(msg, steps, 0, allow_cancel);
  if(!stack.empty())
    value_stack << prg->value();
  value_stack << steps;
  stack << msg;
  cancel_stack << allow_cancel;
  if(ismodal) {
#ifndef WIN32
    usleep(1000);
#endif
    QThread::yieldCurrentThread();
    QCoreApplication::processEvents(QEventLoop::AllEvents);
  }
}

void GlobalProgress::close()
{
  if(prg)
    prg->accept();
}

int GlobalProgress::exec()
{
  if(!prg)
    return QDialog::Rejected;
  return prg->exec();
}

void GlobalProgress::setMaximum(int steps)
{
  if(thread() != QThread::currentThread()) {
    QCoreApplication::postEvent(this, new SetMaximumProgressEvent(steps));
    return;
  }
  QMutexLocker lock(&access);
  if(prg) {
    prg->setMaximum(steps);
    if(ismodal)
      QCoreApplication::processEvents(QEventLoop::AllEvents);
  }
}

int GlobalProgress::maximum() const
{
  if(prg)
    return prg->maximum();
  return -1;
}

QString GlobalProgress::text() const
{
  if(prg)
    return prg->labelText();
  return QString();
}

void GlobalProgress::setText(const QString& txt)
{
  if(thread() != QThread::currentThread()) {
    QCoreApplication::postEvent(this, new SetTextProgressEvent(txt));
    return;
  }
  QMutexLocker lock(&access);
  if(prg) {
    prg->setLabelText(txt);
    if(ismodal)
      QCoreApplication::processEvents(QEventLoop::AllEvents);
  }
}

bool GlobalProgress::advance(int step)
{
  if(thread() != QThread::currentThread()) {
    if(canceled())
      return false;
    QCoreApplication::postEvent(this, new AdvanceProgressEvent(step));
    return true;
  }
  if(!prg)
    return false;
  if(ismodal and prg->wasCanceled()) {
    prg->hide();
    return false;
  }
  QMutexLocker lock(&access);
  if(step <= prg->maximum())
    prg->setValue(step);
  else
    prg->setValue(prg->value());
  if(ismodal) {
    prg->show();
    QCoreApplication::processEvents(QEventLoop::AllEvents);
  }
  return true;
}

bool GlobalProgress::canceled() {
  return _canceled;
}

void GlobalProgress::stop()
{
  if(thread() != QThread::currentThread()) {
    QCoreApplication::postEvent(this, new StopProgressEvent());
    return;
  }
  if(!prg)
    return;
  QMutexLocker lock(&access);
  if(!stack.empty()) {
    stack.pop_back();
    value_stack.pop_back();
    cancel_stack.pop_back();
  }
  if(stack.empty()) {
    showTimer->stop();
    delete prg;
    prg = 0;
  } else {
    int value = value_stack.back();
    value_stack.pop_back();
    int steps = value_stack.back();
    bool allow_cancel = cancel_stack.back();
    QString msg = stack.back();
    createDialog(msg, steps, value, allow_cancel);
  }
  if(ismodal)
    QCoreApplication::processEvents(QEventLoop::AllEvents);
}

void GlobalProgress::clear()
{
  GlobalProgress& i = instance();
  i.stack.clear();
  i.value_stack.clear();
  i.stop();
}

bool GlobalProgress::event(QEvent* e)
{
  StartProgressEvent* start_e = dynamic_cast<StartProgressEvent*>(e);
  if(start_e) {
    start(start_e->msg, start_e->steps, start_e->allow_cancel);
    return true;
  }
  AdvanceProgressEvent* advance_e = dynamic_cast<AdvanceProgressEvent*>(e);
  if(advance_e) {
    advance(advance_e->step);
    return true;
  }
  StopProgressEvent* stop_e = dynamic_cast<StopProgressEvent*>(e);
  if(stop_e) {
    stop();
    return true;
  }
  SetMaximumProgressEvent* max_e = dynamic_cast<SetMaximumProgressEvent*>(e);
  if(max_e) {
    setMaximum(max_e->steps);
    return true;
  }
  SetTextProgressEvent* txt_e = dynamic_cast<SetTextProgressEvent*>(e);
  if(txt_e) {
    setText(txt_e->text);
    return true;
  }
  return false;
}

// Global progress functions for cuda
bool progAdvance(int step) {
  return (GlobalProgress::instance().advance(step));
}

void progStart(const std::string& msg, int steps) {
  GlobalProgress::instance().start(msg, steps);
}

bool progCancelled() {
  return (GlobalProgress::instance().canceled());
}

void progStop() {
  GlobalProgress::instance().stop();
}
} // namespace lgx
