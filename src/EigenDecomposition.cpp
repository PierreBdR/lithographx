/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "EigenDecomposition.hpp"

#include "Information.hpp"

#include <Eigen/Dense>

namespace lgx {
namespace util {

bool EigenVecs2D::valid(double epsilon) const
{
  float f1 = fabs(normsq(evec[0]) - 1)/2;
  float f2 = fabs(normsq(evec[1]) - 1)/2;
  float f3 = fabs(evec[0]*evec[1]);
  return f1 < epsilon and f2 < epsilon and f3 < epsilon;
}

bool EigenVecs3D::valid(double epsilon) const
{
  float f1 = fabs(normsq(evec[0]) - 1)/2;
  float f2 = fabs(normsq(evec[1]) - 1)/2;
  float f3 = fabs(normsq(evec[2]) - 1)/2;
  float f4 = fabs(evec[0]*evec[1]);
  float f5 = fabs(evec[0]*evec[2]);
  float f6 = fabs(evec[1]*evec[2]);
  return f1 < epsilon and f2 < epsilon and f3 < epsilon and f4 < epsilon and f5 < epsilon and f6 < epsilon;
}

void sort(EigenVecs2D& dec, EigenSort sorting)
{
  Point2f tv;
  switch(sorting)
  {
    case EigenSort::Increasing:
      tv = dec.eval;
      break;
    case EigenSort::Decreasing:
      tv = -dec.eval;
      break;
    case EigenSort::IncreasingAbs:
      tv = fabs(dec.eval);
      break;
    case EigenSort::DecreasingAbs:
      tv = -fabs(dec.eval);
      break;
    case EigenSort::NoSort:
      return;
  }
  using std::swap;
  if(tv[0] > tv[1]) {
    swap(dec.eval[0], dec.eval[1]);
    swap(dec.evec[0], dec.evec[1]);
  }
}

void sort(EigenVecs3D& dec, EigenSort sorting)
{
  Point3f tv;
  Point3u order;
  switch(sorting)
  {
    case EigenSort::Increasing:
      tv = dec.eval;
      break;
    case EigenSort::Decreasing:
      tv = -dec.eval;
      break;
    case EigenSort::IncreasingAbs:
      tv = fabs(dec.eval);
      break;
    case EigenSort::DecreasingAbs:
      tv = -fabs(dec.eval);
      break;
    case EigenSort::NoSort:
      return;
  }
  if(tv[0] <= tv[1]) {
    if(tv[1] <= tv[2])
      order = Point3u(0,1,2);
    else if(tv[0] <= tv[2])
      order = Point3u(0,2,1);
    else
      order = Point3u(2,0,1);
  } else if(tv[1] <= tv[2]) {
    if(tv[0] <= tv[2])
      order = Point3u(1,0,2);
    else
      order = Point3u(1,2,0);
  } else
    order = Point3u(2,1,0);

  dec.eval = Point3f(dec.eval[order[0]],
                     dec.eval[order[1]],
                     dec.eval[order[2]]);
  dec.evec = {dec.evec[order[0]], dec.evec[order[1]], dec.evec[order[2]]};
}

EigenVecs2D eigenDecomposition(const Matrix2f& m, EigenSort sorting)
{
  // First, make sure the matrix is symmetric
  float sum = fabs(m(0,0)) + fabs(m(1,1)) + fabs(m(0,1)) + fabs(m(1,1));
  if(fabs(m(1,0) - m(0,1)) / sum > 1e-5)
    return EigenVecs2D();
  double a = m(0,0);
  double b = (m(1,0) + m(0,1))/2;
  double c = m(1,1);
  double delta = (a*a+c*c-2*a*c+4*b*b);
  if(delta < 0)
    return EigenVecs2D();
  delta = sqrt(delta);
  EigenVecs2D result;
  result.eval[0] = (a+c+delta)/2;
  result.eval[1] = (a+c-delta)/2;

  double d11 = result.eval[0]-a, d12 = result.eval[0]-c;
  if(fabs(d11) > fabs(d12)) {
    result.evec[0] = normalized(Point2f(b/d11, 1.));
  } else {
    result.evec[0] = normalized(Point2f(1., b/d12));
  }
  result.evec[1] = Point2f(-result.evec[0].y(), result.evec[0].x());

  sort(result, sorting);
  return result;
}

EigenVecs3D eigenDecomposition(const Matrix3f& m, EigenSort sorting)
{
  typedef Eigen::Matrix<float,3,3,Eigen::RowMajor> EigenMat3f;
  Eigen::Map<const EigenMat3f> corr(m.c_data());
  Eigen::SelfAdjointEigenSolver<EigenMat3f> eigensolver(corr);
  if(eigensolver.info() != Eigen::Success) {
      Information::err << "Couldn't decompose the matrix into eigen vectors\nInformation:"
                       << eigensolver.info() << endl;
      return EigenVecs3D();
  }

  EigenVecs3D result;
  result.eval = Point3f(eigensolver.eigenvalues());
  const auto& evecs = eigensolver.eigenvectors();
  result.evec = {Point3f(evecs.col(0)), Point3f(evecs.col(1)), Point3f(evecs.col(2))};

  sort(result, sorting);

  return result;
}

} // namespace util
} // namespace lgx
