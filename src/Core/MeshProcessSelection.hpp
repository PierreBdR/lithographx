/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef MESH_PROCESS_SELECTION_HPP
#define MESH_PROCESS_SELECTION_HPP

#include <Process.hpp>

namespace lgx {
namespace process {
///\addtogroup MeshProcess
///@{

class LGXCORE_EXPORT MeshSelectByHeat : public MeshProcess {
public:
  MeshSelectByHeat(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  bool operator()(const ParmList& parms) override
  {
    bool ok;
    int mesh_id = parms[1].toInt(&ok);
    if(!ok)
      return setErrorMessage("Error, argument 'Mesh' must be an integer number");
    if(!checkState().mesh(MESH_NON_EMPTY | MESH_SHOW_MESH, mesh_id))
      return false;
    float min = parms[2].toFloat(&ok);
    if(!ok)
      return setErrorMessage("Error, argument 'Min Heat' must be a number");
    float max = parms[3].toFloat(&ok);
    if(!ok)
      return setErrorMessage("Error, argument 'Max Heat' must be a number");
    if(max < min)
      std::swap(min, max);
    return (*this)(mesh(mesh_id), min, max, parmToBool(parms[0]));
  }

  bool operator()(Mesh* m, float minHeat, float maxHeat, bool replaceSelection);

  QString folder() const override {
    return "Selection";
  }
  QString name() const override {
    return "Select By Heat";
  }
  QString description() const override {
    return "Select all vertices with a heat within bounds";
  }
  QStringList parmNames() const override
  {
    return QStringList() << "Replace selection"
                         << "Mesh"
                         << "Min Heat"
                         << "Max Heat";
  }
  QStringList parmDescs() const override
  {
    return QStringList() << "If true, the current selection will be replaced"
                         << "Mesh to act on (-1 for current)"
                         << "Select vertices whose heat is greater or equal to this"
                         << "Select vertices whose heat is less or equal to this";
  }
  ParmList parmDefaults() const override
  {
    return ParmList() << "true"
                         << "-1"
                         << "0"
                         << "1";
  }
  ParmChoiceMap parmChoice() const override
  {
    ParmChoiceMap map;
    map[0] = booleanChoice();
    map[1] = QStringList() << "-1"
                           << "0"
                           << "1";
    return map;
  }
  QIcon icon() const override {
    return QIcon();
  }
};

/**
 * \class MeshSelectAll
 *
 * Select all the vertices in the current mesh.
 */
class LGXCORE_EXPORT MeshSelectAll : public MeshProcess {
public:
  MeshSelectAll(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  bool operator()(const ParmList& ) override
  {
    if(!checkState().mesh(MESH_NON_EMPTY | MESH_SHOW_MESH))
      return false;
    return (*this)(currentMesh());
  }

  bool operator()(Mesh* m);

  QString folder() const override {
    return "Selection";
  }
  QString name() const override {
    return "Select All";
  }
  QString description() const override {
    return "Select all vertices of the current mesh";
  }
  QStringList parmNames() const override {
    return QStringList();
  }
  QStringList parmDescs() const override {
    return QStringList();
  }
  QIcon icon() const override {
    return QIcon();
  }
};

/**
 * \class MeshSelectBadNormals
 *
 * Select all the vertices with invalid normals in the current mesh
 */
class LGXCORE_EXPORT MeshSelectBadNormals : public MeshProcess {
public:
  MeshSelectBadNormals(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  bool operator()(const ParmList& ) override
  {
    if(!checkState().mesh(MESH_NON_EMPTY | MESH_SHOW_MESH))
      return false;
    return (*this)(currentMesh());
  }

  bool operator()(Mesh* m);

  QString folder() const override {
    return "Selection";
  }
  QString name() const override {
    return "Select Bad Normals";
  }
  QString description() const override
  {
    return "Select all vertices of the current mesh with bad normals (i.e. normals is not opf size 1)";
  }
  QStringList parmNames() const override {
    return QStringList();
  }
  QStringList parmDescs() const override {
    return QStringList();
  }
  QIcon icon() const override {
    return QIcon();
  }
};

/**
 * \class MeshUnselect
 *
 * Ensure no vertex is selected in the current mesh.
 */
class LGXCORE_EXPORT MeshUnselect : public MeshProcess {
public:
  MeshUnselect(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  bool operator()(const ParmList& ) override
  {
    if(!checkState().mesh(MESH_NON_EMPTY | MESH_SHOW_MESH))
      return false;
    return (*this)(currentMesh());
  }

  bool operator()(Mesh* m);

  QString folder() const override {
    return "Selection";
  }
  QString name() const override {
    return "Unselect";
  }
  QString description() const override {
    return "Unselect the vertices of the current mesh";
  }
  QStringList parmNames() const override {
    return QStringList();
  }
  QStringList parmDescs() const override {
    return QStringList();
  }
  QIcon icon() const override {
    return QIcon();
  }
};

/**
 * \class MeshInvertSelection
 *
 * Invert the selection status of all the vertices in the current mesh.
 */
class LGXCORE_EXPORT MeshInvertSelection : public MeshProcess {
public:
  MeshInvertSelection(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  bool operator()(const ParmList& ) override
  {
    if(!checkState().mesh(MESH_NON_EMPTY | MESH_SHOW_MESH))
      return false;
    return (*this)(currentMesh());
  }

  bool operator()(Mesh* m);

  QString folder() const override {
    return "Selection";
  }
  QString name() const override {
    return "Invert Selection";
  }
  QString description() const override {
    return "Invert the selection of the current mesh.";
  }
  QStringList parmNames() const override {
    return QStringList();
  }
  QStringList parmDescs() const override {
    return QStringList();
  }
  QIcon icon() const override {
    return QIcon();
  }
};

/**
 * \class MeshSelectUnlabeled
 *
 * Select all the unlabel vertices of the current mesh. Other vertices may either be unselected, or stay in their
 * current selection state.
 */
class LGXCORE_EXPORT MeshSelectUnlabeled : public MeshProcess {
public:
  MeshSelectUnlabeled(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  bool operator()(const ParmList& parms) override
  {
    if(!checkState().mesh(MESH_NON_EMPTY | MESH_SHOW_MESH))
      return false;
    bool replace = parmToBool(parms[0]);
    return (*this)(currentMesh(), replace);
  }

  bool operator()(Mesh* m, bool replace);

  QString folder() const override {
    return "Selection";
  }
  QString name() const override {
    return "Select Unlabeled";
  }
  QString description() const override {
    return "Add to or replace the selection with the unlabeled vertices.";
  }
  QStringList parmNames() const override {
    return QStringList() << "Replace selection";
  }
  QStringList parmDescs() const override {
    return QStringList() << "Replace selection";
  }
  ParmList parmDefaults() const override {
    return ParmList() << "No";
  }
  ParmChoiceMap parmChoice() const override
  {
    ParmChoiceMap map;
    map[0] = booleanChoice();
    return map;
  }
  QIcon icon() const override {
    return QIcon();
  }
};

/**
 * \class MeshSelectLabeled
 *
 * Select all labeled vertices in the current mesh.
 */
class LGXCORE_EXPORT MeshSelectLabeled : public MeshProcess {
public:
  MeshSelectLabeled(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  bool operator()(const ParmList& parms) override
  {
    if(!checkState().mesh(MESH_NON_EMPTY | MESH_SHOW_MESH))
      return false;
    bool replace = parmToBool(parms[0]);
    return (*this)(currentMesh(), replace);
  }

  bool operator()(Mesh* m, bool replace);

  QString folder() const override {
    return "Selection";
  }
  QString name() const override {
    return "Select Labeled";
  }
  QString description() const override {
    return "Add to or replace the selection with the labeled vertices.";
  }
  QStringList parmNames() const override {
    return QStringList() << "Replace selection";
  }
  QStringList parmDescs() const override {
    return QStringList() << "Replace selection";
  }
  ParmList parmDefaults() const override {
    return ParmList() << "No";
  }
  ParmChoiceMap parmChoice() const override
  {
    ParmChoiceMap map;
    map[0] = booleanChoice();
    return map;
  }
  QIcon icon() const override {
    return QIcon();
  }
};

/**
 * \class MeshSelectLabel
 */
class LGXCORE_EXPORT MeshSelectLabel : public MeshProcess {
public:
  MeshSelectLabel(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  bool operator()(const ParmList& parms) override
  {
    if(!checkState().mesh(MESH_NON_EMPTY | MESH_SHOW_MESH))
      return false;
    bool replace = parmToBool(parms[0]);
    bool ok;
    int label = parms[1].toInt(&ok);
    if(not ok)
      return setErrorMessage(QString("Parameter 'Label' is not a valid integer value"));
    return (*this)(currentMesh(), replace, label);
  }

  bool operator()(Mesh* m, bool replace, int label);

  QString folder() const override {
    return "Selection";
  }
  QString name() const override {
    return "Select Label";
  }
  QString description() const override
  {
    return "Add to or replace the selection with the vertices of a given label (0 for current label).";
  }
  QStringList parmNames() const override
  {
    return QStringList() << "Replace selection"
                         << "Label (0 for current)";
  }
  ParmList parmDefaults() const override
  {
    return ParmList() << false
                      << 0;
  }
  ParmChoiceMap parmChoice() const override
  {
    ParmChoiceMap map;
    map[0] = booleanChoice();
    return map;
  }
  QIcon icon() const override {
    return QIcon();
  }
};

/**
 * \class MeshUnselectLabel MeshProcessSelection.hpp <MeshProcessSelection.hpp>
 *
 * Unselect all the vertices having a given label.
 */
class LGXCORE_EXPORT MeshUnselectLabel : public MeshProcess {
public:
  MeshUnselectLabel(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  bool operator()(const ParmList& parms) override
  {
    if(!checkState().mesh(MESH_NON_EMPTY | MESH_SHOW_MESH))
      return false;
    return (*this)(currentMesh(), parms[0].toInt());
  }

  bool operator()(Mesh* m, int label);

  QString folder() const override {
    return "Selection";
  }
  QString name() const override {
    return "Unselect Label";
  }
  QString description() const override
  {
    return "Remove the vertices of a given label (0 for current label) from the selection.";
  }
  QStringList parmNames() const override {
    return QStringList() << "Label (0 for current)";
  }
  QStringList parmDescs() const override {
    return QStringList() << "Label (0 for current)";
  }
  ParmList parmDefaults() const override {
    return ParmList() << "0";
  }
  QIcon icon() const override {
    return QIcon();
  }
};

/**
 * \class MeshSelectClip MeshProcessSelection.hpp <MeshProcessSelection.hpp>
 *
 * Select all vertices within the clipped region.
 */
class LGXCORE_EXPORT MeshSelectClip : public MeshProcess {
public:
  MeshSelectClip(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  bool operator()(const ParmList& ) override
  {
    if(!checkState().mesh(MESH_NON_EMPTY | MESH_SHOW_MESH))
      return false;
    return (*this)(currentMesh());
  }

  bool operator()(Mesh* m);

  QString folder() const override {
    return "Selection";
  }
  QString name() const override {
    return "Select Clip Region";
  }
  QString description() const override {
    return "Add vertices in clip region to selection.";
  }
  QStringList parmNames() const override {
    return QStringList();
  }
  QStringList parmDescs() const override {
    return QStringList();
  }
  QIcon icon() const override {
    return QIcon();
  }
};

/**
 * \class MeshSelectWholeLabelExtend MeshProcessSelection.hpp <MeshProcessSelection.hpp>
 *
 * Extent the current selection so each label having at least one vertex
 * selected will be fully selected.
 */
class LGXCORE_EXPORT MeshSelectWholeLabelExtend : public MeshProcess {
public:
  MeshSelectWholeLabelExtend(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  bool operator()(const ParmList& ) override
  {
    if(!checkState().mesh(MESH_NON_EMPTY | MESH_SHOW_MESH))
      return false;
    return (*this)(currentMesh());
  }

  bool operator()(Mesh* m);

  QString folder() const override {
    return "Selection";
  }
  QString name() const override {
    return "Extend to Whole Cells";
  }
  QString description() const override {
    return "Extend Selection to Whole Cells";
  }
  QStringList parmNames() const override {
    return QStringList();
  }
  QStringList parmDescs() const override {
    return QStringList();
  }
  QIcon icon() const override {
    return QIcon();
  }
};

/**
 * \class MeshSelectDuplicateCells MeshProcessSelection.hpp <MeshProcessSelection.hpp>
 *
 * Select vertices if the region with their label is not contiguous (e.g. will be seen as more than one cell).
 */
class LGXCORE_EXPORT MeshSelectDuplicateCells : public MeshProcess {
public:
  MeshSelectDuplicateCells(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  bool operator()(const ParmList& ) override
  {
    if(!checkState().mesh(MESH_NON_EMPTY | MESH_SHOW_MESH))
      return false;
    return (*this)(currentMesh());
  }

  bool operator()(Mesh* m);

  QString folder() const override {
    return "Selection";
  }
  QString name() const override {
    return "Select Duplicate Cells";
  }
  QString description() const override {
    return "Select cells with duplicate labels.";
  }
  QStringList parmNames() const override {
    return QStringList();
  }
  QStringList parmDescs() const override {
    return QStringList();
  }
  QIcon icon() const override {
    return QIcon();
  }
};

/**
 * \class ExtendByConnectivity MeshProcessSelection.hpp <MeshProcessSelection.hpp>
 *
 * Extend the current selection to all vertices that are connected to a
 * currently selected vertex.
 */
class LGXCORE_EXPORT ExtendByConnectivity : public MeshProcess {
public:
  ExtendByConnectivity(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  bool operator()(const ParmList& ) override
  {
    if(!checkState().mesh(MESH_NON_EMPTY))
      return false;
    return operator()(currentMesh());
  }

  bool operator()(Mesh* m);

  QString folder() const override {
    return "Selection";
  }
  QString name() const override {
    return "Extend by Connectivity";
  }
  QString description() const override {
    return "Extend the selection to connected regions";
  }
  QStringList parmNames() const override {
    return QStringList();
  }
  QStringList parmDescs() const override {
    return QStringList();
  }
  QIcon icon() const override {
    return QIcon(":/images/SelectConnected.png");
  }
};
///@}
} // namespace process
} // namespace lgx

#endif
