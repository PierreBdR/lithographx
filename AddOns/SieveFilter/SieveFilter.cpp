/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "SieveFilter.hpp"

#include <Algorithms.hpp>
#include <Information.hpp>
#include <Progress.hpp>
#include <Store.hpp>

#include <QFile>
#include <QTextStream>

#include <algorithm>
#include <cmath>
#include <cstdint>
#include <limits>
#include <unordered_set>

#include "Sieve2D.hpp"
#include "Sieve3D.hpp"

namespace lgx {
namespace process {

using namespace sieve;

template <typename Sieve>
bool SieveFilter::applySieve(Sieve& sieve, HVecUS& output, sieve::FILTER_TYPE filter, intptr_t int_area, Progress& progress)
{
  progress.setText("Filtering extrema ...");
  progress.setMaximum(2*int_area+2);
  if(not progress.advance(int_area))
    userCancel();

  auto reportExpand = [&progress, int_area, this](int i) -> bool {
    if(not progress.advance(int_area + i))
      userCancel();
    return true;
  };

  /*
   *Information::out << "  Algorithm initialized with " << sieve._minima.size() << " minima and "
   *  << sieve._maxima.size() << " maxima" << endl;
   *Information::out << "     min size minima: " << sieve._min_min_area << endl;
   *Information::out << "     min size maxima: " << sieve._max_min_area << endl;
   */

  auto nbExt = QString::number(sieve.nbExtrema(filter));
  auto N = nbExt.size();
  auto nbSpaces = N / 3;
  if(nbSpaces*3 == nbExt.size()) nbSpaces--;
  for(size_t i = 0 ; i < nbSpaces ; ++i)
    nbExt.insert(N - 3*(i+1), ' ');
  auto text = QString("Sieving through %1 extrema").arg(nbExt);

  progress.setText(text);
  SETSTATUS(text);

  if(not sieve.expand(int_area, filter, reportExpand))
    return setErrorMessage(sieve.errorString());
  Information::out << "  Algorithm completely ran" << endl;
  if(not sieve.writeTo(output))
    return setErrorMessage(sieve.errorString());
  if(not progress.advance(2*int_area+1))
    userCancel();
  Information::out << "  Image written to the output" << endl;
  return true;
}

bool SieveFilter::operator()(const Store* input, Store* output, const QString type_str, float size, bool fully_connected)
{
  if(size < 0)
    return setErrorMessage("Size must be positive");

  auto filter = parseFilterType(type_str);

  // Start the progress bar
  const Stack* stack = output->stack();
  Progress progress(QString("Running Recursive Median Sieve on stack %1").arg(stack->userId()), 0);

  // Get the input and output images, note they may be the same
  const HVecUS& src = input->data();
  HVecUS& dst = output->data();

  progress.setText("Initializing Sieve algorithm ...");

  if(stack->is2D()) {
    Point2u size2d = stack->size2D();
    Point2f step2d = stack->step2D();
    float area = step2d.x() * step2d.y();
    auto int_area = intptr_t(std::ceil(size / area));

    progress.setMaximum(200);

    auto reportInit = [&progress, this](int i) -> bool {
      if(not progress.advance(i))
        this->userCancel();
      return true;
    };

    Information::out << "Filtering with a size of " << int_area << " pixels" << endl;
    Sieve2D sieve2d(src, size2d, filter, (fully_connected ? NDIR_8 : NDIR_4), reportInit);
    if(sieve2d.error())
      return setErrorMessage(sieve2d.errorString());

    if(not applySieve(sieve2d, dst, filter, int_area, progress))
      return false;
  } else {
    Point3f step = stack->step();
    float volume = step.x() * step.y() * step.z();
    auto int_area = intptr_t(std::ceil(size / volume));

    progress.setMaximum(200);

    auto reportInit = [&progress, this](int i) -> bool {
      if(not progress.advance(i))
        this->userCancel();
      return true;
    };

    Information::out << "Filtering with a size of " << int_area << " pixels" << endl;
    Sieve3D sieve3d(src, stack->size(), filter, (fully_connected ? NDIR_26 : NDIR_6), reportInit);
    if(sieve3d.error())
      return setErrorMessage(sieve3d.errorString());

    if(not applySieve(sieve3d, dst, filter, int_area, progress))
      return false;
  }

  // Tell LithoGraphX to update the stack
  output->changed();
  // Copy the meta data for the stack in case store is different
  output->copyMetaData(input);

  // Print message in status bar
  SETSTATUS(QString("Processed Recursive Median Sieve on stack %1").arg(stack->userId()));

  return true;
}

} // namespace process
} // namespace lgx
