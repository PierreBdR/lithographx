/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef LGXCOMPILED_HPP
#define LGXCOMPILED_HPP

#include <LGXConfig.hpp>
#include <Version.hpp>

#include <QList>
#include <QStringList>

#if CORE_COMPILE
#  include <Core/CoreVersion.hpp>
#endif

#if PYTHON_COMPILE
#  include <python/PythonVersion.hpp>
#endif

namespace lgx {

namespace {
QStringList compiledModules = {
#if CORE_COMPILE
  "lgxCore",
#endif
#if PYTHON_COMPILE
  "lgxPython"
#endif
};
QList<util::Version> compiledModulesVersion = {
#if CORE_COMPILE
  util::Version::fromInt(LGX_CORE_VERSION),
#endif
#if PYTHON_COMPILE
  util::Version::fromInt(LGX_PYTHON_VERSION),
#endif
};

QStringList compiledModulesDescriptions = {
#if CORE_COMPILE
  "Main package, including processes always compiled in LithoGraphX.",
#endif
#if PYTHON_COMPILE
  "Interface to the Python language to create macros or processes.",
#endif
};


} // namespace

} // namespace lgx

#endif // LGXCOMPILED_HPP
