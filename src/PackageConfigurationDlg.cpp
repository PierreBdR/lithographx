/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "PackageConfigurationDlg.hpp"

#include <Dir.hpp>
#include <Information.hpp>
#include <PackageArchive.hpp>
#include <PluginManager.hpp>

#include <PackageArchive.hpp>
#include <QCoreApplication>
#include <QDialogButtonBox>
#include <QFileDialog>
#include <QInputDialog>
#include <QItemSelectionModel>
#include <QMessageBox>
#include <QMimeData>
#include <QProcess>
#include <QProgressDialog>
#include <QSettings>
#include <QStandardPaths>
#include <QTextEdit>
#include <QThread>
#include <QTreeView>
#include <QUrl>

#ifdef WIN32
#  include <Wincon.h>
#else
#  include <signal.h>
#endif

namespace lgx {
namespace gui {

namespace {

template <typename Fct>
bool runLgxPack(const QString& title,
                const QString& actionName,
                Fct fct,
                QWidget *parent)
{
  RunningPackaging dlg(title, actionName, parent);
  dlg.setModal(true);

  auto ui = PackagingUi {
    [&dlg]() { return dlg.progress(); },
    {},
    [&dlg](const QString& s) { dlg.step(s); },
    [&dlg](const QString& s, bool paused) { dlg.error(s, paused); },
    [&dlg]() { dlg.ok(); },
    [&dlg](const QString& msg) { return dlg.yesNo(msg); }
  };

  dlg.show();

  bool success = fct(ui);
  if(success)
    dlg.finished();
  if(dlg.isVisible())
    dlg.exec();
  return success;
}

} // namespace

InstallPackageEvent::InstallPackageEvent(QString path)
  : QEvent(QEvent::User)
  , _path(path)
{ }

InstallPackageEvent::~InstallPackageEvent()
{ }

RunningPackaging::RunningPackaging(const QString& title,
                                   const QString& actionName,
                                   QWidget *parent,
                                   Qt::WindowFlags fl)
  : QDialog(parent, fl)
  , isCanceled(false)
  , good(":/images/GreenCheck.png")
  , bad(":/images/RedCross.png")
{
  ui.setupUi(this);
  setWindowTitle(title);
  ui.actionBox->setTitle(actionName);
  auto ok = ui.buttonBox->button(QDialogButtonBox::Ok);
  ok->setEnabled(false);
  steps = new QGridLayout(ui.actionBox);
  good = good.scaled(16, 16);
  bad = bad.scaled(16, 16);
}

int RunningPackaging::exec()
{
  auto ok = ui.buttonBox->button(QDialogButtonBox::Ok);
  if(ok) ok->setEnabled(true);
  auto cancel = ui.buttonBox->button(QDialogButtonBox::Cancel);
  if(cancel) cancel->setEnabled(false);
  ui.progressBar->setMaximum(1);
  ui.progressBar->setValue(1);
  return QDialog::exec();
}

void RunningPackaging::reject()
{
  isCanceled = true;
}

void RunningPackaging::finished()
{
  auto lab = new QLabel("Success!", this);
  lab->setAlignment(Qt::AlignHCenter);
  steps->addWidget(lab, steps->rowCount(), 0, 1, 2);
}

void RunningPackaging::step(const QString& name)
{
  auto nbRow = steps->rowCount();
  steps->addWidget(new QLabel(name, this), nbRow, 0);
}

void RunningPackaging::ok()
{
  auto nbRow = steps->rowCount();
  auto lab = new QLabel(this);
  lab->setPixmap(good);
  if(nbRow > 0) --nbRow;
  steps->addWidget(lab, nbRow, 1);
}

void RunningPackaging::error(const QString& err, bool paused)
{
  auto nbRow = steps->rowCount();
  auto lab = new QLabel(this);
  lab->setPixmap(bad);
  if(nbRow > 0) --nbRow;
  steps->addWidget(lab, nbRow, 1);
  ++nbRow;
  QString realError = (isCanceled ? "Canceled by user.\n" : "") + err;
  auto errLab = new QLabel(realError, this);
  errLab->setStyleSheet("QLabel { color: red; }");
  errLab->setTextInteractionFlags(Qt::TextSelectableByMouse | Qt::TextSelectableByKeyboard | Qt::TextBrowserInteraction);
  steps->addWidget(errLab, nbRow, 0, 1, 2);
  if(paused) {
    this->exec();
  }
}

bool RunningPackaging::yesNo(const QString& msg)
{
  auto answer = QMessageBox::question(this, "Question from packaging.", msg);
  return answer == QMessageBox::Yes;
}

bool RunningPackaging::progress()
{
  QCoreApplication::processEvents();
  return not isCanceled;
}

PackageListModel::PackageListModel(QObject *parent)
{
  good = QPixmap(":/images/GreenCheck.png").scaled(16, 16);
  bad = QPixmap(":/images/RedCross.png").scaled(16, 16);
  makePackageList();
}

void PackageListModel::makePackageList()
{
  packageList = manager.packages();
  packageList.sort(Qt::CaseInsensitive);

  auto usr = manager.userPackages();
  isUserPackage.clear();
  isUserPackage.reserve(packageList.size());
  isValid.clear();
  isValid.reserve(packageList.size());

  for(const auto& pkg: packageList) {
    isUserPackage << usr.contains(pkg);
    isValid << manager.check(pkg);
  }
}

void PackageListModel::updatePackages()
{
  beginResetModel();
  manager.loadPackages();
  makePackageList();
  endResetModel();
}

int PackageListModel::columnCount(const QModelIndex &parent) const
{
  return 3;
}

int PackageListModel::rowCount(const QModelIndex &parent) const
{
  if(parent.isValid())
    return 0;
  return packageList.size();
}

QVariant PackageListModel::headerData(int section, Qt::Orientation orientation, int role) const
{
  if(orientation == Qt::Horizontal and role == Qt::DisplayRole) {
    switch(section) {
      case 0:
        return "Name";
      case 1:
        return "Type";
      case 2:
        return "Valid";
      default:
        break;
    }
  }
  return QVariant();
}

QVariant PackageListModel::data(const QModelIndex &index, int role) const
{
  if(not index.isValid()) return QVariant();
  switch(role) {
    case Qt::DisplayRole:
      switch(index.column()) {
        case 0:
          return packageList.at(index.internalId());
        case 1:
          if(isUserPackage.at(index.internalId()))
            return "User";
          return "System";
        default:
          break;
      }
      break;
    case Qt::DecorationRole:
      if(index.column() == 2) {
        if(isValid[index.internalId()])
          return good;
        return bad;
      }
      break;
  }
  return QVariant();
}

QModelIndex PackageListModel::index(int row, int column, const QModelIndex &parent) const
{
  return createIndex(row, column, row);
}

Qt::ItemFlags PackageListModel::flags(const QModelIndex &index) const
{
  if(index.isValid())
    return Qt::ItemIsSelectable | Qt::ItemIsEnabled | Qt::ItemNeverHasChildren | Qt::ItemIsDropEnabled;
  return Qt::ItemIsDropEnabled;
}

QModelIndex PackageListModel::parent(const QModelIndex &index) const
{
  return QModelIndex();
}

const Package* PackageListModel::package(const QModelIndex& idx)
{
  if(idx.isValid())
    return manager.package(packageList.at(idx.internalId()));
  return nullptr;
}

bool PackageListModel::canDropMimeData(const QMimeData* data, Qt::DropAction action,
                                       int row, int column, const QModelIndex& parent) const
{
  if(action != Qt::CopyAction)
    return false;
  if(data->hasUrls()) {
    auto urls = data->urls();
    if(urls.size() != 1)
      return false;
    auto url = urls.front();
    if(not url.isLocalFile())
      return false;
    auto path = url.toLocalFile();
    QFileInfo fi(path);
    auto ext = fi.suffix();
    if((fi.isFile() and (ext == "lgxps" or ext == "lgxpb")) or fi.isDir()) {
      return true;
    }
  }
  return false;
}

bool PackageListModel::dropMimeData(const QMimeData* data, Qt::DropAction action,
                                    int row, int column, const QModelIndex & parent)
{
  if(not canDropMimeData(data, action, row, column, parent))
    return false;
  auto path = data->urls().front().toLocalFile();
  QFileInfo fi(path);
  Information::out << "Dropped package path '" << fi.absolutePath() << "'" << endl;
  emit droppedPackage(fi.absoluteFilePath());
  return true;
}

Qt::DropActions PackageListModel::supportedDropActions() const
{
  return Qt::CopyAction;
}

QStringList PackageListModel::mimeTypes() const
{
  return { "text/uri-list" };
}


PackageConfigurationDlg::PackageConfigurationDlg(QWidget* parent, Qt::WindowFlags f)
  : QDialog(parent, f)
{
  ui.setupUi(this);

  pkg_model = new PackageListModel(this);
  ui.packageList->setModel(pkg_model);

  // Add extra buttons to button box
  auto* bbox = ui.buttonBox;
  install_file_btn = bbox->addButton("Install File ...", QDialogButtonBox::ActionRole);
  install_folder_btn = bbox->addButton("Install Folder ...", QDialogButtonBox::ActionRole);
  uninstall_btn = bbox->addButton("Uninstall ...", QDialogButtonBox::ActionRole);
  create_btn = bbox->addButton("Create ...", QDialogButtonBox::ActionRole);

  auto selModel = ui.packageList->selectionModel();

  connect(selModel, &QItemSelectionModel::currentChanged,
          this, &PackageConfigurationDlg::packageList_changeCurrent);
  connect(pkg_model.data(), &PackageListModel::droppedPackage,
          this, &PackageConfigurationDlg::delayedInstallPackage);
  connect(parent, SIGNAL(installPackage(const QString&)),
          this, SLOT(delayedInstallPackage(const QString&)));
}

void PackageConfigurationDlg::packageList_changeCurrent(const QModelIndex& current, const QModelIndex& previous)
{
  auto pkg = pkg_model->package(current);
  ui.packageDescription->setHtml(makeText(pkg));
}

void PackageConfigurationDlg::on_buttonBox_clicked(QAbstractButton* btn)
{
  if(btn == install_file_btn) {
    if(curFolder.isEmpty()) curFolder = util::currentPath();
    QString filters = "Source Package (*.lgxps);;Binary Package (*.lgxpb);;Any file (*)";
    QString filename = QFileDialog::getOpenFileName(this, "Choose LithoGraphX package",
                                                    curFolder, filters);
    if(filename.isEmpty())
      return;
    curFolder = QFileInfo(filename).absolutePath();
    installPackage(filename);
  } else if(btn == install_folder_btn) {
    if(curFolder.isEmpty()) curFolder = util::currentPath();
    QString filename = QFileDialog::getExistingDirectory(this, "Choose LithoGraphX package", curFolder);
    if(filename.isEmpty())
      return;
    curFolder = filename;
    installPackage(filename);
  } else if(btn == uninstall_btn) {
    PackageManager manager;
    QStringList pkgs = manager.userPackages();
    if(pkgs.empty()) {
      QMessageBox::information(this, "Cannot uninstall package", "There are no user packages installed.");
      return;
    }
    pkgs.sort(Qt::CaseInsensitive);


    auto listIdx = 0;
    auto selModel = ui.packageList->selectionModel();
    auto curIdx = selModel->currentIndex();
    auto curPkg = pkg_model->package(curIdx);
    listIdx = pkgs.indexOf(curPkg->name());
    if(listIdx < 0)
      listIdx = 0;

    bool ok;
    auto pkg = QInputDialog::getItem(this, "Choose package to uninstall", "List of user packages:", pkgs, listIdx, false, &ok);
    if(not ok)
      return;

    Information::out << "Uninstalling " << pkg << endl;
    uninstallPackage(pkg);
  } else if(btn == create_btn) {
    PackageManager manager;
    QStringList pkgs = manager.packages();
    if(pkgs.empty()) {
      QMessageBox::information(this, "Cannot create package", "There are no packages installed.");
      return;
    }
    pkgs.sort(Qt::CaseInsensitive);


    auto listIdx = 0;
    auto selModel = ui.packageList->selectionModel();
    auto curIdx = selModel->currentIndex();
    auto curPkg = pkg_model->package(curIdx);
    listIdx = pkgs.indexOf(curPkg->name());
    if(listIdx < 0)
      listIdx = 0;

    bool ok;
    auto pkg = QInputDialog::getItem(this, "Choose package to create", "List of packages:", pkgs, listIdx, false, &ok);
    if(not ok)
      return;

    if(curFolder.isEmpty()) curFolder = util::currentPath();
    QString output_dir = QFileDialog::getExistingDirectory(this, "Choose where to save the package", curFolder);
    if(output_dir.isEmpty()) return;
    curFolder = output_dir;

    Information::out << "Creating " << pkg << endl;
    createPackage(pkg, output_dir);
  }
}

void PackageConfigurationDlg::createPackage(const QString& pkg, const QString& output_dir)
{
  runLgxPack("Creating Package",
             QString("Creating package '%1'").arg(pkg),
             [&pkg, &output_dir](const PackagingUi& ui) -> bool {
               return not lgx::createPackage(pkg, output_dir, true, ui).isEmpty();
             }, this);
}

void PackageConfigurationDlg::installPackage(const QString& pkg)
{
  if(runLgxPack("Installing Package",
                QString("Installing package '%1'.").arg(pkg),
                [&pkg](const PackagingUi& ui) -> bool {
                  return lgx::installPackage(pkg, true, false, ui);
                }, this))
  {
    auto pluginManager = process::pluginManager();
    pluginManager->loadNewExtensions();
    pluginManager->refreshAll();
    pkg_model->updatePackages();
  }
}

void PackageConfigurationDlg::uninstallPackage(const QString& pkg)
{
  auto plugManager = process::pluginManager();
  auto packManager = lgx::PackageManager();
  plugManager->unloadMacros();
  if(runLgxPack("Uninstalling Package",
                QString("Uninstalling package '%1'.").arg(pkg),
                [&packManager,&pkg](const PackagingUi& ui) -> bool {
                  bool ok;
                  QStringList deps;
                  std::tie(ok, deps) = packManager.canUninstall(pkg);
                  if(not ok) {
                    if(deps.isEmpty())
                      ui.error(QString("Cannot uninstall package: it doesn't exist."));
                    else {
                      QStringList fullDeps;
                      for(const auto& d : deps) {
                        auto p = packManager.package(d);
                        fullDeps << QString(" - %1: %2").arg(d).arg(p->dependencies()->toString());
                      }
                      ui.error(QString("Cannot uninstall package, or the following will break:\n%1")
                               .arg(fullDeps.join("\n")));
                    }
                    return false;
                  }
                  return lgx::uninstallPackage(pkg, ui);
                }, this)) {
    pkg_model->updatePackages();
  }
  plugManager->loadMacros();
  plugManager->refreshProcesses();
}

void PackageConfigurationDlg::showEvent(QShowEvent *e)
{
  pkg_model->updatePackages();
  QDialog::showEvent(e);
}

QString PackageConfigurationDlg::makeText(const Package* pkg)
{
  if(not pkg)
    return "<p>No Package Selected</p>";

  auto name = pkg->name();
  auto type = pkg->isUserPackage() ? "User" : "System";
  auto desc = pkg->description();
  auto ver = pkg->version().toString();
  auto deps = pkg->dependencies()->toString();
  auto src = pkg->sourcePackage();
  if(src.isEmpty()) src = "-- Not Installed --";
  auto files = pkg->files();

  auto filelist = QString("<tt><ul><li>%1</li></ul></tt>").arg(files.join("</li><li>"));
  if(files.isEmpty())
    filelist = "Package compiled in LithoGraphX -- no installed files.";

  return QString("<h2>%1 (v. %2)</h2>"
                 "<p>%3</p>"
                 "<dl>"
                 "<dt>Type</dt><dd>%4</dd>"
                 "<dt>Dependencies</dt><dd>%5</dd>"
                 "<dt>Source Package</dt><dd>%6</dd>"
                 "<dt>Files installed</dt><dd>%7</dd>"
                 "</dl>").arg(name).arg(ver).arg(desc).arg(type).arg(deps).arg(src).arg(filelist);
}

bool PackageConfigurationDlg::event(QEvent *event)
{
  auto ipe = dynamic_cast<InstallPackageEvent*>(event);
  if(ipe) {
      installPackage(ipe->path());
      ipe->accept();
      return true;
  }
  return QDialog::event(event);
}

void PackageConfigurationDlg::delayedInstallPackage(const QString& path)
{
  if(not isVisible())
    show();
  QCoreApplication::postEvent(this, new InstallPackageEvent(path));
}

} // namespace gui
} // namespace lgx
