/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/
#ifndef LIBRARIES_HPP
#define LIBRARIES_HPP

#include <LGXConfig.hpp>

#include <memory>
#include <typeinfo>

#include <QHash>
#include <QString>
#include <QPixmap>

namespace lgx {

struct LGX_EXPORT LibraryDescription
{
  QString name;        ///< Name of the declared library
  QString version;     ///< Version compiled against
  QString description; ///< Description, may use HTML
  QString website;     ///< Website
  QString logo;        ///< Path to the logo
};

class LGX_EXPORT LibraryDeclaration
{
public:
  LibraryDeclaration(const LibraryDescription& lib);
  LibraryDeclaration(LibraryDeclaration&& copy) = default;
  LibraryDeclaration(const LibraryDeclaration& copy) = delete;

  LibraryDeclaration& operator=(LibraryDeclaration&&) = default;
  LibraryDeclaration& operator=(const LibraryDeclaration&) = delete;

  ~LibraryDeclaration();

  static void reset();
  static std::vector<LibraryDescription> libraries();

private:
  LibraryDescription _lib;
  static std::vector<std::pair<LibraryDescription,int>> _libraries;
};

}

#define DECLARE_LIBRARY(name, version, description, website, logo) \
  static lgx::LibraryDeclaration className ## declaration(LibraryDescription{name, version, description, website, logo});

#endif // LIBRARIES_HPP

