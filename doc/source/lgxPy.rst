Module lgxPy
============

.. py:module:: lgxPy

This module defines how to interact with LithoGraphX itself: various classes,
other processes, ...

Accessing Processes
-------------------

The module defines three objects of class ``ProcessMaker``:

.. py:data:: Stack

    Object of type :py:class:`ProcessMaker` for the stack processes

.. py:data:: Mesh

    Object of type :py:class:`ProcessMaker` for the mesh processes

.. py:data:: Global

    Object of type :py:class:`ProcessMaker` for the global processes

.. py:class:: ProcessMaker

  This class represent a list of processes of a given type.

  .. py:method:: __getattr__(name)

      Retrieve a process. If it doesn't exist, this throws
      a :py:exc:`NoSuchProcess` exception.

      :param name: Name of the process.
      :returns: An object of type :py:class:`ProcessRunner`.

  .. py:method:: __str__()

      :returns: the type of process this object will make.

  .. py:method:: __dir__()

      :returns: The list of all processes of this type.

.. py:class:: ProcessRunner

    This class represent a process.

    .. py:method:: __call__(...)

        Call the process with the given arguments.

        :returns: Always true.
        :raises UserCancelException: Thrown if the user pressed the ``Cancel``
          button on a progress bar.
        :raises ProcessError: If the process (or a sub-process) returns false,
          this exception is raised instead.
        :raises StdException: If the process raises a standard exception, this
          will be translated into this exception.

    .. py:attribute:: lastParms

        List of parameters last used for this process, or the default
        parameters if the process has never been used.

    .. py:attribute:: defaultParms()

        List of default parameters, as defined by the implementation of the
        process.

    .. py:attribute:: nbParms

        Number of parameters for this process.

    .. py:attribute:: folder

        Folder this process is stored in.

    .. py:method:: __str__()

        :returns: The name of this process as ``Type.Name``.

Exceptions
----------

.. py:exception:: UserCancelException

    Exception launched when the user press the ``Cancel`` button

.. py:exception:: ProcessError

    If a process fails (i.e. returns false), this is translated into this
    exception.

.. py:exception:: BadProcessType

    If the user is trying to access a process of the wrong type.

.. py:exception:: NoSuchProcess

    If trying to access a process that doesn't exist.

.. py:exception:: StdException

    If an exception deriving ``std::exception`` is caught, it is translated into
    this exception (except for the ones defined above).

Utilities
---------

.. py:class:: Point3u

    A 3D point with unsigned integers.

    .. py:attribute:: x

      Get/set the first dimension.

    .. py:attribute:: y

      Get/set the second dimension.

    .. py:attribute:: z

      Get/set the third dimension.

.. py:class:: Point3f

    A 3D point with floating point values.

    .. py:attribute:: x

      Get/set the first dimension.

    .. py:attribute:: y

      Get/set the second dimension.

    .. py:attribute:: z

      Get/set the third dimension.

.. py:class:: ImageInfo

    Class used to parse an image and get information such as number of slices,
    channels, timepoints, ...

    .. py:attribute:: filename

        Name of the file containing the image.

    .. py:attribute:: size

        Number of pixels in each dimension, given as a :py:class:`Point3u`.

    .. py:attribute:: step

        Size a of single voxel, given as a :py:class:`Point3f`.

    .. py:attribute:: origin

        Position of the origin of the stack (e.g. the front, lower, left
        corner), given as a :py:class:`Point3f`.

    .. py:attribute:: nb_channels

        Number of channels in the file.

    .. py:attribute:: nb_timepoints

        Number of time points in the image.

    .. py:attribute:: labels

        Whether the image is a labeled image or not.

        :Note: This is only valid for files saved by LithoGraphX, otherwise this
            is always false.

