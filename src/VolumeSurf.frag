uniform bool blending;
uniform vec3 imageSize;
uniform vec3 origin;

uniform bool use_colormap;

smooth centroid in vec3 texPos;
/*
 *in vec3 normal;
 *in vec3 lightDir[4], halfVector[4];
 */

void setColor()
{
  vec3 realTexCoord = (texPos - origin) / imageSize;
  if((realTexCoord.x < 0.0) || (realTexCoord.x > 1.0) ||
     (realTexCoord.y < 0.0) || (realTexCoord.y > 1.0) ||
     (realTexCoord.z < 0.0) || (realTexCoord.z > 1.0))
  {
    discard;
    return;
  }
  vec4 col = color(realTexCoord);

  col.a = 1-col.a;
  if(col.a > 0)
    col.rgb /= col.a;
  if(!blending)
    col.a = 1.0;
  col = light(col);
  gl_FragColor = col;
}

