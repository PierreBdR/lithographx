/*out vec3 normal;*/
/*out vec3 lightDir[4], halfVector[4];*/
smooth centroid out vec4 realPos;
uniform float shift;
uniform float offset;

smooth centroid out float depthProj;

void main()
{
  setTexCoord();

  light();
  /*normal = normalize(gl_NormalMatrix * gl_Normal);*/
  /*
   *for(int i = 0 ; i < 4 ; ++i)
   *{
   *  lightDir[i] = normalize(vec3(gl_LightSource[i].position - gl_Vertex));
   *  halfVector[i] = normalize(gl_LightSource[i].halfVector.xyz);
   *}
   */

  vec4 v = gl_Vertex - shift*vec4(gl_Normal,0.0);
  vec4 vproj = gl_ModelViewProjectionMatrix * v - vec4(0.0, 0.0, offset, 0.0);
  depthProj = vproj.z/vproj.w;
  realPos = gl_ModelViewProjectionMatrix * gl_Vertex - vec4(0.0, 0.0, offset, 0.0);
  gl_Position = vproj;
  gl_FrontColor = gl_Color;
  gl_ClipVertex = gl_ModelViewMatrix * gl_Vertex;
}

