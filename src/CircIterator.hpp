/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef CIRC_ITERATOR_H
#define CIRC_ITERATOR_H

#include <LGXConfig.hpp>

#include <iterator>

namespace lgx {
namespace util {
/**
 * \class CircIterator CircIterator.hpp <CircIterator.hpp>
 *
 * Creates a circular iterator from a range of forward iterators
 */
template <typename ForwardIterator> class CircIterator {
public:
  typedef std::forward_iterator_tag iterator_category;
  typedef typename std::iterator_traits<ForwardIterator>::value_type value_type;
  typedef typename std::iterator_traits<ForwardIterator>::difference_type difference_type;
  typedef typename std::iterator_traits<ForwardIterator>::pointer pointer;
  typedef typename std::iterator_traits<ForwardIterator>::reference reference;

  CircIterator() {
  }

  CircIterator(const ForwardIterator& f, const ForwardIterator& l, const ForwardIterator& c)
    : first(f)
    , last(l)
    , init(c)
    , cur(c)
  {
  }

  CircIterator(const ForwardIterator& f, const ForwardIterator& l)
    : first(f)
    , last(l)
    , init(l)
    , cur(l)
  {
  }

  CircIterator(const CircIterator& copy)
    : first(copy.first)
    , last(copy.last)
    , init(copy.init)
    , cur(copy.cur)
  {
  }

  CircIterator& operator++()
  {
    ++cur;
    if(cur == last)
      cur = first;
    if(cur == init)
      cur = last;
    return *this;
  }

  CircIterator operator++(int)
  {
    CircIterator temp(*this);
    this->operator++();
    return temp;
  }

  reference operator*() {
    return *cur;
  }
  pointer operator->() {
    return cur.operator->();
  }

  bool operator==(const ForwardIterator& other) const {
    return cur == other;
  }

  bool operator==(const CircIterator& other) const {
    return cur == other.cur;
  }

  bool operator!=(const ForwardIterator& other) const {
    return cur != other;
  }

  bool operator!=(const CircIterator& other) const {
    return cur != other.cur;
  }

  ForwardIterator base() const {
    return cur;
  }

protected:
  ForwardIterator first, last, init, cur;
};
} // namespace util
} // namespace lgx
#endif
