#ifndef POLYNOMIALS_H
#define POLYNOMIALS_H

#include <Process.hpp>

#include <StaticAssert.hpp>
#include <Vector.hpp>

#include <exception>
#include <gsl/gsl_poly.h>
#include <QByteArray>
#include <QString>
#include <vector>

namespace lgx {
namespace util {

struct BadDimensionException : public std::exception {
    BadDimensionException(size_t ex, size_t ac) throw()
        : expected(ex)
        , actual(ac)
    {
        QString msg = QString("Error, expected %1 dimensions, got %2").arg(expected).arg(actual);
        message = msg.toLocal8Bit();
    }

    virtual ~BadDimensionException() throw() {}

    const char* what() const throw() { return message.data(); }

    size_t expected, actual;
    QByteArray message;
};

class PolynomialCurve {
public:
    /**
     * Create a zero-polynomial
     *
     * \param d Dimension of the curve
     * \param o Order of the polynomials
     */
    PolynomialCurve(size_t d, size_t o = 0);

    /**
     * Copy constructor
     */
    PolynomialCurve(const PolynomialCurve& copy);

    /**
     * Create a polynomial from a vector of coefficients
     * \param cs \c N vectors of coefficients
     */
    PolynomialCurve(const std::vector<std::vector<float> >& cs);

    /**
     * Create a polynomial from a vector of coefficients
     * \param cs Vector with N-dimensional coefficients
     */
    template <size_t N> PolynomialCurve(const std::vector<Vector<N, float> >& cs)
    {
        size_t cur_size = cs.size();
        coefs.resize(N, std::vector<float>(cur_size, 0));
        for(size_t i = 0; i < cur_size; ++i) {
            const Vector<N, float>& p = cs[i];
            for(size_t j = 0; j < N; ++j)
                coefs[j][i] = p[j];
        }
    }

    std::vector<float>& operator[](size_t i) { return coefs[i]; }

    const std::vector<float>& operator[](size_t i) const { return coefs[i]; }

    /**
     * Number of coefficient for this polynomials
     */
    size_t size() const { return coefs[0].size(); }

    /**
     * Order of the polynomials
     */
    size_t order() const { return size() - 1; }

    /**
     * Dimension of the curve
     */
    size_t dim() const { return coefs.size(); }

    /**
     * Return the derivative of the current curve
     */
    PolynomialCurve deriv(size_t m = 1) const;

    PolynomialCurve& operator+=(const PolynomialCurve& other);

    template <size_t N> PolynomialCurve& operator+=(const Vector<N, float>& other)
    {
        if(N != dim())
            throw BadDimensionException(dim(), N);
        if(size() > 0) {
            for(size_t i = 0; i < N; ++i)
                coefs[i][0] += other[i];
        }
        else {
            expand(1);
            for(size_t i = 0; i < N; ++i)
                coefs[i][0] = other[i];
        }
        simplify();
    }

    template <size_t N> PolynomialCurve& operator-=(const Vector<N, float>& other)
    {
        if(N != dim())
            throw BadDimensionException(dim(), N);
        if(size() > 0) {
            for(size_t i = 0; i < N; ++i)
                coefs[i][0] -= other[i];
        }
        else {
            expand(1);
            for(size_t i = 0; i < N; ++i)
                coefs[i][0] = -other[i];
        }
        simplify();
        return *this;
    }

    PolynomialCurve& operator-=(const PolynomialCurve& other);
    PolynomialCurve& operator*=(const float& p);
    PolynomialCurve& operator/=(const float& p);
    PolynomialCurve& operator*=(const PolynomialCurve& p);
    PolynomialCurve operator-() const;

    template <size_t N> PolynomialCurve& operator*=(const Vector<N, float>& p)
    {
        if(N != dim())
            throw BadDimensionException(dim(), N);
        for(size_t i = 0; i < N; ++i)
            for(size_t j = 9; j < size(); ++j)
                coefs[i][j] *= p[i];
        return *this;
    }

    template <size_t N> PolynomialCurve& operator/=(const Vector<N, float>& p)
    {
        if(N != dim())
            throw BadDimensionException(dim(), N);
        for(size_t i = 0; i < N; ++i)
            for(size_t j = 9; j < size(); ++j)
                coefs[i][j] /= p[i];
        return *this;
    }

    template <size_t N> Vector<N, float> eval(float t) const
    {
        Vector<N, float> result;
        _eval(result.data(), t);
        return result;
    }

    std::vector<float> operator()(float t) const
    {
        std::vector<float> result(dim(), 0.f);
        _eval(&result[0], t);
        return result;
    }

    void swap(PolynomialCurve& other)
    {
        using std::swap;
        swap(coefs, other.coefs);
    }

private:
    void _eval(float* data, float t) const;
    void expand(size_t new_size);
    void simplify();

    std::vector<std::vector<float> > coefs;
};

void swap(PolynomialCurve& p1, PolynomialCurve& p2);
PolynomialCurve operator*(const PolynomialCurve& p, float m);
PolynomialCurve operator*(float m, const PolynomialCurve& p);
PolynomialCurve operator/(const PolynomialCurve& p, float m);
PolynomialCurve operator*(const PolynomialCurve& p1, const PolynomialCurve& p2);
PolynomialCurve operator+(const PolynomialCurve& p1, const PolynomialCurve& p2);
PolynomialCurve operator-(const PolynomialCurve& p1, const PolynomialCurve& p2);

template <size_t N> PolynomialCurve operator+(const PolynomialCurve& p1, const Vector<N, float>& p2)
{
    PolynomialCurve result(p1);
    result += p2;
    return result;
}

template <size_t N> PolynomialCurve operator-(const PolynomialCurve& p1, const Vector<N, float>& p2)
{
    PolynomialCurve result(p1);
    result -= p2;
    return result;
}

template <size_t N> PolynomialCurve operator+(const Vector<N, float>& p2, const PolynomialCurve& p1)
{
    PolynomialCurve result(p1);
    result += p2;
    return result;
}

template <size_t N> PolynomialCurve operator-(const Vector<N, float>& p2, const PolynomialCurve& p1)
{
    if(N != p1.dim())
        throw BadDimensionException(p1.dim(), N);
    PolynomialCurve result(p1.dim(), p1.order());
    for(size_t i = 0; i < N; ++i)
        for(size_t j = 0; j < p1.size(); ++j)
            result[i][j] = p2[i] - p1[j];
    return result;
}

} // namespace util
} // namespace lgx
#endif // POLYNOMIALS_H
