/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef SCALEBAR_HPP
#define SCALEBAR_HPP

#include <LGXConfig.hpp>
#include <GL.hpp>

#include <Geometry.hpp>
#include <LGXViewer/qglviewer.h>
#include <Parms.hpp>

#include <cmath>
#include <QColor>
#include <QFont>
#include <QString>
#include <string>

class QTextStream;

namespace lgx {

class LGX_EXPORT ScaleBar {
public:
  typedef util::Vector<2, size_t> Point2u;
  enum Position { Top, Bottom, Left, Right, TopLeft, BottomLeft, TopRight, BottomRight, Center };

  enum Orientation { Horizontal, Vertical };

  enum TextPosition { In, Out };

  ScaleBar();

  void setWantedSize(double ws)
  {
    if(ws > 0)
      _wantedSize = ws;
  }

  double wantedSize() const { return _wantedSize; }

  void setScale(double s)
  {
    if(s > 0)
      _scale = s;
  }

  double scale() const { return _scale; }

  void setUnit(QString u)
  {
    _unit = u;
    _displayUnit = !u.isEmpty();
  }

  QString unit() const { return _unit; }

  void setThickness(int th)
  {
    if(th < 1)
      _thickness = 1;
    else
      _thickness = th;
  }

  double thickness() const { return _thickness; }

  void setShiftBorder(const Point2u& pt) {
    _shiftBorder = pt;
  }

  const Point2u& shiftBorder() const { return _shiftBorder; }

  void setFont(const QFont& fnt) {
    _unit_font = fnt;
  }

  const QFont& font() const { return _unit_font; }

  void init(QGLViewer* viewer);
  void draw(QGLViewer* viewer, QPaintDevice* device = 0);

  void readParms(util::Parms& parms, QString section);
  void writeParms(QTextStream& pout, QString section);

  void scaleDrawing(double s);
  void restoreScale();

  void setSizeRange(double low, double high);
  double minimumSize() const { return _minSize; }
  double maximumSize() const { return _maxSize; }

  Orientation orientation;
  Position position;
  TextPosition textPosition;

  void loadDefaults();

protected:
  void findScale(double _unit_size);
  double _wantedSize;
  double _scale;
  QString _unit;
  bool _displayUnit;
  QFont _unit_font;
  double _thickness;
  Point2u _shiftBorder;
  bool _autoScale, _autoUnit;
  double _minSize, _maxSize;

  double _globalScale;

  // Saved _positions
  /*
   * double savedWantedSize;
   * double savedThickness;
   * Point2u savedShiftBorder;
   * int savedFontSize;
   * double savedMinSize;
   * double savedMaxSize;
   */
};
} // namespace lgx
#endif // SCALEBAR_HPP
