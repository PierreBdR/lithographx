/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef COLOREDITORDLG_HPP
#define COLOREDITORDLG_HPP

#include <LGXConfig.hpp>

#include <ui_ColorEditorDlg.h>

namespace lgx {
class Colors;
} // namespace lgx

class ColorEditorDlg : public QDialog {
  Q_OBJECT
public:
  ColorEditorDlg(lgx::Colors* colors, QWidget* parent = 0, Qt::WindowFlags f = 0);

protected slots:
  void on_colorsView_doubleClicked(const QModelIndex& idx);
  void on_buttonBox_clicked(QAbstractButton* btn);

protected:
  lgx::Colors* model;
  Ui::ColorEditorDlg ui;
};

#endif
