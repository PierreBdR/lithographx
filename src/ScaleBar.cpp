/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "ScaleBar.hpp"

#include "Colors.hpp"
#include "Information.hpp"

#include <cmath>
#include <QFontMetrics>
#include <QPainter>
#include <QPoint>
#include <QSettings>
#include <QTextStream>

namespace lgx {
using qglviewer::Camera;
using qglviewer::Vec;

QTextStream& operator<<(QTextStream& ts, ScaleBar::Position pos)
{
  switch(pos) {
    case ScaleBar::Top:
      ts << "Top";
      break;
    case ScaleBar::Bottom:
      ts << "Bottom";
      break;
    case ScaleBar::Left:
      ts << "Left";
      break;
    case ScaleBar::Right:
      ts << "Right";
      break;
    case ScaleBar::TopLeft:
      ts << "TopLeft";
      break;
    case ScaleBar::BottomLeft:
      ts << "BottomLeft";
      break;
    case ScaleBar::TopRight:
      ts << "TopRight";
      break;
    case ScaleBar::BottomRight:
      ts << "BottomRight";
      break;
    case ScaleBar::Center:
      ts << "Center";
      break;
  }
  return ts;
}

QTextStream& operator>>(QTextStream& ts, ScaleBar::Position& position)
{
  QString p;
  ts >> p;
  p = p.toLower();
  if(p == "top")
    position = ScaleBar::Top;
  else if(p == "bottom")
    position = ScaleBar::Bottom;
  else if(p == "left")
    position = ScaleBar::Left;
  else if(p == "right")
    position = ScaleBar::Right;
  else if(p == "topleft")
    position = ScaleBar::TopLeft;
  else if(p == "bottomleft")
    position = ScaleBar::BottomLeft;
  else if(p == "topright")
    position = ScaleBar::TopRight;
  else if(p == "bottomright")
    position = ScaleBar::BottomRight;
  else if(p == "center")
    position = ScaleBar::Center;
  else {
    Information::err << "Warning, unknown ScaleBar position '" << p << "', using BottomRight" << endl;
    position = ScaleBar::BottomRight;
  }
  return ts;
}

QTextStream& operator<<(QTextStream& ts, ScaleBar::Orientation orientation)
{
  switch(orientation) {
    case ScaleBar::Horizontal:
      ts << "Horizontal";
      break;
    case ScaleBar::Vertical:
      ts << "Vertical";
      break;
  }
  return ts;
}

QTextStream& operator>>(QTextStream& ts, ScaleBar::Orientation& orientation)
{
  QString ori;
  ts >> ori;
  ori = ori.toLower();
  if(ori == "horizontal")
    orientation = ScaleBar::Horizontal;
  else if(ori == "vertical")
    orientation = ScaleBar::Vertical;
  else {
    Information::err << "Error, invalid orientation: " << ori << endl;
    orientation = ScaleBar::Horizontal;
  }
  return ts;
}

QTextStream& operator<<(QTextStream& ts, ScaleBar::TextPosition text_pos)
{
  switch(text_pos) {
    case ScaleBar::In:
      ts << "In";
      break;
    case ScaleBar::Out:
      ts << "Out";
      break;
  }
  return ts;
}

QTextStream& operator>>(QTextStream& ts, ScaleBar::TextPosition& text_pos)
{
  QString tpos;
  ts >> tpos;
  tpos = tpos.toLower();
  if(tpos == "in")
    text_pos = ScaleBar::In;
  else if(tpos == "out")
    text_pos = ScaleBar::Out;
  else {
    Information::err << "Error, invalid orientation: " << tpos << endl;
    text_pos = ScaleBar::Out;
  }
  return ts;
}

ScaleBar::ScaleBar()
  : orientation(Horizontal)
  , position(BottomRight)
  , textPosition(Out)
  , _wantedSize(1)
  , _scale(1)
  , _unit("m")
  , _displayUnit(true)
  , _thickness(5)
  , _shiftBorder(20, 20)
  , _autoScale(true)
  , _autoUnit(true)
  , _minSize(50)
  , _maxSize(200)
  , _globalScale(1.0)
{
}

void ScaleBar::init(QGLViewer* viewer)
{
  viewer->setAutoFillBackground(false);
  viewer->camera()->setType(qglviewer::Camera::ORTHOGRAPHIC);
}

static const QString unit_prefix[] = {
  "y",                                // yocto - 10^-24
  "z",                                // zepto - 10^-21
  "a",                                // atto  - 10^-18
  "f",                                // femto - 10^-15
  "p",                                // pico  - 10^-12
  "n",                                // nano  - 10^-9
  QString::fromWCharArray(L"\xb5"),   // micro - 10^-6
  "m",                                // milli - 10^-3
  "",                                 // _unit  - 10^0
  "k",                                // kilo  - 10^3
  "M",                                // mega  - 10^6
  "G",                                // giga  - 10^9
  "T",                                // tera  - 10^12
  "P",                                // peta  - 10^15
  "E",                                // exa   - 10^18
  "Z",                                // zetta - 10^21
  "Y",                                // yota  - 10^24
};

static int min_prefix = -24;
static int max_prefix = 24;
static int index_null = 8;

void ScaleBar::findScale(double unit_size)
{
  if(_wantedSize > 0) {
    double s = unit_size * _wantedSize / _scale;
    if(s >= _minSize and s <= _maxSize)
      return;       // Nothing to do .. stay as stable as possible
  }
  double smallest = _scale * _minSize / unit_size;
  double biggest = _scale * _maxSize / unit_size;
  double log10_smallest = floor(log10(smallest));
  double log10_biggest = floor(log10(biggest));
  if(log10_smallest != log10_biggest)
    _wantedSize = pow(10.0, log10_biggest);
  else {
    // Extra first digit of the smallest and the biggest
    double smallest_digit = floor(smallest / pow(10.0, log10_smallest));
    double biggest_digit = floor(biggest / pow(10.0, log10_biggest));
    if(smallest_digit == biggest_digit) {
      throw QString("The _scale range is not big enough to automatically compute the size.");
    }
    if(smallest_digit <= 5 and biggest_digit >= 5)
      _wantedSize = 5 * pow(10.0, log10_biggest);
    else if(smallest_digit <= 2 and biggest_digit >= 2)
      _wantedSize = 2 * pow(10.0, log10_biggest);
    else
      _wantedSize = biggest_digit * pow(10.0, log10_biggest);
  }
}

void ScaleBar::draw(QGLViewer* viewer, QPaintDevice* device)
{
  if(device == 0)
    device = viewer;
  glDisable(GL_CULL_FACE);
  glDisable(GL_DEPTH_TEST);
  glDisable(GL_LIGHTING);
  glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
  // Find _unit size
  Camera* cam = viewer->camera();
  double ratio = (double)cam->pixelGLRatio(Vec(0, 0, 0));
  if(ratio == 0) {
    Information::err << "Error in file " __FILE__ ", line " << __LINE__ << ":\n"
                     << "Error GL pixel ratio is 0! Cannot draw Scale Bar." << endl;
    return;
  }
  double unit_size = 1 / ratio;   // size of a vector of size 1 in pixels
  if(_autoScale)
    findScale(unit_size);
  double needed_size = unit_size * _wantedSize / _scale;
  QSizeF deviceSize = QSizeF(device->width(), device->height()) / _globalScale;
  QSizeF size;
  switch(orientation) {
  case Horizontal:
    size = QSizeF(needed_size, _thickness);
    break;
  case Vertical:
    size = QSizeF(_thickness, needed_size);
  }
  // Convert relative to absolute coordinates
  QPointF abs_pos;
  switch(position) {
  case Top:
    abs_pos = QPointF((deviceSize.width() - size.width()) / 2.0, _shiftBorder.y());
    break;
  case Bottom:
    abs_pos
      = QPointF((deviceSize.width() - size.width()) / 2.0, deviceSize.height() - _shiftBorder.y() - size.height());
    break;
  case Left:
    abs_pos = QPointF(_shiftBorder.x(), (deviceSize.height() - size.height()) / 2.0);
    break;
  case Right:
    abs_pos
      = QPointF(deviceSize.width() - _shiftBorder.x() - size.width(), (deviceSize.height() - size.height()) / 2.0);
    break;
  case TopLeft:
    abs_pos = QPointF(_shiftBorder.x(), _shiftBorder.y());
    break;
  case BottomLeft:
    abs_pos = QPointF(_shiftBorder.x(), deviceSize.height() - size.height() - _shiftBorder.y());
    break;
  case TopRight:
    abs_pos = QPointF(deviceSize.width() - _shiftBorder.x() - size.width(), _shiftBorder.y());
    break;
  case BottomRight:
    abs_pos = QPointF(deviceSize.width() - _shiftBorder.x() - size.width(),
                      deviceSize.height() - _shiftBorder.y() - size.height());
    break;
  case Center:
    abs_pos = QPointF((deviceSize.width() - size.width()) / 2.0, (deviceSize.height() - size.height()) / 2.0);
    break;
  default:
    throw QString("Unknown positionning: %1").arg(position).toStdString();
  }
  QPainter paint(device);
  paint.setRenderHints(QPainter::Antialiasing | QPainter::TextAntialiasing | QPainter::SmoothPixmapTransform
                       | QPainter::HighQualityAntialiasing);
  // paint._scale(_globalScale, _globalScale);
  paint.setWindow(0, 0, deviceSize.width(), deviceSize.height());
  QColor color = Colors::getQColor(Colors::ScaleBarColor);
  QBrush b(color);
  paint.setPen(Qt::NoPen);
  paint.setBrush(b);
  QRectF r(abs_pos, size);
  if(DEBUG)
    Information::out << "Drawing scalebar at position " << abs_pos.x() << "," << abs_pos.y() << " with size "
                     << size.width() << "x" << size.height() << endl;
  paint.drawRect(r);
  if(_displayUnit) {
    QString real_unit = _unit;
    double real_size = _wantedSize;
    if(_autoUnit) {
      double power = floor(log10(_wantedSize));
      if(power < min_prefix)
        power = min_prefix;
      else if(power > max_prefix)
        power = max_prefix;
      power /= 3;
      power = floor(power);
      int pi = int(power) + index_null;
      real_unit = unit_prefix[pi] + _unit;
      double real_power = pow(10.0, 3 * (pi - index_null));
      real_size /= real_power;
    }
    QFont font(_unit_font, device);
    QString text = QString("%1 %2").arg(real_size, 0, 'g', 3).arg(real_unit);
    QFontMetrics metric(font);
    QRect tr = metric.boundingRect(text);
    int th = tr.height();
    QPointF text_pos_hor, text_pos_ver;
    int text_flags_hor = 0, text_flags_ver = 0;
    if(textPosition == In) {
      switch(position) {
      case Top:
        text_pos_hor = QPointF(deviceSize.width() / 2.0, _shiftBorder.y() + size.height() / 2);
        text_pos_ver = QPointF(deviceSize.width() / 2.0, _shiftBorder.y());
        text_flags_hor = Qt::AlignHCenter | Qt::AlignVCenter;
        text_flags_ver = Qt::AlignRight | Qt::AlignVCenter;
        break;
      case Bottom:
        text_pos_hor
          = QPointF(deviceSize.width() / 2.0, deviceSize.height() - _shiftBorder.y() - size.height() / 2);
        text_pos_ver = QPointF(deviceSize.width() / 2.0, deviceSize.height() - _shiftBorder.y());
        text_flags_hor = Qt::AlignHCenter | Qt::AlignVCenter;
        text_flags_ver = Qt::AlignLeft | Qt::AlignVCenter;
        break;
      case Left:
        text_pos_hor = QPointF(_shiftBorder.x(), deviceSize.height() / 2.0);
        text_pos_ver = QPointF(_shiftBorder.x() + size.width() / 2.0, deviceSize.height() / 2.0);
        text_flags_hor = Qt::AlignLeft | Qt::AlignVCenter;
        text_flags_ver = Qt::AlignHCenter | Qt::AlignVCenter;
        break;
      case Right:
        text_pos_hor = QPointF(deviceSize.width() - _shiftBorder.x(), deviceSize.height() / 2.0);
        text_pos_ver
          = QPointF(deviceSize.width() - _shiftBorder.x() - size.width() / 2.0, deviceSize.height() / 2.0);
        text_flags_hor = Qt::AlignRight | Qt::AlignVCenter;
        text_flags_ver = Qt::AlignHCenter | Qt::AlignVCenter;
        break;
      case TopLeft:
        text_pos_hor = QPointF(_shiftBorder.x(), _shiftBorder.y() + size.height() / 2.0);
        text_pos_ver = QPointF(_shiftBorder.x() + size.width() / 2.0, _shiftBorder.y());
        text_flags_hor = Qt::AlignLeft | Qt::AlignVCenter;
        text_flags_ver = Qt::AlignRight | Qt::AlignVCenter;
        break;
      case BottomLeft:
        text_pos_hor = QPointF(_shiftBorder.x(), deviceSize.height() - _shiftBorder.y() - size.height() / 2.0);
        text_pos_ver = QPointF(_shiftBorder.x() + size.width() / 2.0, deviceSize.height() - _shiftBorder.y());
        text_flags_hor = Qt::AlignLeft | Qt::AlignVCenter;
        text_flags_ver = Qt::AlignLeft | Qt::AlignVCenter;
        break;
      case TopRight:
        text_pos_hor = QPointF(deviceSize.width() - _shiftBorder.x(), _shiftBorder.y() + size.height() / 2.0);
        text_pos_ver = QPointF(deviceSize.width() - _shiftBorder.x() - size.width() / 2.0, _shiftBorder.y());
        text_flags_hor = Qt::AlignRight | Qt::AlignVCenter;
        text_flags_ver = Qt::AlignRight | Qt::AlignVCenter;
        break;
      case BottomRight:
        text_pos_hor = QPointF(deviceSize.width() - _shiftBorder.x(),
                               deviceSize.height() - _shiftBorder.y() - size.height() / 2.0);
        text_pos_ver = QPointF(deviceSize.width() - _shiftBorder.x() - size.width() / 2.0,
                               deviceSize.height() - _shiftBorder.y());
        text_flags_hor = Qt::AlignRight | Qt::AlignVCenter;
        text_flags_ver = Qt::AlignLeft | Qt::AlignVCenter;
        break;
      case Center:
        text_pos_ver = text_pos_hor = QPointF(deviceSize.width() / 2.0, deviceSize.height() / 2.0);
        text_flags_hor = text_flags_ver = Qt::AlignHCenter | Qt::AlignVCenter;
        break;
      default:
        throw QString("Unknown positionning: %1").arg(position).toStdString();
      }
    } else {
      switch(position) {
      case Top:
        text_pos_hor = QPointF(deviceSize.width() / 2.0, _shiftBorder.y() + size.height() + th / 4.0);
        text_pos_ver
          = QPointF(deviceSize.width() / 2.0 + size.width() + th / 8.0, _shiftBorder.y() + size.height() / 2);
        text_flags_hor = Qt::AlignHCenter | Qt::AlignTop;
        text_flags_ver = Qt::AlignHCenter | Qt::AlignTop;
        break;
      case Bottom:
        text_pos_hor = QPointF(deviceSize.width() / 2.0,
                               deviceSize.height() - _shiftBorder.y() - size.height() - th / 8.0);
        text_pos_ver = QPointF(deviceSize.width() / 2.0 + size.width() + th / 8.0,
                               deviceSize.height() - _shiftBorder.y());
        text_flags_hor = Qt::AlignHCenter | Qt::AlignBottom;
        text_flags_ver = Qt::AlignLeft | Qt::AlignTop;
        break;
      case Left:
        text_pos_hor = QPointF(_shiftBorder.x(), (deviceSize.height() - size.height()) / 2.0 - th / 8.0);
        text_pos_ver = QPointF(_shiftBorder.x() + size.width() + th / 8.0, deviceSize.height() / 2.0);
        text_flags_hor = Qt::AlignLeft | Qt::AlignBottom;
        text_flags_ver = Qt::AlignHCenter | Qt::AlignTop;
        break;
      case Right:
        text_pos_hor = QPointF(deviceSize.width() - _shiftBorder.x(),
                               (deviceSize.height() - size.height()) / 2.0 - th / 8.0);
        text_pos_ver = QPointF(deviceSize.width() - _shiftBorder.x() - size.width() - th / 8.0,
                               deviceSize.height() / 2.0);
        text_flags_hor = Qt::AlignRight | Qt::AlignBottom;
        text_flags_ver = Qt::AlignHCenter | Qt::AlignBottom;
        break;
      case TopLeft:
        text_pos_hor = QPointF(_shiftBorder.x(), _shiftBorder.y() + size.height() + th / 8.0);
        text_pos_ver = QPointF(_shiftBorder.x() + size.width() + th / 8.0, _shiftBorder.y());
        text_flags_hor = Qt::AlignLeft | Qt::AlignTop;
        text_flags_ver = Qt::AlignRight | Qt::AlignTop;
        break;
      case BottomLeft:
        text_pos_hor
          = QPointF(_shiftBorder.x(), deviceSize.height() - _shiftBorder.y() - size.height() - th / 8.0);
        text_pos_ver
          = QPointF(_shiftBorder.x() + size.width() + th / 8.0, deviceSize.height() - _shiftBorder.y());
        text_flags_hor = Qt::AlignLeft | Qt::AlignBottom;
        text_flags_ver = Qt::AlignLeft | Qt::AlignTop;
        break;
      case TopRight:
        text_pos_hor
          = QPointF(deviceSize.width() - _shiftBorder.x(), _shiftBorder.y() + size.height() + th / 8.0);
        text_pos_ver = QPointF(deviceSize.width() - _shiftBorder.x() - size.width() - th / 8.0, _shiftBorder.y());
        text_flags_hor = Qt::AlignRight | Qt::AlignTop;
        text_flags_ver = Qt::AlignRight | Qt::AlignBottom;
        break;
      case BottomRight:
        text_pos_hor = QPointF(deviceSize.width() - _shiftBorder.x(),
                               deviceSize.height() - _shiftBorder.y() - size.height() - th / 8.0);
        text_pos_ver = QPointF(deviceSize.width() - _shiftBorder.x() - size.width() - th / 8.0,
                               deviceSize.height() - _shiftBorder.y());
        text_flags_hor = Qt::AlignRight | Qt::AlignBottom;
        text_flags_ver = Qt::AlignLeft | Qt::AlignBottom;
        break;
      case Center:
        text_pos_hor
          = QPointF(deviceSize.width() / 2.0, (deviceSize.height() - size.height()) / 2.0 - th / 8.0);
        text_pos_ver = QPointF((deviceSize.width() - size.width()) / 2.0 - th / 8.0, deviceSize.height() / 2.0);
        text_flags_hor = Qt::AlignHCenter | Qt::AlignBottom;
        text_flags_ver = Qt::AlignHCenter | Qt::AlignBottom;
        break;
      default:
        throw QString("Unknown positionning: %1").arg(position).toStdString();
      }
    }
    paint.setFont(font);
    QColor textColor = Colors::getQColor((textPosition == Out) ? Colors::LegendColor : Colors::BackgroundColor);
    paint.setPen(textColor);
    QTransform trans;
    switch(orientation) {
    case Horizontal:
      trans.translate(text_pos_hor.x(), text_pos_hor.y());
      break;
    case Vertical:
      trans.translate(text_pos_ver.x(), text_pos_ver.y());
    }
    if(orientation == Vertical) {
      trans.rotate(-90);
    }
    paint.setWorldTransform(trans, true);
    int text_flags = (orientation == Horizontal) ? text_flags_hor : text_flags_ver;
    paint.drawText(QRectF(QPointF(0, 0), QSizeF()), text_flags | Qt::TextDontClip, text);
    /*
     * if(textPosition == In)
     *{
     *  paint.setWorldTransform(QTransform());
     *  paint.setClipRegion(QRegion(r.toRect()));
     *  //paint.setWorldTransform(trans);
     *  QColor textColor = Colors::getQColor(Colors::BackgroundColor);
     *  paint.setPen(textColor);
     *  paint.drawText(QRectF(QPointF(0,0), QSizeF()), text_flags | Qt::TextDontClip, text);
     *}
     */
  }
  paint.end();
}

void ScaleBar::readParms(util::Parms& parms, QString section)
{
  loadDefaults();

  parms.verboseLevel(1);

  // parms(section, "Scale", _scale);
  parms(section, "AutoScale", _autoScale, true);
  QString u;
  parms(section, "Unit", u, QString("m"));
  setUnit(u);
  if(_autoScale) {
    parms(section, "MinSize", _minSize, _minSize);
    parms(section, "MaxSize", _maxSize, _maxSize);
    _wantedSize = -1;
  } else {
    parms(section, "WantedSize", _wantedSize, 1.0);
  }
  parms(section, "AutoUnit", _autoUnit, true);
  parms.optional(section, "Position", position);
  parms.optional(section, "Orientation", orientation);
  parms.optional(section, "Thickness", _thickness);
  parms.optional(section, "ShiftBorder", _shiftBorder);
  parms.optional(section, "TextPosition", textPosition);
  QString font;
  parms(section, "Font", font, _unit_font.toString());
  _unit_font.fromString(font);
}

void ScaleBar::writeParms(QTextStream& pout, QString section)
{
  pout << endl;
  pout << "[" << section << "]" << endl;
  // pout << "Scale: " << _scale << endl;
  pout << "AutoScale: " << (_autoScale ? "true" : "false") << endl;
  pout << "Unit:" << _unit << endl;
  if(_autoScale) {
    pout << "MinSize: " << _minSize << endl;
    pout << "MaxSize: " << _maxSize << endl;
  } else
    pout << "WantedSize: " << _wantedSize << endl;
  pout << "AutoUnit: " << (_autoUnit ? "true" : "false") << endl;
  switch(position) {
  case Top:
    pout << "Position: Top" << endl;
    break;
  case Bottom:
    pout << "Position: Bottom" << endl;
    break;
  case Left:
    pout << "Position: Left" << endl;
    break;
  case Right:
    pout << "Position: Right" << endl;
    break;
  case TopLeft:
    pout << "Position: TopLeft" << endl;
    break;
  case TopRight:
    pout << "Position: TopRight" << endl;
    break;
  case BottomLeft:
    pout << "Position: BottomLeft" << endl;
    break;
  case BottomRight:
    pout << "Position: BottomRight" << endl;
    break;
  case Center:
    pout << "Position: Center" << endl;
    break;
  }
  switch(orientation) {
  case Horizontal:
    pout << "Orientation: Horizontal" << endl;
    break;
  case Vertical:
    pout << "Orientation: Vertical" << endl;
    break;
  }
  switch(textPosition) {
  case In:
    pout << "TextPosition: In" << endl;
    break;
  case Out:
    pout << "TextPosition: Out" << endl;
  }
  pout << "Thickness: " << _thickness << endl;
  pout << "ShiftBorder: " << _shiftBorder << endl;
  pout << "Font: " << _unit_font.toString() << endl;
}

void ScaleBar::scaleDrawing(double s)
{
  _globalScale = s;
  /*
   * savedWantedSize = _wantedSize;
   * savedThickness = _thickness;
   * savedShiftBorder = _shiftBorder;
   * savedMinSize = _minSize;
   * savedMaxSize = _maxSize;
   * _wantedSize *= s;
   * _thickness *= s;
   * _shiftBorder *= s;
   * _minSize *= s;
   * _maxSize *= s;
   */
}

void ScaleBar::restoreScale()
{
  _globalScale = 1.0;
  /*
   * _wantedSize = savedWantedSize;
   * _thickness = savedThickness;
   * _shiftBorder = savedShiftBorder;
   * _minSize = savedMinSize;
   * _maxSize = savedMaxSize;
   */
}

void ScaleBar::setSizeRange(double low, double high)
{
  if(low > 0 and 2.5*low <= high)
  {
    _minSize = low;
    _maxSize = high;
  }
}

void ScaleBar::loadDefaults()
{
  QSettings settings;
  settings.beginGroup("ScaleBar");

  QFont default_font;
  default_font.setStyleStrategy(QFont::OpenGLCompatible);
  default_font.setStyleStrategy(QFont::PreferAntialias);
  default_font.setPointSize(10);

  bool ok;
  auto pos = settings.value("Position", lgx::ScaleBar::BottomRight).toInt(&ok);
  if(ok and pos >= 0 and pos < 9)
    position = (lgx::ScaleBar::Position)pos;

  auto int_orient = settings.value("Orientation", lgx::ScaleBar::Horizontal).toInt(&ok);
  if(ok and int_orient >= 0 and int_orient < 2)
    orientation = (lgx::ScaleBar::Orientation)int_orient;

  auto textPos = settings.value("TextPosition", lgx::ScaleBar::Out).toInt(&ok);
  if(ok and textPos >= 0 and textPos < 2)
    textPosition = (lgx::ScaleBar::TextPosition)textPos;

  auto textFont = settings.value("Font", default_font);
  if(textFont.canConvert<QFont>()) {
    auto fnt = textFont.value<QFont>();
    setFont(fnt);
  }

  auto minSize = settings.value("MinimumSize", 50).toDouble(&ok);
  if(ok) {
    auto maxSize = settings.value("MaximumSize", 200).toDouble(&ok);
    if(ok) {
      setSizeRange(minSize, maxSize);
    }
  }

  auto thickness = settings.value("Thickness", 5).toUInt(&ok);
  if(ok) setThickness(thickness);

  settings.endGroup();
}

} // namespace lgx
