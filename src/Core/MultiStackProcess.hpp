/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef CORE_MULTISTACKPROCESS_HPP
#define CORE_MULTISTACKPROCESS_HPP

#include <Process.hpp>

#include <Misc.hpp>
#include <Stack.hpp>
#include <Store.hpp>

namespace lgx {
namespace process {

///\addtogroup StackProcess
///@{

/**
 * \class AverageStores StackProcess.hpp <StackProcess.hpp>
 *
 * Average the values of both stores.
 */
class LGXCORE_EXPORT AverageStores : public StackProcess {
public:
  AverageStores(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const ParmList&) override
  {
    if(!checkState().store(STORE_MAIN | STORE_NON_EMPTY | STORE_NON_LABEL).store(STORE_WORK | STORE_NON_EMPTY
                                                                                 | STORE_NON_LABEL))
      return false;
    Stack* stack = currentStack();
    bool res = (*this)(stack);
    if(res) {
      stack->main()->hide();
      stack->work()->show();
    }
    return res;
  }
  /**
   * Average the stores
   * \param stack Stack containing the stores to average. The result will be placed in the work store.
   */
  bool operator()(Stack* stack);

  QString folder() const override {
    return "Multi-stack";
  }
  QString name() const override {
    return "Average Main and Work Stacks";
  }
  QString description() const override {
    return "Compute average of main and work stacks";
  }
  QStringList parmNames() const override {
    return QStringList();
  }
  QStringList parmDescs() const override {
    return QStringList();
  }
  QIcon icon() const override {
    return QIcon(":/images/AverageStacks.png");
  }
};

/**
 * \class CopyMainToWork StackProcess.hpp <StackProcess.hpp>
 *
 * Copy the content of the main stack into the work stack
 */
class LGXCORE_EXPORT CopyMainToWork : public StackProcess {
public:
  CopyMainToWork(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const ParmList&) override
  {
    if(!checkState().store(STORE_MAIN | STORE_NON_EMPTY))
      return false;
    Stack* stack = currentStack();
    bool res = (*this)(stack);
    if(res) {
      stack->main()->hide();
      stack->work()->show();
    }
    return res;
  }

  bool operator()(Stack* stack);

  QString folder() const override {
    return "Multi-stack";
  }
  QString name() const override {
    return "Copy Main to Work Stack";
  }
  QString description() const override {
    return "Copy Main to Work Stack";
  }
  QStringList parmNames() const override {
    return QStringList();
  }
  QStringList parmDescs() const override {
    return QStringList();
  }
  QIcon icon() const override {
    return QIcon(":/images/CopyMainToWork.png");
  }
};

/**
 * \class CopyWorkToMain StackProcess.hpp <StackProcess.hpp>
 *
 * Copy the content of the work stack into the main stack
 */
class LGXCORE_EXPORT CopyWorkToMain : public StackProcess {
public:
  CopyWorkToMain(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const ParmList&) override
  {
    if(!checkState().store(STORE_WORK | STORE_NON_EMPTY))
      return false;
    Stack* stack = currentStack();
    bool res = (*this)(stack);
    if(res) {
      stack->work()->hide();
      stack->main()->show();
    }
    return res;
  }

  bool operator()(Stack* stack);

  QString folder() const override {
    return "Multi-stack";
  }
  QString name() const override {
    return "Copy Work to Main Stack";
  }
  QString description() const override {
    return "Copy Work to Main Stack";
  }
  QStringList parmNames() const override {
    return QStringList();
  }
  QStringList parmDescs() const override {
    return QStringList();
  }
  QIcon icon() const override {
    return QIcon(":/images/CopyWorkToMain.png");
  }
};

/**
 * \class CopySwapStacks <StackProcess.hpp>
 *
 * Copy or swap stacks between stack 1 and 2.
 */
class LGXCORE_EXPORT CopySwapStacks : public StackProcess {
public:
  CopySwapStacks(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const ParmList& parms) override
  {
    // Stack *stack = currentStack();
    bool res = (*this)(parms[0].toString(), parms[1].toString());
    // if(res)
    //{
    //  stack->main()->hide();
    //  stack->work()->show();
    //}
    return res;
  }

  bool operator()(const QString& storeStr, const QString& actionStr);

  QString folder() const override {
    return "Multi-stack";
  }
  QString name() const override {
    return "Swap or Copy Stack 1 and 2";
  }
  QString description() const override {
    return "Copy or Swap Stack 1 and 2";
  }
  QStringList parmNames() const override
  {
    return QStringList() << "Store"
                         << "Action";
  }
  QStringList parmDescs() const override
  {
    return QStringList() << "Store"
                         << "Action";
  }
  ParmList parmDefaults() const override
  {
    return ParmList() << "Main"
                      << "1 -> 2";
  }
  ParmChoiceMap parmChoice() const override
  {
    ParmChoiceMap map;
    map[0] = storeChoice();
    map[1] = QStringList() << "1 -> 2"
                           << "1 <- 2"
                           << "1 <-> 2";
    return map;
  }

  QIcon icon() const override {
    return QIcon(":/images/CopySwapStacks.png");
  }
};

/**
 * \class SwapStacks StackProcess.hpp <StackProcess.hpp>
 *
 * Swap the main and work stores of a stack
 */
class LGXCORE_EXPORT SwapStacks : public StackProcess {
public:
  SwapStacks(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const ParmList&) override
  {
    if(!checkState().stack(STACK_NON_EMPTY))
      return false;
    Stack* s = currentStack();
    if(!s) {
      setErrorMessage("You need to select a stack to launch this process.");
      return false;
    }
    return (*this)(s);
  }

  bool operator()(Stack* stack);

  QString folder() const override {
    return "Multi-stack";
  }
  QString name() const override {
    return "Swap Main and Work Stacks";
  }
  QString description() const override {
    return "Swap the main and work data of the current stack.";
  }
  QStringList parmNames() const override {
    return QStringList();
  }
  QStringList parmDescs() const override {
    return QStringList();
  }
  QIcon icon() const override {
    return QIcon(":/images/SwapStacks.png");
  }
};


///@}
} // namespace process
} // namespace lgx

#endif // CORE_MULTISTACKPROCESS_HPP
