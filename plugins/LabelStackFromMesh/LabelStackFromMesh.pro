MGX_LIBS=$$system(MorphoGraphX --libs)
MGX_INCLUDE=$$system(MorphoGraphX --include)
MGX_PROCESSES=$$system(MorphoGraphX --process)
CUDA_INCLUDE=/usr/local/cuda/include
CIMG_INCLUDE=

TEMPLATE = lib

TARGET=LabelStackFromMesh

DEFINES += cimg_display=0
SOURCES += LabelStackFromMesh.cpp
HEADERS += LabelStackFromMesh.hpp
RESOURCES = LabelStackFromMesh.qrc

CONFIG += qt release plugin
QT += xml opengl

INCLUDEPATH += $$MGX_INCLUDE $$MGX_PROCESSES/include $$CUDA_INCLUDE $$CIMG_INCLUDE
macx: {
LIBS += -F$$MGX_LIBS -framework mgx
} else {
LIBS += -L$$MGX_LIBS -lmgx
}

target.path = $$MGX_PROCESSES
INSTALLS = target


