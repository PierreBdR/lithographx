/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef LGX_FEATURES_H
#define LGX_FEATURES_H

#include <LGXConfig.hpp>

#ifdef __GNUC__
#  ifdef WIN32
#    include <_mingw.h>
#    define __GNUC_PREREQ(x, y) __MINGW_GNUC_PREREQ(x, y)
#  else
#    include <features.h>
#  endif
#endif

#endif // LGX_FEATURES_H
