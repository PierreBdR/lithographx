#ifndef BezierAxis_HPP
#define BezierAxis_HPP

#include <Process.hpp>

#include <CuttingSurface.hpp>

#include <QFileDialog>

namespace lgx {
namespace process {
class BezierReferenceSystemProcess : public MeshProcess {
public:
    enum DisplayType { DT_NONE = 0, DT_LENGTH, DT_NORMAL, DT_BINORM, DT_RADIUS, DT_ANGLE };

    BezierReferenceSystemProcess(const MeshProcess& process)
        : Process(process)
        , MeshProcess(process)
    {
    }

    bool initialize(ParmList& parms, QWidget* parent) override
    {
        QString filename = QFileDialog::getSaveFileName(
            parent, "Select a CSV file to save the result into", parms[0].toString(), "CSV Files (*.csv);;All files (*.*)");
        if(filename.isEmpty())
            return false;

        if(not filename.endsWith(".csv"))
            filename += ".csv";

        parms[0] = filename;
        return true;
    }

    bool operator()(const ParmList& parms) override
    {
        if(not checkState().mesh(MESH_NON_EMPTY))
            return false;
        CuttingSurface* cut = cuttingSurface();
        if(not cut->bezier())
            return setErrorMessage("Error, the cutting surface must be a Bezier line!");
        return operator()(currentMesh(), cut, parms[0].toString(), parseDisplay(parms[1].toString()));
    }

    bool operator()(Mesh* mesh, CuttingSurface* cut, const QString& output, DisplayType display);

    QString folder() const override { return "Bezier Curve"; }
    QString name() const override { return "Bezier Reference Frame"; }
    QString description() const override
    {
        return "Compute the 3D position of each cell in the curvilinear reference frame defined by the Bezier curve.";
    }
    QStringList parmNames() const override
    {
        return QStringList() << "Output"
                             << "Heatmap";
    }
    QStringList parmDescs() const override
    {
        return QStringList() << "CSV output file "
                             << "What to display as a heat map.";
    }
    ParmList parmDefaults() const override
    {
        return ParmList() << "result.csv"
                          << "Length";
    }
    ParmChoiceMap parmChoice() const override
    {
        ParmChoiceMap map;
        map[1] = QStringList() << "none"
                               << "Length"
                               << "Normal"
                               << "Binormal"
                               << "Radius"
                               << "Angle";
        return map;
    }
    QIcon icon() const override { return QIcon(":/images/BezierReferenceSystemProcess.png"); }

protected:
    // Extra methods
    DisplayType parseDisplay(const QString& name);

    QString displayName(DisplayType display);
};
} // namespace process
} // namespace lgx

#endif // BezierAxis_HPP
