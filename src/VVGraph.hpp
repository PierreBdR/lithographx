/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef VV_GRAPH_HPP
#define VV_GRAPH_HPP

/**
 * \file VVGraph.hpp
 *
 * Contain the definition of the VVGraph template class.
 */

#include <LGXConfig.hpp>

#include <CircIterator.hpp>
#include <Edge.hpp>
#include <Forall.hpp>
#include <MemberIterator.hpp>
#include <Tie.hpp>
#include <Vertex.hpp>

#include <algorithm>
#include <exception>
#include <iostream>
#include <iterator>
#include <list>
#include <memory>
#include <unordered_map>
#include <utility>
#include <vector>

#ifdef HASH_NEED_TR1
namespace std {
using namespace tr1;
}
#endif

#define GRAPH_TEMPLATE typename VertexContent, typename EdgeContent, typename Alloc
#define GRAPH_ARGS VertexContent, EdgeContent, Alloc

namespace lgx {
namespace graph {

struct UnknownNeighbor : public std::exception
{
  UnknownNeighbor(vertex_identity_t src, vertex_identity_t tgt) throw()
    : source(src)
    , target(tgt)
  { }

  UnknownNeighbor(const UnknownNeighbor& other) throw()
    : source(other.source)
    , target(other.target)
  { }

  UnknownNeighbor& operator=(const UnknownNeighbor& other) throw()
  {
    source = other.source;
    target = other.target;
    return *this;
  }

  virtual ~UnknownNeighbor() throw() { }

  const char* what() const throw()
  {
    return "Error, vertex has a neighbor that doesn't exist in the graph.";
  }

  vertex_identity_t source, target;
};


/**
 * \class SelfConnectionError "VVGraph.hpp" <VVGraph.hpp>
 */
struct SelfConnectionError : public std::exception
{
  SelfConnectionError(vertex_identity_t i) throw()
    : id(i)
  { }

  SelfConnectionError(const SelfConnectionError& other) throw()
    : id(other.id)
  { }

  SelfConnectionError& operator=(const SelfConnectionError& other) throw()
  {
    id = other.id;
    return *this;
  }

  virtual ~SelfConnectionError() throw() { }

  const char* what() const throw()
  {
    return "Error, vertex connected to itself!";
  }

  vertex_identity_t id;
};

/**
 * Contains all the classes related to the graphs.
 *
 * For now, the only graph available is the VVGraph.
 */

/**
 * \class _EmptyEdgeContent VVGraph.hpp <VVGraph.hpp>
 *
 * Empty class used as default for edge content
 */
struct _EmptyEdgeContent {
  _EmptyEdgeContent& operator=(const _EmptyEdgeContent& /*other*/) {
    return *this;
  }
};

/**
 * \class VVGraph VVGraph.hpp <VVGraph.hpp>
 * Class representing a VV graph.
 *
 * A VV graph is an oriented rotational graph. That is, the edges of a given
 * vertex have a circular order. That is, if an edge has three edges e1, e2,
 * e3, there is no first edge, but we can say e1 < e2 < e3 < e1. In that
 * case, '<' denote the succession.
 *
 * \c VertexContent is the type used to hold the vertex data.
 *
 * \c EdgeContent is the type used to hold the edge data. If no type is
 * specified for \c EdgeContent, then an empty structure is used.
 *
 * \section Perfs Performance notes
 * The graph includes two caching mecanisms to improve performances when
 * iterating over the graph and retrieve edge information:
 *   -# while iterating over the graph, the vertex cache the current iterator.
 *   So that accessing the neighborhood of the vertex is constant time (i.e.
 *   it's dereferencing the cached iterator).
 *   -# while iterating over the neighborhood of a vertex, the target vertex
 *   (i.e. the one returned by the iteration) cache the iterator on the
 *   neighborhood. So that any operation using that specific edge (i.e.
 *   getting edge data, splicing, replacing or erasing) is alos constant
 *   time.
 *
 * To make this caching mecanism correct, the cached data are never copied.
 * This means, to make use of the caching mecanism, you have to iterate using
 * constant references (references will end up with compilation errors if
 * used on the graph):
 * \code
 * forall(const vertex &v, S)
 * {
 *   // Here the vertex v has the vertex cache
 *   forall(const vertex &n, S.neighbors(S)) // Makes use of the cache
 *   {
 *     S.edge(v,n)->pos = Point3d(1,2,3); // Uses the edge cache
 *     S.edge(n,v)->pos = Point3d(3,2,1); // Do not use any cache
 *   }
 *   v->pos = Point3d(0,0,0); // Valid
 * }
 * \endcode
 *
 * \warning Do not iterate using constant references if you ever remove the
 * vertex you iterate on ! (i.e. remove from the graph if you iterate on the
 * graph or remove from the neighborhood if you iterate on the neighborhood)
 *
 * Even though this code manipulate constant references, modifying the data
 * pointed by the vertex is allowed. The vertexes are coded to allow that.
 * That means if you are using vertexes in a dictionary or a set, using their
 * content to compare them, you must not modify them ! The compiler won't
 * guard you against it.
 */
template <typename VertexContent, typename EdgeContent = _EmptyEdgeContent,
          typename Alloc = std::allocator<VertexContent> >
class VVGraph {
public:
  typedef typename Alloc::template rebind<EdgeContent>::other EdgeAlloc;
  static EdgeAlloc edge_alloc;

  ///\name Smart pointer types
  //@{
  /**
   * Smart pointer on a vertex
   */
  typedef typename Alloc::template rebind<VertexContent>::other VertexAlloc;
  typedef Vertex<VertexContent, VertexAlloc> vertex_t;
  /**
   * Weak pointer on an edge
   */
  typedef Edge<EdgeContent> edge_t;
  /**
   * Weak pointer on a constant edge
   */
  typedef Edge<const EdgeContent> const_edge_t;
  /**
   * Weak pointer on an arc.
   *
   * When an object of this type is destroyed, the content of the primary
   * edge (i.e. the edge source -> target) is copied into the other.
   */
  typedef Arc<EdgeContent> arc_t;
  //@}

  /**
   * Type of the value of the graph as standard container.
   */
  typedef vertex_t value_type;

  /**
   * Default constructor
   *
   * Creates an empty VV graph
   */
  VVGraph()
    : modified_vertices(false)
  {
  }

  /**
   * Copy constructor.
   *
   * Maintains the caches in a valid state
   */
  VVGraph(const VVGraph& copy);

  /**
   * Construct from an existing list of vertices
   *
   * \param check_unique If false, there must be not be duplicated vertex in the sequence
   */
  template <typename Iterator> VVGraph(Iterator begin, Iterator end, bool check_unique = true)
  {
    insert(begin, end, check_unique);
  }

  /**
   * Construct from an existing list of vertices
   *
   * \param check_unique If false, there must be not be duplicated vertex in the sequence
   */
  template <typename Container> VVGraph(const Container& c, bool check_unique = true)
  {
    insert(util::ForAll::make_range(c), check_unique);
  }

protected:
  struct single_neighborhood_t;

  /**
   * Structure maintaining the data for a single neighbor
   */
  struct neighbor_t : public EdgeContent {
    /**
     * Type of the list of outgoing neighbors
     */
    typedef std::list<neighbor_t> edge_list_t;

    /**
     * Constructor
     */
    neighbor_t(const vertex_t& tgt, const EdgeContent& e)
      : EdgeContent(e)
      , target(tgt)
    {
    }

    neighbor_t(const neighbor_t& copy)
      : EdgeContent(copy)
      , target(copy.target)
    {
    }

    void clear_edge() {
      static_cast<EdgeContent&>(*this) = EdgeContent();
    }

    ~neighbor_t() {
    }

    neighbor_t& operator=(const neighbor_t& copy)
    {
      target = copy.target;
      static_cast<EdgeContent&>(*this) = static_cast<const EdgeContent&>(copy);
      return *this;
    }

    /**
     * Target of the edge
     *
     * i.e. neighbor itself
     */
    vertex_t target;

    /**
     * Equality of two neighbors/edge
     *
     * A single neighbor is equal to another if the vertex and the content are
     * the same
     */
    bool operator==(const neighbor_t& other) const
    {
      return (target == other.target)
             && (static_cast<const EdgeContent&>(*this) == static_cast<const EdgeContent&>(other));
    }
  };

  /**
   * Type of the list of outgoing neighbors
   */
  typedef typename neighbor_t::edge_list_t edge_list_t;

  /**
   * Type of the list of sources of the in-edges
   */
  typedef std::vector<vertex_t> in_edges_t;

  /**
   * Type of the neighborhood of a vertex
   */
  struct single_neighborhood_t {

    single_neighborhood_t()
      : flagged(0)
    {
    }

    single_neighborhood_t(const single_neighborhood_t& copy)
      : edges(copy.edges)
      , in_edges(copy.in_edges)
      , flagged(copy.flagged)
    {
      if(flagged) {
        for(typename edge_list_t::iterator it = edges.begin(); it != edges.end(); ++it) {
          if(it->target == *flagged) {
            flagged = &it->target;
            break;
          }
        }
      }
    }

    /**
     * List of the outgoing edges
     */
    edge_list_t edges;

    /**
     * Set of the sources of the incoming edges
     */
    in_edges_t in_edges;

    /**
     * Iterator toward the flagged neighbor
     */
    const vertex_t* flagged;

    single_neighborhood_t& operator=(const single_neighborhood_t& other)
    {
      edges = other.edges;
      in_edges = other.in_edges;
      if(other.flagged) {
        for(typename edge_list_t::iterator it = edges.begin(); it != edges.end(); ++it) {
          if(it->target == *flagged) {
            flagged = &it->target;
            break;
          }
        }
      } else
        flagged = 0;
      return *this;
    }

    /**
     * Equality of two neighborhood
     *
     * Only the outgoing edges are compared!
     */
    bool operator==(const single_neighborhood_t& other) const {
      return edges == other.edges;
    }
  };

public:
  struct NeighborhoodPair {
protected:
    NeighborhoodPair(const vertex_t& v)
      : vertex(v)
    {
    }

    NeighborhoodPair(const NeighborhoodPair& copy)
      : vertex(copy.vertex)
      , single_neighborhood(copy.single_neighborhood)
    {
    }

public:
    NeighborhoodPair* copy() const
    {
      NeighborhoodPair* np = np_alloc.allocate(1);
      ::new (np) NeighborhoodPair(*this);
      return np;
    }

    static NeighborhoodPair* New(const vertex_t& v)
    {
      NeighborhoodPair* np = np_alloc.allocate(1);
      ::new (np) NeighborhoodPair(v);
      return np;
    }

    static void Delete(NeighborhoodPair* np)
    {
      if(np) {
        np->~NeighborhoodPair();
        np_alloc.deallocate(np, 1);
      }
    }

    vertex_t vertex;
    single_neighborhood_t single_neighborhood;
  };

  typedef typename Alloc::template rebind<NeighborhoodPair>::other NPAlloc;
  static NPAlloc np_alloc;

  // typedef std::map<vertex_t, single_neighborhood_t> neighborhood_t;
  /**
   * Type of the list of vertexes, together with the neighborhood
   */
  typedef std::vector<NeighborhoodPair*> neighborhood_t;

  /**
   * Type of the fast-search map.
   *
   * The second element is the index in the neighborhood_t vector
   */
  typedef std::unordered_map<vertex_t, size_t> lookup_t;

  /**
   * Shortcut for the value_type of the neighborhood
   */
  typedef typename neighborhood_t::value_type neighborhood_value_type;

  /**
   * Type of a size
   */
  typedef typename neighborhood_t::size_type size_type;

///\name Iterators
//@{
/**
 * Iterator on the vertexes
 */
  typedef util::SelectMemberPointerIterator<typename neighborhood_t::iterator, const vertex_t, NeighborhoodPair,
                                            &NeighborhoodPair::vertex> iterator;
  /**
   * Constant iterator on the vertexes
   */
  typedef util::SelectMemberPointerIterator<typename neighborhood_t::const_iterator, const vertex_t, NeighborhoodPair,
                                            &NeighborhoodPair::vertex> const_iterator;

  /**
   * Iterator on the neighbors of a vertex
   */
  typedef util::SelectMemberIterator<typename edge_list_t::iterator, vertex_t, &neighbor_t::target> neighbor_iterator;
  /**
   * Range of the neighbors of a vertex. The \c first element of the pair is an
   * iterator on the beginning of the range, the \c second element is the
   * past-the-end iterator.
   */
  typedef std::pair<neighbor_iterator, neighbor_iterator> neighbor_iterator_pair;

  /**
   * Iterator used to iterate over the neighbors, but specifying the
   * starting point
   */
  typedef util::CircIterator<neighbor_iterator> circ_neighbor_iterator;

  /**
   * Range of circular iterators
   */
  typedef std::pair<circ_neighbor_iterator, circ_neighbor_iterator> circ_neighbor_iterator_pair;

  /**
   * Iterator on the incoming neighbors of a vertex.
   *
   * An incoming neighbor is a source vertex of an edge whose target is the
   * current vertex.
   */
  typedef typename in_edges_t::iterator ineighbor_iterator;
  typedef typename in_edges_t::const_iterator const_ineighbor_iterator;

  /**
   * Range of the incoming neighbors of a vertex. The \c first element of the
   * pair is an iterator on the beginning of the range, the \c second element
   * is the past-the-end iterator.
   *
   * \see ineighbor_iterator
   */
  typedef std::pair<ineighbor_iterator, ineighbor_iterator> ineighbor_iterator_pair;
  typedef std::pair<const_ineighbor_iterator, const_ineighbor_iterator> const_ineighbor_iterator_pair;

  /**
   * Constant iterator on the neighbors of a vertex
   */
  typedef util::SelectMemberIterator<typename edge_list_t::const_iterator, const vertex_t, &neighbor_t::target>
    const_neighbor_iterator;
  /**
   * Constant range of the neighbors of a vertex. The \c first element of the
   * pair is a constant iterator on the beginning of the range, the \c second
   * element is the past-the-end constant iterator.
   */
  typedef std::pair<const_neighbor_iterator, const_neighbor_iterator> const_neighbor_iterator_pair;

  /**
   * Iterator used to iterate over the neighbors, but spcifying the
   * starting point
   */
  typedef util::CircIterator<const_neighbor_iterator> const_circ_neighbor_iterator;

  /**
   * Range of circular iterators
   */
  typedef std::pair<const_circ_neighbor_iterator, const_circ_neighbor_iterator> const_circ_neighbor_iterator_pair;

  //@}

  ///\name Vertex set edition methods
  //@{

  /**
   * Remove a vertex from the graph.
   *
   * \param v vertex to erase
   *
   * \returns 1 if the vertex was erased, 0 else.
   *
   * Example:
   * \code
   * vvgraph S;
   * // Add vertices to S
   * vertex v = S.any();
   * S.erase(v); // Remove a vertex of S
   * \endcode
   */
  size_type erase(const vertex_t& v);

  /**
   * Remove a list of sorted vertices from the graph.
   *
   * The vertices must be specified by index in the graph and sorted from smallest to largest.
   */
  template <typename BidirectionalIterator>
  void erase_sorted_indices(BidirectionalIterator first, BidirectionalIterator last);

  /**
   * Insert a new vertex in the graph.
   *
   * \returns An iterator on the inserted vertex.
   *
   * Example:
   * \code
   * vvgraph S;
   * vertex v; // Create a vertex
   * S.insert(v); // Insert it in the graph
   * \endcode
   */
  iterator insert(const vertex_t& v);

  //@}

  ///\name Vertex set lookup methods
  //@{

  /**
   * Return a vertex from the graph.
   *
   * \returns A vertex of the graph, or a null vertex if the graph is empty.
   *
   * Example:
   * \code
   * vvgraph S;
   * // Add vertices in S
   * const vertex& v = S.any(); // Get a vertex in S
   * \endcode
   */
  const vertex_t& any() const;

  /**
   * Return the element of index \c idx.
   *
   * \note This is a convenience function whose complexity if O(idx)
   *
   * \returns The element \c idx position after the first one (using
   * iterators)
   *
   * Example:
   * \code
   * vvgraph S;
   * // Fill in S
   * size_t i = 0, selected = 0;
   * forall(const vertex& v, S)
   * {
   *   if(condition(v))
   *   {
   *     selected = i;
   *     break;
   *   }
   *   ++i;
   * }
   * // ...
   * const vertex& v = S[selected]; // Work with the vertex previously selected
   * \endcode
   * This construct is useful is case you need to refer to your vertices by
   * 32bits numbers. It is true if you use selection with OpenGL for
   * example.
   */
  const vertex_t& operator[](size_type idx) const;

  /**
   * Test if \c v is in the graph.
   *
   * \returns \c true is \c v is in the graph.
   *
   * Example:
   * \code
   * vvgraph S;
   * vertex v;
   * S.insert(v);
   * assert(S.contains(v));
   * \endcode
   */
  bool contains(const vertex_t& v) const;

  /**
   * Get a reference on the vertex in the graph
   *
   * If the vertex do not exist in the graph, the function returns a null
   * vertex.
   *
   * \note This function is useful if you obtained the vertex not from this
   * graph but will use it intensively.
   *
   * Example:
   * \code
   * vvgraph S1, S2;
   * // Initialize S1 and S2 with the same vertices but different neighborhood
   * forall(const vertex& v1, S1)
   * {
   *   const vertex& v2 = S2.reference(v1);
   *   // Use v2 in S2 and v1 in v1 for speed optimizatioN
   * }
   * \endcode
   */
  const vertex_t& reference(vertex_t v) const
  {
    const NeighborhoodPair* found = this->findVertex(v);
    if(found)
      return found->vertex;
    return vertex_t::null;     // vertex_t(0);
  }

  /**
   * Return the number of vertexes on the graph
   */
  size_type size() const {
    return neighborhood.size();
  }

  /**
   * Test if there is any vertex in the graph
   */
  bool empty() const {
    return neighborhood.empty();
  }

  //@}

  ///\name Neighborhood lookup methods
  //@{

  /**
   * Return a vertex in the neighborhood of \c v.
   *
   * \returns A vertex in the neighborhood of \c v, or a null vertex if \c v is
   * not in the graph or \c v has no neighborhood.
   *
   * Example:
   * \code
   * vvgraph S;
   * forall(const vertex& v, S)
   * {
   *   const vertex& n = S.anyIn(v);
   *   if(n)
   *   {
   *     edge e = S.edge(v,n); // e exist !
   *   }
   * }
   * \endcode
   */
  const vertex_t& anyIn(const vertex_t& v) const;

  /**
   * Returns the number of neighbors of \c v.
   *
   * If \c v is not in the graph, the behavior is undefined.
   *
   * Example:
   * \code
   * vvgraph S;
   * // initialize S
   * forall(const vertex& v, S)
   * {
   *   size_t nb_neighbors = S.valence(v); // Number of neighbors of v in S
   *   // ...
   * }
   * \endcode
   */
  size_type valence(const vertex_t& v) const;

  /**
   * Test if a vertex has a neighbor.
   *
   * Returns false if it does. If a vertex is not in the graph, it is
   * considered as having no neighbor. In the same way, the null vertex has
   * no neighbors.
   *
   * Example:
   * \code
   * vvgraph S;
   * // initialize S
   * forall(cnst vertex& v, S)
   * {
   *   if(!S.empty(v))
   *   {
   *      const vertex& n = S.anyIn(v);
   *      // Working with n a neighbor of v
   *   }
   * }
   * \endcode
   */
  bool empty(const vertex_t& v) const;

  /**
   * Returns any incoming neighbor of \c v.
   *
   * \returns A vertex in the incoming neighborhood of \c v, or a null
   * vertex if \c v is not in the graph or \c v has no incoming neighbor.
   *
   * Example:
   * \code
   * vvgraph S;
   * // initialize S
   * forall(const vertex& n, S)
   * {
   *   const vertex& v = S.iAnyIn(v);
   *   if(v)
   *   {
   *     edge e = S.edge(v, n);
   *     // Work with e the edge from v to n
   *   }
   * }
   * \endcode
   */
  const vertex_t& iAnyIn(const vertex_t& v) const;

  /**
   * Returns the number of incoming neighbors of \c v
   *
   * The incoming neighbors are the set of vertexes with on edge ending on
   * \c v.
   *
   * Example:
   * \code
   * vvgraph S;
   * // initialize S
   * forall(const vertex& n, S)
   * {
   *    size_t nb_ineighbors = S.iValence(n);
   *    // nb_ineighbors is the number of vertices source of an edge toward n
   * }
   * \endcode
   */
  size_type iValence(const vertex_t& v) const;

  /**
   * Test if a vertex has an incoming neighbor.
   *
   * Returns false if it does. If a vertex is not in the graph, it is
   * considered as having no incoming neighbor. In the same way, the null
   * vertex has no incoming neighbors.
   *
   * Example:
   * \code
   * vvgraph S;
   * // initialize S
   * forall(const vertex& n, S)
   * {
   *   if(!S.iEmpty(n))
   *   {
   *     const vertex& v = S.iAnyIn(n);
   *     edge e = S.edge(v,n); // It exists !
   *   }
   * }
   * \endcode
   */
  bool iEmpty(const vertex_t& v) const;

  /**
   * Returns the nth vertex after \c neighbor in the neighborhood of \c v.
   *
   * \returns The vertex found or a null vertex if there is any problem.
   *
   * Example:
   * \code
   * vvgraph S;
   * // initialize S
   * forall(const vertex& v, S)
   * {
   *   forall(const vertex& n, S.neighbors(v))
   *   {
   *     const vertex& m, S.nextTo(v);
   *     // m is the vertex after n in v
   *   }
   * }
   * \endcode
   */
  const vertex_t& nextTo(const vertex_t& v, const vertex_t& neighbor, unsigned int n = 1) const;

  /**
   * Returns the nth vertex before ref in the neighborhood of v.
   *
   * If ref is not in the neighborhood of v, return a null vertex.
   *
   * Example:
   * \code
   * vvgraph S;
   * // initialize S
   * forall(const vertex& v, S)
   * {
   *   forall(const vertex& n, S.neighbors(v))
   *   {
   *     const vertex& m, S.prevTo(v);
   *     // m is the vertex before n in v
   *   }
   * }
   * \endcode
   */
  const vertex_t& prevTo(const vertex_t& v, const vertex_t& ref, unsigned int n = 1) const;

  /**
   * Returns the edge from \c src to \c tgt.
   *
   * If the edge does not exists, returns a null edge.
   *
   * Example:
   * \code
   * struct VertexContent {};
   * struct EdgeContent { int a; }
   * typedef graph::VVGraph<VertexContent,EdgeContent> vvgraph;
   * typedef vvgraph::vertex_t vertex;
   * typedef vvgraph::edge_t edge;
   * // ...
   *
   * vvgraph S;
   * // initialize S
   * forall(const vertex& v, S)
   * {
   *   forall(const vertex& n, S.neighbors(v))
   *   {
   *     edge e = S.edge(v,n);
   *     e->a = 10;
   *   }
   * }
   * \endcode
   */
  edge_t edge(const vertex_t& src, const vertex_t& tgt);

  /**
   * Returns the arc (i.e. undirected edge) between \c src and \c tgt.
   *
   * When the arc is destroyed, the values of both half-edge are
   * synchronized.
   *
   * Example:
   * \code
   * struct VertexContent();
   * struct EdgeContent {int a; }
   * typedef graph::VVGraph<VertexContent,EdgeContent> vvgraph;
   * typedef vvgraph::vertex_t vertex;
   * typedef vvgraph::arc_t arc;
   * typedef vvgraph::edge_t edge;
   * // ...
   *
   * vvgraph S;
   * // initialize S
   * forall(const vertex& v, S)
   * {
   *   forall(const vertex& n, S.neighbors(v))
   *     if(v<n)
   *       S.arc(v,n)->a = 5
   *  }
   * \endcode
   * At the end, all edges have a value of 5 for \c a, even though we
   * applied the function to only half the edges.
   */
  arc_t arc(const vertex_t& src, const vertex_t& tgt);

  /**
   * Returns the edge from \c src to \c tgt.
   *
   * If the edge does not exists, returns a null edge.
   *
   * Example:
   * \code
   * struct VertexContent {};
   * struct EdgeContent { int a; }
   * typedef graph::VVGraph<VertexContent,EdgeContent> vvgraph;
   * typedef vvgraph::vertex_t vertex;
   * typedef vvgraph::edge_t edge;
   * typedef vvgraph::const_edge_t const_edge;
   * // ...
   *
   * void fct(const vvgraph& S)
   * {
   *   forall(const vertex& v, S)
   *   {
   *     forall(const vertex& n, S.neighbors(v))
   *     {
   *       const_edge e = S.edge(v,n);
   *       cout << e->a << endl; // Here e->a cannot be modified
   *     }
   *   }
   * }
   * \endcode
   */
  const_edge_t edge(const vertex_t& src, const vertex_t& tgt) const;

  /**
   * Flag a neighbor for quick retrieval
   *
   * \return true if the vertex has been correctly flagged
   *
   * Example:
   * \code
   * vvgraph S;
   * // Initialize S
   * forall(const vertex& v, S)
   * {
   *   forall(const vertex& n, S.neighbors(v))
   *   {
   *     if(condition(v,n))
   *     {
   *       S.flag(v,n);
   *       break;
   *     }
   *   }
   * }
   * // ...
   * forall(const vertex& v, S)
   * {
   *   const vertex& n = S.flagged(v);
   *   if(n) // If a neighbor has been flagged
   *   {
   *     // Work with the flagged neighbor
   *   }
   * }
   * \endcode
   */
  bool flag(const vertex_t& src, const vertex_t& neighbor);

  /**
   * Return the flagged neighbor or a null vertex is no neighbor have been flagged.
   *
   * \see VVGraph::flag(const vertex_t&, const vertex_t&)
   */
  const vertex_t& flagged(const vertex_t& src) const;

  /**
   * Return the constant range of neighbors of \c v
   *
   * Example:
   * \code
   * vvgraph S;
   * // initialize S
   * forall(const vertex& v, S)
   * {
   *   forall(const vertex& n, S.neighbors(v))
   *   {
   *     // n is a neighbor of v
   *   }
   * }
   * \endcode
   */
  const_neighbor_iterator_pair neighbors(const vertex_t& v) const;
  /**
   * Return the range of neighbors of \c v
   *
   * Example:
   * \code
   * vvgraph S;
   * // initialize S
   * forall(const vertex& v, S)
   * {
   *   forall(const vertex& n, S.neighbors(v))
   *   {
   *     // n is a neighbor of v
   *   }
   * }
   * \endcode
   */
  neighbor_iterator_pair neighbors(const vertex_t& v);

  /**
   * Return the range of neighbors of \c v, starting at \c n
   *
   * Example:
   * \code
   * vvgraph S;
   * // initialize S
   * forall(const vertex& m, S.neighbors(v,n))
   * {
   *   // m is a neighbor of v
   *   // on the first loop, m == n
   * }
   * \endcode
   */
  circ_neighbor_iterator_pair neighbors(const vertex_t& v, const vertex_t& n);

  /**
   * Return the range of neighbors of \c v, starting at \c n
   *
   * Example:
   * \code
   * vvgraph S;
   * // initialize S
   * forall(const vertex& m, S.neighbors(v,n))
   * {
   *   // m is a neighbor of v
   *   // on the first loop, m == n
   * }
   * \endcode
   */
  const_circ_neighbor_iterator_pair neighbors(const vertex_t& v, const vertex_t& n) const;

  /**
   * Return the range of incoming neighbors of \c v.
   *
   * Iterating over iNeighbors will go through all the vertexes having an edge
   * toward \c v.
   *
   * Example:
   * \code
   * vvgraph S;
   * // initialize S
   * forall(const vertex& n, S)
   * {
   *   forall(const vertex& v, S.iNeighbors(v))
   *   {
   *     // n is a neighbor of v
   *   }
   * }
   * \endcode
   */
  ineighbor_iterator_pair iNeighbors(const vertex_t& v);

  /**
   * Return the range of incoming neighbors of \c v.
   *
   * Iterating over iNeighbors will go through all the vertexes having an edge
   * toward \c v.
   *
   * Example:
   * \code
   * vvgraph S;
   * // initialize S
   * forall(const vertex& n, S)
   * {
   *   forall(const vertex& v, S.iNeighbors(v))
   *   {
   *     // n is a neighbor of v
   *   }
   * }
   * \endcode
   */
  const_ineighbor_iterator_pair iNeighbors(const vertex_t& v) const;

  /**
   * Return the source vertex of the edge
   *
   * Example:
   * \code
   * void fct(edge e, const vvgraph& S)
   * {
   *   const vertex& src = S.source(e);
   *   const vertex& tgt = S.target(e);
   *   // Work with the vertices
   * }
   * \endcode
   */
  const vertex_t& source(const edge_t& edge) const
  {
    const NeighborhoodPair* found = this->findVertex(vertex_t(edge.source()));
    if(found)
      return found->vertex;
    return vertex_t::null;     // vertex_t(0);
  }

  const vertex_t& source(const arc_t& arc) const
  {
    const NeighborhoodPair* found = this->findVertex(vertex_t(arc.source()));
    if(found)
      return found->vertex;
    return vertex_t::null;     // vertex_t(0);
  }

  /**
   * Return the target vertex of the edge
   *
   * Example:
   * \code
   * void fct(edge e, const vvgraph& S)
   * {
   *   const vertex& src = S.source(e);
   *   const vertex& tgt = S.target(e);
   *   // Work with the vertices
   * }
   * \endcode
   */
  const vertex_t& target(const edge_t& edge) const
  {
    const NeighborhoodPair* found = this->findVertex(vertex_t(edge.target()));
    if(found)
      return found->vertex;
    return vertex_t::null;     // vertex_t(0);
  }

  const vertex_t& target(const arc_t& arc) const
  {
    const NeighborhoodPair* found = this->findVertex(vertex_t(arc.target()));
    if(found)
      return found->vertex;
    return vertex_t::null;     // vertex_t(0);
  }

  //@}

  ///\name Neighborhood edition methods
  //@{
  /**
   * Erase the edge from \c src to \c tgt.
   *
   * \returns True if successful.
   *
   * Example:
   * \code
   * vvgraph S;
   * // Initialize S
   * forall(const vertex& v, S)
   * {
   *   forall(const vertex& n, S.neighbors(v))
   *   {
   *     if(condition)
   *     {
   *       S.eraseEdge(v,n);
   *       break; // The iteration must not continue after the neighborhood has been altered
   *     }
   *   }
   * }
   * \endcode
   */
  size_type eraseEdge(const vertex_t& src, const vertex_t& tgt);

  /**
   * Replace a vertex by another in a neighborhood.
   *
   * \param[in] v Vertex whose neighborhood is changed.
   * \param[in] neighbor Vertex to replace
   * \param[in] new_neighbor Vertex replacing \c neighbor
   *
   * If \c new_neighbor is already in the neighborhood of \c v, then the
   * operation fails and nothing is changed.
   *
   * \returns The edge between \c v and \c new_neighbor or a null edge if
   * anything goes wrong.
   *
   * Example:
   * \code
   * vvgraph S;
   * // Initialize S
   * forall(const vertex& v, S)
   * {
   *   forall(const vertex& n, S.neighbors(v))
   *   {
   *     if(condition)
   *     {
   *       vertex n1;
   *       S.replace(v,n,n1); // Now n1 is where n was in v
   *       break; // The iteration must not continue after the neighborhood has been altered
   *     }
   *   }
   * }
   * \endcode
   */
  edge_t replace(const vertex_t& v, const vertex_t& neighbor, const vertex_t& new_neighbor);

  /**
   * Insert neighbor in the neighborhood of v after reference.
   *
   * If \c new_neighbor is already in the neighborhood of \c v, the insertion
   * fails.
   *
   * \returns The just created edge if everything succeed, or a null edge.
   *
   * Example:
   * \code
   * vvgraph S;
   * // Initialize S
   * forall(const vertex& v, S)
   * {
   *   forall(const vertex& n, S.neighbors(v))
   *   {
   *     if(condition)
   *     {
   *       vertex n1;
   *       S.spliceAfter(v,n,n1); // Now n1 is after n in v
   *       break; // The iteration must not continue after the neighborhood has been altered
   *     }
   *   }
   * }
   * \endcode
   */
  edge_t spliceAfter(const vertex_t& v, const vertex_t& neighbor, const vertex_t& new_neighbor);

  /**
   * Insert neighbor in the neighborhood of v before reference.
   *
   * If \c new_neighbor is already in the neighborhood of \c v, the insertion
   * fails.
   *
   * \returns The just created edge if everything succeed, or a null edge.
   *
   * Example:
   * \code
   * vvgraph S;
   * // Initialize S
   * forall(const vertex& v, S)
   * {
   *   forall(const vertex& n, S.neighbors(v))
   *   {
   *     if(condition)
   *     {
   *       vertex n1;
   *       S.spliceBefore(v,n,n1); // Now n1 is before n in v
   *       break; // The iteration must not continue after the neighborhood has been altered
   *     }
   *   }
   * }
   * \endcode
   */
  edge_t spliceBefore(const vertex_t& v, const vertex_t& neighbor, const vertex_t& new_neighbor);

  /**
   * Insert a new edge in the graph, without ordering.
   *
   * If \c new_neighbor is already in the neighborhood of \c v, the insertion
   * fails.
   *
   * \returns The just created edge if everything succeed, or a null edge.
   *
   * Example:
   * \code
   * vvgraph S;
   * vertex v1, v2;
   * S.insert(v1);
   * S.insert(v2);
   * S.insertEdge(v1, v2); // Insert the edge between v1 and v2
   * S.insertEdge(v2, v1); // Insert the edge between v2 and v1
   * \endcode
   */
  edge_t insertEdge(const vertex_t& src, const vertex_t& tgt);

  /**
   * Clear the neighborhood of a vertex.
   *
   * All edges, to and from \c v will be erased.
   *
   * Example:
   * \code
   * vvgraph S;
   * // Initialize S
   * forall(const vertex& v, S)
   * {
   *   if(condition)
   *   {
   *     S.eraseAllEdges(v);
   *     assert(S.empty(v)); // No outgoing edges
   *     assert(S.iEmpty(v)); // No incoming edges
   *   }
   * }
   * \endcode
   */
  bool eraseAllEdges(const vertex_t& v);

  /**
   * Clear the outgoing edges of a vertex
   *
   * Example:
   * \code
   * vvgraph S;
   * // Initialize S
   * forall(const vertex& v, S)
   * {
   *   if(condition)
   *   {
   *     S.clear(v);
   *     assert(S.empty(v));
   *   }
   * }
   * \endcode
   */
  bool clear(const vertex_t& v);

  //@}

  ///\name Whole structure methods
  //@{
  /**
   * Copy the graph into another graph
   */
  VVGraph& operator=(const VVGraph& other);

  /**
   * Reverse the order of all the neighbors
   */
  VVGraph& reverse();

  /**
   * Swap the content of both graphs
   */
  void swap(VVGraph& other);

  /**
   * Test equality for the graphs.
   *
   * Two graphs are equal if they shared the sames vertices and their
   * neighborhood are equivalent.
   *
   * \note Current implementation do not consider invariance of
   * neighborhood by rotation.
   */
  bool operator==(const VVGraph& other) const;

  /**
   * Clear the graph.
   */
  void clear();

  /**
   * Clear all the edges in the graph
   *
   * Example:
   * \code
   * vvgraph S;
   * // Initialize S
   * vvgraph::size_type s1 = S.size(); // Save the number of vertices
   * S.eraseAllEdges();
   * assert(s1 == S.size()); // The number of vertices didn't change
   * forall(const vertex& v, S)
   * {
   *   assert(S.empty(v)); // No neighbor
   * }
   * \endcode
   */
  void eraseAllEdges();

  /**
   * Extract the subgraph containing the \p vertexes
   *
   * The subgraph is the set of vertexes and the edges whose source and target
   * are in the extracted vertexes.
   *
   * Example:
   * \code
   * vvgraph S;
   * // Initialize S
   * std::vector<vertex> to_keep;
   * // Fill in to_keep with a subset of the vertices of S
   * vvgraph S1 = S.subgraph(to_keep);
   * \endcode
   */
  template <typename VertexContainer> VVGraph subgraph(const VertexContainer& vertexes) const;
  //@}

  ///\name STL compatibility methods
  //@{

  /**
   * Erase element at position \p pos
   */
  void erase(iterator pos);

  /**
   * Erase the edge pointed to by the iterator.
   */
  size_type eraseEdge(const vertex_t& v, neighbor_iterator pos);

  /**
   * Erase the edge pointed to by the iterator.
   */
  size_type eraseEdge(const vertex_t& v, circ_neighbor_iterator pos);

  /**
   * Insert a new vertex in the graph
   *
   * The iterator is ignored ...
   */
  iterator insert(iterator pos, const vertex_t& v);
  /**
   * Insert all the vertexes from first to last-1 in the graph.
   */
  template <typename Iterator> void insert(Iterator first, Iterator last, bool check_unique = true);

  /**
   * Insert all the vertexes from range.first to range.last-1 in the graph.
   */
  template <typename Iterator> void insert(const std::pair<Iterator, Iterator>& range, bool check_unique = true);

  /**
   * Returns an iterator on the beginning of the set of vertexes of the graph.
   */
  iterator begin() {
    return neighborhood.begin();
  }
  /**
   * Returns an iterator on the end of the set of vertexes of the graph.
   */
  iterator end() {
    return neighborhood.end();
  }

  /**
   * Returns an iterator on the beginning of the set of vertexes of the graph.
   */
  const_iterator begin() const {
    return neighborhood.begin();
  }
  /**
   * Returns an iterator on the end of the set of vertexes of the graph.
   */
  const_iterator end() const {
    return neighborhood.end();
  }

  /**
   * Store the vertices in a STL container. By default, the cache will be
   * setup for quick vertex access.
   *
   * Example:
   * \code
   * vvgraph S;
   * // Initialize S
   * std::vector<vertex> vertices;
   * S.store_vertices(vertices);
   * forall(const vertex& v, vertices)
   * {
   *   // The cache information is used to speed up lookup
   *   if(condition)
   *   {
   *     S.erase(v); // Valid, but do not use v in S after that
   *   }
   * }
   * \endcode
   */
  void store_vertices(std::vector<vertex_t>& vertices) const
  {
    vertices.resize(this->size(), vertex_t::null);
    size_t k = 0;
    for(typename neighborhood_t::const_iterator it = neighborhood.begin(); it != neighborhood.end(); ++it) {
      const vertex_t& v = (*it)->vertex;
      vertices[k] = v;
      ++k;
    }
  }

  /**
   * Find the vertex \c v in the graph.
   *
   * \returns An iterator on \c v if found, or end()
   */
   iterator find(const vertex_t& v);
   const_iterator find(const vertex_t& v) const;

  /**
   * Find the vertex \c v in the graph.
   *
   * \returns An iterator on \c v if found, or end()
   */
  // const_iterator find(const vertex_t& v) const { return this->findVertex(v); }

  /**
   * Count the number of vertexes \p v in the graph (can be 0 or 1 only)
   */
  size_type count(const vertex_t& v) const;

  /**
   * Find the vertex \c n in the neighborhood of \c v.
   *
   * \returns
   *  - An iterator on \c n in the neighborhood of \c v, if found
   *  - An iterator on the end of the neighborhood if \c n is not found in \c
   *  v
   *  - Otherwise, the result is undefined
   */
  neighbor_iterator findIn(const vertex_t& v, const vertex_t& n);
  /**
   * Find the vertex \c n in the neighborhood of \c v.
   *
   * \returns
   *  - An iterator on \c n in the neighborhood of \c v, if found
   *  - An iterator on the end of the neighborhood if \c n is not found in \c
   *  v
   *  - Otherwise, the result is undefined
   */
  const_neighbor_iterator findIn(const vertex_t& v, const vertex_t& n) const;

  /**
   * Clear the neighborhood of a vertex.
   *
   * All edges, to and from \c *it will be erased.
   */
  void eraseAllEdges(iterator it);

  /**
   * Clear the outgoing edges of a vertex.
   */
  void clear(iterator it);

  /**
   * Reserve space for more vertices
   */
  void reserve(size_type s) {
    neighborhood.reserve(s);
  }

  //@}

protected:
  void eraseAllEdges(NeighborhoodPair* it);
  /**
   * Type of the result of the search for a vertex in a neighborhood
   *
   * This data structure is made such that all edit or lookup operations can
   * be done from that data structure.
   */
  template <class Neighborhood, class Iterator> struct search_result_t {
    /**
     * Default constructor
     *
     * By default, the search is unsuccessful
     */
    search_result_t()
      : found(false)
      , neighborhood(0)
    {
    }

    /**
     * Successful constructor
     */
    search_result_t(Iterator i, Neighborhood* n, bool ok = true)
      : found(ok)
      , it(i)
      , neighborhood(n)
    {
    }

    /**
     * Copy constructor
     */
    search_result_t(const search_result_t& copy)
      : found(copy.found)
      , it(copy.it)
      , neighborhood(copy.neighborhood)
    {
    }

    /**
     * Convert the object to true if the search is successful
     */
    operator bool() {
      return found;
    }

    /**
     * True if the search was completely successful
     */
    bool found;
    /**
     * Iterator pointing in the edge list
     */
    Iterator it;
    /**
     * Neighborhood containing the element
     */
    Neighborhood* neighborhood;
  };

  /**
   * Iterator on the outgoing edges
   */
  typedef typename edge_list_t::iterator int_neighbor_iterator;
  /**
   * Constant iterator on the outgoing edges
   */
  typedef typename edge_list_t::const_iterator int_const_neighbor_iterator;

  /**
   * Result of a search in the neighborhood
   */
  typedef search_result_t<single_neighborhood_t, int_neighbor_iterator> neighbor_found_t;
  /**
   * Constant result of a search in the neighborhood
   */
  typedef search_result_t<const single_neighborhood_t, int_const_neighbor_iterator> const_neighbor_found_t;

  /**
   * Find a vertex in the graph and returns the iterator on it.
   *
   * Goal is to introduce a new optimization. When iterating over the
   * vertices of the graph, they will cache their own iterator. So accessing
   * their neighborhood would be constant time.
   */
  NeighborhoodPair* findVertex(const vertex_t& v);

  /**
   * Constant version of findVertex(const vertex_t&)
   */
  const NeighborhoodPair* findVertex(const vertex_t& v) const;

  /**
   * Find the iterator for a given neighborhood pair
   */
  typename neighborhood_t::iterator findIterator(const NeighborhoodPair* np);
  typename neighborhood_t::const_iterator findIterator(const NeighborhoodPair* np) const;

  /**
   * Find a vertex neighbor in the neighborhood of v.
   *
   * If 'v' is in the graph and 'neighbor' is in its neighborhood, the
   * result is convertible to true, 'neighborhood' points toward the
   * neighborhood structure and 'it' points toward the neighbor.
   *
   * If 'v' is in the graph, but 'neighbor' is not in the neighborhood, the
   * result is convertible to false, 'neighborhood' points toward the
   * neighborhood structure and 'it' is neighborhood->edges.end().
   *
   * If v is not in the graph, the result is convertible to false and the
   * address of the stored neighborhood is 0.
   */
  neighbor_found_t findInVertex(const vertex_t& v, const vertex_t& neighbor);

  /**
   * Constant version of findInVertex(const vertex_t&, const vertex_t&)
   */
  const_neighbor_found_t findInVertex(const vertex_t& v, const vertex_t& neighbor) const;

  /**
   * Insert v in the in_edges of new_neighbor and return true if the insertion
   * was actually done, or false if v was already there.
   */
  std::pair<ineighbor_iterator, bool> insertInEdge(const vertex_t& v, const vertex_t& new_neighbor);

  /**
   * Undo the insertion of an in edge as a roll-back mechanism if the whole
   * edge insertion fails.
   */
  void undoInEdge(const vertex_t& new_neighbor, ineighbor_iterator& it);

  /**
   * Main data structure containing everything
   */
  neighborhood_t neighborhood;

  /**
   * Hash map to optimise look-up
   */
  lookup_t lookup;

  /**
   * If true, the vertex list has been modified
   */
  bool modified_vertices;
};

template <GRAPH_TEMPLATE> typename VVGraph<GRAPH_ARGS>::EdgeAlloc VVGraph<GRAPH_ARGS>::edge_alloc;
template <GRAPH_TEMPLATE> typename VVGraph<GRAPH_ARGS>::NPAlloc VVGraph<GRAPH_ARGS>::np_alloc;

template <GRAPH_TEMPLATE> void VVGraph<GRAPH_ARGS>::clear()
{
  typename neighborhood_t::iterator it;
  for(it = neighborhood.begin(); it != neighborhood.end(); ++it) {
    NeighborhoodPair::Delete(*it);
    *it = 0;
  }
  neighborhood.clear();
  lookup.clear();
}

template <GRAPH_TEMPLATE> void VVGraph<GRAPH_ARGS>::erase(iterator it)
{
  eraseAllEdges(it);
  lookup.erase(*it);
  NeighborhoodPair::Delete(*it.base());
  if(it != end()-1) {
    lookup[neighborhood.back()->vertex] = size_t(it - begin());
    *it.base() = neighborhood.back();
  }
  neighborhood.pop_back();
}

template <GRAPH_TEMPLATE>
template <typename BidirectionalIterator>
void VVGraph<GRAPH_ARGS>::erase_sorted_indices(BidirectionalIterator first, BidirectionalIterator last)
{
  typedef std::reverse_iterator<BidirectionalIterator> rev_iterator;
  rev_iterator rev_first(last), rev_last(first);
  size_t current_size = size();
  for(rev_iterator it = rev_first ; it != rev_last ; ++it) {
    iterator found = begin() + *it;
    eraseAllEdges(found);
    lookup.erase(*found);
    NeighborhoodPair::Delete(*found.base());
    current_size--;
    if(current_size > *it) {
      lookup[neighborhood[current_size]->vertex] = *it;
      neighborhood[*it] = neighborhood[current_size];
    }
  }
  neighborhood.resize(current_size);
}

template <GRAPH_TEMPLATE> typename VVGraph<GRAPH_ARGS>::size_type
VVGraph<GRAPH_ARGS>::erase(const vertex_t& v)
{
  typename lookup_t::iterator it_found = lookup.find(v);
  if(it_found != lookup.end()) {
    typename neighborhood_t::iterator it = this->findIterator(this->neighborhood[it_found->second]);
    erase(it);
    return 1;
  }
  return 0;
}

template <GRAPH_TEMPLATE> void VVGraph<GRAPH_ARGS>::eraseAllEdges()
{
  for(typename neighborhood_t::iterator it = neighborhood.begin(); it != neighborhood.end(); ++it) {
    (*it)->single_neighborhood.edges.clear();
    (*it)->single_neighborhood.in_edges.clear();
    (*it)->single_neighborhood.flagged = 0;
  }
}

template <GRAPH_TEMPLATE> bool VVGraph<GRAPH_ARGS>::eraseAllEdges(const vertex_t& v)
{
  NeighborhoodPair* it_found = this->findVertex(v);
  if(it_found) {
    eraseAllEdges(it_found);
    return true;
  }
  return false;
}

template <GRAPH_TEMPLATE> void VVGraph<GRAPH_ARGS>::eraseAllEdges(NeighborhoodPair* np)
{
  const vertex_t& v = np->vertex;
  for(typename edge_list_t::iterator it_el = np->single_neighborhood.edges.begin();
      it_el != np->single_neighborhood.edges.end(); ++it_el) {
    typename lookup_t::iterator lookup_found = lookup.find(it_el->target);
    if(lookup_found == lookup.end()) throw UnknownNeighbor(np->vertex.id(), it_el->target.id());
    in_edges_t& ie = this->neighborhood[lookup_found->second]->single_neighborhood.in_edges;
    typename in_edges_t::iterator it_found = std::find(ie.begin(), ie.end(), v);
    ie.erase(it_found);
  }
  np->single_neighborhood.edges.clear();
  np->single_neighborhood.flagged = 0;
  // Remove the incoming edges
  in_edges_t& in_edges = np->single_neighborhood.in_edges;
  for(typename in_edges_t::iterator it = in_edges.begin(); it != in_edges.end(); ++it) {
    neighbor_found_t found = findInVertex(*it, v);
    found.neighborhood->edges.erase(found.it);
    if(found.neighborhood->flagged && *found.neighborhood->flagged == v)
      found.neighborhood->flagged = 0;
  }
  in_edges.clear();
}

template <GRAPH_TEMPLATE> void VVGraph<GRAPH_ARGS>::eraseAllEdges(iterator it_found)
{
  eraseAllEdges(*it_found.base());
}

template <GRAPH_TEMPLATE> bool VVGraph<GRAPH_ARGS>::clear(const vertex_t& v)
{
  NeighborhoodPair* it_found = this->findVertex(v);
  if(not it_found) {
    clear(it_found->vertex);
    return true;
  }
  return false;
}

template <GRAPH_TEMPLATE> void VVGraph<GRAPH_ARGS>::clear(iterator it)
{
  const vertex_t& v = *it;
  for(typename edge_list_t::iterator it_el = (*it.base())->single_neighborhood.edges.begin();
      it_el != (*it.base())->single_neighborhood.edges.end(); ++it_el) {
    NeighborhoodPair* it_found = this->findVertex(it_el->target);
    in_edges_t& ie = it_found->single_neighborhood.in_edges;
    typename in_edges_t::iterator it_found2 = std::find(ie.begin(), ie.end(), v);
    ie.erase(it_found2);
  }
  (*it.base())->single_neighborhood.edges.clear();
}

template <GRAPH_TEMPLATE>
typename VVGraph<GRAPH_ARGS>::size_type VVGraph<GRAPH_ARGS>::count(const vertex_t& v) const
{
  return lookup.count(v);
}

template <GRAPH_TEMPLATE>
typename VVGraph<GRAPH_ARGS>::iterator VVGraph<GRAPH_ARGS>::find(const vertex_t& v)
{
  auto found = lookup.find(v);
  if(found == lookup.end())
    return end();
  return this->begin() + found->second;
}

template <GRAPH_TEMPLATE>
typename VVGraph<GRAPH_ARGS>::const_iterator VVGraph<GRAPH_ARGS>::find(const vertex_t& v) const
{
  auto found = lookup.find(v);
  if(found == lookup.end())
    return end();
  return this->begin() + found->second;
}

template <GRAPH_TEMPLATE> typename VVGraph<GRAPH_ARGS>::iterator VVGraph<GRAPH_ARGS>::insert(const vertex_t& v)
{
  typename lookup_t::iterator it_found = lookup.find(v);
  if(it_found == lookup.end()) {
    NeighborhoodPair* np = NeighborhoodPair::New(v);
    lookup[v] = neighborhood.size();
    neighborhood.push_back(np);
    iterator it = neighborhood.end();
    --it;
    return it;
  }
  return findIterator(this->neighborhood[it_found->second]);
}

template <GRAPH_TEMPLATE>
typename VVGraph<GRAPH_ARGS>::iterator VVGraph<GRAPH_ARGS>::insert(iterator, const vertex_t& v)
{
  return this->insert(v);
}

template <GRAPH_TEMPLATE>
template <typename Iterator>
void VVGraph<GRAPH_ARGS>::insert(const std::pair<Iterator, Iterator>& range, bool check_unique)
{
  insert(range.first, range.second, check_unique);
}

template <GRAPH_TEMPLATE>
template <typename Iterator>
void VVGraph<GRAPH_ARGS>::insert(Iterator first, Iterator last, bool check_unique)
{
  if(check_unique) {
    for(Iterator it = first; it != last; ++it) {
      insert(*it);
    }
  } else {
    for(Iterator it = first; it != last; ++it) {
      NeighborhoodPair* np = NeighborhoodPair::New(*it);
      lookup[*it] = neighborhood.size();
      neighborhood.push_back(np);
    }
  }
}

template <GRAPH_TEMPLATE> const typename VVGraph<GRAPH_ARGS>::vertex_t& VVGraph<GRAPH_ARGS>::any() const
{
  if(neighborhood.empty())
    return vertex_t::null;
  return *begin();
}

template <GRAPH_TEMPLATE>
const typename VVGraph<GRAPH_ARGS>::vertex_t& VVGraph<GRAPH_ARGS>::operator[](size_type idx) const
{
  typename neighborhood_t::const_iterator it = neighborhood.begin();
  std::advance(it, idx);
  return (*it)->vertex;
}

template <GRAPH_TEMPLATE>
const typename VVGraph<GRAPH_ARGS>::vertex_t& VVGraph<GRAPH_ARGS>::anyIn(const vertex_t &v) const
{
  if(v.isNull())
    return vertex_t::null;     // vertex_t(0);
  const NeighborhoodPair* it_found = this->findVertex(v);
  if(not it_found || it_found->single_neighborhood.edges.empty())
    return vertex_t::null;     // vertex_t(0);
  const edge_list_t& lst = it_found->single_neighborhood.edges;
  return lst.front().target;
}

template <GRAPH_TEMPLATE> typename VVGraph<GRAPH_ARGS>::size_type VVGraph<GRAPH_ARGS>::valence(const vertex_t& v) const
{
  if(v.isNull())
    return 0;
  const NeighborhoodPair* it_found = this->findVertex(v);
  if(not it_found)
    return 0;
  return it_found->single_neighborhood.edges.size();
}

template <GRAPH_TEMPLATE> bool VVGraph<GRAPH_ARGS>::empty(const vertex_t& v) const
{
  if(v.isNull())
    return true;
  const NeighborhoodPair* it_found = this->findVertex(v);
  if(not it_found)
    return true;
  return it_found->single_neighborhood.edges.empty();
}

template <GRAPH_TEMPLATE>
const typename VVGraph<GRAPH_ARGS>::vertex_t& VVGraph<GRAPH_ARGS>::iAnyIn(const vertex_t &v) const
{
  if(v.isNull())
    return vertex_t::null;     // vertex_t(0);
  const NeighborhoodPair* it_found = this->findVertex(v);
  if(not it_found || it_found->single_neighborhood.in_edges.empty())
    return vertex_t::null;     // vertex_t(0);
  const in_edges_t& lst = it_found->single_neighborhood.in_edges;
  return *lst.begin();
}

template <GRAPH_TEMPLATE> typename VVGraph<GRAPH_ARGS>::size_type VVGraph<GRAPH_ARGS>::iValence(const vertex_t& v) const
{
  if(v.isNull())
    return 0;
  const NeighborhoodPair* it_found = this->findVertex(v);
  if(not it_found)
    return 0;
  return it_found->single_neighborhood.in_edges.size();
}

template <GRAPH_TEMPLATE> bool VVGraph<GRAPH_ARGS>::iEmpty(const vertex_t& v) const
{
  if(v.isNull())
    return true;
  const NeighborhoodPair* it_found = this->findVertex(v);
  if(not it_found)
    return true;
  return it_found->single_neighborhood.in_edges.empty();
}

template <GRAPH_TEMPLATE>
typename VVGraph<GRAPH_ARGS>::neighborhood_t::iterator VVGraph<GRAPH_ARGS>::findIterator(const NeighborhoodPair* np)
{
  return std::find(neighborhood.begin(), neighborhood.end(), np);
}

template <GRAPH_TEMPLATE>
typename VVGraph<GRAPH_ARGS>::neighborhood_t::const_iterator
VVGraph<GRAPH_ARGS>::findIterator(const NeighborhoodPair* np) const
{
  return std::find(neighborhood.begin(), neighborhood.end(), np);
}

template <GRAPH_TEMPLATE>
typename VVGraph<GRAPH_ARGS>::NeighborhoodPair* VVGraph<GRAPH_ARGS>::findVertex(const vertex_t &v)
{
  typename lookup_t::const_iterator it_found = lookup.find(v);
  if(it_found != lookup.end())
    return this->neighborhood[it_found->second];
  return 0;
}

template <GRAPH_TEMPLATE>
const typename VVGraph<GRAPH_ARGS>::NeighborhoodPair* VVGraph<GRAPH_ARGS>::findVertex(const vertex_t &v) const
{
  typename lookup_t::const_iterator it_found = lookup.find(v);
  if(it_found != lookup.end())
    return this->neighborhood[it_found->second];
  return 0;
}

template <GRAPH_TEMPLATE>
typename VVGraph<GRAPH_ARGS>::neighbor_found_t VVGraph<GRAPH_ARGS>::findInVertex(const vertex_t& v, const vertex_t& n)
{
  if(v.isNull() || n.isNull() || v == n)
    return neighbor_found_t();
  NeighborhoodPair* it_found = this->findVertex(v);
  if(not it_found)
    return neighbor_found_t();
  edge_list_t& lst = it_found->single_neighborhood.edges;
  single_neighborhood_t* neighborhood = &it_found->single_neighborhood;
  for(typename edge_list_t::iterator it = lst.begin(); it != lst.end(); ++it) {
    if(it->target == n) {
      return neighbor_found_t(it, neighborhood);
    }
  }
  return neighbor_found_t(lst.end(), neighborhood, false);
}

template <GRAPH_TEMPLATE>
typename VVGraph<GRAPH_ARGS>::const_neighbor_found_t VVGraph<GRAPH_ARGS>::findInVertex(const vertex_t& v,
                                                                                       const vertex_t& n) const
{
  if(v.isNull() || n.isNull() || v == n)
    return const_neighbor_found_t();
  const NeighborhoodPair* it_found = this->findVertex(v);
  if(not it_found)
    return const_neighbor_found_t();
  const edge_list_t& lst = it_found->single_neighborhood.edges;
  const single_neighborhood_t* neighborhood = &it_found->single_neighborhood;
  for(typename edge_list_t::const_iterator it = lst.begin(); it != lst.end(); ++it) {
    if(it->target == n) {
      return const_neighbor_found_t(it, neighborhood);
    }
  }
  return const_neighbor_found_t(lst.end(), neighborhood, false);
}

template <GRAPH_TEMPLATE>
typename VVGraph<GRAPH_ARGS>::neighbor_iterator VVGraph<GRAPH_ARGS>::findIn(const vertex_t& v, const vertex_t& n)
{
  neighbor_found_t found = this->findInVertex(v, n);
  if(found.neighborhood)
    return neighbor_iterator(found.it);
  return neighbor_iterator();
}

template <GRAPH_TEMPLATE>
typename VVGraph<GRAPH_ARGS>::const_neighbor_iterator VVGraph<GRAPH_ARGS>::findIn(const vertex_t& v,
                                                                                  const vertex_t& n) const
{
  const_neighbor_found_t found = this->findInVertex(v, n);
  if(found.neighborhood)
    return const_neighbor_iterator(found.it);
  return const_neighbor_iterator(found.it);
}

template <GRAPH_TEMPLATE>
typename VVGraph<GRAPH_ARGS>::size_type VVGraph<GRAPH_ARGS>::eraseEdge(const vertex_t& v, neighbor_iterator pos)
{
  NeighborhoodPair* it_found = this->findVertex(v);
  if(not it_found)
    return 0;
  const vertex_t& n = *pos;
  NeighborhoodPair* it_found2 = this->findVertex(n);
  if(not it_found2)
    return 0;
  in_edges_t& ie = it_found2->single_neighborhood.in_edges;
  typename in_edges_t::iterator it_found_ie = std::find(ie.begin(), ie.end(), v);
  if(it_found_ie != ie.end()) {
    ie.erase(it_found_ie);
    typename edge_list_t::iterator it = pos.base();
    it_found->single_neighborhood.edges.erase(it);
    return 1;
  }
  return 0;
}

template <GRAPH_TEMPLATE>
typename VVGraph<GRAPH_ARGS>::size_type VVGraph<GRAPH_ARGS>::eraseEdge(const vertex_t& v, circ_neighbor_iterator pos)
{
  return eraseEdge(v, pos.base());
}

template <GRAPH_TEMPLATE>
typename VVGraph<GRAPH_ARGS>::size_type VVGraph<GRAPH_ARGS>::eraseEdge(const vertex_t& v, const vertex_t& n)
{
  neighbor_found_t found = this->findInVertex(v, n);
  if(!found)
    return 0;
  if(found.neighborhood->flagged && *found.neighborhood->flagged == n)
    found.neighborhood->flagged = 0;
  found.neighborhood->edges.erase(found.it);
  typename lookup_t::iterator lookup_found = lookup.find(n);
  if(lookup_found == lookup.end()) throw UnknownNeighbor(v.id(), n.id());
  in_edges_t& ie = this->neighborhood[lookup_found->second]->single_neighborhood.in_edges;
  typename in_edges_t::iterator it_found_ie = std::find(ie.begin(), ie.end(), v);
  ie.erase(it_found_ie);
  return 1;
}

template <GRAPH_TEMPLATE> bool VVGraph<GRAPH_ARGS>::contains(const vertex_t& v) const {
  return this->findVertex(v);
}

template <GRAPH_TEMPLATE>
std::pair<typename VVGraph<GRAPH_ARGS>::ineighbor_iterator, bool>
VVGraph<GRAPH_ARGS>::insertInEdge(const vertex_t& v, const vertex_t& new_neighbor)
{
  typename lookup_t::iterator found = lookup.find(new_neighbor);
  if(found == lookup.end())
    return std::make_pair(ineighbor_iterator(), false);
  in_edges_t& ie = this->neighborhood[found->second]->single_neighborhood.in_edges;
  typename in_edges_t::iterator it_found = std::find(ie.begin(), ie.end(), v);
  if(it_found != ie.end())
    return std::make_pair(it_found, false);
  ie.push_back(v);
  ineighbor_iterator it = ie.end();
  --it;
  return std::make_pair(it, true);
}

template <GRAPH_TEMPLATE> void VVGraph<GRAPH_ARGS>::undoInEdge(const vertex_t& new_neighbor, ineighbor_iterator& it)
{
  typename lookup_t::iterator found = lookup.find(new_neighbor);
  // typename neighborhood_t::iterator found = neighborhood.find(new_neighbor);
  // if(found == neighborhood.end())
  // return;
  if(found == lookup.end())
    return;
  this->neighborhood[found->second]->single_neighborhood.in_edges.erase(it);
}

template <GRAPH_TEMPLATE>
typename VVGraph<GRAPH_ARGS>::edge_t VVGraph<GRAPH_ARGS>::replace(const vertex_t& v, const vertex_t& neighbor,
                                                                  const vertex_t& new_neighbor)
{
  if(new_neighbor.isNull() or (v == new_neighbor) or (neighbor == new_neighbor))
    return edge_t();
  neighbor_found_t found = this->findInVertex(v, neighbor);
  if(!found)
    return edge_t();
  NeighborhoodPair* n_found = this->findVertex(neighbor);
  ineighbor_iterator it_in;
  bool inserted;
  util::tie(it_in, inserted) = insertInEdge(v, new_neighbor);
  if(!inserted)
    return edge_t();
  in_edges_t& ie = n_found->single_neighborhood.in_edges;
  typename in_edges_t::iterator it_found_ie = std::find(ie.begin(), ie.end(), v);
  ie.erase(it_found_ie);
  found.it->target = new_neighbor;
  found.it->clear_edge();
  if(n_found->single_neighborhood.flagged and *n_found->single_neighborhood.flagged == neighbor)
    n_found->single_neighborhood.flagged = 0;
  return edge_t(v.id(), new_neighbor.id(), &*found.it);
}

template <GRAPH_TEMPLATE>
const typename VVGraph<GRAPH_ARGS>::vertex_t& VVGraph<GRAPH_ARGS>::nextTo(const vertex_t &v, const vertex_t &ref,
                                                                          unsigned int n) const
{
  const_neighbor_found_t found = this->findInVertex(v, ref);
  if(!found)
    return vertex_t::null;     // vertex_t(0);
  typename edge_list_t::const_iterator it = found.it;
  const edge_list_t& lst = found.neighborhood->edges;
  for(unsigned int i = 0; i < n; ++i) {
    ++it;
    if(it == lst.end())
      it = lst.begin();
  }
  return it->target;
}

template <GRAPH_TEMPLATE>
const typename VVGraph<GRAPH_ARGS>::vertex_t& VVGraph<GRAPH_ARGS>::prevTo(const vertex_t &v, const vertex_t &ref,
                                                                          unsigned int n) const
{
  const_neighbor_found_t found = this->findInVertex(v, ref);
  if(!found)
    return vertex_t::null;     // vertex_t(0);
  typename edge_list_t::const_iterator it = found.it;
  const edge_list_t& lst = found.neighborhood->edges;
  for(unsigned int i = 0; i < n; ++i) {
    if(it == lst.begin())
      it = lst.end();
    --it;
  }
  return it->target;
}

template <GRAPH_TEMPLATE>
typename VVGraph<GRAPH_ARGS>::edge_t VVGraph<GRAPH_ARGS>::edge(const vertex_t& src, const vertex_t& tgt)
{
  neighbor_found_t found = this->findInVertex(src, tgt);
  if(!found)
    return edge_t();
  return edge_t(src.id(), tgt.id(), &*found.it);
}

template <GRAPH_TEMPLATE>
typename VVGraph<GRAPH_ARGS>::arc_t VVGraph<GRAPH_ARGS>::arc(const vertex_t& src, const vertex_t& tgt)
{
  neighbor_found_t found = this->findInVertex(src, tgt);
  if(!found)
    return arc_t();
  neighbor_found_t other = this->findInVertex(tgt, src);
  if(!other)
    return arc_t();
  return arc_t(src.id(), tgt.id(), &*found.it, &*other.it);
}

template <GRAPH_TEMPLATE>
typename VVGraph<GRAPH_ARGS>::const_edge_t VVGraph<GRAPH_ARGS>::edge(const vertex_t& src, const vertex_t& tgt) const
{
  const_neighbor_found_t found = this->findInVertex(src, tgt);
  if(!found)
    return const_edge_t();
  return const_edge_t(src.id(), tgt.id(), &*found.it);
}

template <GRAPH_TEMPLATE> bool VVGraph<GRAPH_ARGS>::flag(const vertex_t& src, const vertex_t& neighbor)
{
  neighbor_found_t found = this->findInVertex(src, neighbor);
  if(!found)
    return false;
  found.neighborhood->flagged = &(found.it->target);
  return true;
}

template <GRAPH_TEMPLATE>
const typename VVGraph<GRAPH_ARGS>::vertex_t& VVGraph<GRAPH_ARGS>::flagged(const vertex_t &src) const
{
  const NeighborhoodPair* found = this->findVertex(src);
  if(not found or found->single_neighborhood.flagged == 0)
    return vertex_t::null;
  return *found->single_neighborhood.flagged;
}

template <GRAPH_TEMPLATE>
typename VVGraph<GRAPH_ARGS>::edge_t VVGraph<GRAPH_ARGS>::spliceAfter(const vertex_t& v, const vertex_t& neighbor,
                                                                      const vertex_t& new_neighbor)
{
  if(new_neighbor.isNull() || v == new_neighbor)
    return edge_t();
  neighbor_found_t found = this->findInVertex(v, neighbor);
  if(!found)
    return edge_t();
  ineighbor_iterator it_in;
  bool inserted;
  util::tie(it_in, inserted) = insertInEdge(v, new_neighbor);
  if(!inserted)
    return edge_t();
  ++found.it;
  typename edge_list_t::iterator new_edge_it
    = found.neighborhood->edges.insert(found.it, neighbor_t(new_neighbor, EdgeContent()));
  return edge_t(v.id(), new_neighbor.id(), &*new_edge_it);
}

template <GRAPH_TEMPLATE>
typename VVGraph<GRAPH_ARGS>::edge_t VVGraph<GRAPH_ARGS>::spliceBefore(const vertex_t& v, const vertex_t& neighbor,
                                                                       const vertex_t& new_neighbor)
{
  if(new_neighbor.isNull() || v == new_neighbor)
    return edge_t();
  neighbor_found_t found = this->findInVertex(v, neighbor);
  if(!found)
    return edge_t();
  ineighbor_iterator it_in;
  bool inserted;
  util::tie(it_in, inserted) = insertInEdge(v, new_neighbor);
  if(!inserted)
    return edge_t();
  typename edge_list_t::iterator new_edge_it
    = found.neighborhood->edges.insert(found.it, neighbor_t(new_neighbor, EdgeContent()));
  return edge_t(v.id(), new_neighbor.id(), &*new_edge_it);
}

template <GRAPH_TEMPLATE>
typename VVGraph<GRAPH_ARGS>::edge_t VVGraph<GRAPH_ARGS>::insertEdge(const vertex_t& v, const vertex_t& new_neighbor)
{
  if(v.isNull() || new_neighbor.isNull() || v == new_neighbor)
    return edge_t();
  NeighborhoodPair* it_found = this->findVertex(v);
  if(not it_found)
    return edge_t();
  ineighbor_iterator it_in;
  bool inserted;
  util::tie(it_in, inserted) = insertInEdge(v, new_neighbor);
  if(!inserted)
    return edge_t();
  edge_list_t& lst = it_found->single_neighborhood.edges;
  lst.push_front(neighbor_t(new_neighbor, EdgeContent()));
  typename edge_list_t::iterator it = lst.begin();
  return edge_t(v.id(), new_neighbor.id(), &*it);
}

template <GRAPH_TEMPLATE>
typename VVGraph<GRAPH_ARGS>::const_neighbor_iterator_pair VVGraph<GRAPH_ARGS>::neighbors(const vertex_t& v) const
{
  const_neighbor_iterator_pair result;
  if(v.isNull())
    return result;
  const NeighborhoodPair* it_found = this->findVertex(v);
  if(not it_found)
    return result;
  const edge_list_t& lst = it_found->single_neighborhood.edges;
  result.first = const_neighbor_iterator(lst.begin());
  result.second = const_neighbor_iterator(lst.end());
  return result;
}
template <GRAPH_TEMPLATE>
typename VVGraph<GRAPH_ARGS>::neighbor_iterator_pair VVGraph<GRAPH_ARGS>::neighbors(const vertex_t& v)
{
  neighbor_iterator_pair result;
  if(v.isNull())
    return result;
  NeighborhoodPair* it_found = this->findVertex(v);
  if(not it_found)
    return result;
  edge_list_t& lst = it_found->single_neighborhood.edges;
  result.first = neighbor_iterator(lst.begin());
  result.second = neighbor_iterator(lst.end());
  return result;
}

template <GRAPH_TEMPLATE>
typename VVGraph<GRAPH_ARGS>::const_circ_neighbor_iterator_pair VVGraph<GRAPH_ARGS>::neighbors(const vertex_t& v,
                                                                                               const vertex_t& n) const
{
  const_circ_neighbor_iterator_pair result;
  if(v.isNull())
    return result;
  const_neighbor_found_t found = this->findInVertex(v, n);
  if(!found)
    return result;
  const edge_list_t& lst = found.neighborhood->edges;
  result.first = const_circ_neighbor_iterator(lst.begin(), lst.end(), found.it);
  result.second = const_circ_neighbor_iterator(lst.begin(), lst.end());
  return result;
}

template <GRAPH_TEMPLATE>
typename VVGraph<GRAPH_ARGS>::circ_neighbor_iterator_pair VVGraph<GRAPH_ARGS>::neighbors(const vertex_t& v,
                                                                                         const vertex_t& n)
{
  circ_neighbor_iterator_pair result;
  if(v.isNull())
    return result;
  neighbor_found_t found = this->findInVertex(v, n);
  if(!found)
    return result;
  edge_list_t& lst = found.neighborhood->edges;
  result.first = circ_neighbor_iterator(lst.begin(), lst.end(), found.it);
  result.second = circ_neighbor_iterator(lst.begin(), lst.end());
  return result;
}

template <GRAPH_TEMPLATE>
typename VVGraph<GRAPH_ARGS>::const_ineighbor_iterator_pair VVGraph<GRAPH_ARGS>::iNeighbors(const vertex_t& v) const
{
  const_ineighbor_iterator_pair result;
  if(v.isNull())
    return result;
  const NeighborhoodPair* it_found = this->findVertex(v);
  if(not it_found)
    return result;
  const in_edges_t& lst = it_found->single_neighborhood.in_edges;
  result.first = const_ineighbor_iterator(lst.begin());
  result.second = const_ineighbor_iterator(lst.end());
  return result;
}

template <GRAPH_TEMPLATE>
typename VVGraph<GRAPH_ARGS>::ineighbor_iterator_pair VVGraph<GRAPH_ARGS>::iNeighbors(const vertex_t& v)
{
  ineighbor_iterator_pair result;
  if(v.isNull())
    return result;
  NeighborhoodPair* it_found = this->findVertex(v);
  if(not it_found)
    return result;
  in_edges_t& lst = it_found->single_neighborhood.in_edges;
  result.first = ineighbor_iterator(lst.begin());
  result.second = ineighbor_iterator(lst.end());
  return result;
}

template <GRAPH_TEMPLATE>
template <typename VertexContainer>
VVGraph<GRAPH_ARGS> VVGraph<GRAPH_ARGS>::subgraph(const VertexContainer& verts) const
{
  typename VertexContainer::const_iterator it;
  VVGraph result;
  // First, insert the vertexes in the result graph
  for(it = verts.begin(); it != verts.end(); ++it) {
    result.insert(*it);
  }
  const NeighborhoodPair* it_orign;
  typename neighborhood_t::iterator it_n;
  typename edge_list_t::const_reverse_iterator it_sn;
  // Then, creates the in and out edges
  for(it_n = result.neighborhood.begin(); it_n != result.neighborhood.end(); ++it_n) {
    const vertex_t& v = (*it_n)->vertex;
    // For each vertex, find out which edges to add
    it_orign = this->findVertex(v);
    const edge_list_t& orig_lst = it_orign->single_neighborhood.edges;
    edge_list_t& lst = (*it_n)->single_neighborhood.edges;
    // Going backward because it is easier to retrieve the iterator ...
    for(it_sn = orig_lst.rbegin(); it_sn != orig_lst.rend(); ++it_sn) {
      // For each existing edge from the vertex, keep only the ones whose
      // target are on the subgraph
      if(result.contains(it_sn->target)) {
        // Insert the incoming edge
        ineighbor_iterator it_in = result.insertInEdge(v, it_sn->target).first;
        // Insert the outgoing edge
        lst.push_front(neighbor_t(it_sn->target, *it_sn));
      }
    }
    // At last, update the flag, if any
    if(it_orign->single_neighborhood.flagged and result.contains(*it_orign->single_neighborhood.flagged)) {
      const_neighbor_found_t found = this->findInVertex(v, *it_orign->single_neighborhood.flagged);
      (*it_n)->single_neighborhood.flagged = &(found.it->target);
    }
  }
  return result;
}

template <GRAPH_TEMPLATE>
VVGraph<GRAPH_ARGS>::VVGraph(const VVGraph& copy)
  : modified_vertices(false)
{
  *this = copy;
}

template <GRAPH_TEMPLATE> bool VVGraph<GRAPH_ARGS>::operator==(const VVGraph& other) const
{
  if(neighborhood.size() != other.neighborhood.size())
    return false;
  for(typename neighborhood_t::const_iterator it = neighborhood.begin(); it != neighborhood.end(); ++it) {
    const vertex_t& v1 = (*it)->vertex;
    typename lookup_t::const_iterator it_found = other.lookup.find(v1);
    if(it_found == other.lookup.end())
      return false;
    if((*it)->single_neighborhood.edges.empty()) {
      if(!it_found->second->single_neighborhood.edges.empty())
        return false;
    } else {
      const edge_list_t& lst = (*it)->single_neighborhood.edges;
      const edge_list_t& olst = this->neighborhood[it_found->second]->single_neighborhood.edges;
      if(lst.size() != olst.size())
        return false;
      const vertex_t& v2 = lst.begin()->target;
      bool found = false;
      for(typename edge_list_t::const_iterator it_olst = olst.begin(); it_olst != olst.end(); ++it_olst) {
        if(it_olst->target == v2) {
          found = true;
          for(typename edge_list_t::const_iterator it_lst = lst.begin(); it_lst != lst.end(); ++it_lst) {
            if(it_lst->target != it_olst->target)
              return false;
            ++it_olst;
            if(it_olst == olst.end())
              it_olst = olst.begin();
          }
          break;
        }
      }
      if(!found)
        return false;
    }
  }
  return true;
}

template <GRAPH_TEMPLATE> VVGraph<GRAPH_ARGS>& VVGraph<GRAPH_ARGS>::reverse()
{
  typename neighborhood_t::iterator it;
  for(it = neighborhood.begin(); it != neighborhood.end(); ++it) {
    std::reverse((*it)->single_neighborhood.edges.begin(), (*it)->single_neighborhood.edges.end());
  }
  return *this;
}

template <GRAPH_TEMPLATE> VVGraph<GRAPH_ARGS>& VVGraph<GRAPH_ARGS>::operator=(const VVGraph& other)
{
  typename neighborhood_t::const_iterator it_o;
  typename neighborhood_t::iterator it;
  for(it = neighborhood.begin(); it != neighborhood.end(); ++it) {
    NeighborhoodPair::Delete(*it);
    *it = 0;
  }
  neighborhood.clear();
  lookup.clear();
  it = neighborhood.end();
  for(it_o = other.neighborhood.begin(); it_o != other.neighborhood.end(); ++it_o) {
    NeighborhoodPair* np = (*it_o)->copy();
    lookup[(*it_o)->vertex] = neighborhood.size();
    neighborhood.push_back(np);
  }
  return *this;

  // Copy the structure
  /*
   * neighborhood = other.neighborhood;
   * lookup.clear();
   * typename neighborhood_t::iterator it;
   * for(it = neighborhood.begin() ; it != neighborhood.end() ; ++it)
   *  lookup[it->vertex] = it;
   * return *this;
   */
}

template <GRAPH_TEMPLATE> void VVGraph<GRAPH_ARGS>::swap(VVGraph& other)
{
  neighborhood.swap(other.neighborhood);
  lookup.swap(other.lookup);
}

template <typename Graph> void create_graph_methods(Graph& G)
{
  if(G.size()) {
    if(G.valence(G.any()) > G.size()) {
      // Call all fcts
      typename Graph::vertex_t v = G.any();
      G.erase(v);
      G.insert(v);
      G[10];
      G.reference(v);
      G.empty();
      G.anyIn(v);
      G.valence(v);
      G.empty(v);
      G.iAnyIn(v);
      G.iValence(v);
      G.iEmpty(v);
      G.nextTo(v, v);
      G.prevTo(v, v);
      G.edge(v, v);
      G.flag(v, v);
      G.flagged(v);
      G.neighbors(v);
      G.iNeighbors(v);
      G.source(typename Graph::edge_t());
      G.target(typename Graph::edge_t());
      G.eraseEdge(v, v);
      G.replace(v, v, v);
      G.spliceAfter(v, v, v);
      G.spliceBefore(v, v, v);
      G.insertEdge(v, v);
      G.eraseAllEdges(v);
      G.begin();
      G.end();
      G.find(v);
      G.count(v);
      G.findIn(v, v);
      {
        const Graph& cG = G;
        cG[10];
        cG.reference(v);
        cG.empty();
        cG.anyIn(v);
        cG.valence(v);
        cG.empty(v);
        cG.iAnyIn(v);
        cG.iValence(v);
        cG.iEmpty(v);
        cG.nextTo(v, v);
        cG.prevTo(v, v);
        cG.edge(v, v);
        cG.flagged(v);
        cG.neighbors(v);
        cG.iNeighbors(v);
        cG.source(typename Graph::edge_t());
        cG.target(typename Graph::edge_t());
        cG.begin();
        cG.end();
        cG.find(v);
        cG.count(v);
        cG.findIn(v, v);
      }
    }
  }
}
} // namespace graph
} // namespace lgx

#endif
