uniform sampler1D surfcolormap;
uniform sampler1D labelcolormap;
uniform sampler1D heatcolormap;
uniform bool heatmap;
uniform bool labels;
uniform bool blending;
uniform float opacity;
uniform float brightness;
uniform uint nb_colors;
uniform vec2 signalBounds;
uniform vec2 heatBounds;

/*in vec3 normal;*/
/*in vec3 lightDir[4], halfVector[4];*/

smooth centroid in vec3 texPos;

int fmod(int value, int div)
{
  int rat = value / div;
  int mul = rat * div;
  return value - mul;
}

// Triangles are colored based on mesh signal, label, or heatmap color
void setColor()
{
  // Texture coords are (label, signal, heat)
  float label = texPos.x;
  float heat = texPos.z;
  float signal = (texPos.y - signalBounds.x) / (signalBounds.y - signalBounds.x);
  if(signal > 1.) signal = 1.;
  if(signal < 0.) signal = 0.;

  bool has_heatmap = heatmap;
  if(label < 0)
    label = -label;
  else
    has_heatmap = false;

  vec4 color;

  float opac = opacity + signal - opacity*signal;

  if(labels && label > 0.0) {
    // Draw Label Color
    float value = floor(label+0.5);
    uint col_idx = uint(value) % nb_colors;
    vec4 col = texelFetch(labelcolormap, int(col_idx), 0);
    color.rgb = brightness * col.rgb * opac;
    color.a = col.a;
  } else if(has_heatmap) {
    // Draw heatmap color
    heat = (heat - heatBounds.x) / (heatBounds.y - heatBounds.x);
    vec4 col = texture(heatcolormap, heat);
    color.rgb = brightness * col.rgb * opac;
    color.a = col.a;
  } else {
    // Draw signal
    vec4 col = texture(surfcolormap, signal);
    color.rgb = brightness * col.rgb;
    color.a = col.a;
  }

  gl_FragColor = light(color);
  if(blending)
    gl_FragColor.a *= opacity;
  else
    gl_FragColor.a = 1.0;
}

