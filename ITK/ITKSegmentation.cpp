/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "ITKProgress.hpp"

#include "ITKSegmentation.hpp"
#include <Utility.hpp>

#include <itkMorphologicalWatershedFromMarkersImageFilter.h>
#include <itkMorphologicalWatershedImageFilter.h>
#include <itkBinaryThresholdImageFilter.h>
#include <itkMaskImageFilter.h>
#include <itkMaximumImageFilter.h>

namespace lgx {
namespace process {

namespace {

template <size_t dim>
bool itkWatershed(const Store* image, Store* labels, bool connect8, bool markLine, Process* process)
{
  auto converterImage = UImageConverter<dim>::New();
  auto converterLabel = UImageConverter<dim>::New();

  converterImage->SetStore(image);
  converterLabel->SetStore(labels);

  typedef itk::MorphologicalWatershedFromMarkersImageFilter<UImageType<dim>, UImageType<dim>> FilterType;

  auto filter = FilterType::New();

  filter->SetFullyConnected(connect8);
  filter->SetMarkWatershedLine(markLine);
  filter->SetInput1(converterImage->GetOutput());
  filter->SetInput2(converterLabel->GetOutput());

  ITKProgress progress("Seeded Watershed");
  progress.setFilter(filter);

  filter->Update();
  auto result = filter->GetOutput();

  if(!UImageConverter<dim>::TransferImage(labels->stack(), labels, result)) {
    process->setErrorMessage("ITK Process tried to change the size of the stack");
    return false;
  }

  labels->changed();

  return true;
}

template <size_t dim>
bool itkWatershedAutoSeeded(Stack* stack, const Store* image, Store* labels, ushort level, bool connect8,
                            bool markLine, Process* process)
{
  bool hasPreLabeled = labels->labels() and labels->isVisible();

  auto converterImage = UImageConverter<dim>::New();

  typedef itk::MorphologicalWatershedImageFilter<UImageType<dim>, UImageType<dim>> FilterType;
  std::unique_ptr<Store> masked;

  auto filter = FilterType::New();

  filter->SetLevel(level);
  filter->SetFullyConnected(connect8);
  filter->SetMarkWatershedLine(markLine);

  if(hasPreLabeled) {
    // set the values in the initial image to 65535
    masked = std::make_unique<Store>(*image);
    HVecUS& masked_data = masked->data();
    const HVecUS& label_data = labels->data();

#pragma omp parallel for
    for(size_t i = 0 ; i < masked_data.size() ; ++i)
      if(label_data[i] > 0)
        masked_data[i] = 0xffff;

    converterImage->SetStore(masked.get());
  } else
    converterImage->SetStore(image);

  filter->SetInput(converterImage->GetOutput());
  ITKProgress progress("Auto-Seeded Watershed");
  progress.setFilter(filter);

  filter->Update();
  auto result = filter->GetOutput();
  if(filter->GetAbortGenerateData())
    return false;

  labels->setLabels(true);

  //process->setCurrentStackId(stack->id());
  labels->show();

  if(hasPreLabeled) {
    // Store result in masked data
    if(!UImageConverter<dim>::TransferImage(masked->stack(), masked.get(), result)) {
      process->setErrorMessage("ITK Process tried to change the size of the stack");
      return false;
    }
    HVecUS& labels_data = labels->data();
    const HVecUS& masked_data = masked->data();

    // Find labels that can be used
    std::vector<int> convert_labels(1<<16, 0);
    // Set to -1 any value already present in the stack
#pragma omp parallel for
    for(size_t i = 0 ; i < labels_data.size() ; ++i)
      convert_labels[labels_data[i]] = -1;

    // 0 doesn't change
    convert_labels[0] = 0;

    // Set to -2 indices to find
#pragma omp parallel for
    for(size_t i = 0 ; i < masked_data.size() ; ++i) {
      if(convert_labels[masked_data[i]] < 0)
        convert_labels[masked_data[i]] = -2;
      else
        convert_labels[masked_data[i]] = masked_data[i];
    }

    size_t cur_index = 0;
    for(size_t i = 0 ; i < (1<<16) ; ++i) {
      if(convert_labels[i] == -2) {
        do {
          ++cur_index;
          if(cur_index > 65535)
            return process->setErrorMessage("Too many labels, cannot fit on 16 bits.");
        } while(convert_labels[cur_index] != 0);
        convert_labels[i] = cur_index;
      }
    }

#pragma omp parallel for
    for(size_t i = 0 ; i < labels_data.size() ; ++i) {
      if(labels_data[i] == 0 and masked_data[i] > 0) {
        labels_data[i] = convert_labels[masked_data[i]];
      }
    }
  } else {
    if(!UImageConverter<dim>::TransferImage(labels->stack(), labels, result)) {
      process->setErrorMessage("ITK Process tried to change the size of the stack");
      return false;
    }
    if(!process->runProcess("Stack", "Relabel", {"1", "1"}))
      return false;
  }

  if(not hasPreLabeled)
    labels->copyMetaData(image);
  labels->setLabels(true);
  labels->changed();

  return true;
}

} // namespace

bool ITKWatershed::operator()(const Store* image, Store* labels, bool connect8, bool markLine)
{
  const Stack* stack = image->stack();
  if(stack->is2D())
    return itkWatershed<2>(image, labels, connect8, markLine, this);
  return itkWatershed<3>(image, labels, connect8, markLine, this);
}

REGISTER_STACK_PROCESS(ITKWatershed);

bool ITKWatershedAutoSeeded::operator()(Stack* stack, const Store* image, Store* labels, ushort level, bool connect8,
                                        bool markLine)
{
  if(stack->is2D())
    return itkWatershedAutoSeeded<2>(stack, image, labels, level, connect8, markLine, this);
  return itkWatershedAutoSeeded<3>(stack, image, labels, level, connect8, markLine, this);
}

REGISTER_STACK_PROCESS(ITKWatershedAutoSeeded);
}
}
