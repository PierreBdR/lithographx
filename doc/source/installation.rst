************
Installation
************

Hardware Requirements and Recommendations
=========================================

Volumetric rendering and 3D image processing are very demanding tasks both for
the CPU and the GPU. For this reason, it is highly recommended to opt for
high-end desktop computer. Also, both because LithoGraphX uses CUDA and because
their video drivers tend to be more stable we highly recommend using NVidia
GPUs.

Recommended configuration
-------------------------

As of July 2016, here are recommended configurations which should offer a good
price/performance ratio:

Desktop Computer
^^^^^^^^^^^^^^^^

* CPU: Intel Core i7 6700K with 32 GB of RAM
* GPU: NVidia GeForce GTX 970 with 4GB of dedicated memory

Laptop Computer
^^^^^^^^^^^^^^^

* CPU: Intel Core i7-4720HQ with 16 GB of RAM
* GPU: NVidia GeForce GTK 960M with 2GB of dedicated memory

Minimum configuration
---------------------

Desktop Computer
^^^^^^^^^^^^^^^^
* CPU: Intel Core i7 with 8GB to 16GB of RAM (depending on the size of your images)
* GPU: any NVidia or AMD GPU less than 5 years old.

Laptop Computer
^^^^^^^^^^^^^^^
* CPU: Intel Core i7 with 8GB to 16GB of RAM (depending on the size of your images)
* GPU: high-end NVidia GPU of Intel HD Graphics 4600 or more recent with the
  latest video drivers (tested with drivers Build 10.18.15.4248). We do not
  recommend AMD GPUs due to serious issues with their video drivers for laptops.

Install from binaries
=====================

|windows_logo| Microsoft Windows
--------------------------------

.. |windows_logo| image:: images/windows-logo.png
    :width: 24pt

Quick Setup
^^^^^^^^^^^

#. Install LithoGraphX for Windows: http://updates.lithographx.com (the default
   version requires an NVidia graphics cards, otherwise download the NOCUDA version)
#. Install Anaconda Python 2.7 64 bits: https://www.continuum.io/downloads

Full Setup
^^^^^^^^^^

If you don't want to install Anaconda or would rather know the full list of
Python modules used in LithoGraphX, please read on.

First, to run LithoGraphX, you will also need to install the 64 bits
version of Python 2.7 for Windows. There are many sources from which you can
download Python for windows. We recommend using Anaconda
(https://www.continuum.io/downloads), as it will include by default a large
number of libraries for scientific computing and machine learning. However, you
can also download the official python binaries from there:
`Python 2.7.11 64 bits <https://www.python.org/ftp/python/2.7.11/python-2.7.11.amd64.msi>`_.

If you install the official python binaries, you may need to install Numpy,
SciPy and scikit-learn (all of which are provided by Anaconda) as they
are used by some macros. First, install Numpy and Scipy by downloading the
corresponding wheel files from this website:
http://www.lfd.uci.edu/~gohlke/pythonlibs/. Once the files downloaded, you can
use ``pip`` to install them. For example, at the time this is written, you will
download the files ``numpy-1.10.4+mkl-cp27-cp27m-win_amd64.whl`` for numpy,
``scipy-0.17.0-cp27-none-win_amd64.whl`` for scipy and
``pandas-0.17.1-cp27-none-win_amd64.whl`` for pandas. To install them, open
a command prompt and type::

    pip install numpy-1.10.4+mkl-cp27-cp27m-win_amd64.whl scipy-0.17.0-cp27-none-win_amd64.whl pandas-0.17.1-cp27-none-win_amd64.whl

``pip`` can also be used to download and install scikit-learn::

    pip install scikit-learn

You will find all binary installers in the LithoGraphX update site:
http://updates.lithographx.com. LithoGraphX for windows is currently compiled in
two version:

* a version requiring CUDA, this is the default version.
* a version that doesn't use CUDA, named ``-NOCUDA``.

Download the version you want (probably the latest) and execute it to install
LithoGraphX.

.. Note::

    Since the version 1.1.1, LithoGraphX for Windows will check if a new version exist, and will offer to update
    your installation. Automatic checking for a new version can be tweaked/deactivated from the settings dialog box.

.. Hint::

    If you have issues, check the :ref:`FAQ on installation <FAQ-installation>`.

|ubuntu_logo| Linux - Ubuntu
----------------------------

.. |ubuntu_logo| image:: images/ubuntu-logo32.png
    :width: 24pt

The simplest way to install LithoGraphX and to keep it up to date is to use the
`launchpad PPA
<https://code.launchpad.net/~pierre-barbierdereuille/+archive/ubuntu/ppa>`_.
To use it you have two options:

#. From the Ubuntu Software Center, select the menu Edit->Software Sources. In
   the ``Other software`` tab, click the ``Add...`` button and enter
   ``ppa:pierre-barbierdereuille/ppa`` in the ``APT line``. For more details
   look at the answer of this question:

   http://askubuntu.com/questions/4983/what-are-ppas-and-how-do-i-use-them

   The PPA offers 4 packages. The two main ones are ``lithographx`` and
   ``lithographx-nocuda``. As the name suggests, ``lithographx-nocuda`` simply
   doesn't use CUDA. These two packages contain the latest tested version of
   LithoGraphX. The two other packages are ``lithographx-daily`` and
   ``lithographx-nocuda-daily``. As their name suggests, these two packages are
   compiled daily with whatever the latest version of the source code is. As
   such, they are not recommended for non-developers.

#. You can use the command line and type from a terminal::

    $ sudo add-apt-repository ppa:pierre-barbierdereuille/ppa
    $ sudo apt-get update
    $ sudo apt-get install lithographx

.. Hint::

    If you have issues, check the :ref:`FAQ on installation <FAQ-installation>`.

.. Important::

    After installing LithoGraphX on Linux, you need to :ref:`configure the Alt
    key <Alt-key-settings>`.

|linux_logo| Linux - other
--------------------------

.. |linux_logo| image:: images/Tux.*
    :width: 24pt

If you don't use an Ubuntu-derived distribution but still a debian-derived
one, you can obtain the source package and build it yourself:

#. Enable source downloading for the PPA::

   $ sudo add-apt-repository -s ppa:pierre-barbierdereuille/ppa

#. Create a folder where you are going to build the package. From this folder,
   type::

    $ apt-get source -b lithographx

   If you have problems building, or want to adjust the dependencies for your
   system, you can also first get the source code::

    $ apt-get source lithographx

   Make the necessary adjustments to the package, and build it. For the version
   1.1.0, you can build the package with::

    $ cd lithographx-1.1.0
    $ dpkg-buildpackage -us -uc -nc

   The debian package will be created in the folder containing
   ``lithographx-1.1.0``.

For non-debian based systems, you will need to install from source. If you need
help doing so, do not hesitate to `contact us <http://www.lithographx.com/contact-credits>`_.

.. Important::

    After installing LithoGraphX on Linux, you need to :ref:`configure the Alt
    key <Alt-key-settings>`.

Install from source
===================

First, you need to retrieve the source code, which is hosted by BitBucket:
https://bitbucket.org/PierreBdR/lithographx

Then, you need to follow the instructions for your operating system.

Requirements
------------

To compile LithoGraphX you will need the following programs installed (more
recent versions should work too):

- g++ 4.8: https://gcc.gnu.org/
- cmake 2.8.11: http://www.cmake.org
- Qt 5.2 or greater with OpenGL module: http://www.qt.io
- CImg: http://cimg.sourceforge.net/
- GNU Scientific Library 1.16: http://www.gnu.org/software/gsl/gsl.html
- LibTIFF v4 from http://libtiff.maptools.org/
- python 2.7 or 3.4+: http://www.python.org
- sip 4.15: http://www.riverbankcomputing.com/software/sip/download
- PyQt 5.2: http://www.riverbankcomputing.com/software/pyqt/download5
- cuda: https://developer.nvidia.com/cuda-zone

To run some of the macros, you will need:

- NumPy 1.8: http://www.numpy.org
- SciPy 0.13: http://www.scipy.org
- Pandas 0.17: http://pandas.pydata.org
- scikit-learn 0.16: http://scikit-learn.org

To generate the documentation, you will need:

- doxygen: http://www.stack.nl/~dimitri/doxygen/
- sphinx: http://sphinx-doc.org/

|linux_logo| Installation on Linux
----------------------------------

If using Ubuntu, or probably any debian-derived system, you will find a list
of packages to install in the file called `ubuntu-packages.txt
<https://bitbucket.org/PierreBdR/lithographx/src/master/ubuntu-packages.txt>`_
at the root of the source tree.

In the list of dependencies, the packages ``nvidia-cuda-toolkit`` may be
replaced with any package from NVidia (e.g. ``cuda-7-0``), or simply by
``libthrust-dev`` if you want to compile without CUDA.

For other distribution, you will need to find equivalent packages.

Configuration
^^^^^^^^^^^^^
Create a folder in which the application will be built. Then, using
``cmake-gui``, select the source and build directories. Make sure
``CMAKE_BUILD_TYPE`` is set to ``Release``, and select the modules you want to
build by checking the variables starting with ``BUILD_``. When configuring, more
options may appear (in red), in this case, just configure again, until you have
no error and no red line. When that is done, click the ``Generate`` button.

Compilation
^^^^^^^^^^^

Open a shell in the build folder and simply use ``make``. To speed up
compilation, you can choose to use parallel compilation with the ``-j`` options.
For examplem to use 8 compilation processes in parallel, type: ``make -j8``.

|windows_logo| Installation on Windows
--------------------------------------

Configuring your development environment for Windows is a complex task. We are
using gcc for the compilation (to be able to use the GNU Scientific Library),
but at the same time, python and CUDA require the use of Visual Studio (at least
by default). For that reason, windows has its own page:

.. toctree::
    :maxdepth: 2

    windows_conf


Configuring the Operating System
================================

.. _Alt-key-settings:

Configuring the `Alt` key under Linux
-------------------------------------

Most window manager are configured to use the ``Alt`` key in combination with the
mouse to move or resize windows. On the other hand, LithoGraphX uses the ``Alt``
key to interact with the mesh and the stack. So to use LithoGraphX, the window
manager needs to be configured to use another key, such as the ``Super`` or ``Meta``
key (i.e. the window key on your keyboard). Here is how to do it on some common
window manager:

|gnome3_img| Gnome 3
    TODO

.. |gnome3_img| image:: images/gnome3.png
    :width: 48pt

|cinnamon_img| Cinnamon
    In the system settings, find the ``Windows`` settings. There, change the mouse
    modifier key from ``Alt`` to ``Super``.

.. |cinnamon_img| image:: images/cinnamon.png
    :width: 48pt

|kde_img| KDE
    In the system settings, open the ``Window behaviour`` settings. Then, on the
    ``Window Actions`` tab, change the modifier key to ``Meta``.

.. |kde_img| image:: images/kde.png
    :width: 48pt

|XFCE_img| XFCE 4
    In the Settings, select ``Window Manager Tweaks``. From there, in the
    ``Accessibility`` tab, change the ``Key used to grab and move windows`` from
    ``Alt`` to ``Super``.

.. |XFCE_img| image:: images/Xfce.png
    :width: 48pt

