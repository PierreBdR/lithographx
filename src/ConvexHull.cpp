/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "ConvexHull.hpp"

#include <qhull/LGXQHull.hpp>
#include <Information.hpp>

namespace lgx {
namespace qhull {

std::vector<Point3i> convexHull(std::vector<Point3f> pts, QString* errorString)
{
  size_t size = pts.size();

  Information::out << "Creating convex hull of " << pts.size() << " points" << endl;

  // Compute the Delaunay graph
  char qhull_command[] = "qhull Qt"; // ensure the output is triangulated
  float* qh_pts = reinterpret_cast<float*>(pts.data());
  int exit = qh_new_qhull(3, size, qh_pts, false, qhull_command, NULL, stderr);
  if(exit != 0)
  {
    if(errorString)
      *errorString = "Error running qhull. Check terminal for more details.";
    return {};
  }

  Information::out << "Computation done ... extracting results" << endl;

  std::vector<Point3i> result;

  facetT *facet;
  FORALLfacets {
    auto nb_vertices = qh_setsize(facet->vertices);
    if(nb_vertices != 3) {
      if(errorString)
        *errorString = QString("Error, a polygon with %1 vertices found in triangulation").arg(nb_vertices);
      return {};
    }
    vertexT *vertex;
    int vertex_n, vertex_i;
    Point3i triangle;
    FOREACHvertex_i_(facet->vertices) {
      int id = qh_pointid(vertex->point);
      if(id < 0) {
        if(errorString)
          *errorString = "Error, found null or interior point in triangulation";
        return {};
      }
      triangle[vertex_i] = id;
    }
    result.push_back(triangle);
  }

  Information::out << "Now freeing memory" << endl;

  qh_freeqhull(qh_ALL);
  int curlong, totlong;
  qh_memfreeshort(&curlong, &totlong);
  if(curlong or totlong) {
    Information::err << QString("QHull internal warning: did not free %1 bytes of long memory (%2 pieces").arg(totlong).arg(curlong) << endl;
  }

  Information::out << "... convex hull finished" << endl;

  return result;

}

} // namespace qhull
} // namespace lgx
