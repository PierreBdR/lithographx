# Finder for thrust library
# thrust is template only, so only includes are searched
#
# THRUST_INCLUDE_DIR Toplevel include dir containing thrust, includes have to be defined like <thrust/header.h>

include(CMakeDependentOption)

if (THRUST_INCLUDE_DIR)
  # Already in cache, be silent
  set(THRUST_FIND_QUIETLY TRUE)
endif (THRUST_INCLUDE_DIR)

set(THRUST_BACKEND "CUDA" CACHE STRING "Choose the Thrust backend. Can be one of CUDA, OpenMP or CPU.")
set_property(CACHE THRUST_BACKEND PROPERTY STRINGS "CUDA" "OpenMP" "TBB" "CPU")

macro(UPDATE_THRUST_BACKEND)
  if(THRUST_BACKEND STREQUAL "CUDA")
    set(THRUST_BACKEND_CUDA ON CACHE INTERNAL "")
  else()
    set(THRUST_BACKEND_CUDA OFF CACHE INTERNAL "")
  endif()
  if(THRUST_BACKEND STREQUAL "OpenMP")
    set(THRUST_BACKEND_OMP ON CACHE INTERNAL "")
  else()
    set(THRUST_BACKEND_OMP OFF CACHE INTERNAL "")
  endif()
  if(THRUST_BACKEND STREQUAL "TBB")
    set(THRUST_BACKEND_TBB ON CACHE INTERNAL "")
  else()
    set(THRUST_BACKEND_TBB OFF CACHE INTERNAL "")
  endif()
  if(THRUST_BACKEND STREQUAL "CPU")
    set(THRUST_BACKEND_CPP ON CACHE INTERNAL "")
  else()
    set(THRUST_BACKEND_CPP OFF CACHE INTERNAL "")
  endif()
endmacro(UPDATE_THRUST_BACKEND)

update_thrust_backend()

if(NOT (THRUST_BACKEND_CPP OR THRUST_BACKEND_OMP OR THRUST_BACKEND_CUDA OR THRUST_BACKEND_TBB))
  message(SEND_ERROR "THRUST_BACKEND must be one of 'CUDA', 'OpenMP', 'TBB' or 'CPU'")
endif()

if(THRUST_BACKEND_CUDA)
  find_package(CUDA)
  if(NOT CUDA_FOUND)
    find_package(OpenMP)
    if(OPENMP_FOUND)
      set(THRUST_BACKEND_OMP TRUE)
    else()
      set(THRUST_BACKEND_CPP TRUE)
    endif()
  endif()
endif()

# Set the backend for thrust
if(THRUST_BACKEND_TBB)
  list(APPEND CUDA_OPTIONS -DTHRUST_DEVICE_SYSTEM=THRUST_DEVICE_SYSTEM_TBB)
elseif(THRUST_BACKEND_OMP)
  list(APPEND CUDA_OPTIONS -Xcompiler ${OpenMP_CXX_FLAGS} -DTHRUST_DEVICE_SYSTEM=THRUST_DEVICE_SYSTEM_OMP)
elseif(THRUST_BACKEND_CPP)
  list(APPEND CUDA_OPTIONS -DTHRUST_DEVICE_SYSTEM=THRUST_DEVICE_SYSTEM_CPP)
endif()

find_path(THRUST_INCLUDE_DIR
  HINTS /usr/include /usr/local/include
  $ENV{CUDA_INC_PATH}
  ${CUDA_TOOLKIT_INCLUDE}
  ${CUDA_TOOLKIT_ROOT_DIR}
  NAMES thrust/version.h
  DOC "Lib Thrust Include dir"
  )

# ensure that we only get one
if( THRUST_INCLUDE_DIR )
  list( REMOVE_DUPLICATES THRUST_INCLUDE_DIR )
endif( THRUST_INCLUDE_DIR )

# Check for required components
if ( THRUST_INCLUDE_DIR )

  # Parse version file to get the thrust version numbers

  file( STRINGS ${THRUST_INCLUDE_DIR}/thrust/version.h
    version
    REGEX "#define[ \t]+THRUST_VERSION[ \t]+([0-9x]+)"
    )

  string( REGEX REPLACE "#define[ \t]+THRUST_VERSION[ \t]+" "" version ${version} )
  string( REGEX MATCH "^[0-9]" major ${version} )
  string( REGEX REPLACE "^${major}00" "" version ${version} )
  string( REGEX MATCH "^[0-9]" minor ${version} )
  string( REGEX REPLACE "^${minor}0" "" version ${version} )
  set( THRUST_VERSION "${major}.${minor}.${version}")

  set( THRUST_FOUND TRUE )

  include( FindPackageHandleStandardArgs )
  find_package_handle_standard_args( Thrust
    REQUIRED_VARS THRUST_INCLUDE_DIR
    VERSION_VAR THRUST_VERSION
    )
endif()

