/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef PACKAGEARCHIVE_HPP
#define PACKAGEARCHIVE_HPP

#include <LGXConfig.hpp>

#include <PackageManager.hpp>

#include <QString>
#include <QStringList>
#include <QByteArray>

#include <functional>
#include <exception>

namespace lgx {

struct PackagingUi
{
  /**
   * Function call to indicate progress to the user.
   * This function can return false to indicate the user requested ending the process.
   */
  std::function<bool(void)> _progress;
  /**
   * Function called with a report on the output of called programs
   */
  std::function<void(const QByteArray&)> _output;
  /**
   * Indicate the current step to the user.
   */
  std::function<void(const QString&)> _step;
  /**
   * Error. If the second argument is true, the program should pause after
   * displaying the message.
   */
  std::function<void(const QString&, bool)> _error;
  /**
   * Indicate the last step was validated
   */
  std::function<void(void)> _ok;
  /**
   * Ask a yes/no question and returns true/false depending on the answer
   */
  std::function<bool(const QString&)> _yesNo;

  bool progress() const {
    if(_progress) return _progress();
    return true;
  }

  void output(const QByteArray& ba) const {
    if(_output) _output(ba);
  }

  void step(const QString& msg) const {
    if(_step) _step(msg);
  }

  void error(const QString& msg, bool paused = false) const {
    if(_error) _error(msg, paused);
  }

  void ok() const {
    if(_ok) _ok();
  }

  bool yesNo(const QString& msg) const {
    if(_yesNo) return _yesNo(msg);
    return false;
  }
};


class PackageArchive
{
public:
  PackageArchive();
  PackageArchive(QString filename, const PackagingUi& ui);
  PackageArchive(const PackageArchive&) = default;
  PackageArchive(PackageArchive&&) = default;

  PackageArchive& operator=(const PackageArchive&) = default;
  PackageArchive& operator=(PackageArchive&&) = default;

  bool isValid() const
  { return _isValid; }

  explicit operator bool() const
  { return isValid(); }

  bool isSource() const
  { return _isValid and _isSource; }

  bool isBinary() const
  { return _isValid and not _isSource; }

  const QString& archivePath() const
  { return _archivePath; }

  const QString& root() const
  { return _root; }

  /**
   * Returns whether the archive is compressed or a folder
   */
  bool isCompressed() const
  { return _isCompressed; }

  const QString& errorMessage() const
  { return _errorMessage; }

  const QStringList& fileList() const
  { return _fileList; }

  /**
   * Uncompress the archive in a folder.
   *
   * \param folder Folder to uncompress the archive into.
   * \param ui Object defining interactions with the user.
   *
   * \returns The package on the uncompress folder
   */
  PackageArchive uncompress(QString folder, const PackagingUi& ui);

  /**
   * Compress the archive into a file
   *
   * \param filePath Path to the compressed archive
   * \param ui Object defining interactions with the user
   *
   * \returns The packate on the compressed folder
   *
   * \note If the package is already compressed, it's simply copied
   */
  PackageArchive compress(QString filePath, const PackagingUi& ui);

  /**
   * Get details about the package
   */
  const Package& package() const { return _pkg; }

  //@{
  ///\name Methods for source packages

  /**
   * Configure the package in a given folder
   *
   * \param confPath Path in which the configuration is done
   * \param ui Object definining interactions with the user
   *
   * The max wait is 30 sec.
   */
  bool configure(QString confPath,
                 const PackagingUi& ui);

  /**
   * Build the package
   *
   * \param ui Object defining interactions with the user
   *
   * There is no maximum waiting time.
   */
  bool build(const PackagingUi& ui);

  /**
   * Return the full path to the compiled archive
   */
  QString compiledArchive() const;

  /**
   * Return the prefix for the compilation
   */
  QString compiledPrefix() const;

  //@}
  //@{
  //\name Methods for binary packages
  bool install(bool user);
  //@}

private:
  QString _archivePath;
  QStringList _fileList;
  bool _isValid;
  bool _isSource;
  bool _isCompressed;
  QString _root;
  QString _confPath;
  QString _errorMessage;
  Package _pkg;
};

/// Function to compile a package
QString compilePackage(QString packagePath, bool force, const PackagingUi& ui);
/// Compile package, specifying the output
QString compilePackage(QString packagePath, QString outputPath, bool isFolder, bool force, const PackagingUi& ui);

/// Function to compile a package
QString createPackage(QString packageName, const PackagingUi& ui);
/// Compile package, specifying the output
QString createPackage(QString packageName, QString outputPath, bool isFolder, const PackagingUi& ui);

/// Install a package as user or system from the CLI
bool installPackage(QString packagePath, bool user, bool force, const PackagingUi& ui);

// Uninstall a package
bool uninstallPackage(QString packageName, const PackagingUi& ui);

bool find7Zip();

// List installed packages
QStringList listPackages(bool user=false, bool system=false);

// List all the files contained in an installed package
QStringList listFiles(QString packageName, bool *ok = nullptr);

// List the libraries contained in an installed package
QStringList listLibraries(QString packageName, bool *ok = nullptr);

Package findPackage(const QString& filename);

#ifdef WIN32
QString findLGXBuilder();
bool setBuilderEnvironment(const QString& path);
#endif

} // namespace lgx

#endif // PACKAGEARCHIVE_HPP

