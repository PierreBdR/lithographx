/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

/*
 * This file is part of LithoGraphX
 *
 * LithoGraphX is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LithoGraphX is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Curvature.hpp"

namespace lgx {
namespace util {
CurvatureType readCurvatureType(QString type, bool* ok)
{
  type = type.toLower().trimmed();
  if(ok)
    *ok = true;
  if(type == "gaussian")
    return CT_GAUSSIAN;
  if(type == "sum square")
    return CT_SUMSQUARE;
  if(type == "minimal")
    return CT_MIN;
  if(type == "maximal")
    return CT_MAX;
  if(type == "average")
    return CT_AVERAGE;
  if(type == "signed average absolute")
    return CT_SIGNED_AVERAGE;
  if(type == "anisotropy")
    return CT_ANISOTROPY;
  if(type == "root sum square")
    return CT_ROOT_SUMSQUARE;
  if(type == "none")
    return CT_NONE;
  *ok = false;
  return CT_GAUSSIAN;
}

QString toString(CurvatureType type)
{
  switch(type) {
  case CT_NONE:
    return "None";
  case CT_GAUSSIAN:
    return "Gaussian";
  case CT_MAX:
    return "Maximal";
  case CT_MIN:
    return "Minimal";
  case CT_SUMSQUARE:
    return "Sum Square";
  case CT_AVERAGE:
    return "Average";
  case CT_SIGNED_AVERAGE:
    return "Signed Average Absolute";
  case CT_ROOT_SUMSQUARE:
    return "Root Sum Square";
  case CT_ANISOTROPY:
    return "Anisotropy";
  }
  return "";
}

CurvatureMeasure* curvatureMeasure(CurvatureType type)
{
  switch(type) {
  case CT_NONE:
    return 0;
  case CT_GAUSSIAN:
    return new GaussianCurvature();
  case CT_AVERAGE:
    return new AverageCurvature();
  case CT_MIN:
    return new MinimalCurvature();
  case CT_MAX:
    return new MaximalCurvature();
  case CT_SUMSQUARE:
    return new SumSquareCurvature();
  case CT_ROOT_SUMSQUARE:
    return new RootSumSquareCurvature();
  case CT_SIGNED_AVERAGE:
    return new SignedAverageAbsCurvature();
  case CT_ANISOTROPY:
    return new AnisotropyCurvature();
  }
  return 0;
}
} // namespace util
} // namespace lgx
