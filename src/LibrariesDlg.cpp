/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/
#include "LibrariesDlg.hpp"

#include "Information.hpp"

#include <QShowEvent>
#include <QTextBrowser>
#include <QFontMetrics>

class LibrariesModel : public QAbstractTableModel
{
public:
  LibrariesModel(const std::vector<lgx::LibraryDescription>& libraries, QObject* parent = nullptr)
    : QAbstractTableModel(parent)
    , _libraries(libraries)
  {
    // Resize pixmaps
    for(auto& l: _libraries) {
      if(not l.logo.isEmpty()) {
        auto logo = QPixmap(l.logo);
        logo = logo.scaledToHeight(100, Qt::SmoothTransformation);
        _logos.push_back(logo);
      } else
        _logos.emplace_back();
    }
    _webSize.resize(_libraries.size());
  }

  int columnCount(const QModelIndex& parent = QModelIndex()) const override { return 5; }
  int rowCount(const QModelIndex & parent = QModelIndex()) const override {
    if(parent.isValid()) return 0;
    return _libraries.size();
  }

  QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override
  {
    if(orientation == Qt::Vertical)
      return QVariant(); // No vertical data header
    switch(role) {
      case Qt::DisplayRole:
      case Qt::EditRole:
      case Qt::ToolTipRole:
      case Qt::StatusTipRole:
      case Qt::WhatsThisRole:
        switch(section) {
          case 0:
            return "Logo";
          case 1:
            return "Name";
          case 2:
            return "Version";
          case 3:
            return "Website";
          case 4:
            return "Description";
          default:
            return "Unknown";
        }
        break;
      case Qt::TextAlignmentRole:
        return Qt::AlignCenter;
      case Qt::SizeHintRole:
      case Qt::DecorationRole:
      default:
        break;
    }
    return QVariant();
  }

  QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override
  {
    auto id = index.row();
    if(id < 0 or id >= _libraries.size())
      return QVariant();
    const auto& l = _libraries[id];
    switch(role) {
      case Qt::DisplayRole:
        switch(index.column()) {
          case 0:
            return "";
          case 1:
            return l.name;
          case 2:
            return l.version;
          case 3:
            return QString("<a href=\"%1\">%1</a>").arg(l.website);
          case 4:
            return l.description;
          default:
            break;
        }
        break;
      case Qt::DecorationRole:
        if(index.column() == 0)
          return _logos[id];
        break;
      case Qt::SizeHintRole:
        switch(index.column()) {
          case 0:
            return _logos[id].size();
          case 3:
            return _webSize[id];
          default:
            break;
        }
        break;
      case Qt::TextAlignmentRole:
        if(index.column() < 3)
          return Qt::AlignCenter;
        return Qt::AlignLeft;
      case Qt::ToolTipRole:
        return headerData(index.column(), Qt::Horizontal, Qt::DisplayRole);
      case Qt::WhatsThisRole:
      case Qt::StatusTipRole:
      case Qt::EditRole:
      default:
        break;
    }
    return QVariant();
  }

  void setWebSize(int row, const QSize& s)
  {
    if(row < 0 or row >= _webSize.size())
      _webSize[row] = s;
  }

protected:
  std::vector<lgx::LibraryDescription> _libraries;
  std::vector<QSize> _webSize;
  std::vector<QPixmap> _logos;
};

LibrariesDlg::LibrariesDlg(const std::vector<lgx::LibraryDescription>& libraries, QWidget* parent)
  : QDialog(parent)
{
  ui.setupUi(this);
  auto* model = new LibrariesModel(libraries, ui.librariesView);
  ui.librariesView->setModel(model);
  ui.librariesView->setStyleSheet("QTreeView::item { border-bottom: 1px solid black;}");

  // Create views for descriptions and websites
  auto nb_rows = model->rowCount();
  auto nb_columns = model->columnCount();

  for(int i = 0 ; i < nb_rows ; ++i) {
    auto* tb_web = new QTextBrowser;
    auto* tb_desc = new QTextBrowser;
    auto idx = model->index(i, 3);
    tb_web->setReadOnly(true);
    tb_web->setOpenExternalLinks(true);
    tb_web->setHtml(model->data(idx, Qt::DisplayRole).toString());
    tb_web->setAlignment(Qt::AlignCenter);
    tb_web->setFrameShadow(QFrame::Plain);
    tb_web->setFrameShape(QFrame::NoFrame);
    ui.librariesView->setIndexWidget(idx, tb_web);

    idx = model->index(i, 4);
    tb_desc->setReadOnly(true);
    tb_desc->setOpenExternalLinks(true);
    tb_desc->setHtml(model->data(idx, Qt::DisplayRole).toString());
    tb_desc->setFrameShadow(QFrame::Plain);
    tb_desc->setFrameShape(QFrame::NoFrame);
    ui.librariesView->setIndexWidget(idx, tb_desc);
  }

  for(int i = 0 ; i < nb_columns ; ++i)
    ui.librariesView->resizeColumnToContents(i);
}

void LibrariesDlg::showEvent(QShowEvent* event)
{
  lgx::Information::out << "showEvent" << endl;
  auto* model = dynamic_cast<LibrariesModel*>(ui.librariesView->model());
  if(not model)
    return;
  auto nb_rows = model->columnCount();
  for(int i = 0 ; i < nb_rows ; ++i) {
    auto idx = model->index(i, 3);
    auto* tb = dynamic_cast<QTextBrowser*>(ui.librariesView->indexWidget(idx));
    if(tb) {
      auto h = ui.librariesView->sizeHintForRow(i);
      auto w = (tb->document()->idealWidth() +
                tb->contentsMargins().left() +
                tb->contentsMargins().right());
      model->setWebSize(i, QSize(w, h));
      tb->setMinimumHeight(h);
    }
  }
  ui.librariesView->setColumnWidth(4, 1); // Minimize the width
  ui.librariesView->resizeColumnToContents(3);
}

