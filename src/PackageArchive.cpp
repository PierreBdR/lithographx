/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "PackageArchive.hpp"

#include <Information.hpp>
#include <SystemDirs.hpp>
#include <PackageManager.hpp>
#include <Dir.hpp>
#include <LGXVersion.hpp>
#include <Library.hpp>
#include <Defer.hpp>

#ifdef LithoGraphX_PROGRAM
#  include <PluginManager.hpp>
#endif

#include <QCoreApplication>
#include <QDir>
#include <QFileInfo>
#include <QProcess>
#include <QStandardPaths>
#include <QTemporaryDir>
#include <QTextStream>
#include <QSettings>

namespace lgx {

namespace {

QString path7Zip;

// Get the file list from a compressed archive
QStringList extractFileList(const QString& filename, QString& fullOutput, const PackagingUi& ui)
{
  if(not find7Zip()) {
    ui.error("Cannot find 7-Zip application");
    return {};
  }
  QProcess proc;
  proc.setProgram(path7Zip);
  proc.setArguments({"l", "-slt", filename});
  proc.setProcessChannelMode(QProcess::MergedChannels);
  proc.setReadChannel(QProcess::StandardOutput);
  proc.start(QIODevice::ReadOnly);

  QStringList result;
  fullOutput.clear();

  bool failed = false;

  while((proc.state() != QProcess::NotRunning) and not proc.waitForFinished(100)) {
    if(not ui.progress()) {
      proc.kill();
      return {};
    }
  }
  if(proc.exitCode() != 0) {
    ui.error(QString("7-zip exited with code: %1.\nOutput:\n%2\n").arg(proc.exitCode())
      .arg(QString::fromLocal8Bit(proc.readAll())));
    return {};
  }

  {
    bool listStarted = false;
    QString path;
    QTextStream ts(&proc);
    while(not ts.atEnd()) {
      auto line = ts.readLine();
      fullOutput += "\n" + line;
      if(not failed) {
        if(line.startsWith("Error")) {
          failed = true;
          ui.error(line);
        } else if(listStarted) {
          if(line.startsWith("Path ="))
            path = QDir::fromNativeSeparators(line.mid(6).trimmed());
          else if(line.startsWith("Attributes =")) {
            auto isFolder = line.split("=").at(1).trimmed();
            if(not isFolder.startsWith("D"))
              result << path;
          }
        } else if(line.startsWith("----------"))
          listStarted = true;
      }
    }
    if(failed)
      result.clear();
  }

  return result;
}

/// Get the list of files in a folder
QStringList getFileList(const QString& path, QString& errorMessage)
{
  QDir dir(path);
  if(not dir.exists()) {
    errorMessage = QString("No folder called '%1'").arg(path);
    return {};
  }
  QStringList result;
  for(const auto& fi: dir.entryInfoList(QDir::AllEntries | QDir::NoDotAndDotDot)) {
    if(fi.isDir())
      result += getFileList(fi.absoluteFilePath(), errorMessage);
    else
      result << QDir::fromNativeSeparators(fi.absoluteFilePath());
  }
  errorMessage.clear();
  return result;
}


bool runWithWaiting(QString program, const QStringList& args, const QString& workDir,
                    QString& errorMessage,
                    const PackagingUi& ui)
{
  if(QDir::isRelativePath(program)) {
    QString pathProgram = QStandardPaths::findExecutable(program);
    if(pathProgram.isEmpty()) {
      errorMessage = QString("Cannot find '%1' executable in PATH.").arg(program);
      return false;
    }
    program = pathProgram;
  }

  QProcess proc;
  proc.setProgram(program);
  proc.setArguments(args);
  proc.setWorkingDirectory(workDir);
  proc.setProcessChannelMode(QProcess::MergedChannels);
  proc.setReadChannel(QProcess::StandardOutput);
  proc.start(QIODevice::ReadOnly);

  QList<QByteArray> fullOutput;
  QByteArray curOutput;
  while((proc.state() != QProcess::NotRunning) and not proc.waitForFinished(100)) {
    curOutput = proc.readAllStandardOutput();
    fullOutput << curOutput;
    ui.output(curOutput);
    if(not ui.progress())
      return false;
  }
  curOutput = proc.readAllStandardOutput();
  fullOutput << curOutput;
  ui.output(curOutput);
  if(not ui.progress())
    return false;

  auto ecode = proc.exitCode();
  if(ecode != 0) {
#if QT_VERSION >= QT_VERSION_CHECK(5, 4, 0)
    errorMessage = QString("Error %1 running CMake:\n%2").arg(ecode)
      .arg(QString::fromLocal8Bit(fullOutput.join()));
#else
    {
      QString out;
      for(const auto& o: fullOutput)
        out += QString::fromLocal8Bit(o);
      errorMessage = QString("Error %1 running CMake:\n%2").arg(ecode).arg(out);
    }
#endif
    return false;
  }

  return true;
}


#ifndef WIN32
QString pathPatchelf;

bool findPatchelf()
{
  auto pth = QCoreApplication::applicationDirPath();
  pathPatchelf = QStandardPaths::findExecutable("lgx_patchelf", {pth});
  if(not pathPatchelf.isEmpty())
    return true;
  pathPatchelf = QStandardPaths::findExecutable("lgx_patchelf");
  return not pathPatchelf.isEmpty();
}

bool patchRPath(const QString& libPath, const QString& newPath, QString& errorMessage, const PackagingUi& ui)
{
  QProcess proc;
  proc.setProgram(pathPatchelf);
  proc.setArguments({"--print-rpath", libPath});
  proc.setProcessChannelMode(QProcess::MergedChannels);
  proc.setReadChannel(QProcess::StandardOutput);
  proc.start(QIODevice::ReadOnly);
  while((proc.state() != QProcess::NotRunning) and not proc.waitForFinished(100)) {
    if(not ui.progress()) {
      proc.kill();
      return {};
    }
  }
  if(proc.exitCode() != 0) {
    errorMessage = QString("Failed to print rpath:\n%1").arg(QString::fromLocal8Bit(proc.readAllStandardOutput()));
    return false;
  }
  auto content = proc.readAllStandardOutput().trimmed();
  if(not content.isEmpty())
    content += ":";
  content += newPath.toUtf8();
  proc.setArguments({"--set-rpath", content, libPath});
  proc.start();
  while((proc.state() != QProcess::NotRunning) and not proc.waitForFinished(100)) {
    if(not ui.progress()) {
      proc.kill();
      return {};
    }
  }
  if(proc.exitCode() != 0) {
    errorMessage = QString("Failed to set rpath:\n%1").arg(QString::fromLocal8Bit(proc.readAllStandardOutput()));
    return false;
  }
  return true;
}
#endif

} // namespace

#ifdef WIN32
QString findLGXBuilder()
{
  {
    QSettings settings;
    settings.beginGroup("Package");
    auto path = settings.value("Builder", QString()).toString();
    if(not path.isEmpty() and QFile(path).exists())
      return path;
    settings.endGroup();
  }
  // Check a few standard locations
  QString basename = "LithoGraphXBuilder/envs.bat";
  auto testLocations = QStringList{ "C:/", "D:/" };
  auto testVars = QStringList{ "ProgramFiles", "ProgramFiles(x86)", "ProgramW6432"};
  auto env = QProcessEnvironment::systemEnvironment();
  for(const auto& var: testVars) {
    if(env.contains(var))
      testLocations += env.value(var).split(";");
  }
  testLocations.removeDuplicates();

  for(const auto& loc: testLocations) {
    if(QFile(loc + basename).exists())
      return loc + basename;
  }
  return {};
}

bool setBuilderEnvironment(const QString& path)
{
  auto fi = QFileInfo(path);
  if(fi.isFile() and fi.isReadable()) {
    QSettings settings;
    settings.beginGroup("Package");
    settings.setValue("Builder", path);
    settings.endGroup();
    return true;
  }
  Information::err << "Path '" << path << "' is either not a file or not readable." << endl;
  return false;
}
#endif

bool find7Zip()
{
  if(path7Zip.isEmpty()) {
    auto pth = QCoreApplication::applicationDirPath();
    path7Zip = QStandardPaths::findExecutable("7z", {pth});
    if(not path7Zip.isEmpty())
      return true;
    path7Zip = QStandardPaths::findExecutable("7z");
  }
  return not path7Zip.isEmpty();
}

PackageArchive::PackageArchive()
  : _isValid(false)
{ }

PackageArchive::PackageArchive(QString filename, const PackagingUi& ui)
  : _archivePath(filename)
{
  _isValid = false;

  if(not find7Zip()) {
    _errorMessage = "Cannot find 7-Zip application";
    return;
  }

  QFileInfo fi(filename);
  if(not fi.exists()) {
    _errorMessage = QString("No file or folder called '%1'").arg(filename);
    return;
  }

  _isCompressed = not fi.isDir();

  if(_isCompressed)
    _fileList = extractFileList(_archivePath, _errorMessage, ui);
  else
    _fileList = getFileList(_archivePath, _errorMessage);
  if(_fileList.empty())
    return;
  _errorMessage.clear();

  // Determine root of the archive
  QStringList root = _fileList.front().split('/');
  root.pop_back();
  for(int i = 1 ; i < _fileList.size() ; ++i) {
    QStringList dirs = _fileList.at(i).split('/');
    dirs.pop_back();
    int nb_comp = std::min(root.size(), dirs.size());
    QStringList new_root;
    for(int j = 0 ; j < nb_comp ; ++j) {
      if(root[j] != dirs[j]) {
        break;
      } else
        new_root << root[j];
    }
    root = std::move(new_root);
    if(root.isEmpty())
      break;
  }

  _root = root.join('/');

  // Now determine which kind of package it is:
  //   - source packages have a "CMakeLists.txt" at the root
  //   - binary packages have a "description.ini" at the root and no CMakeLists.txt in the same folder

#ifdef WIN32
  auto caseSensitivity = Qt::CaseSensitive;
#else
  auto caseSensitivity = Qt::CaseInsensitive;
#endif

  if(_fileList.contains(QString("%1/description.ini").arg(_root), caseSensitivity)) {
    if(_fileList.contains(QString("%1/CMakeLists.txt").arg(_root), caseSensitivity))
      _isSource = true;
    else
      _isSource = false;
  } else {
    _errorMessage = QString("The archive doesn't contain a 'description.ini' at its root (i.e. '%1')").arg(_root);
    return;
  }

  // Read the description.ini
  {
    QString iniFilePath;
    QTemporaryDir tmpDir;
    if(_isCompressed) {
      QProcess proc;
      proc.setProgram(path7Zip);
      proc.setArguments({"e", "-y", "-o" + tmpDir.path(), _archivePath, QDir(_root).filePath("description.ini")});
      proc.setProcessChannelMode(QProcess::MergedChannels);
      proc.setReadChannel(QProcess::StandardOutput);
      proc.start(QIODevice::ReadOnly);

      while((proc.state() != QProcess::NotRunning) and not proc.waitForFinished(100)) {
        if(not ui.progress()) {
          proc.kill();
          return;
        }
      }

      if(proc.exitCode() != 0) {
        _errorMessage = QString("7-zip exited with code: %1.\nOutput:\n%2\n").arg(proc.exitCode())
          .arg(QString::fromLocal8Bit(proc.readAll()));
        return;
      }

      iniFilePath = QDir(tmpDir.path()).filePath("description.ini");

    } else {
      auto realRoot = QDir(_archivePath).filePath(_root);
      iniFilePath = QDir(realRoot).filePath("description.ini");
    }

    _pkg = Package::fromDescription(iniFilePath);
  }

  _isValid = true;
}

PackageArchive PackageArchive::uncompress(QString folder, const PackagingUi& ui)
{
  if(not find7Zip()) {
    ui.error("Cannot find 7-Zip application");
    return {};
  }

  QProcess proc;
  proc.setProgram(path7Zip);
  proc.setArguments({"x", "-y", _archivePath});
  proc.setWorkingDirectory(folder);
  proc.setProcessChannelMode(QProcess::MergedChannels);
  proc.setReadChannel(QProcess::StandardOutput);
  proc.start(QIODevice::ReadOnly);
  while((proc.state() != QProcess::NotRunning) and not proc.waitForFinished(100)) {
    if(not ui.progress()) {
      proc.kill();
      return {};
    }
  }
  auto ecode = proc.exitCode();
  if(ecode != 0) {
    _errorMessage = QString("Error %1 running 7-zip:\n%2").arg(ecode)
      .arg(QString::fromLocal8Bit(proc.readAllStandardOutput()));
    return PackageArchive();
  }
  return PackageArchive(folder, ui);
}

PackageArchive PackageArchive::compress(QString filePath, const PackagingUi& ui)
{
  if(isCompressed()) {
    if(not QFile(_archivePath).copy(filePath)) {
      ui.error(QString("Error copying file '%1' to '%2'").arg(_archivePath).arg(filePath));
      return {};
    }
  }

  if(not find7Zip()) {
    ui.error("Cannot find 7-Zip application");
    return {};
  }

  QProcess proc;
  proc.setProgram(path7Zip);
  proc.setArguments({"a", "-y", filePath, _archivePath});
  proc.setProcessChannelMode(QProcess::MergedChannels);
  proc.setReadChannel(QProcess::StandardOutput);
  proc.start(QIODevice::ReadOnly);
  while((proc.state() != QProcess::NotRunning) and not proc.waitForFinished(100)) {
    if(not ui.progress()) {
      proc.kill();
      return {};
    }
  }
  auto ecode = proc.exitCode();
  if(ecode != 0) {
    _errorMessage = QString("Error %1 running 7-zip:\n%2").arg(ecode)
      .arg(QString::fromLocal8Bit(proc.readAllStandardOutput()));
    return PackageArchive();
  }
  return PackageArchive(filePath, ui);
}

bool PackageArchive::configure(QString path, const PackagingUi& ui)
{
  if(not _isSource) {
    _errorMessage = "This is not a source package.";
    return false;
  }
#if (defined WIN32) || (defined WIN64)
#  if (defined __MINGW32__) || (defined __MINGW64__)
  QString generator = "MinGW Makefiles";
#  elif (defined _MSC_VER)
  QString generator = "NMake Makefiles";
#  else
#    error "Unknown environment ... check with developer"
#  endif
#else
  QString generator = "Unix Makefiles";
#endif
  if(runWithWaiting("cmake", {"-G", generator, _root}, path,
                    _errorMessage, ui)) {
    _confPath = path;
    return true;
  }
  return false;
}

bool PackageArchive::build(const PackagingUi& ui)
{
  if(not _isSource) {
    _errorMessage = "This is not a source package.";
    return false;
  }
  if(_confPath.isEmpty()) {
    _errorMessage = "Package archive not configured.";
    return false;
  }
  auto prefix = compiledPrefix();
  bool ok = runWithWaiting("cpack", {"-G", "ZIP", "-D", "CPACK_PACKAGE_FILE_NAME="+prefix}, _confPath,
                           _errorMessage, ui);
  if(ok) {
    QDir dir(_confPath);
    QFile output(dir.filePath(prefix+".zip"));
    if(not output.exists()) {
      ui.error(QString("Output file expected to be '%1' but couldn't be found.").arg(output.fileName()));
      return false;
    }
    output.rename(dir.filePath(prefix + ".lgxpb"));
  }
  return ok;
}

QString PackageArchive::compiledPrefix() const
{
  return QString("%1-%2-%3").arg(_pkg.name()).arg(_pkg.version().toString()).arg(LithoGraphX_OS);
}

QString PackageArchive::compiledArchive() const
{
  if(not _isSource)
    return {};
  if(_confPath.isEmpty())
    return {};
  QDir dir(_confPath);
  auto filename = dir.filePath(compiledPrefix() + ".lgxpb");
  if(not QFile(filename).exists())
    return {};
  return filename;
}

QString compilePackage(QString packagePath, QString outputPath, bool isFolder, bool force, const PackagingUi& ui)
{
  if(isFolder) {
    if(not QDir::setCurrent(outputPath)) {
      ui.error(QString("Cannot go to folder '%1'.").arg(outputPath));
      return {};
    }
    return compilePackage(packagePath, force, ui);
  }
  auto prevDir = QDir::currentPath();
  QTemporaryDir destDir;
  if(not destDir.isValid()) {
    ui.error(QString("Couldn't create temporary folder '%1' to create the package.").arg(destDir.path()));
    return {};
  }
  if(not QDir::setCurrent(destDir.path())) {
    ui.error(QString("Couldn't set the current directory to '%1' to create the package.").arg(destDir.path()));
    return {};
  }
  auto result = compilePackage(packagePath, force, ui);
  ui.step(QString("Moving file to '%1'").arg(outputPath));
  if(result.isEmpty())
    return {};
  QFile f(result);
  if(not outputPath.endsWith(".lgxpb"))
    outputPath += ".lgxpb";
  QFile output(outputPath);
  if(output.exists()) {
    if(not output.remove()) {
      ui.error(QString("Error, cannot erase output file '%1'").arg(outputPath));
      return {};
    }
  }
  if(not f.rename(outputPath)) {
    ui.error(QString("Error moving file '%1' to '%2'.").arg(result, outputPath));
    return {};
  }
  ui.ok();
  QDir::setCurrent(prevDir);
  return outputPath;
}

QString compilePackage(QString packagePath, bool force, const PackagingUi& ui)
{
  if(not find7Zip()) {
    Information::err << "Cannot find 7-Zip application" << endl;
    return {};
  }
  ui.step("Analysing package");
  PackageArchive pkgArch(packagePath, ui);
  if(not pkgArch.isValid()) {
    ui.error(QString("The file '%1' is not a valid archive:\n%2").arg(packagePath).arg(pkgArch.errorMessage()));
    return {};
  }

  //Information::out << "This is a " << (pkgArch.isSource() ? "source" : "binary") << " package" << endl;
  if(not pkgArch.isSource()) {
    ui.error("This is not a source package.");
    return {};
  }

  ui.ok();

  if(not force) {
    ui.step("Checking source dependencies");

    auto pkg = pkgArch.package();
    if(not pkg) {
      ui.error(QString("Error, invalid description.ini in source package '%1'").arg(packagePath));
      return {};
    }
    pkg.setUserPackage(true);

    PackageManager manager;
    if(not pkg.canInstall(manager, false)) {
      ui.error(QString("Error, cannot compile package because of dependencies.\nPackage dependencies:\n%1\n")
               .arg(pkg.dependencies()->toString()));
      return {};
    }

    ui.ok();
  }

  QTemporaryDir sourceDir, buildDir;
  QString buildDirPath;
  if(pkgArch.isCompressed()) {
    ui.step("Uncompressing package");
    if(not sourceDir.isValid()) {
      ui.error(QString("Couldn't create temporary folder '%1' to uncompress the package.").arg(sourceDir.path()));
      return {};
    }
    auto uncompressed = pkgArch.uncompress(sourceDir.path(), ui);
    if(not uncompressed) {
      if(not pkgArch.errorMessage().isEmpty())
        ui.error(pkgArch.errorMessage());
      return {};
    }
    pkgArch = std::move(uncompressed);
    ui.ok();
  }

  ui.step("Configuring package");
  if(not buildDir.isValid()) {
    ui.error(QString("Couldn't create temporary folder '%1' to compile the package.").arg(buildDir.path()));
  }
  if(not pkgArch.configure(buildDir.path(), ui)) {
    auto err = QString("%1\n - Source folder = '%2'\n - Build folder = '%3'")
      .arg(pkgArch.errorMessage()).arg(pkgArch.archivePath()).arg(buildDir.path());
    ui.error(err, true);
    return {};
  }

  ui.ok();

  ui.step("Building");

  if(not pkgArch.build(ui)) {
    auto err = QString("%1\n - Source folder = '%2'\n - Build folder = '%3'")
      .arg(pkgArch.errorMessage()).arg(pkgArch.archivePath()).arg(buildDir.path());
    ui.error(err, true);
    return {};
  }

  auto archiveName = QFileInfo(pkgArch.compiledArchive()).canonicalFilePath();
  if(archiveName.isEmpty()) {
    auto err = QString("Cannot find compiled archive name in build folder.\n"
                       " - Source folder = '%1'\n - Build folder = '%2'").arg(pkgArch.archivePath()).arg(buildDir.path());
    ui.error(err, true);
    return {};
  }

  auto filename = QFileInfo(archiveName).fileName();
  QFile destFile(filename);
  if(destFile.exists())
    destFile.remove();
  if(not QFile(archiveName).rename(filename)) {
    ui.error(QString("Error cannot move archive from '%1' to '%2'")
             .arg(archiveName).arg(QDir(".").absoluteFilePath(filename)),
             true);
    return {};
  }

  ui.ok();

  return filename;
}

bool installPackage(QString packagePath, bool user, bool force, const PackagingUi& ui)
{
  if(not find7Zip()) {
    ui.error("Cannot find 7-Zip application");
    return false;
  }

  QDir processDir = (user ? util::userProcessesDir(true) : util::systemProcessesDir());
  QDir macroDir = (user ? util::userMacroDir(true) : util::systemMacroDir());
  QDir packageDir = (user ? util::userPackagesDir(true) : util::systemPackagesDir());
  QDir libsDir = (user ? util::userLibsDir(true) : util::libsDir());

#ifndef WIN32
  if(not findPatchelf()) {
    ui.error("Cannot find path to 'lgx_patchelf' program.");
    return false;
  }
  QString rpath = (user ? QString("%1:%2").arg(util::userLibsDir().path()).arg(util::libsDir().path())
                        : util::libsDir().path());
#endif

  ui.step("Analysing archive");

  PackageArchive pkgArch(packagePath, ui);
  if(not pkgArch.isValid()) {
    ui.error(QString("The file '%1' is not a valid archive:\n%2").arg(packagePath).arg(pkgArch.errorMessage()));
    return false;
  }

  QTemporaryDir binPackDir, binaryDir, sourceDir;
  auto curDir = QDir::currentPath();
  QStringList copiedFiles;

  auto cleanInstall = util::defer([&curDir, &copiedFiles, &ui]() {
                                    QDir::setCurrent(curDir);
                                    if(not copiedFiles.isEmpty()) {
                                      ui.step("Removing already copied files");
                                      for(const auto& f: copiedFiles)
                                        QFile(f).remove();
                                      ui.ok();
                                    }
                                  });

  QString sourceFilePath;

  if(not pkgArch.isBinary()) {
    ui.ok();

    if(not binPackDir.isValid()) {
      ui.error(QString("Couldn't create temporary folder '%1' to compile the package.").arg(binPackDir.path()));
    }

    sourceFilePath = QDir(sourceDir.path()).absoluteFilePath("package.lgxps");
    auto newPkg = pkgArch.compress(sourceFilePath, ui);

    QDir::setCurrent(binPackDir.path());
    QString binPkg = compilePackage(packagePath, force, ui);
    if(binPkg.isEmpty())
      return false;
    pkgArch = PackageArchive(util::absoluteFilePath(binPkg), ui);

    ui.step("Resuming archive analyses");
  }

  if(pkgArch.isCompressed()) {
    ui.ok();
    ui.step("Uncompressing package");
    if(not binaryDir.isValid()) {
      ui.error(QString("Couldn't create temporary folder '%1' to uncompress the binary package.").arg(binaryDir.path()));
      return false;
    }
    auto uncompressed = pkgArch.uncompress(binaryDir.path(), ui);
    if(not uncompressed) {
      ui.error(pkgArch.errorMessage());
      return false;
    }
    pkgArch = std::move(uncompressed);
    ui.ok();
    ui.step("Resuming archive analyses");
  }

  QString root = pkgArch.root();
  auto size_root = root.size();
  if(not root.endsWith("/")) ++size_root;

  auto pkg = pkgArch.package();
  if(not pkg) {
    ui.error("Invalid description file.");
    return false;
  }
  pkg.setUserPackage(user);

  if(pkg.OS() != LithoGraphX_OS) {
    ui.error(QString("This package has been compiled for %1 but you are running %2").arg(pkg.OS()).arg(LithoGraphX_OS));
    return false;
  }

  ui.ok();

  PackageManager manager;

  if(not force) {
    ui.step("Checking binary dependencies");

    bool ok;
    QStringList broken;
    std::tie(ok, broken) = manager.canInstall(pkg);
    if(not ok) {
      if(broken.isEmpty())
        ui.error(QString("Error, dependencies are not satisfied for installing the package:%1").arg(pkg.dependencies()->toString()));
      else
        ui.error(QString("Installing the package would break the following packages: %1").arg(broken.join(", ")));
      return false;
    }

    ui.ok();
  }

  if(manager.hasPackage(pkg.name())) {
    auto otherPkg = manager.package(pkg.name());
    if(otherPkg->version() >= pkg.version()) {
      if(not ui.yesNo(QString("Trying to install package %1 version %2 while version %3 of the same package "
                              "is already installed.\n"
                              "Uninstall installed version?")
                      .arg(pkg.name()).arg(pkg.version().toString()).arg(otherPkg->version().toString()))) {
        ui.error("Cannot install package, another version is already installed.");
        return false;
      }
    }
    ui.step("About to uninstall existing package");
    ui.ok();
    if(not uninstallPackage(pkg.name(), ui))
      return false;
    ui.step("Existing package uninstalled");
    ui.ok();
    manager.loadPackages();
  }

  ui.step("Checking installed files");

  QString descriptionFile;
  QStringList filesToInstall;
  QStringList filesDestination;
  auto size_process = QString("processes/").size();
  auto size_macros = QString("macros/").size();
  auto size_lib = QString("lib/").size();
  for(const auto& filename : pkgArch.fileList()) {
    QString destFile = filename.mid(size_root);
    if(destFile == "description.ini") {
      descriptionFile = filename;
      destFile.clear();
    } else if(destFile.startsWith("processes/")) {
      destFile = processDir.filePath(destFile.mid(size_process));
    } else if(destFile.startsWith("macros/")) {
      destFile = macroDir.filePath(destFile.mid(size_macros));
    } else if(destFile.startsWith("lib/")) {
      destFile = libsDir.filePath(destFile.mid(size_lib));
    } else {
      ui.error(QString("Error, file %1 is in an incorrect location.\n"
                       "All files must be in 'processes', 'macros' or 'lib'").arg(filename));
      return false;
    }
    if(not destFile.isEmpty()) {
      if(QFile(destFile).exists()) {
        ui.error(QString("File '%1' already exists.\n"
                         "You need to uninstall the package that contains it before continuing further.").arg(destFile));
        return false;
      }
      filesToInstall << filename;
      filesDestination << destFile;
    }
  }

  // If created, copy source package
  if(not sourceFilePath.isEmpty()) {
    auto destFilePath = packageDir.absoluteFilePath(QString("%1.lgxps").arg(pkg.name()));
    QFile file(destFilePath);
    if(file.exists()) {
      if(not file.remove()) {
        ui.error(QString("Error, cannot erase previous source package '%1'").arg(destFilePath));
        return false;
      }
    }
    filesToInstall << sourceFilePath;
    filesDestination << destFilePath;
    sourceFilePath = destFilePath;
  }

  if(not ui.progress())
    return false;

  ui.ok();


  ui.step("Creating manifest");
  QString manifest = packageDir.filePath(QString("%1_manifest.txt").arg(pkg.name()));
  {
    QFile file(manifest);
    if(not file.open(QIODevice::WriteOnly)) {
      Information::err << "ERROR:\nCannot open file '" << manifest << "' for writing." << endl;
      return false;
    }
    QTextStream ts(&file);
    for(const auto& fn : filesDestination)
      ts << fn << endl;

    if(not ui.progress())
      return false;
  }

  ui.ok();


  ui.step("Copying files");
  for(int i = 0 ; i < filesToInstall.size() ; ++i) {

    if(not ui.progress())
      return false;

    auto from = filesToInstall[i];
    auto to = filesDestination[i];
    QFileInfo fi(to);
    QDir pth = fi.dir();
    if(not pth.exists()) {
      QFileInfo fii(pth.path());
      QString to_create = fii.baseName();
      pth = fii.dir();
      while(not pth.exists()) {
        fii = QFileInfo(pth.path());
        to_create = fii.baseName() + "/" + to_create;
        pth = fii.dir();
      }
      if(not pth.mkpath(to_create)) {
        ui.error(QString("Cannot create folder '%1'.").arg(pth.path()));
        return false;
      }
    }
    if(not QFile(from).copy(to)) {
      ui.error(QString("Cannot copy file '%1' to '%2'.").arg(from).arg(to));
      return false;
    }
    copiedFiles << to;
#ifndef WIN32
    if(Library::isLibrary(to)) {
      QString errorMessage;
      if(not patchRPath(to, rpath, errorMessage, ui)) {
        if(not errorMessage.isEmpty())
          ui.error(QString("Cannot patch library '%1':\n%2").arg(to).arg(errorMessage));
        return false;
      }
    }
#endif
  }
  ui.ok();

  ui.step("Add package to list");
  {
    QSettings settings(packageDir.filePath("packageList.ini"), QSettings::IniFormat);

    switch(settings.status()) {
      case QSettings::FormatError:
        ui.error("Couldn't update packageList.ini: this is not a valid INI file.");
        return false;
      case QSettings::AccessError:
        ui.error("Couldn't update packageList.ini: trying to write a read-only file.");
        return false;
      case QSettings::NoError:
        break;
    }

    int nb_items = settings.beginReadArray("Packages");
    settings.endArray();
    settings.beginWriteArray("Packages", nb_items+1);
    settings.setArrayIndex(nb_items);
    settings.setValue("Name", pkg.name());
    if(pkg.dependencies())
      settings.setValue("Dependencies", pkg.dependencies()->toString());
    settings.setValue("Description", pkg.description());
    settings.setValue("Version", pkg.version().toString());
    if(not sourceFilePath.isEmpty())
      settings.setValue("SourcePackage", sourceFilePath);
    settings.endArray();
    switch(settings.status()) {
      case QSettings::FormatError:
        ui.error("Couldn't update packageList.ini: this is not a valid INI file.");
        return false;
      case QSettings::AccessError:
        ui.error("Couldn't update packageList.ini: trying to write a read-only file.");
        return false;
      case QSettings::NoError:
        break;
    }
  }

  ui.ok();

  copiedFiles.clear();

  return true;
}

bool uninstallPackage(QString packageName, const PackagingUi& ui)
{
  PackageManager manager;
  auto pkg = manager.package(packageName);
  if(not pkg) {
    ui.error(QString("Error, no package named '%1'.").arg(packageName));
    return false;
  }

  ui.step("Retrieving files list");

  auto files = pkg->files();
  if(files.isEmpty()) {
    ui.error("Error, cannot uninstall: the package doesn't have a manifest.\n"
             "Either the package has been installed at compiled time or you lost the manifest file.");
    return false;
  }

  if(not pkg->sourcePackage().isEmpty())
    files << pkg->sourcePackage();

  ui.ok();

#ifdef LithoGraphX_PROGRAM
  auto pluginManager = process::pluginManager();

  ui.step("Unloading extensions...");
  for(const auto& file: files) {
    if(Library::isLibrary(file)) {
      if(not pluginManager->unloadExtension(file)) {
        ui.error(QString("Cannot unload extension '%1'").arg(file));
        return false;
      }
    }
  }
  ui.ok();
#endif

  ui.step("Updating packageList.ini");
  auto dir = (pkg->isUserPackage() ? util::userPackagesDir() : util::systemPackagesDir());
  auto packageList = dir.filePath("packageList.ini");

  QByteArray plCopy;
  {
    QFile plFile(packageList);
    if(not plFile.open(QIODevice::ReadOnly)) {
      ui.error(QString("Cannot open '%1' for reading.").arg(packageList));
      return false;
    }
    plCopy = plFile.readAll();
  }
  bool copyPl = true;
  auto restorePl = util::defer([&plCopy, &copyPl, &packageList, &ui](){
                                if(copyPl)
                                {
                                  ui.step("Restoring packageList.ini");
                                  QFile plFile(packageList);
                                  if(plFile.open(QIODevice::WriteOnly)) {
                                    plFile.write(plCopy);
                                  }
                                  ui.ok();
                                }
                              });

  {
    QStringList names, descriptions, dependencies, sourcePackages;

    QSettings settings(packageList, QSettings::IniFormat);

    switch(settings.status()) {
      case QSettings::FormatError:
        ui.error("Couldn't update packageList.ini: this is not a valid INI file.");
        return false;
      case QSettings::AccessError:
        ui.error("Couldn't update packageList.ini: trying to write a read-only file.");
        return false;
      case QSettings::NoError:
        break;
    }

    int nbPkg = settings.beginReadArray("Packages");
    for(int i = 0 ; i < nbPkg ; ++i) {
      settings.setArrayIndex(i);
      auto n = settings.value("Name").toString();
      if(n != pkg->name()) {
        names << n;
        descriptions << settings.value("Description", "").toString();
        dependencies << settings.value("Dependencies", "").toString();
        sourcePackages << settings.value("SourcePackage", "").toString();
      }
    }
    settings.endArray();

    nbPkg = names.size();
    settings.beginWriteArray("Packages", nbPkg);
    for(int i = 0 ; i < nbPkg ; ++i) {
      settings.setArrayIndex(i);
      settings.setValue("Name", names[i]);
      settings.setValue("Description", descriptions[i]);
      if(not dependencies[i].isEmpty())
        settings.setValue("Dependencies", dependencies[i]);
      if(not sourcePackages[i].isEmpty())
        settings.setValue("SourcePackage", sourcePackages[i]);
    }
    settings.endArray();

    switch(settings.status()) {
      case QSettings::FormatError:
        ui.error("Couldn't update packageList.ini: this is not a valid INI file.");
        return false;
      case QSettings::AccessError:
        ui.error("Couldn't update packageList.ini: trying to write a read-only file.");
        return false;
      case QSettings::NoError:
        break;
    }

  }

  ui.ok();

  ui.step("Erasing files");

  QStringList not_removed;

  for(const auto& f : files) {
    ui.progress();
    QFile file(f);
    if(file.exists()) {
      if(not file.remove())
        not_removed << f;
    }
  }
  if(not_removed.size() == files.size()) {
    ui.error("Error, coulnd't remove any installed files.");
    return false;
  }

  auto manifest = dir.filePath(QString("%1_manifest.txt").arg(packageName));

  QFile manifestFile(manifest);
  if(not manifestFile.remove()) {
    ui.error(QString("Error, couldn't delete the manifest.\n"
                     "The package is now partially deleted, correct the access rights and restart the uninstall."));
    return false;
  }

  copyPl = false;

  if(not not_removed.isEmpty()) {
    ui.error(QString("The following files couldn't be deleted:\n%1").arg(not_removed.join("\n")));
    return false;
  }

  ui.ok();

  return true;
}

QStringList listPackages(bool user, bool system)
{
  QStringList result;
  PackageManager manager;
  if(user)
    result += manager.userPackages();
  if(system)
    result += manager.systemPackages();
  result.sort(Qt::CaseInsensitive);
  return result;
}

QStringList listFiles(QString packageName, bool* ok)
{
  PackageManager manager;
  auto pkg = manager.package(packageName);
  if(not pkg) {
    if(ok) *ok = false;
    return {};
  }
  if(ok) *ok = true;
  return pkg->files();
}

QStringList listLibraries(QString packageName, bool *ok)
{
  PackageManager manager;
  auto pkg = manager.package(packageName);
  if(not pkg) {
    if(ok) *ok = false;
    return {};
  }
  if(ok) *ok = true;
  return pkg->libraries();
}

Package findPackage(const QString& name)
{
  PackageManager manager;
  auto pkg = manager.package(name);
  if(pkg)
    return *pkg;
  return Package();
}

QString createPackage(QString packageName, const PackagingUi& ui)
{
  return createPackage(packageName, QString(), false, ui);
}

QString createPackage(QString packageName, QString output, bool output_dir, const PackagingUi& ui)
{
  if(not find7Zip()) {
    ui.error("Cannot find 7-Zip application");
    return {};
  }
  ui.step("Finding package");
  PackageManager manager;
  auto pkg = manager.package(packageName);
  if(not pkg) {
    ui.error("No such package.");
    return {};
  }

  ui.ok();

  ui.step("Copying files");

  QTemporaryDir destDir;
  if(not destDir.isValid()) {
    ui.error(QString("Couldn't create temporary folder '%1' to create the package.").arg(destDir.path()));
    return {};
  }

  {
    QString oldCurrent = QDir::currentPath();
    auto restoreDir = util::defer([&oldCurrent]() { QDir::setCurrent(oldCurrent); });
    QDir::setCurrent(destDir.path());

    // Copy all the files
    QString processesDir = (pkg->isUserPackage() ? util::userProcessesDir() : util::systemProcessesDir()).absolutePath();
    QString macrosDir = (pkg->isUserPackage() ? util::userMacroDir() : util::systemMacroDir()).absolutePath();
    QString libsDir = (pkg->isUserPackage() ? util::userLibsDir() : util::libsDir()).absolutePath();
    if(not processesDir.endsWith("/"))
      processesDir += "/";
    if(not macrosDir.endsWith("/"))
      macrosDir += "/";
    if(not libsDir.endsWith("/"))
      libsDir += "/";

    QDir dest(".");
    if(not dest.mkdir(packageName)) {
      ui.error(QString("Error, cannot create path '%1'.").arg(dest.absoluteFilePath(packageName)), true);
      return {};
    }
    dest.cd(packageName);
    auto destPackagesDir = QDir("processes");
    auto destMacrosDir = QDir("macros");
    auto destLibsDir = QDir("lib");


    for(const auto& filename: pkg->files()) {
      QFile src(filename);
      if(not (src.exists() and src.permissions() & QFileDevice::ReadUser)) {
        ui.error(QString("File '%1' doesn't exists or is not readable").arg(filename));
        return {};
      }
      QString destFile;
      if(filename.startsWith(processesDir)) {
        destFile = destPackagesDir.filePath(filename.mid(processesDir.size()));
      } else if(filename.startsWith(macrosDir)) {
        destFile = destMacrosDir.filePath(filename.mid(macrosDir.size()));
      } else if(filename.startsWith(libsDir)) {
        destFile = destLibsDir.filePath(filename.mid(libsDir.size()));
      }
      auto destPath = QFileInfo(destFile).path();
      if(not dest.mkpath(destPath)) {
        ui.error(QString("Error, cannot create path '%1'.").arg(dest.absoluteFilePath(destPath)), true);
        return {};
      }
      QString absoluteDest = dest.absoluteFilePath(destFile);
      if(not src.copy(absoluteDest)) {
        ui.error(QString("Cannot copy file '%1' to '%2'.").arg(filename).arg(absoluteDest), true);
        return {};
      }
      if(not ui.progress()) return {};
    }

    ui.ok();

    ui.step("Writing description file");

    pkg->writeDescription(dest.absoluteFilePath("description.ini"));

    if(output.isEmpty() or output_dir) {
      if(output.isEmpty()) output_dir = false;
      auto base = QDir(output_dir ? output : oldCurrent);
      output = base.filePath(QString("%1-%2-%3.lgxpb")
                             .arg(packageName)
                             .arg(pkg->version().toString())
                             .arg(pkg->OS()));
    }

    ui.ok();

    ui.step("Compressing package");

    QFile outputFile(output);
    if(outputFile.exists()) {
      if(not outputFile.remove()) {
        ui.error(QString("Cannot remove previous file '%1'.").arg(output));
        return {};
      }
    }

    // Now compress the result using 7zip
    {
      QProcess proc;
      proc.setProgram(path7Zip);
      proc.setArguments({"a", output, "-t7z", packageName});
      proc.setProcessChannelMode(QProcess::MergedChannels);
      proc.setReadChannel(QProcess::StandardOutput);
      proc.start(QIODevice::ReadOnly);

      while((proc.state() != QProcess::NotRunning) and not proc.waitForFinished(100)) {
        if(not ui.progress()) {
          proc.kill();
          return {};
        }
      }
      if(proc.exitCode() != 0) {
        ui.error(QString("7-zip exited with code: %1.\nOutput:\n%2\n").arg(proc.exitCode())
                 .arg(QString::fromLocal8Bit(proc.readAll())), true);
        return {};
      }
    }
    ui.ok();

  }

  return output;
}

} // namespace lgx
