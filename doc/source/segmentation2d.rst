**********************
Segmenting cells in 2D
**********************

.. figure:: images/segmentation-2d-cover.*
    :alt: Shoot apical meristem of A. thaliana, with microtubules
    :align: center
    :figwidth: 85%
    :width: 100%

    Shoot apical meristem of A. thaliana, with microtubules

.. _dataset: http://www.botany.unibe.ch/deve/lithographx/Microtubules.zip

In this section, we are going to see how we can extract the 2D shape of cells in
images of the shoot apical meristem of *Arabidopsis thaliana*, and see how we
can use a marker of cortical microtubule to compute their orientation based on
the method published in [Boudaoud2014]_. To start, download this `dataset`_,
provided by Dr. Agata Burian.

To segment the cells in 2D, we need to proceed in two phases:

#. Extract the surface of the meristem
#. Segment the cells on that surface

Extracting the surface of the meristem
======================================

See figure :num:`segmentation2d-fig-seg` A-D.

#. Blur the stack slightly:

    .. LGXProcess::
      :name: Gaussian Blur Stack
      :folder: Filters
      :type: Stack

      X Sigma (μm): 0.5
      Y Sigma (μm): 0.5
      Z Sigma (μm): 0.5

#. Extract the surface, using all the default arguments:

    .. LGXProcess::
      :name: Edge Detect
      :folder: Morphology
      :type: Stack

#. Erase the bumps due to dead cells by hand, using the ``Pixel Edit`` tool
#. Extract a coarse surface:

    .. LGXProcess::
      :name: Marching Cubes Surface
      :folder: Creation
      :type: Mesh

      Cube size (μm): 3

#. Select the bottom and sides with the selection tool: make sure the
   ``Mesh`` check-box is ticked and ``View`` is on ``Selected``. Then,
   orienting properly the meristem, press the ``Alt`` key and click and
   drag a rectangle to select the vertices to remove. You can extend the
   selection by pressing the ``Shift`` key while clicking. For a demonstration
   of what needs to be done, look at this video.
#. We now need to refine the surface. For this, we will alternate smoothing and
   refining the mesh. Check the terminal for the number of vertices after
   subdivision. You really should ends with around 250'000 vertices.

    .. LGXProcess::
      :name: Smooth Mesh
      :folder: Structure
      :type: Mesh

      Passes: 3

    .. LGXProcess::
      :name: Subdivide
      :folder: Structure
      :type: Mesh

.. youtube:: http://www.youtube.com/watch?v=ZbTaYITkObg
    :width: 512
    :height: 395

.. _segmentation2d-fig-seg:

.. figure:: images/segmentation-2d-segmentation.*
    :alt: 2D segmentation of the meristem
    :align: center
    :figwidth: 85%
    :width: 100%

    2D Segmentation of the meristem

Segment the cells
=================

See figure :num:`segmentation2d-fig-seg` E-F.

#. First, make sure the PI channel, which should be the Main Stack 1 is the
   active stack. In the Main tab, the ``Stack 1`` tab should be the selected
   tab, the ``Main`` store should be selected and the ``Work`` store not.
#. Then, project the signal onto the surface. You can play with the mim and max
   distances to see what happens when they change.

    .. LGXProcess::
      :name: Project Signal
      :folder: Signal
      :type: Mesh

      Min Dist (μm): 1
      Max Dist (μm): 4

#. Now, we are going to segment the cells visible in the signal.

    .. LGXProcess::
      :name: Auto-Segmentation
      :folder: Segmentation
      :type: Mesh

      Update: Yes
      Normalize: No
      Blur Cell Radius (μm): 1
      Auto-Seed Radius (μm): 2
      Border Distance (μm): 1
      Combine Threshold: 1


#. After a while, you will see outline of segmented cells. In the Main
   tab, select ``Labels`` as representation for the surface. And using the
   2D bucket, erase the cells above the areas where the staining didn't
   work.

Micro-tubule orientations
=========================

.. figure:: images/segmentation-2d-orientation.*
    :alt: Extracting the orientation of the microtubules
    :align: center
    :figwidth: 85%
    :width: 100%

    Extraction of the microtubules orientation.

#. Load the MBD stack: just drag and drop the file on the LithoGraphX window.
#. Project the new signal onto the surface. Select a range from *-1 µm* to *1
   µm*. You can try different values and see which provide the best contrast.
#. Compute the fibril orientations:

    .. LGXProcess::
      :name: Compute Fibril Orientations
      :folder: Cell Axis/Fibril Orientation
      :type: Mesh


#. You can also adjust the way the orientation is displayed:

    .. LGXProcess::
      :name: Display Fibril Orientation
      :folder: Cell Axis/Fibril Orientation
      :type: Mesh

.. [Boudaoud2014] Boudaoud, A.; Burian, A.; Borowska-Wykręt, D.; Uyttewaal, M.;
    Wrzalik, R.; Kwiatkowska, D. and Hamant, O. **FibrilTool, an ImageJ plug-in to
    quantify fibrillar structures in raw microscopy images**. *Nature Protocols*,
    2014, 9, 457-463
