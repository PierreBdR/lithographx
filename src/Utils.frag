
int imin(int a1, int a2)
{
  if(a1 < a2) return a1;
  return a2;
}

int imax(int a1, int a2)
{
  if(a1 > a2) return a1;
  return a2;
}

vec4 maxColor(vec4 c0, vec4 c1)
{
  return vec4(max(c0.r, c1.r),
              max(c0.g, c1.g),
              max(c0.b, c1.b),
              max(c0.a, c1.a));
}

float fabs(float x) {
  if(x < 0) return -x;
  return x;
}

vec3 vabs(vec3 v) {
  return vec3(fabs(v.x), fabs(v.y), fabs(v.z));
}

float fdot(vec3 v1, vec3 v2)
{
  return v1.x*v2.x + v1.y*v2.y+v1.z*v2.z;
}

int fmod(int value, int div)
{
  int rat = value / div;
  int mul = rat*div;
  return value - mul;
}

vec4 premulColor(in vec4 col)
{
  col.rgb *= col.a;
  col.a = 1-col.a;
  return col;
}

vec4 normalColor(in vec4 col)
{
  col.a = 1-col.a;
  col.rgb /= col.a;
  return col;
}

// Mix premultiplied colors
vec4 mixColors(vec4 c0, vec4 c1)
{
  vec4 res = vec4(max(c0.r, c1.r),
                  max(c0.g, c1.g),
                  max(c0.b, c1.b),
                  min(c0.a, c1.a));
  return res;
}

