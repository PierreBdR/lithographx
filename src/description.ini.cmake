[Package]
Name=${LithoGraphX_PACKAGE_NAME}
Version=${${LithoGraphX_PACKAGE_NAME}_VERSION}
Dependencies="LithoGraphX (== ${LithoGraphX_PROCESS_VERSION})${${LithoGraphX_PACKAGE_NAME}_DEPENDENCIES}"
Description="${${LithoGraphX_PACKAGE_NAME}_DESCRIPTION}"
OS="${LithoGraphX_OS}"
