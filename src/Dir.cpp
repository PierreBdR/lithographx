/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "Dir.hpp"
#include "Information.hpp"

#include <QFileInfo>
#include <QRegExp>
#include <QProcessEnvironment>

namespace lgx {
namespace util {
// static QString _currentPath;

static QString stripDir(const QDir& dir, QString file)
{
  if(file.isEmpty()) return file;
  // Information::out << "stripDir('" << dir.absolutePath() << "', '" << file << "')" << endl;
  QFileInfo fi(file);
  QString result = dir.relativeFilePath(fi.absoluteFilePath());
  // Information::out << "  - '" << result << "'" << endl;
  if(result.startsWith(".."))   // If file is not in a subfolder of dir, then try to go up
  {
    QDir cur = fi.dir();
    QString filename = fi.fileName();
    QString r;
    do {
      r = dir.relativeFilePath(QDir(cur.canonicalPath()).filePath(filename));
      // Information::out << " # '" << cur.canonicalPath() << "' // '" << filename << "'" << endl;
      // Information::out << " - '" << r << "'" << endl;
      if(result.size() > r.size())
        result = r;

      filename = QDir(cur.dirName()).filePath(filename);
      if(!cur.cdUp())
        break;
    } while(r.startsWith(".."));
    if(not r.startsWith(".."))
      result = r;
  }
  // Information::out << " --> '" << result << "'" << endl;
  return result;
}

QString stripCurrentDir(QString file) {
  return stripDir(currentPath(), file);
}

// Strip directory from name
QString stripDir(const QString& dir, QString file) {
  if(file.isEmpty()) return file;
  return stripDir(QDir(dir), file);
}

// Return directory from name
QString getDir(QString file)
{
  // Information::out << "getDir('" << file << "' = '";
  QString realFile = file.trimmed();
  QString result;
  if(QDir::isRelativePath(realFile))
    result = absoluteFilePath(realFile);
  else
    result = realFile;
  // Information::out << result << "'\n" << endl;
  return QFileInfo(result).absolutePath();
}

bool setCurrentPath(const QString& path) {
  return QDir::setCurrent(path);
}

QString currentPath() {
  return QDir::current().absolutePath();
}

QDir currentDir() {
  return QDir::current();
}

QString absoluteFilePath(QString filename)
{
  if(not filename.isEmpty() and QDir::isRelativePath(filename))
    return QDir::cleanPath(currentDir().absoluteFilePath(filename));
  else
    return filename;
}

QString resolvePath(QString path)
{
  path = path.trimmed();
  if(path.startsWith("~/"))
    path = QDir::home().filePath(path.mid(2));
  QRegExp env("\\$(\\w+)");
  QProcessEnvironment environment = QProcessEnvironment::systemEnvironment();
  int idx;
  while((idx = env.indexIn(path)) != -1) {
    QString rep = environment.value(env.cap(1));
    path = path.left(idx) + rep + path.mid(idx + env.matchedLength());
  }
  return absoluteFilePath(path);
}

bool createPath(QString path)
{
  path = QDir::cleanPath(path);
  QString to_create;
  QFileInfo fi = path;
  fi.makeAbsolute();
  path = fi.filePath();
  while(not fi.isDir()) {
    to_create = fi.fileName() + QDir::separator() + to_create;
    path = fi.dir().absolutePath();
    fi = QFileInfo(path);
  }
  return QDir(path).mkpath(to_create);
}

} // namespace util
} // namespace lgx
