/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef CORE_CANVASSTACKPROCESS_HPP
#define CORE_CANVASSTACKPROCESS_HPP

#include <Process.hpp>

#include <Misc.hpp>
#include <Stack.hpp>
#include <Store.hpp>

namespace lgx {
namespace process {

/**
 * \class ReverseStack StackProcess.hpp <StackProcess.hpp>
 *
 * Reverse the direction of the selected axes.
 */
class LGXCORE_EXPORT ReverseStack : public StackProcess {
public:
  ReverseStack(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const ParmList& parms) override
  {
    if(!checkState().store(STORE_NON_EMPTY))
      return false;
    Stack* s = currentStack();
    Store* input = s->currentStore();
    Store* output = s->work();
    bool reverse_x = parmToBool(parms[0]);
    bool reverse_y = parmToBool(parms[1]);
    bool reverse_z = parmToBool(parms[2]);
    if((*this)(output, input, reverse_x, reverse_y, reverse_z)) {
      input->hide();
      output->show();
      return true;
    }
    return false;
  }

  bool operator()(Store* output, const Store* input, bool x, bool y, bool z);

  QString folder() const override {
    return "Canvas";
  }
  QString name() const override {
    return "Reverse Axes";
  }
  QString description() const override
  {
    return "Reverse the direction of the selected axes.\n"
           "Press A-key to display the axis.";
  }
  QStringList parmNames() const override
  {
    return QStringList() << "X"
                         << "Y"
                         << "Z";
  }
  QStringList parmDescs() const override
  {
    return QStringList() << "X"
                         << "Y"
                         << "Z";
  }
  ParmList parmDefaults() const override
  {
    return ParmList() << false
                      << false
                      << true;
  }
  ParmChoiceMap parmChoice() const override
  {
    ParmChoiceMap map;
    map[0] = map[1] = map[2] = booleanChoice();
    return map;
  }
  QIcon icon() const override {
    return QIcon(":/images/Resize.png");
  }
};

/**
 * \class ChangeVoxelSize StackProcess.hpp <StackProcess.hpp>
 *
 * Change the size of the stack's voxel, without changing the data itself.
 */
class LGXCORE_EXPORT ChangeVoxelSize : public StackProcess {
public:
  ChangeVoxelSize(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const ParmList& parms) override
  {
    if(!checkState().stack(STACK_NON_EMPTY))
      return false;
    Stack* s = currentStack();
    bool ok;
    float x, y, z;
    if(parms[0].toString().isEmpty()) x = 0.f;
    else {
      x = parms[0].toFloat(&ok);
      if(not ok) return setErrorMessage("Error, bad value for x parameter");
    }
    if(parms[1].toString().isEmpty()) y = 0.f;
    else {
      y = parms[1].toFloat(&ok);
      if(not ok) return setErrorMessage("Error, bad value for y parameter");
    }
    if(parms[2].toString().isEmpty()) z = 0.f;
    else {
      z = parms[2].toFloat(&ok);
      if(not ok) return setErrorMessage("Error, bad value for z parameter");
    }
    Point3f nv(x, y, z);
    return (*this)(s, nv);
  }

  bool operator()(Stack* stack, Point3f nv);

  QString folder() const override {
    return "Canvas";
  }
  QString name() const override {
    return "Change Voxel Size";
  }
  QString description() const override {
    return "Change the size of a voxel (i.e. doesn't change the data)";
  }
  QStringList parmNames() const override
  {
    return QStringList() << QString::fromWCharArray(L"X (\xb5m)") << QString::fromWCharArray(L"Y (\xb5m)")
                         << QString::fromWCharArray(L"Z (\xb5m)");
  }
  QStringList parmDescs() const override
  {
    return QStringList() << QString::fromWCharArray(L"X (\xb5m)") << QString::fromWCharArray(L"Y (\xb5m)")
                         << QString::fromWCharArray(L"Z (\xb5m)");
  }
  ParmList parmDefaults() const override
  {
    return ParmList() << 1.0
                      << 1.0
                      << 1.0;
  }
  QIcon icon() const override {
    return QIcon(":/images/Resize.png");
  }
};

/**
 * \class ResizeCanvas StackProcess.hpp <StackProcess.hpp>
 *
 * Resize the stack to add or remove voxels.
 */
class LGXCORE_EXPORT ResizeCanvas : public StackProcess {
public:
  ResizeCanvas(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const ParmList& parms) override
  {
    if(!checkState().store(STORE_NON_EMPTY))
      return false;
    Stack* s = currentStack();
    bool okx, oky, okz;
    auto ds = Point3i(parms[2].toInt(&okx), parms[3].toInt(&oky), parms[4].toInt(&okz));
    if(not (okx and oky and okz))
      return setErrorMessage("Error, 'x', 'y' and 'z' parameters must be integer numbers");
    return (*this)(s, parmToBool(parms[0]), parmToBool(parms[1]), ds);
  }

  bool operator()(Stack* stack, bool isRelative, bool center, Point3i ds);

  QString folder() const override {
    return "Canvas";
  }
  QString name() const override {
    return "Resize Canvas";
  }
  QString description() const override
  {
    return "Resize the stack to add or remove voxels.\n"
           "Make sure BBox is checked on before running.";
  }
  QStringList parmNames() const override
  {
    return QStringList() << "Relative"
                         << "Center"
                         << "X"
                         << "Y"
                         << "Z";
  }
  QStringList parmDescs() const override
  {
    return QStringList() << "If true, X, Y and Z are given in percentage, if false in voxels."
                         << "New canvas centered as the old one, or else use the bottom left corner as reference."
                         << "Canvas size for X direction, in percentage or voxels."
                         << "Canvas size for Y direction, in percentage or voxels."
                         << "Canvas size for Z direction, in percentage or voxels.";
  }
  ParmList parmDefaults() const override
  {
    return ParmList() << true
                      << true
                      << 0
                      << 0
                      << 0;
  }
  ParmChoiceMap parmChoice() const override
  {
    ParmChoiceMap map;
    map[0] = booleanChoice();
    map[1] = booleanChoice();
    return map;
  }
  QIcon icon() const override {
    return QIcon(":/images/Resize.png");
  }
};

/**
 * \class ScaleStack StackProcess.hpp <StackProcess.hpp>
 *
 * Scale the stack.
 */
class LGXCORE_EXPORT ScaleStack : public StackProcess {
public:
  ScaleStack(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const ParmList& parms) override
  {
    if(!checkState().store(STORE_NON_EMPTY))
      return false;
    Stack* s = currentStack();
    Point3f newsize(parms[1].toFloat(), parms[2].toFloat(), parms[3].toFloat());
    return (*this)(s, newsize, parmToBool(parms[0]));
  }

  bool operator()(Stack* stack, Point3f newsize, bool percent);

  QString folder() const override {
    return "Canvas";
  }
  QString name() const override {
    return "Scale Stack";
  }
  QString description() const override {
    return "Scale the stack.";
  }
  QStringList parmNames() const override
  {
    return QStringList() << "Percent"
                         << "X"
                         << "Y"
                         << "Z";
  }
  QStringList parmDescs() const override
  {
    return QStringList() << "Percent"
                         << "X"
                         << "Y"
                         << "Z";
  }
  ParmList parmDefaults() const override
  {
    return ParmList() << true
                      << 0.0
                      << 0.0
                      << 0.0;
  }
  ParmChoiceMap parmChoice() const override
  {
    ParmChoiceMap map;
    map[0] = map[1] = booleanChoice();
    return map;
  }
  QIcon icon() const override {
    return QIcon(":/images/Scale.png");
  }
};

/**
 * \class ShiftStack StackProcess.hpp <StackProcess.hpp>
 *
 * Shift main and work stores within the canvas (e.g. the voxels' values are
 * moved within the image)
 */
class LGXCORE_EXPORT ShiftStack : public StackProcess {
public:
  ShiftStack(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const ParmList& parms) override
  {
    if(!checkState().store(STORE_NON_EMPTY))
      return false;
    Stack* s = currentStack();
    Point3i ds(parms[1].toInt(), parms[2].toInt(), parms[3].toInt());
    return (*this)(s, parmToBool(parms[0]), ds);
  }

  bool operator()(Stack* stack, bool origin, Point3i ds);

  QString folder() const override {
    return "Canvas";
  }
  QString name() const override {
    return "Shift Stack";
  }
  QString description() const override {
    return "Shift both stores of the stack to within the canvas.";
  }
  QStringList parmNames() const override
  {
    return QStringList() << "Origin"
                         << "X"
                         << "Y"
                         << "Z";
  }
  QStringList parmDescs() const override
  {
    return QStringList() << "Origin"
                         << "X"
                         << "Y"
                         << "Z";
  }
  ParmList parmDefaults() const override
  {
    return ParmList() << false
                      << 0
                      << 0
                      << 0;
  }
  ParmChoiceMap parmChoice() const override
  {
    ParmChoiceMap map;
    map[0] = booleanChoice();
    return map;
  }
  QIcon icon() const override {
    return QIcon(":/images/Shift.png");
  }
};

} // namespace process
} // namespace lgx

#endif // CORE_CANVASSTACKPROCESS_HPP
