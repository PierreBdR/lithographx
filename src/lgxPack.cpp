/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include <LGXVersion.hpp>

#include <Dir.hpp>
#include <Information.hpp>
#include <PackageArchive.hpp>
#include <PackageManager.hpp>
#include <SystemDirs.hpp>

#include <QCoreApplication>
#include <QCommandLineParser>
#include <QFile>

namespace {

bool quitPacking = false;

// Install handler from CTRL-C
#ifdef WIN32
#  include <Wincon.h>

BOOL WINAPI signalHandler(DWORD dwCtrlType)
{
  quitPacking = true;
}

void installSignalHandler()
{
  SetConsoleCtrlHandler(signalHandler, TRUE);
}

#else
#  include <signal.h>
void signalHandler(int sig)
{
  quitPacking = true;
}

void installSignalHandler()
{
  signal(SIGINT, signalHandler);
  signal(SIGINT, signalHandler);
}

#endif

bool showProgress() {
  static const QStringList waiters = { "/", "-", "\\", "|" };
  static int idx = 0;
  lgx::Information::out << waiters[idx++] << "\b" << flush;
  if(idx >= waiters.size())
    idx = 0;
  if(quitPacking) {
    lgx::Information::err << "User-triggered exit." << endl;
    return false;
  }
  return true;
}

void showStep(const QString& msg) {
  lgx::Information::out << msg << " ... " << flush;
}

void showError(const QString& msg, bool paused) {
  lgx::Information::err << "ERROR:\n" << msg << endl;
  if(paused) {
    lgx::Information::err << "Press ENTER to continue." << endl;
    fgetc(stdin);
  }
}

void showOk() {
  lgx::Information::out << "OK" << endl;
}

bool alwaysNo = false;
bool alwaysYes = false;

bool yesNo(const QString& question) {
  if(alwaysYes) return true;
  if(alwaysNo) return false;
  while(true) {
    lgx::Information::out << question << " [y/n]" << endl;
    int ci = fgetc(stdin);
    if(ci == EOF)
      return false;
    auto c = (unsigned char)ci;
    if(c == 'y' or c == 'Y')
      return true;
    if(c == 'n' or c == 'N')
      return false;
  }
}

using lgx::Information::out;

struct CommandLineParseResult {
  enum Action {
    Error,
    Install,
    Compile,
    Create,
    Uninstall,
    ListPackages,
    ListFiles,
    ListLibraries,
    ShowPackage,
    ShowInstalled
  } toDo = Error;
  bool user = true, system = true, force = false;
  QString package;
  QString output;
  bool output_dir;

  bool Do(Action act, QString& errorMessage) {
    if(toDo != Error) {
      toDo = Error;
      errorMessage = "Only one of 'list', 'install', 'uninstall', 'show', 'files', 'libs' and 'compile' can be specified.";
      return false;
    }
    toDo = act;
    return true;
  }

};

CommandLineParseResult parseCommandLine(QCommandLineParser &parser, QString& errorMessage)
{
  parser.setSingleDashWordOptionMode(QCommandLineParser::ParseAsCompactedShortOptions);
  parser.setApplicationDescription("Command-line utility used to compile and manager packages for LithoGraphX.");
  parser.addHelpOption();
  parser.addVersionOption();

  // Note, addOptions is only introduced in Qt 5.4, so we cannot use it here
  parser.addOption(QCommandLineOption({"c", "compile"}, "Compile a source package.", "packagePath" ));
  parser.addOption(QCommandLineOption({"i", "install"},
                     "Install a package, by default in the user space. "
                     "If the package is a source package, it will also try to compile it.",
                     "packagePath" ));
  parser.addOption(QCommandLineOption({"s", "show"}, "Show detailed information on a package", "packageName" ));
  parser.addOption(QCommandLineOption({"f", "files"}, "List files of a package.", "packageName" ));
  parser.addOption(QCommandLineOption({"l", "list"}, "List installed packages." ));
  parser.addOption(QCommandLineOption({"L", "libs"}, "List libraries installed by a package.", "packageName"));
  parser.addOption(QCommandLineOption({"u", "uninstall"}, "Uninstall a package.", "packageName" ));
  parser.addOption(QCommandLineOption("installed",
                                      "Check if the package is installed and write 'user' or 'system' depending on "
                                      "where it has been installed.", "package" ));
  parser.addOption(QCommandLineOption({"o", "output"}, "Specify the name of the output package.", "output" ));
  parser.addOption(QCommandLineOption({"O", "output-dir"}, "Specify the folder in which the output package will be written.", "outputDir" ));
  parser.addOption(QCommandLineOption({"C", "create"}, "Create a binary package from an installed one.", "packageName"));
  parser.addOption(QCommandLineOption("system", "Work on the system packages only."));
  parser.addOption(QCommandLineOption("user", "Work on the user packages only."));
  parser.addOption(QCommandLineOption({"y", "yes"}, "Answer 'yes' to all questions."));
  parser.addOption(QCommandLineOption({"n", "no"}, "Answer 'no' to all questions."));
  parser.addOption(QCommandLineOption("force", "Force the (un-)installation to proceed even if dependencies say it shouldn't work."));

  CommandLineParseResult result;

  parser.process(QCoreApplication::arguments());

  if(not parser.positionalArguments().empty()) {
    errorMessage = "lgxPack doesn't accept any positional argument.";
    return {};
  }

  if(parser.isSet("list") and
     not result.Do(CommandLineParseResult::ListPackages, errorMessage))
    return {};

  if(parser.isSet("install")) {
    if(not result.Do(CommandLineParseResult::Install, errorMessage))
      return {};
    result.package = parser.value("install");
  }

  if(parser.isSet("uninstall")) {
    if(not result.Do(CommandLineParseResult::Uninstall, errorMessage))
      return {};
    result.package = parser.value("uninstall");
  }

  if(parser.isSet("compile")) {
    if(not result.Do(CommandLineParseResult::Compile, errorMessage))
      return {};
    result.package = parser.value("compile");
    if(parser.isSet("output")) {
      result.output = parser.value("output");
      result.output_dir = false;
    } else if(parser.isSet("output-dir")) {
      result.output = parser.value("output-dir");
      result.output_dir = true;
    }
  }

  if(parser.isSet("create")) {
    if(not result.Do(CommandLineParseResult::Create, errorMessage))
      return {};
    result.package = parser.value("create");
    if(parser.isSet("output")) {
      result.output = parser.value("output");
      result.output_dir = false;
    } else if(parser.isSet("output-dir")) {
      result.output = parser.value("output-dir");
      result.output_dir = true;
    }
  }

  if(parser.isSet("files")) {
    if(not result.Do(CommandLineParseResult::ListFiles, errorMessage))
      return {};
    result.package = parser.value("files");
  }

  if(parser.isSet("libs")) {
    if(not result.Do(CommandLineParseResult::ListLibraries, errorMessage))
      return {};
    result.package = parser.value("libs");
  }

  if(parser.isSet("show")) {
    if(not result.Do(CommandLineParseResult::ShowPackage, errorMessage))
      return {};
    result.package = parser.value("show");
  }

  if(parser.isSet("installed")) {
    if(not result.Do(CommandLineParseResult::ShowInstalled, errorMessage))
      return {};
    result.package = parser.value("installed");
  }

  if(parser.isSet("user")) {
    result.user = true;
    result.system = false;
  }

  if(parser.isSet("system")) {
    if(not result.system) {
      errorMessage = "Error, only one of '--user' or '--system' can be specified.";
      return {};
    }
    result.system = true;
    result.user = false;
  }

  if(parser.isSet("yes") and parser.isSet("no")) {
    errorMessage = "Error, only on of '--yes' or '--no' can be specified.";
    return {};
  }

  if(parser.isSet("yes"))
    alwaysYes = true;
  if(parser.isSet("no"))
    alwaysNo = true;

  if(parser.isSet("force"))
    result.force = true;

  return result;
}

} // namespace

int main(int argc, char** argv)
{
  QCoreApplication::setOrganizationDomain(LGX_DOMAINNAME);
  QCoreApplication::setApplicationName("lgxPack");
  QCoreApplication::setApplicationVersion(VERSION);

  QCoreApplication app(argc, argv);

  QCommandLineParser parser;

  QString errorMessage;
  auto result = parseCommandLine(parser, errorMessage);

  installSignalHandler();

  auto ui = lgx::PackagingUi {
    &showProgress,
    {},
    &showStep,
    &showError,
    &showOk,
    &yesNo
  };

  lgx::PackageManager manager;

  switch(result.toDo) {
    case CommandLineParseResult::Error:
      lgx::Information::err << errorMessage << endl;
      return 1;
    case CommandLineParseResult::Compile:
      {
        auto package = lgx::util::absoluteFilePath(result.package);
        QString compiled_package;
        if(not result.output.isEmpty())
          compiled_package = lgx::compilePackage(package, lgx::util::absoluteFilePath(result.output),
                                                 result.output_dir, result.force, ui);
        else
          compiled_package = lgx::compilePackage(package, result.force, ui);
        if(compiled_package.isEmpty())
          return 2;
        return 0;
      }
    case CommandLineParseResult::Create:
      {
        QString outputFile;
        if(result.output.isEmpty())
          outputFile = lgx::createPackage(result.package, result.output, result.output_dir, ui);
        else
          outputFile = lgx::createPackage(result.package, ui);
        if(outputFile.isEmpty())
          return 2;
        return 0;
      }
    case CommandLineParseResult::Install:
      {
        if(result.user and result.system)
          result.user = false;
        auto package = lgx::util::absoluteFilePath(result.package);
        if(lgx::installPackage(package, result.user, result.force, ui))
          return 0;
        return 2;
      }
    case CommandLineParseResult::Uninstall:
      {
        if(not result.force) {
          bool ok;
          QStringList deps;
          std::tie(ok, deps) = manager.canUninstall(result.package);
          if(not ok) {
            lgx::Information::err << "Error, cannot uninstall the package";
            if(deps.isEmpty())
              lgx::Information::err << " as it doesn't exist";
            else {
              lgx::Information::err << " as the following packages would break:\n";
              for(auto pkgName : deps)
                lgx::Information::err << " - " << pkgName << ": " << manager.package(pkgName)->dependencies()->toString() << endl;
            }
            return 2;
          }
        }
        if(lgx::uninstallPackage(result.package, ui))
          return 0;
        return 2;
      }
    case CommandLineParseResult::ListPackages:
      lgx::Information::out << lgx::listPackages(result.user, result.system).join("\n") << endl;
      return 0;
    case CommandLineParseResult::ListFiles:
      {
        bool ok;
        auto files = lgx::listFiles(result.package, &ok);
        if(not ok) {
          if(QFile::exists(result.package)) {
            lgx::PackageArchive pkgArch(result.package, ui);
            if(pkgArch) {
              files = pkgArch.fileList();
              ok = true;
            }
          }
        }
        if(not ok) {
          lgx::Information::err << "Error, no such package." << endl;
          return 1;
        }
        lgx::Information::out << files.join("\n") << endl;
        return 0;
      }
    case CommandLineParseResult::ListLibraries:
      {
        bool ok;
        auto files = lgx::listLibraries(result.package, &ok);
        if(not ok) {
          lgx::Information::err << "Error, no such package." << endl;
          return 1;
        }
        lgx::Information::out << files.join("\n") << endl;
        return 0;
      }
    case CommandLineParseResult::ShowPackage:
      {
        bool hasOS = true;
        auto pkg = lgx::findPackage(result.package);
        if(not pkg) {
          if(QFile::exists(result.package)) {
            auto pkgArch = lgx::PackageArchive(result.package, ui);
            if(pkgArch) {
              hasOS = pkgArch.isBinary();
              pkg = pkgArch.package();
            }
          }
        }
        if(not pkg) {
          lgx::Information::err << "Error, no such package." << endl;
          return 1;
        }
        lgx::Information::out << "Name = " << pkg.name() << endl;
        lgx::Information::out << "Version = " << pkg.version().toString() << endl;
        lgx::Information::out << "Description = " << pkg.description() << endl;
        lgx::Information::out << "Dependencies = " << pkg.dependencies()->toString() << endl;
        if(not pkg.sourcePackage().isEmpty())
          lgx::Information::out << "SourcePackage = " << pkg.sourcePackage() << endl;
        if(hasOS)
          lgx::Information::out << "OS = " << pkg.OS() << endl;
        lgx::Information::out << "Installation = " << (pkg.isUserPackage() ? "user" : "system") << endl;
        return 0;
      }
    case CommandLineParseResult::ShowInstalled:
      {
        auto usrPkg = lgx::listPackages(true, false);
        auto sysPkg = lgx::listPackages(false, true);
        if(usrPkg.contains(result.package)) {
          lgx::Information::out << "user" << endl;
          return 0;
        } else if(sysPkg.contains(result.package)) {
          lgx::Information::out << "system" << endl;
          return 0;
        }
        lgx::Information::err << "Error, no such package." << endl;
        return 1;
      }
  }
  return 0;
}
