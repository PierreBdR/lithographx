set(SAMPLES_FOLDER "${LithoGraphX_SOURCE_DIR}/AddOns/plugins/Samples")

macro(INSTALL_SAMPLE name)
  file(GLOB ${name}_file_list
    ${SAMPLES_FOLDER}/${name}/*)
  install(FILES ${${name}_file_list}
    COMPONENT doc
    DESTINATION ${DOC_PATH}/samples/${name})
endmacro(INSTALL_SAMPLE)

install_sample(ExportMeshToFile)
install_sample(ITKMedianImageFilter)
install_sample(MeshGammaFilter)
install_sample(MultiStackGammaFilter)
install_sample(StackGammaFilter)
