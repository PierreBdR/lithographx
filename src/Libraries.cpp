/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/
#include "Libraries.hpp"

#include <Information.hpp>

using std::swap;

namespace lgx
{

LGX_EXPORT std::vector<std::pair<LibraryDescription,int>> LibraryDeclaration::_libraries = {};

LibraryDeclaration::LibraryDeclaration(const LibraryDescription& l)
  : _lib(l)
{
  auto found = std::find_if(_libraries.begin(), _libraries.end(),
                            [&l](const std::pair<LibraryDescription,int>& e) -> bool {
                              return e.first.name == l.name and e.first.version == l.version;
                            });
  if(found == _libraries.end()) {
    _libraries.push_back({l, 1});
  } else {
    found->second++;
  }
}

LibraryDeclaration::~LibraryDeclaration()
{
  const auto& l = _lib;
  auto found = std::find_if(_libraries.begin(), _libraries.end(),
                            [&l](const std::pair<LibraryDescription,int>& e) -> bool {
                              return e.first.name == l.name and e.first.version == l.version;
                            });
  if(found != _libraries.end()) {
    if(--found->second <= 0) {
      if(found != (_libraries.end()-1))
        swap(*found, *(_libraries.end()-1));
      _libraries.pop_back();
    }
  }
}

void LibraryDeclaration::reset()
{
  _libraries.clear();
}

std::vector<LibraryDescription> LibraryDeclaration::libraries()
{
  std::vector<LibraryDescription> libs;
  for(const auto& p: _libraries) {
    libs.push_back(p.first);
  }
  std::sort(begin(libs), end(libs),
            [](const LibraryDescription& d1, const LibraryDescription& d2) -> bool {
              if(d1.name < d2.name) return true;
              if(d1.name > d2.name) return false;
              return d1.version < d2.version;
            });
  return libs;
}

} // namespace lgx
