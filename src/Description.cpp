/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "Description.hpp"
#include "Information.hpp"
#include "CSVStream.hpp"

#include <QBuffer>

namespace lgx {
QString& Description::fieldName(size_t idx) {
  return _fields[idx].first;
}

const QString& Description::fieldName(size_t idx) const {
  return _fields[idx].first;
}

QStringList Description::fieldNames() const
{
  QStringList res;
  for(const_iterator it = begin(); it != end(); ++it)
    res << it->first;
  return res;
}

QString& Description::operator[](size_t idx) {
  return _fields[idx].second;
}

const QString& Description::operator[](size_t idx) const {
  return _fields[idx].second;
}

QString& Description::operator[](const QString& name)
{
  for(iterator it = begin(); it != end(); ++it)
    if(it->first == name)
      return it->second;
  _fields.push_back(std::make_pair(name, QString()));
  return _fields.back().second;
}

QString Description::field(const QString& name, const QString& def) const
{
  for(const_iterator it = begin(); it != end(); ++it)
    if(it->first == name)
      return it->second;
  return def;
}

bool Description::remove(const QString& name)
{
  for(iterator it = begin(); it != end(); ++it)
    if(it->first == name) {
      erase(it);
      return true;
    }
  return false;
}

bool Description::remove(size_t i)
{
  if(i < size()) {
    iterator it = begin() + i;
    erase(it);
    return true;
  }
  return false;
}

QString Description::field(size_t i, const QString& def) const
{
  if(i < size())
    return _fields[i].second;
  return def;
}

Description& Description::operator<<(const std::pair<QString, QString>& p)
{
  _fields.push_back(p);
  return *this;
}

QString Description::toCSV() const
{
  QStringList clean;
  clean << util::CSVStream::shield(_type);
  for(const_iterator it = begin(); it != end(); ++it)
    clean << util::CSVStream::shield(it->first) << util::CSVStream::shield(it->second);
  return clean.join(",");
}

QStringList Description::toList() const
{
  QStringList lst;
  lst << _type;
  for(const_iterator it = begin(); it != end(); ++it)
    lst << it->first << it->second;
  return lst;
}

Description Description::fromCSV(const QString& value, bool* ok)
{
  if(value.isEmpty()) {
    if(ok)
      *ok = true;
    return Description();
  }
  QByteArray arr = value.toUtf8();
  QBuffer buffer(&arr);
  buffer.open(QIODevice::ReadOnly);

  QStringList fields;
  util::CSVStream data(&buffer);

  data >> fields;
  if(data.status() != QTextStream::Ok) {
    if(ok)
      *ok = false;
    return Description();
  }

  if(ok)
    *ok = true;
  return fromList(fields, ok);
}

Description Description::fromList(const QStringList& fields, bool* ok)
{
  Description res;

  if(fields.empty()) {
    if(ok)
      *ok = false;
    return res;
  }

  bool need_type = true;
  bool need_name = true;
  QString name;

  for(const QString& field: fields) {
    if(need_type) {
      res._type = field;
      need_type = false;
    } else if(need_name) {
      name = field;
      need_name = false;
    } else {
      res._fields.push_back({name, field});
      need_name = true;
    }
  }

  if(not need_name) {
    if(ok)
      *ok = false;
    res.clear();
    return res;
  }

  if(ok)
    *ok = true;

  return res;
}
} // namespace lgx
