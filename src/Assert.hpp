/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef ASSERT_HPP
#define ASSERT_HPP

/**
 * \file Assert.hpp
 *
 * Graphical (or textual) assertion utility
 *
 * Define the macro \c vvassert(expr)
 */

#include <cassert>

class QString;

namespace lgx {
namespace util {
void __assert_fail(const QString& assertion, const char* file, unsigned int line, const char* function);

#ifdef NDEBUG
#  define lgx_assert(expr) static_cast<void>(0)
#  define lgx_assert_msg(expr, msg) static_cast<void>(0)
#else
/**
 * \def vvassert(expr)
 *
 * If NDEBUG is not defined and expr is false, warn the user of the program
 * of the failed assertion. If TEXT_VVASSERT is defined, vvassert falls back
 * onto the default assert function, otherwise, it shows a Qt message box
 * with the assert message, then abort the program.
 */
#  define lgx_assert(expr) \
  ((expr) ? static_cast<void>(0) : lgx::util::__assert_fail(#expr, __FILE__, __LINE__, __PRETTY_FUNCTION__))
#  define lgx_assert_msg(expr, msg) \
  ((expr) ? static_cast<void>(0) : lgx::util::__assert_fail(msg, __FILE__, __LINE__, __PRETTY_FUNCTION__))
#endif
} // namespace util
} // namespace lgx

#endif
