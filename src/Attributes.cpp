#include <Attributes.hpp>

#include <Information.hpp>
#include <GL.hpp>

#include <QStringList>

#include <memory>
#include <new>
#include <string.h>

namespace lgx {
namespace util {

AttrArray::AttrArray(size_type element_size)
  : _properties(0)
{
  allocate(0, element_size);
}

AttrArray::AttrArray(size_t count, size_t element_size)
  : _properties(0)
{
  allocate(count, element_size);
}

bool AttrArray::empty() const
{
  return _properties->count == 0;
}

void AttrArray::modified()
{
  _properties->modified = true;
}

void AttrArray::resetModified()
{
  _properties->modified = false;
}

bool AttrArray::wasModified()
{
  return _properties->modified;
}

AttrArray& AttrArray::operator=(const AttrArray& copy)
{
  release();
  _properties = copy._properties;
  acquire();
  return *this;
}

AttrArray& AttrArray::operator=(AttrArray&& copy)
{
  std::swap(_properties, copy._properties);
  return *this;
}

bool AttrArray::set(const AttrArray& copy)
{
  reserve_(copy.size());
  _properties->count = copy.size();
  _properties->element_size = copy.element_size();
  memcpy(data(), copy.data(), byteSize());
  return true;
}

void AttrArray::allocate(size_type count, size_type element_size)
{
  std::unique_ptr<properties_t> prop(new properties_t(count, element_size));
  if(prop->element_size > 0)
  {
    prop->_data = (array_pointer) new value_type[prop->reserved*prop->element_size];
    if(!prop->_data) throw std::bad_alloc();
    prop->_min = (array_pointer) new value_type[prop->element_size];
    if(!prop->_min) throw std::bad_alloc();
    prop->_max = (array_pointer) new value_type[prop->element_size];
    if(!prop->_max) throw std::bad_alloc();
  }
  _properties = prop.release();
}

AttrArray::AttrArray(const AttrArray& copy)
  : _properties(copy._properties)
{
  acquire();
}

AttrArray::AttrArray(AttrArray&& other)
  : _properties(std::move(other._properties))
{
  other._properties = 0;
}

void AttrArray::acquire()
{
  ++_properties->reference_count;
}

AttrArray AttrArray::copy() const
{
  AttrArray result(size(), element_size());
  memcpy(result.data(), data(), byteSize());
  return result;
}

AttrArray::~AttrArray()
{
  release();
}

void AttrArray::release()
{
  if(_properties)
  {
    --_properties->reference_count;
    if(_properties->reference_count == 0)
      delete _properties;
    _properties = 0;
  }
}

void AttrArray::reserve_(size_type size)
{
  size_type to_allocate = (size == 0 ? 1 : size);
  array_pointer new_data = (array_pointer) new value_type[_properties->element_size*to_allocate];
  if(not new_data) throw std::bad_alloc();
  size_type to_copy = _properties->count;
  if(to_copy > size) to_copy = size;
  if(to_copy > 0)
    memcpy(new_data.get(), _properties->_data.get(), to_copy*_properties->element_size);
  _properties->reserved = to_allocate;
  _properties->_data.reset(new_data.release());
}

void AttrArray::reserve(size_type size)
{
  if(size > _properties->reserved)
    reserve_(size);
}

void AttrArray::resize(size_type size)
{
  if(size != _properties->reserved)
    reserve_(size);
  _properties->count = size;
}

AttrArray::pointer AttrArray::push_back(size_type n)
{
  _properties->count += n;
  size_type r = _properties->reserved;
  while(_properties->count > r)
    r *= 2;;
  if(r > _properties->reserved)
    reserve(r);
  return back();
}

void AttrArray::pop_back(size_type n)
{
  if(n > 0)
  {
    if(n > _properties->count) _properties->count = 0;
    else _properties->count -= n;
  }
}

const AttrArray::size_type AttributesSet::typeSizes[NB_ATTR_TYPES] =
{
  sizeof(char),
  sizeof(int8_t),
  sizeof(uint8_t),
  sizeof(int16_t),
  sizeof(uint16_t),
  sizeof(int32_t),
  sizeof(uint32_t),
  sizeof(float),
  sizeof(double)
};

const GLuint AttributesSet::openGLTypes[NB_ATTR_TYPES] =
{
  GL_BYTE,
  GL_BYTE,
  GL_UNSIGNED_BYTE,
  GL_SHORT,
  GL_UNSIGNED_SHORT,
  GL_INT,
  GL_UNSIGNED_INT,
  GL_FLOAT,
  GL_DOUBLE
};

AttributesSet::AttributesSet()
  : _count(0)
  , _array_size(0)
{ }

AttributesSet::AttributesSet(const AttributesSet& copy)
  : _count(copy._count)
  , _array_size(copy._array_size)
{
  for(attribute_map_t::const_iterator it = copy.attribute_map.begin() ;
      it != copy.attribute_map.end() ; ++it)
  {
    Attribute attr;
    const Attribute& to_copy = it->second;
    attr.name = to_copy.name;
    attr.type = to_copy.type;
    attr.unit = to_copy.unit;
    attr.array = to_copy.array.copy();
    attr.interpolator = to_copy.interpolator->copy();
    attr.interpolator->set(attr.array);
    attribute_map[attr.name] = attr;
  }
}

AttributesSet::AttributesSet(AttributesSet&& copy)
  : attribute_map(std::move(copy.attribute_map))
  , _count(std::move(copy._count))
  , _array_size(std::move(copy._array_size))
{
  copy.attribute_map.clear();
  copy._array_size = 0;
  copy._count = 0;
}

AttributesSet::~AttributesSet()
{
  reset();
}

AttributesSet& AttributesSet::operator=(const AttributesSet& copy)
{
  reset();
  for(attribute_map_t::const_iterator it = copy.attribute_map.begin() ;
      it != copy.attribute_map.end() ; ++it)
  {
    Attribute attr;
    const Attribute& to_copy = it->second;
    attr.name = to_copy.name;
    attr.type = to_copy.type;
    attr.unit = to_copy.unit;
    attr.array = to_copy.array;
    attr.interpolator = to_copy.interpolator->copy();
    attr.interpolator->set(attr.array);
    attribute_map[attr.name] = attr;
  }
  return *this;
}

AttributesSet& AttributesSet::operator=(AttributesSet&& copy)
{
  attribute_map = std::move(copy.attribute_map);
  _count = std::move(copy._count);
  _array_size = std::move(_array_size);
  copy.attribute_map.clear();
  copy._array_size = 0;
  copy._count = 0;
  return *this;
}

bool AttributesSet::needsConsolidation() const
{
  return _count != _array_size;
}

bool AttributesSet::add(QString name, size_type tuple_size, AttributeType type, std::shared_ptr<Interpolator> interpolator)
{
  attribute_map_t::const_iterator found = attribute_map.find(name);
  if(found != attribute_map.end()) return false;
  Attribute attr;
  attr.name = name;
  attr.type = type;
  attr.unit = QString();
  attr.array = AttrArray(_array_size, typeSizes[type]*tuple_size);
  if(interpolator)
  {
    interpolator->set(attr.array);
    attr.interpolator = interpolator;
    for(size_type i = 0 ; i < _array_size ; ++i)
      (*interpolator)(i);
  }
  attribute_map[name] = attr;
  return true;
}

bool AttributesSet::add(QString name, size_type tuple_size, QString unit, std::shared_ptr<Interpolator> interpolator)
{
  attribute_map_t::const_iterator found = attribute_map.find(name);
  if(found != attribute_map.end()) return false;
  Attribute attr;
  attr.name = name;
  attr.type = UNIT;
  attr.unit = unit;
  attr.array = AttrArray(_array_size, typeSizes[UNIT]*tuple_size);
  if(interpolator)
  {
    interpolator->set(attr.array);
    attr.interpolator = interpolator;
    for(size_type i = 0 ; i < _array_size ; ++i)
      (*interpolator)(i);
  }
  attribute_map[name] = attr;
  return true;
}

bool AttributesSet::add(QString name, const AttrArray& array, AttributeType type, std::shared_ptr<Interpolator> interpolator)
{
  attribute_map_t::const_iterator found = attribute_map.find(name);
  if(found != attribute_map.end()) return false;
  if(array.element_size() % typeSizes[type] != 0) return false;
  if(array.size() != _array_size) return false;
  Attribute attr;
  attr.name = name;
  attr.type = type;
  attr.unit = QString();
  attr.array = AttrArray(array);
  if(interpolator)
  {
    interpolator->set(attr.array);
    attr.interpolator = interpolator;
  }
  attribute_map[name] = attr;
  return true;
}

bool AttributesSet::add(QString name, const AttrArray& array, QString unit, std::shared_ptr<Interpolator> interpolator)
{
  attribute_map_t::const_iterator found = attribute_map.find(name);
  if(found != attribute_map.end()) return false;
  if(array.element_size() % typeSizes[UNIT] != 0) return false;
  if(array.size() != _array_size) return false;
  Attribute attr;
  attr.name = name;
  attr.type = UNIT;
  attr.unit = unit;
  attr.array = AttrArray(array);
  if(interpolator)
  {
    interpolator->set(attr.array);
    attr.interpolator = interpolator;
  }
  attribute_map[name] = attr;
  return true;
}

bool AttributesSet::contains(QString name)
{
  return attribute_map.find(name) != attribute_map.end();
}

bool AttributesSet::erase(QString name)
{
  attribute_map_t::iterator found = attribute_map.find(name);
  if(found == attribute_map.end()) return false;
  attribute_map.erase(found);
  return true;
}

void AttributesSet::reset()
{
  attribute_map.clear();
  _array_size = _count;
}

void AttributesSet::reserve(size_type count)
{
  for(attribute_map_t::iterator it = attribute_map.begin() ;
      it != attribute_map.end() ; ++it)
    it->second.array.reserve(count);
}

QStringList AttributesSet::names() const
{
  QStringList lst;
  for(const Attribute& attr: *this)
  {
    lst << attr.name;
  }
  return lst;
}

QStringList AttributesSet::names(const QList<AttributeType>& types) const
{
  QStringList lst;
  for(const Attribute& attr: *this)
  {
    if(types.contains(attr.type))
      lst << attr.name;
  }
  return lst;
}

QStringList AttributesSet::names(AttributeType type) const
{
  QStringList lst;
  for(const Attribute& attr: *this)
  {
    if(attr.type == type)
      lst << attr.name;
  }
  return lst;
}

AttributesSet::Attribute AttributesSet::attribute(QString name) const
{
  attribute_map_t::const_iterator found = attribute_map.find(name);
  if(found != attribute_map.end()) return found->second;
  return Attribute();
}

AttributesSet::AttributeType AttributesSet::attributeType(QString name) const
{
  attribute_map_t::const_iterator found = attribute_map.find(name);
  if(found != attribute_map.end()) return found->second.type;
  return NB_ATTR_TYPES;
}

AttributesSet::size_type AttributesSet::addElement()
{
  size_type pos = _array_size;
  for(attribute_map_t::iterator it = attribute_map.begin() ;
      it != attribute_map.end() ; ++it)
  {
    it->second.array.push_back(1);
    (*it->second.interpolator)(pos);
  }
  ++_count;
  ++_array_size;
  return pos;
}

AttributesSet::size_type AttributesSet::addElements(size_type n)
{
  size_type pos = _array_size;
  for(attribute_map_t::iterator it = attribute_map.begin() ;
      it != attribute_map.end() ; ++it)
  {
    it->second.array.push_back(n);
    for(size_type i = 0 ; i < n ; ++i)
      (*it->second.interpolator)(i+pos);
  }
  _count += n;
  _array_size += n;
  return pos;
}

bool AttributesSet::consolidate(const std::vector<size_type>& reordering)
{
  for(attribute_map_t::iterator it = attribute_map.begin() ;
      it != attribute_map.end() ; ++it)
  {
    std::shared_ptr<Interpolator> interp = it->second.interpolator;
    AttrArray old = it->second.array;
    AttrArray new_data(reordering.size(), old.element_size());
    for(size_t i = 0 ; i < reordering.size() ; ++i)
    {
      size_type target = reordering[i];
      if(target < old.size())
        memcpy(new_data[i], old[target], old.element_size());
      else
        (*interp)(i);
    }
    old.set(new_data);
  }
  _array_size = _count;
  return true;
}

AttrBounds AttrArray::bounds() const
{
  return AttrBounds(_properties->element_size, _properties->_min.get(), _properties->_max.get());
}

bool AttrArray::setBounds(const AttrBounds& bounds)
{
  if(element_size() == bounds.element_size())
  {
    memcpy(_properties->_min.get(), bounds.min(), element_size());
    memcpy(_properties->_max.get(), bounds.max(), element_size());
    return true;
  }
  return false;
}

bool AttributesSet::updateBounds(QString name)
{
  Attribute attr = attribute(name);
  if(attr.valid())
  {
    attr.updateBounds();
    return true;
  }
  return false;
}

void AttributesSet::Attribute::updateBounds()
{
  switch(type)
  {
    case CHAR:
      TupleArray<char>(array).updateBounds();
      break;
    case INT8:
      TupleArray<int8_t>(array).updateBounds();
      break;
    case UINT8:
      TupleArray<int8_t>(array).updateBounds();
      break;
    case INT16:
      TupleArray<int16_t>(array).updateBounds();
      break;
    case UINT16:
      TupleArray<uint16_t>(array).updateBounds();
      break;
    case INT32:
      TupleArray<int32_t>(array).updateBounds();
      break;
    case UINT32:
      TupleArray<uint32_t>(array).updateBounds();
      break;
    case FLOAT:
      TupleArray<float>(array).updateBounds();
      break;
    case DOUBLE:
      TupleArray<double>(array).updateBounds();
      break;
    default:
      Information::err << "Error, unknown type: " << type << endl;
  };
}

AttrBounds::AttrBounds(size_type element_size)
  : _element_size(element_size)
  , _min(new value_type[element_size])
  , _max(new value_type[element_size])
{
}

AttrBounds::AttrBounds(const AttrBounds& copy)
  : _element_size(copy._element_size)
  , _min(new value_type[_element_size])
  , _max(new value_type[_element_size])
{
  memcpy(_min.get(), copy._min.get(), _element_size);
  memcpy(_max.get(), copy._max.get(), _element_size);
}

AttrBounds::AttrBounds(size_type element_size,
                       pointer min,
                       pointer max)
  : _element_size(element_size)
  , _min(new value_type[element_size])
  , _max(new value_type[element_size])
{
  memcpy(_min.get(), min, element_size);
  memcpy(_max.get(), max, element_size);
}

} // namespace util
} // namespace lgx
