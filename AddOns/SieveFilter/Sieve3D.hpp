#ifndef SIEVE3D_HPP
#define SIEVE3D_HPP

#include "Sieve.hpp"

#include <Stack.hpp>

namespace lgx {
namespace process {
namespace sieve {


struct Sieve3D : public Sieve {
  /// Note: implementation based on the 1998 MATLAB MEX file of J. Andrew Bangham

  /**
   * Directions in 3D:
   *
   * Bottom-left-front corner is (0,0,0)
   *
   * Simple directions
   *
   *       Z+   Y+
   *       5   3
   *       | /
   *  0 -- * -- 1 X+
   *     / |
   *   2   4
   *
   * Left side: (-1)
   *
   * Z
   * ^
   * +---+---+---+
   * |11 |12 |13 |
   * +---+---+---+
   * | 9 | 0 |10 |
   * +---+---+---+
   * | 6 | 7 | 8 |
   * +---+---+---+ --> Y
   *
   * Right side: (+1)
   *
   * Z
   * ^
   * +---+---+---+
   * |19 |20 |21 |
   * +---+---+---+
   * |17 | 1 |18 |
   * +---+---+---+
   * |14 |15 |16 |
   * +---+---+---+ --> Y
   *
   * Front side: (-M)
   *
   * Z
   * ^
   * +---+---+---+
   * |11 |23 |19 |
   * +---+---+---+
   * | 9 | 2 |17 |
   * +---+---+---+
   * | 6 |22 |14 |
   * +---+---+---+ --> X
   *
   * Back side: (+M)
   *
   * Z
   * ^
   * +---+---+---+
   * |13 |25 |21 |
   * +---+---+---+
   * |10 | 3 |18 |
   * +---+---+---+
   * | 8 |24 |16 |
   * +---+---+---+ --> X
   *
   * Bottom side: (-P)
   *
   * Y
   * ^
   * +---+---+---+
   * | 8 |22 |19 |
   * +---+---+---+
   * | 7 | 4 |17 |
   * +---+---+---+
   * | 6 |24 |14 |
   * +---+---+---+ --> X
   *
   * Top side: (+P)
   *
   * Y
   * ^
   * +---+---+---+
   * |13 |25 |21 |
   * +---+---+---+
   * |12 | 5 |20 |
   * +---+---+---+
   * |11 |23 |19 |
   * +---+---+---+ --> X
   *
   */

  // Input data
  NUM_DIRS _ndir;      ///< Number of directions for the neighborhood

  // Internal structures
  int32_t _M = 0;                                            ///< X dimension
  int32_t _N = 0;                                            ///< Y dimension
  int32_t _P = 0;                                            ///< Z dimension
  static const std::array<Point3i, 26> _shifts;              ///< Shifts of the various directions
  std::array<Point3i, 26> _borders;                          ///< Border of the image for various positions

  mutable QString _errorString;

  Sieve3D(const HVecUS& data,
          const Point3u& size,
          FILTER_TYPE first_filter,
          NUM_DIRS ndir,
          SieveProgress progress);

  /**
   * Write the resulting image to the output
   */
  bool writeTo(HVecUS& output) const;

  /**
   * Returns true if an error occured
   */
  bool error() const { return not _errorString.isEmpty(); }

  /**
   * Get the text of the last error
   */
  const QString& errorString() const { return _errorString; }

private:

  int32_t offX(intptr_t off) const
  {
    return off % _M;
  }

  int32_t offY(intptr_t off) const
  {
    return (off / _M) % _N;
  }

  int32_t offZ(intptr_t off) const
  {
    return off / (_M * _N);
  }


  Point3i offXYZ(intptr_t off) const
  {
    return Point3i{offX(off), offY(off), offZ(off)};
  }

  /**
   * Initial preparation of the mesh
   */
  bool prepareMesh(FILTER_TYPE first_filter, SieveProgress& progress);

  /**
   * Find the connected sets in the initial, prepared, mesh
   */
  bool findConnectedSets(SieveProgress& progress);

  /**
   * Process the graph to find adjacency and extrema
   *
   * The root of each zone is given its area as new root
   */
  bool findAdjacencyAndExtrema(SieveProgress& progress);
};

} // namespace sieve
} // namespace process
} // namespace lgx

#endif // SIEVE2D_HPP

