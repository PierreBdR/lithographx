/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "PackageManager.hpp"

#include <LGXCompiled.hpp>
#include <LGXVersion.hpp>

#include <Defer.hpp>
#include <Dependency.hpp>
#include <Information.hpp>
#include <SystemDirs.hpp>

#include <exception>
#include <QRegularExpression>
#include <QSettings>

namespace lgx {

Package::Package()
  : _depend(std::make_unique<DependAll>())
{ }

Package::Package(const QString& name, const lgx::util::Version& version, bool user)
  : _name(name)
  , _version(version)
  , _OS(LithoGraphX_OS)
  , _depend(std::make_unique<DependAll>())
  , _user(user)
{
}

Package::Package(const Package& copy)
  : _name(copy._name)
  , _description(copy._description)
  , _OS(copy._OS)
  , _source(copy._source)
  , _version(copy._version)
  , _depend(clone(*copy._depend))
  , _user(copy._user)
  , _files(copy._files)
{
}

Package& Package::operator=(const Package& copy)
{
  _name = copy._name;
  _description = copy._description;
  _OS = copy._OS;
  _source = copy._source;
  _version = copy._version;
  _depend = clone(*copy._depend);
  _user = copy._user;
  _files = copy._files;
  return *this;
}

bool Package::valid() const
{
  return not _name.isEmpty() and _version.valid();
}

bool Package::writeDescription(const QString& filename) const
{
  QSettings settings(filename, QSettings::IniFormat);
  settings.beginGroup("Package");
  settings.setValue("Name", name());
  settings.setValue("Version", version().toString());
  settings.setValue("Dependencies", dependencies()->toString());
  settings.setValue("Description", description());
  if(not OS().isEmpty())
    settings.setValue("OS", OS());
  if(not sourcePackage().isEmpty())
    settings.setValue("SourcePackage", sourcePackage());
  settings.endGroup();
  return settings.status() == QSettings::NoError;
}

Package Package::fromDescription(const QString& filename, bool user)
{
  QSettings settings(filename, QSettings::IniFormat);
  settings.beginGroup("Package");
  auto name = settings.value("Name").toString();
  if(name.isEmpty())
    return {};
  auto verStr = settings.value("Version").toString();
  auto ver = util::Version(verStr);
  if(not ver)
    return {};
  auto desc = settings.value("Description", "").toString();
  auto depStr = settings.value("Dependencies", "").toString();
  Package pkg(name, ver, user);
  pkg.setDescription(desc);
  if(settings.contains("OS"))
    pkg.setOS(settings.value("OS", "").toString());
  if(not depStr.isEmpty()) {
    try {
      pkg.setDependencies(depStr);
    } catch(std::exception& ex) {
      Information::err << "Error parsing dependencies:\n" << ex.what() << endl;
      return {};
    }
  }
  auto src = settings.value("SourcePackage", "").toString();
  pkg.setSourcePackage(src);
  settings.endGroup();
  return pkg;
}

void Package::setOS(QString os)
{
  _OS = os;
}

void Package::setDescription(QString txt)
{
  _description = txt;
}

void Package::setSourcePackage(const QString& s)
{
  _source = s;
}

QStringList Package::files() const
{
  if(_files.isEmpty()) {
    QString filesLoc = QString("%1_manifest.txt").arg(_name);
    if(_user)
      filesLoc = util::userPackagesDir().filePath(filesLoc);
    else
      filesLoc = util::systemPackagesDir().filePath(filesLoc);
    QFile file(filesLoc);
    if(not file.open(QIODevice::ReadOnly | QIODevice::Text))
      return {}; // System package ... no installed files
    QTextStream ts(&file);
    _files = ts.readAll().trimmed().split("\n", QString::SkipEmptyParts);
    auto root = util::installDir();
    for(auto& file: _files) {
      if(QDir::isRelativePath(file))
        file = root.absoluteFilePath(file);
    }
  }
  return _files;
}

QStringList Package::libraries() const
{
  auto files = this->files();
  auto prefix = (_user ? util::userLibsDir() : util::libsDir()).absolutePath();
  auto suffixes = QStringList{
#ifdef WIN32
    "lib", "a"
#else
    "so", "a"
#endif
  };
  QStringList libs;
  for(const auto& f: files) {
    QFileInfo fi(f);
    if(f.startsWith(prefix) and suffixes.contains(fi.suffix()))
      libs << f;
  }
  return libs;
}

const QString& Package::description() const
{
  return _description;
}

bool Package::canInstall(const PackageManager& manager, bool testSelf) const
{
  if(testSelf and manager.hasPackage(_name))
    return false;
  return _depend->check(manager, isUserPackage());
}

const Dependency* Package::dependencies() const
{
  return _depend.get();
}

void Package::setDependencies(QString dep)
{
  _depend = parseDependency(dep);
}

void Package::setDependencies(std::unique_ptr<Dependency>&& dep)
{
  _depend = std::move(dep);
}

void Package::setDependencies(std::shared_ptr<Dependency> dep)
{
  _depend = clone(*dep);
}

class PackageManagerPrivate
{
public:
  std::unordered_map<QString, std::unique_ptr<Package>> packages;
};

PackageManager::PackageManager()
{
  loadPackages();
}

PackageManager::~PackageManager()
{ }

void PackageManager::loadPackages()
{
  // reset the private pointer
  _p = std::make_unique<PackageManagerPrivate>();
  auto lgxVersion = util::Version::fromInt(PROCESS_VERSION);
  // Add LithoGraphX itself
  {
    QString name = "LithoGraphX";
    auto pkg = std::make_unique<Package>(name, lgxVersion, false);
    pkg->setDescription("Package representing LithoGraphX itself. Mostly used for dependencies.");
    _p->packages[name] = std::move(pkg);
  }
  auto nb_compiled_modules = compiledModules.size();
  QString dep = QString("LithoGraphX (== %1)").arg(lgxVersion.toString());
  for(int i = 0 ; i < nb_compiled_modules ; ++i) {
    QString name = compiledModules.at(i);
    auto version = compiledModulesVersion.at(i);
    auto desc = compiledModulesDescriptions.at(i);
    auto package = std::make_unique<Package>(name, version, false);
    package->setDescription(desc);
    package->setDependencies(dep);
    _p->packages[name] = std::move(package);
  }

  loadPackages(false);
  loadPackages(true);
}

void PackageManager::loadPackages(bool user)
{
  auto base = (user ? util::userPackagesDir() : util::systemPackagesDir());
  if(not base.exists())
    return;

  auto pkgs = base.filePath("packageList.ini");
  if(not QFile(pkgs).exists())
    return;

  QSettings settings(pkgs, QSettings::IniFormat);
  int nbPackages = settings.beginReadArray("Packages");
  for(int i = 0 ; i < nbPackages ; ++i) {
    settings.setArrayIndex(i);
    QString name = settings.value("Name").toString();
    try {
      auto version = lgx::util::Version(settings.value("Version").toString());
      auto depStr = settings.value("Dependencies", "").toString();
      auto desc = settings.value("Description", "").toString();
      auto ver = util::Version(settings.value("Version", "").toString());
      auto src = settings.value("SourcePackage", "").toString();

      auto package = std::make_unique<Package>(name, version, user);
      package->setDescription(desc);
      package->setSourcePackage(src);
      try {
        package->setDependencies(depStr);
      } catch(std::exception& ex) {
        Information::err << "Error parsing dependencies of package '" << name << "':\n" << ex.what() << endl;
      }
      if(_p->packages.find(name) == _p->packages.end())
        _p->packages[name] = std::move(package);
    } catch(std::exception& ex) {
      Information::err << "Error loading package '" << name << "': " << ex.what() << endl;
    }
  }
}

bool PackageManager::hasPackage(QString name) const
{
  return _p->packages.find(name) != _p->packages.end();
}

bool PackageManager::hasUserPackage(QString name) const
{
  auto found = _p->packages.find(name);
  return found != _p->packages.end() and found->second->isUserPackage();
}

bool PackageManager::hasSystemPackage(QString name) const
{
  auto found = _p->packages.find(name);
  return found != _p->packages.end() and not found->second->isUserPackage();
}

const Package* PackageManager::package(QString name) const
{
  auto found = _p->packages.find(name);
  if(found == _p->packages.end())
    return nullptr;
  return found->second.get();
}

bool PackageManager::check(const QString& name) const
{
  auto pkg = package(name);
  if(pkg)
    return pkg->dependencies()->check(*this, pkg->isUserPackage());
  return false;
}

QStringList PackageManager::packages() const
{
  QStringList result;
  for(const auto& p: _p->packages)
    result << p.first;
  return result;
}

QStringList PackageManager::userPackages() const
{
  QStringList result;
  for(const auto& p: _p->packages)
    if(p.second->isUserPackage())
      result << p.first;
  return result;
}

QStringList PackageManager::systemPackages() const
{
  QStringList result;
  for(const auto& p: _p->packages)
    if(not p.second->isUserPackage())
      result << p.first;
  return result;
}

namespace {
struct RevDepManager : public PackageManager
{
  RevDepManager(const QString& t, const PackageManager& other)
    : PackageManager(other)
    , tested(t)
  { }

  bool hasPackage(QString name) const override
  {
    if(name == tested) return false;
    return PackageManager::hasPackage(name);
  }

  bool hasUserPackage(QString name) const override
  {
    if(name == tested) return false;
    return PackageManager::hasUserPackage(name);
  }

  bool hasSystemPackage(QString name) const override
  {
    if(name == tested) return false;
    return PackageManager::hasSystemPackage(name);
  }

  const Package* package(QString name) const override
  {
    if(name == tested) return nullptr;
    return PackageManager::package(name);
  }

  QString tested;
};

struct VersionTestManager : public PackageManager
{
  VersionTestManager(const QString& t, const util::Version& new_version, const PackageManager& other)
    : PackageManager(other)
    , tested(t)
  {
    auto old_pkg = PackageManager::package(tested);
    if(old_pkg) {
      pkg = Package(tested, new_version, old_pkg->isUserPackage());
      pkg.setDependencies(clone(*old_pkg->dependencies()));
      pkg.setDescription(old_pkg->description());
    }
  }

  const Package* package(QString name) const override
  {
    if(name == tested)
      return &pkg;
    return PackageManager::package(name);
  }

  QString tested;
  Package pkg;
};

}

std::pair<bool, QStringList> PackageManager::canInstall(const Package& pkg) const
{
  if(not pkg.canInstall(*this, false))
    return {false, {}};
  if(hasPackage(pkg.name())) {
    auto lst = testVersionChange(pkg.name(), pkg.version());
    if(not lst.isEmpty())
      return {false, lst};
  }
  return {true, {}};
}

std::pair<bool, QStringList> PackageManager::canUninstall(const QString& pkgName) const
{
  if(not hasPackage(pkgName))
    return {false, {}};
  // If the package is already broken, we can always uninstall
  if(not check(pkgName))
    return {true, {}};
  auto lst = reverseDependencies(pkgName);
  if(not lst.isEmpty())
    return {false, lst};
  return {true, {}};
}

QStringList PackageManager::reverseDependencies(const QString& name) const
{
  RevDepManager man(name, *this);
  QStringList result;
  for(const auto& pkgName: packages()) {
    if(pkgName != name and not man.check(pkgName))
      result << pkgName;
  }
  return result;
}

QStringList PackageManager::testVersionChange(const QString& name, const util::Version& new_version) const
{
  VersionTestManager man(name, new_version, *this);
  QStringList result;
  for(const auto& pkgName: packages()) {
    if(pkgName != name and not man.check(pkgName))
      result << pkgName;
  }
  return result;
}

}
