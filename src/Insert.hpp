/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef INSERT_HPP
#define INSERT_HPP
/**
 * \file Insert.hpp
 *
 * Defines the algorithms::Insert class template
 */
#include <LGXConfig.hpp>

#include <Information.hpp>
#include <StaticAssert.hpp>
#include <VVGraph.hpp>

#include <utility>

namespace lgx {
namespace util {

/**
 * \class Insert Insert.hpp <Insert.hpp>
 * \brief Insert a new vertex on an edge.
 * \param a an existing vertex
 * \param b an exsisting vertex
 * \param S the vv graph to edit
 *
 * This function creates a new vertex and inserts it between two existing
 * vertices by replacing it into the existing neighbourhoods.  If the vertices
 * have no relation or an assymmetric relation, a warning is printed to stderr
 * and an empty vertex is returned.
 *
 * Example:
 * \code
 * vvgraph S;
 * vertex v1, v2;
 * S.insert(v1);
 * S.insert(v2);
 * S.insertEdge(v1,v2);
 * S.insertEdge(v2,v1);
 * Insert<vvgraph> insert;
 * vertex v = insert(v1, v2, S);
 * \endcode
 *
 * The pre-condition is that edges (v1,v2) and (v2,v1) exist.
 *
 * The post-condition is that v replaced v2 in the neighborhood of v1 and v1
 * in the neighborhood of v2, keeping the double connections.
 */
template <class vvgraph, bool do_checks = true>
class Insert
#ifdef JUST_FOR_DOXYGEN
{
};
#else
;
#endif

template <class vvgraph> class Insert<vvgraph, true> {
public:
  typedef typename vvgraph::vertex_t vertex;

  vertex operator()(const vertex& a, const vertex& b, vvgraph& S) const
  {
    if(a.isNull() || b.isNull()) {
      return vertex(0);
    }

    unsigned int check = 0;
    if(S.edge(a, b))
      check++;
    if(S.edge(b, a))
      check++;

    switch(check) {
    case 0:
      Information::err << "Warning: Attempt to insert a vertex between vertices that have no relation." << endl;
      return vertex(0);
      break;
    case 1:
      Information::err << "Warning: Attempt to insert a vertex between vertices that have an assymetric relation."
                       << endl;
      return vertex(0);
      break;
    default:
      break;
    }

    vertex x;
    S.insert(x);

    S.replace(a, b, x);
    S.replace(b, a, x);

    S.insertEdge(x, a);
    S.insertEdge(x, b);

    return x;
  }
};

template <class vvgraph> class Insert<vvgraph, false> {
public:
  typedef typename vvgraph::vertex_t vertex;

  const vertex& operator()(const vertex& a, const vertex& b, vvgraph& S) const
  {
    vertex x;
    const vertex& r = *S.insert(x);

    S.replace(a, b, x);
    S.replace(b, a, x);

    S.insertEdge(x, a);
    S.insertEdge(x, b);

    return r;
  }
};

template <GRAPH_TEMPLATE>
const typename graph::VVGraph<GRAPH_ARGS>::vertex_t& insert(const typename graph::VVGraph<GRAPH_ARGS>::vertex_t& a,
                                                            const typename graph::VVGraph<GRAPH_ARGS>::vertex_t& b,
                                                            graph::VVGraph<GRAPH_ARGS>&S)
{
  typedef Insert<graph::VVGraph<GRAPH_ARGS> > Insert;
  static const Insert fct = Insert();
  return fct(a, b, S);
}

/**
 * Splice \c nv after \c ref in \c v if ref is not null. Otherwise just
 * insert the edge (v,nv) in S.
 */
template <class Graph>
typename Graph::edge_t insertAfter(const typename Graph::vertex_t& v, const typename Graph::vertex_t& ref,
                                   const typename Graph::vertex_t& nv, Graph& S)
{
  if(ref)
    return S.spliceAfter(v, ref, nv);
  return S.insertEdge(v, nv);
}

/**
 * Splice \c nv before \c ref in \c v if ref is not null. Otherwise just
 * insert the edge (v,nv) in S.
 */
template <class Graph>
typename Graph::edge_t insertBefore(const typename Graph::vertex_t& v, const typename Graph::vertex_t& ref,
                                    const typename Graph::vertex_t& nv, Graph& S)
{
  if(ref)
    return S.spliceBefore(v, ref, nv);
  return S.insertEdge(v, nv);
}
} // namespace util
} // namespace lgx
#endif // INSERT_HPP
