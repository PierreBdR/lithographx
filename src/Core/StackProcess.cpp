/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "StackProcess.hpp"
#include "Information.hpp"
#include "Progress.hpp"
#include "cuda/CudaExport.hpp"

#include <chrono>
#include <QFileDialog>
#include <algorithm>
#include <cfloat>

namespace lgx {
namespace process {
// Get triangle list for cuda operations
bool AutoScaleStack::operator()(const Store* input, Store* output)
{
  unsigned int low = 65536, high = 0;
  const HVecUS& idata = input->data();

  for(size_t i = 0; i < idata.size(); ++i) {
    if(idata[i] < low)
      low = idata[i];
    if(idata[i] > high)
      high = idata[i];
  }

  unsigned int delta = high - low;
  if(delta == 0)
    delta = 65535u;
  HVecUS& data = output->data();
  output->copyMetaData(input);
  if(delta == 65535u) {
    if(output != input)
      data = idata; // Nothing to scale
  } else {
    if(output != input) {
      for(size_t i = 0; i < idata.size(); ++i) {
        data[i] = (((unsigned int)(idata[i]) - low) * 65535u) / delta;
      }
    } else {
      for(size_t i = 0; i < data.size(); ++i) {
        data[i] = (((unsigned int)(data[i]) - low) * 65535u) / delta;
      }
    }
    auto fct = output->transferFct();
    fct.adjust(0., 1.);
    output->setTransferFct(fct);
  }
  output->changed();
  return true;
}

REGISTER_STACK_PROCESS(AutoScaleStack);

bool ApplyTransferFunction::operator()(Store* input, Store* output, float red, float green, float blue, float alpha)
{
  TransferFunction::Colorf minColor, maxColor;
  const TransferFunction& fct = input->transferFct();
  float vmin = HUGE_VAL, vmax = -HUGE_VAL;
  if(input != output)
    output->allocate();
  HVecUS& data = input->data();
  HVecUS& outputData = output->data();
  float sum = red + green + blue + alpha;
  red /= sum;
  green /= sum;
  blue /= sum;
  alpha /= sum;
  for(size_t i = 0; i < data.size(); i++) {
    float f = float(data[i]) / 65535.0f;
    TransferFunction::Colorf c = fct.rgba(f);
    float value = red * c.r() + green * c.g() + blue * c.b() + alpha * c.a();
    if(value < vmin) {
      vmin = value;
      minColor = c;
    }
    if(value > vmax) {
      vmax = value;
      maxColor = c;
    }
    int val = int(roundf(value * 65535));
    outputData[i] = (val > 65535 ? 65535 : val);
  }
  TransferFunction new_fct;
  new_fct.add_rgba_point(0, minColor);
  new_fct.add_rgba_point(1, maxColor);
  output->copyMetaData(input);
  output->setTransferFct(new_fct);
  output->changed();
  return true;
}

REGISTER_STACK_PROCESS(ApplyTransferFunction);

typedef std::pair<Point3u, float> PixPair;
static bool pixCompare(const PixPair& i, const PixPair& j) {
  return (i.second > j.second);
}
bool BlobDetect::operator()(const Store* input, Store* output, bool watershed, uint startlabel)
{
  uint label = startlabel;
  HVecUS bdata = input->data();
  int id = input->stack()->userId();
  const Stack* stack = input->stack();

  // Sort pixels
  Progress progress(QString("Stack %1 Blob Detect, sorting pixels").arg(id), 3);
  if(!progress.advance(1))
    userCancel();

  Point3u size = stack->size();
  size_t SizeXYZ = size_t(size.x()) * size.y() * size.z();
  std::vector<PixPair> Pix(SizeXYZ);
  uint i = 0;
  for(uint z = 0; z < size.z(); z++)
    for(uint y = 0; y < size.y(); y++)
      for(uint x = 0; x < size.x(); x++) {
        Pix[i] = PixPair(Point3u(x, y, z), bdata[i]);
        i++;
      }
  if(!progress.advance(2))
    userCancel();
  std::sort(Pix.begin(), Pix.end(), pixCompare);

  // Prepare result area
  output->reset();
  HVecUS& workData = output->data();

  progress.restart(QString("Stack %1 Blob Detect, finding regions").arg(id), bdata.size());
  for(size_t i = 0; i < bdata.size(); i++) {
    // Find pixel offset
    const Point3u& p = Pix[i].first;
    size_t off = stack->offset(p.x(), p.y(), p.z());
    if(bdata[off] == 0)
      continue;

    int xs = int(p.x() - 1);
    int xe = int(p.x() + 1);
    if(xs < 0)
      xs = 0;
    if(xe >= (int)size.x())
      xe = (int)size.x() - 1;

    int ys = int(p.y() - 1);
    int ye = int(p.y() + 1);
    if(ys < 0)
      ys = 0;
    if(ye >= (int)size.y())
      ye = (int)size.y() - 1;

    int zs = int(p.z() - 1);
    int ze = int(p.z() + 1);
    if(zs < 0)
      zs = 0;
    if(ze >= (int)size.z())
      ze = (int)size.z() - 1;

    bool hashigher = false;
    bool isback = false;
    int labcount = 0, lastlab = 0;
    for(int x = xs; x <= xe; x++)
      for(int y = ys; y <= ye; y++)
        for(int z = zs; z <= ze; z++) {
          if(x == (int)p.x() and y == (int)p.y() and z == (int)p.z())
            continue;
          size_t nboff = stack->offset(x, y, z);
          if(Pix[i].second <= bdata[nboff]) {
            hashigher = true;
            int w = workData[nboff];
            if(!watershed and w == 0xFFFF)
              isback = true;
            else if(w > 0 and w != lastlab) {
              labcount++;
              lastlab = w;
            }
          }
        }
    if(!hashigher and labcount == 0)
      workData[off] = label++;
    else if(isback)
      workData[off] = 0xFFFF;
    else if(labcount == 1)
      workData[off] = lastlab;
    else
      workData[off] = 0xFFFF;
    if(label == 0xFFFF)
      throw(QString("Blob Detect:too many labels"));
    if(i % 100000 == 0) {
      if(!progress.advance(i))
        userCancel();
      updateState();
      updateViewer();
    }
  }
  // Clear all the background pixels
  for(size_t i = 0; i < bdata.size(); i++)
    if(workData[i] == 0xFFFF)
      workData[i] = 0;

  output->copyMetaData(input);
  output->changed();

  SETSTATUS("Stack " << id << " " << label - startlabel << " blobs found");
  return true;
}

REGISTER_STACK_PROCESS(BlobDetect);

bool ClearWorkStack::operator()(Stack* stack, uint fillval)
{
  Store* work = stack->work();
  work->allocate();
  if(fillval > 0xFFFF)
    fillval = 0xFFFF;
  HVecUS& data = work->data();
  std::fill_n(data.begin(), data.size(), ushort(fillval));
  work->changed();
  work->setFile("");
  return true;
}

REGISTER_STACK_PROCESS(ClearWorkStack);

bool ClearMainStack::operator()(Stack* stack, uint fillval)
{
  Store* main = stack->main();
  main->allocate();
  if(fillval > 0xFFFF)
    fillval = 0xFFFF;
  HVecUS& data = main->data();
  std::fill_n(data.begin(), data.size(), ushort(fillval));
  main->changed();
  main->setFile("");
  return true;
}

REGISTER_STACK_PROCESS(ClearMainStack);

bool StackRelabel::operator()(Stack* stack, const Store* store, Store* output, int start, int step)
{
  if(step < 1)
    step = 1;
  if(start < 1)
    start = 1;
  Progress progress(QString("Relabeling current stack"), store->size());
  std::unordered_map<int, int> relabel_map;
  int prog = store->size() / 100;
  if(prog < 1) prog = 1;
  const HVecUS& data = store->data();
  HVecUS& odata = output->data();
  ushort label = 1;
  for(size_t i = 0; i < data.size(); ++i) {
    if(data[i] > 0u) {
      std::unordered_map<int, int>::iterator found = relabel_map.find(data[i]);
      if(found != relabel_map.end())
        odata[i] = found->second;
      else {
        relabel_map[data[i]] = label;
        odata[i] = label;
        label++;
      }
    }
    if((i % prog) == 0 and !progress.advance(i))
      userCancel();
  }
  // Find a random arangement of labels
  std::vector<ushort> labels(label);
  for(ushort i = 0; i < (ushort)labels.size(); ++i)
    labels[i] = ushort(start + i * step);
  ushort last = labels.back();
  // Force reseeding of random number generator
  unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
  std::shuffle(labels.begin(), labels.end(), std::default_random_engine(seed));

#pragma omp parallel for
  for(size_t i = 0; i < data.size(); ++i) {
    if(odata[i] > 0u) {
      odata[i] = labels[odata[i] - 1];
    }
  }

  if(!progress.advance(data.size()))
    userCancel();
  output->changed();
  output->copyMetaData(store);
  stack->setLabel(last);
  Information::out << "Next label = " << stack->viewLabel() << endl;
  SETSTATUS(QString("Total number of labels created = %1. Last label = %2").arg(relabel_map.size()).arg(last));
  return true;
}

REGISTER_STACK_PROCESS(StackRelabel);

bool SaveTransform::initialize(ParmList& parms, QWidget* parent)
{
  QString filename = parms[0].toString();
  if(filename.isEmpty())
    filename = QFileDialog::getSaveFileName(parent, "Choose transform file to save", QDir::currentPath(),
                                            "Text files (*.txt)");
  if(filename.isEmpty())
    return false;
  if(!filename.endsWith(".txt", Qt::CaseInsensitive))
    filename += ".txt";
  parms[0] = filename;
  return true;
}

bool SaveTransform::operator()(Stack* stack, const QString& filename)
{
  QFile file(filename);
  if(!file.open(QIODevice::WriteOnly)) {
    setErrorMessage(QString("File '%1' cannot be opened for writing").arg(filename));
    return false;
  }
  QTextStream out(&file);

  // Write matrix to file
  Matrix4d m(stack->getFrame().worldMatrix(), util::GL_STYLE);
  out << m << endl;
  file.close();

  SETSTATUS(QString("Transform saved to file: %1").arg(filename));
  return true;
}
REGISTER_STACK_PROCESS(SaveTransform);

bool TransformStack::operator()(const ParmList& parms) {
  auto trans = parms[0].toString();
  auto transform = Matrix4d::identity();
  if(not trans.isEmpty()) {
    auto fields = trans.split(' ');
    double values[16];
    if(fields.size() != 16)
      return setErrorMessage("The Transform parameter must have 16 values.");
    for(int i = 0 ; i < 16 ; ++i) {
      bool ok;
      values[i] = fields[i].toDouble(&ok);
      if(not ok)
        return setErrorMessage(QString("The element %1 is not a floating point value.").arg(i));
    }
    transform = Matrix4d(values);
  }
  return (*this)(currentStack(), transform, parmToBool(parms[1]));
}

bool TransformStack::operator()(Stack* stack, const Matrix4d& transform, bool combined)
{
  auto m3 = Matrix3d(transform);
  double nsq = (m3[0] ^ m3[1]) * m3[2];
  if(fabs(nsq-1) > 1e-5) {
    return setErrorMessage("Error, the determinant of the rotation part is not 1.");
  }
  Matrix4d cur;
  if(combined) {
    cur = Matrix4d(stack->showTrans() ? stack->trans().matrix() : stack->frame().matrix());
    cur *= transform;
  } else
    cur = transform;

  if(stack->showTrans())
    stack->trans().setFromMatrix(cur.c_data());
  else
    stack->frame().setFromMatrix(cur.c_data());
  return true;
}

REGISTER_STACK_PROCESS(TransformStack);

bool LoadTransform::initialize(ParmList& parms, QWidget* parent)
{
  QString filename = parms[0].toString();
  if(filename.isEmpty())
    filename = QFileDialog::getOpenFileName(parent, "Choose transform file to load", QDir::currentPath(),
                                            "Text files (*.txt)");
  if(filename.isEmpty())
    return false;
  if(!filename.endsWith(".txt", Qt::CaseInsensitive))
    filename += ".txt";
  parms[0] = filename;
  return true;
}

bool LoadTransform::operator()(Stack* stack, const QString& filename)
{
  QFile file(filename);
  if(!file.open(QIODevice::ReadOnly)) {
    setErrorMessage(QString("File '%1' cannot be opened for reading").arg(filename));
    return false;
  }
  QTextStream out(&file);

  // Read in transform
  Matrix4d m;
  QTextStream in(&file);
  in >> m;
  file.close();

  // Set frame or transform
  if(stack->showTrans())
    stack->trans().setFromMatrix(m.c_data());
  else
    stack->frame().setFromMatrix(m.c_data());

  SETSTATUS(QString("Transform loaded from file: %1").arg(filename));
  return true;
}
REGISTER_STACK_PROCESS(LoadTransform);
} // namespace process
} // namespace lgx
