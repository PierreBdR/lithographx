/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include <Version.hpp>

#include <QStringList>

namespace lgx {
namespace util {

bool Version::valid() const
{
  return (major < 255 and minor < 255 and patch < 255 and
          major >= 0 and (minor >= 0 or patch < 0));
}

Version::operator bool() const
{
  return valid();
}

Version::Version()
{ }

Version::Version(QString s)
{
  // First, remove anything after the first space
  QString ver = s.trimmed();
  int idx = ver.indexOf(' ');
  if(idx > 0)
      ver = ver.left(idx);
  QStringList fields = ver.split(".");
  if(fields.size() > 3)
    return;
  int maj, min = -1, pat = -1;
  bool ok;
  maj = fields[0].toUInt(&ok);
  if(not ok) return;
  if(fields.size() > 1) {
      min = fields[1].toUInt(&ok);
      if(not ok) return;
  }
  if(fields.size() > 2) {
      pat = fields[2].toUInt(&ok);
      if(not ok) return;
  }
  major = maj;
  minor = min;
  patch = pat;
}

QString Version::toString() const
{
  QString var;
  QString min = (minor >= 0 ? QString(".%1").arg(minor) : QString());
  QString pat = (patch >= 0 ? QString(".%1").arg(patch) : QString());
  return QString::number(major) + min + pat;
}

Version Version::fromInt(uint32_t ver)
{
  Version result;
  int patch = ver & 0xff;
  if(patch == 0xff)
    patch = -1;
  ver >>= 8;
  int minor = ver & 0xff;
  if(minor == 0xff)
    minor = -1;
  ver >>= 8;
  int major = ver & 0xff;
  if(major == 0xff)
    major = -1;
  return Version(major, minor, patch);
}

bool operator==(const Version& v1, const Version& v2)
{
  return (v1.major == v2.major and
          (v1.minor < 0 or v2.minor < 0 or
           (v1.minor == v2.minor and
            (v1.patch < 0 or v2.patch < 0 or v1.patch == v2.patch))));
}

bool operator!=(const Version& v1, const Version& v2)
{
  return not (v1 == v2);
}

bool operator>(const Version& v1, const Version& v2)
{
  return (v1.major > v2.major or
          (v1.major == v2.major and
           (v1.minor >= 0 and v2.minor >= 0 and
            (v1.minor > v2.minor or
             (v1.minor == v2.minor and
              (v1.patch >= 0 and v2.patch >= 0 and
               (v1.patch > v2.patch)))))));
}

bool operator<(const Version& v1, const Version& v2)
{
  return (v1.major < v2.major or
          (v1.major == v2.major and
           (v1.minor >= 0 and v2.minor >= 0 and
            (v1.minor < v2.minor or
             (v1.minor == v2.minor and
              (v1.patch >= 0 and v2.patch >= 0 and
               (v1.patch < v2.patch)))))));
}

bool operator>=(const Version& v1, const Version& v2)
{
  return not (v1 < v2);
}

bool operator<=(const Version& v1, const Version& v2)
{
  return not (v2 < v1);
}

const Version Version::invalid = Version();

} // namespace lgx
} // namespace util
