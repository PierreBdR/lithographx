/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef PLUGINLISTVIEW_HPP
#define PLUGINLISTVIEW_HPP

#include <LGXConfig.hpp>

#include <QTreeView>
#include <QWidget>

namespace lgx {
namespace gui {

class PluginListView : public QTreeView
{
  Q_OBJECT
public:
  PluginListView(QWidget* parent = nullptr);

protected:
  void dragMoveEvent(QDragMoveEvent *event);

private:
};

} // namespace lgx
} // namespace gui

#endif // PLUGINLISTVIEW_HPP
