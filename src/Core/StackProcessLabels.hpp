/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef STACKPROCESSLABELS_HPP
#define STACKPROCESSLABELS_HPP

#include <Process.hpp>

#include <Misc.hpp>
#include <Stack.hpp>

namespace lgx {

typedef util::Vector<3,bool> Point3b;

namespace process {
///\addtogroup StackProcess
///@{
/**
 * \class WatershedStack StackProcessLabels.hpp <StackProcessLabels.hpp>
 *
 * Compute the seeded watershed of the current stack using the CImg library.
 */
class LGXCORE_EXPORT WatershedStack : public StackProcess {
public:
  WatershedStack(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const ParmList&) override
  {
    if(!checkState().store(STORE_MAIN | STORE_VISIBLE | STORE_NON_LABEL).store(STORE_WORK | STORE_VISIBLE
                                                                               | STORE_LABEL))
      return false;
    Stack* stack = currentStack();
    Store* main = stack->main();
    Store* labels = stack->work();
    return (*this)(stack, main, labels);
  }

  bool operator()(Stack* stack, Store* main, Store* labels);

  QString folder() const override {
    return "Segmentation";
  }
  QString name() const override {
    return "Watershed3D";
  }
  QString description() const override {
    return "3D Watershed on the current labeled stack.";
  }
  QStringList parmNames() const override {
    return QStringList();
  }
  QStringList parmDescs() const override {
    return QStringList();
  }
  QIcon icon() const override {
    return QIcon(":/images/SegmentMesh.png");
  }
};

/**
 * \class ConsolidateRegions StackProcessLabels.hpp <StackProcessLabels.hpp>
 *
 * Try to merge segmented regions based on their contact area and the total voxel
 * intensity at their boundary.
 */
class LGXCORE_EXPORT ConsolidateRegions : public StackProcess {
public:
  ConsolidateRegions(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const ParmList& parms) override
  {
    if(!checkState().store(STORE_MAIN | STORE_VISIBLE | STORE_NON_LABEL).store(STORE_WORK | STORE_VISIBLE
                                                                               | STORE_LABEL))
      return false;
    Stack* stack = currentStack();
    Store* main = stack->main();
    Store* labels = stack->work();
    return (*this)(main, labels, parms[0].toUInt(), parms[1].toUInt());
  }

  bool operator()(Store* data, Store* labels, uint threshold, uint minvoxels);

  QString folder() const override {
    return "Segmentation";
  }
  QString name() const override {
    return "Consolidate Regions";
  }
  QString description() const override {
    return "Consilodate regions after watershed overseeding";
  }
  QStringList parmNames() const override
  {
    return QStringList() << "Threshold"
                         << "Min Voxels";
  }
  QStringList parmDescs() const override
  {
    return QStringList() << "Threshold"
                         << "Min Voxels";
  }
  ParmList parmDefaults() const override
  {
    return ParmList() << 5000
                      << 100;
  }
  QIcon icon() const override {
    return QIcon(":/images/Consolidate.png");
  }
};

/**
 * \class ConsolidateRegionsNormalized StackProcessLabels.hpp <StackProcessLabels.hpp>
 *
 * Variation on the ConsolidateRegions process.
 */
class LGXCORE_EXPORT ConsolidateRegionsNormalized : public StackProcess {
public:
  ConsolidateRegionsNormalized(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const ParmList& parms) override
  {
    if(!checkState().store(STORE_MAIN | STORE_VISIBLE | STORE_NON_LABEL).store(STORE_WORK | STORE_VISIBLE
                                                                               | STORE_LABEL))
      return false;
    Stack* stack = currentStack();
    Store* main = stack->main();
    Store* labels = stack->work();
    return (*this)(main, labels, parms[0].toFloat());
  }

  bool operator()(Store* data, Store* labels, float tolerance);

  QString folder() const override {
    return "Segmentation";
  }
  QString name() const override {
    return "Consolidate Regions Normalized";
  }
  QString description() const override {
    return "Consolidate regions with normalization (slower)";
  }
  QStringList parmNames() const override {
    return QStringList() << "Tolerance";
  }
  QStringList parmDescs() const override {
    return QStringList() << "Tolerance";
  }
  ParmList parmDefaults() const override {
    return ParmList() << 0.3;
  }
  QIcon icon() const override {
    return QIcon(":/images/Consolidate.png");
  }
};

/**
 * \class ThresholdLabelDelete StackProcessLabels.hpp <StackProcessLabels.hpp>
 *
 * Delete labels that have too few or too many voxels.
 */
class LGXCORE_EXPORT ThresholdLabelDelete : public StackProcess {
public:
  ThresholdLabelDelete(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const ParmList& parms) override
  {
    if(!checkState().store(STORE_LABEL))
      return false;
    Stack* stack = currentStack();
    Store* input = stack->currentStore();
    Store* output = stack->work();
    if(!input)
      return false;
    bool res = (*this)(input, output, parms[0].toInt(), parms[1].toInt());
    if(res) {
      input->hide();
      output->show();
    }
    return res;
  }

  bool operator()(const Store* input, Store* labels, uint minVoxels, uint maxVoxels);

  QString folder() const override {
    return "Segmentation";
  }
  QString name() const override {
    return "Delete Labels by Threshold";
  }
  QString description() const override {
    return "Delete Labels above/below voxel thresholds";
  }
  QStringList parmNames() const override
  {
    return QStringList() << "Min voxels"
                         << "Max voxels";
  }
  QStringList parmDescs() const override
  {
    return QStringList() << "Min voxels"
                         << "Max voxels";
  }
  ParmList parmDefaults() const override
  {
    return ParmList() << 1000
                      << 0;
  }
  QIcon icon() const override {
    return QIcon(":/images/DeleteLabel.png");
  }
};

/**
 * \class LocalMaximaStack StackProcessLabels.hpp <StackProcessLabels.hpp>
 *
 * Find the local maxima of the current image
 *
 * \note If the algorithm finds more than 65535 maxima, it will fail as it
 * cannot number them all.
 */
class LGXCORE_EXPORT LocalMaximaStack : public StackProcess {
public:
  LocalMaximaStack(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const ParmList& parms) override
  {
    if(!checkState().store(STORE_NON_LABEL))
      return false;
    Stack* stack = currentStack();
    Store* input = stack->currentStore();
    Store* labels = stack->work();
    if(!input)
      return false;
    Point3f radius = Point3u(parms[0].toFloat(), parms[1].toFloat(), parms[2].toFloat());
    uint lab = parms[3].toUInt();
    uint minColor = parms[4].toUInt();
    bool res = (*this)(input, labels, radius, lab, minColor);
    if(res) {
      input->hide();
      labels->show();
    }
    return res;
  }

  bool operator()(const Store* input, Store* labels, Point3f radius, uint startLabel, uint minColor);

  QString folder() const override {
    return "Segmentation";
  }
  QString name() const override {
    return "Local Maxima";
  }
  QString description() const override {
    return "Find local maxima and possibly number them";
  }
  QStringList parmNames() const override
  {
    return QStringList() << QString("X Radius (%1)").arg(UM) << QString("Y Radius (%1)").arg(UM)
                         << QString("Z Radius (%1)").arg(UM) << "Start Label"
                         << "Min Color";
  }
  QStringList parmDescs() const override
  {
    return QStringList() << QString("X Radius (%1)").arg(UM) << QString("Y Radius (%1)").arg(UM)
                         << QString("Z Radius (%1)").arg(UM) << "Start Label"
                         << "Min Color";
  }
  ParmList parmDefaults() const override
  {
    return ParmList() << 5.0
                      << 5.0
                      << 5.0
                      << 2
                      << 10000;
  }
  QIcon icon() const override {
    return QIcon(":/images/LocalMaxima.png");
  }
};

/**
 * \class FillLabelStack StackProcessLabels.hpp <StackProcessLabels.hpp>
 *
 * Replace one label by another one on the stack
 */
class LGXCORE_EXPORT FillLabelStack : public StackProcess {
public:
  FillLabelStack(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const ParmList& parms) override
  {
    if(!checkState().store(STORE_LABEL))
      return false;
    Stack* stack = currentStack();
    Store* input = stack->currentStore();
    Store* output = stack->work();
    if(!input)
      return false;
    bool res = (*this)(input, output, ushort(parms[0].toUInt()), ushort(parms[1].toUInt()));
    if(res) {
      input->hide();
      output->show();
    }
    return res;
  }

  bool operator()(const Store* input, Store* output, ushort filledLabel, ushort newLabel);

  QString folder() const override {
    return "Segmentation";
  }
  QString name() const override {
    return "Fill Label";
  }
  QString description() const override {
    return "Replace a label with another one";
  }
  QStringList parmNames() const override
  {
    return QStringList() << "Filled label"
                         << "New label";
  }
  QStringList parmDescs() const override
  {
    return QStringList() << "Filled label"
                         << "New label";
  }
  ParmList parmDefaults() const override
  {
    return ParmList() << 1000
                      << 0;
  }
  QIcon icon() const override {
    return QIcon(":/images/FillLabel.png");
  }
};

/**
 * \class EraseAtBorderStack StackProcessLabels.hpp <StackProcessLabels.hpp>
 *
 * Erase any label touching the border of the image.
 */
class LGXCORE_EXPORT EraseAtBorderStack : public StackProcess {
public:
  EraseAtBorderStack(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const ParmList& parms) override
  {
    if(!checkState().store(STORE_LABEL))
      return false;
    Stack* stack = currentStack();
    Store* input = stack->currentStore();
    Store* output = stack->work();
    if(!input)
      return false;
    Point3f dist = util::stringToPoint3f(parms[0].toString());
    if(std::isnan(dist.x()))
      return setErrorMessage("The 'Distance' parameter must be either a valid floating point value, "
                             "or 3 floating point values, separated by spaces.");
    Point3b axes;
    axes.x() = parmToBool(parms[1]);
    axes.y() = parmToBool(parms[2]);
    axes.z() = parmToBool(parms[3]);
    bool res = (*this)(input, output, dist, axes);
    if(res) {
      input->hide();
      output->show();
    }
    return res;
  }

  bool operator()(const Store* input, Store* output, const Point3f& dist, const Point3b& axes);

  QString folder() const override {
    return "Segmentation";
  }
  QString name() const override {
    return "Erase at Border";
  }
  QString description() const override {
    return "Erase any labelled region touching the border of the image";
  }
  QStringList parmNames() const override {
    return QStringList() << ("Distance (" + UM + ")")
                         << "X" << "Y" << "Z";
  }
  QStringList parmDescs() const override {
    return QStringList() << "Distance, in micrometer, from which the voxels should be to be erased."
                            "This can also be a 3D vector"
                         << "If true, erase parts touching the border along the X axis"
                         << "If true, erase parts touching the border along the Y axis"
                         << "If true, erase parts touching the border along the Z axis";
  }
  ParmList parmDefaults() const override {
    return ParmList() << 0 << true << true << true;
  }
  ParmChoiceMap parmChoice() const override {
    ParmChoiceMap map;
    map[1] = map[2] = map[3] = booleanChoice();
    return map;
  }
  QIcon icon() const override {
    return QIcon(":/images/EraseLabel.png");
  }
};

class LGXCORE_EXPORT MajorityFilter : public StackProcess {
public:
  MajorityFilter(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const ParmList& parms) override
  {
    if(!checkState().store(STORE_LABEL))
      return false;
    Stack* stack = currentStack();
    Store* input = stack->currentStore();
    Store* output = stack->work();
    if(!input)
      return false;

    bool ok;
    int nb_steps = parms[0].toInt(&ok);
    if(!ok)
      return setErrorMessage("Error the 'Steps' argument must be an integer number");
    bool res = (*this)(input, output, nb_steps);
    if(res) {
      input->hide();
      output->show();
    }
    return res;
  }

  bool operator()(const Store* input, Store* output, int nb_steps);

  QString folder() const override {
    return "Segmentation";
  }
  QString name() const override {
    return "Majority Filter";
  }
  QString description() const override
  {
    return "Replace all labels with the label that is the most present in its neighborhood.\n"
           "In case of tie with the original color, it doesn't change. Otherwise, the smaller label is chosen.";
  }
  QStringList parmNames() const override {
    return QStringList() << "Steps";
  }
  QStringList parmDescs() const override {
    return QStringList() << "Number of iteration of the algorithm.";
  }
  ParmList parmDefaults() const override {
    return ParmList() << 1;
  }
  QIcon icon() const override {
    return QIcon(":/images/FixCorners.png");
  }
};

class LGXCORE_EXPORT RemoveStackLabelsInShape : public StackProcess {
public:
  RemoveStackLabelsInShape(const StackProcess& process)
    : Process(process)
      , StackProcess(process)
  { }

  enum Shape
  {
    SPHERE,
    CUBE
  };

  bool operator()(const ParmList& parms) override
  {
    if(!checkState().store(STORE_LABEL))
      return false;
    Stack* stack = currentStack();
    Store* input = stack->currentStore();
    Store* output = stack->work();
    QString shape_str = parms[0].toString().toLower().trimmed();
    Shape shape;
    if(shape_str == "sphere")
      shape = SPHERE;
    else if(shape_str == "cube")
      shape = CUBE;
    else
      return setErrorMessage(QString("Error, unknown shape '%1', should be 'Sphere' or 'Cube'").arg(parms[0].toString()));
    Point3f center = util::stringToPoint3f(parms[2].toString());
    if(std::isnan(center.x()))
      return setErrorMessage("The center must be a valid 3D point");
    Point3f size = util::stringToPoint3f(parms[3].toString());
    if(std::isnan(size.x()))
      return setErrorMessage("The size must be a single number or a 3D vector");
    bool inside = parmToBool(parms[1]);
    bool globalCoordinates = parmToBool(parms[4]);
    return (*this)(input, output, shape, inside, center, size, globalCoordinates);
  }

  bool operator()(const Store* input, Store* output, Shape shape, bool inside,
                  Point3f center, Point3f size, bool globalCoordinates);

  QString folder() const override {
    return "Segmentation";
  }
  QString name() const override {
    return "Remove Labels In Shape";
  }
  QString description() const override
  {
    return "Remove labels whose centers are inside or outside a defined shape.";
  }
  QStringList parmNames() const override {
    return QStringList() << "Shape"               // 0
                         << "Inside"              // 1
                         << "Center"              // 2
                         << "Size"                // 3
                         << "Global Coordinates"; // 4
  }
  QStringList parmDescs() const override {
    return QStringList() << "Shape to consider"
                         << "If true, labels in the shape are removed, if false labels outside the shape are removed"
                         << "Center of the shape"
                         << "Size of the shape, either as a single number or three numbers (X, Y, Z)"
                         << "If true, the global coordinate system is used (e.g. including movement of the stack),"
                            " otherwise the local coordinate system is used";
  }
  ParmList parmDefaults() const override {
    return ParmList() << "Sphere" << false << "0 0 0" << 1. << true;
  }
  ParmChoiceMap parmChoice() const override {
    ParmChoiceMap map;
    map[0] = QStringList() << "Sphere" << "Cube";
    map[1] = map[4] = booleanChoice();
    return map;
  }
  QIcon icon() const override {
    return QIcon(":/images/FixCorners.png");
  }

};

///@}
} // namespace process
} // namespace lgx

#endif
