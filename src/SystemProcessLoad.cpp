/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "SystemProcessLoad.hpp"

#include "Algorithms.hpp"
#include "Dir.hpp"
#include "Forall.hpp"
#include "Image.hpp"
#include "Information.hpp"
#include "LGXViewer/qglviewer.h"
#include "Misc.hpp"
#include "Parms.hpp"
#include "PlyFile.hpp"
#include "Progress.hpp"
#include "Shapes.hpp"
#include "Thrust.hpp"

#include <unordered_map>

#include <string.h>
#include <CImg.h>
#include <QCheckBox>
#include <QDialog>
#include <QDoubleSpinBox>
#include <QInputDialog>
#include <QFile>
#include <QFileDialog>
#include <QFileInfo>
#include <QHash>
#include <QListWidget>
#include <QListWidgetItem>
#include <QMessageBox>
#include <QRegularExpression>

#include "ui_LoadMesh.h"
#include "ui_LoadStack.h"
#include "ui_MultiChannelOpenDlg.h"

#ifdef WIN32
#  define FileDialogOptions QFileDialog::DontUseNativeDialog
#else
#  define FileDialogOptions 0
#endif

using namespace qglviewer;
using namespace cimg_library;

namespace lgx {
namespace process {

namespace {
// Returns true if the mesh is a cell mesh
bool checkCells(Mesh* mesh)
{
  const vvgraph& S = mesh->graph();
  for(const vertex& v : S) {
    if(v->type == 'j') {
      char prev = S.prevTo(v, S.anyIn(v))->type;
      bool has_cell = false;
      for(const vertex& n : S.neighbors(v)) {
        if(n->type == 'c') {
          if(prev == 'c')
            return false;
          has_cell = true;
        }
        prev = n->type;
      }
      if(not has_cell)
        return false;
    } else if(v->type == 'c') {
      for(const vertex& n : S.neighbors(v))
        if(n->type != 'j')
          return false;
    } else
      return false;
  }
  return true;
}

/**
 * Transform a position in a mesh file to a position in the image reference system
 *
 * \param pos Position in the mesh file
 * \param transform If true, the transformation of the image is applied
 * \param mesh Mesh being used
 * \param scale If true, the position need to be scaled as it in the [-0.5,0.5] reference system
 */
Point3f realPos(Point3f pos, bool transform, const Mesh* mesh, bool scale = false)
{
  if(scale)
    pos = mesh->stack()->abstractToWorld(pos);
  if(transform)
    pos = Point3f(mesh->stack()->frame().coordinatesOf(Vec(pos)));
  return pos;
}

Point3f realVector(Point3f pos, bool transform, const Mesh* mesh, bool scale = false)
{
  if(scale)
    pos = mesh->stack()->abstractToWorld(pos);
  if(transform)
    pos = Point3f(mesh->stack()->frame().transformOf(Vec(pos)));
  return pos;
}

} // namespace


typedef std::unordered_map<int, vertex> vertex_map;

typedef CImg<ushort> CImgUS;

bool StackSwapBytes::operator()(const Store* input, Store* output)
{
  const HVecUS& idata = input->data();
  HVecUS& odata = output->data();
#pragma omp parallel for
  for(size_t i = 0; i < idata.size(); ++i) {
    ushort us = idata[i];
    odata[i] = ((us & 0x00ff) << 8) + ((us & 0xff00) >> 8);
  }

  output->copyMetaData(input);
  output->changed();
  return true;
}

struct StackHdr {
  uint hdrsz;
  uint xsz, ysz, zsz;
  float xum, yum, zum;
  uint datasz;

  StackHdr()
    : hdrsz(1024)
  {
  }
};

StackImportDlg::StackImportDlg(QWidget* p, Qt::WindowFlags f)
  : QDialog(p, f)
  , ui(new Ui::LoadStackDialog)
{
  ui->setupUi(this);
}

StackImportDlg::~StackImportDlg()
{ }

void StackImportDlg::setBrightness(float val)
{
  ui->Brightness->setValue(val);
}

float StackImportDlg::brightness() const
{
  return ui->Brightness->value();
}

void StackImportDlg::clearImageFiles()
{
  ui->ImageFiles->clear();
}

QStringList StackImportDlg::imageFiles() const
{
  QStringList result;
  for(int i = 0; i < ui->ImageFiles->count(); ++i)
    result << ui->ImageFiles->item(i)->text();
  return result;
}

void StackImportDlg::on_LoadProfile_clicked()
{
  LoadProfile();
}

void StackImportDlg::LoadProfile(QString filename)
{
  if(filename.isEmpty()) {
    filename = QFileDialog::getOpenFileName(this, QString("Select profile file to open"), filename,
                                            QString("Text files (*.txt)"), 0, FileDialogOptions);
    if(filename.isEmpty())
      return;
  }

  QStringList imageFiles;
  Point3u size;
  Point3f step = imageResolution();
  float bright= brightness();
  try {
    parseImageList(filename, size, step, bright, imageFiles);
  } catch(std::exception& ex) {
    Information::err << "Error parsing image list:" << ex.what() << endl;
    return;
  }

  setImageSize(size.x(), size.y(), size.z());
  ui->StepX->setValue(step.x());
  ui->StepY->setValue(step.y());
  ui->StepZ->setValue(step.z());
  ui->Brightness->setValue(bright);
  QListWidget* lst = ui->ImageFiles;
  lst->clear();
  for(const QString& s : imageFiles)
    lst->addItem(s);

  _loadedFile = filename;
}

void StackImportDlg::on_SaveProfile_clicked()
{
  // Write to file
  QString filename = QFileDialog::getSaveFileName(this, QString("Select file to save profile for "), _loadedFile,
                                                  QString("Text files (*.txt)"), 0, FileDialogOptions);
  if(filename.isEmpty())
    return;

  // Check ending, add suffix if required
  if(filename.right(4) != ".txt")
    filename.append(".txt");

  QFile file(filename);
  if(!file.open(QIODevice::WriteOnly | QIODevice::Text)) {
    SETSTATUS("loadStackSaveProfile::Error:Cannot open output file:" << filename);
    return;
  }
  QString pth = QFileInfo(filename).absolutePath();
  QString StackFile = util::stripCurrentDir(filename);
  //actingFile(filename);

  QTextStream out(&file);
  out << "[Stack]" << endl;
  out << "SizeX: " << imageSize.x() << endl;
  out << "SizeY: " << imageSize.y() << endl;
  out << "StepX: " << ui->StepX->value() << endl;
  out << "StepY: " << ui->StepY->value() << endl;
  out << "StepZ: " << ui->StepZ->value() << endl;
  out << "Brightness: " << ui->Brightness->value() << endl;
  QListWidget* lst = ui->ImageFiles;
  for(int i = 0; i < lst->count(); i++) {
    QListWidgetItem* item = lst->item(i);
    auto absPath = util::absoluteFilePath(item->text());
    out << "ImageFile: " << util::stripDir(pth, absPath) << endl;
  }
  file.close();
  _loadedFile = filename;
  SETSTATUS("Saving profile, file:" << filename);
}

void StackImportDlg::on_AddFiles_clicked()
{
  // RSS How to get a complete list from CImg? (there are over 100)
  QStringList files = QFileDialog::getOpenFileNames(this, QString("Select stack image files "), util::currentPath(),
                                                    QString("Any files (*.*)"), 0);
  if(files.empty())
    return;

  on_ImageFiles_filesDropped(files);
}

void StackImportDlg::on_ImageFiles_filesDropped(const QStringList& files)
{
  _loadedFile.clear();
  QListWidget* lst = ui->ImageFiles;
  if(lst->count() == 0) {
    QString first = files[0];
    ImageInfo image;
    try {
      image = getImageInfo(first);
      if(not image) {
        QMessageBox::critical(this, "Error reading image", QString("Error, cannot read image from '%1'").arg(first));
        return;
      }
    } catch(std::exception& ex) {
      QMessageBox::critical(this, "Error reading image", QString("Error reading image from file '%1':\n%2").arg(first).arg(ex.what()));
      return;
    } catch(...) {
      QMessageBox::critical(this, "Error reading image", QString("Unknown error reading image from file '%1'").arg(first));
      return;
    }
    image.step = imageResolution();
    SETSTATUS(QString("Loaded first image: size = %1x%2").arg(image.size.x()).arg(image.size.y()));
    setImageSize(image.size.x(), image.size.y(), 0);
    setImageResolution(image.step);
    if(image.depth < 16)
      setBrightness(256);
  }
  lst->addItems(files);
  setImageSize(imageSize.x(), imageSize.y(), lst->count());
}

Point3f StackImportDlg::imageResolution() {
  return Point3f(ui->StepX->value(), ui->StepY->value(), ui->StepZ->value());
}

void StackImportDlg::setImageResolution(Point3f step)
{
  ui->StepX->setValue(step.x());
  ui->StepY->setValue(step.y());
  ui->StepZ->setValue(step.z());

  Information::out << "step: " << ui->StepX->value() << ", " << ui->StepY->value()
    << ", " << ui->StepZ->value() << endl;
}

void StackImportDlg::setImageSize(Point3u size)
{
  imageSize = size;
  ui->SizeXY->setText(QString("(%1,%2,%3)").arg(imageSize.x()).arg(imageSize.y()).arg(imageSize.z()));
}

void StackImportDlg::on_RemoveFiles_clicked()
{
  _loadedFile.clear();
  QListWidget* lst = ui->ImageFiles;
  QList<QListWidgetItem*> items = lst->selectedItems();
  for(QList<QListWidgetItem*>::iterator i = items.begin(); i != items.end(); i++) {
    QListWidgetItem* item = lst->takeItem(lst->row(*i));
    delete item;
  }
  setImageSize(imageSize.x(), imageSize.y(), lst->count());
}

void StackImportDlg::on_FilterFiles_clicked()
{
  _loadedFile.clear();
  QListWidget* lst = ui->ImageFiles;
  QString text = ui->FilterText->text();

  QList<QListWidgetItem*> items = lst->findItems(".*", Qt::MatchRegExp);
  for(QList<QListWidgetItem*>::iterator i = items.begin(); i != items.end(); i++) {
    QString itemtext = (*i)->text();
    if(itemtext.indexOf(text) < 0) {
      QListWidgetItem* item = lst->takeItem(lst->row(*i));
      delete item;
    }
  }
}

void StackImportDlg::on_SortAscending_clicked()
{
  _loadedFile.clear();
  QListWidget* lst = ui->ImageFiles;
  lst->sortItems(Qt::AscendingOrder);
}

void StackImportDlg::on_SortDescending_clicked()
{
  _loadedFile.clear();
  QListWidget* lst = ui->ImageFiles;
  lst->sortItems(Qt::DescendingOrder);
}

bool StackImport::initialize(ParmList& parms, QWidget* parent)
{
  Point3f step(parms[2].toFloat(), parms[3].toFloat(), parms[4].toFloat());
  float brightness(parms[5].toFloat());
  QString filename(parms[6].toString());

  // Get stack and store
  int stackId = -1;
  Stack* stk = currentStack();
  if(not parms[0].toString().isEmpty()) {
    bool ok;
    stackId = parms[0].toInt(&ok);
    if(not ok)
      return setErrorMessage("Stack parameter must be a valid integer or empty.");
    stk = stack(parms[0].toInt());
  }
  if(not stk)
    return setErrorMessage("Invalid stack selected");
  if(!checkState().stack(STACK_ANY, stackId))
    return false;

  Store* store = stk->store(parms[1].value<STORE>());

  // Set up dialog
  StackImportDlg dlg(parent);

  bool res = false;
  dlg.setImageSize(512, 512, 0);
  dlg.setImageResolution(step);
  dlg.setBrightness(brightness);
  dlg.clearImageFiles();

  QString currentFile(store->file());

  if(not filename.isEmpty()) {
    // If file list contains one
    dlg.LoadProfile(filename);
  } else if(not currentFile.isEmpty()) {
    // If current stack file is a .txt file, load it as a profile
    dlg.LoadProfile(currentFile);
  }

  if(dlg.exec() == QDialog::Accepted) {
    // Change parms
    Point3f step = dlg.imageResolution();
    parms[2] = QString::number(step.x());
    parms[3] = QString::number(step.y());
    parms[4] = QString::number(step.z());
    parms[5] = QString::number(dlg.brightness());
    filename = dlg.loadedFile();
    if(filename.isEmpty()) {
      filename = dlg.imageFiles().join(";;");
    }
    parms[6] = filename;

    res = true;
  }

  return res;
}

bool StackImport::operator()(Stack* stack, Store* store, Point3f step, float brightness, QString filename, bool autoscale, float scale_range)
{
  Point3u size;
  QString txtFile;
  QStringList imageFiles;

  if(filename.size() > 0) {
    parseImageList(filename, size, step, brightness, imageFiles);
    if(filename.endsWith(".txt"))
      actingFile(filename);
    else
      actingFile(imageFiles[0]);
  } else
    return setErrorMessage("You need to specify a valid filename.");

  Information::out << "Size = " << size << endl;

  stack->setSize(size);
  stack->setStep(step);
  size_t SizeXYZ = size_t(size.x()) * size.y() * size.z();
  store->setFile(filename);
  HVecUS& data = store->data();
  uint filecount = 0;
  SETSTATUS("Loading " << ((store == stack->main()) ? "main" : "work") << " stack " << stack->userId());
  Progress progress(QString("Loading %1 Stack %2").arg((store == stack->main()) ? "main" : "work").arg(stack->userId()),
                    size.z());
  memset(data.data(), 0, SizeXYZ * sizeof(ushort));

  Image5D image(filename, data.data(), size, step);
  image.brightness = brightness;
  image.minc = 65535;
  image.maxc = 0;

  loadImageSequence(imageFiles, image, 0, 0, &progress);

  if(image.maxc > 0xFFFF)
    SETSTATUS("WARNING: max intensity:" << image.maxc << ", clipped at:" << 0xFFFF);
  else
    SETSTATUS(filecount << " of " << size.z() << " files read, intensity range:" << image.minc << "-" << image.maxc);

  // By default, this is not a labeled stack
  store->setLabels(false);
  store->changed();

  if(autoscale)
    store->autoScale(scale_range);

  stack->center();
  return true;
}

// REGISTER_STACK_PROCESS(StackImport);

MultiChannelOpenDlg::MultiChannelOpenDlg(const ImageInfo& info, QWidget* p, Qt::WindowFlags f)
  : QDialog(p, f)
  , ui(new Ui::MultiChannelOpenDlg)
{
  ui->setupUi(this);

  nb_channels = info.nbChannels();
  nb_timepoints = info.nbTimePoints();

  Information::out << "Loading samples" << endl;

  Image5D img;
  try {
    loadSamples(info.filename, img);
  } catch(...) {
    nb_channels = 0;
    return;
  }

  auto nb_pixels = img.size.x()*img.size.y();
  std::vector<uint8_t> sample_data(4*nb_pixels, 0xff);
  for(size_t i = 0, k = 0 ; i < nb_timepoints ; ++i)
    for(size_t j = 0 ; j < nb_channels ; ++j, ++k) {
      size_t shift = k*nb_pixels;
      int low = 65535, high = 0;
#pragma omp parallel for reduction(min: low) reduction(max: high)
      for(size_t d = 0 ; d < nb_pixels ; ++d) {
        ushort v = img[shift+d];
        if(v < low) low = v;
        if(v > high) high = v;
      }
      ++high;
      int delta = high - low;
      if(delta == 0) {
        delta = 1;
      }
      Information::out << "-- low = " << low << " - high = " << high << endl;
#pragma omp parallel for
      for(size_t d = 0 ; d < nb_pixels ; ++d) {
        uint8_t val = uint8_t((int(img[shift+d] - low) << 8) / delta);
        sample_data[4*d] = sample_data[4*d+1] = sample_data[4*d+2] = val;
      }
      QPixmap sample(256, 256);
      {
        auto sample_image = QImage(sample_data.data(), img.size.x(), img.size.y(), QImage::Format_RGB32);
        sample_image = sample_image.scaled(QSize(256, 256), Qt::KeepAspectRatio, Qt::SmoothTransformation);
        sample_image = sample_image.mirrored(false, true);
        QPainter paint(&sample);
        QRect r(0, 0, 256, 256);
        paint.setBrush(Qt::black);
        paint.drawRect(r);
        paint.drawImage(r, sample_image, r);
      }
      samples.push_back(sample);
    }

  Information::out << "# samples: " << samples.size() << endl;

  ui->sample->setPixmap(samples[0]);
  ui->channel->setMaximum(nb_channels);
  ui->timePoint->setMaximum(nb_timepoints);
  ui->stackSize->setText(QString("(%1,%2,%3)").arg(img.size.x()).arg(img.size.y()).arg(img.size.z()));
  ui->stackStep->setText(QString("%1%4 x %2%4 x %3%4").arg(img.step.x()).arg(img.step.y()).arg(img.step.z()).arg(UM));
  ui->nbChannels->setText(QString::number(nb_channels));
  ui->nbTimePoints->setText(QString::number(nb_timepoints));
}

MultiChannelOpenDlg::~MultiChannelOpenDlg()
{ }

int MultiChannelOpenDlg::channel() const
{
  return ui->channel->value()-1;
}

int MultiChannelOpenDlg::timepoint() const
{
  return ui->timePoint->value()-1;
}

void MultiChannelOpenDlg::setChannel(int value)
{
  ui->channel->setValue(value);
}

void MultiChannelOpenDlg::setTimepoint(int value)
{
  ui->timePoint->setValue(value);
}

void MultiChannelOpenDlg::on_channel_valueChanged(int value)
{
  auto pos = value-1 + timepoint()*nb_channels;
  Information::out << "Changed channel to " << value << " showing image " << pos << endl;
  ui->sample->setPixmap(samples[pos]);
}

void MultiChannelOpenDlg::on_timePoint_valueChanged(int value)
{
  auto pos = channel() + (value-1)*nb_channels;
  Information::out << "Changed timePoint to " << value << " showing image " << pos << endl;
  ui->sample->setPixmap(samples[pos]);
}

bool StackOpen::initialize(ParmList& parms, QWidget* parent)
{
  STORE which_store = parms[1].value<STORE>();
  int stackId = -1;
  if(not parms[2].toString().isEmpty())
  {
    bool ok;
    stackId = parms[2].toInt(&ok);
    if(not ok)
      return setErrorMessage("Stack parameter must be a valid integer or empty");
  }
  if(!checkState().stack(STACK_ANY, stackId))
    return false;
  bool choose_file = parmToBool(parms[3]);
  parms[3] = "Yes";
  QString allFilters("All Stack files (*.mgxs *.inr *.tif *.tiff)");
  QString mgxsFilter("LithoGraphX Stack files (*.mgxs)");
  QString inrFilter("Inria Stack files (*.inr)");
  QString tifFilter("TIFF Image Directory (*.tif *.tiff)");
  QString otherFilter("All Files (*.*)");
  QString filter;
  QString filename = parms[0].toString();

  int channel = 0;
  int timepoint = 0;
  std::tie(filename, channel, timepoint) = parseImageName(filename);

  if(filename.isEmpty() or choose_file) {
    auto store = stack(stackId)->store(which_store);
    QString currentFile;
    if(store)
      currentFile = store->file();
    if(!currentFile.isEmpty())
      filename = currentFile;

    // If we loaded a list of images before, strip of the ending and default type
    if(filename.right(4) == ".txt") {
      filename = filename.left(filename.length() - 4);
      filename += ".mgxs";
    }

    if(filename.right(4) == ".inr")
      filter = inrFilter;
    else if(filename.endsWith(".tif", Qt::CaseInsensitive) or filename.endsWith(".tiff", Qt::CaseInsensitive))
      filter = tifFilter;
    else if(filename.endsWith(".lgx", Qt::CaseInsensitive))
      filter = mgxsFilter;
    else
      filter = allFilters;

    // Get the file name
    filename = QFileDialog::getOpenFileName(parent, QString("Select stack file"), filename,
                                            QStringList{allFilters, mgxsFilter, inrFilter, tifFilter, otherFilter}.join(";;"),
                                            &filter, FileDialogOptions);
    if(filename.isEmpty())
      return false;
  }

  parms[0] = filename;

  ImageInfo info = getImageInfo(filename);
  Information::out << "image size: " << info.size << endl;
  if(info.nbTimePoints() > 1 or info.nbChannels() > 1) {
    MultiChannelOpenDlg dlg(info, parent);

    if(not dlg.valid())
      return setErrorMessage("Error, couldn't open TIFF file. Check terminal for precise error.");

    dlg.setTimepoint(timepoint);
    dlg.setChannel(channel);

    if(not dlg.valid())
      return setErrorMessage("Couldn't load TIFF samples");
    if(dlg.exec() == QDialog::Accepted) {
      filename += QString("?C=%1&T=%2").arg(dlg.channel()).arg(dlg.timepoint());
      parms[0] = filename;
      Information::out << "channel: " << dlg.channel() << endl;
      Information::out << "time point: " << dlg.timepoint() << endl;
      Information::out << "Full filename: " << filename << endl;
      return true;
    }
    return false;
  }

  return true;
}

bool StackOpen::operator()(const ParmList& parms)
{
  int stackId = parms[2].toInt();
  if(!checkState().stack(STACK_ANY, stackId))
    return false;
  Stack* stack = this->stack(stackId);
  Store* store = stack->store(parms[1].value<STORE>());
  if(not store)
    store = stack->main();
  QString filename = parms[0].toString();
  bool autoscale;
  float range = parms[4].toFloat(&autoscale);
  bool res = (*this)(stack, store, filename, autoscale, range);
  if(res) {
    store->show();
  }
  return res;
}

bool StackOpen::loadMGXS_0(QIODevice& file, Stack* stack, Store* store)
{
  HVecUS& data = store->data();
  QByteArray stkdata = file.readAll();
  file.close();
  stkdata = qUncompress(stkdata);

  struct StackHdr stkhdr;
  memcpy(&stkhdr, stkdata.constData(), sizeof(stkhdr));
  Point3u size(stkhdr.xsz, stkhdr.ysz, stkhdr.zsz);
  Point3f step(stkhdr.xum, stkhdr.yum, stkhdr.zum);
  stack->setSize(size);
  stack->setStep(step);
  if(stkhdr.datasz / 2 != stack->storeSize())
    throw QString("Datasize does not match dimensions in .mgxs file");
  memcpy(data.data(), stkdata.constData() + stkhdr.hdrsz, stkhdr.datasz);
  return true;
}

bool StackOpen::loadMGXS_1_3(QIODevice& file, Stack* stack, Store* store)
{
  bool isLabel;
  float sxum, syum, szum;
  file.read((char*)&isLabel, sizeof(bool));
  file.read((char*)&sxum, sizeof(float));
  file.read((char*)&syum, sizeof(float));
  file.read((char*)&szum, sizeof(float));
  bool result = loadMGXS_1_0(file, stack, store);
  if(result) {
    store->setLabels(isLabel);
    stack->setOrigin(Point3f(sxum, syum, szum));
    SETSTATUS("Set stack step to " << stack->origin());
  }
  return result;
}

bool StackOpen::loadMGXS_1_2(QIODevice& file, Stack* stack, Store* store)
{
  bool result = loadMGXS_1_3(file, stack, store);
  if(result and store->labels()) {
    StackSwapBytes swap(*this);
    return swap(store, store);
  }
  return result;
}

bool StackOpen::loadMGXS_1_1(QIODevice& file, Stack* stack, Store* store)
{
  bool isLabel;
  file.read((char*)&isLabel, sizeof(bool));
  bool result = loadMGXS_1_0(file, stack, store);
  if(result) {
    store->setLabels(isLabel);
    if(store->labels()) {
      StackSwapBytes swap(*this);
      return swap(store, store);
    }
  }
  return result;
}

bool StackOpen::loadMGXS_1_0(QIODevice& file, Stack* stack, Store* store)
{
  HVecUS& data = store->data();
  quint32 xsz;
  quint32 ysz;
  quint32 zsz;
  float xum;
  float yum;
  float zum;
  quint64 datasz;
  quint8 cl;
  file.read((char*)&xsz, sizeof(quint32));
  file.read((char*)&ysz, sizeof(quint32));
  file.read((char*)&zsz, sizeof(quint32));
  Information::out << "Size: " << xsz << " " << ysz << " " << zsz << endl;
  file.read((char*)&xum, sizeof(float));
  file.read((char*)&yum, sizeof(float));
  file.read((char*)&zum, sizeof(float));
  file.read((char*)&datasz, sizeof(quint64));
  file.read((char*)&cl, sizeof(quint8));
  quint64 expected_size = 2ul * (quint64(xsz) * quint64(ysz) * quint64(zsz));
  if(datasz != expected_size)
    throw QString("Datasize does not match dimensions in MGXS file (size = %1, expected from dimension = %2)")
          .arg(datasz)
          .arg(expected_size);
  Point3u size(xsz, ysz, zsz);
  Point3f step(xum, yum, zum);
  stack->setSize(size);
  stack->setStep(step);
  if(cl > 0) {
    Progress progress(QString("Loading stack %1").arg(stack->id()), expected_size);
    quint64 cur_size = 0;
    char* d = (char*)data.data();
    while(cur_size < 2 * data.size()) {
      if(!progress.advance(cur_size))
        userCancel();
      // Information::out << "advance to " << cur_size << endl;
      quint32 sz;
      qint64 qty = file.read((char*)&sz, sizeof(quint32));
      if(qty != sizeof(quint32)) {
        throw QString("Could not read enough data from MGXS file (read = %1, expected = %2)").arg(qty).arg(
                sizeof(quint32));
      }
      QByteArray stkdata = file.read(sz);
      if(quint32(stkdata.size()) != sz) {
        throw QString("Could not read enough data from MGXS file (read = %1, expected = %2)")
              .arg(stkdata.size())
              .arg(sz);
      }
      stkdata = qUncompress(stkdata);
      if(stkdata.isEmpty()) {
        throw QString("Compressed data in MGXS file is corrupted");
      }
      memcpy(d, stkdata.data(), stkdata.size());
      d += stkdata.size();
      cur_size += stkdata.size();
    }
    if(!progress.advance(cur_size))
      userCancel();
  } else {
    // Ok, if greater than 64MB, read in bits
    quint64 nb_slices = expected_size >> 26;
    quint64 slice_size = 1ul << 26;
    Progress progress(QString("Loading stack %1").arg(stack->id()), nb_slices);
    Information::out << "Nb of slices = " << nb_slices << endl;
    for(quint64 i = 0; i < nb_slices; ++i) {
      qint64 qty = file.read((char*)data.data() + i * slice_size, slice_size);
      if(qty != (int)slice_size) {
        throw QString("Could not read enough data from MGXS file (read = %1, expected = %2)").arg(qty).arg(
                slice_size);
      }
      if(!progress.advance(i))
        userCancel();
      // Information::out << "advance to " << i << endl;
    }
    quint64 left_size = expected_size - (nb_slices << 26);
    qint64 qty = file.read((char*)data.data() + (nb_slices << 26), left_size);
    if(qty != (int)left_size) {
      throw QString("Could not read enough data from MGXS file (read = %1, expected = %2)").arg(qty).arg(
              left_size);
    }
    if(!progress.advance(nb_slices))
      userCancel();
  }
  return true;
}

QList<int> extractVersion(QIODevice& file)
{
  QByteArray version;
  version.reserve(10);
  do {
    char c;
    qint64 qty = file.read(&c, 1);
    if(qty == 0) {
      throw QString("extractVersion:Could not read enough data from file (read = %1, expected = %2)")
            .arg(version.size())
            .arg(version.size() + 1);
    }
    version.push_back(c);
  } while(version[version.size() - 1] != ' ');
  version.chop(1);
  QList<QByteArray> values = version.split('.');
  QList<int> result;
  foreach(const QByteArray &a, values) {
    bool ok;
    result << a.toInt(&ok);
    if(!ok)
      throw QString("extractVersion:Version number must contain only integers (read = '%1')").arg(QString(a));
  }
  return result;
}

bool StackOpen::operator()(Stack* stack, Store* store, QString filename, bool autoscale, float scale_range)
{
  if(filename.isEmpty())
    throw QString("StackOpen::Error:Error, trying to load a stack from an empty filename.");

  actingFile(filename);
  store->setFile(filename);

  int channel, timepoint;
  std::tie(filename, channel, timepoint) = parseImageName(filename);
  if(filename.isEmpty())
    return false;

  QFile file(filename);
  if(!file.open(QIODevice::ReadOnly))
    throw QString("StackOpen::Error:Cannot open input file - '%1'").arg(filename);

  Progress progress(QString("Loading %1 Stack %2 from File '%3'")
                    .arg((store == stack->main()) ? "main" : "work")
                    .arg(stack->userId())
                    .arg(filename),
                    0, false);
  // Progress progress(QString("Reading stack file %1").arg(filename), 0, false);
  if(filename.endsWith(".mgxs", Qt::CaseInsensitive)) {
    static const char version[] = "MGXS ";
    static const int size_version = sizeof(version) - 1;
    char header_version[size_version];
    file.read(header_version, size_version);
    if(memcmp(header_version, version, size_version) == 0) {
      QList<int> version = extractVersion(file);
      if(version.size() != 2) {
        QStringList str;
        foreach(int i, version)
        str << QString::number(i);
        throw QString("StackOpen::Error:Invalid file version format (%1), expected: MAJOR.MINOR")
              .arg(str.join("."));
      }
      bool success = false;
      switch(version[0]) {
      case 1:
        switch(version[1]) {
        case 0:
          success = loadMGXS_1_0(file, stack, store);
          stack->center();
          break;
        case 1:
          success = loadMGXS_1_1(file, stack, store);
          stack->center();
          break;
        case 2:
          success = loadMGXS_1_2(file, stack, store);
          break;
        case 3:
          success = loadMGXS_1_3(file, stack, store);
          break;
        default:
          throw QString(
                  "StackOpen::Error:Unknown file version %1.%2, this software recognizes up to version 1.1")
                .arg(version[0])
                .arg(version[1]);
        }
        break;
      default:
        throw QString("StackOpen::Error:Unknown file version %1.%2, this software recognizes up to version 1.0")
              .arg(version[0])
              .arg(version[1]);
      }
      if(!success)
        return false;
    } else {
      file.seek(0);
      if(!loadMGXS_0(file, stack, store))
        return false;
      stack->center();
    }
  } else if(filename.endsWith(".inr", Qt::CaseInsensitive)) {
    HVecUS& data = store->data();
    QByteArray stkdata = file.readAll();
    file.close();
    CImgUS image;
    Point3f voxelsz;
    image.load_inr(filename.toLocal8Bit(), voxelsz.data());
    Point3u size(image.width(), image.height(), image.depth());
    Point3f step(voxelsz.x(), voxelsz.y(), voxelsz.z());
    stack->setSize(size);
    stack->setStep(step);
    memcpy(data.data(), image.data(), stack->storeSize() * 2);
    //stack->center();
  } else {
    Image5D data(filename, nullptr, stack->size(), stack->step());
    auto info = getImageInfo(filename);
    if(info.depth == 8)
      data.brightness = 256;
    try {
      loadImage(filename, data, channel, timepoint, &progress);
    } catch(...) {
      stack->setSize(Point3u(0, 0, 0));
      stack->main()->changed();
      stack->work()->changed();
      setErrorMessage(QString("Error, cannot load image file '%1'. Check standard output for exact error.").arg(filename));
      throw;
    }
    stack->setSize(Point3u(data.size));
    stack->setStep(data.step);
    store->setLabels(data.labels);
    memcpy(store->data().data(), data.c_data(), store->data().size()*sizeof(ushort));
    stack->center();
  }

  store->changed();

  if(store->labels()) {
    ushort max_label = 0;
    const HVecUS& data = store->data();
#pragma omp parallel for reduction(max: max_label)
    for(size_t i = 0 ; i < data.size() ; ++i) {
      auto label = data[i];
      if(label > max_label)
        max_label = label;
    }
    stack->setLabel(max_label);
  } else if(autoscale)
    store->autoScale(scale_range);

  SETSTATUS("Loaded stack " << stack->userId() << ", file:" << store->file() << " size: " << stack->size());
  return true;
}

// REGISTER_STACK_PROCESS(StackOpen);

bool MeshLoad::initialize(ParmList& parms, QWidget* parent)
{
  int meshId = parms[3].toInt();
  if(!checkState().mesh(MESH_ANY, meshId))
    return false;

  bool choose_file = parmToBool(parms[4]);
  parms[4] = "yes";
  QString filename = parms[0].toString();
  if(choose_file or filename.isEmpty()) {
    QDialog dlg(parent);
    Ui::LoadMeshDialog ui;
    ui.setupUi(&dlg);

    this->ui = &ui;
    this->dlg = &dlg;

    QObject::connect(ui.SelectMeshFile, &QAbstractButton::clicked, this, &MeshLoad::selectMeshFile);

    Mesh* mesh = this->mesh(meshId);
    if(not mesh->file().isEmpty())
      filename = mesh->file();

    ui.meshId->clear();
    for(int i = 0 ; i < meshCount() ; ++i)
      ui.meshId->addItem(QString::number(i+1));
    ui.meshId->setCurrentIndex(meshId);

    setMeshFile(filename);
    ui.Transform->setChecked(parmToBool(parms[1]));
    ui.Add->setChecked(parmToBool(parms[2]));

    bool res = false;
    if(dlg.exec() == QDialog::Accepted) {
      parms[0] = ui.MeshFile->text();
      parms[1] = (ui.Transform->isChecked() ? "yes" : "no");
      parms[2] = (ui.Add->isChecked() ? "yes" : "no");
      res = true;
    }
    this->ui = 0;
    this->dlg = 0;
    return res;
  }
  return true;
}

bool MeshLoad::operator()(const ParmList& parms)
{
  int meshId = parms[3].toInt();
  if(!checkState().mesh(MESH_ANY, meshId))
    return false;
  Mesh* mesh = this->mesh(meshId);
  QString filename = parms[0].toString();
  bool transform = parmToBool(parms[1]);
  bool add = parmToBool(parms[2]);
  bool res = (*this)(mesh, filename, transform, add);
  if(res) {
    // Show mesh if nothing visible
    if(!(mesh->isMeshVisible() or mesh->isSurfaceVisible())) {
      mesh->showSurface();
      mesh->showMesh();
    }
    if(mesh->hasImgTex())
      mesh->showImage();
    else
      mesh->showSignal();
    mesh->setNormals();
  }
  return res;
}

bool MeshLoad::operator()(Mesh* mesh, QString filename, bool transform, bool add)
{
  Progress progress(QString("Loading Mesh %1 from File '%2'").arg(mesh->userId()).arg(filename), 0, false);
  // Progress progress(QString("Loading Mesh File - %1").arg(filename), 0, false);
  actingFile(filename);
  QFileInfo fi(filename);
  QString suf = fi.suffix();
  bool success = false;
  auto type = extToTypeId(suf);
  if(type == TI_ALL) {
    // Maybe later look at file content
    return setErrorMessage(QString("Couldn't find the type of the file '%1'").arg(filename));
  }
  switch(type) {
    case TI_PLY:
      success = loadMeshPLY(mesh, filename, transform, add);
      break;
    case TI_MGXM:
      success = loadMGXM(mesh, filename, transform, add);
      break;
    case TI_VTU:
      success = loadMeshVTK(mesh, filename, transform, add);
      break;
    case TI_TEXT:
      success = loadText(mesh, filename, transform, add);
      break;
    case TI_KEYENCE:
      success = loadKeyence(mesh, filename, transform, add);
      break;
    case TI_MESH:
      success = loadMeshEdit(mesh, filename, transform, add);
      break;
    case TI_OBJ:
      success = loadMeshOBJ(mesh, filename, transform, add);
      break;
    case TI_ALL:
      setErrorMessage(QString("Error, invalid file type id '%1'").arg(type));
  }
  if(success) {
    actingFile(filename);
    mesh->setFile(filename);
    mesh->setTransformed(transform);
    mesh->updateBBox();
    mesh->updateAll();
    SETSTATUS("Loaded mesh, file:" << mesh->file() << ", vertices:" << mesh->graph().size());
  }
  return success;
}

bool MeshLoad::loadText(Mesh* mesh, const QString& filename, bool transform, bool add)
{
  QFile file(filename);
  if(!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
    setErrorMessage(QString("loadText::Error:Cannot open input file: %1").arg(filename));
    return false;
  }

  vvgraph& S = mesh->graph();

  if(!S.empty() and !add)
    mesh->reset();

  vertex_map vmap;

  QTextStream in(&file);
  in.setIntegerBase(10);
  bool is_cells = false;
  int vCnt;
  in >> vCnt;
  Progress progress(QString("Loading Mesh %1 from File '%2'").arg(mesh->userId()).arg(mesh->file()), vCnt * 2);
  // Progress progress(QString("Loading Mesh-%1 - %2").arg(mesh->userId()).arg(mesh->file()), vCnt*2);
  int interval = vCnt / 100;
  if(interval < 1)
    interval = 1;
  S.reserve(vCnt);
  vertex first(0);
  for(int i = 0; i < vCnt; i++) {
    if(in.atEnd())
      throw QString("Premature end of file reading vertices");
    vertex v;
    if(i == 0)
      first = v;
    S.insert(v);
    // On the second line, we test the type of mesh
    if(i == 1) {
      QString next_word;
      in >> next_word;
      bool ok;
      v->saveId = next_word.toInt(&ok);
      if(not ok) {
        first->type = next_word.toUtf8()[0];
        is_cells = true;
        in >> v->saveId;
      }
    } else
      in >> v->saveId;
    in >> v->pos >> v->label;
    if(is_cells)
      in >> v->type;
    v->pos = realPos(v->pos, transform, mesh);
    vmap[v->saveId] = v;
    if(v->label > mesh->viewLabel())
      mesh->setLabel(v->label);
    if((i % interval == 0)and !progress.advance(i))
      userCancel();
  }
  for(int i = 0; i < vCnt; i++) {
    if(in.atEnd())
      throw QString("Premature end of file reading neighborhoods");
    uint vId, nCnt;
    in >> vId >> nCnt;
    vertex_map::const_iterator vit = vmap.find(vId);
    if(vit == vmap.end())
      throw QString("Invalid vertex id: %1").arg(vId);
    vertex v = vit->second;
    vertex pn(0);
    for(uint j = 0; j < nCnt; j++) {
      uint nId;
      in >> nId;
      vertex_map::const_iterator nit = vmap.find(nId);
      if(nit == vmap.end())
        throw QString("Invalid vertex id: %1").arg(nId);
      vertex n = nit->second;
      if(S.valence(v) == 0)
        S.insertEdge(v, n);
      else
        S.spliceAfter(v, pn, n);
      pn = n;
    }
    if((i % interval == 0)and !progress.advance(vCnt + i))
      userCancel();
  }
  if(!progress.advance(2 * vCnt))
    userCancel();
  file.close();
  mesh->setCells(is_cells);
  mesh->setScaled(true);
  mesh->clearImgTex();
  mesh->setNormals();
  SETSTATUS("Loaded mesh, file:" << mesh->file() << ", vertices:" << S.size());
  return true;
}

using util::PlyFile;

void showHeader(PlyFile& ply)
{
  Information::out << "PLY file content";
  Information::out << "format = " << PlyFile::formatNames[ply.format()] << endl;
  Information::out << "version = " << ply.version() << endl;
  Information::out << "content position = " << ply.contentPosition() << endl;
  Information::out << endl;
  Information::out << "# elements = " << ply.nbElements() << endl;
  for(size_t i = 0; i < ply.nbElements(); ++i) {
    const PlyFile::Element& element = *ply.element(i);
    Information::out << "Element '" << element.name() << "' - " << element.size()
                     << " # properties = " << element.nbProperties() << endl;
    for(size_t j = 0; j < element.nbProperties(); ++j) {
      const PlyFile::Property& prop = *element.property(j);
      Information::out << "Property '" << prop.name() << "'";
      if(prop.kind() == PlyFile::Property::LIST)
        Information::out << " list " << PlyFile::typeNames[prop.sizeType()];
      Information::out << " " << PlyFile::typeNames[prop.fileType()] << " / "
                       << PlyFile::typeNames[prop.memType()] << endl;
    }
  }
}

bool MeshLoad::loadMGXM(Mesh* mesh, const QString& filename, bool transform, bool add)
{
  QFile file(filename);
  if(!file.open(QIODevice::ReadOnly)) {
    setErrorMessage(QString("loadMesh::Error:Cannot open input file: %1").arg(filename));
    return false;
  }

  char magic[9];
  char magic_check[9] = "MGX MESH";
  file.read(magic, 8);
  magic[8] = 0;
  bool scale = false;
  if(strcmp(magic, magic_check) == 0) {
    scale = true;
    if(!loadMGXM_0(file, mesh, scale, transform, add))
      return false;
    findSignalBounds(mesh);
  } else {
    file.seek(0);
    file.read(magic, 5);
    if(magic[4] == ' ')
      magic[4] = 0;
    if(strcmp(magic, "MGXM") == 0) {
      QList<int> version = extractVersion(file);
      if(version.size() != 2) {
        QStringList str;
        foreach(int i, version)
        str << QString::number(i);
        throw QString("MeshLoad::Error:Invalid file version format (%1), expected: MAJOR.MINOR")
              .arg(str.join("."));
      }
      bool success = false;
      switch(version[0]) {
      case 1:
        switch(version[1]) {
        case 0:
          success = loadMGXM_1_0(file, mesh, scale, transform, add);
          mesh->setCells(checkCells(mesh));
          findSignalBounds(mesh);
          break;
        case 1:
          success = loadMGXM_1_1(file, mesh, scale, transform, add);
          mesh->setCells(checkCells(mesh));
          break;
        case 2:
          success = loadMGXM_1_2(file, mesh, scale, transform, add);
          break;
        case 3:
          success = loadMGXM_1_3(file, mesh, scale, transform, add);
          break;
        default:
          throw QString(
                  "MeshLoad::Error:Unknown file version %1.%2, this software recognises up to version 1.0")
                .arg(version[0])
                .arg(version[1]);
        }
        break;
      default:
        throw QString("MeshLoad::Error:Unknown file version %1.%2, this software recognises up to version 1.0")
              .arg(version[0])
              .arg(version[1]);
      }
      if(!success)
        return false;
    } else {
      setErrorMessage(QString("The file '%1' is not a lgx mesh file").arg(filename));
      return false;
    }
  }
  file.close();
  mesh->clearImgTex();
  if(mesh->stack()->empty())
    mesh->setScaled(scale);
  else
    mesh->setScaled(true);
  return true;
}

bool MeshLoad::loadMeshPLY(Mesh* mesh, const QString& filename, bool transform, bool add)
{
  vvgraph& S = mesh->graph();

  Progress progress(QString("Loading PLY Mesh %1 from File '%2'").arg(mesh->userId()).arg(filename), 0);

  if(!S.empty() and !add)
    mesh->reset();

  // Define the reader and read header
  PlyFile ply;
  if(!ply.parseHeader(filename))
    throw(QString("Cannot parse PLY file header"));

  // Get global properties
  auto* globalElement = ply.element("global");
  util::PlyFile::Property *typeProp = nullptr, *orientedProp = nullptr,
                          *cell_meshProp = nullptr, *length_unitProp = nullptr,
                          *signal_unitProp = nullptr, *signal_descriptionProp = nullptr,
                          *signal_lowProp = nullptr, *signal_highProp = nullptr,
                          *heat_unitProp = nullptr, *heat_descriptionProp = nullptr,
                          *heat_lowProp = nullptr, *heat_highProp = nullptr,
                          *cellAxis_unitProp = nullptr, *cellAxis_descriptionProp = nullptr;
  if(globalElement and globalElement->size() == 1) {
    typeProp = globalElement->property("type");
    if(typeProp) typeProp->setMemType(PlyFile::CHAR);
    cell_meshProp = globalElement->property("cell_mesh");
    if(cell_meshProp) cell_meshProp->setMemType(PlyFile::UCHAR);
    orientedProp = globalElement->property("oriented");
    if(orientedProp) orientedProp->setMemType(PlyFile::UCHAR);
    length_unitProp = globalElement->property("length_unit");
    if(length_unitProp) length_unitProp->setMemType(PlyFile::FLOAT);
    signal_unitProp = globalElement->property("signal_unit");

    if(signal_unitProp) signal_unitProp->setMemType(PlyFile::CHAR);
    signal_descriptionProp = globalElement->property("signal_description");
    if(signal_descriptionProp) signal_descriptionProp->setMemType(PlyFile::CHAR);
    signal_lowProp = globalElement->property("signal_low");
    if(signal_lowProp) signal_lowProp->setMemType(PlyFile::FLOAT);
    signal_highProp = globalElement->property("signal_high");
    if(signal_highProp) signal_highProp->setMemType(PlyFile::FLOAT);

    heat_unitProp = globalElement->property("heat_unit");
    if(heat_unitProp) heat_unitProp->setMemType(PlyFile::CHAR);
    heat_descriptionProp = globalElement->property("heat_description");
    if(heat_descriptionProp) heat_descriptionProp->setMemType(PlyFile::CHAR);
    heat_lowProp = globalElement->property("heat_low");
    if(heat_lowProp) heat_lowProp->setMemType(PlyFile::FLOAT);
    heat_highProp = globalElement->property("heat_high");
    if(heat_highProp) heat_highProp->setMemType(PlyFile::FLOAT);

    cellAxis_unitProp = globalElement->property("cellAxis_unit");
    if(cellAxis_unitProp) cellAxis_unitProp->setMemType(PlyFile::CHAR);
    cellAxis_descriptionProp = globalElement->property("cellAxis_description");
    if(cellAxis_descriptionProp) cellAxis_descriptionProp->setMemType(PlyFile::CHAR);
  }

  // Get vertices and properties
  auto* vertexElement = ply.element("vertex");
  if(!vertexElement)
    return setErrorMessage("No vertex element in PLY file");

  auto* vertex_xProp = vertexElement->property("x");   // vertex positions
  auto* vertex_yProp = vertexElement->property("y");
  auto* vertex_zProp = vertexElement->property("z");
  if(not vertex_xProp or not vertex_yProp or not vertex_zProp)
    return setErrorMessage("x, y or z missing from vertex element in PLY file");

  vertex_xProp->setMemType(PlyFile::FLOAT);
  vertex_yProp->setMemType(PlyFile::FLOAT);
  vertex_zProp->setMemType(PlyFile::FLOAT);

  auto* vertex_labelProp = vertexElement->property("label");
  if(vertex_labelProp) vertex_labelProp->setMemType(PlyFile::INT);

  auto* vertex_selProp = vertexElement->property("selected");
  if(vertex_selProp) vertex_selProp->setMemType(PlyFile::UCHAR);

  // Vertex signal, if not signal, use the color
  bool hasSignal = false;
  bool hasRGB = false;
  auto* vertex_signalProp = vertexElement->property("signal");
  auto* vertex_redProp = vertexElement->property("red");
  auto* vertex_greenProp = vertexElement->property("green");
  auto* vertex_blueProp = vertexElement->property("blue");
  if(vertex_signalProp) {
    hasSignal = true;
    vertex_signalProp->setMemType(PlyFile::FLOAT);
  } else if(vertex_redProp and vertex_greenProp and vertex_blueProp) {
    hasRGB = true;
    vertex_redProp->setMemType(PlyFile::UCHAR);
    vertex_greenProp->setMemType(PlyFile::UCHAR);
    vertex_blueProp->setMemType(PlyFile::UCHAR);
  }

  // Get faces and properties
  auto* faceElement = ply.element("face");
  if(not faceElement)
    return setErrorMessage("No face element in PLY file");

  auto* vertex_indexProp = faceElement->property("vertex_index");
  if(not vertex_indexProp) {
    vertex_indexProp = faceElement->property("vertex_indices");
    if(not vertex_indexProp)
      return setErrorMessage("Neither vertex_index nor vertex_indices are present in face element");
  }
  vertex_indexProp->setMemType(PlyFile::UINT);

  auto* face_labelProp = faceElement->property("label");
  if(face_labelProp) face_labelProp->setMemType(PlyFile::INT);

  auto* face_areaProp = faceElement->property("area");
  if(face_areaProp) face_areaProp->setMemType(PlyFile::FLOAT);

  auto* face_xProp = faceElement->property("x");
  if(face_xProp) face_xProp->setMemType(PlyFile::FLOAT);
  auto* face_yProp = faceElement->property("y");
  if(face_yProp) face_yProp->setMemType(PlyFile::FLOAT);
  auto* face_zProp = faceElement->property("z");
  if(face_zProp) face_zProp->setMemType(PlyFile::FLOAT);

  auto* face_nxProp = faceElement->property("nx");
  if(face_nxProp) face_nxProp->setMemType(PlyFile::FLOAT);
  auto* face_nyProp = faceElement->property("ny");
  if(face_nyProp) face_nyProp->setMemType(PlyFile::FLOAT);
  auto* face_nzProp = faceElement->property("nz");
  if(face_nzProp) face_nzProp->setMemType(PlyFile::FLOAT);

  auto* labelElement = ply.element("label");
  PlyFile::Property *label_labelProp = nullptr, *label_heatProp = nullptr, *label_parentProp = nullptr,
                    *label_xProp = nullptr, *label_yProp = nullptr, *label_zProp = nullptr,
                    *label_nxProp = nullptr, *label_nyProp = nullptr, *label_nzProp = nullptr,
                    *label_xVisProp = nullptr, *label_yVisProp = nullptr, *label_zVisProp = nullptr,
                    *label_nxVisProp = nullptr, *label_nyVisProp = nullptr, *label_nzVisProp = nullptr,
                    *label_ev1XProp = nullptr, *label_ev1YProp = nullptr, *label_ev1ZProp = nullptr,
                    *label_ev2XProp = nullptr, *label_ev2YProp = nullptr, *label_ev2ZProp = nullptr,
                    *label_evals1Prop = nullptr, *label_evals2Prop = nullptr, *label_evals3Prop = nullptr;

  if(labelElement) {
    label_labelProp = labelElement->property("label");
    if(label_labelProp) {
      label_labelProp->setMemType(PlyFile::INT);
      label_heatProp = labelElement->property("heat");
      if(label_heatProp) label_heatProp->setMemType(PlyFile::FLOAT);
      label_parentProp = labelElement->property("parent");
      if(label_parentProp) label_parentProp->setMemType(PlyFile::INT);

      label_xProp = labelElement->property("x");
      if(label_xProp) label_xProp->setMemType(PlyFile::FLOAT);
      label_yProp = labelElement->property("y");
      if(label_yProp) label_yProp->setMemType(PlyFile::FLOAT);
      label_zProp = labelElement->property("z");
      if(label_zProp) label_zProp->setMemType(PlyFile::FLOAT);

      label_nxProp = labelElement->property("nx");
      if(label_nxProp) label_nxProp->setMemType(PlyFile::FLOAT);
      label_nyProp = labelElement->property("ny");
      if(label_nyProp) label_nyProp->setMemType(PlyFile::FLOAT);
      label_nzProp = labelElement->property("nz");
      if(label_nzProp) label_nzProp->setMemType(PlyFile::FLOAT);

      label_xVisProp = labelElement->property("xVis");
      if(label_xVisProp) label_xVisProp->setMemType(PlyFile::FLOAT);
      label_yVisProp = labelElement->property("yVis");
      if(label_yVisProp) label_yVisProp->setMemType(PlyFile::FLOAT);
      label_zVisProp = labelElement->property("zVis");
      if(label_zVisProp) label_zVisProp->setMemType(PlyFile::FLOAT);

      label_nxVisProp = labelElement->property("nxVis");
      if(label_nxVisProp) label_nxVisProp->setMemType(PlyFile::FLOAT);
      label_nyVisProp = labelElement->property("nyVis");
      if(label_nyVisProp) label_nyVisProp->setMemType(PlyFile::FLOAT);
      label_nzVisProp = labelElement->property("nzVis");
      if(label_nzVisProp) label_nzVisProp->setMemType(PlyFile::FLOAT);

      label_ev1XProp = labelElement->property("ev1X");
      if(label_ev1XProp) label_ev1XProp->setMemType(PlyFile::FLOAT);
      label_ev1YProp = labelElement->property("ev1Y");
      if(label_ev1YProp) label_ev1YProp->setMemType(PlyFile::FLOAT);
      label_ev1ZProp = labelElement->property("ev1Z");
      if(label_ev1ZProp) label_ev1ZProp->setMemType(PlyFile::FLOAT);

      label_ev2XProp = labelElement->property("ev2X");
      if(label_ev2XProp) label_ev2XProp->setMemType(PlyFile::FLOAT);
      label_ev2YProp = labelElement->property("ev2Y");
      if(label_ev2YProp) label_ev2YProp->setMemType(PlyFile::FLOAT);
      label_ev2ZProp = labelElement->property("ev2Z");
      if(label_ev1ZProp) label_ev1ZProp->setMemType(PlyFile::FLOAT);

      label_evals1Prop = labelElement->property("evals1");
      if(label_evals1Prop) label_evals1Prop->setMemType(PlyFile::FLOAT);
      label_evals2Prop = labelElement->property("evals2");
      if(label_evals2Prop) label_evals2Prop->setMemType(PlyFile::FLOAT);
      label_evals3Prop = labelElement->property("evals3");
      if(label_evals3Prop) label_evals3Prop->setMemType(PlyFile::FLOAT);
    }
  }

  // Parse file
  if(!ply.parseContent())
    return setErrorMessage("Unable to parse contents of PLY file");

  float length_unit = 1e-6;

  // Check global properties, if present
  if(globalElement) {
    auto* type = (typeProp ? typeProp->list<char>() : nullptr);
    if(type) {
      const auto& t = (*type)[0];
      std::string res = "cell_complex";
      bool good = false;
      if(t.size() == res.size()) {
        good = true;
        for(size_t i = 0 ; i < res.size() ; ++i)
          if(t[i] != res[i]) {
            good = false;
            break;
          }
      }
      if(not good)
        return setErrorMessage("Error, LithoGraphX can only load plain or cell_complex type PLY file");

      auto* oriented = (orientedProp ? orientedProp->value<uint8_t>() : nullptr);
      if(oriented and not (*oriented)[0])
        return setErrorMessage("Error, LithoGraphX can only load correctly oriented PLY files");

      auto* length_unit_p = (length_unitProp ? length_unitProp->value<float>() : nullptr);
      if(length_unit_p) length_unit = (*length_unit_p)[0];
      if(length_unit <= 0) length_unit = 1e-6;
    }
  }

  length_unit *= 1e6;
  Information::out << "Length multplication factor: " << length_unit << endl;

  // Get vertex data
  std::vector<vertex> vertices(vertexElement->size(), vertex(0));   // vertex positions
  auto* vertex_x = vertex_xProp->value<float>();
  if(not vertex_x)
    return setErrorMessage("Couldn't access value for 'x' property in 'vertex' element of the PLY object");
  auto* vertex_y = vertex_yProp->value<float>();
  if(not vertex_y)
    return setErrorMessage("Couldn't access value for 'y' property in 'vertex' element of the PLY object");
  auto* vertex_z = vertex_zProp->value<float>();
  if(not vertex_z)
    return setErrorMessage("Couldn't access value for 'z' property in 'vertex' element of the PLY object");

  auto* vertex_label = (vertex_labelProp ? vertex_labelProp->value<int>() : nullptr);

  std::vector<uchar>* vertex_red = nullptr;   // vertex colors
  std::vector<uchar>* vertex_green = nullptr;
  std::vector<uchar>* vertex_blue = nullptr;
  std::vector<float>* vertex_signal = nullptr;
  if(hasSignal) {
    Information::out << "Has signal" << endl;
    vertex_signal = vertex_signalProp->value<float>();
  } else if(hasRGB) {
    Information::out << "Has RGB information" << endl;
    vertex_red = vertex_redProp->value<uchar>();
    vertex_green = vertex_greenProp->value<uchar>();
    vertex_blue = vertex_blueProp->value<uchar>();
  }

  std::vector<uint8_t>* vertex_sel = (vertex_selProp ? vertex_selProp->value<uint8_t>() : nullptr);

  // Get face data
  std::vector<Point3i> triangles;
  auto* vertex_index = vertex_indexProp->list<uint32_t>();
  if(not vertex_index)
    return setErrorMessage("Couldn't access value for 'vertex_index' property in 'face' element of the PLY object");

  auto* face_label = (face_labelProp ? face_labelProp->value<int>() : nullptr);
  auto* face_area = (face_areaProp ? face_areaProp->value<int>() : nullptr);
  auto* face_x = (face_xProp ? face_xProp->value<int>() : nullptr);
  auto* face_y = (face_yProp ? face_yProp->value<int>() : nullptr);
  auto* face_z = (face_zProp ? face_zProp->value<int>() : nullptr);
  auto* face_nx = (face_nxProp ? face_nxProp->value<int>() : nullptr);
  auto* face_ny = (face_nyProp ? face_nyProp->value<int>() : nullptr);
  auto* face_nz = (face_nzProp ? face_nzProp->value<int>() : nullptr);

  auto* label = (label_labelProp ? label_labelProp->value<int>() : nullptr);
  auto* label_heat = (label_heatProp ? label_heatProp->value<float>() : nullptr);
  auto* label_parent = (label_parentProp ? label_parentProp->value<int>() : nullptr);

  auto* label_x = (label_xProp ? label_xProp->value<float>() : nullptr);
  auto* label_y = (label_yProp ? label_yProp->value<float>() : nullptr);
  auto* label_z = (label_zProp ? label_zProp->value<float>() : nullptr);

  auto* label_nx = (label_nxProp ? label_nxProp->value<float>() : nullptr);
  auto* label_ny = (label_nyProp ? label_nyProp->value<float>() : nullptr);
  auto* label_nz = (label_nzProp ? label_nzProp->value<float>() : nullptr);

  auto* label_xVis = (label_xVisProp ? label_xVisProp->value<float>() : nullptr);
  auto* label_yVis = (label_yVisProp ? label_yVisProp->value<float>() : nullptr);
  auto* label_zVis = (label_zVisProp ? label_zVisProp->value<float>() : nullptr);

  auto* label_nxVis = (label_nxVisProp ? label_nxVisProp->value<float>() : nullptr);
  auto* label_nyVis = (label_nyVisProp ? label_nyVisProp->value<float>() : nullptr);
  auto* label_nzVis = (label_nzVisProp ? label_nzVisProp->value<float>() : nullptr);

  auto* label_ev1X = (label_ev1XProp ? label_ev1XProp->value<float>() : nullptr);
  auto* label_ev1Y = (label_ev1YProp ? label_ev1YProp->value<float>() : nullptr);
  auto* label_ev1Z = (label_ev1ZProp ? label_ev1ZProp->value<float>() : nullptr);

  auto* label_ev2X = (label_ev2XProp ? label_ev2XProp->value<float>() : nullptr);
  auto* label_ev2Y = (label_ev2YProp ? label_ev2YProp->value<float>() : nullptr);
  auto* label_ev2Z = (label_ev2ZProp ? label_ev2ZProp->value<float>() : nullptr);

  auto* label_evals1 = (label_evals1Prop ? label_evals1Prop->value<float>() : nullptr);
  auto* label_evals2 = (label_evals2Prop ? label_evals2Prop->value<float>() : nullptr);
  auto* label_evals3 = (label_evals3Prop ? label_evals3Prop->value<float>() : nullptr);

  // See if cellular mesh
  bool cells = false;
  auto* cell_mesh = (cell_meshProp ? cell_meshProp->value<uint8_t>() : nullptr);
  if(cell_mesh)
    cells = (*cell_mesh)[0];
  else {
    for(size_t i = 0; i < vertex_index->size(); ++i)
      if((*vertex_index)[i].size() > 3) {
        cells = true;
        break;
      }
  }
  float minSignal = std::numeric_limits<float>::max();
  float maxSignal = -minSignal;
  int maxLabel = 0;
  // Process vertices
#pragma omp parallel for reduction(max: maxLabel, maxSignal) reduction(min: minSignal)
  for(uint i = 0; i < vertexElement->size(); ++i) {
    vertex v;
    v->pos = Point3f((*vertex_x)[i], (*vertex_y)[i], (*vertex_z)[i]) * length_unit;
    v->pos = realPos(v->pos, transform, mesh);
    v->saveId = i;
    v->type = 'j';
    vertices[i] = v;
    //S.insert(v);
    if(cells)
      v->label = -1;
    else if(vertex_label)
      v->label = (*vertex_label)[i];

    if(hasSignal)
      v->signal = (*vertex_signal)[i];
    else if(hasRGB)
      v->signal = (0.21 * (*vertex_red)[i] + 0.71 * (*vertex_green)[i] + 0.07 * (*vertex_blue)[i]) / 256.0;
    else
      v->signal = 0;

    if(vertex_sel)
      v->selected = (*vertex_sel)[i];

    if(minSignal < v->signal)
      minSignal = v->signal;
    if(maxSignal > v->signal)
      maxSignal = v->signal;

    if(v->label > maxLabel)
      maxLabel = v->label;
  }
  mesh->setLabel(maxLabel);

  auto* signal_low = (signal_lowProp ? signal_lowProp->value<float>() : nullptr);
  auto* signal_high = (signal_highProp ? signal_highProp->value<float>() : nullptr);
  if(signal_low)
    minSignal = (*signal_low)[0];
  if(signal_high)
    maxSignal = (*signal_high)[0];
  if(hasSignal or hasRGB)
    mesh->signalBounds() = Point2f(minSignal, maxSignal);
  else
    mesh->signalBounds() = Point2f(0, 1);

  // Process faces
  int nextLabel = mesh->viewLabel();
  if(cells) {
    for(size_t i = 0; i < faceElement->size(); ++i) {
      // If a cellular mesh, make a triangle fan.
      vertex c;

      if(face_x and face_y and face_z) {
        c->pos = Point3f((*face_x)[i], (*face_y)[i], (*face_z)[i]) * length_unit;
        c->pos = realPos(c->pos, transform, mesh);
      } else {
        // Find center vertex position
        c->pos = Point3f(0, 0, 0);
        for(int j : (*vertex_index)[i])
          c->pos += vertices[j]->pos;
        c->pos /= (*vertex_index)[i].size();
      }
      c->type = 'c';

      // Put label on center vertex
      if(face_label)
        c->label = (*face_label)[i];
      else
        c->label = ++nextLabel;

      int centerv = vertices.size();
      for(size_t j = 0; j < (*vertex_index)[i].size(); ++j) {
        size_t prev = j - 1;
        if(j == 0)
          prev = (*vertex_index)[i].size() - 1;

        triangles.push_back(Point3i(centerv, (*vertex_index)[i][prev], (*vertex_index)[i][j]));
      }
      vertices.push_back(c);

      if(c->label > mesh->viewLabel())
        mesh->setLabel(c->label);
    }
  } else {
    triangles.resize(faceElement->size());
#pragma omp parallel for
    for(size_t i = 0; i < faceElement->size(); ++i)
      triangles[i] = Point3i((*vertex_index)[i][0], (*vertex_index)[i][1], (*vertex_index)[i][2]);
  }

  // Create connectivity from triangle list
  if(!shape::meshFromTriangles(S, vertices, triangles))
    throw(QString("Cannot add all the triangles"));

  // Load heat map and parent if present
  if(labelElement) {
    if(not add) {
      mesh->clearHeatmap();
      mesh->parentLabelMap().clear();
    }

    auto& labelHeat = mesh->labelHeat();
    auto& labelParent = mesh->parentLabelMap();
    auto& labelCenter = mesh->labelCenter();
    auto& labelNormal = mesh->labelNormal();
    auto& labelCenterVis = mesh->labelCenterVis();
    auto& labelNormalVis = mesh->labelNormalVis();
    auto& cellAxis = mesh->cellAxis();

    for(size_t i = 0 ; i < label->size() ; ++i) {
      auto lab = (*label)[i];
      if(label_heat and not std::isnan((*label_heat)[i])) labelHeat[lab] = (*label_heat)[i];
      if(label_parent and (*label_parent)[i] >= 0) labelParent[lab] = (*label_parent)[i];
      if(label_x and not std::isnan((*label_x)[i])
         and label_y and not std::isnan((*label_y)[i])
         and label_z and not std::isnan((*label_z)[i])) {
        auto pos = Point3f((*label_x)[i], (*label_y)[i], (*label_z)[i]) * length_unit;
        labelCenter[lab] = realPos(pos, transform, mesh);
      }
      if(label_nx and not std::isnan((*label_nx)[i])
         and label_ny and not std::isnan((*label_ny)[i])
         and label_nz and not std::isnan((*label_nz)[i])) {
        auto pos = Point3f((*label_nx)[i], (*label_ny)[i], (*label_nz)[i]);
        labelNormal[lab] = normalized(realVector(pos, transform, mesh));
      }
      if(label_xVis and not std::isnan((*label_xVis)[i])
         and label_yVis and not std::isnan((*label_yVis)[i])
         and label_zVis and not std::isnan((*label_zVis)[i])) {
        auto pos = Point3f((*label_xVis)[i], (*label_yVis)[i], (*label_zVis)[i]) * length_unit;
        labelCenterVis[lab] = realPos(pos, transform, mesh);
      }
      if(label_nxVis and not std::isnan((*label_nxVis)[i])
         and label_nyVis and not std::isnan((*label_nyVis)[i])
         and label_nzVis and not std::isnan((*label_nzVis)[i])) {
        auto pos = Point3f((*label_nxVis)[i], (*label_nyVis)[i], (*label_nzVis)[i]);
        labelNormalVis[lab] = normalized(realVector(pos, transform, mesh));
      }
      if(label_ev1X and not std::isnan((*label_ev1X)[i])
         and label_ev1Y and not std::isnan((*label_ev1Y)[i])
         and label_ev1Z and not std::isnan((*label_ev1Z)[i])
         and label_ev2X and not std::isnan((*label_ev2X)[i])
         and label_ev2Y and not std::isnan((*label_ev2Y)[i])
         and label_ev2Z and not std::isnan((*label_ev2Z)[i])
         and label_evals1 and not std::isnan((*label_evals1)[i])
         and label_evals2 and not std::isnan((*label_evals2)[i])
         and label_evals3 and not std::isnan((*label_evals3)[i])) {
        auto ev1 = Point3f((*label_ev1X)[i], (*label_ev1Y)[i], (*label_ev1Z)[i]);
        auto ev2 = Point3f((*label_ev2X)[i], (*label_ev2Y)[i], (*label_ev2Z)[i]);
        auto evals = Point3f((*label_evals1)[i], (*label_evals2)[i], (*label_evals3)[i]);
        cellAxis[lab] = util::SymmetricTensor(normalized(realVector(ev1, transform, mesh)),
                                              normalized(realVector(ev2, transform, mesh)),
                                              evals);
      }
    }
  }

  // Finalize with global specifications
  {
    auto* signal_unit = (signal_unitProp ? signal_lowProp->list<char>() : nullptr);
    if(signal_unit) {
      QByteArray arr((*signal_unit)[0].data(), (*signal_unit)[0].size());
      mesh->signalUnit() = QString::fromUtf8(arr);
    }

    auto* signal_description = (signal_descriptionProp ? signal_lowProp->list<char>() : nullptr);
    if(signal_description) {
      QByteArray arr((*signal_description)[0].data(), (*signal_description)[0].size());
      bool ok;
      mesh->signalDesc() = Description::fromCSV(QString::fromUtf8(arr), &ok);
      if(not ok)
        setWarningMessage("Warning, invalid signal description");
    }

    auto* heat_unit = (heat_unitProp ? heat_unitProp->list<char>() : nullptr);
    if(heat_unit) {
      QByteArray arr((*heat_unit)[0].data(), (*heat_unit)[0].size());
      mesh->heatMapUnit() = QString::fromUtf8(arr);
    }

    auto* heat_description = (heat_descriptionProp ? heat_descriptionProp->list<char>() : nullptr);
    if(heat_description) {
      QByteArray arr((*heat_description)[0].data(), (*heat_description)[0].size());
      bool ok;
      mesh->heatMapDesc() = Description::fromCSV(QString::fromUtf8(arr), &ok);
      if(not ok)
        setWarningMessage("Warning, invalid heat map description");
    }

    auto* heat_low = (heat_lowProp ? heat_lowProp->value<float>() : nullptr);
    if(heat_low) mesh->heatMapBounds()[0] = (*heat_low)[0];

    auto* heat_high = (heat_highProp ? heat_highProp->value<float>() : nullptr);
    if(heat_high) mesh->heatMapBounds()[1] = (*heat_high)[0];

    auto* cellAxis_unit = (cellAxis_unitProp ? cellAxis_unitProp->list<char>() : nullptr);
    if(cellAxis_unit) {
      QByteArray arr((*cellAxis_unit)[0].data(), (*cellAxis_unit)[0].size());
      mesh->cellAxisUnit() = QString::fromUtf8(arr);
    }

    auto* cellAxis_description = (cellAxis_descriptionProp ? cellAxis_descriptionProp->list<char>() : nullptr);
    if(cellAxis_description) {
      QByteArray arr((*cellAxis_description)[0].data(), (*cellAxis_description)[0].size());
      bool ok;
      mesh->cellAxisDesc() = Description::fromCSV(QString::fromUtf8(arr), &ok);
      if(not ok)
        setWarningMessage("Warning, invalid cellAxis map description");
    }

  }

  mesh->setCells(cells);
  mesh->clearImgTex();
  mesh->setNormals();
  if(cells)
    SETSTATUS("Loaded cellular mesh, file:" << mesh->file() << ", vertices:" << S.size());
  else
    SETSTATUS("Loaded triangle mesh, file:" << mesh->file() << ", vertices:" << S.size());
  return true;
}

bool MeshLoad::loadKeyence(Mesh* mesh, const QString& filename, bool transform, bool /*add*/)
{
  QFile file(filename);
  if(!file.open(QIODevice::ReadOnly)) {
    setErrorMessage(QString("loadMesh::Error:Cannot open input file: %1").arg(filename));
    return false;
  }

  vvgraph& S = mesh->graph();

  mesh->reset();

  // First read in the entire file
  QByteArray buff = file.readAll();
  if(buff.size() != file.size())
    throw QString("Could not read the whole file");
  file.close();
  // Grab the image size
  int xsize = *(short*)(buff.data() + 47);
  int ysize = *(short*)(buff.data() + 49);
  // Height map uses every 4th pixel
  int xsz = xsize / 4;
  int ysz = ysize / 4;
  Progress progress(QString("Loading Height Map %1 from File '%2'").arg(mesh->userId()).arg(mesh->file()), ysz);

  // Find second image file
  int imgpos = buff.indexOf(QByteArray("JFIF"), 16);
  if(imgpos < 0)
    throw QString("Can't find color image");
  imgpos -= 6;
  QTemporaryFile imgfile;

  // Write to temp file
  imgfile.open();
  imgfile.write(buff.data() + imgpos, buff.size() - imgpos - xsz * ysz * 4);
  imgfile.close();

  // Load surface texture image
  QImage image(imgfile.fileName());
  // image.invertPixels();
  if(xsize != image.width() or ysize != image.height())
    throw QString("Invalid image size");
  mesh->setImgTex(image);

  // Find max/min z values to center image in z
  float maxz = -1e10, minz = 1e10;
  float* htp = (float*)(buff.data() + buff.size() - xsz * ysz * 4);
  S.reserve(xsz * ysz);
  for(int i = 0; i < xsz * ysz; i++) {
    float z = *htp++;
    if(z > maxz)
      maxz = z;
    if(z < minz)
      minz = z;
  }
  float zshift = (minz + maxz) / 2.0;

  // Save the vertex ids for connecting
  // vertex vtx[xsz][ysz];
  std::vector<std::vector<vertex> > vtx(xsz, std::vector<vertex>(ysz, vertex(0)));

  // Create the vertices and fill in height and texture coords
  htp = (float*)(buff.data() + buff.size() - xsz * ysz * 4);
  for(int y = ysz - 1; y >= 0; y--) {
    for(int x = xsz - 1; x >= 0; x--) {
      float z = *htp++;
      vertex v;
      S.insert(v);
      vtx[x][y] = v;
      v->pos.x() = (double(x) / double(xsz) - .5) * 4 / 3;
      v->pos.y() = double(y) / double(ysz) - .5;
      v->pos.z() = (z - zshift) * .03;
      v->pos = realPos(v->pos, transform, mesh);
      v->txpos.x() = (float(x * 4) / float(xsize));
      v->txpos.y() = (float(y * 4) / float(ysize));
    }
  }

  // Connect neighborhoods
  for(int y = 0; y < ysz; y++) {
    if(!progress.advance(y + 1))
      throw(QString("Keyence load cancelled"));
    for(int x = 0; x < xsz; x++) {
      std::vector<vertex> nhbs;
      nhbs.push_back(vtx[x][y]);
      if(x == 0 and y == ysz - 1) {       // top left corner;
        nhbs.push_back(vtx[x][y - 1]);
        nhbs.push_back(vtx[x + 1][y - 1]);
        nhbs.push_back(vtx[x + 1][y]);
      } else if(x == 0 and y == 0) {       // bottom left corner
        nhbs.push_back(vtx[x + 1][y]);
        nhbs.push_back(vtx[x][y + 1]);
      } else if(x == xsz - 1 and y == 0) {       // bottom right corner
        nhbs.push_back(vtx[x][y + 1]);
        nhbs.push_back(vtx[x - 1][y + 1]);
        nhbs.push_back(vtx[x - 1][y]);
      } else if(x == xsz - 1 and y == ysz - 1) {       // top right corner
        nhbs.push_back(vtx[x - 1][y]);
        nhbs.push_back(vtx[x][y - 1]);
      } else if(y == ysz - 1) {       // top edge
        nhbs.push_back(vtx[x - 1][y]);
        nhbs.push_back(vtx[x][y - 1]);
        nhbs.push_back(vtx[x + 1][y - 1]);
        nhbs.push_back(vtx[x + 1][y]);
      } else if(x == 0) {       // left edge
        nhbs.push_back(vtx[x][y - 1]);
        nhbs.push_back(vtx[x + 1][y - 1]);
        nhbs.push_back(vtx[x + 1][y]);
        nhbs.push_back(vtx[x][y + 1]);
      } else if(y == 0) {       // bottom edge
        nhbs.push_back(vtx[x + 1][y]);
        nhbs.push_back(vtx[x][y + 1]);
        nhbs.push_back(vtx[x - 1][y + 1]);
        nhbs.push_back(vtx[x - 1][y]);
      } else if(x == xsz - 1) {       // right edge
        nhbs.push_back(vtx[x][y - 1]);
        nhbs.push_back(vtx[x - 1][y + 1]);
        nhbs.push_back(vtx[x - 1][y]);
        nhbs.push_back(vtx[x][y + 1]);
      } else {       // Interior vertex
        nhbs.push_back(vtx[x + 1][y - 1]);
        nhbs.push_back(vtx[x + 1][y]);
        nhbs.push_back(vtx[x][y + 1]);
        nhbs.push_back(vtx[x - 1][y + 1]);
        nhbs.push_back(vtx[x - 1][y]);
        nhbs.push_back(vtx[x][y - 1]);
      }
      vertex v = nhbs[0];
      vertex pn(0);
      for(uint i = 1; i < nhbs.size(); i++) {
        vertex n = nhbs[i];
        if(i == 1)
          S.insertEdge(v, n);
        else
          S.spliceAfter(v, pn, n);
        pn = n;
      }
    }
  }
  mesh->setCells(false);
  mesh->setNormals();
  SETSTATUS("Loaded Keyence mesh, file:" << mesh->file() << ", vertices:" << S.size());
  return true;
}

bool MeshLoad::loadMeshOBJ(Mesh* mesh, const QString& filename, bool transform, bool add)
{
  QFile file(filename);
  if(!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
    setErrorMessage(QString("loadMesh::Error:Cannot open input file: %1").arg(filename));
    return false;
  }

  std::vector<Point3f> points;
  std::vector<float> signal;
  std::vector<int> labels;
  IntFloatMap& label_heat = mesh->labelHeat();
  label_heat.clear();
  std::vector<Point3i> triangles;
  float minSignal = FLT_MAX, maxSignal = -FLT_MAX;
  float minHeat = FLT_MAX, maxHeat = -FLT_MAX;

  QTextStream ts(&file);
  int line_count = 0;
  bool fixedBound = false;

  Information::out << "Reading the file" << endl;
  QString signalDescription;
  QString heatDescription;

  while(!ts.atEnd()) {
    ++line_count;
    QString line = ts.readLine().trimmed();
    int idx = line.indexOf('#');
    if(idx != -1)
      line = line.left(idx).trimmed();
    if(!line.isEmpty()) {
      QStringList fields = line.split(QRegularExpression("[ \t]"), QString::SkipEmptyParts);
      if(fields[0] == "v") {
        if(fields.size() != 4 and fields.size() != 5) {
          setErrorMessage(
            QString("Error reading file '%1' on line %2: vertex needs 3 or 4 values (x y z [w]), got %3")
            .arg(filename)
            .arg(line_count)
            .arg(fields.size() - 1));
          return false;
        }
        Point3f pos;
        bool ok;
        pos.x() = fields[1].toFloat(&ok);
        if(!ok) {
          setErrorMessage(QString("Error reading file '%1' on line %2: invalid value for vertex x: '%3'")
                          .arg(filename)
                          .arg(line_count)
                          .arg(fields[1]));
          return false;
        }
        pos.y() = fields[2].toFloat(&ok);
        if(!ok) {
          setErrorMessage(QString("Error reading file '%1' on line %2: invalid value for vertex y: '%3'")
                          .arg(filename)
                          .arg(line_count)
                          .arg(fields[2]));
          return false;
        }
        pos.z() = fields[3].toFloat(&ok);
        if(!ok) {
          setErrorMessage(QString("Error reading file '%1' on line %2: invalid value for vertex z: '%3'")
                          .arg(filename)
                          .arg(line_count)
                          .arg(fields[3]));
          return false;
        }
        points.push_back(pos);

        // If there is a 5th field, this is w, so we need to normalize the position with this
        if(fields.size() == 5) {
          float w = fields[4].toFloat(&ok);
          if(!ok) {
            setErrorMessage(QString("Error reading file '%1' on line %2: invalid value for vertex w: '%3'")
                            .arg(filename)
                            .arg(line_count)
                            .arg(fields[4]));
            return false;
          }
          if(w != 0)
            pos /= w;
          else {
            setErrorMessage(
              QString("Error reading file '%1' on line %2: LithoGraphX cannot interpret a vertex w of 0")
              .arg(filename)
              .arg(line_count));
            return false;
          }
        }
      } else if(fields[0] == "f") {
        std::vector<int> poly(fields.size() - 1);
        bool ok;
        int psz = points.size();
        for(int i = 0; i < fields.size() - 1; ++i) {
          poly[i] = fields[i + 1].toInt(&ok) - 1;
          if(!ok or poly[i] < 0 or poly[i] >= psz) {
            setErrorMessage(
              QString("Error reading file '%1' on line %2: the first vertex id is not valid: '%3'")
              .arg(filename)
              .arg(line_count)
              .arg(fields[i + 1]));
            return false;
          }
        }
        // If a triangle, just add it
        if(poly.size() == 3)
          triangles.push_back(Point3i(poly[0], poly[1], poly[2]));
        else {         // Otherwise make a triangle fan
                       // Find center
          Point3f c(0, 0, 0);
          for(size_t i = 0; i < poly.size(); ++i)
            c += points[poly[i]];
          c /= poly.size();
          points.push_back(c);

          // Add tris
          for(size_t i = 0; i < poly.size(); ++i)
            if(i == poly.size() - 1)
              triangles.push_back(Point3i(psz, poly[i], poly[0]));
            else
              triangles.push_back(Point3i(psz, poly[i], poly[i + 1]));
        }
      } else if(fields[0] == "label") {
        if(fields.size() != 2) {
          setErrorMessage(
            QString("Error reading file '%1' on line %2: the label field must have 1 value, got %3")
            .arg(filename)
            .arg(line_count)
            .arg(fields.size() - 1));
          return false;
        }
        int lab;
        bool ok;
        lab = fields[1].toInt(&ok);
        if(!ok) {
          setErrorMessage(
            QString("Error reading file '%1' on line %2: the vertex label has invalid value: '%3'")
            .arg(filename)
            .arg(line_count)
            .arg(fields[1]));
          return false;
        }
        labels.push_back(lab);
      } else if(fields[0] == "vv") {
        if(fields.size() != 2) {
          setErrorMessage(
            QString("Error reading file '%1' on line %2: the vv field must have 1 value, got %3")
            .arg(filename)
            .arg(line_count)
            .arg(fields.size() - 1));
          return false;
        }
        float value;
        bool ok;
        value = fields[1].toFloat(&ok);
        if(!ok) {
          setErrorMessage(
            QString("Error reading file '%1' on line %2: the vertex value has invalid value: '%3'")
            .arg(filename)
            .arg(line_count)
            .arg(fields[1]));
          return false;
        }
        signal.push_back(value);
        if(!fixedBound) {
          if(value > maxSignal)
            maxSignal = value;
          if(value < minSignal)
            minSignal = value;
        }
      } else if(fields[0] == "vv_desc") {
        fields.pop_front();
        signalDescription = fields.join(" ");
      } else if(fields[0] == "vv_range") {
        if(fields.size() != 3) {
          return setErrorMessage(
            QString("Error reading file '%1' on line %2: the vv_range needs 2 values, not %3")
            .arg(filename)
            .arg(line_count)
            .arg(fields.size() - 1));
        }
        float vmin, vmax;
        bool ok;
        vmin = fields[1].toFloat(&ok);
        if(!ok) {
          return setErrorMessage(
            QString("Error reading file '%1' on line %2: min value for vv_range is not a value float: %3")
            .arg(filename)
            .arg(line_count)
            .arg(fields[1]));
        }
        vmax = fields[2].toFloat(&ok);
        if(!ok) {
          return setErrorMessage(
            QString("Error reading file '%1' on line %2: max value for vv_range is not a value float: %3")
            .arg(filename)
            .arg(line_count)
            .arg(fields[2]));
        }
        minSignal = vmin;
        maxSignal = vmax;
        Information::out << "vv_range = " << minSignal << " - " << maxSignal << endl;
        fixedBound = true;
      } else if(fields[0] == "heat") {
        if(fields.size() != 3) {
          setErrorMessage(
            QString("Error reading file '%1' on line %2: the vv field must have 2 value, got %3")
            .arg(filename)
            .arg(line_count)
            .arg(fields.size() - 1));
          return false;
        }
        int lab;
        float value;
        bool ok;
        lab = fields[1].toInt(&ok);
        if(!ok) {
          setErrorMessage(
            QString("Error reading file '%1' on line %2: the heat label has invalid value: '%3'")
            .arg(filename)
            .arg(line_count)
            .arg(fields[1]));
          return false;
        }
        value = fields[2].toFloat(&ok);
        if(!ok) {
          setErrorMessage(
            QString("Error reading file '%1' on line %2: the heat value has invalid value: '%3'")
            .arg(filename)
            .arg(line_count)
            .arg(fields[2]));
          return false;
        }
        label_heat[lab] = value;
        if(value < minHeat)
          minHeat = value;
        if(value > maxHeat)
          maxHeat = value;
      } else if(fields[0] == "heat_desc") {
        fields.pop_front();
        heatDescription = fields.join(" ");
      }
      // Ignore anything else
    }
  }

  if(!signal.empty() and signal.size() != points.size()) {
    setErrorMessage(
      QString(
        "Error reading file '%1', signal array size mismatch. Current file has %2 vs fields and %3 vertices")
      .arg(filename)
      .arg(signal.size())
      .arg(points.size()));
    return false;
  }
  if(!labels.empty() and labels.size() != points.size()) {
    setErrorMessage(QString("Error reading file '%1', labels array size mismatch. Current file has %2 labels "
                            "fields and %3 vertices")
                    .arg(filename)
                    .arg(labels.size())
                    .arg(points.size()));
    return false;
  }

  Information::out << "Extracted " << points.size() << " points and " << triangles.size() << " triangles" << endl;

  vvgraph& S = mesh->graph();

  if(!add)
    mesh->reset();
  else
    for(const vertex& v : S)
      v->saveId = -1;

  if(signal.empty() and not fixedBound) {
    minSignal = 0.0;
    maxSignal = 1.0;
  } else if(maxSignal == minSignal)
    minSignal -= 1.0f;

  // First add vertices
  std::vector<vertex> vtx(points.size(), vertex(0));
  for(size_t i = 0; i < points.size(); ++i) {
    vertex v;
    v->pos = realPos(points[i], transform, mesh);
    vtx[i] = v;
    if(signal.empty())
      v->signal = 1.0;
    else {
      v->signal = signal[i];
    }
    if(!labels.empty())
      v->label = labels[i];
    // Check for NaN
    v->signal = v->signal == v->signal ? v->signal : 0;
  }

  if(!shape::meshFromTriangles(S, vtx, triangles)) {
    setErrorMessage("Error, cannot add all the triangles");
    return false;
  }

  std::cout << "MinSignal:" << minSignal << ", MaxSignal:" << maxSignal << std::endl;

  if(!label_heat.empty()) {
    if(minHeat >= maxHeat)
      maxHeat += 1;
    mesh->heatMapBounds() = Point2f(minHeat, maxHeat);
    Information::out << "heatMapBounds = " << mesh->heatMapBounds() << endl;
    mesh->showHeat();
  }
  if(!heatDescription.isEmpty())
    mesh->heatMapUnit() = heatDescription;

  mesh->signalBounds() = Point2f(minSignal, maxSignal);
  mesh->signalUnit() = signalDescription;
  mesh->setCells(false);
  mesh->clearImgTex();
  mesh->setNormals();

  return true;
}

bool MeshLoad::loadMeshEdit(Mesh* mesh, const QString& filename, bool transform, bool add)
{
  QFile file(filename);
  if(!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
    setErrorMessage(QString("loadMesh::Error:Cannot open input file: %1").arg(filename));
    return false;
  }

  vvgraph& S = mesh->graph();

  if(!add)
    mesh->reset();
  else
    for(const vertex& v : S)
      v->saveId = -1;

  std::vector<vertex> vertices;
  std::vector<Point3i> triangles;

  QTextStream in(&file);
  QString tmp;
  tmp = in.readLine();
  tmp = in.readLine();
  tmp = in.readLine();
  int vCnt;
  in >> vCnt;
  Progress progress(QString("Loading Triangle Mesh Vertices %1 from File '%2'").arg(mesh->userId()).arg(mesh->file()),
                    vCnt);
  int prog = 1;
  uint saveId = 0;
  S.reserve(vCnt);
  for(int i = 0; i < vCnt; i++) {
    if(in.atEnd())
      throw(QString("Premature end of file reading vertices"));
    vertex v;
    S.insert(v);
    in >> v->pos >> v->label;
    v->pos = realPos(v->pos, transform, mesh);
    v->saveId = saveId++;
    vertices.push_back(v);
    if(v->label > mesh->viewLabel())
      mesh->setLabel(v->label);
    if(!progress.advance(prog++))
      userCancel();
  }
  tmp = in.readLine();
  tmp = in.readLine();
  int tCnt, t;

  in >> tCnt;
  progress.restart(QString("Loading triangles-%1 - %2").arg(mesh->userId()).arg(mesh->file()), tCnt);
  for(int i = 0; i < tCnt; i++) {
    if(in.atEnd())
      throw QString("Premature end of file reading triangles");
    Point3i tri;
    in >> tri.x() >> tri.y() >> tri.z() >> t;
    tri -= Point3i(1, 1, 1);
    triangles.push_back(tri);
    if(!progress.advance(prog++))
      userCancel();
  }
  file.close();

  if(!shape::meshFromTriangles(S, vertices, triangles)) {
    setErrorMessage("Error, cannot add all the triangles");
    return false;
  }
  mesh->setCells(false);
  mesh->clearImgTex();
  mesh->setNormals();
  SETSTATUS("Loaded mesh, file:" << mesh->file() << ", vertices:" << S.size());
  return true;
}

void MeshLoad::selectMeshFile()
{
  QString filename = ui->MeshFile->text();
  QStringList filters;
  for(size_t ti = 0 ; ti <= TI_ALL ; ++ti) {
    auto exts = typeIdToExtensions((TypeId)ti);
    exts.replaceInStrings(QRegularExpression("^"), "*.");
    filters << QString("%1 Files (%2)").arg(typeIdToName((TypeId)ti))
                                       .arg(exts.join(" "));
  }
  QString filter;
  {
    QFileInfo fi(filename);
    QString suf = fi.suffix();
    filter = filters[extToTypeId(suf)];
  }
  filename = QFileDialog::getOpenFileName(dlg, QString("Select mesh file"), filename, filters.join(";;"), &filter,
                                          FileDialogOptions);
  if(!filename.isEmpty()) {
    if(!QFile::exists(filename)) {
      QMessageBox::information(dlg, "Error selecting file",
                               "You must select an existing file. Your selection is ignored.");
      return;
    }
    setMeshFile(filename);
  }
}

MeshLoad::TypeId MeshLoad::extToTypeId(QString ext) const
{
  for(int i = 0 ; i < TI_ALL ; ++i) {
    auto ti = (TypeId)i;
    const auto& exts = typeIdToExtensions((TypeId)ti);
    if(exts.contains(ext)) return ti;
  }
  return TI_ALL;
}

QStringList MeshLoad::typeIdToExtensions(TypeId t) const
{
  switch(t)
  {
    case TI_PLY:
      return QStringList() << "ply";
    case TI_MGXM:
      return QStringList() << "mgxm";
    case TI_VTU:
      return QStringList() << "vtu";
    case TI_TEXT:
      return QStringList() << "txt";
    case TI_KEYENCE:
      return QStringList() << "jpg" << "jpeg";
    case TI_MESH:
      return QStringList() << "mesh";
    case TI_OBJ:
      return QStringList() << "obj";
    case TI_ALL:
      return QStringList() << "ply" << "mgxm" << "vtu" << "txt" << "jpg" << "jpeg" << "mesh" << "obj";
  }
  return {};
}

QString MeshLoad::typeIdToName(TypeId t) const
{
  switch(t)
  {
    case TI_PLY:
      return "PLY (Stanford Polygon)";
    case TI_MGXM:
      return "MorphoGraphX Mesh";
    case TI_VTU:
      return "VTK Unstructured Mesh";
    case TI_TEXT:
      return "Text";
    case TI_KEYENCE:
      return "Keyence JPEG";
    case TI_MESH:
      return "MeshEdit";
    case TI_OBJ:
      return "Wavefront OBJ";
    case TI_ALL:
      return "All Mesh";
  }
  return "";
}

void MeshLoad::setMeshFile(const QString& filename)
{
  ui->MeshFile->setText(filename);
}

bool MeshLoad::loadMGXM_1_3(QIODevice& file, Mesh* mesh, bool& scale, bool& transform, bool add)
{
  return loadMGXM_1_2(file, mesh, scale, transform, add, false);
}

bool MeshLoad::loadMGXM_1_2(QIODevice& file, Mesh* mesh, bool& scale, bool& transform, bool add, bool has_color)
{
  bool is_cells;
  file.read((char*)&is_cells, sizeof(bool));
  mesh->setCells(is_cells);
  return loadMGXM_1_1(file, mesh, scale, transform, add, has_color);
}

bool MeshLoad::loadMGXM_1_1(QIODevice& file, Mesh* mesh, bool& scale, bool& transform, bool add, bool has_color)
{
  file.read((char*)&scale, sizeof(bool));
  scale = not scale; // The file contains whether or not the tissues is scaled ...
  file.read((char*)&transform, sizeof(bool));
  Point2f signalBounds;
  file.read((char*)&signalBounds[0], sizeof(float));
  file.read((char*)&signalBounds[1], sizeof(float));
  if(signalBounds[0] == signalBounds[1]) {
    signalBounds[0] = 0;
    signalBounds[1] = 65535;
  }
  mesh->signalBounds() = signalBounds;
  uint stringSize;
  file.read((char*)&stringSize, sizeof(uint));
  QByteArray string(stringSize, 0);
  file.read(string.data(), stringSize);
  mesh->signalUnit() = QString::fromUtf8(string);
  bool res = loadMGXM_0(file, mesh, scale, transform, add, has_color);
  if(res and has_color) {
    float deltaSignal = signalBounds[1] - signalBounds[0];
    for(const vertex& v : mesh->graph())
      v->signal = signalBounds[0] + v->signal * deltaSignal;
  }
  return res;
}

bool MeshLoad::loadMGXM_1_0(QIODevice& file, Mesh* mesh, bool& scale, bool& transform, bool add, bool )
{
  file.read((char*)&scale, sizeof(bool));
  file.read((char*)&transform, sizeof(bool));
  return loadMGXM_0(file, mesh, scale, transform, add);
}

bool MeshLoad::loadMGXM_0(QIODevice& file, Mesh* mesh, bool scale, bool transform, bool add, bool has_color)
{
  vvgraph& S = mesh->graph();
  vertex_map vmap;

  if(!S.empty() and !add)
    mesh->reset();

  uint hSz, vCnt, vSz, eSz;
  file.read((char*)&hSz, 4);
  file.read((char*)&vCnt, 4);
  file.read((char*)&vSz, 4);
  file.read((char*)&eSz, 4);
  bool has_label = vSz >= 4;
  if(has_label)
    vSz -= 4;
  if(has_color) {
    has_color = vSz >= 4;
    if(has_color)
      vSz -= 4;
  }
  bool has_signal = vSz >= 4;
  if(has_signal)
    vSz -= 4;
  bool has_type = vSz >= 1;
  if(has_type)
    vSz -= 1;
  bool has_selected = vSz >= 1;
  if(has_selected)
    vSz -= 1;

  if(hSz > 0) {
    std::vector<char> t(hSz);
    file.read(t.data(), hSz);
  }
  float tmp;

  Progress progress(QString("Loading Mesh %1 from File '%2'").arg(mesh->userId()).arg(mesh->file()), vCnt * 2);
  // Progress progress(QString("Loading Mesh-%1 - %2").arg(mesh->userId()).arg(mesh->file()), vCnt * 2);
  int interval = vCnt / 100;
  if(interval < 1)
    interval = 1;
  S.reserve(vCnt);
  for(uint i = 0; i < vCnt; i++) {
    if(file.atEnd())
      throw(QString("Premature end of file reading vertices"));
    vertex v;
    S.insert(v);
    file.read((char*)&v->saveId, 4);
    file.read((char*)&v->pos, 12);
    v->pos = realPos(v->pos, transform, mesh, scale);
    if(has_label)
      file.read((char*)&v->label, 4);
    if(has_color or has_signal)
      file.read((char*)&v->signal, 4);
    if(has_color and has_signal)
      file.read((char*)&tmp, 4);
    if(has_type)
      file.read((char*)&v->type, 1);
    if(has_selected)
      file.read((char*)&v->selected, 1);
    if(vSz > 0) {
      std::vector<char> t(vSz);
      file.read(t.data(), vSz);
    }
    vmap[v->saveId] = v;
    if(v->label > mesh->viewLabel())
      mesh->setLabel(v->label);
    if((i % interval == 0)and !progress.advance(i))
      userCancel();
  }
  for(uint i = 0; i < vCnt; i++) {
    if(file.atEnd())
      throw(QString("Premature end of file reading neighborhoods"));
    uint vId, nCnt;
    file.read((char*)&vId, 4);
    file.read((char*)&nCnt, 4);
    if(eSz > 0) {
      std::vector<char> t(eSz);
      file.read(t.data(), eSz);
    }
    vertex_map::const_iterator vit = vmap.find(vId);
    if(vit == vmap.end())
      throw QString("Invalid vertex id: %1").arg(vId);
    vertex v = vit->second;
    vertex pn(0);
    for(uint j = 0; j < nCnt; j++) {
      uint nId;
      file.read((char*)&nId, 4);
      vertex_map::const_iterator nit = vmap.find(nId);
      if(vit == vmap.end())
        throw QString("Invalid vertex id: %1").arg(nId);
      vertex n = nit->second;
      if(S.valence(v) == 0)
        S.insertEdge(v, n);
      else
        S.spliceAfter(v, pn, n);
      pn = n;
    }
    if((i % interval) == 0 and !progress.advance(vCnt + i))
      userCancel();
  }
  if(!progress.advance(2 * vCnt))
    userCancel();
  return true;
}

void MeshLoad::findSignalBounds(Mesh* mesh)
{
  vvgraph& S = mesh->graph();
  float minSignal = FLT_MAX, maxSignal = -FLT_MAX;
  for(const vertex& v : S) {
    float s = v->signal;
    if(s > maxSignal)
      maxSignal = s;
    if(s < minSignal)
      minSignal = s;
  }
  if(minSignal == maxSignal)
    minSignal -= 1;
  mesh->signalBounds() = Point2f(minSignal, maxSignal);
}

// REGISTER_MESH_PROCESS(MeshLoad);

bool LoadAllData::operator()(const ParmList&)
{
  if(!(*this)())
    return false;
  for(int i = 0; i < stackCount(); ++i) {
    Stack* s = stack(i);
    if(s->empty()) {
      s->main()->hide();
      s->work()->hide();
    }
  }
  for(int i = 0; i < meshCount(); ++i) {
    Mesh* m = mesh(i);
    if(m->graph().empty()) {
      m->hideSurface();
      m->hideMesh();
    } else {
      if(m->toShow() == SurfaceView::Heat)
        m->showNormal();
    }
  }
  return true;
}

bool LoadAllData::loadStore(Stack* stack, Store* store, QStringList& errors)
{
  if(!store->file().isEmpty()) {
    if(store->file().endsWith(".txt", Qt::CaseInsensitive)) {
      StackImport importStack(*this);
      Point3f step;
      float brightness = 1.f;
      if(!importStack(stack, store, step, brightness, store->file(), false, 0)) {
        errors << importStack.errorMessage();
        return false;
      }
    } else {
      StackOpen openStack(*this);
      if(!openStack(stack, store, store->file(), false, 0)) {
        errors << openStack.errorMessage();
        return false;
      }
    }
  } else
    store->reset();
  return true;
}

bool LoadAllData::operator()()
{
  QStringList errors;
  for(int i = 0; i < stackCount(); ++i) {
    Stack* s = stack(i);
    Store* main = s->main();
    QString mainFilename = main->file();
    Store* work = s->work();
    QString workFilename = work->file();
    try {
      main->setFile(mainFilename);
      if(!loadStore(s, main, errors))
        main->reset();
    }
    catch(UserCancelException&) {
      main->reset();
      return true;
    }
    try {
      work->setFile(workFilename);
      if(!loadStore(s, work, errors))
        work->reset();
    }
    catch(UserCancelException&) {
      work->reset();
      return true;
    }
    if(main->file().isEmpty() and work->file().isEmpty()) {
      s->setSize(Point3u(0, 0, 0));
      main->reset();
      work->reset();
    }
  }
  MeshLoad loadMesh(*this);
  for(int i = 0; i < meshCount(); ++i) {
    Mesh* m = mesh(i);
    if(!m->file().isEmpty()) {
      try {
        if(not loadMesh(m, m->file(), m->transformed(), false)) {
          m->reset();
          errors << loadMesh.errorMessage();
        }
      }
      catch(UserCancelException&) {
        m->graph().clear();
        return true;
      }
    }
  }
  if(!errors.empty())
    setWarningMessage(QString("Warnings:\n  *%1").arg(errors.join("\n  *")));
  return true;
}

// REGISTER_GLOBAL_PROCESS(LoadAllData);

bool LoadProjectFile::operator()(const ParmList& parms) {
  return (*this)(parms[0].toString());
}

bool LoadProjectFile::operator()(QString filename)
{
  actingFile(filename, true);
  bool res = systemCommand(LOAD_PROJECT, ParmList() << filename);
  if(!res) {
    setErrorMessage("Error while loading project file.");
    return false;
  }
  LoadAllData loaddata(*this);
  res = loaddata();
  if(!res)
    setErrorMessage(loaddata.errorMessage());
  return res;
}

bool LoadProjectFile::initialize(ParmList& parms, QWidget* parent)
{
  QString filename = parms[0].toString();
  bool choose_file = parmToBool(parms[1]);
  parms[1] = "Yes";

  // Get the file name
  if(choose_file or filename.isEmpty()) {
    if(filename.isEmpty())
      filename = file();
    QString filters = "LithoGraphX Project files (*.lgxp);;MorphoGraphX View files (*.mgxv);;All files (*.*)";
    filename = QFileDialog::getOpenFileName(parent, QString("Select project file"), filename,
                                            filters, 0, FileDialogOptions);

    if(filename.isEmpty())
      return false;
  }

  parms[0] = filename;
  return true;
}

// REGISTER_GLOBAL_PROCESS(LoadProjectFile);

bool ResetMeshProcess::operator()(const ParmList& parms)
{
  bool ok;
  int meshId = parms[0].toInt(&ok);
  if(not ok) {
    if(parms[0].toString().isEmpty())
      meshId = -1;
    else
      return setErrorMessage("Error, the 'Mesh' parameter must be empty or an integer.");
  }
  if(!checkState().mesh(MESH_ANY, meshId))
    return false;
  return operator()(this->mesh(meshId));
}

bool ResetMeshProcess::operator()(Mesh* m)
{
  m->reset();
  return true;
}
} // namespace process
} // namespace lgx
