/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "Shader.hpp"

#include <Information.hpp>

#include <QFile>
#include <QTextStream>
#include <QString>
#include <QSet>

#include <algorithm>
#include <iostream>

namespace lgx {

using Information::err;

bool Shader::has_shaders = false;
QString Shader::default_version = "150 compatibility";

namespace {
const GLSLValue null_value = GLSLValue();
const QString null_string = QString();

void glUniform1fv_(GLint id, GLsizei s, const GLfloat* val) { opengl->glUniform1fv(id, s, val); }
void glUniform2fv_(GLint id, GLsizei s, const GLfloat* val) { opengl->glUniform2fv(id, s, val); }
void glUniform3fv_(GLint id, GLsizei s, const GLfloat* val) { opengl->glUniform3fv(id, s, val); }
void glUniform4fv_(GLint id, GLsizei s, const GLfloat* val) { opengl->glUniform4fv(id, s, val); }
void glUniform1iv_(GLint id, GLsizei s, const GLint* val) { opengl->glUniform1iv(id, s, val); }
void glUniform2iv_(GLint id, GLsizei s, const GLint* val) { opengl->glUniform2iv(id, s, val); }
void glUniform3iv_(GLint id, GLsizei s, const GLint* val) { opengl->glUniform3iv(id, s, val); }
void glUniform4iv_(GLint id, GLsizei s, const GLint* val) { opengl->glUniform4iv(id, s, val); }
void glUniform1uiv_(GLint id, GLsizei s, const GLuint* val) { opengl->glUniform1uiv(id, s, val); }
void glUniform2uiv_(GLint id, GLsizei s, const GLuint* val) { opengl->glUniform2uiv(id, s, val); }
void glUniform3uiv_(GLint id, GLsizei s, const GLuint* val) { opengl->glUniform3uiv(id, s, val); }
void glUniform4uiv_(GLint id, GLsizei s, const GLuint* val) { opengl->glUniform4uiv(id, s, val); }
void glUniformTransposedMatrix2fv_(GLint location, GLsizei count, const GLfloat* v)
{
  opengl->glUniformMatrix2fv(location, count, 1, v);
}
void glUniformTransposedMatrix3fv_(GLint location, GLsizei count, const GLfloat* v)
{
  opengl->glUniformMatrix3fv(location, count, 1, v);
}
void glUniformTransposedMatrix4fv_(GLint location, GLsizei count, const GLfloat* v)
{
  opengl->glUniformMatrix4fv(location, count, 1, v);
}

void glVertexAttrib1fv_(GLuint id, const GLfloat* val) { opengl->glVertexAttrib1fv(id, val); }
void glVertexAttrib2fv_(GLuint id, const GLfloat* val) { opengl->glVertexAttrib2fv(id, val); }
void glVertexAttrib3fv_(GLuint id, const GLfloat* val) { opengl->glVertexAttrib3fv(id, val); }
void glVertexAttrib4fv_(GLuint id, const GLfloat* val) { opengl->glVertexAttrib4fv(id, val); }

void glVertexAttribI1iv_(GLuint id, const GLint* val) { opengl->glVertexAttribI1iv(id, val); }
void glVertexAttribI2iv_(GLuint id, const GLint* val) { opengl->glVertexAttribI2iv(id, val); }
void glVertexAttribI3iv_(GLuint id, const GLint* val) { opengl->glVertexAttribI3iv(id, val); }
void glVertexAttribI4iv_(GLuint id, const GLint* val) { opengl->glVertexAttribI4iv(id, val); }

void glVertexAttribI1uiv_(GLuint id, const GLuint* val) { opengl->glVertexAttribI1uiv(id, val); }
void glVertexAttribI2uiv_(GLuint id, const GLuint* val) { opengl->glVertexAttribI2uiv(id, val); }
void glVertexAttribI3uiv_(GLuint id, const GLuint* val) { opengl->glVertexAttribI3uiv(id, val); }
void glVertexAttribI4uiv_(GLuint id, const GLuint* val) { opengl->glVertexAttribI4uiv(id, val); }
} // namespace

void Shader::setDefaultGLSLVersion(QString v)
{
  default_version = v;
}

Shader::Shader(QString v, int _verbosity)
  : verbosity(_verbosity)
  , _program(0)
  , _initialized(false)
  , _version(v.isEmpty() ? default_version : v)
{
}

Shader::Shader(const Shader& copy)
  : vertex_shaders_code(copy.vertex_shaders_code)
  , fragment_shaders_code(copy.fragment_shaders_code)
  , geometry_shaders_code(copy.geometry_shaders_code)
  , uniform_names(copy.uniform_names)
  , model_uniform_names(copy.model_uniform_names)
  , uniform_values(copy.uniform_values)
  , model_uniform_values(copy.model_uniform_values)
  , constants(copy.constants)
  , definitions(copy.definitions)
  , verbosity(copy.verbosity)
  , _program(0)
  , _initialized(false)
  , _version(copy._version)
{
}

Shader& Shader::operator=(const Shader& copy)
{
  if(this == &copy)
    return *this;
  vertex_shaders_code = copy.vertex_shaders_code;
  fragment_shaders_code = copy.fragment_shaders_code;
  geometry_shaders_code = copy.geometry_shaders_code;
  uniform_names = copy.uniform_names;
  model_uniform_names = copy.model_uniform_names;
  uniform_values = copy.uniform_values;
  model_uniform_values = copy.model_uniform_values;
  constants = copy.constants;
  definitions = copy.definitions;
  _version = copy._version;
  verbosity = copy.verbosity;
  _program = 0;
  vertex_shaders.clear();
  fragment_shaders.clear();
  geometry_shaders.clear();
  _initialized = false;
  return *this;
}

Shader::Shader(Shader&& other)
  : vertex_shaders_code(std::move(other.vertex_shaders_code))
  , fragment_shaders_code(std::move(other.fragment_shaders_code))
  , geometry_shaders_code(std::move(other.geometry_shaders_code))
  , vertex_shaders(std::move(other.vertex_shaders))
  , fragment_shaders(std::move(other.fragment_shaders))
  , geometry_shaders(std::move(other.geometry_shaders))
  , uniform_names(std::move(other.uniform_names))
  , model_uniform_names(std::move(other.model_uniform_names))
  , uniform_values(std::move(other.uniform_values))
  , model_uniform_values(std::move(other.model_uniform_values))
  , constants(std::move(other.constants))
  , definitions(std::move(other.definitions))
  , verbosity(other.verbosity)
  , _program(other._program)
  , _initialized(other._initialized)
  , _version(std::move(other._version))
{
  other.vertex_shaders.clear();
  other.fragment_shaders.clear();
  other.geometry_shaders.clear();
  other._program = 0;
  other._initialized = false;
}

Shader& Shader::operator=(Shader&& other)
{
  vertex_shaders_code = std::move(other.vertex_shaders_code);
  fragment_shaders_code = std::move(other.fragment_shaders_code);
  geometry_shaders_code = std::move(other.geometry_shaders_code);
  uniform_names = std::move(other.uniform_names);
  vertex_shaders = std::move(other.vertex_shaders);
  fragment_shaders = std::move(other.fragment_shaders);
  geometry_shaders = std::move(other.geometry_shaders);
  model_uniform_names = std::move(other.model_uniform_names);
  uniform_values = std::move(other.uniform_values);
  model_uniform_values = std::move(other.model_uniform_values);
  constants = std::move(other.constants);
  definitions = std::move(other.definitions);
  _version = std::move(other._version);
  verbosity = other.verbosity;
  _program = other._program;
  _initialized = other._initialized;
  other._initialized = false;
  other._program = 0;
  other.vertex_shaders.clear();
  other.fragment_shaders.clear();
  other.geometry_shaders.clear();
  other._program = 0;
  return *this;
}

bool Shader::init()
{
  has_shaders = true;
  return true;
}

void Shader::reset(bool include_version)
{
  cleanShaders();
  vertex_shaders_code.clear();
  fragment_shaders_code.clear();
  geometry_shaders_code.clear();
  uniform_names.clear();
  model_uniform_names.clear();
  uniform_values.clear();
  model_uniform_values.clear();
  constants.clear();
  definitions.clear();
  vertex_shaders.clear();
  fragment_shaders.clear();
  geometry_shaders.clear();
  _program = 0;
  if(include_version)
    _version = default_version;
}

void Shader::addVertexShader(QString filename)
{
  auto it_found = std::find(vertex_shaders_code.begin(), vertex_shaders_code.end(),
                            std::make_pair(filename, false));
  if(it_found == vertex_shaders_code.end()) {
    vertex_shaders_code.push_back(std::make_pair(filename, false));
    invalidate();
  }
}

void Shader::removeVertexShader(int pos)
{
  if(pos >= 0 and pos < (int)vertex_shaders_code.size()) {
    vertex_shaders_code.erase(vertex_shaders_code.begin()+pos);
    invalidate();
  }
}


void Shader::removeVertexShader(QString filename)
{
  auto it_found = std::find(vertex_shaders_code.begin(), vertex_shaders_code.end(),
                            std::make_pair(filename, false));
  if(it_found != vertex_shaders_code.end()) {
    vertex_shaders_code.erase(it_found);
    invalidate();
  }
}

void Shader::clearVertexShader()
{
  if(not vertex_shaders_code.empty()) {
    vertex_shaders_code.clear();
    invalidate();
  }
}


void Shader::addFragmentShader(QString filename)
{
  auto it_found = std::find(fragment_shaders_code.begin(), fragment_shaders_code.end(),
                            std::make_pair(filename, false));
  if(it_found == fragment_shaders_code.end()) {
    fragment_shaders_code.push_back(std::make_pair(filename, false));
    invalidate();
  }
}

bool Shader::changeFragmentShader(int pos, QString filename)
{
  if(pos < 0)
    pos += (int)fragment_shaders_code.size();
  if(pos < 0)
    return false;
  if(fragment_shaders_code.size() > (size_t)pos) {
    code_t c = std::make_pair(filename, false);
    if(c != fragment_shaders_code[pos]) {
      invalidate();
      fragment_shaders_code[pos] = std::make_pair(filename, false);
    }
    return true;
  }
  return false;
}

void Shader::addGeometryShader(QString filename)
{
  auto it_found = std::find(geometry_shaders_code.begin(), geometry_shaders_code.end(),
                            std::make_pair(filename, false));
  if(it_found == geometry_shaders_code.end()) {
    geometry_shaders_code.push_back(std::make_pair(filename, false));
    invalidate();
  }
}

bool Shader::changeGeometryShader(int pos, QString filename)
{
  if(pos < 0)
    pos += (int)geometry_shaders_code.size();
  if(pos < 0)
    return false;
  if(geometry_shaders_code.size() > (size_t)pos) {
    code_t c = std::make_pair(filename, false);
    if(c != geometry_shaders_code[pos]) {
      invalidate();
      geometry_shaders_code[pos] = std::make_pair(filename, false);
    }
    return true;
  }
  return false;
}


bool Shader::changeVertexShader(int pos, QString filename)
{
  if(pos < 0)
    pos += (int)vertex_shaders_code.size();
  if(pos < 0)
    return false;
  if(vertex_shaders_code.size() > (size_t)pos) {
    code_t c = std::make_pair(filename, false);
    if(c != vertex_shaders_code[pos]) {
      invalidate();
      vertex_shaders_code[pos] = std::make_pair(filename, false);
    }
    return true;
  }
  return false;
}

bool Shader::changeGeometryShaderCode(int pos, QString filename)
{
  if(pos < 0)
    pos += (int)geometry_shaders_code.size();
  if(pos < 0)
    return false;
  if(geometry_shaders_code.size() > (size_t)pos) {
    code_t c = std::make_pair(filename, true);
    if(c != geometry_shaders_code[pos]) {
      invalidate();
      geometry_shaders_code[pos] = std::make_pair(filename, true);
    }
    return true;
  }
  return false;
}


bool Shader::changeFragmentShaderCode(int pos, QString filename)
{
  if(pos < 0)
    pos += (int)fragment_shaders_code.size();
  if(pos < 0)
    return false;
  if(fragment_shaders_code.size() > (size_t)pos) {
    code_t c = std::make_pair(filename, true);
    if(c != fragment_shaders_code[pos]) {
      invalidate();
      fragment_shaders_code[pos] = std::make_pair(filename, true);
    }
    return true;
  }
  return false;
}

bool Shader::changeVertexShaderCode(int pos, QString filename)
{
  if(pos < 0)
    pos += (int)vertex_shaders_code.size();
  if(pos < 0)
    return false;
  if(vertex_shaders_code.size() > (size_t)pos) {
    code_t c = std::make_pair(filename, true);
    if(c != vertex_shaders_code[pos]) {
      invalidate();
      vertex_shaders_code[pos] = std::make_pair(filename, true);
    }
    return true;
  }
  return false;
}

void Shader::removeGeometryShader(int pos)
{
  if(pos >= 0 and pos < (int)geometry_shaders_code.size()) {
    geometry_shaders_code.erase(geometry_shaders_code.begin()+pos);
    invalidate();
  }
}

void Shader::clearGeometryShader()
{
  if(not geometry_shaders_code.empty()) {
    geometry_shaders_code.clear();
    invalidate();
  }
}

void Shader::removeGeometryShader(QString filename)
{
  auto it_found = std::find(geometry_shaders_code.begin(), geometry_shaders_code.end(),
                            std::make_pair(filename, false));
  if(it_found != geometry_shaders_code.end()) {
    geometry_shaders_code.erase(it_found);
    invalidate();
  }
}

void Shader::removeFragmentShader(int pos)
{
  if(pos >= 0 and pos < (int)fragment_shaders_code.size()) {
    fragment_shaders_code.erase(fragment_shaders_code.begin()+pos);
    invalidate();
  }
}

void Shader::removeFragmentShader(QString filename)
{
  auto it_found = std::find(fragment_shaders_code.begin(), fragment_shaders_code.end(),
                            std::make_pair(filename, false));
  if(it_found != fragment_shaders_code.end()) {
    fragment_shaders_code.erase(it_found);
    invalidate();
  }
}

void Shader::clearFragmentShader()
{
  if(not fragment_shaders_code.empty()) {
    fragment_shaders_code.clear();
    invalidate();
  }
}

void Shader::addVertexShaderCode(QString code)
{
  auto it_found = std::find(vertex_shaders_code.begin(), vertex_shaders_code.end(),
                            std::make_pair(code, true));
  if(it_found == vertex_shaders_code.end()) {
    vertex_shaders_code.push_back(std::make_pair(code, true));
    invalidate();
  }
}

void Shader::removeVertexShaderCode(QString code)
{
  auto it_found = std::find(vertex_shaders_code.begin(), vertex_shaders_code.end(),
                            std::make_pair(code, true));
  if(it_found != vertex_shaders_code.end()) {
    vertex_shaders_code.erase(it_found);
    invalidate();
  }
}

void Shader::addGeometryShaderCode(QString code)
{
  auto it_found = std::find(geometry_shaders_code.begin(), geometry_shaders_code.end(),
                            std::make_pair(code, true));
  if(it_found == geometry_shaders_code.end()) {
    geometry_shaders_code.push_back(std::make_pair(code, true));
    invalidate();
  }
}

void Shader::removeGeometryShaderCode(QString code)
{
  auto it_found = std::find(geometry_shaders_code.begin(), geometry_shaders_code.end(),
                            std::make_pair(code, true));
  if(it_found != geometry_shaders_code.end()) {
    geometry_shaders_code.erase(it_found);
    invalidate();
  }
}


void Shader::addFragmentShaderCode(QString code)
{
  auto it_found = std::find(fragment_shaders_code.begin(), fragment_shaders_code.end(),
                            std::make_pair(code, true));
  if(it_found == fragment_shaders_code.end()) {
    fragment_shaders_code.push_back(std::make_pair(code, true));
    invalidate();
  }
}

void Shader::removeFragmentShaderCode(QString code)
{
  auto it_found = std::find(fragment_shaders_code.begin(), fragment_shaders_code.end(),
                            std::make_pair(code, true));
  if(it_found != fragment_shaders_code.end()) {
    fragment_shaders_code.erase(it_found);
    invalidate();
  }
}

void Shader::cleanShaders()
{
  if(!hasShaders())
    return;
  for(const GLuint& id : vertex_shaders) {
    if(_program)
      opengl->glDetachShader(_program, id);
    opengl->glDeleteShader(id);
  }
  for(const GLuint& id : fragment_shaders) {
    if(_program)
      opengl->glDetachShader(_program, id);
    opengl->glDeleteShader(id);
  }
  for(const GLuint& id : geometry_shaders) {
    if(_program)
      opengl->glDetachShader(_program, id);
    opengl->glDeleteShader(id);
  }
  vertex_shaders.clear();
  fragment_shaders.clear();
  geometry_shaders.clear();
  if(_program)
    opengl->glDeleteProgram(_program);
  _program = 0;
}

namespace {
QString getShader(unsigned int pos, const std::vector<Shader::code_t>& _code, int verbosity)
{
  const Shader::code_t& code = _code[pos];
  if(code.second)
    return code.first;
  else {
    QString filename = code.first;
    QFile f(filename);
    if(f.open(QIODevice::ReadOnly)) {
      QTextStream ts(&f);
      return ts.readAll();
    }
    else {
      if(verbosity > 1)
        err << "Error, cannot open shader file " << filename << endl;
      return QString();
    }
  }
}

QString getWholeShader(const std::vector<Shader::code_t>& _code, QString version, int verbosity,
                       const QHash<QString, GLSLValue>& constants, const QHash<QString, QString>& definitions)
{
  QString content = QString("#version %1\n\n").arg(version);
  for(auto it = definitions.begin(); it != definitions.end(); ++it) {
    const QString& name = it.key();
    const QString& value = it.value();
    QString cst = QString("#define %1 %2\n").arg(name).arg(value);
    content += cst;
  }
  for(auto it = constants.begin(); it != constants.end(); ++it) {
    const QString& name = it.key();
    const GLSLValue& value = it.value();
    QString cst = QString("const %1 %2 = %3;\n").arg(value.typeName()).arg(name).arg(value.toGLSL());
    content += cst;
  }
  for(const Shader::code_t& code : _code) {
    if(code.second) {
      content += code.first;
      content += "\n";
    }
    else {
      QString filename = code.first;
      QFile f(filename);
      if(f.open(QIODevice::ReadOnly)) {
        QTextStream ts(&f);
        content += ts.readAll();
        content += "\n";
      }
      else {
        if(verbosity > 1)
          err << "Error, cannot open shader file " << filename << endl;
        return QString();
      }
    }
  }
  return content;
}
} // namespace

QString Shader::getVertexShader(unsigned int pos) const { return getShader(pos, vertex_shaders_code, verbosity); }

QString Shader::getFragmentShader(unsigned int pos) const { return getShader(pos, fragment_shaders_code, verbosity); }

QString Shader::getGeometryShader(unsigned int pos) const { return getShader(pos, geometry_shaders_code, verbosity); }

QString Shader::vertexShader() const
{
  return getWholeShader(vertex_shaders_code, version(), verbosity, constants, definitions);
}

QString Shader::fragmentShader() const
{
  return getWholeShader(fragment_shaders_code, version(), verbosity, constants, definitions);
}

QString Shader::geometryShader() const
{
  return getWholeShader(geometry_shaders_code, version(), verbosity, constants, definitions);
}

bool Shader::setupShaders()
{
  if(!hasShaders()) {
    if(verbosity > 2)
      err << "Warning, trying to setup a shader without code" << endl;
    return false;
  }
  cleanShaders();
  // First, compile the shaders
  // For that, concatenate all the files, and add the version 120 to the beginning
  {
    QString content = vertexShader();
    if(content.isEmpty()) {
      cleanShaders();
      return false;
    }
    if(verbosity > 3)
      err << "Compiling vertex shader" << endl;
    if(verbosity > 4)
      err << "Content of the shader:\n------------------\n" << content << "\n-----------------\n" << endl;
    GLuint s = compileShader(GL_VERTEX_SHADER, content);
    if(s == 0) {
      cleanShaders();
      return false;
    }
    vertex_shaders.push_back(s);
  }

  {
    QString content = fragmentShader();
    if(content.isEmpty()) {
      cleanShaders();
      return false;
    }
    if(verbosity > 3)
      err << "Compiling fragment shader" << endl;
    if(verbosity > 4)
      err << "Content of the shader:\n------------------\n" << content << "\n-----------------\n" << endl;
    GLuint s = compileShader(GL_FRAGMENT_SHADER, content);
    if(s == 0) {
      cleanShaders();
      return false;
    }
    fragment_shaders.push_back(s);
  }

  if(not geometry_shaders_code.empty()) {
    QString content = geometryShader();
    if(content.isEmpty()) {
      cleanShaders();
      return false;
    }
    if(verbosity > 3)
      err << "Compiling geometry shader" << endl;
    if(verbosity > 4)
      err << "Content of the shader:\n------------------\n" << content << "\n-----------------\n" << endl;
    GLuint s = compileShader(GL_GEOMETRY_SHADER, content);
    if(s == 0) {
      cleanShaders();
      return false;
    }
    geometry_shaders.push_back(s);
  }

  if(fragment_shaders.empty() and vertex_shaders.empty() and geometry_shaders.empty()) {
    if(verbosity > 2)
      err << "Warning, no shader to compile" << endl;
    return false;
  }
  // Create the program
  _program = opengl->glCreateProgram();
  REPORT_GL_ERROR("_program = glCreateProgram()");
  // Add shader
  for(const GLuint& id : vertex_shaders) {
    opengl->glAttachShader(_program, id);
    REPORT_GL_ERROR("glAttachObject(_program, id)");
  }
  for(const GLuint& id : fragment_shaders) {
    opengl->glAttachShader(_program, id);
    REPORT_GL_ERROR("glAttachObject(_program, id)");
  }
  for(const GLuint& id : geometry_shaders) {
    opengl->glAttachShader(_program, id);
    REPORT_GL_ERROR("glAttachObject(_program, id)");
  }
  // Link shader
  opengl->glLinkProgram(_program);
  REPORT_GL_ERROR("glLinkProgram(_program)");

  int status;
  opengl->glGetProgramiv(_program, GL_LINK_STATUS, &status);
  if(!status) {
    if(verbosity > 0)
      err << "Error while linking program:" << endl;
    printProgramInfoLog(_program);
    return false;
  }
  _initialized = true;
  return true;
}

bool reportGLError(QString id, const char* file, int line)
{
  GLenum error = glGetError();
  if(error) {
    err << "OpenGL error in file " << file << " on line " << line << " for command :\n - " << id << ":" << endl
        << (char*)gluErrorString(error) << endl;
    return true;
  }
  return false;
}

bool reportGLError(const char* id, const char* file, int line)
{
  return reportGLError(QString::fromLocal8Bit(id), file, line);
}

QString Shader::shaderTypeName(GLenum shader_type)
{
  switch(shader_type) {
  case GL_VERTEX_SHADER:
    return "vertex";
  case GL_FRAGMENT_SHADER:
    return "fragment";
  case GL_GEOMETRY_SHADER:
    return "geometry";
  default:
    return "unknown";
  }
}

GLuint Shader::compileShaderFile(GLenum shader_type, QString filename)
{
  if(!hasShaders())
    return 0;
  QFile f(filename);
  if(!f.open(QIODevice::ReadOnly)) {
    err << "Cannot open file " << f.fileName() << endl;
    return 0;
  }
  QTextStream ts(&f);
  QString content = ts.readAll();
  return compileShader(shader_type, content);
}

void Shader::printProgramInfoLog(GLuint object)
{
  if(!hasShaders())
    return;

  GLsizei log_length;
  opengl->glGetProgramiv(object, GL_INFO_LOG_LENGTH, &log_length);
  if(log_length > 0) {
    std::vector<char> cs(log_length + 1);
    GLsizei l;
    opengl->glGetProgramInfoLog(object, log_length, &l, &cs[0]);
    err << "*** START LOGS ***\n";
    err << &cs[0] << "\n*** END LOGS ***" << endl;
  }
  else
    err << "*** EMPTY LOG ***" << endl;
}

void Shader::printShaderInfoLog(GLuint object)
{
  if(!hasShaders())
    return;

  GLsizei log_length;
  opengl->glGetShaderiv(object, GL_INFO_LOG_LENGTH, &log_length);
  if(log_length > 0) {
    std::vector<char> cs(log_length + 1);
    GLsizei l;
    opengl->glGetShaderInfoLog(object, log_length, &l, &cs[0]);
    err << "*** START LOGS ***\n";
    err << &cs[0] << "\n*** END LOGS ***" << endl;
  }
  else
    err << "*** EMPTY LOG ***" << endl;
}

GLuint Shader::compileShader(GLenum shader_type, QString content)
{
  if(!hasShaders())
    return 0;
  int shader = opengl->glCreateShader(shader_type);
  QByteArray ba = content.toLocal8Bit();
  const char* src = ba;
  GLint size = content.size();
  opengl->glShaderSource(shader, 1, (const GLchar**)(&src), &size);
  opengl->glCompileShader(shader);
  REPORT_GL_ERROR("glCompileShader(shader)");
  int status;
  opengl->glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
  if(!status) {
    err << "Error while compiling this " << shaderTypeName(shader_type) << " shader: " << endl
        << "***** Beginning of shader *****" << endl
        << src << endl
        << "***** End of shader *****" << endl;
    printShaderInfoLog(shader);
    return 0;
  }
  return shader;
}

bool Shader::useShaders(bool validate)
{
  if(hasShaders() and _program) {
    opengl->glUseProgram(_program);
    setupUniforms();
    if(validate) {
      opengl->glValidateProgram(_program);
      int result;
      opengl->glGetProgramiv(_program, GL_VALIDATE_STATUS, &result);
      if(result != GL_TRUE) {
        if(verbosity > 0) {
          err << "Error validating shader program:" << endl;
          printProgramInfoLog(_program);
        }
        return false;
      }
    }
    return true;
  }
  else if(verbosity > 1)
    err << "Warning, trying to use a shader without program" << endl;
  return false;
}

bool Shader::stopUsingShaders()
{
  opengl->glUseProgram(0);
  return true;
}

QTextStream& GLSLValue::write(QTextStream& s) const
{
  if(!value) {
    s << "invalid value";
    return s;
  }
  switch(type) {
  case GLSL_INT:
    s << "int ";
    break;
  case GLSL_INT2:
    s << "ivec2 ";
    break;
  case GLSL_INT3:
    s << "ivec3 ";
    break;
  case GLSL_INT4:
    s << "ivec4 ";
    break;
  case GLSL_FLOAT:
    s << "float ";
    break;
  case GLSL_FLOAT2:
    s << "vec2 ";
    break;
  case GLSL_FLOAT3:
    s << "vec3 ";
    break;
  case GLSL_FLOAT4:
    s << "vec4 ";
    break;
  case GLSL_MATRIX2:
    s << "mat2 ";
    break;
  case GLSL_MATRIX3:
    s << "mat3 ";
    break;
  case GLSL_MATRIX4:
    s << "mat4 ";
    break;
  default:
    s << "Unkown type: " << type;
  }
  return value->write(s);
}

std::ostream& GLSLValue::write(std::ostream& s) const
{
  if(!value) {
    s << "invalid value";
    return s;
  }
  switch(type) {
  case GLSL_INT:
    s << "int ";
    break;
  case GLSL_INT2:
    s << "ivec2 ";
    break;
  case GLSL_INT3:
    s << "ivec3 ";
    break;
  case GLSL_INT4:
    s << "ivec4 ";
    break;
  case GLSL_UINT:
    s << "uint ";
    break;
  case GLSL_UINT2:
    s << "uvec2 ";
    break;
  case GLSL_UINT3:
    s << "uvec3 ";
    break;
  case GLSL_UINT4:
    s << "uvec4 ";
    break;
  case GLSL_FLOAT:
    s << "float ";
    break;
  case GLSL_FLOAT2:
    s << "vec2 ";
    break;
  case GLSL_FLOAT3:
    s << "vec3 ";
    break;
  case GLSL_FLOAT4:
    s << "vec4 ";
    break;
  case GLSL_MATRIX2:
    s << "mat2 ";
    break;
  case GLSL_MATRIX3:
    s << "mat3 ";
    break;
  case GLSL_MATRIX4:
    s << "mat4 ";
    break;
  default:
    s << "Unkown type: " << type;
  }
  return value->write(s);
}

QTextStream& GLSLValue::read(QTextStream& s)
{
  QString str;
  s >> str;
  delete value;
  if(str == "int") {
    type = GLSL_INT;
    setValue(ivec1());
  } else if(str == "ivec2") {
    type = GLSL_INT2;
    setValue(ivec2());
  } else if(str == "ivec3") {
    type = GLSL_INT3;
    setValue(ivec3());
  } else if(str == "ivec4") {
    type = GLSL_INT4;
    setValue(ivec4());
  } else if(str == "uint") {
    type = GLSL_UINT;
    setValue(uvec1());
  } else if(str == "uvec2") {
    type = GLSL_UINT2;
    setValue(uvec2());
  } else if(str == "uvec3") {
    type = GLSL_UINT3;
    setValue(uvec3());
  } else if(str == "uvec4") {
    type = GLSL_UINT4;
    setValue(uvec4());
  } else if(str == "float") {
    type = GLSL_FLOAT;
    setValue(vec1());
  } else if(str == "vec2") {
    type = GLSL_FLOAT2;
    setValue(vec2());
  } else if(str == "vec3") {
    type = GLSL_FLOAT3;
    setValue(vec3());
  } else if(str == "vec4") {
    type = GLSL_FLOAT4;
    setValue(vec4());
  } else if(str == "mat2") {
    type = GLSL_MATRIX2;
    setValue(mat2());
  } else if(str == "mat3") {
    type = GLSL_MATRIX3;
    setValue(mat3());
  } else if(str == "mat4") {
    type = GLSL_MATRIX4;
    setValue(mat4());
  } else {
    value = 0;
    err << "Error, uniform type '" << str << "' not recognised" << endl;
    return s;
  }
  return value->read(s);
}

std::istream& GLSLValue::read(std::istream& s)
{
  std::string str;
  s >> str;
  delete value;
  if(str == "int") {
    type = GLSL_INT;
    setValue(ivec1());
  } else if(str == "ivec2") {
    type = GLSL_INT2;
    setValue(ivec2());
  } else if(str == "ivec3") {
    type = GLSL_INT3;
    setValue(ivec3());
  } else if(str == "ivec4") {
    type = GLSL_INT4;
    setValue(ivec4());
  } else if(str == "uint") {
    type = GLSL_UINT;
    setValue(uvec1());
  } else if(str == "uvec2") {
    type = GLSL_UINT2;
    setValue(uvec2());
  } else if(str == "uvec3") {
    type = GLSL_UINT3;
    setValue(uvec3());
  } else if(str == "uvec4") {
    type = GLSL_UINT4;
    setValue(uvec4());
  } else if(str == "float") {
    type = GLSL_FLOAT;
    setValue(vec1());
  } else if(str == "vec2") {
    type = GLSL_FLOAT2;
    setValue(vec2());
  } else if(str == "vec3") {
    type = GLSL_FLOAT3;
    setValue(vec3());
  } else if(str == "vec4") {
    type = GLSL_FLOAT4;
    setValue(vec4());
  } else if(str == "mat2") {
    type = GLSL_MATRIX2;
    setValue(mat2());
  } else if(str == "mat3") {
    type = GLSL_MATRIX3;
    setValue(mat3());
  } else if(str == "mat4") {
    type = GLSL_MATRIX4;
    setValue(mat4());
  } else {
    value = 0;
    err << "Error, uniform type '" << QString::fromStdString(str) << "' not recognised" << endl;
    return s;
  }
  return value->read(s);
}

namespace {
QString intTypeFunction(GLint i)
{
  if(i == 1)
    return "int";
  else
    return QString("ivec%d").arg(i);
}

QString uintTypeFunction(GLint i)
{
  if(i == 1)
    return "uint";
  else
    return QString("uvec%d").arg(i);
}

QString floatTypeFunction(GLint i)
{
  if(i == 1)
    return "float";
  else
    return QString("vec%d").arg(i);
}

QString intGLSLValue(GLint i, const int* values)
{
  if(i == 1)
    return QString::number(*values);
  else {
    QStringList lst;
    for(int n = 0; n < i; ++n)
      lst << QString::number(values[n]);
    return QString("ivec%d(%s)").arg(i).arg(lst.join(", "));
  }
}

QString uintGLSLValue(GLint i, const uint* values)
{
  if(i == 1)
    return QString::number(*values);
  else {
    QStringList lst;
    for(int n = 0; n < i; ++n)
      lst << QString::number(values[n]);
    return QString("uvec%d(%s)").arg(i).arg(lst.join(", "));
  }
}

QString floatGLSLValue(GLint i, const float* values)
{
  if(i == 1)
    return QString::number(*values);
  else {
    QStringList lst;
    for(int n = 0; n < i; ++n)
      lst << QString::number(values[n]);
    return QString("vec%d(%s)").arg(i).arg(lst.join(", "));
  }
}
} // namespace

void GLSLValue::setValue(const GLint* val, int count)
{
  type = GLSL_INT;
  value = new ValueImpl<ivec1>(glUniform1iv_, 0, intTypeFunction, intGLSLValue, (const ivec1*)val, count);
}

void GLSLValue::setValue(const ivec1* val, int count)
{
  type = GLSL_INT;
  value = new ValueImpl<ivec1>(glUniform1iv_, glVertexAttribI1iv_, intTypeFunction, intGLSLValue, val, count);
}

void GLSLValue::setValue(const ivec2* val, int count)
{
  type = GLSL_INT2;
  value = new ValueImpl<ivec2>(glUniform2iv_, glVertexAttribI2iv_, intTypeFunction, intGLSLValue, val, count);
}

void GLSLValue::setValue(const ivec3* val, int count)
{
  type = GLSL_INT3;
  value = new ValueImpl<ivec3>(glUniform3iv_, glVertexAttribI3iv_, intTypeFunction, intGLSLValue, val, count);
}

void GLSLValue::setValue(const ivec4* val, int count)
{
  type = GLSL_INT4;
  value = new ValueImpl<ivec4>(glUniform4iv_, glVertexAttribI4iv_, intTypeFunction, intGLSLValue, val, count);
}

void GLSLValue::setValue(const GLuint* val, int count)
{
  type = GLSL_UINT;
  value = new ValueImpl<uvec1>(glUniform1uiv_, 0, uintTypeFunction, uintGLSLValue, (const uvec1*)val, count);
}

void GLSLValue::setValue(const uvec1* val, int count)
{
  type = GLSL_UINT;
  value = new ValueImpl<uvec1>(glUniform1uiv_, glVertexAttribI1uiv_, uintTypeFunction, uintGLSLValue, val, count);
}

void GLSLValue::setValue(const uvec2* val, int count)
{
  type = GLSL_UINT2;
  value = new ValueImpl<uvec2>(glUniform2uiv_, glVertexAttribI2uiv_, uintTypeFunction, uintGLSLValue, val, count);
}

void GLSLValue::setValue(const uvec3* val, int count)
{
  type = GLSL_UINT3;
  value = new ValueImpl<uvec3>(glUniform3uiv_, glVertexAttribI3uiv_, uintTypeFunction, uintGLSLValue, val, count);
}

void GLSLValue::setValue(const uvec4* val, int count)
{
  type = GLSL_UINT4;
  value = new ValueImpl<uvec4>(glUniform4uiv_, glVertexAttribI4uiv_, uintTypeFunction, uintGLSLValue, val, count);
}

void GLSLValue::setValue(const GLfloat* val, int count)
{
  type = GLSL_FLOAT;
  value = new ValueImpl<vec1>(
      glUniform1fv_, glVertexAttrib1fv_, floatTypeFunction, floatGLSLValue, (const vec1*)val, count);
}

void GLSLValue::setValue(const vec1* val, int count)
{
  type = GLSL_FLOAT;
  value = new ValueImpl<vec1>(glUniform1fv_, glVertexAttrib1fv_, floatTypeFunction, floatGLSLValue, val, count);
}

void GLSLValue::setValue(const vec2* val, int count)
{
  type = GLSL_FLOAT2;
  value = new ValueImpl<vec2>(glUniform2fv_, glVertexAttrib2fv_, floatTypeFunction, floatGLSLValue, val, count);
}

void GLSLValue::setValue(const vec3* val, int count)
{
  type = GLSL_FLOAT3;
  value = new ValueImpl<vec3>(glUniform3fv_, glVertexAttrib3fv_, floatTypeFunction, floatGLSLValue, val, count);
}

void GLSLValue::setValue(const vec4* val, int count)
{
  type = GLSL_FLOAT4;
  value = new ValueImpl<vec4>(glUniform4fv_, glVertexAttrib4fv_, floatTypeFunction, floatGLSLValue, val, count);
}

void GLSLValue::setValue(const mat2* val, int count)
{
  type = GLSL_MATRIX2;
  value = new ValueImpl<mat2>(glUniformTransposedMatrix2fv_, 0, 0, 0, val, count);
}

void GLSLValue::setValue(const mat3* val, int count)
{
  type = GLSL_MATRIX3;
  value = new ValueImpl<mat3>(glUniformTransposedMatrix3fv_, 0, 0, 0, val, count);
}

void GLSLValue::setValue(const mat4* val, int count)
{
  type = GLSL_MATRIX4;
  value = new ValueImpl<mat4>(glUniformTransposedMatrix4fv_, 0, 0, 0, val, count);
}

bool Shader::removeUniform(QString name)
{
  for(size_t i = 0; i < model_uniform_names.size(); ++i) {
    if(model_uniform_names[i] == name) {
      model_uniform_values.erase(model_uniform_values.begin() + i);
      model_uniform_names.erase(model_uniform_names.begin() + i);
      return true;
    }
  }
  return false;
}

bool Shader::setUniform(QString name, const GLSLValue& value)
{
  bool found = false;
  for(size_t i = 0; i < model_uniform_names.size(); ++i) {
    if(model_uniform_names[i] == name) {
      model_uniform_values[i] = value;
      found = true;
    }
  }
  if(!found) {
    model_uniform_names.push_back(name);
    model_uniform_values.push_back(value);
  }
  return found;
}

bool Shader::setUniform_instant(QString name, const GLSLValue& value)
{
  return loadUniform(_program, name, value);
}

GLint Shader::uniformLocation(QString name)
{
  return opengl->glGetUniformLocation(_program, name.toLocal8Bit());
}

bool Shader::loadUniform(GLint program, QString name, const GLSLValue& value)
{
  GLint loc = opengl->glGetUniformLocation(program, name.toLocal8Bit());
  if(loc != -1) {
    value.setUniform(loc);
    REPORT_GL_ERROR(QString("setUniform(%1)").arg(name));
    return true;
  }
  if(verbosity > 1) {
    err << "Error, uniform '" << name << "' was not found in the program" << endl;
  }
  return false;
}

//  void Shaders::setupLocations()
//  {
//    if(hasShaders() and _program)
//    {
//      attribs_locations.clear();
//      attribs_locations.resize(attribs_names.size(), -1);
//      for(size_t i = 0 ; i < attribs_names.size() ; ++i)
//      {
//        GLint loc = glGetAttribLocation(_program, attribs_names[i]);
//        if(loc >= 0)
//        {
//          attribs_locations[i] = loc;
//        }
//        else if(verbosity > 1)
//        {
//          err << "Error, attributes '" << attribs_names[i] <<
//          "' not found in shader" << endl;
//        }
//      }
//    }
//  }

GLint Shader::attribLocation(QString name)
{
  return opengl->glGetAttribLocation(_program, name.toLocal8Bit());
}

bool Shader::setAttrib(QString name, const GLSLValue& value)
{
  GLint loc = attribLocation(name);
  if(loc < 0)
    return false;
  setAttrib(loc, value);
  return true;
}

void Shader::setAttrib(GLint loc, const GLSLValue& value)
{
  value.setAttrib(loc);
}

void Shader::setupUniforms()
{
  if(hasShaders() and _program) {
    QSet<QString> done_uniforms;
    std::vector<size_t> to_delete;
    for(size_t i = 0; i < model_uniform_names.size(); ++i) {
      QString name = model_uniform_names[i];
      if(!done_uniforms.contains(name)) {
        if(!loadUniform(_program, name, model_uniform_values[i]))
          to_delete.push_back(i);
        else if(verbosity > 4)
          err << "Loading uniform '" << name << "' with value " << model_uniform_values[i] << endl;
        done_uniforms.insert(name);
      }
      else if(verbosity > 2) {
        err << "Warning: uniform " << name << " defined more than once" << endl;
      }
    }
    for(size_t i = 0 ; i < to_delete.size(); ++i) {
      size_t pos = to_delete.size() - i - 1;
      model_uniform_names.erase(model_uniform_names.begin() + to_delete[pos]);
      model_uniform_values.erase(model_uniform_values.begin() + to_delete[pos]);
    }
    to_delete.clear();
    for(size_t i = 0; i < uniform_names.size(); ++i) {
      QString name = uniform_names[i];
      if(!done_uniforms.contains(name)) {
        loadUniform(_program, name, uniform_values[i]);
        if(verbosity > 4)
          err << "Loading uniform '" << name << "' with value " << uniform_values[i] << endl;
        done_uniforms.insert(name);
      } else if(verbosity > 2) {
        err << "Warning: uniform '" << name << "' overriden by model or defined more than once" << endl;
      }
    }
    for(size_t i = 0; i < to_delete.size(); ++i) {
      size_t pos = to_delete.size() - i - 1;
      uniform_names.erase(uniform_names.begin() + to_delete[pos]);
      uniform_values.erase(uniform_values.begin() + to_delete[pos]);
    }
  } else if(verbosity > 1) {
    err << "Warning: trying to setup uniforms without having a running program" << endl;
  }
}

void Shader::setConstant(QString name, const GLSLValue& value)
{
  auto found = constants.find(name);
  if(found != constants.end()) {
    if(found.value() != value) {
      found.value() = value;
      invalidate();
    }
  }
  else {
    constants[name] = value;
    invalidate();
  }
}

void Shader::removeConstant(QString name)
{
  if(constants.contains(name)) {
    constants.remove(name);
    invalidate();
  }
}

QStringList Shader::constantsNames() { return constants.keys(); }

const GLSLValue& Shader::getConstant(QString name) const
{
  auto found = constants.find(name);
  if(found != constants.end())
    return found.value();
  return null_value;
}

void Shader::setDefinition(QString name, QString value)
{
  auto found = definitions.find(name);
  if(found != definitions.end()) {
    if(found.value() != value) {
      found.value() = value;
      invalidate();
    }
  }
  else {
    definitions[name] = value;
    invalidate();
  }
}

void Shader::removeDefinition(QString name)
{
  if(definitions.contains(name)) {
    definitions.remove(name);
    invalidate();
  }
}

QStringList Shader::definitionsNames() { return definitions.keys(); }

const QString& Shader::getDefinition(QString name) const
{
  auto found = definitions.find(name);
  if(found != definitions.end())
    return found.value();
  return null_string;
}

} // namespace lgx
