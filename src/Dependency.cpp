/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "Dependency.hpp"

#include <Information.hpp>
#include <PackageManager.hpp>

#include <QRegularExpression>
#include <QList>

namespace lgx {

using util::Version;

namespace {

// Write relation to a version as a string, adding leading space if not empty
QString relationToString(Relationship rel, const Version& version)
{
  switch(rel) {
    case Relationship::Lesser:
      return " (< " + version.toString() + ")";
    case Relationship::LesserEqual:
      return " (<= " + version.toString() + ")";
    case Relationship::Equal:
      return " (== " + version.toString() + ")";
    case Relationship::GreaterEqual:
      return " (>= " + version.toString() + ")";
    case Relationship::Greater:
      return " (> " + version.toString() + ")";
    case Relationship::Any:
      return "";
  }
}


Relationship relationFromString(const QString& s)
{
  if(s == "==")
    return Relationship::Equal;
  if(s == "<")
    return Relationship::Lesser;
  if(s == ">")
    return Relationship::Greater;
  if(s == "<=")
    return Relationship::LesserEqual;
  if(s == ">=")
    return Relationship::GreaterEqual;
  return Relationship::Any;
}

// Parse a dependency description

enum class TokenType {
  OpenBracket,
  CloseBracket,
  And,
  Or,
  Text,
  Relation,
  Item,
  Group,
  OrGroup,
  AndGroup
};

class Token {
public:
  Token(int p) : pos(p) { }
  virtual ~Token() { }
  virtual TokenType type() const = 0;
  virtual QString text() const = 0;
  int pos;
};

class OpenBracketToken : public Token {
public:
  OpenBracketToken(int p)
    : Token(p)
  { }

  ~OpenBracketToken() override { }
  TokenType type() const override { return TokenType::OpenBracket; }
  QString text() const override { return "("; }
};

class CloseBracketToken : public Token {
public:
  CloseBracketToken(int p)
    : Token(p)
  { }

  ~CloseBracketToken() override { }
  TokenType type() const override { return TokenType::CloseBracket; }
  QString text() const override { return ")"; }
};

class AndToken : public Token {
public:
  AndToken(int p)
    : Token(p)
  { }

  ~AndToken() override { }
  TokenType type() const override { return TokenType::And; }
  QString text() const override { return ","; }
};

class OrToken : public Token {
public:
  OrToken(int p)
    : Token(p)
  { }

  ~OrToken() override { }
  TokenType type() const override { return TokenType::Or; }
  QString text() const override { return "|"; }
};

class TextToken : public Token {
public:
  TextToken(QString txt, int p)
    : Token(p)
    , _text(txt)
  { }

  ~TextToken() override { }
  TokenType type() const override { return TokenType::Text; }
  QString text() const override { return _text; }

  QString _text;
};

class RelationToken : public Token {
public:
  RelationToken(QString rel, int p)
    : Token(p)
    , _rel(rel)
  { }

  ~RelationToken() override { }
  TokenType type() const override { return TokenType::Relation; }
  QString text() const override { return _rel; }

  QString _rel;
};

class DependencyToken : public Token {
public:
  DependencyToken(int p)
    : Token(p)
  { }

  virtual std::shared_ptr<Dependency> dependency() const = 0;
  QString text() const override {
    return dependency()->toString();
  }
};

class ItemToken : public DependencyToken {
public:
  ItemToken(QString name, Version version, Relationship rel, int p)
    : DependencyToken(p)
    , _dep(std::make_shared<DependVersion>(name, version, rel))
  { }

  std::shared_ptr<Dependency> dependency() const override {
    return _dep;
  }
  TokenType type() const override { return TokenType::Item; }

  std::shared_ptr<DependVersion> _dep;
};


class GroupToken : public DependencyToken {
public:
  GroupToken(std::shared_ptr<Dependency> dep, int p)
    : DependencyToken(p)
    , _dep(dep)
  { }

  std::shared_ptr<Dependency> dependency() const override {
    return _dep;
  }
  TokenType type() const override { return TokenType::Group; }

  std::shared_ptr<Dependency> _dep;
};


class OrGroupToken : public DependencyToken {
public:
  OrGroupToken(int p)
    : DependencyToken(p)
    , _dep(std::make_shared<DependAny>())
  { }

  TokenType type() const override { return TokenType::OrGroup; }
  std::shared_ptr<Dependency> dependency() const override {
    return _dep;
  }

  std::shared_ptr<DependAny> dependGroup() {
    return _dep;
  }

  std::shared_ptr<DependAny> _dep;
};

class AndGroupToken : public DependencyToken {
public:
  AndGroupToken(int p)
    : DependencyToken(p)
    , _dep(std::make_shared<DependAll>())
  { }

  TokenType type() const override { return TokenType::AndGroup; }
  std::shared_ptr<Dependency> dependency() const override {
    return _dep;
  }

  std::shared_ptr<DependAll> dependGroup() {
    return _dep;
  }

  std::shared_ptr<DependAll> _dep;
};

QString toString(TokenType t)
{
  switch(t) {
    case TokenType::OpenBracket:
      return "OpenBracket";
    case TokenType::CloseBracket:
      return "CloseBracket";
    case TokenType::And:
      return "And";
    case TokenType::Or:
      return "Or";
    case TokenType::Text:
      return "Text";
    case TokenType::Group:
      return "Group";
    case TokenType::Relation:
      return "Relation";
    case TokenType::Item:
      return "Item";
    case TokenType::OrGroup:
      return "OrGroup";
    case TokenType::AndGroup:
      return "AndGroup";
  }
}


BadTokenDependencyException badTokenDependencyException(const Token& tok, const QString& expectedToken)
{
  return BadTokenDependencyException(toString(tok.type()), tok.text(), tok.pos, expectedToken);
}

// Tokenizer returns a list of names, version, relationships, brackets, comas and vertical bars
QList<std::shared_ptr<Token>> tokenizeDependencies(QString expr)
{
  // First, remove spaces as they don't matter
  static const QRegularExpression token = QRegularExpression(R"((,|\||\(|\)|<=|>=|==|<|>|[^,|()<>=[:space:]]+|[[:space:]]+))");
  QList<std::shared_ptr<Token>> result;
  int pos = 0;
  auto n = expr.size();
  while(pos < n) {
    auto m = token.match(expr, pos, QRegularExpression::NormalMatch, QRegularExpression::AnchoredMatchOption);
    if(m.capturedLength() == 0)
      throw InvalidDependencyException(pos, expr);
    auto c = m.captured();
    if(not c[0].isSpace()) {
      std::shared_ptr<Token> tok;
      if(c == "(")
        tok = std::make_shared<OpenBracketToken>(pos);
      else if(c == ")")
        tok = std::make_shared<CloseBracketToken>(pos);
      else if(c == "|")
        tok = std::make_shared<OrToken>(pos);
      else if(c == ",")
        tok = std::make_shared<AndToken>(pos);
      else if(c == "<" or c == ">" or
              c == "<=" or c == ">=" or
              c == "==")
        tok = std::make_shared<RelationToken>(c, pos);
      else
        tok = std::make_shared<TextToken>(c, pos);
      result << tok;
    }
    pos += c.size();
  }
  return result;
}

void parseFront(QList<std::shared_ptr<Token>>& deps);

void parseOrGroup(std::shared_ptr<Token> tok, QList<std::shared_ptr<Token>>& deps)
{
  deps.pop_front();
  deps.pop_front();
  parseFront(deps);
  if(deps.empty())
    throw UnfinishedDependencyException();
  auto otherTok = std::dynamic_pointer_cast<DependencyToken>(deps[0]);
  if(!otherTok)
    throw badTokenDependencyException(*deps[0], "Item|Group|OrGroup|AndGroup");
  std::shared_ptr<OrGroupToken> new_tok;
  if(tok->type() == TokenType::OrGroup)
    new_tok = std::static_pointer_cast<OrGroupToken>(tok);
  else {
    new_tok = std::make_shared<OrGroupToken>(tok->pos);
    new_tok->dependGroup()->addDependency(clone(*std::static_pointer_cast<ItemToken>(tok)->dependency()));
  }
  new_tok->dependGroup()->addDependency(clone(*otherTok->dependency()));
  deps[0] = new_tok;
}

void parseAndGroup(std::shared_ptr<Token> tok, QList<std::shared_ptr<Token>>& deps)
{
  deps.pop_front();
  deps.pop_front();
  parseFront(deps);
  if(deps.empty())
    throw UnfinishedDependencyException();
  while(deps.size() > 1 and
        deps[1]->type() != TokenType::And and
        deps[1]->type() != TokenType::CloseBracket) {
    parseFront(deps);
  }
  auto otherTok = std::dynamic_pointer_cast<DependencyToken>(deps[0]);
  if(!otherTok)
    throw badTokenDependencyException(*deps[0], "Item|Group|OrGroup|AndGroup");
  std::shared_ptr<AndGroupToken> new_tok;
  if(tok->type() == TokenType::AndGroup)
    new_tok = std::static_pointer_cast<AndGroupToken>(tok);
  else {
    new_tok = std::make_shared<AndGroupToken>(tok->pos);
    new_tok->dependGroup()->addDependency(clone(*std::static_pointer_cast<ItemToken>(tok)->dependency()));
  }
  new_tok->dependGroup()->addDependency(clone(*otherTok->dependency()));
  deps[0] = new_tok;
}

void parseFront(QList<std::shared_ptr<Token>>& deps) {
  if(deps.empty())
    throw UnfinishedDependencyException();
  auto tok = deps.front();
  switch(tok->type()) {
    case TokenType::OpenBracket:
      {
        deps.pop_front();
        parseFront(deps);
        while(deps.size() >= 2 and deps[1]->type() != TokenType::CloseBracket)
          parseFront(deps);
        if(deps.size() < 2)
          throw UnfinishedDependencyException();
        auto dep_tok = std::dynamic_pointer_cast<DependencyToken>(deps.front());
        if(not dep_tok)
          throw badTokenDependencyException(*deps.front(), "Item|Group|OrGroup|AndGroup");
        auto new_tok = std::make_shared<GroupToken>(dep_tok->dependency(), dep_tok->pos);
        deps.pop_front();
        deps[0] = new_tok;
      }
      return;
    case TokenType::Text:
      if(deps.size() == 1 or deps[1]->type() != TokenType::OpenBracket) {
        deps[0] = std::make_shared<ItemToken>(tok->text(), Version(), Relationship::Any, tok->pos);
        return;
      }
      {
        if(deps.size() < 5)
          throw UnfinishedDependencyException();
        if(deps[2]->type() != TokenType::Relation)
          throw badTokenDependencyException(*deps[2], "Relation");
        if(deps[3]->type() != TokenType::Text)
          throw badTokenDependencyException(*deps[3], "Version");
        Version ver(deps[3]->text());
        if(not ver.valid())
          throw badTokenDependencyException(*deps[3], "Version");
        if(deps[4]->type() != TokenType::CloseBracket)
          throw badTokenDependencyException(*deps[4], "CloseBracket");
        auto new_tok = std::make_shared<ItemToken>(tok->text(), ver, relationFromString(deps[2]->text()), tok->pos);
        deps.pop_front(); // name
        deps.pop_front(); // bracket
        deps.pop_front(); // rel
        deps.pop_front(); // version
        deps[0] = new_tok; // end bracket
      }
      return;
    case TokenType::Item:
    case TokenType::Group:
    case TokenType::OrGroup:
      {
        if(deps.size() == 1) return;
        switch(deps[1]->type()) {
          case TokenType::Or:
            parseOrGroup(tok, deps);
            return;
          case TokenType::And:
            parseAndGroup(tok, deps);
            return;
          case TokenType::CloseBracket:
            return;
          default:
            throw badTokenDependencyException(*deps[1], "Or|And|CloseBracket");
        }
      }
      return;
    case TokenType::AndGroup:
      {
        if(deps.size() == 1) return;
        switch(deps[1]->type()) {
          case TokenType::And:
            parseAndGroup(tok, deps);
            return;
          case TokenType::CloseBracket:
            return;
          default:
            throw badTokenDependencyException(*deps[1], "And|CloseBracket");
        }
      }
      return;
    case TokenType::CloseBracket:
      return;
    case TokenType::And:
    case TokenType::Or:
    case TokenType::Relation:
      throw badTokenDependencyException(*tok, "OpenBracket|Text|Item|Group|OrGroup|AndGroup|CloseBracket");
  }
}

std::shared_ptr<Dependency> parseDependencies(QList<std::shared_ptr<Token>> deps)
{
  auto cur_size = deps.size();
  while(deps.size() > 1) {
    parseFront(deps);
    if(deps.size() >= cur_size) {
      Information::err << "Error, cannot reduce the size of the dependency tokens" << endl;
      throw UnfinishedDependencyException();
    }
  }
  if(deps.empty())
    throw UnfinishedDependencyException();
  auto dep = std::dynamic_pointer_cast<DependencyToken>(deps.front());
  if(dep)
    return dep->dependency();
  throw UnfinishedDependencyException();
}

} // namespace


InvalidDependencyException::InvalidDependencyException(int pos, const QString& s)
  : std::exception()
{
  msg = QString("Invalid dependency syntax at pos %1:\n%2").arg(pos).arg(s).toUtf8();
}

InvalidDependencyException::~InvalidDependencyException() throw()
{ }

const char* InvalidDependencyException::what() const throw()
{
  return msg.data();
}

BadTokenDependencyException::BadTokenDependencyException(const QString& tokenType, const QString& tokenText,
                                                         int pos, const QString& expectedTokenType)
  : std::exception()
{
  msg = QString("Invalid token at pos %1 within context: %2 (%3) - expected: %4").arg(pos)
    .arg(tokenType).arg(tokenText).arg(expectedTokenType).toUtf8();
}

BadTokenDependencyException::~BadTokenDependencyException() throw()
{ }

const char* BadTokenDependencyException::what() const throw()
{
  return msg.data();
}

UnfinishedDependencyException::UnfinishedDependencyException()
  : std::exception()
{
  msg = QString("Dependency definition ends in the middle of a definition").toUtf8();
}

UnfinishedDependencyException::~UnfinishedDependencyException() throw()
{ }

const char* UnfinishedDependencyException::what() const throw()
{
  return msg.data();
}

std::unique_ptr<Dependency> parseDependency(QString s)
{
  auto tokens = tokenizeDependencies(s);
  auto result = parseDependencies(tokens);
  return clone(*result);
}

DependAll* DependAll::copy() const
{
  auto ptr = std::make_unique<DependAll>();
  for(const auto& dep : dependencies)
    ptr->addDependency(clone(*dep));
  return ptr.release();
}

void DependAll::addDependency(std::unique_ptr<Dependency>&& dep)
{
  dependencies.emplace_back(std::move(dep));
}

void DependAll::addDependency(std::shared_ptr<Dependency> dep)
{
  dependencies.emplace_back(clone(*dep));
}

void DependAll::insertDependency(int pos, std::unique_ptr<Dependency>&& dep) {
  dependencies.insert(dependencies.begin(), std::move(dep));
}

void DependAll::insertDependency(int pos, std::shared_ptr<Dependency> dep) {
  dependencies.insert(dependencies.begin(), clone(*dep));
}

QString DependAll::toString(bool embedded) const
{
  QStringList sub;
  for(const auto& dep: dependencies)
    sub << dep->toString(false);
  auto sol = sub.join(", ");
  if(embedded)
    return "(" + sol + ")";
  return sol;
}

bool DependAll::check(const PackageManager& manager, bool user) const
{
  for(const auto& dep: dependencies) {
    if(not dep->check(manager, user))
      return false;
  }
  return true;
}

DependAny* DependAny::copy() const
{
  auto ptr = std::make_unique<DependAny>();
  for(const auto& dep : dependencies)
    ptr->addDependency(clone(*dep));
  return ptr.release();
}

void DependAny::addDependency(std::unique_ptr<Dependency>&& dep)
{
  dependencies.emplace_back(std::move(dep));
}

void DependAny::addDependency(std::shared_ptr<Dependency> dep)
{
  dependencies.emplace_back(clone(*dep));
}

void DependAny::insertDependency(int pos, std::unique_ptr<Dependency>&& dep) {
  dependencies.insert(dependencies.begin(), std::move(dep));
}

void DependAny::insertDependency(int pos, std::shared_ptr<Dependency> dep) {
  dependencies.insert(dependencies.begin(), clone(*dep));
}

QString DependAny::toString(bool embedded) const
{
  QStringList sub;
  for(const auto& dep: dependencies)
    sub << dep->toString(true);
  auto sol = sub.join(" | ");
  if(embedded)
    return "(" + sol + ")";
  return sol;
}

bool DependAny::check(const PackageManager& manager, bool user) const
{
  for(const auto& dep: dependencies) {
    if(dep->check(manager, user))
      return true;
  }
  return false;
}

DependVersion::DependVersion(QString name,
                             Version version,
                             Relationship rel)
  : Dependency()
  , _name(name)
  , _version(version)
  , _rel(rel)
{ }

DependVersion* DependVersion::copy() const
{
  return new DependVersion(_name, _version, _rel);
}

bool DependVersion::check(const PackageManager& manager, bool user) const
{
  if((user and not manager.hasPackage(_name)) or
     (not user and not manager.hasSystemPackage(_name)))
    return false;
  auto pkg = manager.package(_name);
  auto ver = pkg->version();
  switch(_rel) {
    case Relationship::Lesser:
      return ver < _version;
    case Relationship::LesserEqual:
      return ver <= _version;
    case Relationship::Equal:
      return ver == _version;
    case Relationship::GreaterEqual:
      return ver >= _version;
    case Relationship::Greater:
      return ver > _version;
    case Relationship::Any:
      return true;
  }
}

QString DependVersion::toString(bool embedded) const
{
  return QString("%1%2").arg(_name).arg(relationToString(_rel, _version));
}

} // namespace lgx
