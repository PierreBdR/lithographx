from __future__ import print_function, division, absolute_import

from .path import Path
import sys

if sys.version_info.major < 3:
    import codecs

    def readline_unicode(f):
        line = f.readline()
        return codecs.utf_8_decode(line)[0]

    def write_unicode(f, s):
        f.write(codecs.utf_8_encode(s)[0])

else:
    unicode = str

    def readline_unicode(f):
        return f.readline()

    def write_unicode(f, s):
        f.write(s)


class INIFile(object):
    """
    Read a INI or INI-like file.

    :Properties"
        - `filename`: string or None
            Path to the file to load. When set, the file is loaded automatically.
        - `comment`: string
            String identifying the start of a comment
        - `assignment`: string
            String separating a key from its value
    """
    def __init__(self, filename=None, **kwords):
        """
        :Parameters:
            - `filename`: string or None
                Path to a filename to load

        :Keywords:
            - `comment`: string
                String identifying a comment (default: ';')
            - `assignment`: string
                String separating the key from the value (default: '=')
            - `read_project`: bool
                If True, set comment to '//' and assignment to ':'

        """
        self._sections = {}
        self._filename = None
        self.comment = kwords.get("comment", ";")
        self.assignment = kwords.get("assignment", "=")
        if kwords.get("read_project", False):
            self.comment = "//"
            self.assignment = ":"
        if filename is not None:
            self.filename = filename

    def _set_filename(self, filename):
        if filename is None:
            del self.filename
            return
        pth = Path(filename)
        if pth.exists() and pth.isfile():
            self._filename = filename
            self.load()
        else:
            raise ValueError("Path '{0}' doesn't exist or isn't a file".format(filename))

    def _get_filename(self):
        return self._filename

    def _del_filename(self):
        self._filename = None
        self._sections = {}

    filename = property(_get_filename, _set_filename, _del_filename)

    def load(self):
        fn = self._filename
        assignment = self.assignment
        assign_size = len(assignment)
        comment = self.comment
        sections = self._sections
        if fn is None:
            raise ValueError("Error, you need to set a valid filename before trying to load the file")
        with open(fn, "rt") as f:
            current_section = None
            line_num = 0
            while True:
                line = readline_unicode(f)
                line_num += 1
                if not line:
                    break  # Finished reading
                try:
                    idx = line.index(comment)
                    line = line[:idx]
                except ValueError:
                    pass
                line = line.strip()
                if line:
                    if line[0] == '[' and line[-1] == ']':
                        current_section = line[1:-1].strip()
                        sections.setdefault(current_section, {})
                    else:
                        try:
                            idx = line.index(assignment)
                        except ValueError:
                            raise ValueError("Error in file '{0}' on line {1}: "
                                             "the line is neither empty, a section or a key.".format(fn, line_num))
                        key = line[:idx].strip()
                        value = line[idx + assign_size:].strip()
                        sections[current_section].setdefault(key, []).append(value)

    def save(self, filename):
        with open(filename, "wt") as f:
            for sec in self._sections:
                write_unicode(f, u'[{}]\n'.format(sec))
                section = self._sections[sec]
                for k in section:
                    for it in section[k]:
                        write_unicode(f, u'{0}: {1}\n'.format(k, it))

    def sections(self):
        return self._sections.keys()

    def keys(self, section):
        return self._sections[section].keys()

    def all(self, section, key):
        return self._sections[section][key]

    def __getitem__(self, arg):
        section, key = arg
        return self._sections[section][key][-1]

    def __setitem__(self, arg, value):
        section, key = arg
        self._sections.setdefault(section, {})[key] = [str(value)]

    def __delitem__(self, arg):
        try:
            section, key = arg
            del self._sections[section][key]
        except TypeError:
            del self._sections[section]

    def add(self, section, key, value):
        self._sections.setdefault(section, {}).setdefault(key, []).append(str(value))

    def set_all(self, section, key, values):
        self._sections.setdefault(section, {})[key] = [unicode(v) for v in values]
