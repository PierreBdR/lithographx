LithoGraphX CMake package
=========================

First, the module defined the following variables:

``LithoGraphX_FOUND``
    Whether LithoGraphX was found or not
``LithoGraphX_VERSION``
    Version of the installed LithoGraphX
``LithoGRaphX_PROCESS_VERSION``
    Version of the process subsystem
``LithoGraphX_PROGRAM``
    Path to the LithoGraphX program
``LithoGraphX_lgxPack``
    Path to the lgxPack program
``LithoGraphX_LIBRARIES``
    List of libraries to link to LithoGraphX
``LithoGraphX_OS``
    Name of the operating system LithoGraphX has been compiled for
``LithoGraphX_BIN_DIR``
    Folder containing the LithoGraphX program
``LithoGraphX_USER_PROCESS_DIR``
    Folder containing user-installed plugins
``LithoGraphX_PROCESS_DIR``
    Folder containing system-installed plugins
``LithoGraphX_USER_MACRO_DIR``
    Folder containing user-installed macros
``LithoGraphX_MACRO_DIR``
    Folder containing system-installed macros
``LithoGraphX_INCLUDE_DIR``
    Main LithoGraphX include directory
``LithoGraphX_USER_LIB_DIR``
    Folder containing user-installed libraries
``LithoGraphX_LIB_DIR``
    Folder containing system-installed libraries
``LithoGraphX_USER_PACKAGES_DIR``
    Folder containing the definition of user packages
``LithoGraphX_PACKAGES_DIR``
    Folder containing the definition of system packages

Each plug-ins containing support libraries define a component you can include
when importing the package. For example, if you want to call directly the
process defined in the ``SieveFilter`` package, you can use:

.. code-block:: cmake

    find_package(LithoGraphX REQUIRED COMPONENTS SieveFilter)

Then, for each requested module, the following variables will be defined:

``LithoGraphX_${Module}_FOUND``
     Whether the module has been found or not
``LithoGraphX_${Module}_VERSION``
     Version of the module
``LithoGraphX_${Module}_LIBRARIES``
     Libraries to link against
``LithoGraphX_${Module}_INCLUDE_DIRS``
     Folders containing the include files
``LithoGraphX_${Module}_LIB_DIR``
     Folder containing the libraries
``LithoGraphX_${Module}_LOCATION``
     Location of the file ${module}.cmake
``LithoGraphX_${Module}_INSTALL_TYPE``
     ``user``, ``system`` or ``builtin`` depending on how the module was
     installed. If 'builtin', the ``INCLUDE_DIRS``, ``LIB_DIR`` and ``LOCATION`` are
     not defined.

 Also, for each module, a target ``LithoGraphX::${Module}`` is created to use
 in ``TARGET_LINK_LIBRARIES()`` which will set all dependent libraries, include
 directories, ...

 This module also creates the following macro:

``INIT_LGX_PACKAGE()``
  This function analyses the description.ini and creates the following
  variables and call the PROJECT function with the package name as
  argument:

      ``LithoGraphX_PACKAGE_NAME``
          Name of the package
      ``${LithoGraphX_PACKAGE_NAME}_VERSION``
          Version of the package, which MUST be in the form MAJOR.MINOR.PATCH
      ``${LithoGraphX_PACKAGE_NAME}_DESCRIPTION``
          Description of the package
      ``${LithoGraphX_PACKAGE_NAME}_DEPENDENCIES``
          Dependencies of the package

  You can also set these variables by hand and call ``PROJECT()`` yourself.

``LGX_PACKAGE()``
  This function creates the files needed by the package and setup CPack variables.
  Two files may be created:

    1. ``description.ini`` is always created using the variables created by ``INIT_LGX_PACKAGE()``
    2. ``${LithoGraphX_PACKAGE_NAME}.cmake`` is created if the package
        exports at least one library and contains the definition to link
        against it. It uses as a model the file ``Package.cmake`` found
        either in the current folder or in the ``cmake`` directory of
        LithoGraphX. The following variables can be used to influence the generated cmake module:

            ``LithoGraphX_${package}_EXTRA_INCLUDE_DIRS``
                List of directories containing useful include dirs (probably from dependencies)
            ``LithoGraphX_${package}_EXTRA_LIBS``
                List of libraries to link against, in addition to the ones defined in the package.
            ``${package}_HAS_LIBS``
                This is set automatically by LGX_ADD_LIBRARY, but if set
                to false, no CMake module will be generated. You can use
                this to prevent users from linking to your module.

``LGX_FIND_PACKAGE(...)``
  Calls ``FIND_PACKAGE(...)`` but record the call to repeat it in the module.

``LGX_ADD_PLUGIN(target source1 [source2 ...])``
  Add a plugin, compiled from source. It will be installed at the proper place.

``LGX_ADD_MACRO(macro1 [macro2 ...] [STRIP_DIR dir])``
  Add a set of macros to the macro folder. If ``STRIP_DIR`` is specified,
  all files should be in the given folder, which will not be created in
  the macro folder.

``LGX_ADD_LIBRARY(target [SHARED|STATIC] [MAIN] source1 [source2 ...] [PUBLIC_HEADER header1 [header2 ...]])``
  Add a library linked to LithoGraphX and installed in the library folder.
  If no library is marked as MAIN, the last specified one will be the main one.

``LGX_DESCRIPTION(field output_var required)``
  Read the given field from the description.ini file in the local folder
  and store the result in output_var. If required is TRUE and the field
  is not present or empty, this raises an error.

Note: All paths must use forward slash, indenpendent from the system, so
a windows path may look like: ``"C:/Program Files/LithoGraphX/bin"``.


