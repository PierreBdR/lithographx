#ifndef MT_HPP
#define MT_HPP

#include <Process.hpp>

namespace process
{
class MTorientation : public MeshProcess
{
public:
    MTorientation(const MeshProcess& process) // le process va apparaitre dans l'onglet "Mesh" de LGX
        : Process(process)
        , MeshProcess(process)
    { }
    
    bool operator()(const QStringList& strings, const QList<float>& values)
    {
        if (!checkState().mesh(MESH_NON_EMPTY|MESH_SIGNAL)) return false;
        Mesh* m=currentMesh();
        return operator()(m,strings[0], values[0]);
    }
    
    bool operator()(Mesh* m,QString output, float bsize);
    
    QString folder() const { return "Signal"; } // directory in which the plugin will appear in LGX
    QString name() const { return "MT orientation"; }
    QString description() const { return "Computes mean MT orientation per cell, assuming the signal of the mesh is MT signal"; }
    QStringList parameters() const { return QStringList()<<"Output"<<"Border size"; }
    QStringList default_strings() const { return QStringList() << "output.csv"; }
    StringChoiceMap string_choice() const {
        StringChoiceMap map;
        return map;
    }
    QList<float> default_values() const { return QList<float>()<<0.5; }
    QIcon icon() const
    {
        return QIcon(":/images/MT.png");
    }
    
};
}

#endif // MT_HPP


