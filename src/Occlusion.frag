uniform sampler2D depth;
uniform sampler2D color;
uniform sampler2D background; // depth!

smooth centroid in vec2 texPos;

void main()
{
  float d = texture(depth, texPos).r;
  float a = texture(color, texPos).r;
  float bg = texture(background, texPos).r;
  if((d >= bg) || (d >= 0.999999) || (a >= 0.99))
    discard;
  else
    gl_FragColor = vec4(0,1,0,1);
}

