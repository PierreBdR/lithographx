/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef MergeStacks_HPP
#define MergeStacks_HPP

#include <Process.hpp>

#include <Stack.hpp>

namespace lgx {
namespace process {

///\addtogroup StackProcess
///@{
/**
 * \class AlignCanvas MergeStacks.hpp <MergeStacks.hpp>
 *
 * Align two stacks' canvas. Expand the canvas of the first stack so the
 * second one can be drawn onto it. If the 'change both stack' option is
 * selected, then all the stacks are extended and made aligned with the axis
 * of the reference system. Otherwise, the work store of the current stack is
 * replaced with the projection of the other stack onto the extended canvas,
 * keeping the resolution of the current stack.
 */
class LGXCORE_EXPORT AlignCanvas : public StackProcess {
public:
  AlignCanvas(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const ParmList& parms) override
  {
    if(not checkState().store(STORE_NON_EMPTY | STORE_VISIBLE, 0).store(STORE_NON_EMPTY | STORE_VISIBLE, 1))
      return false;
    Stack* current = currentStack();
    Stack* other = stack(1 - current->id());
    if(current->currentStore()->labels() != other->currentStore()->labels())
      return setErrorMessage("Error, one stack is labeled and the other is not.");
    bool change_both = parmToBool(parms[0]);
    bool result = operator()(current, other, change_both);
    if(not change_both and result)
      current->work()->show();
    return result;
  }

  bool operator()(Stack* target, Stack* other, bool change_both);

  QString folder() const override {
    return "Multi-stack";
  }
  QString name() const override {
    return "Align Canvas";
  }
  QString description() const override
  {
    return "Align two stacks' canvas. Expand the canvas of the first stack so the\n"
           "second one can be drawn onto it. If the 'change both stack' option is \n"
           "selected, then all the stacks are extended and made aligned with the \n"
           "axis of the reference system. Otherwise, the work store of the current\n"
           "stack is replaced with the projection of the other stack onto the\n"
           "extended canvas, keeping the resolution of the current stack.\n";
  }
  QStringList parmNames() const override {
    return QStringList() << "Change both stacks";
  }
  QStringList parmDescs() const override {
    return QStringList() << "Extend the non-active stacks canvas as well";
  }
  ParmList parmDefaults() const override {
    return ParmList() << true;
  }
  ParmChoiceMap parmChoice() const override
  {
    ParmChoiceMap map;
    map[0] = booleanChoice();
    return map;
  }
  QIcon icon() const override {
    return QIcon(":/images/AlignCanvas.png");
  }

  bool projectGlobal(Stack* s1, Stack* s2);
  bool projectOnStack(Stack* target, const Stack* to_project);
};

/**
 * \class CombineStacks MergeStacks.hpp <MergeStacks.hpp>
 *
 * Combine the values of the main and work store onto the work store.
 */
class LGXCORE_EXPORT CombineStacks : public StackProcess {
public:
  CombineStacks(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const ParmList& parms) override
  {
    if(not checkState().store(STORE_NON_EMPTY | STORE_VISIBLE | STORE_MAIN)
                       .store(STORE_NON_EMPTY | STORE_VISIBLE | STORE_WORK))
      return false;
    Stack* current = currentStack();
    if(current->main()->labels() != current->work()->labels())
      return setErrorMessage("Error, one store is labeled and the other is not.");
    bool ok;
    float factorMain = parms[1].toFloat(&ok);
    if(!ok) return setErrorMessage("Parameter 'Factor Main' is not a number.");
    float factorWork = parms[2].toFloat(&ok);
    if(!ok) return setErrorMessage("Parameter 'Factor Work' is not a number.");
    float factorOutput = parms[3].toFloat(&ok);
    if(!ok) return setErrorMessage("Parameter 'Factor Output' is not a number.");
    float shiftMain = parms[4].toFloat(&ok);
    if(not ok) return setErrorMessage("Parameter 'Shift Main' is not a number'.");
    float shiftWork = parms[5].toFloat(&ok);
    if(not ok) return setErrorMessage("Parameter 'Shift Work' is not a number'.");
    float shiftOutput = parms[6].toFloat(&ok);
    if(not ok) return setErrorMessage("Parameter 'Shift Output' is not a number'.");
    return operator()(current, parms[0].toString(),
                      Point3f(factorMain, factorWork, factorOutput),
                      Point3f(shiftMain, shiftWork, shiftOutput));
  }

  bool operator()(Stack* target, QString method, Point3f factor, Point3f shift);

  QString folder() const override {
    return "Multi-stack";
  }
  QString name() const override {
    return "Combine Stacks";
  }
  QString description() const override {
    return "Combine the values of the main and work store onto the work store.\n"
           "In this process, images are first scaled from [0,65535] to [0,1].\n"
           "The output for a function `f` is given by:\n"
           "   output = ShiftOutput + FactorOutput*f(ShiftMain + FactorMain*main, ShiftWork + FactorWork*work)\n"
           "In the end, the image is scaled back from [0,1] to [0,65535].";
  }
  QStringList parmNames() const override {
    return QStringList() << "Method"
                         << "Factor Main" << "Factor Work" << "Factor Output"
                         << "Shift Main" << "Shift Work" << "Shift Output";
  }
  QStringList parmDescs() const override {
    return QStringList() << "Method"
                         << "Multiplicative factor of the main image."
                         << "Multiplicative factor of the work image."
                         << "Multiplicative factor of the output image."
                         << "Additive shift of the main image."
                         << "Additive shift of the work image."
                         << "Additive shift of the output image.";
  }
  ParmList parmDefaults() const override {
    return ParmList() << "Max" << 1 << 1 << 1 << 0 << 0 << 0;
  }
  ParmChoiceMap parmChoice() const override
  {
    ParmChoiceMap map;
    map[0] = QStringList() << "Max"
                           << "Min"
                           << "Add"
                           << "Product"
                           << "Average"
                           << "Divide by work"
                           << "Divide by main";
    return map;
  }
  QIcon icon() const override {
    return QIcon(":/images/CombineStacks.png");
  }
};

/**
 * \class MergeStacks MergeStacks.hpp <MergeStacks.hpp>
 *
 * Merge the main store of the current stack with the current store of the other one.
 * The current stack will be aligned with the other before the stores being combines.
 * The method argument is simply passed to the Combine_Stacks process.
 */
class LGXCORE_EXPORT MergeStacks : public StackProcess {
public:
  MergeStacks(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const ParmList& parms) override
  {
    if(not checkState().store(STORE_NON_EMPTY | STORE_VISIBLE, 0).store(STORE_NON_EMPTY | STORE_VISIBLE, 1))
      return false;
    Stack* current = currentStack();
    Stack* other = stack(1 - current->id());
    if(current->currentStore() != current->main())
      return setErrorMessage("Error, the current stack must have its main store active.");
    if(current->currentStore()->labels() != other->currentStore()->labels())
      return setErrorMessage("Error, one stack is labeled and the other is not.");
    bool ok;
    float factorMain = parms[1].toFloat(&ok);
    if(not ok) return setErrorMessage("Parameter 'Factor Main' is not a number'.");
    float factorWork = parms[2].toFloat(&ok);
    if(not ok) return setErrorMessage("Parameter 'Factor Work' is not a number'.");
    float factorOutput = parms[3].toFloat(&ok);
    if(not ok) return setErrorMessage("Parameter 'Factor Output' is not a number'.");
    float shiftMain = parms[4].toFloat(&ok);
    if(not ok) return setErrorMessage("Parameter 'Shift Main' is not a number'.");
    float shiftWork = parms[5].toFloat(&ok);
    if(not ok) return setErrorMessage("Parameter 'Shift Work' is not a number'.");
    float shiftOutput = parms[6].toFloat(&ok);
    if(not ok) return setErrorMessage("Parameter 'Shift Output' is not a number'.");
    bool result = operator()(current, other, parms[0].toString(),
                             Point3f(factorMain, factorWork, factorOutput),
                             Point3f(shiftMain, shiftWork, shiftOutput));
    if(result)
      current->work()->show();
    return result;
  }

  bool operator()(Stack* target, const Stack* other, QString method, Point3f factor, Point3f shift);

  QString folder() const override {
    return "Multi-stack";
  }
  QString name() const override {
    return "Merge Stacks";
  }
  QString description() const override
  {
    return "Merge the main store of the current stack with the current store of the other one.\n"
           "The current stack will be aligned with the other before the stores being combines.\n"
           "The method argument is simply passed to the Combine_Stacks process.";
  }
  QStringList parmNames() const override {
    return QStringList() << "Method"
                         << "Factor Main" << "Factor Work" << "Factor Output"
                         << "Shift Main" << "Shift Work" << "Shift Output";
  }
  QStringList parmDescs() const override {
    return QStringList() << "Method" << "Multiplicative factor for the output.";
  }
  ParmList parmDefaults() const override {
    return ParmList() << "Max" << "1" << "1" << "1" << "0" << "0" << "0";
  }
  ParmChoiceMap parmChoice() const override
  {
    ParmChoiceMap map;
    map[0] = QStringList() << "Max"
                           << "Min"
                           << "Average"
                           << "Add"
                           << "Product"
                           << "Divide by main"
                           << "Divide by work";
    return map;
  }
  QIcon icon() const override {
    return QIcon(":/images/MergeStacks.png");
  }
};
///@}
} // namespace process
} // namespace lgx
#endif
