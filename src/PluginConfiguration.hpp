/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef PLUGINCONFIGURATION_HPP
#define PLUGINCONFIGURATION_HPP

#include <LGXConfig.hpp>

#include <PluginManager.hpp>

#include <QAbstractItemModel>
#include <QDialog>
#include <QPointer>

#include <ui_PluginConfiguration.h>

namespace lgx {
namespace gui {

class PluginListModel : public QAbstractItemModel
{
  Q_OBJECT
public:
  PluginListModel(QObject *parent = nullptr);

  //@{
  ///\name Structure description
  int columnCount(const QModelIndex &parent = QModelIndex()) const override;
  int rowCount(const QModelIndex &parent = QModelIndex()) const override;
  QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
  //@}

  //@{
  ///\name Definition of the items
  QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
  QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const override;
  Qt::ItemFlags flags(const QModelIndex &index) const override;
  QModelIndex parent(const QModelIndex &index) const override;
  bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole) override;
  //@}

  //@{
  ///\name Drag and drop support
  Qt::DropActions supportedDropActions() const override;
  Qt::DropActions supportedDragActions() const override;
  bool moveRows(const QModelIndex &sourceParent, int sourceRow, int count,
                const QModelIndex &destinationParent, int destinationChild) override;
  bool dropMimeData(const QMimeData *data, Qt::DropAction action, int row, int column, const QModelIndex &parent) override;
  QMimeData* mimeData(const QModelIndexList &indexes) const override;
  QStringList mimeTypes() const override;
  //@}

  std::shared_ptr<process::Plugin> plugin(const QModelIndex& idx);

  void updateProcesses();
  void resetProcesses();
  void cleanPlugins();

signals:
  void usedChange(process::Plugin* plugin);

private:
  process::PluginManager* manager;
  QStringList pluginList, savedPluginList;
};

class PluginModel : public QAbstractItemModel
{
  Q_OBJECT

public:
  PluginModel(std::shared_ptr<process::Plugin> plugin, QObject *parent = nullptr);

  //@{
  ///\name Structure description
  int columnCount(const QModelIndex &parent = QModelIndex()) const override;
  int rowCount(const QModelIndex &parent = QModelIndex()) const override;
  QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const override;
  //@}

  //@{
  ///\name Definition of the items
  QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
  QModelIndex index(int row, int column, const QModelIndex &parent = QModelIndex()) const override;
  QModelIndex parent(const QModelIndex &index) const override;
  //@}

  process::Plugin* plugin() {
    return _plugin.get();
  }

public slots:
  void usedChanged();

private:
  std::shared_ptr<process::Plugin> _plugin;
  QStringList macroLanguages;
  QStringList stackProcesses, meshProcesses, globalProcesses;
  bool errorMode;
  QString errorMessage;
};

class PluginConfiguration : public QDialog {
  Q_OBJECT
public:
  PluginConfiguration(QWidget* parent = nullptr);

public slots:
  void pluginsList_usedChange(process::Plugin* plugin);
  void pluginsList_currentChanged(const QModelIndex& current, const QModelIndex& previous);

  void on_buttonBox_clicked(QAbstractButton* btn);

protected:
  bool event(QEvent* e) override;

private:
  void setPluginColumnWidth();

  Ui::PluginConfiguration ui;
  QPointer<PluginListModel> listModel;
  QPointer<PluginModel> pluginModel;
};

} // namespace plugins
} // namespace lgx

#endif // PLUGINCONFIGURATION_HPP
