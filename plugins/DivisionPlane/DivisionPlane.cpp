#include "DivisionPlane.hpp"
#include "Information.hpp"
#include "Progress.hpp"
#include <gsl/gsl_matrix.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_eigen.h>
#include <CuttingSurface.hpp>
#include <QFile>
#include <QTextStream>

namespace process
{

bool DivisionPlane::operator()(QString filename, Mesh *mesh, const Store* store, int id1, int id2)
{
    const Stack* stk = store->stack();
    const HVecUS& data = store->data();
    if(id1 == -1 and id2 == -1)
    {
        // Find the two first ID
        for(size_t k = 0 ; k < data.size() ; ++k)
        {
            const ushort& us = data[k];
            if(us > 0)
            {
                if(id1 == -1) id1 = us;
                else if(us != id1) { id2 = us; break; }
            }
        }
    }
    else if(id1 == -1 or id2 == -1)
        return setErrorMessage("Error, Cell ID 1 and Cell ID 2 must be either both specified or both set to -1.");

    Information::out << "Division between cells " << id1 << " and " << id2 << endl;
    // Find the borders
    std::vector<Point3f> boundary;
    Point3f mean;
    int dx = 1;
    int dy = stk->size().x();
    int dz = dy*stk->size().y();
    uint xmax = stk->size().x()-1;
    uint ymax = stk->size().y()-1;
    uint zmax = stk->size().z()-1;
    for(uint z = 0, k = 0 ; z < stk->size().z() ; ++z)
        for(uint y = 0 ; y < stk->size().y() ; ++y)
            for(uint x = 0 ; x < stk->size().x() ; ++x, ++k)
            {
                const ushort& lab = data[k];
                if(lab == id1 or lab == id2)
                {
                    ushort other = (lab == id1 ? id2 : id1);
                    bool border = (
                                (x > 0    and data[k-dx] == other) or
                                (x < xmax and data[k+dx] == other) or
                                (y > 0    and data[k-dy] == other) or
                                (y < ymax and data[k+dy] == other) or
                                (z > 0    and data[k-dz] == other) or
                                (z < zmax and data[k+dz] == other));
                    if(border)
                    {
                        boundary.push_back(stk->imageToWorld(Point3u(x,y,z)));
                        mean += boundary.back();
                    }
                }
            }
    mean /= boundary.size();
    // Perform PCA on the boundary points to fit a plane
    Matrix3f corr;
    forall(Point3f p, boundary)
    {
        p -= mean;
        corr(0,0) += p.x()*p.x();
        corr(1,1) += p.y()*p.y();
        corr(2,2) += p.z()*p.z();
        corr(1,0) += p.x()*p.y();
        corr(2,0) += p.x()*p.z();
        corr(2,1) += p.y()*p.z();
    }
    corr(0,1) = corr(1,0);
    corr(0,2) = corr(2,0);
    corr(1,2) = corr(2,1);
    corr /= boundary.size();

    Information::out << "Correlation matrix = \n" << corr[0] << "\n" << corr[1] << "\n" << corr[2] << endl;

    gsl_matrix *mat = gsl_matrix_alloc(3, 3);
    gsl_vector *eval = gsl_vector_alloc(3);
    gsl_matrix *evec = gsl_matrix_alloc(3,3);
    gsl_eigen_symmv_workspace *w = gsl_eigen_symmv_alloc(3);

    for(int i = 0 ; i < 3 ; ++i)
        for(int j = 0 ; j < 3 ; ++j)
            gsl_matrix_set(mat, i, j, corr(i,j));

    gsl_eigen_symmv(mat, eval, evec, w);
    gsl_eigen_symmv_sort(eval, evec, GSL_EIGEN_SORT_ABS_DESC);

    // Normal to the plane
    Point3d v1(gsl_matrix_get(evec, 0, 0),
               gsl_matrix_get(evec, 1, 0),
               gsl_matrix_get(evec, 2, 0));
    Point3d v2(gsl_matrix_get(evec, 0, 1),
               gsl_matrix_get(evec, 1, 1),
               gsl_matrix_get(evec, 2, 1));
    Point3d n(gsl_matrix_get(evec, 0, 2),
              gsl_matrix_get(evec, 1, 2),
              gsl_matrix_get(evec, 2, 2));

    if((v1^v2)*n < 0)
        n *= -1;

    Point3d vec(gsl_vector_get(eval, 0),
                gsl_vector_get(eval, 1),
                gsl_vector_get(eval, 2));

    Information::out << "v1 = " << v1 << endl;
    Information::out << "v2 = " << v2 << endl;
    Information::out << "n  = " << n << endl;
    Information::out << "Eigen values = " << vec << endl;

    // Prepare the transformation matrix
    Matrix4d m;
    m[0] = Point4d(v1);
    m[1] = Point4d(v2);
    m[2] = Point4d(n);
    m[3] = Point4d(mean);
    m(3,3) = 1;

    Information::out << "transform matrix = " << m << endl;

    // Set the cutting surface
    CuttingSurface *cut = cuttingSurface();
    qglviewer::ManipulatedFrame& frame = cut->frame();
    frame.setFromMatrix(m.data());
    Matrix4d glm(frame.matrix());
    Information::out << "store transform matrix = " << glm << endl;

    gsl_eigen_symmv_free(w);
    gsl_vector_free(eval);
    gsl_matrix_free(evec);
    gsl_matrix_free(mat);

    mesh->reset();
    vvgraph& S = mesh->graph();
    for(uint i = 0 ; i < boundary.size() ; ++i)
    {
        vertex v;
        v->pos = boundary[i];
        S.insert(v);
    }
    mesh->updateAll();

    if(not filename.isEmpty())
    {
        QFile f(filename);
        if(!f.open(QIODevice::WriteOnly))
            return setErrorMessage(QString("Error, cannot open file '%1' for writing").arg(filename));
        QTextStream ts(&f);
        ts << QString("Center,%1,%2,%3").arg(mean.x(), 0, 'g', 15).arg(mean.y(), 0, 'g', 15).arg(mean.z(), 0, 'g', 15) << endl;
        ts << QString("Normal,%1,%2,%3").arg(n.x(), 0, 'g', 15).arg(n.y(), 0, 'g', 15).arg(n.z(), 0, 'g', 15) << endl;
        ts << QString("V1,%1,%2,%3").arg(v1.x(), 0, 'g', 15).arg(v1.y(), 0, 'g', 15).arg(v1.z(), 0, 'g', 15) << endl;
        ts << QString("V2,%1,%2,%3").arg(v2.x(), 0, 'g', 15).arg(v2.y(), 0, 'g', 15).arg(v2.z(), 0, 'g', 15) << endl;
        ts << endl;
        ts << "X,Y,Z" << endl;
        for(size_t i = 0 ; i < boundary.size() ; ++i)
        {
            Point3f p = boundary[i];
            ts << QString("%1,%2,%3").arg(p.x(), 0, 'g', 15).arg(p.y(), 0, 'g', 15).arg(p.z(), 0, 'g', 15) << endl;
        }
    }
    return true;
}

REGISTER_GLOBAL_PROCESS(DivisionPlane);
}


