#include "Sieve.hpp"

#include <Process.hpp>

#include <unordered_set>

namespace lgx {
namespace process {
namespace sieve {

QString toString(FILTER_TYPE filter)
{
  switch(filter) {
    case FT_MEDIAN:
      return "Median";
    case FT_OPENING:
      return "Opening";
    case FT_CLOSING:
      return "Closing";
    case FT_OPENCLOSE:
      return "OpenClose";
    case FT_CLOSEOPEN:
      return "CloseOpen";
  }
}

FILTER_TYPE parseFilterType(const QString& type)
{
  QString low = type.toLower();
  if(low == "median" or low == "m")
    return FT_MEDIAN;
  if(low == "opening" or low == "open" or low == "o")
    return FT_OPENING;
  if(low == "closing" or low == "close" or low == "c")
    return FT_CLOSING;
  if(low == "openclose" or low == "p")
    return FT_OPENCLOSE;
  if(low == "closeopen" or low == "l")
    return FT_CLOSEOPEN;
  Information::out << "Warning, invalid filter type: " << type << ", using median" << endl;
  return FT_MEDIAN;
}

bool SieveProgress::advance(int value)
{
  _canceled &= not _reporter(value);
  return not _canceled;
}

Sieve::Sieve(const HVecUS& data)
{
  if(intptr_t(data.size()) < 0) {
    _errorString = "Error, the image is too large to be processed.";
    return;
  }

  _image.resize(data.size());
  memcpy(_image.data(), &data[0], sizeof(short)*data.size());

  _G.resize(_image.size());
  _adj_flags.resize(_image.size());

  memset(_G.data(), 0, _G.size()*sizeof(graph_node));
  memset(_adj_flags.data(), 0, _adj_flags.size()*sizeof(uint32_t));
}

bool Sieve::expand(intptr_t area, FILTER_TYPE filter, SieveProgress progress)
{
  intptr_t prev_area = -1;

  intptr_t step_area = area / 50;
  if(step_area < 1) step_area = 1;
  intptr_t next_area = step_area;

  while(true) {
    intptr_t min_area;
    switch(filter) {
      case FT_OPENING:
        min_area = _max_min_area;
        break;
      case FT_CLOSING:
        min_area  = _min_min_area;
        break;
      default:
        min_area = std::min(_min_min_area, _max_min_area);
    }

    if(step_area < 2 or min_area >= next_area) {
      if(not progress.advance(min_area)) return false;
      next_area = (min_area / step_area + 1) * step_area;
    }

    if(prev_area == min_area) {
      //saveFiles("failed_");
      _errorString = "Failed to increase granule size";
      return false;
    }

    if((min_area <= 0) or (min_area > area) or (min_area >= _G.size()))
      return true;

    prev_area = min_area;

    switch(filter) {
      case FT_OPENCLOSE:
        if(not processExtremaList(min_area, FT_OPENING, progress)) return false;
        if(not processExtremaList(min_area, FT_CLOSING, progress)) return false;
        break;
      case FT_CLOSEOPEN:
        if(not processExtremaList(min_area, FT_CLOSING, progress)) return false;
        if(not processExtremaList(min_area, FT_OPENING, progress)) return false;
        break;
      default:
        if(not processExtremaList(min_area, filter, progress)) return false;
    }
  }
  return true;
}

bool Sieve::processExtremaList(intptr_t area, FILTER_TYPE filter, SieveProgress& progress)
{
  auto it_min = end(_minima);
  auto it_max = end(_maxima);

  bool process_maxima = false, process_minima = false;

  // Process only the list that has the target minimum area
  if(filter != FT_OPENING and _min_min_area == area) {
    _min_min_area = std::numeric_limits<size_t>::max();
    it_min = begin(_minima);
    process_minima = true;
  }
  if(filter != FT_CLOSING and _max_min_area == area) {
    _max_min_area = std::numeric_limits<size_t>::max();
    it_max = begin(_maxima);
    process_maxima = true;
  }

  intptr_t processed = 0;

  std::vector<intptr_t> new_minima, new_maxima;

  // Process extrema in order of position in the image
  while(it_min != end(_minima) or it_max != end(_maxima)) {

    if(++processed % 200 == 0)
      if(not progress.advance(area)) return false;

    if(it_min != end(_minima) and (it_max == end(_maxima) or *it_min < *it_max)) {

      // Process minimum
      auto& cur = _G[*it_min];
      auto new_area = -cur.root_pel;
      intptr_t root = *it_min;

      if(new_area <= area) {
        if(new_area > 0)
          std::tie(new_area, root) = processExtremum(root);

        if(new_area > 0)
          new_minima.push_back(root);
      } else
        new_minima.push_back(*it_min);

      ++it_min;

      // Update minimum area
      if(new_area > 0 and (new_area < _min_min_area))
        _min_min_area = new_area;
    } else {
      // Process maximum
      auto& cur = _G[*it_max];
      auto new_area = -cur.root_pel;
      intptr_t root = *it_max;

      if(new_area <= area) {
        if(new_area > 0)
          std::tie(new_area, root) = processExtremum(root);

        if(new_area > 0)
          new_maxima.push_back(root);
      } else
        new_maxima.push_back(*it_max);
      ++it_max;

      if(new_area > 0 and (new_area < _max_min_area))
        _max_min_area = new_area;
    }
  }

  if(process_minima)
    swap(_minima, new_minima);
  if(process_maxima)
    swap(_maxima, new_maxima);

  if(_maxima.empty())
    _max_min_area = std::numeric_limits<size_t>::max();
  if(_minima.empty())
    _min_min_area = std::numeric_limits<size_t>::max();

  return true;
}

std::pair<intptr_t, intptr_t> Sieve::processExtremum(intptr_t init_pos)
{
  using std::swap;
  std::unordered_set<intptr_t> known_neighbors;

  // Storing the closest values less and greater than the current area
  int32_t lt_val = -1;
  int32_t gt_val = std::numeric_limits<ushort>::max() + 1;
  std::vector<intptr_t> lt_roots, gt_roots;
  auto ext_val = _image[init_pos];
  intptr_t last_pos = -1;

  known_neighbors.insert(init_pos);

  for(auto pos = init_pos ; pos >= 0 ; pos = _G[pos].next_adj) {
    auto& cur = _G[pos];
    auto flags = _adj_flags[pos];
    uint8_t bit = 1;
    uint8_t off = 0;

    for(auto dirns = flags ; dirns ;
        dirns >>= 1, bit <<= 1, ++off) {
      if(dirns & 1) {
        auto adj = pos + _offsets[off];
        auto adj_root = _G[adj].root_pel;
        if(adj_root < 0) // adj is a root
          adj_root = adj;

        auto adj_val = _image[adj_root];

        // If the root was already known
        if(not known_neighbors.insert(adj_root).second)
          flags ^= bit; // remove the direction from the flag
        else {
          if(adj_val <= ext_val) {
            if(adj_val > lt_val) {
              lt_val = adj_val;
              lt_roots.clear();
            }
            if(adj_val == lt_val)
              lt_roots.push_back(adj_root);
          } else {
            if(adj_val < gt_val) {
              gt_val = adj_val;
              gt_roots.clear();
            }
            if(adj_val == gt_val)
              gt_roots.push_back(adj_root);
          }
        }
      }
    }
    _adj_flags[pos] = flags;

    // Update flags and, if needed, delete the entry
    if(flags == 0 and last_pos >= 0)
      _G[last_pos].next_adj = _G[pos].next_adj;
    else {
      last_pos = pos;
    }
  }

  /* Return if region is not an extremum */
  if (lt_val >= 0 && gt_val <= std::numeric_limits<ushort>::max() && lt_val != ext_val)
    return {0, -1};

  // Merge pel lists for extremum and adjacent regions

  auto new_root = init_pos;
  std::vector<intptr_t> roots;
  if(lt_val >= 0) {
    _image[new_root] = (ushort)lt_val;
    swap(roots, lt_roots);
  } else {
    _image[new_root] = (ushort)gt_val;
    swap(roots, gt_roots);
  }

  /*
   *Information::out << "Merge zones:";
   *for(intptr_t z: roots)
   *  Information::out << " " << z << "(" << offX(z) << " " << offY(z) << ") / " << _image[z];
   *Information::out << endl;
   */

  for(intptr_t adj_root: roots) {
    auto& cur = _G[adj_root];

    // Always keep the larger area => area is the opposite of root_pel
    if(cur.root_pel >= _G[new_root].root_pel) {
      _G[new_root].root_pel += cur.root_pel;

      // Update root pels of adjacent region
      intptr_t last_pel = adj_root;
      for(auto pel = adj_root ; pel >= 0 ; pel = _G[pel].next_pel) {
        _G[pel].root_pel = new_root;
        last_pel = pel;
      }

      // Insert new region at the start of the current region
      _G[last_pel].next_pel = _G[new_root].next_pel;
      _G[new_root].next_pel = adj_root;

      // Add adjacent boundary pels after extremum boundary pels
      _G[last_pos].next_adj = adj_root;

      // If not at the end, find the last boundary pel in the adjacent section
      if(adj_root != roots.back()) {
        while(_G[last_pos].next_adj >= 0)
          last_pos = _G[last_pos].next_adj;
      }

    } else {
      cur.root_pel += _G[new_root].root_pel;

      // Update root pels of adjacent region
      intptr_t last_pel = new_root;
      for(auto pel = new_root ; pel >= 0 ; pel = _G[pel].next_pel) {
        _G[pel].root_pel = adj_root;
        last_pel = pel;
      }

      _G[last_pel].next_pel = _G[adj_root].next_pel;
      _G[adj_root].next_pel = new_root;

      if(adj_root != roots.back()) {
        // Add extremum boundary pels after adjacent boundary pel
        last_pel = adj_root;
        while(_G[last_pel].next_adj >= 0)
          last_pel = _G[last_pel].next_adj;
        _G[last_pel].next_adj = new_root;
      } else {
        // Insert extremum boundary pels after adjacent root pel
        _G[last_pos].next_adj = _G[adj_root].next_adj;
        _G[adj_root].next_adj = new_root;
      }

      new_root = adj_root;
    }
  }

  return {-_G[new_root].root_pel, new_root};
}

size_t Sieve::nbExtrema(FILTER_TYPE filter) const
{
  switch(filter) {
    case FT_MEDIAN:
    case FT_OPENCLOSE:
    case FT_CLOSEOPEN:
      return _minima.size() + _maxima.size();
    case FT_OPENING:
      return _maxima.size();
    case FT_CLOSING:
      return _minima.size();
  }
}

} // namespace sieve
} // namespace process
} // namespace lgx

