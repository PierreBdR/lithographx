/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef ITK_MEDIAN_IMAGE_FILTERER_HPP
#define ITK_MEDIAN_IMAGE_FILTERER_HPP

#include <ITKProcess.hpp>

#include <Stack.hpp>
#include <Store.hpp>

namespace lgx {
namespace process {
// Gamma filter on mesh signal, inherit from mesh process
class ITKMedianImageFilter : public StackProcess {
public:
  ITKMedianImageFilter(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  // Standard call interface for all processes
  bool operator()(const ParmList& parms) override
  {
    if(!checkState().store(STORE_NON_EMPTY))
      return false;
    Store* input = currentStack()->currentStore();
    Store* output = currentStack()->work();
    Point3u radius(parms[0].toUInt(), parms[1].toUInt(), parms[2].toUInt());

    bool res = (*this)(input, output, radius);
    if(res) {
      input->hide();
      output->show();
    }
    return res;
  }

  // Call interface with parameters used
  bool operator()(const Store* input, Store* output, Point3u radius);

  // Plug-in folder
  QString folder() const override {
    return "ITK/Filters";
  }
  // Plug-in name
  QString name() const override {
    return "ITK Median Filter";
  }
  // Plug-in long description
  QString description() const override {
    return "Median Filter";
  }
  // List of parameter names
  QStringList parmNames() const override
  {
    return QStringList() << "X Radius"
                         << "Y Radius"
                         << "Z Radius";
  }
  // List of parameter long descriptions
  QStringList parmDescs() const override
  {
    return QStringList() << "X Radius"
                         << "Y Radius"
                         << "Z Radius";
  }
  // List of parameter default values
  ParmList parmDefaults() const override
  {
    return ParmList() << 2
                      << 2
                      << 2;
  }
  // Plug-in icon
  QIcon icon() const override;
};
}
}
#endif
