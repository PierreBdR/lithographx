/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef IMAGE_HPP
#define IMAGE_HPP

#include <LGXConfig.hpp>

#include <cuda/CudaExport.hpp>
#include <Vector.hpp>

#include <array>
#include <exception>
#include <QString>
#include <QStringList>

namespace lgx {

/**
 * Exception used in image functions to signify a critical error.
 */
struct ImageError : public std::exception
{
public:
  ImageError(const QString& msg)
    : std::exception()
  {
    message = msg.toUtf8();
  }

  ~ImageError() throw() override { }

  const char* what() const throw() override {
    return message.data();
  }

  QByteArray message;
};

class Progress;

typedef util::Vector<3, unsigned int> Point3u;
typedef util::Vector<5, unsigned int> Point5u;
typedef util::Vector<3, size_t> Point3s;
typedef util::Vector<5, size_t> Point5s;

LGX_EXPORT QStringList supportedImageReadFormats();
LGX_EXPORT QStringList supportedImageWriteFormats();

/**
 * Information about a 5D image.
 */
struct LGX_EXPORT ImageInfo {
  enum Axis {
    X = 0,
    Y,
    Z,
    C,
    T
  };
  explicit ImageInfo(const QString& fn)
    : filename(fn)
  { }
  ImageInfo(const QString& fn,
            const Point5u& si,
            const Point3f& st)
    : filename(fn)
    , size(si)
    , step(st)
  { }
  ImageInfo(const QString& fn,
            const Point3u& si,
            const Point3f& st)
    : filename(fn)
    , size(si.x(), si.y(), si.z(), 1, 1)
    , step(st)
  { }
  ImageInfo(const ImageInfo&) = default;
  ImageInfo& operator=(const ImageInfo&) = default;

  ImageInfo() { }
  ImageInfo(ImageInfo&&) = default;
  ImageInfo& operator=(ImageInfo&&) = default;

  virtual ~ImageInfo() { }

  bool valid() const {
    return size != Point5u(0u);
  }

  operator bool() const {
    return valid();
  }

  unsigned int nbChannels() const { return size[3]; }
  unsigned int& nbChannels() { return size[3]; }
  unsigned int nbTimePoints() const { return size[4]; }
  unsigned int& nbTimePoints() { return size[4]; }

  size_t nbVoxels() const {
    return size[0] * size[1] * size[2] * size[3] * size[4];
  }

  void resetAxisOrder() {
    axisOrder = {{X, Y, Z, C, T}};
  }

  QString filename;                                 ///< File loaded for this image (if any)
  Point5u size = Point5u(1u, 1u, 1u, 1u, 1u);       ///< Size of the image in all 5 dimensions
  Point3f step = Point3f(1.f, 1.f, 1.f);            ///< Size of a single voxel in 3D
  Point3f origin;                                   ///< Origin of the image in 3D
  uint16_t depth = 16;                              ///< Number of bits required to represent each voxel
  bool labels = false;                              ///< If the image contains labels
  std::array<Axis,5> axisOrder = {{X, Y, Z, C, T}}; ///< Vector defining the order in which the dimensions are encoded
};

/**
 * Stores a 5D image.
 *
 * The Image5D always refer to the same underlying data when copied, unless the
 * 'allocate' method is called.
 *
 * Note that to be valid, the axis order **must** be X, Y, Z, C, T.
 */
struct LGX_EXPORT Image5D : public ImageInfo {
  Image5D();
  explicit Image5D(const QString& filename);
  Image5D(const QString& filename, ushort* data, const Point3u& size, const Point3f& step = Point3f(1, 1, 1));

  Image5D(const Image5D& other);
  Image5D& operator=(const Image5D& other);

  virtual ~Image5D();

  template <size_t N>
  Image5D view(uint n);

  /// Return a view on a single time point of the image
  Image5D timepoint(uint t) {
    return view<4>(t);
  }
  /// Return a view on a single channel of the image
  Image5D channel(uint c) {
    return view<3>(c);
  }
  /// Return a view on a single plane of the image
  Image5D plane(uint z) {
    return view<2>(z);
  }
  /// Return a view on a single line of the image
  Image5D line(uint y) {
    return view<1>(y);
  }
  /// Return a view on a single column of the image
  Image5D column(uint x) {
    return view<0>(x);
  }

  /// Allocate space for a 5D image
  void allocate(const Point5u& size);
  /// Allocate space for a 3D image
  void allocate(const Point3u& size);
  /// Allocate space using the pre-defined size
  void allocate();

  /// Free the memory
  void free();

  /// Access a voxel by index
  const ushort& operator[](int i) const {
    return _data[i];
  }

  /// Access a voxel by index
  ushort& operator[](int i) {
    return _data[i];
  }

  /// Access a voxel by position
  ushort& operator()(size_t x, size_t y, size_t z = 0, size_t c = 0, size_t t = 0);

  /// Access a voxel by position
  ushort& operator()(const Point5s& p) {
    return (*this)(p[0], p[1], p[2], p[3], p[4]);
  }

  /// Access a voxel by position
  const ushort& operator()(size_t x, size_t y, size_t z = 0, size_t c = 0, size_t t = 0) const;

  /// Access a voxel by position
  const ushort& operator()(const Point5s& p) const {
    return (*this)(p[0], p[1], p[2], p[3], p[4]);
  }

  /// Raw pointer to the data
  ushort* data() { return _data; }
  /// Raw pointer to the data
  const ushort* data() const { return _data; }
  /// Raw pointer to the data
  const ushort* c_data() const { return _data; }

  /// Update the strides based on the size and the axisOrder
  void updateStrides();

  Point5s strides;               ///< Strides, that is the number of voxels separating two elements along that dimension
  uint minc = 0;                 ///< Minimum value in the image
  uint maxc = 0;                 ///< Maximum value in the image
  float brightness = 1.0f;       ///< Brightness, i.e. multiplicating factor used while loading an image
  uint8_t compression_level = 9; ///< Compression level to use when saving a TIFF file

protected:
  ushort* _data = nullptr;
  bool allocated = false;
};

extern template Image5D Image5D::view<0>(uint);
extern template Image5D Image5D::view<1>(uint);
extern template Image5D Image5D::view<2>(uint);
extern template Image5D Image5D::view<3>(uint);
extern template Image5D Image5D::view<4>(uint);

/**
 * Save a 5D image into a file using CImg.
 *
 * \relates Image5D
 */
LGX_EXPORT void saveImage(QString filename, const Image5D& data, QString type = "CImg Auto",
                          unsigned int nb_digits = 0);

/**
 * Save a TIFF file using libtiff directly
 *
 * \relates Image5D
 */
LGX_EXPORT void saveTIFFImage(QString filename, const Image5D& data);

/**
 * Load a single plane from a TIFF file
 *
 * \param filename File to load
 * \param data Structure containing the data
 * \param plane Which plane to load from the file
 * \param channel Which channel to load. -1 to load all channels.
 * \param timepoint Which time point to load. -1 to load all time points.
 *
 * \returns True on success
 *
 * \relates Image5D
 */
LGX_EXPORT void loadTIFFPlane(QString filename, Image5D& data,
                              uint plane, int channel = 0, int timepoint = 0);

/**
 * Load a single plane from an image file using Image Magick
 *
 * \param files List of files to load
 * \param data Structure containing the data
 * \param plane Which plane to load from the file
 * \param channel Which channel to load. -1 to load all channels.
 * \param timepoint Which time point to load. -1 to load all time points.
 *
 * \returns True on success
 *
 * \relates Image5D
 */
LGX_EXPORT void loadImageMagickPlane(const QStringList& files, Image5D& data,
                                     uint plane, int channel = 0, int timepoint = 0);

/**
 * Load a sample of each timepoint / channel
 *
 * \relates Image5D
 */
LGX_EXPORT void loadTIFFSamples(QString filename, Image5D& data,
                                Progress* progress = 0);

/** Load a sample of each timepoint / channel
 *
 * \relates Image5D
 */
LGX_EXPORT void loadImageMagickSamples(const QStringList& files, Image5D& data,
                                       Progress* progress = 0);

/**
 * Load a TIFF image, with progress bar!
 *
 * \relates Image5D
 */
LGX_EXPORT void loadTIFFImage(QString filename, Image5D& data,
                              int channel = 0, int timepoint = 0,
                              Progress* progress = 0);

/**
 * Load an image using Image Magick
 *
 * \relates Image5D
 */
LGX_EXPORT void loadImageMagickImage(const QStringList& files, Image5D& data,
                                     int channel = 0, int timepoint = 0,
                                     Progress* progress = 0);

/**
 * Load an image from a file
 *
 * \param filename File to load
 * \param data Structure containing the data
 * \param channel Which channel to load. -1 to load all channels.
 * \param timepoint Which time point to load. -1 to load all time points.
 * \param progress If non null, use the progress bar. Currently only works with TIFF images.
 *
 * \returns True on success
 *
 * \note If \p data is already allocated and its \c plane attribute is positive
 * or 0, this function expect the image file to contain a single plane, which
 * will be loaded at the location indicated by \c data.plane.
 *
 * \relates Image5D
 */
LGX_EXPORT void loadImage(QString filename, Image5D& data,
                          int channel = 0, int timepoint = 0,
                          Progress* progress = 0);

/** Load a sample of each timepoint / channel
 *
 * \relates Image5D
 */
LGX_EXPORT void loadSamples(QString filename, Image5D& data,
                            Progress* progress = 0);

/**
 * Load a list of 2D images, possibly multi-channel
 *
 * \param filenames List of files to load
 * \param data Structure containing the data
 * \param channel Which channel to load in the images. -1 to load all channels.
 * \param timepoint Which time point to load. -1 to load all time points.
 * \param progress If non null, use the progress bar. Currently only works with TIFF images.
 *
 * \returns True on success
 *
 * \note The \c plane attribute of the \p data must be -1.
 *
 * \relates Image5D
 */
LGX_EXPORT void loadImageSequence(QStringList filenames, Image5D& data,
                                  int channel = 0, int timepoint = 0,
                                  Progress* progress = 0);

/**
 * Resize an image
 *
 * \relates Image5D
 */
LGX_EXPORT HVecUS resize(const HVecUS& data, const Point3u& before, const Point3u& after, bool center);

/**
 * Get the attributes of a TIFF image, without loading it
 *
 * \relates Image5D
 */
LGX_EXPORT ImageInfo getTIFFInfo(QString filename);

/**
 * Get the attributes of an image using image magick
 *
 * \relates Image5D
 */
LGX_EXPORT ImageInfo getImageMagickInfo(const QStringList& files, float* gamma = 0);

/**
 * Get the attributes of an image using CImg
 *
 * \relates Image5D
 */
LGX_EXPORT ImageInfo getCImgInfo(QString filename);

/**
 * Get the attributes of an image, without loading it
 *
 * \relates Image5D
 */
LGX_EXPORT ImageInfo getImageInfo(QString filename);

/**
 * Take an image name and returns the file name, setting the selected channel and timepoint at the same time.
 *
 * \param filename Name to parse
 * \param single_image If true, the output is always a single 3D image,
 *     so channel and timepoints are 0 by default, not -1.
 *
 * \returns A tuple with the filename, the channel and the time point, in that order.
 *
 * \relates Image5D
 */
LGX_EXPORT std::tuple<QString,int,int> parseImageName(const QString& filename, bool single_image = true);

/**
 * Parse an image list.
 *
 * \param[in] filename File name of list of files separated by ";;"
 * \param[out] size Stack size
 * \param[out] step Voxel size
 * \param[out] brightness Brightness read from the file, or 1.
 * \param[out] files List of image files.
 *
 * An image list can be one of two things:
 *
 *  -# A list of files separated by ";;", in which case step and size will be
 *     estimated from the number of images and the properties of the first image.
 *  -# A text file containing the description of the list, including size, step and brightness.
 *
 * If providing a text file, it should have the following format:
 *
 *      [Stack]
 *      // This is a comment
 *      SizeX: integer number
 *      SizeY: integer number
 *      StepX: number
 *      StepY: number
 *      StepZ: number
 *      Brightness: number
 *      ImageFile: relative/path/to/file1
 *      ImageFile: relative/path/to/file2
 *      ImageFile: relative/path/to/file3
 *      ImageFile: ...
 *
 * The file must start with the characters "[Stack]".
 *
 * \throws ImageError If any error happens while parsing.
 *
 * \relates Image5D
 */
LGX_EXPORT void parseImageList(const QString& filename, Point3u& size, Point3f& step, float& brightness, QStringList& files);

/**
 * Test whether a file contains an image list or not
 *
 * \seealso parseImageList
 * \relates Image5D
 */
LGX_EXPORT bool isImageList(const QString& filename);

} // namespace lgx

#endif // IMAGE_HPP
