# -*- coding:utf-8 -*-
from __future__ import print_function, division
from lgxPy import Mesh, Stack, Global, parmToBool, stringToBool, NoSuchProcess
from lgxUtils.path import Path
from cell_classifier.utils import processFilePattern, isValidFilePattern, printable, correctFormat
from cell_classifier.find_files_model import FindFilesModel
from lgxUtils.read_ini import INIFile
import numpy as np

from PyQt5.QtWidgets import QFileDialog, QDialog, QDialogButtonBox
from PyQt5.QtCore import pyqtSlot, pyqtSignal, Qt, QTimer
from PyQt5.QtGui import QColor, QPalette
from PyQt5.uic import loadUi

processType = "Global"
processName = "Generate Classifier Features"
processFolder = "Cell Classifier"
processIcon = ":/images/CellFeatures.png"
parmsDescs = dict(Folder=u"Folder containing all the files.",
                  Main_File_Pattern=u"""Pattern used to find the main files.
By default, the base name of the file will be the file name without its extension.
If a pair of brackets is used, this defines which part of the file name should be
used to generate other file names""",
                  Main_File_Type="One of 'Stack' or 'Mesh'",
                  Recursive=u"If true, files will be also searched in sub-folders",
                  Add_Cell_Type='It True, the cell type is not in the main file but must be added using an external file.',
                  Cell_Type_Suffix='Suffix of the cell types to add to the current file',
                  Feature_Suffix="Suffix to add to the base name of the main file.",
                  Feature_Process='Name of the process used to compute the features',
                  Feature_Process_Output='Index of the parameter of the feature process defining the output file.')
parmsChoice = dict(Recursive=["Yes", "No"],
                   Main_File_Type=["Stack", "Mesh"])


_processesTypes = [Stack, Mesh, Global]
_processesTypeNames = ['Stack', 'Mesh', 'Global']

class CreateClassifierFeaturesDlg(QDialog):

    preventValidation = pyqtSignal(bool)
    allowValidation = pyqtSignal(bool)

    def __init__(self, parent):
        QDialog.__init__(self, parent)
        self._initialized = False
        pth = Path(__file__).dirname() / 'CreateClassifierFeatures.ui'
        self.ui = loadUi(pth, self)
        model = FindFilesModel(self.ui.baseFolder.text(), ('Main', self.ui.mainFilePattern.text()),
                               [('Feature', self.ui.featureFileSuffix.text(), "output"),
                                ('Cell Type', self.ui.cellTypeSuffix.text(), "mandatory"),
                                ('Project', self.ui.projectSuffix.text(), "optional")],
                               False, self)
        model.showColumn(2, False)
        model.showColumn(3, False)
        model.setObjectName("filesModel")
        model.modelReset.connect(self.on_filesModel_modelReset)
        btn = self.ui.buttonBox.addButton("Refresh files", QDialogButtonBox.ActionRole)
        btn.clicked.connect(self.refreshFiles)
        self._model = model
        self.ui.filesView.setModel(model)
        bbox = self.ui.buttonBox
        okBtn = bbox.button(bbox.Ok)
        self.preventValidation.connect(okBtn.setDisabled)
        self.allowValidation.connect(okBtn.setEnabled)
        pal = self.ui.mainFilePattern.palette()
        self.goodBack = pal.color(self.ui.mainFilePattern.backgroundRole())
        self.badBack = QColor(Qt.red).lighter()

        self.resetTimer = QTimer()
        self.resetTimer.setInterval(10)
        self.resetTimer.setSingleShot(True)
        self.resetTimer.timeout.connect(self.resizeColumns)

        processes = self.ui.featureProcess
        self._processes = [None]*3
        for i in range(3):
            typ = _processesTypes[i]
            self._processes[i] = sorted(p for p in dir(typ) if getattr(typ, p).nbParms > 0)
        self.ui.featureProcessType.setCurrentIndex(1)
        self.resetTimer.start()

    def featureProcessName(self):
        return "{0}.{1}".format(self.ui.featureProcessType.currentText(), self.ui.featureProcess.currentText())

    def setFeatureProcess(self, typ, proc):
        self.ui.featureProcessType.setCurrentIndex(_processesTypes.index(typ))
        self.ui.featureProcess.setCurrentText(proc)
        self.updateOutputRange()

    def updateOutputRange(self):
        typ = _processesTypes[self.ui.featureProcessType.currentIndex()]
        nam = self.ui.featureProcess.currentText()
        nb_params = 99
        try:
            proc = getattr(typ, nam)
            nb_params = proc.nbParms
        except NoSuchProcess:
            pass
        self.ui.featureProcessOutput.setMaximum(max(0, nb_params-1))

    @pyqtSlot(int)
    def on_featureProcessType_currentIndexChanged(self, idx):
        self.ui.featureProcess.clear()
        self.ui.featureProcess.addItems(self._processes[idx])

    @pyqtSlot()
    def on_filesModel_modelReset(self):
        self.resetTimer.start()

    @pyqtSlot()
    def resizeColumns(self):
        n = self._model.columnCount()
        for i in range(n):
            self.ui.filesView.resizeColumnToContents(i)
        h = self.ui.filesView.header()
        h.stretchLastSection()

    @pyqtSlot()
    def refreshFiles(self):
        self._model.findFiles()

    def exec_(self):
        self._initialized = True
        return QDialog.exec_(self)

    @pyqtSlot(str)
    def on_baseFolder_textChanged(self, txt):
        p = Path(txt)
        if p.isdir():
            self._model.baseFolder = p

    @pyqtSlot(bool)
    def on_findRecursive_toggled(self, on):
        self._model.recursive = on

    def setRecursive(self, on):
        self._model.recursive = on
        self.ui.findRecursive.setChecked(on)

    @pyqtSlot(bool)
    def on_useCellTypes_toggled(self, on):
        self._model.showColumn(2, on)

    @pyqtSlot(bool)
    def on_useProject_toggled(self, on):
        self._model.showColumn(3, on)

    @pyqtSlot(str)
    def on_mainFilePattern_textChanged(self, txt):
        if not isValidFilePattern(txt):
            self.setError(self.ui.mainFilePattern)
            del self._model.mainFilePattern
            self.preventValidation.emit(True)
        else:
            self.setError(self.ui.mainFilePattern, False)
            self.allowValidation.emit(True)
            self._model.mainFilePattern = txt

    def setError(self, item, err=True):
        pal = item.palette()
        pal.setColor(QPalette.Base, self.badBack if err else self.goodBack)
        item.setPalette(pal)

    @pyqtSlot(str)
    def on_featureFileSuffix_textChanged(self, txt):
        self._model.setColumnFormat(1, txt)

    @pyqtSlot(str)
    def on_cellTypeSuffix_textChanged(self, txt):
        self._model.setColumnFormat(2, txt)

    @pyqtSlot(str)
    def on_projectSuffix_textChanged(self, txt):
        self._model.setColumnFormat(3, txt)

    @pyqtSlot()
    def on_chooseFolder_clicked(self):
        newFolder = self.ui.baseFolder.text()
        newFolder = QFileDialog.getExistingDirectory(self, "Choose Base Folder", newFolder)
        if newFolder:
            self.ui.baseFolder.setText(newFolder)

    def showEvent(self, event):
        self.allowValidation.emit(isValidFilePattern(self.ui.mainFilePattern.text()))
        # self.findFiles()


def parseParms(Folder, Main_File_Pattern, Main_File_Type, Recursive,
               Add_Cell_Type, Cell_Type_Suffix,
               Add_Project, Project_Suffix,
               Feature_Process, Feature_Process_Output,
               Feature_Suffix):
    Folder = Path(Folder).abspath()
    Main_File_Pattern = str(Main_File_Pattern)
    Main_File_Type = str(Main_File_Type)
    if Main_File_Type not in ('Stack', 'Mesh'):
        raise ValueError("'Main_File_Type' must be one of 'Stack' or 'Mesh'.")
    Recursive = parmToBool(Recursive)
    Feature_Process = str(Feature_Process)
    try:
        idx = Feature_Process.index('.')
        process_type = Feature_Process[:idx]
        if process_type == 'Stack':
            process_type = Stack
        elif process_type == 'Mesh':
            process_type = Mesh
        elif process_type == 'Global':
            process_type = Global
        else:
            raise ValueError("Process type must be one of 'Stack', 'Mesh' or 'Global'")
        process_name = Feature_Process[idx+1:]
    except ValueError:
        process_type = Mesh
        process_name = Feature_Process
    Feature_Process = (process_type, process_name)
    Feature_Process_Output = int(Feature_Process_Output)
    Feature_Suffix = str(Feature_Suffix)
    Add_Cell_Type = parmToBool(Add_Cell_Type)
    Cell_Type_Suffix = str(Cell_Type_Suffix)
    Add_Project = parmToBool(Add_Project)
    Project_Suffix = str(Project_Suffix)
    return (Folder, Main_File_Pattern, Main_File_Type, Recursive,
            Add_Cell_Type, Cell_Type_Suffix,
            Add_Project, Project_Suffix,
            Feature_Process, Feature_Process_Output,
            Feature_Suffix)


def initialize(Folder, Main_File_Pattern, Main_File_Type, Recursive,
               Add_Cell_Type, Cell_Type_Suffix,
               Add_Project, Project_Suffix,
               Feature_Process, Feature_Process_Output,
               Feature_Suffix,
               parent):
    dlg = CreateClassifierFeaturesDlg(parent)

    dlg.ui.baseFolder.setText(Folder)
    dlg.ui.mainFilePattern.setText(Main_File_Pattern)
    dlg.ui.useCellTypes.setChecked(Add_Cell_Type)
    dlg.ui.cellTypeSuffix.setText(Cell_Type_Suffix)
    dlg.ui.useProject.setChecked(Add_Project)
    dlg.ui.projectSuffix.setText(Project_Suffix)
    dlg.setFeatureProcess(*Feature_Process)
    dlg.ui.featureProcessOutput.setValue(Feature_Process_Output)
    dlg.ui.featureFileSuffix.setText(Feature_Suffix)
    dlg.setRecursive(Recursive)

    if dlg.exec_() == QDialog.Accepted:
        Folder = dlg.ui.baseFolder.text()
        Main_File_Pattern = dlg.ui.mainFilePattern.text()
        Main_File_Type = dlg.ui.mainFileType.currentText()
        Recursive = dlg.ui.findRecursive.isChecked()
        Add_Cell_Type = dlg.ui.useCellTypes.isChecked()
        Cell_Type_Suffix = dlg.ui.cellTypeSuffix.text()
        Add_Project = dlg.ui.useProject.isChecked()
        Project_Suffix = dlg.ui.projectSuffix.text()
        Feature_Process = dlg.featureProcessName()
        Feature_Process_Output = dlg.ui.featureProcessOutput.value()
        Feature_Suffix = dlg.ui.featureFileSuffix.text()
        return (Folder, Main_File_Pattern, Main_File_Type, Recursive,
                Add_Cell_Type, Cell_Type_Suffix,
                Add_Project, Project_Suffix,
                Feature_Process, Feature_Process_Output,
                Feature_Suffix)


def run(Folder="", Main_File_Pattern="{*}.ply", Main_File_Type="Mesh", Recursive=False,
        Add_Cell_Type=False, Cell_Type_Suffix='-training.csv',
        Add_Project=True, Project_Suffix=".lgxp",
        Feature_Process="Mesh.Cell_Shapes_Features_2D", Feature_Process_Output = 0,
        Feature_Suffix="-features.csv"):
    Cell_Type_Suffix = correctFormat(Cell_Type_Suffix)
    Project_Suffix = correctFormat(Project_Suffix)
    Feature_Suffix = correctFormat(Feature_Suffix)
    main_glob, main_basename = processFilePattern(Main_File_Pattern)
    if Recursive:
        files = Folder.walkfiles(main_glob)
    else:
        files = Folder.files(main_glob)
    process = getattr(*Feature_Process)
    if process.nbParms <= Feature_Process_Output:
        raise ValueError("Error, process '{0}' has only {1} parameters when {2} is needed."
                         .format(str(process), process.nbParms, Feature_Process_Output+1))
    parms = list(process.lastParms)
    for f in files:
        basename = main_basename(f.relpath(Folder))
        cellType = None
        if Add_Cell_Type:
            cellType = Folder / Cell_Type_Suffix.format(basename)
            if not cellType.isfile():
                continue
        project = None
        if Add_Project:
            project = Folder / Project_Suffix.format(basename)
            if not project.isfile():
                project = None
        print("### Computing features of file ", printable(f))
        feat = Folder / Feature_Suffix.format(basename)
        if Main_File_Type == "Stack":
            Stack.Open(f, 'Work', 0, False, '')
        else:
            Mesh.Load(f, False, False, 0, False)
        if cellType:
            Mesh.Load_Parents(cellType, 'No')
        if project:
            project = INIFile(project, read_project=True)
            use_trans = stringToBool(project['Stack1', 'ShowTrans'])
            frame_name = 'Trans' if use_trans else 'Frame'
            frame = project['Stack1', frame_name]
            Stack.Transform_Stack(frame, False)
        else:
            Stack.Transform_Stack('', False)
        parms[Feature_Process_Output] = feat
        process(*parms)
        # Make sure the parents are visible on the mesh
        Mesh.View(True, 'Parents', '', '', '', '', '', '', '', '', '', '', '', '', '')
        Mesh.Add_Lineage_to_Table(feat, '', 0, 'CellType', '', False)
    return True
