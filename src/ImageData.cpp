/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "ImageData.hpp"

#include "ClipRegion.hpp"
#include "CutSurf.hpp"
#include "Dir.hpp"
#include "Geometry.hpp"
#include "Information.hpp"
#include "Insert.hpp"
#include "Mesh.hpp"
#include "Misc.hpp"
#include "Progress.hpp"
#include "Vector.hpp"

#include "Assert.hpp"

#include <limits>
#include <cmath>

namespace lgx {

typedef util::Vector<16, double> Point16d;

using Information::err;

using namespace std;
using namespace util;

// Static class varaibles
int ImgData::PixelEditRadius = 25;
int ImgData::PixelEditMaxPix = 1024000;
int ImgData::ClearColor = 0;
uint ImgData::TileCount = 20u;
Point3u ImgData::MaxTexSize = Point3u(2048u, 2048u, 2048u);
Point2u ImgData::Max2DTexSize = Point2u(2048u, 2048u);
bool ImgData::defaultTex16Bits = true;
float ImgData::ZOffset = .0001f;
bool ImgData::SeedStack = false;
ImgData::SelectedTool ImgData::tool = ImgData::ST_PICK_LABEL;

float ImgData::DrawNormals = 0.0f;
float ImgData::DrawZeroLabels = 0.0f;
float ImgData::DrawNhbds = 0.0f;
float ImgData::DrawOffset = 2.0f;
bool ImgData::DeleteBadVertex = false;
bool ImgData::FillWorkData = false;
ushort ImgData::FillWorkValue = 65535u;
std::vector<Colorf> ImgData::LabelColors; // = std::vector<Point3f>(1);

ScaleBar ImgData::scaleBar;
ColorBar ImgData::colorBar;

float ImgData::MeshPointSize = 1.0f;
float ImgData::MeshLineWidth = 1.0f;

#ifdef WIN32
#  define FileDialogOptions QFileDialog::DontUseNativeDialog
#else
const int FileDialogOptions = 0;
#endif

ImgData::ImgData(int id, QWidget* parent)
  : QObject(parent)
  , StackId(id)
  , Main16Bit(true)
  , Work16Bit(true)
  , needUpdateLines(false)
  , needUpdateTris(false)
  , newMainColorMap(true)
  , newWorkColorMap(true)
  , newSurfColorMap(true)
  , newHeatColorMap(true)
  , mainDataTexId(0)
  , workDataTexId(0)
  , dataTexColor(0)
  , surfTexId(0)
  , heatTexId(0)
  , imgTexId(0)
  , mcmapTexId(0)
  , wcmapTexId(0)
  , labelTexId(0)
  , lineId(0)
  , TCount(0)
  , LCount(0)
  , VCount(0)
  , posVAid(0)
  , nrmlVAid(0)
  , selVAid(0)
  , texVAid(0)
  , imgVAid(0)
  , pntsVAid(0)
  , pcolVAid(0)
  , pselVAid(0)
  , texMap(0)
  , min(0)
  , max(1)
  , labelWallBordMin(0)
  , labelWallBordMax(1)
  , pixelRadius(1)
  , marchLabel(0)
  , LoadMeshAdd(false)
  , meshShift(0)
  , marchData(0)
  , EnableRotate(false)
  , readOnce(true)
  , changed(false)
  , pixelsChanged(false)
{
  surfTransferDlg = new gui::TransferFunctionDlg(parent);
  heatTransferDlg = new gui::TransferFunctionDlg(parent);
  workTransferDlg = new gui::TransferFunctionDlg(parent);
  mainTransferDlg = new gui::TransferFunctionDlg(parent);
}

void ImgData::init(Stack* s, Mesh* m)
{
  stack = s;
  mesh = m;
  mainTransferDlg->setDefaultTransferFunction(stack->main()->transferFct());
  workTransferDlg->setDefaultTransferFunction(stack->work()->transferFct());
  surfTransferDlg->setDefaultTransferFunction(mesh->surfFct());
  heatTransferDlg->setDefaultTransferFunction(mesh->heatFct());
  changeMainColorMap(stack->main()->transferFct());
  changeWorkColorMap(stack->work()->transferFct());
  changeSurfColorMap(mesh->surfFct());
  changeHeatColorMap(mesh->heatFct());
  if(info_dlg)
    info_dlg->updateData();
}

ImgData::~ImgData()
{
  // Delete VBOs
  if(posVAid)
    opengl->glDeleteBuffers(1, &posVAid);
  if(nrmlVAid)
    opengl->glDeleteBuffers(1, &nrmlVAid);
  if(texVAid)
    opengl->glDeleteBuffers(1, &texVAid);
  if(imgVAid)
    opengl->glDeleteBuffers(1, &imgVAid);
  if(selVAid)
    opengl->glDeleteBuffers(1, &selVAid);
  if(pntsVAid)
    opengl->glDeleteBuffers(1, &pntsVAid);
  if(pcolVAid)
    opengl->glDeleteBuffers(1, &pcolVAid);
  if(pselVAid)
    opengl->glDeleteBuffers(1, &pselVAid);

  // Delete textures
  if(mainDataTexId)
    opengl->glDeleteTextures(1, &mainDataTexId);
  if(workDataTexId)
    opengl->glDeleteTextures(1, &workDataTexId);
  if(surfTexId)
    opengl->glDeleteTextures(1, &surfTexId);
  if(heatTexId)
    opengl->glDeleteTextures(1, &heatTexId);
  if(imgTexId)
    opengl->glDeleteTextures(1, &imgTexId);

  delete mainTransferDlg;
  delete workTransferDlg;
  delete surfTransferDlg;
  delete heatTransferDlg;
}

// Set parameters for function
void ImgData::readParms(util::Parms& parms, QString section)
{
  QString StackFile, WorkStackFile, MeshFile;
  parms(section, "StackFile", StackFile, QString(""));
  if(!StackFile.isEmpty())
    StackFile = absoluteFilePath(StackFile);
  stack->main()->setFile(StackFile);
  parms(section, "WorkStackFile", WorkStackFile, QString(""));
  if(!WorkStackFile.isEmpty())
    WorkStackFile = absoluteFilePath(WorkStackFile);
  stack->work()->setFile(WorkStackFile);
  parms(section, "MeshFile", MeshFile, QString(""));
  if(!MeshFile.isEmpty())
    MeshFile = absoluteFilePath(MeshFile);
  mesh->setFile(MeshFile);

  bool MainShow;
  parms(section, "MainShow", MainShow, true);
  if(MainShow)
    stack->main()->show();
  else
    stack->main()->hide();
  float MainBright;
  parms(section, "MainBright", MainBright, 1.0f);
  stack->main()->setBrightness(MainBright);
  float MainOpacity;
  parms(section, "MainOpacity", MainOpacity, 0.8f);
  stack->main()->setOpacity(MainOpacity);
  parms(section, "Main16Bit", Main16Bit, defaultTex16Bits);
  bool MainLabels;
  parms(section, "MainLabels", MainLabels, false);
  stack->main()->setLabels(MainLabels);

  bool WorkShow;
  parms(section, "WorkShow", WorkShow, false);
  if(WorkShow)
    stack->work()->show();
  else
    stack->work()->hide();
  float WorkBright;
  parms(section, "WorkBright", WorkBright, 1.0f);
  stack->work()->setBrightness(WorkBright);
  float WorkOpacity;
  parms(section, "WorkOpacity", WorkOpacity, 0.8f);
  stack->work()->setOpacity(WorkOpacity);
  parms(section, "Work16Bit", Work16Bit, defaultTex16Bits);
  bool WorkLabels;
  parms(section, "WorkLabels", WorkLabels, false);
  stack->work()->setLabels(WorkLabels);

  if(WorkStackFile == StackFile) {
    stack->work()->setFile(QString());
    if(WorkShow) {
      stack->main()->show();
      stack->work()->hide();
    }
  }

  bool hasMesh = not mesh->empty();
  bool SurfShow;
  parms(section, "SurfShow", SurfShow, hasMesh);
  if(SurfShow)
    mesh->showSurface();
  else
    mesh->hideSurface();
  float SurfBright;
  parms(section, "SurfBright", SurfBright, 1.0f);
  mesh->setBrightness(SurfBright);
  float SurfOpacity;
  parms(section, "SurfOpacity", SurfOpacity, 1.0f);
  mesh->setOpacity(SurfOpacity);
  bool SurfCull, SurfBlend;
  parms(section, "SurfBlend", SurfBlend, false);
  parms(section, "SurfCull", SurfCull, true);
  mesh->setCulling(SurfCull);
  mesh->setBlending(SurfBlend);

  int SurfView;
  parms(section, "SurfView", SurfView, (int)SurfaceView::Label);
  mesh->show((SurfaceView)SurfView);

  int SurfColor;
  parms(section, "SurfColor", SurfColor, (int)SurfaceColor::Signal);
  mesh->show((SurfaceColor)SurfColor);

  bool MeshShow, MeshPoints, MeshLines;
  parms(section, "MeshShow", MeshShow, hasMesh);
  mesh->setShowMesh(MeshShow);
  parms(section, "MeshPoints", MeshPoints, true);
  mesh->setShowMeshPoints(MeshPoints);
  parms(section, "MeshLines", MeshLines, true);
  mesh->setShowMeshLines(MeshLines);
  int mvm;
  parms(section, "MeshViewMode", mvm, (int)MeshView::Selected);
  mesh->show((MeshView)mvm);

  bool CellMap, ShowTrans, ShowBBox, TieScales;   //, ShowScale, TieScales;
  Point3f Scale = Point3f(1.0, 1.0, 1.0);
  // float Scale;
  parms(section, "CellMap", CellMap, false);
  mesh->setShowMeshCellMap(CellMap);
  parms(section, "ShowTrans", ShowTrans, false);
  stack->setShowTrans(ShowTrans);
  parms(section, "ShowBBox", ShowBBox, false);
  stack->setShowBBox(ShowBBox);
  // parms(section, "ShowScale", ShowScale, false);// we don't load ShowScale so that there is no scaling when
  // launching lgxp file
  // stack->setShowScale(ShowScale);
  parms(section, "TieScales", TieScales, true);
  stack->setTieScales(TieScales);
  parms(section, "ScaleX", Scale.x(), 1.0f);
  parms(section, "ScaleY", Scale.y(), 1.0f);
  parms(section, "ScaleZ", Scale.z(), 1.0f);

  bool LoadMeshScaleUm, LoadMeshTransform;
  parms(section, "LoadMeshScaleUm", LoadMeshScaleUm, true);
  mesh->setScaled(LoadMeshScaleUm);
  parms(section, "LoadMeshTransform", LoadMeshTransform, false);
  mesh->setTransformed(LoadMeshTransform);
  parms(section, "LoadMeshAdd", LoadMeshAdd, false);
  parms(section, "MeshShift", meshShift, 0.f);

  QString sf;
  parms(section, "MainColorMap", sf, QString());
  TransferFunction mainTransferFct = stack->main()->transferFct();
  mainTransferFct.clear();
  if(not sf.isEmpty()) {
    QString qsf = unshield(sf);
    mainTransferFct = TransferFunction::load(qsf);
  }
  if(mainTransferFct.empty()) {
    if(section == "Stack1")
      mainTransferFct = TransferFunction::scale_green();
    else
      mainTransferFct = TransferFunction::scale_red();
  }
  mainTransferDlg->setTransferFunction(mainTransferFct);
  changeMainColorMap(mainTransferFct);

  QString sf2;
  parms(section, "WorkColorMap", sf2, QString());
  TransferFunction workTransferFct = stack->work()->transferFct();
  workTransferFct.clear();
  if(not sf2.isEmpty()) {
    QString qsf = unshield(sf2);
    workTransferFct = TransferFunction::load(qsf);
  }
  if(workTransferFct.empty()) {
    if(section == "Stack1")
      workTransferFct = TransferFunction::scale_cyan();
    else
      workTransferFct = TransferFunction::scale_yellow();
  }
  workTransferDlg->setTransferFunction(workTransferFct);
  changeWorkColorMap(workTransferFct);

  QString sf3;
  parms(section, "SurfColorMap", sf3, QString());
  TransferFunction surfTransferFct = mesh->surfFct();
  surfTransferFct.clear();
  if(not sf3.isEmpty()) {
    QString qsf = unshield(sf3);
    surfTransferFct = TransferFunction::load(qsf);
  }
  if(surfTransferFct.empty())
    surfTransferFct = TransferFunction::scale_gray();
  surfTransferDlg->setTransferFunction(surfTransferFct);
  changeSurfColorMap(surfTransferFct);

  QString sf4;
  parms(section, "HeatColorMap", sf4, QString());
  TransferFunction heatTransferFct = mesh->heatFct();
  heatTransferFct.clear();
  if(not sf4.isEmpty()) {
    QString qsf = unshield(sf4);
    heatTransferFct = TransferFunction::load(qsf);
  }
  if(heatTransferFct.empty())
    heatTransferFct = TransferFunction::viridis();
  heatTransferDlg->setTransferFunction(heatTransferFct);
  changeHeatColorMap(heatTransferFct);

  Matrix4d m = 1;
  parms(section, "Frame", m, m);
  stack->frame().setFromMatrix(m.c_data());
  m = 1;
  parms(section, "Trans", m, m);
  stack->trans().setFromMatrix(m.c_data());

  if(section == "Stack1") {
    MeshColor = Colors::Mesh1Color;
    MeshBorderColor = Colors::Mesh1BorderColor;
    // MeshPointColor = Colors::Mesh1PointColor;
    MeshSelectColor = Colors::Mesh1SelectColor;
  } else {
    MeshColor = Colors::Mesh2Color;
    MeshBorderColor = Colors::Mesh2BorderColor;
    // MeshPointColor = Colors::Mesh2PointColor;
    MeshSelectColor = Colors::Mesh2SelectColor;
  }
}

void ImgData::setSurfColorMap(const TransferFunction& fct) {
  surfTransferDlg->setTransferFunction(fct);
}

void ImgData::setHeatColorMap(const TransferFunction& fct) {
  heatTransferDlg->setTransferFunction(fct);
}

void ImgData::setWorkColorMap(const TransferFunction& fct) {
  workTransferDlg->setTransferFunction(fct);
}

void ImgData::setMainColorMap(const TransferFunction& fct) {
  mainTransferDlg->setTransferFunction(fct);
}

// Set parameters for function
void ImgData::writeParms(QTextStream& pout, QString section)
{
  pout << endl;
  pout << "[" << section << "]" << endl;

  pout << "StackFile: " << stripCurrentDir(stack->main()->file()) << endl;
  pout << "WorkStackFile: " << stripCurrentDir(stack->work()->file()) << endl;
  pout << "MeshFile: " << stripCurrentDir(mesh->file()) << endl;

  pout << "MainShow: " << (stack->main()->isVisible() ? "true" : "false") << endl;
  pout << "MainBright:" << stack->main()->brightness() << endl;
  pout << "MainOpacity:" << stack->main()->opacity() << endl;
  pout << "Main16Bit:" << (Main16Bit ? "true" : "false") << endl;
  pout << "MainLabels:" << (stack->main()->labels() ? "true" : "false") << endl;

  pout << "WorkShow: " << (stack->work()->isVisible() ? "true" : "false") << endl;
  pout << "WorkBright:" << stack->work()->brightness() << endl;
  pout << "WorkOpacity:" << stack->work()->opacity() << endl;
  pout << "Work16Bit:" << (Work16Bit ? "true" : "false") << endl;
  pout << "WorkLabels:" << (stack->work()->labels() ? "true" : "false") << endl;

  pout << "SurfShow:" << (mesh->isSurfaceVisible() ? "true" : "false") << endl;
  pout << "SurfBright:" << mesh->brightness() << endl;
  pout << "SurfOpacity:" << mesh->opacity() << endl;
  pout << "SurfBlend:" << (mesh->blending() ? "true" : "false") << endl;
  pout << "SurfCull:" << (mesh->culling() ? "true" : "false") << endl;

  pout << "SurfView:" << (int)mesh->toShow() << endl;

  pout << "SurfColor:" << (int)mesh->coloring() << endl;

  pout << "MeshShow:" << (mesh->isMeshVisible() ? "true" : "false") << endl;
  pout << "MeshViewMode:" << (int)mesh->meshView() << endl;
  pout << "MeshPoints:" << (mesh->showMeshPoints() ? "true" : "false") << endl;
  pout << "MeshLines:" << (mesh->showMeshLines() ? "true" : "false") << endl;

  pout << "CellMap:" << (mesh->showMeshCellMap() ? "true" : "false") << endl;
  pout << "ShowTrans:" << (stack->showTrans() ? "true" : "false") << endl;
  pout << "ShowBBox:" << (stack->showBBox() ? "true" : "false") << endl;
  pout << "ShowScale:" << (stack->showScale() ? "true" : "false") << endl;
  pout << "TieScales:" << (stack->tieScales() ? "true" : "false") << endl;
  pout << "ScaleX:" << stack->scale().x() << endl;
  pout << "ScaleY:" << stack->scale().y() << endl;
  pout << "ScaleZ:" << stack->scale().z() << endl;
  pout << "Scale:" << norm(stack->scale()) << endl;

  pout << "LoadMeshAdd: " << (LoadMeshAdd ? "true" : "false") << endl;
  pout << "LoadMeshScaleUm: " << (mesh->scaled() ? "true" : "false") << endl;
  pout << "LoadMeshTransform: " << (mesh->transformed() ? "true" : "false") << endl;
  pout << "MeshShift: " << meshShift << endl;

  QString qsf = shield(stack->main()->transferFct().dump());
  pout << "MainColorMap: " << qsf << endl;
  qsf = shield(stack->work()->transferFct().dump());
  pout << "WorkColorMap: " << qsf << endl;
  qsf = shield(mesh->surfFct().dump());
  pout << "SurfColorMap: " << qsf << endl;
  qsf = shield(mesh->heatFct().dump());
  pout << "HeatColorMap: " << qsf << endl;

  pout << "Frame: " << Point16d(stack->frame().matrix()) << endl;
  pout << "Trans: " << Point16d(stack->trans().matrix()) << endl;
  pout << endl;
}

// Set stack sizes
void ImgData::updateStackSize()
{
  if(stack->empty())
    unloadTex();

  check3DTexture();

  emit changeSize(stack->size(), stack->step(), stack->origin());
}

namespace {
void drawPlane(Shader* shader, float scaling, int slices,
               GLboolean hasClip1, GLboolean hasClip2, GLboolean hasClip3,
               Point3f c1, Point3f c2, Point3f c3, Point3f c4)
{
  shader->setUniform("scaling", GLSLValue(scaling));
  shader->setUniform("slices", GLSLValue(float(slices) / 10000.f));
  shader->setUniform("has_clipped", GLSLValue(ivec3(hasClip1, hasClip2, hasClip3)));
  shader->useShaders();
  REPORT_GL_ERROR("drawStack");

  std::array<Point3f,4> pts = {{c4, c3, c2, c1}};
  opengl->glEnableClientState(GL_VERTEX_ARRAY);
  opengl->glVertexPointer(3, GL_FLOAT, 0, pts.data());
  opengl->glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
  //opengl->glDrawArrays(GL_QUADS, 0, pts.size());
  opengl->glDisableClientState(GL_VERTEX_ARRAY);
  REPORT_GL_ERROR("drawStack");

  shader->stopUsingShaders();
}
} // namespace

void ImgData::setupVolumeShader(Shader& shader)
{
  shader.setDefinition("COLOR_TEX", "0");
  shader.setDefinition("LABEL_TEX", "0");
  shader.setDefinition("SECOND_COLOR_TEX", "0");
  shader.setDefinition("SECOND_LABEL_TEX", "0");
  if(isMainVisible() and isWorkVisible()) {
    if(stack->main()->labels())
      shader.setDefinition("LABEL_TEX", "1");
    else
      shader.setDefinition("COLOR_TEX", "1");
    if(stack->work()->labels())
      shader.setDefinition("SECOND_LABEL_TEX", "1");
    else
      shader.setDefinition("SECOND_COLOR_TEX", "1");
  } else if(isMainVisible()) {
    if(stack->main()->labels())
      shader.setDefinition("LABEL_TEX", "1");
    else
      shader.setDefinition("COLOR_TEX", "1");
  } else if(isWorkVisible()) {
    if(stack->work()->labels())
      shader.setDefinition("SECOND_LABEL_TEX", "1");
    else
      shader.setDefinition("SECOND_COLOR_TEX", "1");
  }
}

void ImgData::setup3DRenderingData(Shader* shader)
{
  shader->setUniform("origin", GLSLValue(vec3(stack->origin())));
  shader->setUniform("imageSize", GLSLValue(vec3(stack->worldSize())));
  shader->setUniform("texSize", GLSLValue(ivec3(stack->size())));

  // When drawing 3D seeds, we need to also draw the main stack
  if(stack->main()->isVisible() and mainDataTexId) {
    bind3DTex(mainDataTexId, Shader::AT_TEX3D);
    setupMainColorMap();

    float brightness = powf(stack->main()->brightness(), 2.0f);
    float opacity = powf(stack->main()->opacity(), 2.0f);
    shader->setUniform("tex_brightness[0]", GLSLValue(brightness));
    shader->setUniform("tex_opacity[0]", GLSLValue(opacity));
  }

  if(stack->work()->isVisible() and workDataTexId) {
    bind3DTex(workDataTexId, Shader::AT_SECOND_TEX3D);
    setupWorkColorMap();

    float brightness = powf(stack->work()->brightness(), 2.0f);
    float opacity = powf(stack->work()->opacity(), 2.0f);
    shader->setUniform("tex_brightness[1]", GLSLValue(brightness));
    shader->setUniform("tex_opacity[1]", GLSLValue(opacity));
  }
}

void ImgData::draw2DStack(Shader* shader)
{
  if(!isVisible() or (not stack->is2D()) or !(mainDataTexId or workDataTexId)
     or ((stack->main()->opacity() == 0 or !isMainVisible()) and (stack->work()->opacity() == 0 or !isWorkVisible())))
    return;

  Point3f bottomLeft, bottomRight, topRight, topLeft;
  Point2u size2D = stack->size2D();

  if(stack->size().x() == 1) {
    bottomLeft = stack->imageToWorld(Point3f(0, -0.5, -0.5));
    bottomRight = stack->imageToWorld(Point3f(0, size2D.x()-0.5, -0.5));
    topRight = stack->imageToWorld(Point3f(0, size2D.x()-0.5, size2D.y()-0.5));
    topLeft = stack->imageToWorld(Point3f(0, -0.5, size2D.y()-0.5));
  } else if(stack->size().y() == 1) {
    bottomLeft = stack->imageToWorld(Point3f(-0.5, 0, -0.5));
    bottomRight = stack->imageToWorld(Point3f(size2D.x()-0.5, 0, -0.5));
    topRight = stack->imageToWorld(Point3f(size2D.x()+0.5, 0, size2D.y()-0.5));
    topLeft = stack->imageToWorld(Point3f(-0.5, 0, size2D.y()-0.5));
  } else {
    bottomLeft = stack->imageToWorld(Point3f(-0.5, -0.5, 0));
    bottomRight = stack->imageToWorld(Point3f(size2D.x()-0.5, -0.5, 0));
    topRight = stack->imageToWorld(Point3f(size2D.x()-0.5, size2D.y()-0.5, 0));
    topLeft = stack->imageToWorld(Point3f(-0.5, size2D.y()-0.5, 0));
  }

  // Prepare arrays
  std::array<Point3f, 4> pos = {{bottomLeft, bottomRight, topRight, topLeft}};
  static const std::array<Point2f, 4> tex = {{Point2f(0,0), Point2f(1,0), Point2f(1,1), Point2f(0,1)}};
  //auto c = Colorf(1, 1, 1, 1);
  //std::array<Colorf, 4> col = {c, c, c, c};

  opengl->glDisable(GL_TEXTURE_1D);
  opengl->glDisable(GL_TEXTURE_2D);
  opengl->glDisable(GL_TEXTURE_3D);
  opengl->glDisable(GL_BLEND);
  opengl->glDisable(GL_CULL_FACE);

  opengl->glEnable(GL_DEPTH_TEST);
  opengl->glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

  Shader::activeTexture(Shader::AT_LABEL_TEX);
  opengl->glBindTexture(GL_TEXTURE_1D, labelTexId);
  Shader::activeTexture(Shader::AT_NONE);

  setup3DRenderingData(shader);

  opengl->glPushMatrix();
  float offset = 0;
  if(StackId == 1)
    offset = -1e-4;

  shader->setUniform("offset", GLSLValue(offset));

  opengl->glMultMatrixd(getFrame().worldMatrix());
  if(stack->scale() != Point3f(1.0, 1.0, 1.0))
    opengl->glScaled(stack->scale().x(), stack->scale().y(), stack->scale().z());

  opengl->glEnableClientState(GL_VERTEX_ARRAY);
  auto texAttrib = shader->attribLocation("texCoord");
  opengl->glEnableVertexAttribArray(texAttrib);

  opengl->glVertexAttribPointer(texAttrib, 2, GL_FLOAT, GL_FALSE, 0, tex.data());
  opengl->glVertexPointer(3, GL_FLOAT, 0, pos.data());

  shader->useShaders();
  opengl->glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
  shader->stopUsingShaders();

  opengl->glPopMatrix();

  unbind3DTex();

  opengl->glDisableVertexAttribArray(texAttrib);
  //opengl->glDisableClientState(GL_TEXTURE_COORD_ARRAY);
  opengl->glDisableClientState(GL_VERTEX_ARRAY);
}

void ImgData::drawStack(Shader* shader)
{
  if(!isVisible() or stack->is2D() or !(mainDataTexId or workDataTexId)
     or ((stack->main()->opacity() == 0 or !isMainVisible())and (stack->work()->opacity() == 0 or !isWorkVisible())))
    return;

  opengl->glEnable(GL_AUTO_NORMAL);
  opengl->glDisable(GL_LIGHTING);
  // opengl->glDisable(GL_DEPTH_TEST);
  // opengl->glEnable(GL_MULTISAMPLE); // RSS screws up selection
  opengl->glDisable(GL_BLEND);

  opengl->glFrontFace(GL_CCW);
  opengl->glPolygonMode(GL_FRONT, GL_FILL);

  opengl->glDisable(GL_TEXTURE_1D);
  opengl->glDisable(GL_TEXTURE_2D);
  opengl->glDisable(GL_TEXTURE_3D);

  // Choose blending function
  //opengl->glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

  // Set matrix
  opengl->glPushMatrix();
  opengl->glMultMatrixd(getFrame().worldMatrix());

  GLdouble mv[16], proj[16];
  GLint viewport[4];
  opengl->glGetDoublev(GL_MODELVIEW_MATRIX, mv);
  opengl->glGetDoublev(GL_PROJECTION_MATRIX, proj);
  opengl->glGetIntegerv(GL_VIEWPORT, viewport);

  // Get unit vectors in x,y,z direction
  Point3f x(mv[0], mv[4], mv[8]);
  Point3f y(mv[1], mv[5], mv[9]);
  Point3f z(mv[2], mv[6], mv[10]);
  // Vertex array for quads
  std::vector<Point3f> vtxVA;

  // Render object aligned or view aligned
  const Point3f& Size = stack->worldSize();
  double x2 = Size.x() * Size.x() / 4;
  double y2 = Size.y() * Size.y() / 4;
  double z2 = Size.z() * Size.z() / 4;
  float dis = sqrt(x2 + y2 + z2);
  z *= dis;
  x *= dis;
  y *= dis;

  Shader::activeTexture(Shader::AT_LABEL_TEX);
  opengl->glBindTexture(GL_TEXTURE_1D, labelTexId);
  Shader::activeTexture(Shader::AT_NONE);

  GLdouble winX = viewport[2];
  GLdouble winY = viewport[3];

  typedef util::Vector<3, GLdouble> Point3GLd;
  Point3GLd c1, c2, c3, c4;
  gluUnProject(0, 0, 0, mv, proj, viewport, &c1.x(), &c1.y(), &c1.z());
  gluUnProject(0, winY, 0, mv, proj, viewport, &c2.x(), &c2.y(), &c2.z());
  gluUnProject(winX, winY, 0, mv, proj, viewport, &c3.x(), &c3.y(), &c3.z());
  gluUnProject(winX, 0, 0, mv, proj, viewport, &c4.x(), &c4.y(), &c4.z());

  GLboolean clip1, clip2, clip3;
  opengl->glGetBooleanv(GL_CLIP_PLANE0, &clip1);
  opengl->glGetBooleanv(GL_CLIP_PLANE2, &clip2);
  opengl->glGetBooleanv(GL_CLIP_PLANE4, &clip3);

  setup3DRenderingData(shader);

  drawPlane(shader, stack->scale().x(),
            _slices, clip1, clip2, clip3, Point3f(c1), Point3f(c2), Point3f(c3), Point3f(c4));

  // drawPlane(shader, Size, stack->size(), stack->origin(), stack->scale(),
  //          _slices,
  //          clip1, clip2, clip3,
  //          Point3f(c1), Point3f(c2), Point3f(c3), Point3f(c4));
  unbind3DTex();

  // opengl->glBlendEquation(GL_FUNC_ADD);
  opengl->glDisable(GL_BLEND);
  opengl->glPopMatrix();
  REPORT_GL_ERROR("drawStack");
}

void ImgData::drawBBox()
{
  if(!stack->showBBox() or !(mainDataTexId or workDataTexId))
    return;
  opengl->glDisable(GL_LIGHTING);
  opengl->glDisable(GL_TEXTURE_1D);
  opengl->glDisable(GL_TEXTURE_2D);
  opengl->glDisable(GL_TEXTURE_3D);
  opengl->glDisable(GL_BLEND);
  //opengl->glDisable(GL_ALPHA_TEST);
  opengl->glDisable(GL_AUTO_NORMAL);
  opengl->glEnable(GL_DEPTH_TEST);

  opengl->glLineWidth(MeshLineWidth);

  opengl->glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

  opengl->glPushMatrix();
  opengl->glMultMatrixd(getFrame().worldMatrix());

  Colorf c = Colors::getColor((StackId == 0) ? Colors::Stack1BBoxColor : Colors::Stack2BBoxColor);

  opengl->glColor4fv(c.c_data());

  const Point3f& Origin = stack->origin();
  Point3f c1 = Origin;
  Point3f c2 = Origin + stack->worldSize();

  opengl->glBegin(GL_QUADS);
  opengl->glVertex3f(c1.x(), c1.y(), c1.z());
  opengl->glVertex3f(c1.x(), c2.y(), c1.z());
  opengl->glVertex3f(c2.x(), c2.y(), c1.z());
  opengl->glVertex3f(c2.x(), c1.y(), c1.z());

  opengl->glVertex3f(c1.x(), c1.y(), c2.z());
  opengl->glVertex3f(c1.x(), c2.y(), c2.z());
  opengl->glVertex3f(c2.x(), c2.y(), c2.z());
  opengl->glVertex3f(c2.x(), c1.y(), c2.z());
  opengl->glEnd();

  opengl->glBegin(GL_LINES);
  opengl->glVertex3f(c1.x(), c1.y(), c1.z());
  opengl->glVertex3f(c1.x(), c1.y(), c2.z());

  opengl->glVertex3f(c1.x(), c2.y(), c1.z());
  opengl->glVertex3f(c1.x(), c2.y(), c2.z());

  opengl->glVertex3f(c2.x(), c2.y(), c1.z());
  opengl->glVertex3f(c2.x(), c2.y(), c2.z());

  opengl->glVertex3f(c2.x(), c1.y(), c1.z());
  opengl->glVertex3f(c2.x(), c1.y(), c2.z());
  opengl->glEnd();

  opengl->glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
  opengl->glPopMatrix();
  REPORT_GL_ERROR("drawBBox");
}

void ImgData::drawMeshSelection()
{
  vvgraph& S = mesh->graph();
  if(S.empty() or !mesh->isMeshVisible() or !meshSelect())
    return;

  opengl->glDisable(GL_LIGHTING);
  opengl->glDisable(GL_TEXTURE_1D);
  opengl->glDisable(GL_TEXTURE_2D);
  opengl->glDisable(GL_TEXTURE_3D);
  opengl->glDisable(GL_BLEND);
  //opengl->glDisable(GL_ALPHA_TEST);
  opengl->glDisable(GL_AUTO_NORMAL);
  opengl->glEnable(GL_DEPTH_TEST);
  // opengl->glEnable(GL_MULTISAMPLE);
  opengl->glFrontFace(GL_CCW);

  opengl->glPushMatrix();
  opengl->glMultMatrixd(getFrame().worldMatrix());
  // if(stack->scale() != 1.0)
  if(stack->scale() != Point3f(1.0, 1.0, 1.0))
    opengl->glScaled(stack->scale().x(), stack->scale().y(), stack->scale().z());
  // opengl->glScaled(stack->scale(), stack->scale(), stack->scale());

  opengl->glEnableClientState(GL_VERTEX_ARRAY);
  opengl->glEnableClientState(GL_COLOR_ARRAY);

  opengl->glClearColor(0, 0, 0, 1.0);
  opengl->glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  opengl->glPointSize(MeshPointSize * 2);

  opengl->glBindBuffer(GL_ARRAY_BUFFER, pntsVAid);
  opengl->glVertexPointer(3, GL_FLOAT, 0, 0);
  opengl->glBindBuffer(GL_ARRAY_BUFFER, pselVAid);
  opengl->glColorPointer(3, GL_UNSIGNED_BYTE, 0, 0);
  opengl->glBindBuffer(GL_ARRAY_BUFFER, 0);
  opengl->glDrawArrays(GL_POINTS, 0, pntsVA.size());

  opengl->glDisableClientState(GL_VERTEX_ARRAY);
  opengl->glDisableClientState(GL_COLOR_ARRAY);

  opengl->glPopMatrix();
  opengl->glDisable(GL_POLYGON_OFFSET_FILL);
  opengl->glDisable(GL_POLYGON_OFFSET_LINE);
}

// Draw mesh wireframe
void ImgData::drawMesh()
{
  vvgraph& S = mesh->graph();
  if(S.empty() or !mesh->isMeshVisible())
    return;

  opengl->glDisable(GL_LIGHTING);
  opengl->glDisable(GL_TEXTURE_1D);
  opengl->glDisable(GL_TEXTURE_2D);
  opengl->glDisable(GL_TEXTURE_3D);
  opengl->glDisable(GL_BLEND);
  //opengl->glDisable(GL_ALPHA_TEST);
  opengl->glDisable(GL_AUTO_NORMAL);
  opengl->glEnable(GL_DEPTH_TEST);
  // opengl->glEnable(GL_MULTISAMPLE);
  opengl->glFrontFace(GL_CCW);

  opengl->glPushMatrix();
  opengl->glMultMatrixd(getFrame().worldMatrix());
  // if(stack->scale() != 1.0)
  if(stack->scale() != Point3f(1.0, 1.0, 1.0))
    opengl->glScaled(stack->scale().x(), stack->scale().y(), stack->scale().z());
  // opengl->glScaled(stack->scale(), stack->scale(), stack->scale());

  if(mesh->showMeshLines()) {
    std::vector<uint> *lines = 0;

    switch(mesh->meshView()) {
    case MeshView::All:
      lines = &lineVA;
      break;
    case MeshView::Border:
      lines = &lbrdVA;
      break;
    case MeshView::Cell:
      lines = &lcellVA;
      break;
    case MeshView::Selected:
      lines = &lselVA;
      break;
    }

    if(lines and not lines->empty()) {
      opengl->glLineWidth(MeshLineWidth);
      opengl->glEnableClientState(GL_VERTEX_ARRAY);
      opengl->glEnableClientState(GL_COLOR_ARRAY);

      opengl->glBindBuffer(GL_ARRAY_BUFFER, pntsVAid);
      opengl->glVertexPointer(3, GL_FLOAT, 0, 0);
      opengl->glBindBuffer(GL_ARRAY_BUFFER, pcolVAid);
      opengl->glColorPointer(3, GL_UNSIGNED_BYTE, 0, 0);
      opengl->glBindBuffer(GL_ARRAY_BUFFER, 0);

      opengl->glDrawElements(GL_LINES, lines->size(), GL_UNSIGNED_INT, lines->data());
      opengl->glDisableClientState(GL_VERTEX_ARRAY);
      opengl->glDisableClientState(GL_COLOR_ARRAY);
    }
  }

  if(mesh->showMeshPoints()) {
    opengl->glEnableClientState(GL_VERTEX_ARRAY);
    opengl->glEnableClientState(GL_COLOR_ARRAY);

    opengl->glPointSize(MeshPointSize);

    if(mesh->meshView() == MeshView::All) {
      opengl->glBindBuffer(GL_ARRAY_BUFFER, pntsVAid);
      opengl->glVertexPointer(3, GL_FLOAT, 0, 0);
      opengl->glBindBuffer(GL_ARRAY_BUFFER, pcolVAid);
      opengl->glColorPointer(3, GL_UNSIGNED_BYTE, 0, 0);
      opengl->glBindBuffer(GL_ARRAY_BUFFER, 0);
      opengl->glDrawArrays(GL_POINTS, 0, pntsVA.size());
    } else {
      opengl->glBindBuffer(GL_ARRAY_BUFFER, pntsVAid);
      opengl->glVertexPointer(3, GL_FLOAT, 0, 0);
      opengl->glBindBuffer(GL_ARRAY_BUFFER, pcolVAid);
      opengl->glColorPointer(3, GL_UNSIGNED_BYTE, 0, 0);
      opengl->glBindBuffer(GL_ARRAY_BUFFER, 0);
      std::vector<uint>* points = 0;
      switch(mesh->meshView()) {
      case MeshView::Border:
        points = &pbrdVA;
        break;
      case MeshView::Cell:
        points = &pcellVA;
        break;
      case MeshView::Selected:
        points = &pselVA;
      case MeshView::All:
        break;
      }
      if(points and not points->empty())
        opengl->glDrawElements(GL_POINTS, points->size(), GL_UNSIGNED_INT, points->data());
    }

    opengl->glDisableClientState(GL_VERTEX_ARRAY);
    opengl->glDisableClientState(GL_COLOR_ARRAY);
  }

  if(DrawNormals > 0) {
    opengl->glLineWidth(MeshLineWidth);
    Color4f col = Colors::getColor(MeshBorderColor);
    opengl->glColor3fv(col.data());
    opengl->glBegin(GL_LINES);
    forall(const vertex& v, S) {
      opengl->glVertex3fv(v->pos.c_data());
      opengl->glVertex3fv((v->pos + DrawNormals * v->nrml).c_data());
    }
    opengl->glEnd();
  }
  if(DrawZeroLabels > 0) {
    opengl->glLineWidth(MeshLineWidth);
    Color4f col = Colors::getColor(MeshBorderColor);
    opengl->glColor3fv(col.data());
    opengl->glBegin(GL_LINES);
    forall(const vertex& v, S) {
      if(v->label != 0)
        continue;
      opengl->glVertex3fv(v->pos.c_data());
      opengl->glVertex3fv((v->pos + DrawZeroLabels * v->nrml).c_data());
    }
    opengl->glEnd();
  }
  if(DrawNhbds > 0) {
    opengl->glLineWidth(MeshLineWidth);
    Color4f col = Colors::getColor(MeshBorderColor);
    opengl->glColor3fv(col.data());
    opengl->glBegin(GL_LINES);
    forall(const vertex& v, S) {
      float d = .1f;
      forall(const vertex& n, S.neighbors(v)) {
        Point3f a = (n->pos - v->pos).normalize();
        opengl->glVertex3fv(v->pos.c_data());
        opengl->glVertex3fv((v->pos + DrawNhbds * (d * a + .1f * v->nrml)).c_data());
        d += .1f;
      }
    }
    opengl->glEnd();
  }
  /*
   * if(MeshBorderV) {
   *  opengl->glLineWidth(MeshLineWidth);
   *  Color4f col = Colors::getColor(MeshBorderColor);
   *  col /= 255.0;
   *  opengl->glColor3fv(col.data());
   *  opengl->glBegin(GL_LINES);
   *  forall(const vertex &v, S) {
   *    if(v->minb != 0) {
   *      vertex minb(v->minb);
   *      opengl->glVertex3fv(v->pos.c_data());
   *      opengl->glVertex3fv(minb->pos.c_data());
   *    }
   *  }
   *  opengl->glEnd();
   *}
   */
  opengl->glPopMatrix();
  opengl->glDisable(GL_POLYGON_OFFSET_FILL);
  opengl->glDisable(GL_POLYGON_OFFSET_LINE);
  REPORT_GL_ERROR("drawMesh");
}

// Draw line in between pairs of junctions of two corresponding cell meshes (used in MeshProcessGrowth)
void ImgData::drawVertexVertexLine(ImgData* otherstack)
{
  const vvgraph& S1 = this->mesh->graph();
  const vvgraph& S2 = otherstack->mesh->graph();

  IntMatrix3x3fMap& VVCorrespondance = this->mesh->vvCorrespondance();

  if(!this->mesh->isMeshVisible() and !otherstack->mesh->isMeshVisible() and !this->mesh->isSurfaceVisible()
     and !otherstack->mesh->isSurfaceVisible())
    return;

  if(S1.empty() or S2.empty() or VVCorrespondance.empty()) {
    DEBUG_OUTPUT("VVcorrespondance empty");
    return;
  }

  bool showlines = this->mesh->showVVCorrespondance();
  if(!showlines)
    return;

  // stack matrices
  Matrix4d st1 = ~Matrix4d(getFrame().worldMatrix());
  Matrix4d st2 = ~Matrix4d(otherstack->getFrame().worldMatrix());

  opengl->glLineWidth(1);
  opengl->glColor3f(1., 1., 1.);
  opengl->glDisable(GL_LIGHTING);
  opengl->glBegin(GL_LINES);
  for(int i = 1; i <= (int)VVCorrespondance.size(); i++) {
    if(VVCorrespondance.count(i) > 0) {
      Point4f v1pos = Point4f(VVCorrespondance[i][0]);
      Point4f v2pos = Point4f(VVCorrespondance[i][1]);
      v1pos.t() = v2pos.t() = 1.0f;
      // Clipping planes, point normal form
      Point4f v1pos_transf = st1 * v1pos;
      Point4f v2pos_transf = st2 * v2pos;
      opengl->glVertex4fv(v1pos_transf.c_data());
      opengl->glVertex4fv(v2pos_transf.c_data());
    }
  }
  opengl->glEnd();
  opengl->glEnable(GL_LIGHTING);
  REPORT_GL_ERROR("drawVertexVertexLine");
}

// Draw cell axis (3 lines). The color is defined separately for each axis in each cell
void ImgData::drawCellAxis()
{
  if(!mesh->isMeshVisible() and !mesh->isSurfaceVisible())
    return;

  const vvgraph& S = mesh->graph();
  const IntMatrix3x3fMap& cellAxisScaled = mesh->cellAxisScaled();
  const IntMatrix3x3fMap& cellAxisColor = mesh->cellAxisColor();
  float cellAxisOffset = mesh->cellAxisOffset();

  if(S.empty() or cellAxisScaled.empty())
    return;

  // Compute the color and positions of all the lines
  const IntPoint3fMap& centers = (mesh->isParentAxis() ? mesh->parentCenterVis() : mesh->labelCenterVis());
  const IntPoint3fMap& normals = (mesh->isParentAxis() ? mesh->parentNormalVis() : mesh->labelNormalVis());

  std::vector<Point3f> colors, points;
  // draw the 3 axis for each cell
  forall(const IntPoint3fPair& lc, centers) {
    int label = lc.first;
    auto found_axis = cellAxisScaled.find(label);
    auto found_color = cellAxisColor.find(label);
    auto found_normal = normals.find(label);
    if(found_axis != cellAxisScaled.end() and found_color != cellAxisColor.end()) {
      const Point3f& c = lc.second;
      const Matrix3f& axis = found_axis->second;
      const Matrix3f& col = found_color->second;

      Point3f offset;
      if(found_normal != normals.end() and cellAxisOffset != 0)
        offset = found_normal->second * cellAxisOffset;

      for(size_t i = 0; i < 3; ++i) {
        Point3f ax(axis[i]);
        Point3f color(col[i]);

        if(ax != Point3f()) {
          colors.push_back(color);
          colors.push_back(color);

          points.push_back(c - ax + offset);
          points.push_back(c + ax + offset);
        }
      }
    }
  }

  // Draw the lines

  // to follow camera and object transformation:
  opengl->glPushMatrix();
  opengl->glMultMatrixd(getFrame().worldMatrix());
  // if(stack->scale() != 1.0)
  if(norm(stack->scale()) != 1.0)
    opengl->glScaled(stack->scale().x(), stack->scale().y(), stack->scale().z());
  // opengl->glScaled(stack->scale(), stack->scale(), stack->scale());

  opengl->glLineWidth(mesh->cellAxisWidth());
  opengl->glDisable(GL_LIGHTING);

  opengl->glEnableClientState(GL_COLOR_ARRAY);
  opengl->glEnableClientState(GL_VERTEX_ARRAY);

  opengl->glColorPointer(3, GL_FLOAT, 0, &colors.front());
  opengl->glVertexPointer(3, GL_FLOAT, 0, &points.front());
  opengl->glDrawArrays(GL_LINES, 0, points.size());

  opengl->glDisableClientState(GL_COLOR_ARRAY);
  opengl->glDisableClientState(GL_VERTEX_ARRAY);

  opengl->glPopMatrix();   // to follow camera and object transformation
  REPORT_GL_ERROR("drawCellAxis");
}

// Function for back-to-front sorting of triangles
bool bToFCompare(const std::pair<float, uint>& p1, const std::pair<float, uint>& p2) {
  return p1.first < p2.first;
}

void ImgData::drawSurfSelection()
{
  vvgraph& S = mesh->graph();
  if(S.empty() or meshSelect())
    return;

  opengl->glDisable(GL_LIGHTING);
  opengl->glDisable(GL_TEXTURE_1D);
  opengl->glDisable(GL_TEXTURE_2D);
  opengl->glDisable(GL_TEXTURE_3D);
  opengl->glDisable(GL_BLEND);
  opengl->glDisable(GL_CULL_FACE);
  opengl->glEnable(GL_DEPTH_TEST);
  opengl->glFrontFace(GL_CCW);
  opengl->glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

  opengl->glPushMatrix();
  opengl->glMultMatrixd(getFrame().worldMatrix());
  if(stack->scale() != Point3f(1.0, 1.0, 1.0))
    opengl->glScaled(stack->scale().x(), stack->scale().y(), stack->scale().z());

  opengl->glEnableClientState(GL_VERTEX_ARRAY);
  opengl->glBindBuffer(GL_ARRAY_BUFFER, posVAid);
  opengl->glVertexPointer(3, GL_FLOAT, 0, 0);

  opengl->glEnableClientState(GL_COLOR_ARRAY);
  opengl->glClearColor(0, 0, 0, 1);
  opengl->glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  opengl->glBindBuffer(GL_ARRAY_BUFFER, selVAid);
  opengl->glColorPointer(3, GL_UNSIGNED_BYTE, 0, 0);

  opengl->glDrawArrays(GL_TRIANGLES, 0, posVA.size());

  opengl->glDisableClientState(GL_COLOR_ARRAY);
  opengl->glDisableClientState(GL_VERTEX_ARRAY);

  opengl->glPopMatrix();

  REPORT_GL_ERROR("drawSurfSelection");
}

// Draw mesh shaded, with and without select
void ImgData::drawSurf(Shader* texture_shader, Shader* label_shader, Shader* volume_shader)
{
  vvgraph& S = mesh->graph();
  if(S.empty() or !mesh->isSurfaceVisible())
    return;

  opengl->glDisable(GL_LIGHTING);
  opengl->glDisable(GL_TEXTURE_1D);
  opengl->glDisable(GL_TEXTURE_2D);
  opengl->glDisable(GL_TEXTURE_3D);
  opengl->glDisable(GL_BLEND);
  opengl->glDisable(GL_CULL_FACE);
  opengl->glEnable(GL_DEPTH_TEST);
  opengl->glFrontFace(GL_CCW);
  opengl->glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

  opengl->glEnableClientState(GL_VERTEX_ARRAY);

  // First bind positions
  opengl->glBindBuffer(GL_ARRAY_BUFFER, posVAid);
  opengl->glVertexPointer(3, GL_FLOAT, 0, 0);

  Shader* shader = 0;
  Shader::activeTexture(Shader::AT_NONE);

  float offset = -2e-4;
  if(StackId == 1)
    offset = -3e-4;

  // Set matrix, scale if required
  opengl->glPushMatrix();
  opengl->glMultMatrixd(getFrame().worldMatrix());

  if(stack->scale() != Point3f(1.0, 1.0, 1.0))
    opengl->glScaled(stack->scale().x(), stack->scale().y(), stack->scale().z());

  GLuint texAttrib = 0;
  bool bound2D = false, bound3D = false;

  // Now do colors and/or texture
  setupSurfColorMap();
  setupHeatColorMap();
  setupLabelColorMap();
  opengl->glEnable(GL_LIGHTING);
  if(mesh->culling())
    opengl->glEnable(GL_CULL_FACE);
  // opengl->glDisable(GL_COLOR_MATERIAL);
  opengl->glBindBuffer(GL_ARRAY_BUFFER, nrmlVAid);
  opengl->glNormalPointer(GL_FLOAT, 0, 0);
  if(mesh->coloring() == SurfaceColor::Image and mesh->hasImgTex()) {
    shader = texture_shader;

    opengl->glEnableClientState(GL_NORMAL_ARRAY);
    texAttrib = shader->attribLocation("texCoord");
    opengl->glEnableVertexAttribArray(texAttrib);

    opengl->glColor4f(0, 0, 0, 1);
    bind2DTex(imgTexId, Shader::AT_TEX2D);
    bound2D = true;

    shader->setUniform("tex", GLSLValue(Shader::AT_TEX2D));
    shader->setUniform("blending", GLSLValue(mesh->blending()));

    opengl->glBindBuffer(GL_ARRAY_BUFFER, imgVAid);
    opengl->glVertexAttribPointer(texAttrib, 2, GL_FLOAT, GL_FALSE, 0, 0);
    opengl->glBindBuffer(GL_ARRAY_BUFFER, 0);
  } else if(mesh->coloring() == SurfaceColor::Texture and not stack->is2D()) {
    shader = volume_shader;

    opengl->glEnableClientState(GL_NORMAL_ARRAY);
    texAttrib = shader->attribLocation("texCoord");
    opengl->glEnableVertexAttribArray(texAttrib);

    setup3DRenderingData(shader);
    bound3D = true;

    shader->setUniform("blending", GLSLValue(mesh->blending()));

    opengl->glColor4f(1, 1, 1, 1);
    opengl->glBindBuffer(GL_ARRAY_BUFFER, posVAid);
    opengl->glVertexAttribPointer(texAttrib, 3, GL_FLOAT, GL_FALSE, 0, 0);
    opengl->glBindBuffer(GL_ARRAY_BUFFER, 0);
  } else {
    Point2f& signalBounds = mesh->signalBounds();
    if(signalBounds[1] <= signalBounds[0])
      signalBounds[1] = signalBounds[0] + 1;
    Point2f& heatMapBounds = mesh->heatMapBounds();
    if(heatMapBounds[1] <= heatMapBounds[0])
      heatMapBounds[1] = heatMapBounds[0] + 1;
    // The normal case (including labels and heatmap)
    shader = label_shader;

    opengl->glEnableClientState(GL_NORMAL_ARRAY);
    texAttrib = shader->attribLocation("texCoord");
    opengl->glEnableVertexAttribArray(texAttrib);

    shader->setUniform("labels", GLSLValue(mesh->toShow() == SurfaceView::Label or mesh->toShow() == SurfaceView::Parents));
    shader->setUniform("heatmap", GLSLValue(mesh->toShow() == SurfaceView::Heat));
    // shader->setUniform("surfcolormap", GLSLValue(Shader::AT_SURF_TEX));
    // shader->setUniform("labelcolormap", GLSLValue(Shader::AT_LABEL_TEX));
    // shader->setUniform("heatcolormap", GLSLValue(Shader::AT_HEAT_TEX));
    shader->setUniform("blending", GLSLValue(mesh->blending()));
    shader->setUniform("signalBounds", GLSLValue(signalBounds));
    shader->setUniform("heatBounds", GLSLValue(heatMapBounds));

    opengl->glColor4f(1, 1, 1, 1);
    opengl->glBindBuffer(GL_ARRAY_BUFFER, texVAid);
    opengl->glVertexAttribPointer(texAttrib, 3, GL_FLOAT, GL_FALSE, 0, 0);
    setupHeatColorMap();

    opengl->glBindBuffer(GL_ARRAY_BUFFER, 0);
  }

  shader->setUniform("offset", GLSLValue(offset));
  shader->setUniform("brightness", GLSLValue(powf(mesh->brightness(), 2.0f)));
  if(mesh->coloring() == SurfaceColor::Texture) {
    shader->setUniform("tex_brightness[0]", GLSLValue(powf(mesh->brightness(), 2.0f)));
    shader->setUniform("tex_opacity[0]", GLSLValue(mesh->opacity()));
    shader->setUniform("tex_brightness[1]", GLSLValue(powf(mesh->brightness(), 2.0f)));
    shader->setUniform("tex_opacity[1]", GLSLValue(mesh->opacity()));
  } else
    shader->setUniform("opacity", GLSLValue(mesh->opacity()));
  const BoundingBox3f& bbox = mesh->boundingBox();
  float meshSize = norm(bbox[1] - bbox[0]);
  shader->setUniform("shift", GLSLValue(meshShift * meshSize));
  shader->useShaders();
  setupSurfColorMap();

  opengl->glDrawArrays(GL_TRIANGLES, 0, posVA.size());

  shader->stopUsingShaders();
  opengl->glDisableClientState(GL_NORMAL_ARRAY);
  opengl->glDisableClientState(GL_TEXTURE_COORD_ARRAY);
  opengl->glDisableVertexAttribArray(texAttrib);
  opengl->glDisableClientState(GL_VERTEX_ARRAY);

  opengl->glPopMatrix();
  if(bound3D) unbind3DTex();
  if(bound2D) unbind2DTex();

  REPORT_GL_ERROR("drawSurf");
}

// Compute area of 2D triangle
float triangleArea2D(Point2f a, Point2f b, Point2f c)
{
  return fabs(((b.x() - a.x()) * (c.y() - a.y()) - (c.x() - a.x()) * (b.y() - a.y())) / 2.0);
}

// Subdivide bisect, recursively divide to prevent degenerate triangles
void ImgData::subdivideBisect(vertex v1, vertex v2, vertex v3)
{
  // v1, v2, v3 must be in order
  vertex a(0), b(0), c(0);

  // Find longest edge and divide
  a = v1;
  b = v2;
  c = v3;
  if((v2->pos - v3->pos).norm() > (a->pos - b->pos).norm()) {
    a = v2;
    b = v3;
    c = v1;
  } else if((v3->pos - v1->pos).norm() > (a->pos - b->pos).norm()) {
    a = v3;
    b = v1;
    c = v2;
  }

  vvgraph& S = mesh->graph();

  // Insert vertex in long edge if not already there
  if(S.edge(a, b)) {
    vertex d;
    d->type = 'j';
    S.insert(d);
    S.insertEdge(d, a);
    S.insertEdge(d, b);
    d->pos = (a->pos + b->pos) / 2.0;
    d->signal = (a->signal + b->signal) / 2.0;
    d->txpos = (a->txpos + b->txpos) / 2.0;
    if(a->label == b->label)
      d->label = a->label;
    S.replace(a, b, d);
    S.replace(b, a, d);
  } else {
    // Already there, must be the last triangle
    vertex d = S.prevTo(a, c);
    S.spliceAfter(c, a, d);
    S.spliceBefore(d, a, c);
    return;
  }

  // Create dividing edge
  vertex d(0);
  if(S.edge(a, c)) {
    d = S.prevTo(a, c);
    S.spliceAfter(c, a, d);
    S.spliceBefore(d, a, c);
    vertex v = S.nextTo(c, d);
    // If edge c,b split by v, connect d to v
    if(v != b) {
      S.spliceBefore(d, c, v);
      S.spliceAfter(v, c, d);
    }
  } else if(S.edge(b, c)) {
    d = S.nextTo(b, c);
    S.spliceBefore(c, b, d);
    S.spliceAfter(d, b, c);
    vertex v = S.prevTo(c, d);
    // If edge c,a split by v, connect d to v
    if(v != a) {
      S.spliceAfter(d, c, v);
      S.spliceBefore(v, c, d);
    }
  } else {
    throw(QString("subdivideBisect::Error:Both incoming short edges divided"));
  }

  // Find neighbor
  vertex n(0);
  if(S.prevTo(a, d) == S.nextTo(b, d)) {
    n = S.prevTo(a, d);
    // Neighbor already divided
    if(S.edge(d, n))
      return;
    subdivideBisect(b, a, n);
  }
}

// Get all vertices close to a set of border vertices
void ImgData::markBorder(float borderSize)
{
  vvgraph& S = mesh->graph();
  if(S.empty())
    return;

  SETSTATUS("Finding border vertices");

  try {
    // For cellular mesh we will mark all vertices
    if(mesh->cells()) {
      int bcount = 0;
      forall(const vertex& v, S)
        if(v->type == 'j') {
          v->minb = v.id();
          bcount++;
        } else
          v->minb = S.anyIn(v).id();
      SETSTATUS("Border vertices:" << bcount << ", vertices near border:" << S.size() << " (cellular mesh)");
    } else {
      // Process non cellular mesh, first all border vertices in R
      std::set<vertex> R;
      forall(const vertex& v, S)
        if(v->label == -1) {
          R.insert(v);
          v->minb = v.id();
        } else
          v->minb = 0;
      uint bsize = R.size();

      // Start with border vertices and propogate
      std::set<vertex> T = R;       // Temp list
      std::set<vertex> D;           // delete list
      std::set<vertex> I;           // insert list

      uint rsize;
      do {
        rsize = R.size();
        forall(const vertex& v, T) {
          // If a vertex doesn't add any neighbors, we'll assume it is interior
          bool del = true;
          forall(const vertex& n, S.neighbors(v)) {
            // If not in border front
            if(R.find(n) == R.end()) {
              vertex w = vertex(v->minb);
              if(w.isNull()) {
                Information::out << "markBorder::Error:Null vertex w" << endl;
                continue;
              }
              vertex minb = w;
              double min = (n->pos - w->pos).norm();
              // forall(const vertex &m, S.neighbors(w))
              forall(const vertex& m, S.neighbors(n)) {
                if(m->minb != 0) {
                  vertex mb(m->minb);
                  if(mb.isNull()) {
                    Information::out << "markBorder::Error:Null vertex mb" << endl;
                    continue;
                  }
                  double dis = (n->pos - mb->pos).norm();
                  if(dis < min) {
                    min = dis;
                    minb = mb;
                  }
                }
              }
              if(min <= borderSize) {
                n->minb = minb.id();
                R.insert(n);
                I.insert(n);
              }
              del = false;
            }
          }
          if(del)
            D.insert(v);
        }
        forall(const vertex& v, I)
          T.insert(v);
        forall(const vertex& v, D)
          T.erase(v);
        I.clear();
        D.clear();
      } while(rsize < R.size());       // Do till R stops growing
      SETSTATUS("Border vertices:" << bsize << ", vertices near border:" << R.size());
    }
  }
  catch(QString mesg) {
    throw(QString("markBorders::Unknow exception-" + mesg));
  }
  catch(...) {
    throw(QString("markBorders::Unknow exception"));
  }
}

bool ImgData::isBordTriangle(int label, vertex v, vertex n, vertex m, IntIntPair& wall)
{
  return mesh->isBordTriangle(label, v, n, m, wall);
  /*
   *  if(WallVId.empty() or v->minb == 0 or n->minb == 0 or m->minb == 0)
   *    return(false);
   *
   *  // Search through neighbor list and sum signal and area
   *  forall(int lab, LabelNeighbors[label]) {
   *    IntIntPair p(label, lab);
   *    VIdSet &VtxSet = WallVId[p];
   *
   *    // If triangle belongs to this side of wall
   *    int count = 0;
   *    if(VtxSet.find(v->minb) != VtxSet.end())
   *      count++;
   *    if(VtxSet.find(n->minb) != VtxSet.end())
   *      count++;
   *    if(VtxSet.find(m->minb) != VtxSet.end())
   *      count++;
   *    if(count < 2)
   *      continue;
   *
   *    wall = p;
   *    return true;
   *  }
   *  return(false);
   */
}

// Reset mesh
void ImgData::resetMesh()
{
  mesh->reset();
  fillVBOs();
}
// Load mesh from a file
// Routines to connect neighborhoods
void ImgData::makeNhbd(vvgraph& S, vertex v, vertex n1, vertex n2, vertex n3, vertex n4)
{
  S.insertEdge(v, n1);
  S.spliceAfter(v, n1, n2);
  S.spliceAfter(v, n2, n3);
  S.spliceAfter(v, n3, n4);
}

void ImgData::makeNhbd(vvgraph& S, vertex v, vertex n1, vertex n2, vertex n3, vertex n4, vertex n5, vertex n6)
{
  S.insertEdge(v, n1);
  S.spliceAfter(v, n1, n2);
  S.spliceAfter(v, n2, n3);
  S.spliceAfter(v, n3, n4);
  S.spliceAfter(v, n4, n5);
  S.spliceAfter(v, n5, n6);
}

void ImgData::makeNhbd(std::vector<vertex>& vtxs)
{
  vvgraph& S = mesh->graph();
  vertex v = vtxs[0];
  vertex pn(0);
  for(uint i = 1; i < vtxs.size(); i++) {
    vertex n = vtxs[i];
    if(i == 1)
      S.insertEdge(v, n);
    else
      S.spliceAfter(v, pn, n);
    pn = n;
  }
}

// Get points for ICP
int ImgData::getPointsICP(std::vector<Point3f>& pts, qglviewer::Frame& frame)
{
  vvgraph& S = mesh->graph();
  int count = 0;
  if(mesh->isMeshVisible() and !S.empty()) {
    forall(const vertex& v, S) {
      // Choose vertices based on what is shown in gui
      if((mesh->meshView() != MeshView::Selected)and !v->selected)
        continue;
      if((mesh->meshView() != MeshView::Border)and !v->margin)
        continue;
      if((mesh->meshView() != MeshView::Cell) and v->label != -1)
        continue;
      count++;
      pts.push_back(Point3f(frame.inverseCoordinatesOf(qglviewer::Vec(v->pos))));
    }
  }
  return count;
}

// Reset stack data
void ImgData::resetStack()
{
  stack->reset();
  unloadTex();
  emit stackUnloaded();
}

// Initialize controls
void ImgData::initControls(QWidget* viewer)
{
  // Transfer functions
  connect(workTransferDlg, &gui::TransferFunctionDlg::changedTransferFunction,
          this, &ImgData::changeWorkColorMap);
  connect(mainTransferDlg, &gui::TransferFunctionDlg::changedTransferFunction,
          this, &ImgData::changeMainColorMap);
  connect(surfTransferDlg, &gui::TransferFunctionDlg::changedTransferFunction,
          this, &ImgData::changeSurfColorMap);
  connect(heatTransferDlg, &gui::TransferFunctionDlg::changedTransferFunction,
          this, &ImgData::changeHeatColorMap);

  connect(workTransferDlg, SIGNAL(changedTransferFunction(const TransferFunction&)),
          viewer, SLOT(update3D()));
  connect(mainTransferDlg, SIGNAL(changedTransferFunction(const TransferFunction&)),
          viewer, SLOT(update3D()));
  connect(surfTransferDlg, SIGNAL(changedTransferFunction(const TransferFunction&)),
          viewer, SLOT(update3D()));
  connect(heatTransferDlg, SIGNAL(changedTransferFunction(const TransferFunction&)),
          viewer, SLOT(update3D()));
}

// Determine if triangle is labeled, all vertices labelled and the same (or -1 = boundary)
int ImgData::getLabel(vertex v1, vertex v2, vertex v3, bool useParentLabel) const
{
  return mesh->getLabel(v1, v2, v3, useParentLabel);
}

void ImgData::unloadTex()
{
  if(mainDataTexId)
    opengl->glDeleteTextures(1, &mainDataTexId);
  if(workDataTexId)
    opengl->glDeleteTextures(1, &workDataTexId);
  if(mcmapTexId)
    opengl->glDeleteTextures(1, &mcmapTexId);
  if(wcmapTexId)
    opengl->glDeleteTextures(1, &wcmapTexId);
  if(surfTexId)
    opengl->glDeleteTextures(1, &surfTexId);
  if(heatTexId)
    opengl->glDeleteTextures(1, &heatTexId);

  surfTexId = heatTexId = wcmapTexId = mcmapTexId = mainDataTexId = workDataTexId = 0;
  displayStep = Point3f();

  // Histograms require update
  invalidateHistogram(MainHist);
  invalidateHistogram(WorkHist);
  invalidateHistogram(SurfHist);
  invalidateHistogram(HeatHist);
}

// Get decimation factors for each dimension
Point3u ImgData::getTexStep()
{
  Point3u step;
  if(stack->is2D()) {
    step.z() = 1;
    auto size2d = stack->size2D();
    for(uint i = 0 ; i < 2 ; ++i) {
      step[i] = size2d[i] / Max2DTexSize[i];
      if(size2d[i] % Max2DTexSize[i] > 0)
        ++step[i];
    }
  } else {
    for(uint i = 0; i < 3; ++i) {
      step[i] = stack->size()[i] / MaxTexSize[i];
      if(stack->size()[i] % MaxTexSize[i] > 0)
        ++step[i];
    }
  }
  return step;
}

void ImgData::check3DTexture()
{
  Point3u step = getTexStep();
  if(step != displayStep or was2D != stack->is2D()) {
    Information::out << "new displayStep = " << step << " -- reloading textures" << endl;
    displayStep = step;
    reloadMainTex();
    reloadWorkTex();
    was2D = stack->is2D();
    emit viewerUpdate();
  }
}

bool ImgData::imageFormat(GLuint texId,
                          GLenum& internalFormat,
                          GLenum& externalFormat,
                          GLenum& dataType)
{
  bool isLabeled;
  bool is16Bits;
  if(texId == workDataTexId) {
    isLabeled = stack->work()->labels();
    is16Bits = Work16Bit;
  } else if(texId == mainDataTexId) {
    isLabeled = stack->main()->labels();
    is16Bits = Main16Bit;
  } else
    return false;
  if(isLabeled) {
    if(is16Bits) {
      internalFormat = GL_R16UI;
      dataType = GL_UNSIGNED_SHORT;
      externalFormat = GL_RED_INTEGER;
    } else {
      internalFormat = GL_R8UI;
      dataType = GL_UNSIGNED_BYTE;
      externalFormat = GL_RG_INTEGER;
    }
  } else {
    externalFormat = GL_RED;
    dataType = GL_UNSIGNED_SHORT;
    if(is16Bits)
      internalFormat = GL_R16;
    else
      internalFormat = GL_R8;
  }
  return true;
}

// Load data into texture, handle decimation if texture too big
void ImgData::load3DTexData(GLuint texId, const ushort* data)
{
  GLenum internalFormat;
  GLenum externalFormat;
  GLenum dataType;
  imageFormat(texId, internalFormat, externalFormat, dataType);

  if(stack->is2D()) {
    Point2u size2d = stack->size2D();
    Point2u step2d = Point2u(displayStep);
    HVecUS tpix;
    if(step2d != Point2u(1u, 1u)) {
      Point2u sz = Point2u(size2d.x() / step2d.x(), size2d.y() / step2d.y());
      tpix.resize(size_t(sz.x())*sz.y());
      ushort *p = tpix.data();
      auto start = step2d / 2;
      for(uint j = 0 ; j < size2d.y() ; ++j)
        for(uint i = 0 ; i < size2d.x() ; ++i, p++)
          *p = data[start.x() + i * step2d.x() +
                    (start.y() + j * step2d.y()) * size2d.x()];
      data = tpix.data();
      size2d = sz;
    }
    GLsizei width = size2d.x();
    if(width % 4 == 0)
      opengl->glPixelStorei(GL_UNPACK_ALIGNMENT, 8);
    else if(width % 2 != 0)
      opengl->glPixelStorei(GL_UNPACK_ALIGNMENT, 2);

    opengl->glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, size2d.x(), size2d.y(), 0, externalFormat, dataType, data);
    REPORT_GL_ERROR("glTexImage2D");

    opengl->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, interpolation(mainDataTexId));
    opengl->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, interpolation(mainDataTexId));
    opengl->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    opengl->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    float anisotropy;
    opengl->glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &anisotropy);
    opengl->glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, anisotropy);
  } else {
    auto size = stack->size();
    std::vector<ushort> tpix;
    if(displayStep != Point3u(1u, 1u, 1u)) {
      Point3u sz = Point3u(size.x() / displayStep.x(),
                           size.y() / displayStep.y(),
                           size.z() / displayStep.z());

      tpix.resize(size_t(sz.x()) * sz.y() * sz.z());
      ushort* p = tpix.data();

      // Decimate array
      auto start = displayStep / 2;
      for(uint k = 0 ; k < sz.z() ; ++k)
        for(uint j = 0 ; j < sz.y() ; ++j)
          for(uint i = 0 ; i < sz.x() ; ++i, p++)
            *p = data[offset(start.x() + i * displayStep.x(),
                             start.y() + j * displayStep.y(),
                             start.z() + k * displayStep.z())];


      size = sz;
      data = tpix.data();
    }

    GLsizei width = size.x();
    if(width % 4 == 0)
      opengl->glPixelStorei(GL_UNPACK_ALIGNMENT, 8);
    else if(width % 2 != 0)
      opengl->glPixelStorei(GL_UNPACK_ALIGNMENT, 2);
    opengl->glTexImage3D(GL_TEXTURE_3D, 0, internalFormat, size.x(), size.y(), size.z(),
                         0, externalFormat, dataType, data);
    REPORT_GL_ERROR("glTexImage3D");

    opengl->glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, interpolation(mainDataTexId));
    opengl->glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, interpolation(mainDataTexId));
    opengl->glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    opengl->glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    opengl->glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
    float anisotropy;
    opengl->glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &anisotropy);
    opengl->glTexParameterf(GL_TEXTURE_3D, GL_TEXTURE_MAX_ANISOTROPY_EXT, anisotropy);
  }

  // Histograms require update
  if(texId == mainDataTexId)
    invalidateHistogram(MainHist);
  else if(texId == workDataTexId)
    invalidateHistogram(WorkHist);
  opengl->glPixelStorei(GL_UNPACK_ALIGNMENT, 4);   // Restore default
}

void ImgData::reloadTex(GLuint texId)
{
  if(texId == 0 or stack->storeSize() == 0
     or (texId == mainDataTexId and stack->main()->data().size() != stack->storeSize())
     or (texId == workDataTexId and stack->work()->data().size() != stack->storeSize())) {
    unloadTex();
    return;
  }

  if(stack->is2D())
    opengl->glBindTexture(GL_TEXTURE_2D, texId);
  else
    opengl->glBindTexture(GL_TEXTURE_3D, texId);
  HVecUS& wdata = stack->work()->data();
  HVecUS& mdata = stack->main()->data();
  const ushort* data = (texId == mainDataTexId) ? mdata.data() : wdata.data();
  load3DTexData(texId, data);
}

// Update a section of a texture defined in image coordinates by a bounding box
void ImgData::updateTex(GLuint texId, BoundingBox3i bBox)
{
  if(texId == 0 or stack->storeSize() == 0 or stack->main()->data().size() != stack->storeSize()
     or stack->work()->data().size() != stack->storeSize())
    return;

  HVecUS& data = (texId == mainDataTexId ? stack->main()->data() : stack->work()->data());

  REPORT_GL_ERROR("Start of updateTex");

  GLenum internalFormat;
  GLenum externalFormat;
  GLenum dataType;
  imageFormat(texId, internalFormat, externalFormat, dataType);

  // Get factor to decimate texture if required
  using util::min;
  Point3u step = displayStep;

  if(stack->is2D()) {
    auto size2d = stack->size2D();
    BoundingBox2i bBox2d = stack->to2D(bBox);
    Point2i base = bBox2d[0];
    Point2i bsz = min(bBox2d.size() + 1, Point2i(size2d) - base);

    // Enlarge bounding box to multiples of steps
    if(step != Point3u(1u, 1u, 1u)) {
      for(uint i = 0 ; i < 2 ; ++i) {
        auto delta = base[i] % step[i];
        if(delta <= step[i]/2)
          delta = step[i] / 2 - delta;
        else
          delta = step[i] - delta + step[i] / 2;
        // Make sure the base starts in the middle of a sub-sampled section
        base[i] += delta;
        bsz[i] -= delta; // Extend the size to still end at the same place
        if(bsz[i] % step[i] != 0)
          bsz[i] += step[i];
        bsz[i] /= step[i];
      }
      HVecUS tpix(size_t(bsz.x()) * size_t(bsz.y()));

      ushort* p = tpix.data();
      for(int j = 0 ; j < bsz.y() ; ++j)
        for(int i = 0 ; i < bsz.x() ; ++i) {
          size_t off = base.x() + i*step.x() + (base.y() + j*step.y()) * size2d.x();
          *p++ = data[off];
        }

      opengl->glPixelStorei(GL_UNPACK_ALIGNMENT, 2);

      base /= Point2i(step);

      // Load texture subimage
      opengl->glBindTexture(GL_TEXTURE_2D, texId);
      opengl->glTexSubImage2D(GL_TEXTURE_2D, 0, base.x(), base.y(),
                              bsz.x(), bsz.y(), externalFormat, dataType, tpix.data());
      opengl->glBindTexture(GL_TEXTURE_2D, 0);
      if(REPORT_GL_ERROR("glTexSubImage2D with step != 1")) {
        Information::out << "base = " << base << " - bsz = " << bsz
          << "\n  step = " << step << " - size = " << size2d << endl;

      }
    } else {
      // Update only the part needed
      opengl->glPixelStorei(GL_UNPACK_ALIGNMENT, 2);

      opengl->glBindTexture(GL_TEXTURE_2D, texId);
      opengl->glPixelStorei(GL_UNPACK_ROW_LENGTH, size2d.x());
      opengl->glTexSubImage2D(GL_TEXTURE_2D, 0, base.x(), base.y(), bsz.x(), bsz.y(),
                              externalFormat, dataType, &data[size_t(base.y()) * size2d.x() + base.x()]);
      opengl->glPixelStorei(GL_UNPACK_ROW_LENGTH, 0);
      opengl->glPixelStorei(GL_UNPACK_IMAGE_HEIGHT, 0);
      opengl->glBindTexture(GL_TEXTURE_2D, 0);
      REPORT_GL_ERROR("glTexSubImage2D with step == 1");
    }
  } else {
    auto size = stack->size();
    Point3i base = bBox[0];
    Point3i bsz = min(bBox.size() + 1, Point3i(size) - base);

    // Enlarge bounding box to multiples of steps
    if(step != Point3u(1u, 1u, 1u)) {
      for(uint i = 0 ; i < 3 ; ++i) {
        auto delta = base[i] % step[i];
        if(delta <= step[i]/2)
          delta = step[i] / 2 - delta;
        else
          delta = step[i] - delta + step[i] / 2;
        // Make sure the base starts in the middle of a sub-sampled section
        base[i] += delta;
        bsz[i] -= delta; // Extend the size to still end at the same place
        if(bsz[i] % step[i] != 0)
          bsz[i] += step[i];
        bsz[i] /= step[i];
      }

      HVecUS tpix(size_t(bsz.x()) * size_t(bsz.y()) * size_t(bsz.z()));

      ushort* p = tpix.data();
      for(int k = 0 ; k < bsz.z() ; ++k)
        for(int j = 0 ; j < bsz.y() ; ++j)
          for(int i = 0 ; i < bsz.x() ; ++i) {
            size_t off = offset(base.x() + i*step.x(),
                                base.y() + j*step.y(),
                                base.z() + k*step.z());
            *p++ = data[off];
          }

      opengl->glPixelStorei(GL_UNPACK_ALIGNMENT, 2);

      base /= Point3i(step);

      // Load texture subimage
      opengl->glBindTexture(GL_TEXTURE_3D, texId);
      opengl->glTexSubImage3D(GL_TEXTURE_3D, 0, base.x(), base.y(), base.z(),
                              bsz.x(), bsz.y(), bsz.z(), externalFormat, dataType, tpix.data());
      opengl->glBindTexture(GL_TEXTURE_3D, 0);
      if(REPORT_GL_ERROR("glTexSubImage3D with step != 1")) {
        Information::out << "base = " << base << " - bsz = " << bsz
          << "\n  step = " << step << " - size = " << size << endl;
      }
    } else {
      // Update only the part needed
      opengl->glPixelStorei(GL_UNPACK_ALIGNMENT, 2);

      opengl->glBindTexture(GL_TEXTURE_3D, texId);
      opengl->glPixelStorei(GL_UNPACK_ROW_LENGTH, size.x());
      opengl->glPixelStorei(GL_UNPACK_IMAGE_HEIGHT, size.y());
      opengl->glTexSubImage3D(GL_TEXTURE_3D, 0, base.x(), base.y(), base.z(), bsz.x(), bsz.y(), bsz.z(),
                              externalFormat, dataType, &data[offset(base.x(), base.y(), base.z())]);
      opengl->glPixelStorei(GL_UNPACK_ROW_LENGTH, 0);
      opengl->glPixelStorei(GL_UNPACK_IMAGE_HEIGHT, 0);
      opengl->glBindTexture(GL_TEXTURE_3D, 0);
      REPORT_GL_ERROR("glTexSubImage3D with step == 1");

    }
  }
  opengl->glPixelStorei(GL_UNPACK_ALIGNMENT, 4);     // Restore default

  // Histograms require update
  if(texId == mainDataTexId)
    invalidateHistogram(MainHist);
  else if(texId == workDataTexId)
    invalidateHistogram(WorkHist);
}

void ImgData::reloadMainTex(const BoundingBox3i& bbox)
{
  if(mainDataTexId and was2D == stack->is2D()) {
    if(bbox.empty() or bbox.size() == stack->size()) {
      DEBUG_OUTPUT("Reload the whole main texture" << endl);
      reloadTex(mainDataTexId);
    } else {
      DEBUG_OUTPUT("Reload the main texture for box: " << bbox << endl);
      updateTex(mainDataTexId, bbox);
    }
  } else {
    if(stack->storeSize() == 0 or stack->main()->data().size() != stack->storeSize()) {
      Information::out << "Error cannot reload main tex as SizeXYZ = " << stack->storeSize()
                       << " and MainData.size() = " << stack->main()->data().size() << endl;
      unloadTex();
      return;
    }
    if(mainDataTexId) {
      Information::out << "Delete texId " << mainDataTexId << endl;
      opengl->glDeleteTextures(1, &mainDataTexId);
    }
    SETSTATUS("Creating new texture of size " << stack->size());
    opengl->glGenTextures(1, &mainDataTexId);
    Information::out << "Allocated texId " << mainDataTexId << endl;
    if(stack->is2D()) {
      Information::out << "Load 2D stack" << endl;
      opengl->glBindTexture(GL_TEXTURE_2D, mainDataTexId);
    } else {
      Information::out << "Load 3D stack" << endl;
      opengl->glBindTexture(GL_TEXTURE_3D, mainDataTexId);
    }
    HVecUS& mdata = stack->main()->data();
    load3DTexData(mainDataTexId, mdata.data());
  }
}

void ImgData::reloadWorkTex(const BoundingBox3i& bbox)
{
  if(workDataTexId and was2D == stack->is2D()) {
    if(bbox.empty() or bbox.size() == stack->size()) {
      DEBUG_OUTPUT("Reload the whole work texture" << endl);
      reloadTex(workDataTexId);
    } else {
      DEBUG_OUTPUT("Reload the work texture for box: " << bbox << endl);
      updateTex(workDataTexId, bbox);
    }
  } else {
    if(stack->storeSize() == 0 or stack->work()->data().size() != stack->storeSize()) {
      Information::out << "Error cannot reload work tex as SizeXYZ = " << stack->storeSize()
                       << " and WorkData.size() = " << stack->main()->data().size() << endl;
      unloadTex();
      return;
    }
    if(workDataTexId)
      opengl->glDeleteTextures(1, &workDataTexId);
    opengl->glGenTextures(1, &workDataTexId);
    if(stack->is2D())
      opengl->glBindTexture(GL_TEXTURE_2D, workDataTexId);
    else
      opengl->glBindTexture(GL_TEXTURE_3D, workDataTexId);
    HVecUS& wdata = stack->work()->data();
    load3DTexData(workDataTexId, wdata.data());
  }
}

// Bind the stack texture
void ImgData::bind3DTex(GLuint texId, Shader::ActiveTextures atexId)
{
  if(texId) {
    GLenum target = (stack->is2D() ? GL_TEXTURE_2D : GL_TEXTURE_3D);
    Shader::activeTexture(atexId);
    opengl->glBindTexture(target, texId);
    opengl->glTexParameteri(target, GL_TEXTURE_MIN_FILTER, interpolation(texId));
    opengl->glTexParameteri(target, GL_TEXTURE_MAG_FILTER, interpolation(texId));
    Shader::activeTexture(Shader::AT_NONE);
    opengl->glBindTexture(target, 0);
    REPORT_GL_ERROR("bind3DTex");
  }
}

void ImgData::bind2DTex(GLuint texId, Shader::ActiveTextures atexId)
{
  if(texId) {
    Shader::activeTexture(atexId);
    opengl->glBindTexture(GL_TEXTURE_2D, texId);
    opengl->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, interpolation(texId));
    opengl->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, interpolation(texId));
    Shader::activeTexture(Shader::AT_NONE);
    opengl->glBindTexture(GL_TEXTURE_2D, 0);
    REPORT_GL_ERROR("bind2DTex");
  }
}

void ImgData::unbind3DTex()
{
  Shader::activeTexture(Shader::AT_TEX3D);
  opengl->glBindTexture(GL_TEXTURE_3D, 0);
  Shader::activeTexture(Shader::AT_SECOND_TEX3D);
  opengl->glBindTexture(GL_TEXTURE_3D, 0);
  Shader::activeTexture(Shader::AT_NONE);
  REPORT_GL_ERROR("unbind3DTex");
}

void ImgData::unbind2DTex()
{
  Shader::activeTexture(Shader::AT_TEX2D);
  opengl->glBindTexture(GL_TEXTURE_2D, 0);
  Shader::activeTexture(Shader::AT_NONE);
  REPORT_GL_ERROR("unbind2DTex");
}

// Load a texture image (for Keyence)
void ImgData::loadImgTex(const QImage& image)
{
  if(image.isNull() or image.width() <= 0 or image.height() <= 0)
    return;

  if(imgTexId)
    opengl->glDeleteTextures(1, &imgTexId);
  opengl->glGenTextures(1, &imgTexId);
  opengl->glBindTexture(GL_TEXTURE_2D, imgTexId);

  std::vector<float> pix(image.width() * image.height() * 3);
  float* px = pix.data();
  for(int y = image.height() - 1; y >= 0; y--)
    for(int x = 0; x < image.width(); x++) {
      *px++ = float(qRed(image.pixel(x, y))) / 255.0;
      *px++ = float(qGreen(image.pixel(x, y))) / 255.0;
      *px++ = float(qBlue(image.pixel(x, y))) / 255.0;
    }

  opengl->glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, image.width(), image.height(), 0, GL_RGB, GL_FLOAT, pix.data());

  opengl->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
  opengl->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  opengl->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
  opengl->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
  opengl->glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_BORDER);
}

void ImgData::initTex()
{
  // Initialize 1D index texture
  LabelColorsChanged = true;
  reloadLabelTex();
}

void ImgData::reloadLabelTex()
{
  if(LabelColorsChanged) {
    if(!labelTexId)
      opengl->glGenTextures(1, &labelTexId);
    opengl->glBindTexture(GL_TEXTURE_1D, labelTexId);

    if(!LabelColors.empty())
      opengl->glTexImage1D(GL_TEXTURE_1D, 0, GL_RGB, LabelColors.size(), 0, GL_RGBA, GL_FLOAT, LabelColors.data());
    else
      opengl->glTexImage1D(GL_TEXTURE_1D, 0, GL_RGB, 0, 0, GL_RGB, GL_FLOAT, 0);
    opengl->glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    opengl->glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    opengl->glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    opengl->glBindTexture(GL_TEXTURE_1D, 0);
    LabelColorsChanged = false;
  }
}

void ImgData::correctSelection(bool inclusive) {
  mesh->correctSelection(inclusive);
}

// Select vertices with a specified label
// Repeat used for "select on mesh" option on "Edit Labels" dialog
void ImgData::selectLabel(int label, int repeat)
{
  vvgraph& S = mesh->graph();
  if(!mesh->isSurfaceVisible() or S.empty())
    return;

  bool empty = true;
  vvgraph M;
  if(repeat > 0) {
    forall(const vertex& v, S)
      if((v->label % repeat) == label and !v->selected) {
        empty = false;
        v->selected = true;
      }
  } else {
    forall(const vertex& v, S)
      if(v->label == label and !v->selected) {
        empty = false;
        v->selected = true;
      }
  }
  if(empty)
    return;
  updateSelection();
}

// Unselect vertices with a specified label
void ImgData::unselectLabel(int label)
{
  vvgraph& S = mesh->graph();
  if(!mesh->isSurfaceVisible() or S.empty())
    return;

  bool empty = true;
  vvgraph M;
  forall(const vertex& v, S)
    if(v->label == label and v->selected) {
      empty = false;
      v->selected = false;
    }
  if(empty)
    return;
  updateSelection();
}

// Select vertices with a specified parent
void ImgData::selectParent(int parent, int repeat)
{
  vvgraph& S = mesh->graph();
  if(!mesh->isSurfaceVisible() or S.empty())
    return;

  bool empty = true;
  if(repeat > 0) {
    IntIntMap& parentMap = mesh->parentLabelMap();
    forall(const vertex& v, S) {
      IntIntMap::iterator it = parentMap.find(v->label);
      if(it != parentMap.end() and it->second % repeat == parent and !v->selected) {
        empty = false;
        v->selected = true;
      }
    }
  } else {
    IntIntMap& parentMap = mesh->parentLabelMap();
    forall(const vertex& v, S) {
      IntIntMap::iterator it = parentMap.find(v->label);
      if(it != parentMap.end() and it->second == parent and !v->selected) {
        empty = false;
        v->selected = true;
      }
    }
  }
  if(empty)
    return;
  updateSelection();
}

// Unselect vertices with a specified parent
void ImgData::unselectParent(int parent)
{
  vvgraph& S = mesh->graph();
  if(!mesh->isSurfaceVisible() or S.empty())
    return;

  bool empty = true;
  IntIntMap& parentMap = mesh->parentLabelMap();
  forall(const vertex& v, S) {
    IntIntMap::iterator it = parentMap.find(v->label);
    if(it != parentMap.end() and it->second == parent and v->selected) {
      empty = false;
      v->selected = false;
    }
  }
  if(empty)
    return;
  updateSelection();
}

// Select connected vertices
void ImgData::selectConnected(std::vector<uint>& vList, bool unselect)
{
  vvgraph& S = mesh->graph();
  if(!mesh->isSurfaceVisible() or S.empty() or vList.empty())
    return;

  vvgraph V;
  forall(uint u, vList) {
    vertex v = idVA[u];
    V.insert(v);
  }
  mesh->getConnectedVertices(V);
  forall(const vertex& v, V)
    v->selected = !unselect;
  updateSelection();
}

void ImgData::fillLabel(int label, int currlabel)
{
  // Make some checks
  if(label < 0 or label == currlabel)
    return;
  if(!mesh->isSurfaceVisible() or mesh->toShow() != SurfaceView::Label)
    return;
  vvgraph& S = mesh->graph();
  if(S.empty())
    return;
  mesh->setLabel(std::max(currlabel, mesh->viewLabel()));

  // If workstack has labels on and if do 3D label fill as well
  bool do3DFill = isWorkVisible() and stack->work()->labels();
  BoundingBox3f bBox(Point3f(FLT_MAX, FLT_MAX, FLT_MAX), Point3f(-FLT_MAX, -FLT_MAX, -FLT_MAX));

  // Find all vertices with current label and set to new label
  std::set<vertex> M;
  for(const vertex& v: S)
    if(v->label == label) {
      v->label = currlabel;
      M.insert(v);
    }
  // If none do nothing.
  if(M.empty())
    return;

  // Expand set to include neighbors, find bound box for 3D if reqd
  std::set<vertex> T = M;
  for(const vertex& v: T) {
    if(do3DFill)
      bBox |= v->pos;
    for(const vertex& n: S.neighbors(v))
      M.insert(n);
  }

  // Clear cell axis, cell center for new label
  mesh->labelCenter().erase(label);
  mesh->labelNormal().erase(label);
  mesh->labelCenterVis().erase(label);
  mesh->labelNormalVis().erase(label);
  mesh->cellAxis().erase(label);
  mesh->cellAxisScaled().erase(label);
  // Clear cell axis, cell center for current label
  mesh->labelCenter().erase(currlabel);
  mesh->labelNormal().erase(currlabel);
  mesh->labelCenterVis().erase(currlabel);
  mesh->labelNormalVis().erase(currlabel);
  mesh->cellAxis().erase(currlabel);
  mesh->cellAxisScaled().erase(currlabel);
  // Clear parent label
  IntIntMap::iterator p = mesh->parentLabelMap().find(label);
  if(p != mesh->parentLabelMap().end()) {
    int lparent = p->second;
    mesh->parentCenter().erase(lparent);
    mesh->parentNormal().erase(lparent);
    mesh->parentCenterVis().erase(lparent);
    mesh->parentNormalVis().erase(lparent);
    mesh->parentLabelMap().erase(label);
  }
  // Clear parent label
  IntIntMap::iterator cp = mesh->parentLabelMap().find(currlabel);
  if(cp != mesh->parentLabelMap().end()) {
    int currparent = cp->second;
    mesh->parentCenter().erase(currparent);
    mesh->parentNormal().erase(currparent);
    mesh->parentCenterVis().erase(currparent);
    mesh->parentNormalVis().erase(currparent);
    mesh->parentLabelMap().erase(currlabel);
  }

  // Update margin
  markMargin(M, true);
  needsLinesUpdate();
  needsTriangleUpdate();

  // Labels are stored byte swapped in the texture to allow the use of 8 bit textures.
  uint stklabel = ((uint(label) & 0xFF) << 8) + (uint(label) >> 8);
  uint stkcurrlabel = ((uint(currlabel) & 0xFF) << 8) + (uint(currlabel) >> 8);

  // Do 3D label fill if required
  if(do3DFill and label >= 0 and currlabel >= 0 and label < 65536 and currlabel < 65536) {
    // Convert bounding box to tex coords.
    BoundingBox3i bBoxTex(worldToImagei(bBox[0]) - Point3i(1, 1, 1), worldToImagei(bBox[1]) + Point3i(1, 1, 1));
    bBoxTex &= BoundingBox3i(Point3i(0, 0, 0), stack->size());

    // Update label in work tex
    HVecUS& wdata = stack->work()->data();
    for(int i = bBoxTex[0].x(); i < bBoxTex[1].x(); i++)
      for(int j = bBoxTex[0].y(); j < bBoxTex[1].y(); j++)
        for(int k = bBoxTex[0].z(); k < bBoxTex[1].z(); k++) {
          if(wdata[offset(i, j, k)] == stklabel)
            wdata[offset(i, j, k)] = stkcurrlabel;
        }
    updateTex(workDataTexId, bBoxTex);
  }
}

void ImgData::setParent(int label, int parentLabel)
{
  bool changed = false;

  if(parentLabel == 0) {
    mesh->parentLabelMap().erase(label);
    changed = true;
  } else if(mesh->parentLabelMap()[label] != parentLabel) {
    mesh->parentLabelMap()[label] = parentLabel;
    changed = true;
  }

  // See if we added something, and update GUI if required
  if(changed)
    needsTriangleUpdate();
}

void ImgData::addSeed(int label, std::vector<uint>& vList)
{
  if(!mesh->isSurfaceVisible() or mesh->toShow() != SurfaceView::Label or vList.empty())
    return;

  mesh->setLabel(std::max(label, mesh->viewLabel()));

  // Only lablel unlabeled vertices
  if(mesh->cells()) {
    std::set<uint> vset;
    forall(uint u, vList) {
      vertex v = idVA[u];
      if(v->type == 'c') {
        vset.insert(u);

        // Find all triangles that contain the center vertex
        for(uint i = 0; i < selVA.size(); i += 3)
          for(uint j = 0; j < 3; j++)
            if(idVA[i + j] == idVA[u])
              for(uint k = 0; k < 3; k++)
                vset.insert(i + k);
      }
    }
    std::vector<uint> tlist;
    forall(uint u, vset)
      tlist.push_back(u);
    updLabel(label, tlist);
  } else
    updLabel(label, vList);
}

// Fill selection with current
void ImgData::fillSelect(int label)
{
  vvgraph& S = mesh->graph();
  if(S.empty() or !mesh->isSurfaceVisible())
    return;

  if(mesh->toShow() == SurfaceView::Label) {
    std::set<vertex> M;
    forall(uint u, selectV) {
      vertex v = pidVA[u];
      if(v->label != label) {
        v->label = label;
        M.insert(v);
      }
    }
    markMargin(M, true);
    updateLines();
  } else if(mesh->toShow() == SurfaceView::Parents) {
    forall(uint u, selectV) {
      vertex v = pidVA[u];
      mesh->parentLabelMap()[v->label] = label;
    }
  } else
    return;

  needsTriangleUpdate();
}

// Delete Label
void ImgData::deleteLabel(int label)
{
  vvgraph& S = mesh->graph();
  if(S.empty())
    return;

  std::vector<vertex> T;

  if(meshSelect()) {
    if(mesh->showMeshPoints()) {
      forall(uint u, selectV) {
        vertex v = pidVA[u];
        T.push_back(v);
      }
      selectV.clear();
    }
  } else if(mesh->isSurfaceVisible() and mesh->toShow() == SurfaceView::Label) {
    // Clear any interior border lines
    //    markMargin(true);
    // Label delete, remove all vertices with this label
    // or all border vertices connected only to other border vertices or this label
    forall(const vertex& v, S) {
      if(v->label == label)
        T.push_back(v);
      else if(v->label == -1) {
        bool remove = true;
        forall(const vertex& n, S.neighbors(v)) {
          if(n->label != label and n->label != -1) {
            remove = false;
            break;
          }
        }
        if(remove)
          T.push_back(v);
      }
    }
  } else
    return;

  // Now delete them
  forall(const vertex& v, T)
    S.erase(v);

  // Delete any singletons
  T.clear();
  forall(const vertex& v, S)
    if(S.valence(v) <= 1)
      T.push_back(v);
  forall(const vertex& v, T)
    S.erase(v);

  fillVBOs();
  if(meshSelect() and mesh->showMeshPoints())
    SETSTATUS("Stack " << (StackId+1) << " deleted selected vertices");
  else
    SETSTATUS("Stack " << (StackId+1) << " deleted label:" << label);
}

// Mark margin vertices and border labels, set normals, count triangles and fill Point Id for VBOs
void ImgData::marginNormalsVBOs()
{
  vvgraph& S = mesh->graph();
  if(S.empty())
    return;

  size_t nb_bad_normal = 0;
  VCount = LCount = TCount = 0;
  int nextLabel = mesh->viewLabel();

  // Mark margin and borders
  uint vcnt = 0;
  pidVA.resize(S.size());
  forall(const vertex& v, S) {
    VCount++;
    // Save vertex id
    pidVA[vcnt] = v;
    v->saveId = vcnt++;

    // Clear normal
    v->nrml = Point3f(0, 0, 0);

    v->margin = false;
    // All vertices with valence <= 2 are border and margin
    if(S.valence(v) <= 2) {
      v->margin = true;
      if(v->type != 'l')
        v->label = -1;
    }

    int label = 0, labcount = 0, zcount = 0;
    forall(const vertex& n, S.neighbors(v)) {
      if(uniqueLine(S, v, n))
        LCount++;

      vertex m = S.nextTo(v, n);
      // add to normal
      if(n != m and S.edge(n, m)) {
        Point3f vn = n->pos - v->pos;
        Point3f vm = m->pos - v->pos;
        if(S.nextTo(n, m) != v)
          v->nrml -= vn.cross(vm);
        else
          v->nrml += vn.cross(vm);
      }
      if(mesh->uniqueTri(v, n, m))
        TCount++;

      // If there is no edge between a pair of vertices next to each other
      // in a neighborhood, they must be on the margin
      if(!S.edge(n, m)) {
        v->margin = true;
        if(v->type != 'l')
          v->label = -1;
        continue;
      }
      if(n->label == 0)
        zcount++;
      else if(n->label > 0 and n->label != label) {
        labcount++;
        label = n->label;
      }
    }
    // Check for 0 normals
    if(v->nrml.norm() == 0) {
      ++nb_bad_normal;
      v->nrml = Point3f(0, 0, 0);
    } else
      v->nrml.normalize();

    // Check for nan
    if(fabs(1.0 - v->nrml.norm()) < 1e-4)
      ;
    else {
      ++nb_bad_normal;
      v->nrml = Point3f(0, 0, 0);
    }

    // Margin vertices always have border label
    if(v->margin)
      continue;
    // Adjacent to more than one label means a cell boundary
    if(labcount > 1 and v->type != 'l')
      v->label = -1;
    // Clear hanging border lines,
    if(v->label == -1) {
      // On zero labeled area clear
      if(labcount == 0)
        v->label = 0;
      else if(labcount == 1 and zcount == 0)
        v->label = label;
    }
    if(v->label > nextLabel)
      nextLabel = v->label;
  }
  if(nextLabel > mesh->viewLabel())
    mesh->setLabel(nextLabel);
  if(nb_bad_normal > 0)
    SETSTATUS("markMargin:Error Bad normal for " << nb_bad_normal << " vertices");
}

void ImgData::updateTris(vvgraph& T)
{
  if(T.empty())
    return;

  int nextLabel = mesh->viewLabel();
  std::vector<uint> vList;
  forall(const vertex& v, T) {
    uint idx = vMap[v];
    while(idx < idVA.size() and v == idVA[idx]) {
      vertex n(idVA[idx + 1]), m(idVA[idx + 2]);
      int label = getLabel(v, n, m);
      texVA[idx] = texCoord(label, v, v, n, m);
      vList.push_back(idx++);
      texVA[idx] = texCoord(label, n, v, n, m);
      vList.push_back(idx++);
      texVA[idx] = texCoord(label, m, v, n, m);
      vList.push_back(idx++);
    }
    if(v->label > nextLabel)
      nextLabel = v->label;
  }
  if(nextLabel > mesh->viewLabel())
    mesh->setLabel(nextLabel);

  // Write texture data to video memory
  opengl->glBindBuffer(GL_ARRAY_BUFFER, texVAid);
  Point3f* tp = (Point3f*)(opengl->glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY));
  if(!tp) {
    REPORT_GL_ERROR("opengl->glMapBuffer(texVAid) - using opengl->glBufferData");
    opengl->glBufferData(GL_ARRAY_BUFFER, texVA.size() * sizeof(Point3f), texVA.data(), GL_STATIC_DRAW);
  } else {
    forall(uint u, vList)
      tp[u] = texVA[u];
    opengl->glUnmapBuffer(GL_ARRAY_BUFFER);
  }

  opengl->glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void ImgData::updateTris()
{
  needUpdateTris = false;
  if(idVA.empty())
    return;

  int nextLabel = mesh->viewLabel();
  uint tcnt = 0;
  for(size_t i = 0; i < idVA.size(); i += 3) {
    if(tcnt >= texVA.size())
      throw(QString("updateTris:Error Triangle index too large"));

    vertex v(idVA[i]), n(idVA[i + 1]), m(idVA[i + 2]);
    int label = getLabel(v, n, m);
    texVA[tcnt++] = texCoord(label, v, v, n, m);
    texVA[tcnt++] = texCoord(label, n, v, n, m);
    texVA[tcnt++] = texCoord(label, m, v, n, m);
    if(label > nextLabel)
      nextLabel = label;
  }
  if(nextLabel > mesh->viewLabel())
    mesh->setLabel(nextLabel);

  opengl->glBindBuffer(GL_ARRAY_BUFFER, texVAid);
  opengl->glBufferData(GL_ARRAY_BUFFER, texVA.size() * sizeof(Point3f), texVA.data(), GL_STATIC_DRAW);
  opengl->glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void ImgData::updatePos()
{
  if(idVA.empty() or pidVA.empty())
    return;

  int nextLabel = mesh->viewLabel();

  // Update positions for line and point drawing
  BoundingBox3f bBox(Point3f(FLT_MAX, FLT_MAX, FLT_MAX), Point3f(-FLT_MAX, -FLT_MAX, -FLT_MAX));
  for(size_t i = 0; i < pidVA.size(); i++) {
    vertex v(pidVA[i]);
    bBox |= v->pos;
    pntsVA[i] = v->pos;
    if(v->label > nextLabel)
      nextLabel = v->label;
  }
  mesh->setBoundingBox(bBox);

  // Update positions for triangles
  uint tcnt = 0;
  float step = std::max(stack->step().x(), std::max(stack->step().y(), stack->step().z()));
  for(size_t i = 0; i < idVA.size(); i += 3) {
    if(tcnt >= texVA.size())
      throw(QString("updateTex:Error Triangle index too large"));

    vertex v(idVA[i]), n(idVA[i + 1]), m(idVA[i + 2]);
    int label = getLabel(v, n, m);

    posVA[tcnt] = v->pos - ZOffset * step * v->nrml;
    nrmlVA[tcnt] = v->nrml;
    texVA[tcnt++] = texCoord(label, v, v, n, m);

    posVA[tcnt] = n->pos - ZOffset * step * n->nrml;
    nrmlVA[tcnt] = n->nrml;
    texVA[tcnt++] = texCoord(label, n, v, n, m);

    posVA[tcnt] = m->pos - ZOffset * step * m->nrml;
    nrmlVA[tcnt] = m->nrml;
    texVA[tcnt++] = texCoord(label, m, v, n, m);

    if(label > nextLabel)
      nextLabel = label;
  }
  if(nextLabel > mesh->viewLabel())
    mesh->setLabel(nextLabel);

  opengl->glBindBuffer(GL_ARRAY_BUFFER, posVAid);
  opengl->glBufferData(GL_ARRAY_BUFFER, posVA.size() * sizeof(Point3f), posVA.data(), GL_STATIC_DRAW);

  opengl->glBindBuffer(GL_ARRAY_BUFFER, nrmlVAid);
  opengl->glBufferData(GL_ARRAY_BUFFER, nrmlVA.size() * sizeof(Point3f), nrmlVA.data(), GL_STATIC_DRAW);

  opengl->glBindBuffer(GL_ARRAY_BUFFER, pntsVAid);
  opengl->glBufferData(GL_ARRAY_BUFFER, pntsVA.size() * sizeof(Point3f), pntsVA.data(), GL_STATIC_DRAW);

  opengl->glBindBuffer(GL_ARRAY_BUFFER, 0);
}

// Update point and lines VAs for cells and borders
void ImgData::updateLines()
{
  needUpdateLines = false;
  if(pidVA.empty())
    return;

  try {
    vvgraph& S = mesh->graph();
    pbrdVA.clear();
    pcellVA.clear();
    pselVA.clear();
    lbrdVA.clear();
    lcellVA.clear();
    lselVA.clear();

    for(size_t i = 0; i < pidVA.size(); i++) {
      // Border points, should be small
      vertex v = pidVA[i];
      if(v->selected)
        pselVA.push_back(v->saveId);
      pcolVA[i] = pColor(v);
      if(v->margin) {
        pbrdVA.push_back(v->saveId);
        pcellVA.push_back(v->saveId);
      } else if(v->label == -1) {
        pcellVA.push_back(v->saveId);
      } else if(!v->selected)
        continue;

      for(const vertex& n: S.neighbors(v)) {
        // Make arrays for lines
        if(uniqueLine(S, v, n)) {
          // Border lines
          if(v->margin and n->margin) {
            lbrdVA.push_back(v->saveId);
            lbrdVA.push_back(n->saveId);
            lcellVA.push_back(v->saveId);
            lcellVA.push_back(n->saveId);
          } else if(v->label == -1 and n->label == -1) {
            lcellVA.push_back(v->saveId);
            lcellVA.push_back(n->saveId);
          }
          if(v->selected) {
            lselVA.push_back(v->saveId);
            lselVA.push_back(n->saveId);
          }
        }
      }
    }
  } catch(QString msg) {
    SETSTATUS("updateLines::Error " << msg);
  } catch(...) {
    SETSTATUS("updateLines::Error unknown exception");
  }
}

// Fill the vertex array
void ImgData::fillVBOs()
{
  vvgraph& S = mesh->graph();
  float step = std::max(stack->step().x(), std::max(stack->step().y(), stack->step().z()));

  // Leave if nothing to draw
  if(S.empty())
    return;
  try {
    // Also counts triangles and fills in pidVA[]
    marginNormalsVBOs();

    // Vertex arrays for points
    pntsVA.resize(S.size());
    pcolVA.resize(S.size());
    pselcolVA.resize(S.size());
    pbrdVA.clear();
    pcellVA.clear();
    pselVA.clear();

    // Vertex arrays for lines
    lineVA.resize(LCount * 2);
    lbrdVA.clear();
    lcellVA.clear();
    lselVA.clear();

    // Vertex arrays for triangles
    idVA.resize(TCount * 3);
    posVA.resize(TCount * 3);
    nrmlVA.resize(TCount * 3);
    selVA.resize(TCount * 3);
    texVA.resize(TCount * 3);
    imgVA.resize(TCount * 3);
    vMap.clear();

    BoundingBox3f bBox(Point3f(FLT_MAX, FLT_MAX, FLT_MAX), Point3f(-FLT_MAX, -FLT_MAX, -FLT_MAX));

    uint vcnt = 0, lcnt = 0, tcnt = 0, pcol = 0, ucol = 0;
    vertex pv(0);
    forall(const vertex& v, pidVA) {
      // Make arrays for points
      pntsVA[vcnt] = v->pos;
      pselcolVA[vcnt] = vMapColor(pcol++);
      pcolVA[vcnt++] = pColor(v);

      bBox |= v->pos;

      // Border points, should be small
      if(v->margin) {
        pbrdVA.push_back(v->saveId);
        pcellVA.push_back(v->saveId);
      } else if(v->label == -1)
        pcellVA.push_back(v->saveId);
      if(v->selected)
        pselVA.push_back(v->saveId);

      // Do lines
      forall(const vertex& n, S.neighbors(v)) {
        // Make arrays for lines
        if(uniqueLine(S, v, n)) {
          if(int(lcnt) >= LCount * 2)
            throw(QString("Line count exceeded"));

          lineVA[lcnt++] = v->saveId;
          lineVA[lcnt++] = n->saveId;

          // Border lines, should be small
          if(v->margin and n->margin) {
            lbrdVA.push_back(v->saveId);
            lbrdVA.push_back(n->saveId);
            lcellVA.push_back(v->saveId);
            lcellVA.push_back(n->saveId);
          } else if(v->label == -1 and n->label == -1) {
            lcellVA.push_back(v->saveId);
            lcellVA.push_back(n->saveId);
          }
          if(v->selected) {
            lselVA.push_back(v->saveId);
            lselVA.push_back(n->saveId);
          }
        }

        vertex m = S.nextTo(v, n);
        if(not mesh->uniqueTri(v, n, m))
          continue;

        if(int(tcnt) >= TCount * 3)
          throw(QString("Triangle count exceeded"));

        // Draw based on label
        int label = getLabel(v, n, m);

        if(v != pv) {
          vMap[v] = tcnt;
          pv = v;
        }
        idVA[tcnt] = v;
        posVA[tcnt] = v->pos - ZOffset * step * v->nrml;
        nrmlVA[tcnt] = v->nrml;
        selVA[tcnt] = vMapColor(ucol);
        texVA[tcnt] = texCoord(label, v, v, n, m);
        imgVA[tcnt++] = v->txpos;

        idVA[tcnt] = n;
        posVA[tcnt] = n->pos - ZOffset * step * n->nrml;
        nrmlVA[tcnt] = n->nrml;
        selVA[tcnt] = vMapColor(ucol);
        texVA[tcnt] = texCoord(label, n, v, n, m);
        imgVA[tcnt++] = n->txpos;

        idVA[tcnt] = m;
        posVA[tcnt] = m->pos - ZOffset * step * m->nrml;
        nrmlVA[tcnt] = m->nrml;
        selVA[tcnt] = vMapColor(ucol++);
        texVA[tcnt] = texCoord(label, m, v, n, m);
        imgVA[tcnt++] = m->txpos;
      }
    }
    mesh->setBoundingBox(bBox);

    // Set up Vertex Buffer Object to put data onto graphics card
    if(!posVAid)
      opengl->glGenBuffers(1, &posVAid);
    opengl->glBindBuffer(GL_ARRAY_BUFFER, posVAid);
    opengl->glBufferData(GL_ARRAY_BUFFER, posVA.size() * sizeof(Point3f), posVA.data(), GL_STATIC_DRAW);
    if(!nrmlVAid)
      opengl->glGenBuffers(1, &nrmlVAid);
    opengl->glBindBuffer(GL_ARRAY_BUFFER, nrmlVAid);
    opengl->glBufferData(GL_ARRAY_BUFFER, nrmlVA.size() * sizeof(Point3f), nrmlVA.data(), GL_STATIC_DRAW);
    if(!texVAid)
      opengl->glGenBuffers(1, &texVAid);
    opengl->glBindBuffer(GL_ARRAY_BUFFER, texVAid);
    opengl->glBufferData(GL_ARRAY_BUFFER, texVA.size() * sizeof(Point3f), texVA.data(), GL_STATIC_DRAW);
    if(!imgVAid)
      opengl->glGenBuffers(1, &imgVAid);
    opengl->glBindBuffer(GL_ARRAY_BUFFER, imgVAid);
    opengl->glBufferData(GL_ARRAY_BUFFER, imgVA.size() * sizeof(Point2f), imgVA.data(), GL_STATIC_DRAW);
    if(!selVAid)
      opengl->glGenBuffers(1, &selVAid);
    opengl->glBindBuffer(GL_ARRAY_BUFFER, selVAid);
    opengl->glBufferData(GL_ARRAY_BUFFER, selVA.size() * sizeof(Point3GLub), selVA.data(), GL_STATIC_DRAW);
    if(!pntsVAid)
      opengl->glGenBuffers(1, &pntsVAid);
    opengl->glBindBuffer(GL_ARRAY_BUFFER, pntsVAid);
    opengl->glBufferData(GL_ARRAY_BUFFER, pntsVA.size() * sizeof(Point3f), pntsVA.data(), GL_STATIC_DRAW);
    if(!pcolVAid)
      opengl->glGenBuffers(1, &pcolVAid);
    opengl->glBindBuffer(GL_ARRAY_BUFFER, pcolVAid);
    opengl->glBufferData(GL_ARRAY_BUFFER, pcolVA.size() * sizeof(Point3GLub), pcolVA.data(), GL_STATIC_DRAW);
    if(!pselVAid)
      opengl->glGenBuffers(1, &pselVAid);
    opengl->glBindBuffer(GL_ARRAY_BUFFER, pselVAid);
    opengl->glBufferData(GL_ARRAY_BUFFER, pselcolVA.size() * sizeof(Point3GLub), pselcolVA.data(), GL_STATIC_DRAW);

    opengl->glBindBuffer(GL_ARRAY_BUFFER, 0);
  }
  catch(QString msg) {
    SETSTATUS("fillVBOs::Error " << msg);
  }
  catch(...) {
    SETSTATUS("fillVBOs::Error unknown exception");
  }
}

// Update a list of vertex positions in the VBO
void ImgData::updPos(std::set<uint>& vList, bool points)
{
  if(vList.empty())
    return;
  vvgraph& S = mesh->graph();

  // Find out which tables to update
  uint pVAid = points ? pntsVAid : posVAid;
  std::vector<vertex>& iVA = points ? pidVA : idVA;
  std::vector<Point3f>& pVA = points ? pntsVA : posVA;

  // Update normals including neighbors
  std::set<vertex> nlist;
  forall(uint u, vList) {
    vertex v = iVA[u];
    nlist.insert(v);
    forall(const vertex& n, S.neighbors(v))
      nlist.insert(n);
  }

  // Update vertex position in Vertex array table
  forall(uint u, vList) {
    vertex v = iVA[u];
    pVA[u] = v->pos;
  }

  // Write data to GPU
  opengl->glBindBuffer(GL_ARRAY_BUFFER, pVAid);
  Point3f* mp = (Point3f*)opengl->glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);
  if(!mp) {
    REPORT_GL_ERROR("opengl->glMapBuffer(pVAid) - using opengl->glBufferData");
    opengl->glBufferData(GL_ARRAY_BUFFER, pVA.size() * sizeof(Point3f), pVA.data(), GL_STATIC_DRAW);
  } else {
    forall(uint u, vList)
      mp[u] = pVA[u];
    opengl->glUnmapBuffer(GL_ARRAY_BUFFER);
  }
  opengl->glBindBuffer(GL_ARRAY_BUFFER, 0);
}

// Find selected triangle in triangle select mode (!meshSelect())
void ImgData::findSelectTriangle(uint x, uint y, std::vector<uint>& vList, int& label, bool useParentLabel)
{
  //emit needSelectionFI();

  GLint readBuffer, renderBuffer;
  glGetIntegerv(GL_READ_BUFFER, &readBuffer);
  glGetIntegerv(GL_DRAW_BUFFER, &renderBuffer);

  glDrawBuffer(GL_AUX0);

  // Draw unique colors in tha AUX0 buffer
  drawSurfSelection();

  glDrawBuffer(renderBuffer);

  // Find the color of the pixels drawn in BACK buffer
  Point3GLub pix;
  GLint viewport[4];
  opengl->glGetIntegerv(GL_VIEWPORT, viewport);
  opengl->glReadBuffer(GL_AUX0);
  opengl->glReadPixels(x, viewport[3] - y, 1, 1, GL_RGB, GL_UNSIGNED_BYTE, &pix);
  opengl->glReadBuffer(readBuffer);

  //emit needResetFI();
  int u = vMapColor(pix);
  // Return if nothing selected
  if(u < 0)
  {
    label = -1;
    return;
  }

  u *= 3;
  if(u + 3 > int(idVA.size())) {
    Information::out << "findSelectTriangle:Error Index too large, tri idx:" << u << " idVA size:" << idVA.size()
                     << endl;
    label = -1;
    return;
  }
  // Return triangle and label for selected color
  for(uint i = 0; i < 3; i++)
    vList.push_back(u + i);
  label = getLabel(idVA[u], idVA[u + 1], idVA[u + 2], useParentLabel);
}

// Trim bounding box with a clip plane in point normal form
void ImgData::bBoxClip(BoundingBox3f& bBox, Point3f p, Point3f n)
{
  int x, y, z;
  Point3f u1, u2, pt;
  float dir, s;

  // check x direction, find which line to test
  dir = n * Point3f(1.0f, 0.0f, 0.0f);
  y = (n.y() > 0 ? 1 : 0);
  z = (n.z() > 0 ? 1 : 0);
  u1 = Point3f(bBox[0].x(), bBox[y].y(), bBox[z].z());
  u2 = Point3f(bBox[1].x(), bBox[y].y(), bBox[z].z());
  if(planeLineIntersect(p, n, u1, u2, s, pt)) {
    if(dir > 0) {     // outside points in positive x dir
      if(pt.x() > bBox[0].x())
        bBox[0].x() = pt.x();
    } else {
      if(pt.x() < bBox[1].x())
        bBox[1].x() = pt.x();
    }
  }
  // check y direction, find which line to test
  dir = n * Point3f(0.0f, 1.0f, 0.0f);
  x = (n.x() > 0 ? 1 : 0);
  z = (n.z() > 0 ? 1 : 0);
  u1 = Point3f(bBox[x].x(), bBox[0].y(), bBox[z].z());
  u2 = Point3f(bBox[x].x(), bBox[1].y(), bBox[z].z());
  if(planeLineIntersect(p, n, u1, u2, s, pt)) {
    if(dir > 0) {
      if(pt.y() > bBox[0].y())
        bBox[0].y() = pt.y();
    } else {
      if(pt.y() < bBox[1].y())
        bBox[1].y() = pt.y();
    }
  }
  // check z direction, find which line to test
  dir = n * Point3f(0.0f, 0.0f, 1.0f);
  x = (n.x() > 0 ? 1 : 0);
  y = (n.y() > 0 ? 1 : 0);
  u1 = Point3f(bBox[x].x(), bBox[y].y(), bBox[0].z());
  u2 = Point3f(bBox[x].x(), bBox[y].y(), bBox[1].z());
  if(planeLineIntersect(p, n, u1, u2, s, pt)) {
    if(dir > 0) {
      if(pt.z() > bBox[0].z())
        bBox[0].z() = pt.z();
    } else {
      if(pt.z() < bBox[1].z())
        bBox[1].z() = pt.z();
    }
  }
}

//  Calculate bounding box from clipping planes (in point normal form)
void ImgData::bBoxFromClip()
{
  // clear, set to entire texture
  Point3f Origin = stack->origin();
  const Point3f& Size = stack->size();
  bBox[0] = Origin;
  bBox[1] = Origin + Size;

  // Test all 6 planes (twice)
  for(int i = 0; i < 2; i++)
    for(int pln = 0; pln < 6; pln++) {
      if(!clipDo[pln / 2])
        continue;

      Point3f n(pn[pln].x(), pn[pln].y(), pn[pln].z());
      float d = pn[pln][3];
      bBoxClip(bBox, n * -d, n);
    }
}

// Setup data for clipping test
void ImgData::getClipTestData(ClipRegion& clip1, ClipRegion& clip2, ClipRegion& clip3)
{
  Clip* c1 = clip1.clip;
  Clip* c2 = clip2.clip;
  Clip* c3 = clip3.clip;
  // Indicator if in use
  clipDo[0] = (c1->enabled() or c1->grid() ? 1 : 0);
  clipDo[1] = (c2->enabled() or c2->grid() ? 1 : 0);
  clipDo[2] = (c3->enabled() or c3->grid() ? 1 : 0);

  // Frame matrix
  frm = Matrix4d(getFrame().worldMatrix());

  // Clip plane matrices
  clm[0] = Matrix4d(c1->frame().inverse().worldMatrix());
  clm[1] = Matrix4d(c2->frame().inverse().worldMatrix());
  clm[2] = Matrix4d(c3->frame().inverse().worldMatrix());

  // Clipping planes, point normal form
  pn[0] = Point4f(frm * clm[0] * Point4f(c1->normal().x(), c1->normal().y(), c1->normal().z(), c1->width()));
  pn[1] = Point4f(frm * clm[0] * Point4f(-c1->normal().x(), -c1->normal().y(), -c1->normal().z(), c1->width()));
  pn[2] = Point4f(frm * clm[1] * Point4f(c2->normal().x(), c2->normal().y(), c2->normal().z(), c2->width()));
  pn[3] = Point4f(frm * clm[1] * Point4f(-c2->normal().x(), -c2->normal().y(), -c2->normal().z(), c2->width()));
  pn[4] = Point4f(frm * clm[2] * Point4f(c3->normal().x(), c3->normal().y(), c3->normal().z(), c3->width()));
  pn[5] = Point4f(frm * clm[2] * Point4f(-c3->normal().x(), -c3->normal().y(), -c3->normal().z(), c3->width()));

  // Put in host vector
  Hpn.resize(6);
  for(int i = 0; i < 6; i++)
    Hpn[i] = pn[i];
}

void ImgData::pixelEditStart(ClipRegion& clip1, ClipRegion& clip2, ClipRegion& clip3)
{
  if(stack->work()->isVisible()) {
    // Setup matrices and planes for clip test
    getClipTestData(clip1, clip2, clip3);

    // If 3D seeding, increment label number
    if(FillWorkData and stack->work()->isVisible() and SeedStack and stack->work()->labels()) {
      int lab = stack->nextLabel();
      SETSTATUS("Adding seed " << lab);
    }

    // Setup texture update area
    bBoxTex[0] = Point3i(stack->size().x(), stack->size().y(), stack->size().z());
    bBoxTex[1] = Point3i(0, 0, 0);
  }
}

void ImgData::pixelEditStop()
{
  // Reload the texture to see the edit result
  if(stack->work()->isVisible() and pixelsChanged)
    updateTex(workDataTexId, bBoxTex);
  pixelsChanged = false;
}

void ImgData::pixelEdit(float pixelRadius, const Point3f& p, const Point3f& px, const Point3f& py, const Point3f& pz,
                        bool doCut, int currentLabel)
{
  Point3f Origin = stack->origin();
  if(not stack->work()->isVisible()) return;
  // Clear bounding box, set to entire texture
  bBox[0] = Origin;
  bBox[1] = Origin + multiply(Point3f(stack->size()), stack->step());
  // Trim bounding box to clip planes
  for(int i = 0; i < 2; i++) {
    // Test all 6 planes (twice)
    for(int pln = 0; pln < 6; pln++) {
      if(not clipDo[pln / 2])
        continue;

      Point3f n(pn[pln].x(), pn[pln].y(), pn[pln].z());
      float d = pn[pln][3];
      bBoxClip(bBox, n * -d, n);
    }
    bBoxClip(bBox, p - pixelRadius * px, px);
    bBoxClip(bBox, p + pixelRadius * px, -px);
    bBoxClip(bBox, p - pixelRadius * py, py);
    bBoxClip(bBox, p + pixelRadius * py, -py);
    if(doCut) {
      bBoxClip(bBox, p - pixelRadius * pz, pz);
      bBoxClip(bBox, p + pixelRadius * pz, -pz);
    }
  }

  // Get bounding box in image coordinates and pad a pixel
  Point3i imgSize(stack->size().x(), stack->size().y(), stack->size().z());
  Point3i base = worldToImagei(bBox[0]) - Point3i(1, 1, 1);
  Point3i end = worldToImagei(bBox[1]) + Point3i(1, 1, 1);

  // Check bounds and update bound box for texture update
  for(int i = 0; i < 3; i++) {
    base[i] = std::max(base[i], 0);
    end[i] = std::min(end[i], imgSize[i]);
    bBoxTex[0][i] = std::min(bBoxTex[0][i], base[i]);
    bBoxTex[1][i] = std::max(bBoxTex[1][i], end[i]);
  }
  Point3i size = end - base;

  // Call cuda
  if(size.x() > 0 and size.y() > 0 and size.z() > 0) {
    ushort pixval = 0x0;
    if(FillWorkData) {
      if(stack->work()->labels()) {
        if(SeedStack)
          pixval = stack->viewLabel();
        else
          pixval = currentLabel;
      } else
        pixval = ImgData::FillWorkValue;
    }
    pixelEditGPU(base, size, imgSize, stack->step(), Origin, p, pz, pixelRadius, pixval, clipDo, Hpn,
                 stack->work()->data());
    pixelsChanged = true;
  }
  auto pixelEditMaxPix = PixelEditMaxPix;
#ifndef THRUST_BACKEND_CUDA
  pixelEditMaxPix /= 1000;
#endif

  if(pixelsChanged and size_t(bBoxTex[1].x() - bBoxTex[0].x()) * (bBoxTex[1].y() - bBoxTex[0].y())
     * (bBoxTex[1].z() - bBoxTex[0].z()) < size_t(pixelEditMaxPix)) {
    updateTex(workDataTexId, bBoxTex);
    pixelsChanged = false;
    bBoxTex[0] = Point3i(stack->size().x(), stack->size().y(), stack->size().z());
    bBoxTex[1] = Point3i(0, 0, 0);
  }
}

// Find selected point in mesh select mode (meshSelect())
int ImgData::findSelectPoint(uint x, uint y)
{
  GLint renderBuffer, readBuffer;
  glGetIntegerv(GL_READ_BUFFER, &readBuffer);
  glGetIntegerv(GL_DRAW_BUFFER, &renderBuffer);

  glDrawBuffer(GL_AUX0);
  // Draw unique colors in the BACK buffer
  drawMeshSelection();
  drawSurfSelection();
  glDrawBuffer(renderBuffer);
  // Find the color of the pixels drawn in AUX0 buffer
  Point3GLub pix;
  GLint viewport[4];
  opengl->glGetIntegerv(GL_VIEWPORT, viewport);
  opengl->glReadBuffer(GL_AUX0);
  opengl->glReadPixels(x, viewport[3] - y, 1, 1, GL_RGB, GL_UNSIGNED_BYTE, &pix);
  opengl->glReadBuffer(readBuffer);
  int i = vMapColor(pix);
  if(i >= int(pidVA.size())) {
    Information::out << "findSelectPoint::Error Index too large, point idx:" << i << " pidVA size:" << pidVA.size()
                     << endl;
    return -1;
  }
  return (i);
}

// Average a list of vertices to find select point
bool ImgData::findSeedPoint(uint x, uint y, CutSurf& cutSurf, Point3f& p)
{
  if(!SeedStack)
    return (false);

  vvgraph& S = mesh->graph();
  p = Point3f(0, 0, 0);
  if(cutSurf.cut->isVisible()) {
    cutSurf.drawCutSurf(*this, true);
    // Pixel color is x,y,z
    GLint viewport[4];
    opengl->glGetIntegerv(GL_VIEWPORT, viewport);
    opengl->glReadBuffer(GL_AUX0);
    opengl->glReadPixels(x, viewport[3] - y, 1, 1, GL_RGB, GL_FLOAT, &p);
    opengl->glReadBuffer(GL_FRONT);
    p -= Point3f(.5f, .5f, .5f);
  } else if(mesh->isSurfaceVisible() and !S.empty()) {
    std::vector<uint> vList;
    int label;
    findSelectTriangle(x, y, vList, label);
    if(vList.empty())
      return (false);
    forall(uint u, vList) {
      if(u < idVA.size()) {
        vertex v = idVA[u];
        p += v->pos;
      }
    }
    p /= vList.size();
  }
  return (true);
}

void ImgData::updateSelection()
{
  vvgraph& S = mesh->graph();
  selectV.clear();
  pselVA.clear();
  lselVA.clear();
  for(uint u = 0; u < pidVA.size(); ++u) {
    vertex v = pidVA[u];
    if(v->selected) {
      selectV.insert(u);
      pselVA.push_back(v->saveId);
      forall(const vertex& n, S.neighbors(v)) {
        lselVA.push_back(v->saveId);
        lselVA.push_back(n->saveId);
      }
    }
  }
  updateColors();
  needsLinesUpdate();
}

// Clear mesh selection
void ImgData::clearMeshSelect()
{
  std::vector<uint> vList;
  forall(uint u, selectV) {
    vertex v = pidVA[u];
    if(v->selected) {
      v->selected = false;
      vList.push_back(u);
    }
  }
  selectV.clear();
  if(vList.size() > 0)
    updateVertexesColors(vList);
  pselVA.clear();
  lselVA.clear();
  needsLinesUpdate();
}

// Remove points from selection
void ImgData::removeSelect(const std::vector<uint>& vList)
{
  forall(uint u, vList) {
    if(u >= pidVA.size())
      continue;
    vertex v = pidVA[u];
    v->selected = false;
  }
  updateSelection();
}

// Add point to selection
void ImgData::addSelect(const std::vector<uint>& vList)
{
  std::vector<uint> slist;
  forall(uint u, vList) {
    if(u >= pidVA.size())
      continue;
    vertex v = pidVA[u];
    std::set<uint>::iterator i = selectV.find(u);
    if(i == selectV.end()) {
      selectV.insert(u);
      if(!v->selected) {
        v->selected = true;
        slist.push_back(u);
      }
    }
  }
  if(selectV.size() == 1) {
    SETSTATUS("Vertex label: " << pidVA[*(selectV.begin())]->label);
  } else if(selectV.size() == 2) {
    std::set<uint>::iterator i = selectV.begin();
    vertex v1 = pidVA[*i++];
    vertex v2 = pidVA[*i];
    SETSTATUS("Distance bewteen vertices: " << (v1->pos - v2->pos).norm() << "um");
  } else
    SETSTATUS(selectV.size() << " vertex(es) selected");

  vvgraph& S = mesh->graph();
  if(slist.size() > 0) {
    forall(uint u, slist) {
      vertex v = pidVA[u];
      pselVA.push_back(u);
      forall(const vertex& n, S.neighbors(v)) {
        lselVA.push_back(u);
        lselVA.push_back(n->saveId);
      }
    }
    updateVertexesColors(vList);
    needsLinesUpdate();
  }
}

void ImgData::updateColors()
{
  std::vector<uint> idx(pidVA.size());
  for(uint i = 0; i < pidVA.size(); ++i)
    idx[i] = i;
  updateVertexesColors(idx);
}

// Update selection colors for points
void ImgData::updateVertexesColors(const std::vector<uint>& vList)
{
  if(vList.empty())
    return;

  forall(uint u, vList) {
    vertex v = pidVA[u];
    pcolVA[u] = pColor(v);
  }

  // Write color data to video memory
  opengl->glBindBuffer(GL_ARRAY_BUFFER, pcolVAid);
  Point3GLub* cp = (Point3GLub*)(opengl->glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY));
  if(!cp) {
    REPORT_GL_ERROR("opengl->glMapBuffer(pcolVAid) - using opengl->glBufferData");
    opengl->glBufferData(GL_ARRAY_BUFFER, pcolVA.size() * sizeof(Point3GLub), pcolVA.data(), GL_STATIC_DRAW);
  } else {
    forall(uint u, vList)
      cp[u] = pcolVA[u];
    opengl->glUnmapBuffer(GL_ARRAY_BUFFER);
  }
  opengl->glBindBuffer(GL_ARRAY_BUFFER, 0);
}

// Update a list of vertex labels
void ImgData::updLabel(int label, std::vector<uint>& vList)
{
  // Update vertex arrays
  bool changed = false;
  std::set<vertex> M;
  forall(uint u, vList) {
    vertex v(idVA[u]);
    if((!mesh->cells() or v->type == 'c')) {
      v->label = label;
      M.insert(v);
      changed = true;
    }
    texVA[u] = texCoord(label, v);
  }
  if(!changed)
    return;

  markMargin(M, true);
  needUpdateLines = true;

  // Write texture data to video memory
  opengl->glBindBuffer(GL_ARRAY_BUFFER, texVAid);
  Point3f* tp = (Point3f*)(opengl->glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY));
  if(!tp) {
    REPORT_GL_ERROR("opengl->glMapBuffer(texVAid) - using opengl->glBufferData");
    opengl->glBufferData(GL_ARRAY_BUFFER, texVA.size() * sizeof(Point3f), texVA.data(), GL_STATIC_DRAW);
  } else {
    forall(uint u, vList)
      tp[u] = texVA[u];
    opengl->glUnmapBuffer(GL_ARRAY_BUFFER);
  }

  opengl->glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void ImgData::updateHistogram(std::vector<uint64_t>& hist, Store* store, std::pair<double, double>& minMaxValues,
                              int max_data)
{
  store->updateHistogram(true);
  hist = store->histogram();
  minMaxValues.first = double(store->bounds()[0]) / max_data;
  minMaxValues.second = double(store->bounds()[1]+1) / max_data;
}

// Add a triangle to the VV mesh when converting from triangle list (triangles must be oriented)
bool ImgData::addTriangle(std::vector<vertex>& vertices, Point3i tri)
{
  vertex x = vertices[tri.x()];
  vertex y = vertices[tri.y()];
  vertex z = vertices[tri.z()];
  vvgraph& S = mesh->graph();
  if(S.valence(x) == 0) {
    S.insertEdge(x, y);
    S.spliceAfter(x, y, z);
  } else {
    bool yinx = S.edge(x, y);
    bool zinx = S.edge(x, z);

    if(yinx and zinx) {     // Triangle already there
      if(S.nextTo(x, y) != z)
        SETSTATUS("addTriangle::Error:Error in nhbd");
      return true;
    } else if(yinx) {
      S.spliceAfter(x, y, z);
      return true;
    } else if(zinx) {
      S.spliceBefore(x, z, y);
      return true;
    }
  }
  return false;   // Did not insert triangle
}

// Check graph for missing edges
void ImgData::chkGraph()
{
  vvgraph& S = mesh->graph();
  int count = 0;
  forall(const vertex& v, S)
    forall(const vertex& n, S.neighbors(v))
      if(!S.edge(n, v))
        count++;

  if(count > 0)
    SETSTATUS("chkGraph::Warning:Mesh has " << count << " missing edges");

  forall(const vertex& v, S) {
    float norm = v->pos.norm();
    if(norm == norm)
      ;
    else
      SETSTATUS("chkGraph::Warning:Bad vertex (nan)");
  }
}

// Mark margin vertices and border labels
void ImgData::markMargin(std::set<vertex>& M, bool remborders)
{
  vvgraph& S = mesh->graph();
  if(M.empty() or S.empty())
    return;

  // Mark margin and borders
  forall(const vertex& v, M) {
    v->margin = false;
    // All vertices with valence <= 2 are border and margin
    if(S.valence(v) <= 2) {
      v->margin = true;
      if(v->type != 'l')
        v->label = -1;
      continue;
    }

    int label = 0, labcount = 0, zcount = 0;
    forall(const vertex& n, S.neighbors(v)) {
      // If there is no edge between a pair of vertices next to each other
      // in a neighborhood, they must be on the margin
      vertex m = S.nextTo(v, n);
      if(!S.edge(n, m)) {
        v->margin = true;
        if(v->type != 'l')
          v->label = -1;
        break;
      }
      if(n->label == 0)
        zcount++;
      else if(n->label > 0 and n->label != label) {
        labcount++;
        label = n->label;
      }
    }
    // Margin vertices always have border label
    if(v->margin)
      continue;
    // Adjacent to more than one label means a cell boundary
    if(labcount > 1 and v->type != 'l')
      v->label = -1;
    // Clear hanging border lines,
    if(v->label == -1 and remborders) {
      // On zero labeled area, only clear if remborders set (so we can draw)
      if(labcount == 0)
        v->label = 0;
      else if(labcount == 1 and zcount == 0)
        v->label = label;
    }
  }
}

void ImgData::updateSurfHistogram()
{
  if(SurfHist.empty()) {
    // TODO
  }
}

void ImgData::updateHeatHistogram()
{
  if(HeatHist.empty()) {
    // TODO
  }
}
void ImgData::updateWorkHistogram()
{
  if(WorkHist.empty())
    updateHistogram(WorkHist, stack->work(), workBounds);
}

void ImgData::updateMainHistogram()
{
  if(MainHist.empty())
    updateHistogram(MainHist, stack->main(), mainBounds);
}

void ImgData::editMainTransferFunction()
{
  updateMainHistogram();
  mainTransferDlg->changeHistogram(MainHist);
  mainTransferDlg->changeBounds(mainBounds);
  //mainTransferDlg->changeBounds(mainBounds);
  mainTransferDlg->setTransferFunction(stack->main()->transferFct());
  if(mainTransferDlg->exec() == QDialog::Accepted) {
    emit changedInterface();
    changeMainColorMap(mainTransferDlg->transferFunction());
  }
}

void ImgData::editWorkTransferFunction()
{
  updateWorkHistogram();
  workTransferDlg->changeHistogram(WorkHist);
  workTransferDlg->changeBounds(workBounds);
  //workTransferDlg->changeBounds(workBounds);
  workTransferDlg->setTransferFunction(stack->work()->transferFct());
  if(workTransferDlg->exec() == QDialog::Accepted) {
    emit changedInterface();
    changeWorkColorMap(workTransferDlg->transferFunction());
  }
}

void ImgData::editSurfTransferFunction()
{
  updateSurfHistogram();
  surfTransferDlg->changeHistogram(SurfHist);
  //surfTransferDlg->changeBounds(surf);
  surfTransferDlg->setTransferFunction(mesh->surfFct());
  surfTransferDlg->changeRange(mesh->signalBounds()[0], mesh->signalBounds()[1]);
  if(surfTransferDlg->exec() == QDialog::Accepted) {
    emit changedInterface();
    changeSurfColorMap(surfTransferDlg->transferFunction());
  }
}

void ImgData::editHeatTransferFunction()
{
  updateHeatHistogram();
  heatTransferDlg->changeHistogram(HeatHist);
  //heatTransferDlg->changeBounds(heatMapBounds);
  heatTransferDlg->setTransferFunction(mesh->heatFct());
  heatTransferDlg->changeRange(mesh->heatMapBounds()[0], mesh->heatMapBounds()[1]);
  if(heatTransferDlg->exec() == QDialog::Accepted) {
    emit changedInterface();
    changeHeatColorMap(heatTransferDlg->transferFunction());
  }
}

void ImgData::updateColorMap(bool& newColorMap, std::vector<TransferFunction::Colorf>& ColorMap,
                             const TransferFunction& transferFct)
{
  static const int n = 4096;
  ColorMap.resize(n);
  double dc = 1.0 / (n - 1);
  for(int i = 0; i < n; ++i) {
    ColorMap[i] = transferFct.rgba(i * dc);
  }
  newColorMap = true;
}

void ImgData::setupColorMap(bool& newColorMap, GLuint& texId, const std::vector<TransferFunction::Colorf>& ColorMap,
                            Shader::ActiveTextures activeTex)
{
  if(newColorMap or !texId) {
    newColorMap = false;
    if(!texId)
      opengl->glGenTextures(1, &texId);
    opengl->glBindTexture(GL_TEXTURE_1D, texId);
    opengl->glTexImage1D(GL_TEXTURE_1D, 0, GL_RGBA8, ColorMap.size(), 0, GL_RGBA, GL_FLOAT, ColorMap.data());
    opengl->glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    opengl->glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    opengl->glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    opengl->glBindTexture(GL_TEXTURE_1D, 0);
  }
  Shader::activeTexture(activeTex);
  opengl->glBindTexture(GL_TEXTURE_1D, texId);
  Shader::activeTexture(Shader::AT_NONE);
}

void ImgData::changeWorkColorMap(const TransferFunction& fct)
{
  stack->work()->setTransferFct(fct);
  updateWorkColorMap();
}

void ImgData::changeMainColorMap(const TransferFunction& fct)
{
  stack->main()->setTransferFct(fct);
  updateMainColorMap();
}

void ImgData::changeSurfColorMap(const TransferFunction& fct)
{
  mesh->setSurfFct(fct);
  updateSurfColorMap();
}

void ImgData::changeHeatColorMap(const TransferFunction& fct)
{
  mesh->setHeatFct(fct);
  updateHeatColorMap();
}

void ImgData::setColorMap(const QString& pth, bool work)
{
  QFile f(pth);
  if(!f.open(QIODevice::ReadOnly)) {
    err << "Error, cannot open file '" << pth << "' for reading" << endl;
    return;
  }
  QTextStream ts(&f);
  QString content = ts.readAll();
  TransferFunction fct = TransferFunction::load(content);
  if(fct.empty()) {
    err << "Error, file '" << pth << "' doesn't contain a valid transfer function" << endl;
    return;
  }
  if(work)
    workTransferDlg->setTransferFunction(fct);
  else
    mainTransferDlg->setTransferFunction(fct);
}

// Return texture info for a vertex, used when triangle context not known (messes up wall heat map)
Point3f ImgData::texCoord(int label, vertex v)
{
  if(label <= 0)
    return Point3f(0.0f, v->signal, 0.0f);

  const IntFloatMap& LabelHeat = mesh->labelHeat();
  float retlab = float(label);
  const Point2f& heatMapBounds = mesh->heatMapBounds();
  IntFloatMap::const_iterator found = LabelHeat.find(label);
  if(found == LabelHeat.end())
    return Point3f(retlab, v->signal, 0.f);

  // Vertex has a cell based heatmap
  float heat = trim(found->second, heatMapBounds[0], heatMapBounds[1]);
  // Use a negative label to represent a valid heat
  return Point3f(-retlab, v->signal, heat);
}

// Return texture info for a vertex with triangle context. This allows to color walls.
Point3f ImgData::texCoord(int label, vertex v, vertex a, vertex b, vertex c)
{
  const IntIntFloatMap& WallHeat = mesh->wallHeat();
  if(WallHeat.empty())
    return texCoord(label, v);

  IntIntPair wall;
  const Point2f& heatMapBounds = mesh->heatMapBounds();
  float retlab = float(label);
  bool is_bord = isBordTriangle(label, a, b, c, wall);
  IntIntFloatMap::const_iterator found = WallHeat.end();
  if(is_bord)
    found = WallHeat.find(wall);
  if(found == WallHeat.end())
    return Point3f(retlab, v->signal, 0.f);

  // Vertex has a wall heat
  float heat = trim(found->second, heatMapBounds[0], heatMapBounds[1]);
  // Use a negative label to represent a valid heat
  return Point3f(-retlab, v->signal, heat);
}

// Slots for stack controls
// Main stack
void ImgData::showMain(bool val)
{
  if(val != stack->main()->isVisible()) {
    if(val)
      stack->main()->show();
    else
      stack->main()->hide();
    emit changedInterface();
    emit viewerUpdate();
  }
}

void ImgData::setMainBrightness(int val)
{
  float v = float(val) / 10000.0;
  if(stack->main()->brightness() != v) {
    stack->main()->setBrightness(v);
    emit changedInterface();
    emit viewerUpdate();
  }
}

void ImgData::setMainOpacity(int val)
{
  float v = float(val) / 10000.0;
  if(stack->main()->opacity() != v) {
    stack->main()->setOpacity(v);
    emit changedInterface();
    emit viewerUpdate();
  }
}

void ImgData::setMainLabel(bool val)
{
  if(val != stack->main()->labels()) {
    emit changedInterface();
    stack->main()->setLabels(val);
    reloadMainTex();
    emit viewerUpdate();
  }
}

void ImgData::setMain16Bits(bool val)
{
  if(val != Main16Bit) {
    emit changedInterface();
    Main16Bit = val;
    reloadMainTex();
    emit viewerUpdate();
  }
}

// Work stack
void ImgData::showWork(bool val)
{
  if(val != stack->work()->isVisible()) {
    emit changedInterface();
    if(val)
      stack->work()->show();
    else
      stack->work()->hide();
    emit viewerUpdate();
  }
}

void ImgData::setWorkBrightness(int val)
{
  float v = float(val) / 10000.0;
  if(stack->work()->brightness() != v) {
    emit changedInterface();
    stack->work()->setBrightness(v);
    emit viewerUpdate();
  }
}

void ImgData::setWorkOpacity(int val)
{
  float v = float(val) / 10000.0;
  if(stack->work()->opacity() != v) {
    emit changedInterface();
    stack->work()->setOpacity(v);
    emit viewerUpdate();
  }
}

void ImgData::setWorkLabels(bool val)
{
  if(val != stack->work()->labels()) {
    emit changedInterface();
    stack->work()->setLabels(val);
    reloadWorkTex();
    emit viewerUpdate();
  }
}

void ImgData::setWork16Bits(bool val)
{
  if(val != Work16Bit) {
    emit changedInterface();
    Work16Bit = val;
    reloadWorkTex();
    emit viewerUpdate();
  }
}

// Surface
void ImgData::showSurf(bool val)
{
  if(val != mesh->isSurfaceVisible()) {
    emit changedInterface();
    if(val)
      mesh->showSurface();
    else
      mesh->hideSurface();
    emit viewerUpdate();
  }
}

void ImgData::setSurfBrightness(int val)
{
  float v = float(val) / 10000.0;
  if(mesh->brightness() != v) {
    emit changedInterface();
    mesh->setBrightness(v);
    emit viewerUpdate();
  }
}

void ImgData::setSurfOpacity(int val)
{
  float v = float(val) / 10000.0;
  if(mesh->opacity() != v) {
    emit changedInterface();
    mesh->setOpacity(v);
    emit viewerUpdate();
  }
}

void ImgData::setSurfBlend(bool val)
{
  if(val != mesh->blending()) {
    emit changedInterface();
    mesh->setBlending(val);
    emit viewerUpdate();
  }
}

void ImgData::setSurfView(int val)
{
  auto view = (SurfaceView)val;
  if(view != mesh->toShow()) {
    emit changedInterface();
    bool updTris = (mesh->toShow() == SurfaceView::Parents or view == SurfaceView::Parents);
    mesh->show(view);
    if(updTris)
      updateTris();
    emit viewerUpdate();
  }
}

void ImgData::setSurfSignal(bool val)
{
  if(val and mesh->coloring() != SurfaceColor::Signal) {
    emit changedInterface();
    mesh->showSignal();
    emit viewerUpdate();
  }
}

void ImgData::setSurfTexture(bool val)
{
  if(val and mesh->coloring() != SurfaceColor::Texture) {
    emit changedInterface();
    mesh->showTexture();
    emit viewerUpdate();
  }
}

void ImgData::setSurfImage(bool val)
{
  if(val and mesh->coloring() != SurfaceColor::Image) {
    emit changedInterface();
    mesh->showImage();
    emit viewerUpdate();
  }
}

void ImgData::setSurfCull(bool val)
{
  if(val != mesh->culling()) {
    emit changedInterface();
    mesh->setCulling(val);
    emit viewerUpdate();
  }
}

// Mesh
void ImgData::showMesh(bool val)
{
  if(val != mesh->isMeshVisible()) {
    emit changedInterface();
    if(val)
      mesh->showMesh();
    else
      mesh->hideMesh();
    emit viewerUpdate();
  }
}

void ImgData::setChangeMeshViewMode(int mode)
{
  if(mode < 4) {
    auto m = (MeshView)mode;
    if(m != mesh->meshView()) {
      mesh->show(m);
      emit viewerUpdate();
      emit changedInterface();
    }
  }
}

void ImgData::setMeshLines(bool val)
{
  if(val != mesh->showMeshLines()) {
    emit changedInterface();
    mesh->setShowMeshLines(val);
    emit viewerUpdate();
  }
}

void ImgData::setMeshPoints(bool val)
{
  if(val != mesh->showMeshPoints()) {
    emit changedInterface();
    mesh->setShowMeshPoints(val);
    emit viewerUpdate();
  }
}

void ImgData::setCellMap(bool val)
{
  if(val != mesh->showMeshCellMap()) {
    emit changedInterface();
    mesh->setShowMeshCellMap(val);
    emit viewerUpdate();
  }
}

void ImgData::setShowTrans(bool val)
{
  if(val != stack->showTrans()) {
    emit changedInterface();
    stack->setShowTrans(val);
    emit viewerUpdate();
  }
}

void ImgData::setShowBBox(bool val)
{
  if(val != stack->showBBox()) {
    emit changedInterface();
    stack->setShowBBox(val);
    emit viewerUpdate();
  }
}

void ImgData::setShowScale(bool val)
{
  if(val != stack->showScale()) {
    emit changedInterface();
    stack->setShowScale(val);
    updateStackSize();
    emit viewerUpdate();
  }
}

void ImgData::setTieScales(bool val)
{
  if(val != stack->tieScales()) {
    emit changedInterface();
    stack->setTieScales(val);
    emit viewerUpdate();
  }
}

// Map back and forth between slider and scale
int ImgData::toSliderScale(float s) {
  return int((s - 1.0f) * 15000.0f);
}

float ImgData::fromSliderScale(int i) {
  return 1.0f + float(i) / 15000.0f;
}

// Scale slots in x,y,z
void ImgData::setScaleX(int val)
{
  float newScale = fromSliderScale(val);
  if(newScale != stack->scale().x()) {
    emit changedInterface();
    if(!stack->tieScales())
      stack->setScale(Point3f(newScale, stack->scale().y(), stack->scale().z()));
    else
      stack->setScale(Point3f(newScale, newScale, newScale));
    updateStackSize();
    emit updateSliderScale();
    emit viewerUpdate();
  }
}
void ImgData::setScaleY(int val)
{
  float newScale = fromSliderScale(val);
  if(newScale != stack->scale().y()) {
    emit changedInterface();
    if(!stack->tieScales())
      stack->setScale(Point3f(stack->scale().x(), newScale, stack->scale().z()));
    else
      stack->setScale(Point3f(newScale, newScale, newScale));
    updateStackSize();
    emit updateSliderScale();
    emit viewerUpdate();
  }
}
void ImgData::setScaleZ(int val)
{
  float newScale = fromSliderScale(val);
  if(newScale != stack->scale().z()) {
    emit changedInterface();
    if(!stack->tieScales())
      stack->setScale(Point3f(stack->scale().x(), stack->scale().y(), newScale));
    else
      stack->setScale(Point3f(newScale, newScale, newScale));
    updateStackSize();
    emit updateSliderScale();
    emit viewerUpdate();
  }
}
Point3f ImgData::imageGradientI(Point3i ipos)
{
  uint ix = ipos.x();
  uint iy = ipos.y();
  uint iz = ipos.z();
  Point3d grad(0.0);
  const Point3f& Size = stack->size();
  double xScale = (Size.x() / stack->size().x()) * 2.0;
  double yScale = (Size.y() / stack->size().y()) * 2.0;
  double zScale = (Size.z() / stack->size().z()) * 2.0;
  HVecUS& Data = currentData();
  grad[0] = (Data[offset(ix, iy, iz) + 1] - Data[offset(ix, iy, iz) - 1]) / xScale;
  grad[1] = (Data[offset(ix, iy, iz) + stack->size().x()] - Data[offset(ix, iy, iz) - stack->size().x()]) / yScale;
  grad[2] = (Data[offset(ix, iy, iz) + stack->size().x() * stack->size().y()]
             - Data[offset(ix, iy, iz) - stack->size().x() * stack->size().y()]) / zScale;
  return grad;
}

Point3f ImgData::imageGradientW(Point3d worldpos)
{
  Point3i ipos = worldToImagei(worldpos);
  return imageGradientI(ipos);
}

uint ImgData::imageLevel(Point3d worldpos)
{
  HVecUS& Data = currentData();
  Point3i ipos = worldToImagei(worldpos);
  return Data[offset(ipos)];
}

Point2i ImgData::imageMinMax()
{
  HVecUS& Data = currentData();
  uint min = 65535;
  uint max = 0;
  for(size_t i = 0; i < stack->storeSize(); ++i) {
    uint val = Data[i];
    if(val > max)
      max = val;
    if(val < min)
      min = val;
  }
  return Point2i(min, max);
}

void ImgData::updateImgData()
{
  if(needUpdateLines)
    updateLines();
  if(needUpdateTris)
    updateTris();
}

void ImgData::needsLinesUpdate()
{
  if(not needUpdateLines) {
    needUpdateLines = true;
    emit needsUpdate();
  }
}

void ImgData::needsTriangleUpdate()
{
  if(not needUpdateTris) {
    needUpdateTris = true;
    emit needsUpdate();
  }
}

void ImgData::showInfo()
{
  if(not info_dlg)
    info_dlg = new gui::StackInfoDlg(this);
  info_dlg->show();
}

bool ImgData::meshSelect()
{
  return tool == ST_MESH_SELECT;
}

bool ImgData::hasSelectedVertices()
{
  for(const vertex& v : mesh->graph())
    if(v->selected)
      return true;
  return false;
}

void ImgData::setSlices(uint slices)
{
  if(slices > 0)
    _slices = slices;
}

std::vector<vertex> ImgData::selectionToVertices(const std::vector<uint>& vids) const
{
  std::vector<vertex> result(vids.size(), vertex(0));
  for(size_t i = 0 ; i < vids.size() ; ++i)
    result[i] = idVA[vids[i]];
  return result;
}

} // namespace lgx
