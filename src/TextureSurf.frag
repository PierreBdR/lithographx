uniform sampler2D tex;
uniform float opacity;
uniform float brightness;
uniform bool blending;

smooth centroid in vec2 texPos;

void setColor()
{
  vec4 color = texture(tex, texPos);
  color = light(color);
  gl_FragColor = brightness*color;
  if(blending)
    gl_FragColor.a *= opacity;
  else
    gl_FragColor.a = 1.0;
}

