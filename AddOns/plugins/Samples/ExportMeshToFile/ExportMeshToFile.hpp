/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef EXPORT_MESH_TO_FILE_HPP
#define EXPORT_MESH_TO_FILE_HPP

#include <Process.hpp>

namespace lgx {
namespace process {
// Simple export to OBJ
class ExportMeshToFile : public MeshProcess {
public:
  ExportMeshToFile(const MeshProcess& process)
    : Process(process)
    , MeshProcess(process)
  {
  }

  // Standard call interface for all processes
  bool operator()(const ParmList& parms) override
  {
    if(!checkState().mesh(MESH_NON_EMPTY))
      return false;
    return (*this)(currentMesh(), parms[0].toString(), parms[1].toString());
  }

  // Initialize the process, used to pop-up a GUI to get the filename
  bool initialize(ParmList& parms, QWidget* parent) override;

  // Call interface with parameters used
  bool operator()(Mesh* mesh, const QString& fileName, const QString& fileType);

  // Plug-in folder
  QString folder() const override {
    return "System";
  }
  // Plug-in name
  QString name() const override {
    return "Export Mesh To File";
  }
  // Plug-in long description
  QString description() const override {
    return "Export mesh to a text file for import into other software";
  }
  // List of parameter names
  QStringList parmNames() const override
  {
    return QStringList() << "Filename"
                         << "Type";
  }
  // List of parameter long descriptions
  QStringList parmDescs() const override
  {
    return QStringList() << "File for mesh export"
                         << "File type";
  }
  // List of parameter default values
  ParmList parmDefaults() const override
  {
    return ParmList() << ""
                      << "obj";
  }
  // Make a pickbox for the file types available
  ParmChoiceMap parmChoice() const override
  {
    ParmChoiceMap map;
    map[1] = QStringList() << "obj"
                           << "ply";
    return map;
  }
  // Plug-in icon
  QIcon icon() const override {
    return QIcon(":/images/ExportMeshToFile.png");
  }
};
} // namespace process
} // namespace lgx
#endif
