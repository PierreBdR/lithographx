/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef LGX_CONFIG_HPP
#define LGX_CONFIG_HPP

// Sometimes, with MinGW, only the version with underscore are defined
#if defined(_WIN32) && !defined(WIN32)
#  define WIN32 _WIN32
#endif

#if defined(_WIN64) && !defined(WIN64)
#  define WIN64 _WIN64
#endif

#if defined(WIN64) && !defined(WIN32)
#    define WIN32 WIN64
#endif

#if defined(WIN32) || defined(WIN64)

#    ifdef lgxPack_EXPORTS
#        define LGX_EXPORT
#    elif defined(LithoGraphX_EXPORTS)
#        define LGX_EXPORT __declspec(dllexport)
#    else
#        define LGX_EXPORT __declspec(dllimport)
#    endif
#    ifdef lgxCore_EXPORTS
#        define LGXCORE_EXPORT __declspec(dllexport)
#    else
#        define LGXCORE_EXPORT __declspec(dllimport)
#    endif

#    ifdef _MSC_VER
       // Disable possible data loss during initializing variable
#      pragma warning (disable: 4305)
#      pragma warning (disable: 4267)
#      pragma warning (disable: 4244)
#    endif

#    define WINDOWS
#    ifndef _USE_MATH_DEFINES
#      define _USE_MATH_DEFINES
#    endif
#    include <windows.h>
#    undef max
#    undef min
#    ifdef _MSC_VER
#        define xor ^
#        define and &&
#        define or ||
#        define not !
#        define finite _finite
#        define isnan _isnan
#        define round _round
#    endif
#    include <math.h>
#    include <float.h>
#else
#    define LGX_EXPORT
#    define LGXCORE_EXPORT
#endif

#cmakedefine01 CORE_COMPILE
#cmakedefine01 PYTHON_COMPILE
#cmakedefine IS_PY3K
#define PYTHON_VERSION "${PYTHON_VERSION_MAJOR}.${PYTHON_VERSION_MINOR}"

#cmakedefine LithoGraphX_CHECK_UPDATES

#define LGX_APPNAME "LithoGraphX"
#define LGX_DOMAINNAME "lithographx.com"

#endif

