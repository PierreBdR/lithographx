/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "Library.hpp"

#include <Defer.hpp>

#include "Information.hpp"
#include <QFileInfo>

Library::Library(QString path)
  : _filename(path)
  , _handle(0)
{
}

#if defined(linux) || defined(__linux__)

#  include <QFile>

#  include <dlfcn.h>

bool Library::load()
{
  if(isLoaded()) {
    _errorMessage = QString("Library %1 is already loaded").arg(_filename);
    return false;
  }

  QByteArray ba = _filename.toLocal8Bit();
  _handle = dlopen(ba.data(), RTLD_NOW | RTLD_GLOBAL);
  if(!_handle) {
    _errorMessage = QString::fromLocal8Bit(dlerror());
    return false;
  } else {
    _errorMessage = "No Error";
    return true;
  }
}

bool Library::unload()
{
  if(not isLoaded()) {
    _errorMessage = QString("Library %1 isn't loaded.").arg(_filename);
    return false;
  }

  if(dlclose(_handle)) {
    char* err = dlerror();
    _errorMessage = QString::fromLocal8Bit(err);
    return false;
  } else {
    // Check it's really unloaded
    _handle = 0;
    _errorMessage = "No Error";
    return true;
  }
}

bool Library::isLibrary(QString path)
{
  return path.endsWith(".so", Qt::CaseInsensitive);
  if(path.endsWith(".so", Qt::CaseInsensitive)) {
    // Check magic number
    // First, ELF
    QFile f(path);
    if(!f.open(QIODevice::ReadOnly))
      return false;

    const char elf[4] = { '\177', 'E', 'L', 'F' };
    char buffer[4];
    f.read(buffer, 4);
    if(memcmp(elf, buffer, 4) != 0)
      return false;

    quint8 sys;
    // Check 32/64 bits
    if(!f.seek(4))
      return false;
    f.read((char*)&sys, 1);
#  ifdef __x86_64
    if(sys != 2)
      return false;
// err << "  64 bits" << endl;
#  else
    if(sys != 1)
      return false;
// err << "  32 bits" << endl;
#  endif

    quint16 typ;
    // Checked shared lib
    if(!f.seek(16))
      return false;
    f.read((char*)&typ, 2);
    if(typ != 3)
      return false;
    // err << "  shared lib" << endl;
    f.close();
    return true;
  }
  return false;
}

#elif defined(__APPLE__)

#  include <QFileInfo>
#  include <QProcess>
#  include <stdio.h>
#  include <mach-o/dyld.h>

bool Library::load()
{
  // Success or failure result value
  NSObjectFileImage img;   // Represents the bundle's object file
  /* Get an object file for the bundle. */
  QByteArray ba = _filename.toLocal8Bit();
  int rc = NSCreateObjectFileImageFromFile(ba.data(), &img);
  if(rc != NSObjectFileImageSuccess) {
    _errorMessage = QString("Could not load %1.").arg(_filename);
    return false;
  }
  /* Get a _handle for the bundle. */
  _handle = NSLinkModule(img, ba.data(), NSLINKMODULE_OPTION_RETURN_ON_ERROR);
  if(!_handle) {
    _errorMessage = QString("Could not load %1.").arg(_filename);
    return false;
  }
  return true;
}

bool Library::unload()
{
  if(!NSUnLinkModule((NSModule)_handle, NSUNLINKMODULE_OPTION_NONE)) {
    _errorMessage = QString("Could not unload %1.").arg(_filename);
    return false;
  }
  _handle = 0;
  return true;
}

bool Library::isLibrary(QString path)
{
  QFileInfo fi(path);
  if(fi.exists() and (path.endsWith(".so", Qt::CaseInsensitive) or path.endsWith(".dylib", Qt::CaseInsensitive)
                      or path.endsWith(".bundle", Qt::CaseInsensitive))) {
    QProcess filetype;
    filetype.start("file", QStringList() << path);
    if((filetype.state() != QProcess::NotRunning) and not filetype.waitForFinished(1000))
      return false;
    QString str = QString::fromLocal8Bit(filetype.readAllStandardOutput());
    QStringList lines = str.split("\n");
    // First, check Mach-O
    QString macho("Mach-O");
    if(lines.filter(macho).isEmpty())
      return false;
    //  err << "  Mach-O" << endl;
    // Then, test shared lib
    QString dylib("dynamically linked shared library");
    QString bundle("bundle");
    if(lines.filter(dylib).isEmpty() and lines.filter(bundle).isEmpty())
      return false;
//  err << "  DLL" << endl;
#  ifdef __x86_64
    QString arch("x86_64");
#  else
    QString arch("i386");
#  endif
    if(lines.filter(arch).isEmpty())
      return false;
    return true;
  }
  return false;
}

#elif defined(WIN32) || defined(WIN64)

#include <WinBase.h>

bool Library::load()
{
  QByteArray ba = _filename.toLocal8Bit();
  // Disable dialog box errors when loading a library
  DWORD previous;
  if(not SetThreadErrorMode(SEM_FAILCRITICALERRORS|SEM_NOOPENFILEERRORBOX|SEM_NOGPFAULTERRORBOX, &previous))
      previous = 0;
  auto defered = lgx::util::defer([previous]() { SetErrorMode(previous); });
  _handle = LoadLibraryEx(ba, NULL, LOAD_LIBRARY_SEARCH_DEFAULT_DIRS | LOAD_LIBRARY_SEARCH_DLL_LOAD_DIR);
  if(!_handle) {
    char msg[1000];
    FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, 0, GetLastError(), 0, msg, 1000, NULL);
    _errorMessage = QString::fromLocal8Bit(msg);
    return false;
  }
  return true;
}

bool Library::unload()
{
  if(!FreeLibrary(_handle)) {
    _errorMessage = "Error while closing DLL";
    return false;
  }
  _handle = 0;
  return true;
}

bool Library::isLibrary(QString path)
{
  return QFileInfo(path).exists() and path.endsWith(".dll", Qt::CaseInsensitive);
}

#else
#  error "LithoGraphX doesn't know how to load libraries on your system. Please implement Library.cpp"
#endif
