/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef ITKFILTER_HPP
#define ITKFILTER_HPP

#include <ITKConfig.hpp>
#include <ITKProcess.hpp>

#include <Stack.hpp>
#include <Store.hpp>
#include <Misc.hpp>

namespace lgx {
namespace process {
class itk_EXPORT ITKDiscreteGaussianImageFilter : public StackProcess {
public:
  ITKDiscreteGaussianImageFilter(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const ParmList& parms) override
  {
    if(!checkState().store(STORE_NON_EMPTY|STORE_NON_LABEL))
      return false;
    Stack* s = currentStack();
    Store* input = s->currentStore();
    Store* output = s->work();
    bool ok;
    float x = parms[0].toFloat(&ok);
    if(!ok)
        return setErrorMessage("X parameter must be a number");
    float y = parms[1].toFloat(&ok);
    if(!ok)
        return setErrorMessage("Y parameter must be a number");
    float z = parms[2].toFloat(&ok);
    if(!ok)
        return setErrorMessage("Z parameter must be a number");
    Point3f sigma(x, y, z);
    bool res = (*this)(input, output, sigma);
    if(res) {
      input->hide();
      output->show();
    }
    return res;
  }

  bool operator()(const Store* input, Store* output, const Point3f& sigma);

  QString name() const override {
    return "ITK Discrete Gaussian Image Filter";
  }
  QString folder() const override {
    return "ITK/Filters";
  }
  QString description() const override {
    return "Discrete Gaussian Blur. Simply perform the convolution explicitly.";
  }
  QStringList parmNames() const override {
    return QStringList() << QString("X Sigma (%1)").arg(UM)
                         << QString("Y Sigma (%1)").arg(UM)
                         << QString("Z Sigma (%1)").arg(UM);
  }
  QStringList parmDescs() const override {
    return QStringList() << QString("X Sigma (%1)").arg(UM)
                         << QString("Y Sigma (%1)").arg(UM)
                         << QString("Z Sigma (%1)").arg(UM);
  }
  ParmList parmDefaults() const override {
    return ParmList() << 1.0 << 1.0 << 1.0;
  }
  QIcon icon() const override {
    return QIcon(":/images/Blur.png");
  }
};

class itk_EXPORT ITKSmoothingRecursiveGaussianImageFilter : public StackProcess {
public:
  ITKSmoothingRecursiveGaussianImageFilter(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const ParmList& parms) override
  {
    if(!checkState().store(STORE_NON_EMPTY|STORE_NON_LABEL))
      return false;
    Stack* s = currentStack();
    Store* input = s->currentStore();
    Store* output = s->work();
    bool ok;
    float x = parms[0].toFloat(&ok);
    if(!ok)
        return setErrorMessage("X parameter must be a number");
    float y = parms[1].toFloat(&ok);
    if(!ok)
        return setErrorMessage("Y parameter must be a number");
    float z = parms[2].toFloat(&ok);
    if(!ok)
        return setErrorMessage("Z parameter must be a number");
    Point3f sigma(x, y, z);
    bool res = (*this)(input, output, sigma);
    if(res) {
      input->hide();
      output->show();
    }
    return res;
  }

  bool operator()(const Store* input, Store* output, const Point3f& sigma);

  QString name() const override {
    return "ITK Smoothing Recursive Gaussian Image Filter";
  }
  QString folder() const override {
    return "ITK/Filters";
  }
  QString description() const override {
    return "Smoothing Recursive Gaussian Blur";
  }
  QStringList parmNames() const override {
    return QStringList() << QString("X Sigma (%1)").arg(UM)
                         << QString("Y Sigma (%1)").arg(UM)
                         << QString("Z Sigma (%1)").arg(UM);
  }
  QStringList parmDescs() const override {
    return QStringList() << QString("X Sigma (%1)").arg(UM)
                         << QString("Y Sigma (%1)").arg(UM)
                         << QString("Z Sigma (%1)").arg(UM);
  }
  ParmList parmDefaults() const override {
    return ParmList() << 1.0 << 1.0 << 1.0;
  }
  QIcon icon() const override {
    return QIcon(":/images/Blur.png");
  }
};

class itk_EXPORT ITKCurvatureFlowImageFilter : public StackProcess {
public:
  ITKCurvatureFlowImageFilter(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const ParmList& parms) override
  {
    if(!checkState().store(STORE_NON_EMPTY|STORE_NON_LABEL))
      return false;
    Stack* s = currentStack();
    Store* input = s->currentStore();
    Store* output = s->work();
    bool res = (*this)(input, output, parms[0].toFloat(), parms[1].toInt());
    if(res) {
      input->hide();
      output->show();
    }
    return res;
  }

  bool operator()(const Store* input, Store* output, float timestep, int steps);

  QString name() const override {
    return "ITK Curvature Flow Image Filter";
  }
  QString folder() const override {
    return "ITK/Filters";
  }
  QString description() const override {
    return "Curvature Flow Edge Preserving Smoothing";
  }
  QStringList parmNames() const override
  {
    return QStringList() << "TimeStep"
                         << "Steps";
  }
  QStringList parmDescs() const override
  {
    return QStringList() << "TimeStep"
                         << "Steps";
  }
  ParmList parmDefaults() const override
  {
    return ParmList() << .0625
                      << 10;
  }
  QIcon icon() const override {
    return QIcon(":/images/Blur.png");
  }
};

class itk_EXPORT ITKCurvatureAnisotropicDiffusionImageFilter : public StackProcess {
public:
  ITKCurvatureAnisotropicDiffusionImageFilter(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const ParmList& parms) override
  {
    if(!checkState().store(STORE_NON_EMPTY|STORE_NON_LABEL))
      return false;
    Stack* s = currentStack();
    Store* input = s->currentStore();
    Store* output = s->work();
    bool res = (*this)(input, output, parms[0].toFloat(), parms[1].toInt(), parms[2].toFloat());
    if(res) {
      input->hide();
      output->show();
    }
    return res;
  }

  bool operator()(const Store* input, Store* output, float timestep, int iterations, float conductance);

  QString name() const override {
    return "ITK Curvature Anisotropic Diffusion Image Filter";
  }
  QString folder() const override {
    return "ITK/Filters";
  }
  QString description() const override {
    return "Curvature Anisotropic Diffusion";
  }
  QStringList parmNames() const override
  {
    return QStringList() << "TimeStep"
                         << "Iterations"
                         << "Conductance";
  }
  QStringList parmDescs() const override
  {
    return QStringList() << "TimeStep"
                         << "Iterations"
                         << "Conductance";
  }
  ParmList parmDefaults() const override
  {
    return ParmList() << 0.03
                      << 5
                      << 9.0;
  }
  QIcon icon() const override {
    return QIcon(":/images/Blur.png");
  }
};

class itk_EXPORT ITKGradientMagnitudeRecursiveGaussianImageFilter : public StackProcess {
public:
  ITKGradientMagnitudeRecursiveGaussianImageFilter(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const ParmList& parms) override
  {
    if(!checkState().store(STORE_NON_EMPTY|STORE_NON_LABEL))
      return false;
    Stack* s = currentStack();
    Store* input = s->currentStore();
    Store* output = s->work();
    bool res = (*this)(input, output, parms[0].toFloat());
    if(res) {
      input->hide();
      output->show();
    }
    return res;
  }

  bool operator()(const Store* input, Store* output, float sigma);

  QString name() const override {
    return "ITK Gradient Magnitude Recursive Gaussian Image Filter";
  }
  QString folder() const override {
    return "ITK/Filters";
  }
  QString description() const override {
    return "Gradient Magnitude Recursive Gaussian";
  }
  QStringList parmNames() const override {
    return QStringList() << QString("Sigma (%1)").arg(UM);
  }
  QStringList parmDescs() const override {
    return QStringList() << QString("Sigma (%1)").arg(UM);
  }
  ParmList parmDefaults() const override {
    return ParmList() << 0.5;
  }
  QIcon icon() const override {
    return QIcon(":/images/Blur.png");
  }
};

class itk_EXPORT ITKSigmoidImageFilter : public StackProcess {
public:
  ITKSigmoidImageFilter(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const ParmList& parms) override
  {
    if(!checkState().store(STORE_NON_EMPTY|STORE_NON_LABEL))
      return false;
    Stack* s = currentStack();
    Store* input = s->currentStore();
    Store* output = s->work();
    bool res = (*this)(input, output, parms[0].toFloat(), parms[1].toFloat());
    if(res) {
      input->hide();
      output->show();
    }
    return res;
  }

  bool operator()(const Store* input, Store* output, float alpha, float beta);

  QString name() const override {
    return "ITK Sigmoid Image Filter";
  }
  QString folder() const override {
    return "ITK/Filters";
  }
  QString description() const override {
    return "Sigmoid Filter";
  }
  QStringList parmNames() const override
  {
    return QStringList() << "Alpha"
                         << "Beta";
  }
  QStringList parmDescs() const override
  {
    return QStringList() << "Alpha"
                         << "Beta";
  }
  ParmList parmDefaults() const override
  {
    return ParmList() << 1000.0
                      << 2000.0;
  }
  QIcon icon() const override {
    return QIcon(":/images/Blur.png");
  }
};

class itk_EXPORT ITKPatchBasedDenoising : public StackProcess {
public:
  ITKPatchBasedDenoising(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const ParmList& parms) override
  {
    if(!checkState().store(STORE_NON_EMPTY|STORE_NON_LABEL))
      return false;
    Stack* s = currentStack();
    Store* input = s->currentStore();
    Store* output = s->work();
    Point3f bandwidth;
    bandwidth.x() = 1.f;
    bool auto_bandwidth = true;
    bool ok = true;
    float radius = parms[0].toFloat(&ok);
    if(not ok or radius <= 0)
      return setErrorMessage("Patch Radius parameter must be a positive number.");
    size_t nb_iter = parms[1].toUInt(&ok);
    if(not ok or nb_iter == 0)
      return setErrorMessage("The number of iterations must be a positive integer.");
    QString bandwidth_str = parms[3].toString();
    if(not bandwidth_str.isEmpty()) {
      if(bandwidth_str.endsWith("%")) {
        bandwidth = bandwidth_str.left(bandwidth.size()-1).toFloat(&ok);
        if(not ok)
          bandwidth.x() = -1;
      } else {
        auto_bandwidth = false;
        bandwidth = util::stringToPoint3f(bandwidth_str);
      }
      if(bandwidth.x() < 0 or std::isnan(bandwidth.x()))
        return setErrorMessage("Bandwidth parameter must be either a percentage (e.g. 100%), a positive number, "
                               "a 3D vector of positive numbers or empty");
    }
    float fraction = parms[1].toFloat(&ok);
    if(not ok or fraction <= 0 or fraction > 1)
      return setErrorMessage("Image Fraction for Bandwidth Estimation parameter accept a single number between 0 and 1");
    float fidelityWeight = parms[8].toFloat(&ok);
    if(not ok or fidelityWeight <= 0)
      return setErrorMessage("Noise Model Fidelity Weight must be a positive number");
    float smoothingWeight = parms[9].toFloat(&ok);
    if(not ok or smoothingWeight <= 0)
      return setErrorMessage("Smoothing Weight must be a positive number");
    float noise = parms[10].toFloat(&ok);
    if(not ok or noise < 0)
      return setErrorMessage("Noise Sigma parameter requires a positive number as argument.");
    bool res = (*this)(input, output, radius, nb_iter, bandwidth, auto_bandwidth,
                       parms[4].toBool(), parms[5].toBool(), parms[6].toBool(),
                       parms[7].toString(), fidelityWeight, smoothingWeight, noise);
    if(res) {
      input->hide();
      output->show();
    }
    return res;
  }

  bool operator()(const Store* input, Store* output, float radius, size_t nb_iteration, const Point3f& bandwidth,
                  bool auto_bandwidth, bool useSmoothDisc, bool useConditionalDerivs, bool useFastTensor,
                  const QString& noiseModel, float fidelityWeight, float smoothingWeight, float noise);

  QString name() const override {
    return "ITK Patch Based Denoising";
  }
  QString folder() const override {
    return "ITK/Filters";
  }
  QString description() const override {
    return "From the ITK documentation:\n"
           "This class implements a denoising filter that uses iterative non-local, "
           "or semi-local, weighted averaging of image patches for image denoising. "
           "The intensity at each pixel 'p' gets updated as a weighted average of "
           "intensities of a chosen subset of pixels from the image";
  }
  QStringList parmNames() const override
  {
    return QStringList() << QString("Patch Radius (%1)").arg(UM)
                         << "Number of Iterations"
                         << "Kernel Bandwidth"
                         << "Image Fraction"
                         << "Smooth Disc Patch Weights"
                         << "Conditional Derivatives"
                         << "Use Fast Tensor Computation"
                         << "Noise Model"
                         << "Noise Model Fidelity Weight"
                         << "Smoothing Weight"
                         << "Noise Sigma";
  }
  QStringList parmDescs() const override
  {
    return QStringList() << "Radius of the patch in voxel in the dimension of maximum voxel size. This should be an even number.\n"
                            "For example, if the radius is 4 and a voxel size if (0.5, 0.25, 1.) then the patch will be of size (2,1,4)"
                         << "Number of Iterations"
                         << "Bandwidth of the kernel using for the KDE. If "
                            "specified as a percentage, this becomes a multiplicative factor relative "
                            "to the automatically estimated bandwidth"
                         << "Portion of the image on which to estimate the kernel bandwidth"
                         << "Use smooth disc patch weights"
                         << "Compute conditional derivatives"
                         << "Change how tensor are computed"
                         << "Noise model to use. Must be one of 'NoModel', 'Gaussian', 'Rician' or 'Poisson'"
                         << "Set the weight on the fidelity term (penalizes deviations from the noisy data)."
                         << "Set the weight on the smoothing term when considering noise."
                         << "Amount of noise (used by the noise model)";
  }
  ParmList parmDefaults() const override
  {
    return ParmList() << 4
                      << 1
                      << "100%"
                      << 0.1
                      << true
                      << true
                      << true
                      << "NoModel"
                      << 1.
                      << 1.
                      << 0.05;
  }
  ParmChoiceMap parmChoice() const override
  {
    ParmChoiceMap map;
    map[4] = map[5] = map[6] = booleanChoice();
    map[7] = QStringList() << "NoModel" << "Gaussian" << "Rician" << "Poisson";
    return map;
  };
  QIcon icon() const override {
    return QIcon(":/images/Blur.png");
  }
};
}
}

#endif // ITKFILTER_HPP
