#ifndef PCAnalysis_HPP
#define PCAnalysis_HPP

#include <Process.hpp>
#include "Information.hpp"
#include <QString>
#include <QRegExp>
#include <QStringList>

namespace process
{

// Available in main MorphoGraphX
mgx_EXPORT bool meshFromTriangles(vvgraph& S, std::vector<vertex> &vertices, std::vector<Point3i> &triangles);

class PCAnalysis : public GlobalProcess
{
public:
    PCAnalysis(const GlobalProcess& process)
        : Process(process)
        , GlobalProcess(process)
    { }
    
    bool operator()(const QStringList& strings, const QList<float>& values)
    {
        if(!checkState().stack())
            return false;
        Stack* stk = currentStack();
        Store *store = stk->currentStore();
        QString fn = strings[0];
        QString correction = strings[1].trimmed();
        Point3f correcting_factor;
        if(correction == "Ellipsoid")
            correcting_factor = sqrt(5);
        else if(correction == "Cuboid")
            correcting_factor = sqrt(3);
        else if(correction == "Elliptical Cylinder")
            correcting_factor = Point3f(sqrt(3), sqrt(4), sqrt(4));
        else if(correction == "Maximum Span")
            correcting_factor = 0;
        else if(correction.endsWith('%'))
        {
            QString cor = correction.left(correction.size()-1);
            bool ok;
            float cr = cor.toFloat(&ok);
            if(ok)
            {
                correcting_factor = -cr/100.f;
            }
            else
            {
                setErrorMessage(QString("Error, percentage value is not valid: '%1'").arg(cor));
                return false;
            }
        }
        else
        {
            bool ok;
            float cr = correction.toFloat(&ok);
            if(ok)
                correcting_factor = cr;
            else
            {
                QStringList vs = correction.split(QRegExp("[ ,-;:-]"));
                if(vs.size() == 3)
                {
                    correcting_factor.x() = vs[0].toFloat(&ok);
                    if(!ok)
                    {
                        setErrorMessage(QString("Invalid x value for correction factor: '%1'.").arg(vs[0]));
                        return false;
                    }
                    correcting_factor.y() = vs[1].toFloat(&ok);
                    if(!ok)
                    {
                        setErrorMessage(QString("Invalid y value for correction factor: '%1'.").arg(vs[1]));
                        return false;
                    }
                    correcting_factor.z() = vs[2].toFloat(&ok);
                    if(!ok)
                    {
                        setErrorMessage(QString("Invalid z value for correction factor: '%1'.").arg(vs[2]));
                        return false;
                    }
                }
                else
                {
                    setErrorMessage(QString("Invalid correction string '%1', expected one of 'Ellipsoid', 'Cuboid', 'Elliptical Cylinder', Maximum Span', a percentage, a single value or three values"));
                    return false;
                }
            }
        }
        bool draw_result = false;
        bool res = operator()(stk, store, fn, (int)values[0], strings[2], correcting_factor, (int)values[1], draw_result);
        if(res and draw_result)
        {
            Mesh *m = mesh(stk->id());
            m->showSurface();
            m->showLabel();
        }
        return res;
    }
    
    /**
     * @brief operator ()
     * @param stack Stack to act on
     * @param store Store containing the data to process
     * @param filename File to save the result to
     * @param treshold Threshold for volume detection, used if the store is not labeled
     * @param shape Shape used to draw the cells, "No" or empty string for no drawing.
     * @param correcting_factor Vector giving the correction factor to apply for eigen values if the x coordinate is 0, then maximum span is used instead,
     *        and negative values (between 0 and 1) correspond to extracting the percentile of the span.
     * @param number of elements used to discretize the shape (if any)
     * @param draw_result Return whether the mesh has been replaced by cell shapes or not
     * @return
     */
    bool operator()(Stack* stack, Store* store, QString filename, int treshold, QString shape, Point3f correcting_factor, int slices, bool& draw_result);
    
    QString folder() const { return "Misc"; }
    QString name() const { return "PCAnalysis"; }
    QString description() const { return "Compute the principle components of the image. If the threshold is -1, then all the values are used, as is.\n"
                "If 'Draw Result' is set to true, the current mesh will be erased and replaced with shapes representing the cells fit.\n"
                "'Splan Correction' can be either a shape, a single value of a vector of 3 values, corresponding to the correction to apply for the eigen-values on all three directions."; }
    QStringList parameters() const { return QStringList() << "Output" << "Span Correction" << "Draw Result" << "Threshold" << "Shape Slices"; }
    QStringList default_strings() const { return QStringList() << "output.csv" << "Ellipsoid" << "No"; }
    StringChoiceMap string_choice() const {
        StringChoiceMap map;
        map[1] = QStringList() << "Ellipsoid" << "Cuboid" << "Elliptical Cylinder" << "Maximum Span";
        map[2] = QStringList() << "No" << "Cuboids" << "Cylinder";
        return map;
    }
    QList<float> default_values() const { return QList<float>() << 100 << 10; }
    QIcon icon() const
    {
        return QIcon(":/images/PCAnalysis.png");
    }
    
};
}

#endif // PCAnalysis_HPP


