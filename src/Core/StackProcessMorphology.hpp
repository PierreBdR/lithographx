/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef STACKPROCESSMORPHOLOGY_HPP
#define STACKPROCESSMORPHOLOGY_HPP

#include <Process.hpp>

#include <Misc.hpp>
#include <Stack.hpp>
#include <Store.hpp>

namespace lgx {
namespace process {
///\addtogroup StackProcess
///@{
/**
 * \class EdgeDetectProcess StackProcessMorphology.hpp <StackProcessMorphology.hpp>
 *
 * TODO: This class needs documenting!
 */
struct LGXCORE_EXPORT EdgeDetectProcess : public StackProcess {
public:
  EdgeDetectProcess(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const ParmList& parms) override
  {
    if(!checkState().store(STORE_NON_EMPTY | STORE_NON_LABEL))
      return false;
    Stack* stack = currentStack();
    Store* store = stack->currentStore();
    Store* work = stack->work();
    bool res = (*this)(store, work, parms[0].toFloat(), parms[1].toFloat(), parms[2].toFloat(), parms[3].toUInt());
    if(res) {
      store->hide();
      work->show();
    }
    return res;
  }

  bool operator()(Store* input, Store* output, float threshold, float multiplier, float factor, uint fillValue);

  QString folder() const override {
    return "Morphology";
  }
  QString name() const override {
    return "Edge Detect";
  }
  QString description() const override
  {
    return "Do a multipass edge detection in Z direction.\n"
           "Stack is turned into a mask (0 or fill value)";
  }
  QStringList parmNames() const override
  {
    return QStringList() << "Threshold"
                         << "Multiplier"
                         << "Adapt Factor"
                         << "Fill Value";
  }
  QStringList parmDescs() const override
  {
    return QStringList() << "Values of signal that should not belong to surface."
                         << "Multiplicative factor for the threshold."
                         << "Adaptative factor for threshold."
                         << "Value to fill the mask with.";
  }
  ParmList parmDefaults() const override
  {
    return ParmList() << 10000.0
                      << 2.0
                      << 0.3
                      << 30000;
  }
  QIcon icon() const override {
    return QIcon(":/images/EdgeDetect.png");
  }
};

/**
 * \class DilateStack StackProcessMorphology.hpp <StackProcessMorphology.hpp>
 *
 * Dilate the gray-level stack using a cubic kernel.
 *
 * During dilation, a new image is created in which each voxel is replaced by
 * the maximum value within a given neighborhood around the voxel. In
 * LithoGraphX, this neighborhood is defined as a cube, centered on the
 * voxel, and of size given by the user. The radius is then half the length
 * of the corresponding edge on the cube.
 */
class LGXCORE_EXPORT DilateStack : public StackProcess {
public:
  DilateStack(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const ParmList& parms) override
  {
    if(!checkState().store(STORE_NON_EMPTY))
      return false;
    Stack* s = currentStack();
    Store* input = s->currentStore();
    Store* output = s->work();
    bool res = (*this)(s, input, output, parms[0].toUInt(), parms[1].toUInt(), parms[2].toUInt(),
                       parmToBool(parms[3]));
    if(res) {
      input->hide();
      output->show();
    }
    return res;
  }
  bool operator()(Stack* stack, const Store* input, Store* output, uint xradius, uint yradius, uint zradius,
                  bool auto_resize = true);

  QString folder() const override {
    return "Morphology";
  }
  QString name() const override {
    return "Dilate";
  }
  QString description() const override {
    return "Morphological dilation (max filter) on stack";
  }
  QStringList parmNames() const override
  {
    return QStringList() << "X Radius"
                         << "Y Radius"
                         << "Z Radius"
                         << "Auto-Resize";
  }
  QStringList parmDescs() const override
  {
    return QStringList() << "X Radius"
                         << "Y Radius"
                         << "Z Radius"
                         << "Auto-Resize";
  }
  ParmList parmDefaults() const override
  {
    return ParmList() << 1
                      << 1
                      << 1
                      << false;
  }
  ParmChoiceMap parmChoice() const override
  {
    ParmChoiceMap map;
    map[3] = booleanChoice();
    return map;
  }
  QIcon icon() const override {
    return QIcon(":/images/Dilate.png");
  }
};

/**
 * \class ErodeStack StackProcessMorphology.hpp <StackProcessMorphology.hpp>
 *
 * Erode the gray-level stack using a cubic kernel.
 *
 * During dilation, a new image is created in which each voxel is replaced by
 * the minimum value within a given neighborhood around the voxel. In
 * LithoGraphX, this neighborhood is defined as a cube, centered on the
 * voxel, and of size given by the user. The radius is then half the length
 * of the corresponding edge on the cube.
 */
class LGXCORE_EXPORT ErodeStack : public StackProcess {
public:
  ErodeStack(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const ParmList& parms) override
  {
    if(!checkState().store(STORE_NON_EMPTY))
      return false;
    Stack* s = currentStack();
    Store* input = s->currentStore();
    Store* output = s->work();
    bool res = (*this)(s, input, output, parms[0].toUInt(), parms[1].toUInt(), parms[2].toUInt(),
                       parmToBool(parms[3]));
    if(res) {
      input->hide();
      output->show();
    }
    return res;
  }
  bool operator()(Stack* stack, const Store* input, Store* output, uint xradius, uint yradius, uint zradius,
                  bool auto_resize = true);

  QString folder() const override {
    return "Morphology";
  }
  QString name() const override {
    return "Erode";
  }
  QString description() const override {
    return "Morphological erosion on stack";
  }
  QStringList parmNames() const override
  {
    return QStringList() << "X Radius"
                         << "Y Radius"
                         << "Z Radius"
                         << "Auto-Resize";
  }
  QStringList parmDescs() const override
  {
    return QStringList() << "X Radius"
                         << "Y Radius"
                         << "Z Radius"
                         << "Auto-Resize";
  }
  ParmList parmDefaults() const override
  {
    return ParmList() << 1
                      << 1
                      << 1
                      << false;
  }
  ParmChoiceMap parmChoice() const override
  {
    ParmChoiceMap map;
    map[3] = booleanChoice();
    return map;
  }
  QIcon icon() const override {
    return QIcon(":/images/Erode.png");
  }
};

/**
 * \class ClosingStack StackProcessMorphology.hpp <StackProcessMorphology.hpp>
 *
 * Close the gray-level stack using a cubic kernel.
 *
 * A morphological closing is a dilation, followed by an erosion using the same kernel.
 * The result is that any gap smaller than the kernel will be filled. This
 * will work both for holes and for gaps between separated objects.
 */
class LGXCORE_EXPORT ClosingStack : public StackProcess {
public:
  ClosingStack(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const ParmList& parms) override
  {
    if(!checkState().store(STORE_NON_EMPTY))
      return false;
    Stack* s = currentStack();
    Store* input = s->currentStore();
    Store* output = s->work();
    bool res = (*this)(s, input, output, parms[0].toUInt(), parms[1].toUInt(), parms[2].toUInt());
    if(res) {
      input->hide();
      output->show();
    }
    return res;
  }
  bool operator()(Stack* stack, const Store* input, Store* output, uint xradius, uint yradius, uint zradius);

  QString folder() const override {
    return "Morphology";
  }
  QString name() const override {
    return "Closing";
  }
  QString description() const override {
    return "Morphological closure (i.e. dilatation followed erosion) on stack";
  }
  QStringList parmNames() const override
  {
    return QStringList() << "X Radius"
                         << "Y Radius"
                         << "Z Radius";
  }
  QStringList parmDescs() const override
  {
    return QStringList() << "X Radius"
                         << "Y Radius"
                         << "Z Radius";
  }
  ParmList parmDefaults() const override
  {
    return ParmList() << 1
                      << 1
                      << 1;
  }
  QIcon icon() const override {
    return QIcon(":/images/Closing.png");
  }
};

/**
 * \class OpeningStack StackProcessMorphology.hpp <StackProcessMorphology.hpp>
 *
 * Open the gray-level stack using a cubic kernel.
 *
 * A morphological opening is an erosion, followed by a dilation using the same kernel.
 * The result is that any part on an object narrower than the kernel will be
 * erased. As a consequence, any object smaller than the kernel will be
 * completly deleted.
 */
class LGXCORE_EXPORT OpeningStack : public StackProcess {
public:
  OpeningStack(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const ParmList& parms) override
  {
    if(!checkState().store(STORE_NON_EMPTY))
      return false;
    Stack* s = currentStack();
    Store* input = s->currentStore();
    Store* output = s->work();
    bool res = (*this)(s, input, output, parms[0].toUInt(), parms[1].toUInt(), parms[2].toUInt());
    if(res) {
      input->hide();
      output->show();
    }
    return res;
  }
  bool operator()(Stack* stack, const Store* input, Store* output, uint xradius, uint yradius, uint zradius);

  QString folder() const override {
    return "Morphology";
  }
  QString name() const override {
    return "Opening";
  }
  QString description() const override {
    return "Morphological opening (i.e. erosion followed dilatation) on stack";
  }
  QStringList parmNames() const override
  {
    return QStringList() << "X Radius"
                         << "Y Radius"
                         << "Z Radius";
  }
  QStringList parmDescs() const override
  {
    return QStringList() << "X Radius"
                         << "Y Radius"
                         << "Z Radius";
  }
  ParmList parmDefaults() const override
  {
    return ParmList() << 5
                      << 5
                      << 5;
  }
  QIcon icon() const override {
    return QIcon(":/images/Opening.png");
  }
};

class LGXCORE_EXPORT FillHolesProcess : public StackProcess {
public:
  FillHolesProcess(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const ParmList& parms) override
  {
    if(!checkState().store(STORE_NON_EMPTY | STORE_NON_LABEL))
      return false;
    Stack* s = currentStack();
    Store* input = s->currentStore();
    Store* output = s->work();
    bool res = (*this)(input, output, parms[0].toUInt(), parms[1].toUInt(), parms[2].toUInt(), parms[3].toUInt());
    if(res) {
      input->hide();
      output->show();
    }
    return res;
  }
  bool operator()(const Store* input, Store* output, uint xradius, uint yradius, uint threshold, uint fillValue);

  QString folder() const override {
    return "Morphology";
  }
  QString name() const override {
    return "Fill Holes";
  }
  QString description() const override
  {
    return "Fill holes in stack.\n"
           "Use after Edge Detect.";
  }
  QStringList parmNames() const override
  {
    return QStringList() << "X Radius"
                         << "Y Radius"
                         << "Threshold"
                         << "Fill Value";
  }
  QStringList parmDescs() const override
  {
    return QStringList() << "X Radius of hole"
                         << "Y Radius of hole"
                         << "Minimal signal value to fill the hole."
                         << "Filling value. Usually same as Edge Detect.";
  }
  ParmList parmDefaults() const override
  {
    return ParmList() << 10
                      << 10
                      << 10000
                      << 30000;
  }
  QIcon icon() const override {
    return QIcon(":/images/FillHoles.png");
  }
};

/**
 * \class OpenStackLabel StackProcessMorphology.hpp <StackProcessMorphology.hpp>
 *
 * Open a labeled stack.
 *
 * This process perform a binary opening on each label, independently. As the
 * result of an opening is always within the original object, this is a
 * well-defined operation in the case of multiple label.
 */
class LGXCORE_EXPORT OpenStackLabel : public StackProcess {
public:
  OpenStackLabel(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const ParmList& parms) override
  {
    if(!checkState().store(STORE_NON_EMPTY | STORE_LABEL))
      return false;
    Stack* s = currentStack();
    Store* input = s->currentStore();
    Store* output = s->work();
    bool res = (*this)(s, input, output, parms[0].toUInt(), parms[1].toUInt(), parms[2].toUInt());
    if(res) {
      input->hide();
      output->show();
    }
    return res;
  }
  bool operator()(Stack* stack, const Store* input, Store* output, uint xradius, uint yradius, uint zradius);

  QString folder() const override {
    return "Morphology";
  }
  QString name() const override {
    return "Open by label";
  }
  QString description() const override {
    return "Morphological opening on a labeled stack";
  }
  QStringList parmNames() const override
  {
    return QStringList() << "X Radius"
                         << "Y Radius"
                         << "Z Radius";
  }
  QStringList parmDescs() const override
  {
    return QStringList() << "X Radius"
                         << "Y Radius"
                         << "Z Radius";
  }
  ParmList parmDefaults() const override
  {
    return ParmList() << 1
                      << 1
                      << 1;
  }
  QIcon icon() const override {
    return QIcon(":/images/Opening.png");
  }
};

/**
 * \class CloseStackLabel StackProcessMorphology.hpp <StackProcessMorphology.hpp>
 *
 * Close a labeled stack.
 *
 * This process perform a binary closing on each label, independently. As the
 * result of a closing is almost always larger than the original object, the
 * result will depend on the order on which the closing is performed. As
 * such, the result is not well-defined if the labels are too close to each
 * other.
 */
class LGXCORE_EXPORT CloseStackLabel : public StackProcess {
public:
  CloseStackLabel(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const ParmList& parms) override
  {
    if(!checkState().store(STORE_NON_EMPTY | STORE_LABEL))
      return false;
    Stack* s = currentStack();
    Store* input = s->currentStore();
    Store* output = s->work();
    bool res = (*this)(s, input, output, parms[0].toUInt(), parms[1].toUInt(), parms[2].toUInt());
    if(res) {
      input->hide();
      output->show();
    }
    return res;
  }
  bool operator()(Stack* stack, const Store* input, Store* output, uint xradius, uint yradius, uint zradius);

  QString folder() const override {
    return "Morphology";
  }
  QString name() const override {
    return "Close by label";
  }
  QString description() const override {
    return "Morphological closure on a labeled stack";
  }
  QStringList parmNames() const override
  {
    return QStringList() << "X Radius"
                         << "Y Radius"
                         << "Z Radius";
  }
  QStringList parmDescs() const override
  {
    return QStringList() << "X Radius"
                         << "Y Radius"
                         << "Z Radius";
  }
  ParmList parmDefaults() const override
  {
    return ParmList() << 1
                      << 1
                      << 1;
  }
  QIcon icon() const override {
    return QIcon(":/images/Closing.png");
  }
};

/**
 * \class ApplyMaskToStack StackProcessMorphology.hpp <StackProcessMorphology.hpp>
 *
 * Apply a mask to a grey-level image. The image must be in the main store,
 * while the mask must be in the work store.
 *
 * The operations are:
 *
 * 1. Normal. Keep any value for which the mask is greater than the threshold. Any
 * other value is set to 0.
 * 2. Invert. Keep any value for which the mask is less than or equal to the
 * threshold. Any other value is set to 0.
 * 3. Combine. If the value in the mask is less than or equal to the
 * threshold, the value is replaced by the one of the image. Otherwise, the
 * value is the one in the mask.
 */
class LGXCORE_EXPORT ApplyMaskToStack : public StackProcess {
public:
  ApplyMaskToStack(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const ParmList& parms) override
  {
    if(!checkState().store(STORE_NON_EMPTY))
      return false;
    Stack* stack = currentStack();
    Store* input = stack->main();
    Store* output = stack->work();
    bool res = (*this)(stack, input, output, parms[0].toString(), parms[1].toUInt());
    if(res) {
      input->hide();
      output->show();
    }
    return res;
  }
  bool operator()(Stack* stack, const Store* input, Store* output, const QString& mode, uint threshold);

  QString folder() const override {
    return "Morphology";
  }
  QString name() const override {
    return "Apply Mask to Stack";
  }
  QString description() const override {
    return "Apply the work mask to the main replacing work";
  }
  QStringList parmNames() const override
  {
    return QStringList() << "Mode"
                         << "Threshold";
  }
  QStringList parmDescs() const override
  {
    return QStringList() << "Mode"
                         << "Threshold";
  }
  ParmList parmDefaults() const override
  {
    return ParmList() << "Normal"
                      << 0;
  }
  ParmChoiceMap parmChoice() const override
  {
    ParmChoiceMap map;
    map[0] = QStringList() << "Normal"
                           << "Invert"
                           << "Combine";
    return map;
  }
  QIcon icon() const override {
    return QIcon(":/images/Mask.png");
  }
};

/**
 * \class ApplyMaskLabels StackProcessMorphology.hpp <StackProcessMorphology.hpp>
 *
 * Apply a mask to labels. The labels must be in the main store, while the
 * mask must be in the work store.
 *
 * Labels are kept if they cover at least a voxel for which the mask is
 * greater than the threshold. Otherwise, they are erased. If the inverted
 * flag is set, labels are kept if they cover no voxel for which the mask is
 * greater than the threshold.
 */
class LGXCORE_EXPORT ApplyMaskLabels : public StackProcess {
public:
  ApplyMaskLabels(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const ParmList& parms) override
  {
    if(!checkState().store(STORE_NON_EMPTY))
      return false;
    Stack* stack = currentStack();
    Store* input = stack->main();
    Store* output = stack->work();
    bool res = (*this)(stack, input, output, parms[0].toUInt(), parmToBool(parms[1]));
    if(res) {
      input->hide();
      output->show();
      output->setLabels(true);
    }
    return res;
  }
  bool operator()(Stack* stack, const Store* input, Store* output, bool invert, uint threshold);

  QString folder() const override {
    return "Morphology";
  }
  QString name() const override {
    return "Apply Mask to Labels";
  }
  QString description() const override {
    return "Apply mask in work stack to labels in main stack, replacing work";
  }
  QStringList parmNames() const override
  {
    return QStringList() << "Invert"
                         << "Threshold";
  }
  QStringList parmDescs() const override
  {
    return QStringList() << "Invert"
                         << "Threshold";
  }
  ParmList parmDefaults() const override
  {
    return ParmList() << false
                      << 1;
  }
  ParmChoiceMap parmChoice() const override
  {
    ParmChoiceMap map;
    map[0] = booleanChoice();
    return map;
  }
  QIcon icon() const override {
    return QIcon(":/images/MaskLabels.png");
  }
};

/**
 * \class WhiteTopHatFilter StackProcessMorphology.hpp <StackProcessMorphology.hpp>
 *
 * Compute the difference between the image and the opening and the image.
 */
class LGXCORE_EXPORT WhiteTopHatFilter : public OpeningStack {
public:
  WhiteTopHatFilter(const StackProcess& process)
    : Process(process)
    , OpeningStack(process)
  {
  }

  bool operator()(const ParmList& parms) override;

  QString folder() const override {
    return "Morphology";
  }
  QString name() const override {
    return "White Top Hat";
  }
  QString description() const override {
    return "White top-hat filter (e.g. difference between the image and its opening)";
  }
  QIcon icon() const override {
    return QIcon(":/images/WhiteTopHat.png");
  }
};


/**
 * \class BlackTopHatFilter StackProcessMorphology.hpp <StackProcessMorphology.hpp>
 *
 * Compute the difference between the closing of the image and the image itself.
 */
class LGXCORE_EXPORT BlackTopHatFilter : public ClosingStack {
public:
  BlackTopHatFilter(const StackProcess& process)
    : Process(process)
    , ClosingStack(process)
  {
  }

  bool operator()(const ParmList& parms) override;

  QString folder() const override {
    return "Morphology";
  }
  QString name() const override {
    return "Black Top Hat";
  }
  QString description() const override {
    return "Black top-hat filter (e.g. difference between the closing of an image and itself)";
  }
  QIcon icon() const override {
    return QIcon(":/images/BlackTopHat.png");
  }
};


///@}
} // namespace process
} // namespace lgx

#endif
