/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef TRANSFERFUNCTIONDLG_HPP
#define TRANSFERFUNCTIONDLG_HPP

#include <LGXConfig.hpp>

#include <QStringList>
#include <QList>

#include <ui_TransferFunctionDlg.h>

class QAbstractButton;

namespace lgx {
namespace gui {
class LGX_EXPORT TransferFunctionDlg : public QDialog {
  Q_OBJECT
public:
  TransferFunctionDlg(QWidget* parent = 0, Qt::WindowFlags f = 0);
  virtual ~TransferFunctionDlg();

  const TransferFunction& transferFunction() const;
  static bool nameFunction(const TransferFunction& fct, const QString& name);
  static TransferFunction transferFunction(const QString& name);

public slots:
  void loadSettings(bool changeFunction = true);
  void saveSettings();
  void setTransferFunction(const TransferFunction& fct);
  void setDefaultTransferFunction(const TransferFunction& fct);
  void setStickers(const std::vector<double>& pos);
  void changeHistogram(const std::vector<uint64_t>& h);
  void changeBounds(const std::pair<double, double>& bounds);
  void changeRange(double low, double high);
  void apply();
  void reset();
  void accept();
  void reject();
  int exec();
  void autoAdjust(double p = -1);

signals:
  void changedTransferFunction(const TransferFunction& fct);
  void appliedTransferFunction(const TransferFunction& fct);

protected slots:
  void changeTransferFunction(const QString& name);
  void changeTransferFunction(const TransferFunction& fct);
  void on_useChecks_toggled(bool on);
  void on_useWhite_toggled(bool on);
  void on_useBlack_toggled(bool on);
  void on_useRGB_toggled(bool on);
  void on_useHSV_toggled(bool on);
  void on_useCyclicHSV_toggled(bool on);
  void on_selectSelectionColor_clicked();
  void on_functionList_currentIndexChanged(const QString& name);
  void on_exportFunction_clicked();
  void on_importFunction_clicked();
  void on_saveFunction_clicked();
  void on_renameFunction_clicked();
  void on_deleteFunction_clicked();
  void resetFunctionList();
  void on_buttonBox_clicked(QAbstractButton* btn);
  void on_adjustRange_valueChanged(double value);

protected:
  QColor getColor(QWidget* w);
  void setColor(QWidget* w, const QColor& col);
  bool changeColor(QWidget* w);

  TransferFunction current, default_fct;
  Ui::TransferFunctionDlg ui;
  QStringList fct_names;
  QList<TransferFunction> fcts;
};
} // namespace gui
} // namespace lgx
#endif // TRANSFERFUNCTIONDLG_HPP
