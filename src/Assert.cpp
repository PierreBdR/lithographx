/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "Assert.hpp"

#include <QString>
#include <QCoreApplication>
#include <iostream>
#include <stdlib.h>
#include "Information.hpp"

namespace lgx {
namespace util {
void __assert_fail(const QString& assertion, const char* file, unsigned int line, const char* function)
{
  QString app_name = QCoreApplication::applicationName();
  QString txt = QString("%1: %2:%3: %4: Assertion `%5' failed.")
    .arg(app_name)
    .arg(file)
    .arg(QString::number(line))
    .arg(function)
    .arg(assertion);
  Information::out << txt << endl;
  abort();
}
} // namespace util
} // namespace lgx
