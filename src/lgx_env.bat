@echo off
set LGX_HOME=%~dp0..
rem First, finding the right Python
for /f "tokens=*" %%F in ('"%LGX_HOME%\bin\FindPython.exe"') do set LGX_PYTHON=%%F
if ["%LGX_PYTHON%"] == [""] (
                         echo ###### PYTHON ERROR #######
                         echo Error, cannot find installation path of Python.
                         echo Ensure python.exe is in your PATH, or set the LGX_PYTHON environment variable to the path containing python.exe.
                         pause
                         exit 1
                        )

rem Remove trailing backslash from LGX_PYTHON, if any
if [%LGX_PYTHON:~-1%]==[\] SET LGX_PYTHON=%LGX_PYTHON:~,-1%
set LGX_PYTHON
if exist "%LGX_PYTHON%\python.exe" (
    set PYTHONHOME=%LGX_PYTHON%
    set PYTHONPATH=%LGX_HOME%\python-packages;%PYTHONPATH%
) else set LGX_PYTHON=
rem Setting QT's variables
set QT_QPA_PLATFORM_PLUGIN_PATH=%LGX_HOME%\bin\plugins\platforms
rem Setting Builder variables, if any
for /f "tokens=*" %%F in ('"%LGX_HOME%\bin\LithoGraphX.exe" --builder') do set LGX_BUILDER=%%F
if [%LGX_BUILDER_CMD%] NEQ [] call %LGX_BUILDER_CMD%
set CMAKE_PREFIX_PATH=%LGX_HOME%\lib\cmake;%CMAKE_PREFIX_PATH%
echo Set LithoGraphX environment variables
