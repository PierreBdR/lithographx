/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef ITKSINK_HPP
#define ITKSINK_HPP

#include <ITKConfig.hpp>
#include <ITKProcess.hpp>

#include <Stack.hpp>
#include <Store.hpp>

namespace lgx {
namespace process {
class itk_EXPORT ITKVTKWriter : public StackProcess {
public:
  ITKVTKWriter(const StackProcess& process)
    : Process(process)
    , StackProcess(process)
  {
  }

  bool operator()(const ParmList& parms) override
  {
    if(!checkState().store(STORE_NON_EMPTY))
      return false;
    Stack* s = currentStack();
    Store* input = s->currentStore();
    bool res = (*this)(input, parms[0].toString());
    return res;
  }

  bool operator()(const Store* input, QString filename);

  QString name() const override {
    return "ITK VTK Writer";
  }
  QString folder() const override {
    return "ITK/System";
  }
  QString description() const override {
    return "Write a VTK Image File";
  }
  QStringList parmNames() const override {
    return QStringList() << "Filename";
  }
  ParmList parmDefaults() const override {
    return ParmList() << "";
  }
  QIcon icon() const override {
    return QIcon(":/images/save.png");
  }
};
}
}

#endif // ITKSINK_HPP
