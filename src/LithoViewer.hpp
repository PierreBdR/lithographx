/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef LITHOVIEWER_HPP
#define LITHOVIEWER_HPP

#include <LGXConfig.hpp>
#include <GL.hpp>

#include <ClipRegion.hpp>
#include <CutSurf.hpp>
#include <EdgeData.hpp>
#include <ImageData.hpp>
#include <LGXViewer/qglviewer.h>
#include <Shader.hpp>
#include <VertexData.hpp>

#include <QMutex>
#include <QtGui>

class QDomElement;
class QShowEvent;
class LithoGraphX;
class LGXCamera;
class QWheelEvent;

class LGXKeyFrameInterpolator : public qglviewer::KeyFrameInterpolator {
public:
  LGXKeyFrameInterpolator(qglviewer::Frame* fr = NULL);
  virtual void interpolateAtTime(float time);

  std::vector<float> zooms;
};

class LGXCameraFrame : public qglviewer::ManipulatedCameraFrame {
public:
  LGXCameraFrame();

  void wheelEvent(QWheelEvent* const event, qglviewer::Camera* const camera);

  float zoom() const {
    return _zoom;
  }
  void setZoom(float z);

  void getMatrix(GLdouble m[4][4]) const override;
  void getMatrix(GLdouble m[16]) const override;
  const GLdouble* worldMatrix() const override;
  void setFromMatrix(const GLdouble m[4][4]) override;
  void setFromMatrix(const GLdouble m[16]) override;

protected:
  float _zoom = 0.0f;
};

class LGXCamera : public qglviewer::Camera {
public:
  LGXCamera();

  void getOrthoWidthHeight(GLdouble& halfWidth, GLdouble& halfHeight) const override;

  void resetZoom();

  void fitSphere(const qglviewer::Vec& center, float radius) override;

  virtual void addKeyFrameToPath(int i) override;

  virtual void playPath(int i) override;

  virtual void deletePath(int i) override;

  virtual void resetPath(int i) override;

  virtual void drawAllPaths() override;

  float zoom() const {
    return frame()->zoom();
  }
  void setZoom(float z) {
    frame()->setZoom(z);
  }

  LGXCameraFrame* frame() {
    return static_cast<LGXCameraFrame*>(qglviewer::Camera::frame());
  }
  const LGXCameraFrame* frame() const {
    return static_cast<const LGXCameraFrame*>(qglviewer::Camera::frame());
  }
  /**
   * Set the frame of the camera.
   *
   * \param mcf Frame to set. If should be a LGXCameraFrame. If not, a new
   * LGXCameraFrame will be created and this frame will be put as reference.
   */
  void setFrame(qglviewer::ManipulatedCameraFrame* const mcf) override;
};

class LithoViewer : public QGLViewer {
  friend class DebugDlg;
  friend class LithoGraphX;

  Q_OBJECT

  Q_PROPERTY(int maxNbPeels MEMBER _maxNbPeels NOTIFY changedNbPeels);
  Q_PROPERTY(float specular MEMBER _specular NOTIFY changedSpecular);
  Q_PROPERTY(float shininess MEMBER _shininess NOTIFY changedShininess);
  Q_PROPERTY(float unsharpening MEMBER _unsharpStrength NOTIFY changedUnsharpening);
  Q_PROPERTY(float globalContrast MEMBER _globalContrast NOTIFY changedGlobalContrast);
  Q_PROPERTY(float globalBrightness MEMBER _globalBrightness NOTIFY changedGlobalBrightness);
  Q_PROPERTY(float screenSampling MEMBER _screenSampling NOTIFY changedScreenSampling);
  Q_PROPERTY(uint slices MEMBER _slices NOTIFY changedSlices);

public:
  // Confocal stack objects
  QPointer<lgx::ImgData> _stack1, _stack2;
  // Clipping planes
  lgx::ClipRegion _clip1, _clip2, _clip3;
  QPointer<lgx::Clip> _c1, _c2, _c3;
  // Cutting surface
  QPointer<lgx::CutSurf> _cutSurf;

private:
  enum FrameIdentity {
    FI_BACKGROUND,
    FI_FULL_IMG1,
    FI_FULL_IMG2,
    FI_CUR_PEEL,
    FI_VOLUME1,
    FI_VOLUME2,
    FI_OCCLUSION,
    NB_FRAMES
  };

  bool _quitting;
  bool _needShowScene;
  bool _drawCellMap;
  bool _drawClipBox;
  lgx::Point3f _oldVPos;
  QPoint _oldPos;
  QRect _selectRect;
  bool _pixelEditCursor;
  float _pixelRadius;
  QPoint _mousePos;
  lgx::Shader raycasting_shader1, raycasting_shader2;
  lgx::Shader stack_surface_shader1, stack_surface_shader2;
  lgx::Shader colormap2_shader, index2_shader;
  lgx::Shader final_combine_shader, combine_shader, render_depth_shader;
  lgx::Shader texture_surf_shader, index_surf_shader;
  lgx::Shader volume_surf_shader1, volume_surf_shader2;
  lgx::Shader post_process_shader;
  lgx::Shader occlusion_shader;

  GLuint baseFboId, fboId, fboCopyId;
  GLuint depthTexId[NB_FRAMES];
  GLuint colorTexId[NB_FRAMES];
  GLuint colorBuffer;
  GLuint depthBuffer;
  std::vector<GLfloat> depthTexture;
  int _drawWidth, _drawHeight;
  int _prevWidth, _prevHeight;

  enum SelectMode { NONE, MOVE, SELRECT };
  SelectMode _selectMode;

  bool _shiftPressed;
  bool _altPressed;
  bool _controlPressed;
  bool _leftButton;
  bool _editionActive;

  float _flySpeed;
  int _selectedLabel;
  float _screenSampling;
  uint _slices;
  bool _fast_draw;
  int _texWidth, _texHeight;
  int _showSlice;
  int _sliceType;

  int _maxNbPeels;
  float _specular;
  float _shininess;
  float _globalBrightness;
  float _globalContrast;
  float _unsharpStrength;
  float _spinning;
  LGXCamera* _camera;

  LithoGraphX* mainWindow();

  void setSceneBoundingBox(const lgx::Point3f& bbox);
  float getSceneRadius() const {
    return sceneRadius;
  }

//private:
  float sceneRadius;

  QString fullSnapshotFileName();
  void initObject(QWidget* parent);
  bool initialized;
  qglviewer::Camera* lastInitCamera;

  void initCamera();

public:
  LithoViewer(QWidget* parent);
  LithoViewer(QGLContext* context, QWidget* parent);
  LithoViewer(const QGLFormat& format, QWidget* parent);
  virtual ~LithoViewer();

  static void initFormat();

  virtual QDomElement domElement(const QString& name, QDomDocument& document) const override;

  void drawSelectRect();
  void drawPixelCursor();
  void checkPixelCursor(bool altPressed);
  void drawColorBar();
  void drawScaleBar();
  void setLighting(int stackid);
  void readParms(lgx::util::Parms& parms, QString section);
  void writeParms(QTextStream& pout, QString section);
  lgx::Point3f pointUnderPixel(const QPoint& pos, bool& found);
  void startScreenCoordinatesSystem(bool upward = false) const override;
  bool setRevolveAroundPointFromPixel(const QPoint& px) override;
  void setDefaultTex16Bits(bool on);

protected:
  void stopEdition();
  void preDraw() override;
  void draw() override;
  void draw(QPaintDevice* device);
  void fastDraw() override;
  virtual void postDraw() override;
  virtual void init() override;
  void resizeGL(int width, int height) override;
  bool updateFBOTex(int width, int height, bool fast_draw = false);
  void drawColorTexture(int i, lgx::Shader* shader = nullptr, bool draw_depth = false);
  void drawColorTexture(int i, bool draw_depth) {
    drawColorTexture(i, nullptr, draw_depth);
  }
  void alternatePeels(int& curPeelId, int& prevPeelId, int fullImgId);
  void combinePeels(int& fullImgId, int curPeelId, int volume1, int volume2);
  void setupCopyFB(GLuint depth, GLint color);
  void setupFramebuffer(GLuint depth, GLuint color, GLbitfield clear = GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  void resetupFramebuffer(GLuint depth, GLuint color, GLbitfield clear = GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  void leaveEvent(QEvent* e) override;
  void enterEvent(QEvent* e) override;
  void keyReleaseEvent(QKeyEvent* e) override;
  void keyPressEvent(QKeyEvent* e) override;
  void mouseMoveEvent(QMouseEvent* e) override;
  void mousePressEvent(QMouseEvent* e) override;
  void mouseReleaseEvent(QMouseEvent* e) override;
  void dragEnterEvent(QDragEnterEvent* event) override;
  void dropEvent(QDropEvent* event) override;
  void wheelEvent(QWheelEvent* event) override;

private:
  bool handleKeyPress(int key, Qt::KeyboardModifiers mod);
  bool handleMousePress(const QPoint& pos, bool& redraw);
  bool handleMouseMove(const QPoint& pos, bool& redraw);

  bool render3D();
  //bool render2D();

  lgx::ImgData* findEditedStack(bool printMsg);
  lgx::ImgData* findSelectStack(bool printMsg);
  lgx::ImgData* findSelectSurf(bool printMsg);
  lgx::ImgData* findSelectMesh(bool printMsg);
  void findSelectTriangle(lgx::ImgData* stk, uint x, uint y, std::vector<uint>& vlist, int& label,
                          bool useParentLabel = true);

  void getGUIFlags(QMouseEvent* e);
  void drawCellMap();

  Qt::MouseButtons _lastButtons;

  void clipEnable();
  void clipDisable();
  // Pick a voxel, if the stack is not labeled, the threshold defines the value above which a voxel is not transparent
  lgx::Point3i pickVoxel(const lgx::Store* store, lgx::Point3f start, lgx::Point3f dir, int threshold = 0) const;

  void deletePickedLabel();
  void labelEditStop();

  QPaintDevice* _current_device;
  bool _need3Dupdate;
  int _finalTexId;

public slots:
  void setupSelectionFI();
  void resetFramebuffer();

  void resetLabel();
  // Make sure textures are of the right kind
  void updateTextures();
  void update3D();
  void update2D();
  void setNewLabel();
  void setLabel(int label);
  void selectMeshLabel(int label, int repeat = 0, bool replace = true);
  void unselectMeshLabel(int label);
  void reloadShaders();
  void UpdateSlot() {
    update();
  }
  void loadFile(const QString& pth, bool stack2, bool load_work);
  void UpdateLabels();

  void enableClip(lgx::Clip* c, bool _val);
  void showClipGrid(lgx::Clip* c, bool _val);
  void setClipWidth(lgx::Clip* c, int _val);

  void resetView();

  void recordMovie(bool on);

  void saveScreenshot(bool automatic = false, bool overwrite = false)
  {
    return QGLViewer::saveSnapshot(automatic, overwrite);
  }
  void saveScreenshot(const QString& fileName, bool overwrite = false)
  {
    return QGLViewer::saveSnapshot(fileName, overwrite);
  }
  void setScreenshotFileName(const QString& name) {
    return QGLViewer::setSnapshotFileName(name);
  }
  void setScreenshotFormat(const QString& format) {
    return QGLViewer::setSnapshotFormat(format);
  }
  void setScreenshotCounter(int counter) {
    return QGLViewer::setSnapshotCounter(counter);
  }
  void setScreenshotQuality(int quality) {
    return QGLViewer::setSnapshotQuality(quality);
  }
  bool openScreenshotFormatDialog() {
    return QGLViewer::openSnapshotFormatDialog();
  }

  virtual void initFromDOMElement(const QDomElement& element) override;

  void ToggleHeatMapSlot();

  virtual bool saveImageSnapshot(const QString& fileName, QSize finalSize, double oversampling = 1.0,
                                 bool expand = false, bool has_gui = false) override;

  bool saveImageSnapshot(const QString& fileName, QSize finalSize, double oversampling, bool expand, bool has_gui,
                         QString* error);

  void showEntireScene() {
    camera()->showEntireScene();
  }

  void grabLabel(const QPoint& pos);
  void setParent(const QPoint& pos);
  void selectLabel(const QPoint& pos);
  void fillLabel(const QPoint& pos);
  void floodFillLabel(const QPoint& pos);
  void selectConnected(const QPoint& pos);
  void addNewSeed(const QPoint& pos);
  void addCurrentSeed(const QPoint& pos);
  void grabSeed(const QPoint& pos);
  void pickVolumeLabel(const QPoint& pos);
  void pixelEdit(const QPoint& pos);
  void deleteVolumeLabel(const QPoint& pos);
  void replaceVolumeLabel(const QPoint& pos);
  void fillVolumeLabel(const QPoint& pos);
  void meshEditStart(const QPoint& pos);
  void meshEditMove(const QPoint& pos);
  void meshEditStop();

signals:
  void deleteSelection();
  void modified();
  void stepDrawFinished(bool);
  void setLabelColor(QIcon&);
  void recordingMovie(bool);
  void changeSceneRadius(float);
  void selectLabelChanged(int);
  void changedNbPeels(int);
  void changedSpecular(float);
  void changedShininess(float);
  void changedUnsharpening(float);
  void changedGlobalContrast(float);
  void changedGlobalBrightness(float);
  void changedScreenSampling(float);
  void changedSlices(uint);
};

#endif /* LITHOVIEWER_HPP */
