/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef AUTOUPDATE_HPP
#define AUTOUPDATE_HPP

#include <LGXConfig.hpp>

#include <Version.hpp>

#include <QString>
#include <QUrl>
#include <QDialog>
#include <QPointer>
#include <QList>

#include "ui_CheckVersionDlg.h"

class QNetworkReply;
class QNetworkAccessManager;

extern QUrl latestINIUrl;

struct INIVersion
{
  INIVersion() {}
  INIVersion(const INIVersion&) = default;
  INIVersion(INIVersion&&) = default;
  INIVersion& operator=(const INIVersion&) = default;
  INIVersion& operator=(INIVersion&&) = default;
  lgx::util::Version version;
  QUrl url;
  QByteArray md5sum;
};

INIVersion parseINIVersion(QIODevice* dev);

class CheckVersionDlg : public QDialog
{
  Q_OBJECT
public:
  CheckVersionDlg(QWidget* parent, bool autoclose = false);

  lgx::util::Version latest_version;

public slots:
  void reject() override;
  void accept() override;
  int exec() override;

protected slots:
  void on_buttonBox_clicked(QAbstractButton* btn);
  void receivedLatestIni(QNetworkReply* reply);
  void receivedNewVersion(QNetworkReply* reply);
  void downloadProgress(qint64 received, qint64 total);
  void answerReceived(QNetworkReply* reply);

signals:
  void fileDownloaded(QNetworkReply* reply);
  void newVersionAvailable();

private:
  void retrieveUrl(const QUrl& url);

  bool autoclose;
  Ui::CheckVersionDlg ui;
  QPointer<QNetworkReply> cur_reply;
  QPointer<QNetworkAccessManager> network;
  lgx::util::Version latestVersion;
  QByteArray md5sum;
  QUrl versionUrl;
};

#endif // AUTOUPDATE_HPP

