/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef CLAMP_H
#define CLAMP_H

#include <LGXConfig.hpp>

/**
 * \file Clamp.hpp
 *
 * Defines the util::clamp function
 */

//#include <config.h>
namespace lgx {
namespace util {
/**
 * \brief A function to clamp a value to a range.
 * \param val The start value.
 * \param min The minimum value of the range.
 * \param max The maximum value of the range.
 *
 *  If \c min is more than \c max, the function returns \c max.
 */
template <class T> T clamp(const T& val, const T& min, const T& max)
{
  if(min >= max)
    return max;
  else if(val < min)
    return min;
  else if(val > max)
    return max;
  else
    return val;
}
} // namespace util
} // namespace lgx

#endif
