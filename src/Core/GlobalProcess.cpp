/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "GlobalProcess.hpp"
#include "MeshProcessCreation.hpp"
#include "Information.hpp"
#include "Progress.hpp"
#include "TransferFunctionDlg.hpp"

#include <QFileDialog>

using qglviewer::Vec;

namespace lgx {
namespace process {
bool SetCurrentStack::operator()(STORE which, int id) {
  return setCurrentStack(id, which);
}
REGISTER_GLOBAL_PROCESS(SetCurrentStack);

bool SaveGlobalTransform::initialize(ParmList& parms, QWidget* parent)
{
  QString filename = parms[0].toString();
  if(filename.isEmpty())
    filename = QFileDialog::getSaveFileName(parent, "Choose transform file to save", QDir::currentPath(),
                                            "Text files (*.txt)");
  if(filename.isEmpty())
    return false;
  if(!filename.endsWith(".txt", Qt::CaseInsensitive))
    filename += ".txt";
  parms[0] = filename;
  return true;
}

bool SaveGlobalTransform::operator()(const QString& filename)
{
  QFile file(filename);
  if(!file.open(QIODevice::WriteOnly)) {
    setErrorMessage(QString("File '%1' cannot be opened for writing").arg(filename));
    return false;
  }
  QTextStream out(&file);

  // Get difference between stacks
  qglviewer::Frame f1(stack(0)->getFrame().worldInverse()), f2;
  f2.setFromMatrix(stack(1)->getFrame().worldMatrix());
  f2.setReferenceFrame(&f1);

  // Write to file
  Matrix4d m(f2.worldMatrix(), util::GL_STYLE);
  out << m << endl;
  file.close();

  SETSTATUS(QString("Transform saved to: %1").arg(filename));
  return true;
}
REGISTER_GLOBAL_PROCESS(SaveGlobalTransform);

bool JoinRegionsSegment::operator()(Store* work, Mesh* mesh, float cubeSize)
{
  const Stack* stack = work->stack();
  HVecUS& data = work->data();

  // Get selected vertices, and find connected region
  vvgraph V(mesh->activeVertices());
  mesh->getConnectedVertices(V);

  // Find center of all cells (should be closed surfaces)
  typedef std::pair<int, int> IntIntPair;
  typedef std::pair<int, Point3f> IntPoint3fPair;
  std::map<int, Point3f> posMap;
  std::map<int, int> cntMap;
  int count = 0;
  while(!V.empty()) {
    vvgraph T;
    T.insert(*V.begin());
    mesh->getConnectedVertices(T);
    forall(const vertex& v, T) {
      posMap[count] += v->pos;
      cntMap[count]++;
      V.erase(v);
    }
    count++;
  }
  forall(const IntIntPair& p, cntMap)
    posMap[p.first] /= p.second;

  // Find stack label of center of regions and put in a set
  std::set<int> stackLabelSet;
  forall(const IntPoint3fPair& p, posMap) {
    Point3i imgPos = stack->worldToImagei(p.second);
    int stackLabel = data[stack->offset(imgPos)];
    if(stackLabel > 0)
      stackLabelSet.insert(data[stack->offset(imgPos)]);
  }

  // Update voxels
  int newLabel = *(stackLabelSet.begin());
  for(uint i = 0; i < data.size(); i++) {
    if(data[i] == 0)
      continue;
    if(stackLabelSet.count(data[i]) > 0)
      data[i] = newLabel;
  }

  // Also update mesh labels
  V = vvgraph(mesh->activeVertices());
  mesh->getConnectedVertices(V);
  forall(const vertex& v, V) {
    v->selected = false;
    v->label = newLabel;
  }

  // Now re-march the label
  if(cubeSize > 0) {
    MarchingCube3D mc(*this);
    mc(mesh, work, cubeSize, 1, 0, newLabel);
  }

  mesh->updateTriangles();
  mesh->updateSelection();
  work->changed();
  work->setLabels(true);
  return true;
}
REGISTER_GLOBAL_PROCESS(JoinRegionsSegment);

bool NameTransferFunction::operator()(const ParmList& parms)
{
  QString name = parms[0].toString();
  if(name.isEmpty())
    return setErrorMessage("Error, you need to specify a valid name.");
  QString which = parms[1].toString().toLower();
  if(which == "store")
    return nameStoreFunction(name);
  if(which == "heat")
    return nameHeatFunction(name);
  if(which == "signal")
    return nameSignalFunction(name);
  return setErrorMessage("Error, 'Which' argument should be one of 'store', 'heat' or 'signal'.");
}

bool NameTransferFunction::nameStoreFunction(const QString& name)
{
  auto* store = currentStack()->currentStore();
  if(not store or not store->isVisible())
    return setErrorMessage("Error, a store must be active to name its transfer function");
  auto fct = store->transferFct();
  return nameFunction(fct, name);
}

bool NameTransferFunction::nameSignalFunction(const QString& name)
{
  auto* mesh = currentMesh();
  auto fct = mesh->surfFct();
  return nameFunction(fct, name);
}

bool NameTransferFunction::nameHeatFunction(const QString& name)
{
  auto* mesh = currentMesh();
  auto fct = mesh->heatFct();
  return nameFunction(fct, name);
}

bool NameTransferFunction::nameFunction(const TransferFunction& fct, const QString& name)
{
  return gui::TransferFunctionDlg::nameFunction(fct, name);
}

REGISTER_GLOBAL_PROCESS(NameTransferFunction);

bool ChooseTransferFunction::operator()(const ParmList& parms)
{
  QString name = parms[0].toString();
  if(name.isEmpty())
    return setErrorMessage("Error, you need to specify a valid name.");
  QString str_bounds = parms[2].toString().toLower();
  Point2d bounds(0, 0);
  if(str_bounds == "keep")
    bounds[1] = 0;
  else if(str_bounds == "adjust") {
    bounds[0] = 1;
    bounds[1] = -1;
  } else if(str_bounds == "default")
    bounds[1] = -2;
  else {
    bool ok;
    double f = str_bounds.toDouble(&ok);
    if(ok) {
      if(f > 1) f /= 100;
      if(f > 1) f = 1;
      bounds[0] = f;
      bounds[1] = -1;
    } else {
      bounds = (Point2d)util::stringToPoint2f(str_bounds);
      if(bounds[0] != bounds[0] or bounds[0] >= bounds[1]) // if NaN
        return setErrorMessage("Error, 'Bounds' parameter must be one of 'keep', 'adjust', 'default', a single number or two increasing numbers.");
    }
  }
  QString which = parms[1].toString().toLower();
  if(which == "store")
    return chooseStoreFunction(name, bounds);
  if(which == "heat")
    return chooseHeatFunction(name, bounds);
  if(which == "signal")
    return chooseSignalFunction(name, bounds);
  return setErrorMessage("Error, 'Which' argument should be one of 'store', 'heat' or 'signal'.");
}

bool ChooseTransferFunction::chooseStoreFunction(const QString& name, Point2d bounds)
{
  auto* store = currentStack()->currentStore();
  if(not store or not store->isVisible())
    return setErrorMessage("Error, a store needs to be active to change its transfer function.");
  auto fct = gui::TransferFunctionDlg::transferFunction(name);
  if(fct.empty())
    return setErrorMessage(QString("Error, no function names '%1'").arg(name));
  if(bounds[0] == bounds[1])
    bounds = store->transferFct().valueRange();
  bool autoScale = false;
  if(bounds[0] > bounds[1]) {
    if(bounds[1] == -1) {
      if(bounds[0] < 1) {
        autoScale = true;
      } else {
        store->updateBounds();
        bounds = Point2d(store->bounds()) / 65535.;
      }
    }
  }
  if(bounds[0] < bounds[1])
    fct.adjust(bounds[0], bounds[1]);
  store->setTransferFct(fct);
  if(autoScale)
    store->autoScale(bounds[0]);
  return true;
}

bool ChooseTransferFunction::chooseSignalFunction(const QString& name, Point2d bounds)
{
  auto* mesh = currentMesh();
  if(not mesh)
    return setErrorMessage("Error, you need an active stack.");
  auto fct = gui::TransferFunctionDlg::transferFunction(name);
  if(fct.empty())
    return setErrorMessage(QString("Error, no function names '%1'").arg(name));
  if(bounds[0] == bounds[1])
    bounds = mesh->surfFct().valueRange();
  else if(bounds[0] > bounds[1])
    bounds = mesh->signalBounds();
  if(bounds[0] < bounds[1])
    fct.adjust(bounds[0], bounds[1]);
  mesh->setSurfFct(fct);
  return true;
}

bool ChooseTransferFunction::chooseHeatFunction(const QString& name, Point2d bounds)
{
  auto* mesh = currentMesh();
  if(not mesh)
    return setErrorMessage("Error, you need an active stack.");
  auto fct = gui::TransferFunctionDlg::transferFunction(name);
  if(fct.empty())
    return setErrorMessage(QString("Error, no function names '%1'").arg(name));
  if(bounds[0] == bounds[1])
    bounds = mesh->heatFct().valueRange();
  else if(bounds[0] > bounds[1])
    bounds = mesh->heatMapBounds();
  if(bounds[0] < bounds[1])
    fct.adjust(bounds[0], bounds[1]);
  mesh->setHeatFct(fct);
  return true;
}

REGISTER_GLOBAL_PROCESS(ChooseTransferFunction);

} // namespace process
} // namespace lgx
