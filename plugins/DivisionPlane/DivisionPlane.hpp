#ifndef DivisionPlane_HPP
#define DivisionPlane_HPP

#include <Process.hpp>

namespace process
{
class DivisionPlane : public GlobalProcess
{
public:
    DivisionPlane(const GlobalProcess& process)
        : Process(process)
        , GlobalProcess(process)
    { }
    
    bool operator()(const QStringList& strings, const QList<float>& values)
    {
        if(!checkState().store(STORE_NON_EMPTY|STORE_LABEL))
            return false;
        Stack *stk = currentStack();
        Store *store = stk->currentStore();
        int id1 = (int)values[0];
        int id2 = (int)values[1];
        return operator()(strings[0], currentMesh(), store, id1, id2);
    }
    
    bool operator()(QString filename, Mesh* mesh, const Store* store, int id1, int id2);
    
    QString folder() const { return "Shape Analysis"; }
    QString name() const { return "Division Plane"; }
    QString description() const { return "Compute the division plane between the two cells present in the current stack. Place the cutting plane on the division plane."; }
    QStringList parameters() const { return QStringList() << "Output" << "Cell ID 1" << "Cell ID 2"; }
    QStringList default_strings() const { return QStringList() << ""; }
    StringChoiceMap string_choice() const {
        StringChoiceMap map;
        return map;
    }
    QList<float> default_values() const { return QList<float>() << -1 << -1; }
    QIcon icon() const
    {
        return QIcon(":/images/DivisionPlane.png");
    }
    
};
}

#endif // DivisionPlane_HPP


