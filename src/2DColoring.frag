// Number of colors in the label color map
uniform uint nb_colors;
// The label color map
uniform sampler1D labelcolormap;

uniform float tex_brightness[2];
uniform float tex_opacity[2];

#if LABEL_TEX
uniform usampler2D tex;
#endif
#if COLOR_TEX
uniform sampler2D tex;
#endif

#if SECOND_LABEL_TEX
uniform usampler2D second_tex;
#endif
#if SECOND_COLOR_TEX
uniform sampler2D second_tex;
#endif

uniform sampler1D colormap[2];

smooth centroid in vec2 texPos;

vec4 colormap_color(sampler2D tex, uint idx)
{
  float value = texture(tex, texPos).r;
  vec4 col = texture(colormap[idx], value);
  col.rgb *= tex_brightness[idx];
  col.a *= tex_opacity[idx];
  return col;
}

vec4 index_color(usampler2D tex, uint idx)
{
  uint value = texture(tex, texPos).r;
  if(value == 0u) return vec4(0,0,0,0);
  if(value >= nb_colors)
    value = value % nb_colors;
  vec4 col = texelFetch(labelcolormap, int(value), 0);
  col.rgb *= tex_brightness[idx];
  col.a *= tex_opacity[idx];
  return col;
}

#if LABEL_TEX
vec4 color1()
{
  /*return vec4(tex_opacity[0],0,0,tex_opacity[0]);*/
  return index_color(tex, 0u);
}
#elif COLOR_TEX
vec4 color1()
{
  /*return vec4(0,tex_opacity[0],0,tex_opacity[0]);*/
  return colormap_color(tex, 0u);
}
#endif

#if SECOND_LABEL_TEX
vec4 color2()
{
  /*return vec4(tex_opacity[1],0,1,tex_opacity[1]);*/
  return index_color(second_tex, 1u);
}
#elif SECOND_COLOR_TEX
vec4 color2()
{
  /*return vec4(0,tex_opacity[1],1,tex_opacity[1]);*/
  return colormap_color(second_tex, 1u);
}
#endif

void setColor() {
#if (LABEL_TEX || COLOR_TEX) && (SECOND_LABEL_TEX || SECOND_COLOR_TEX)
  vec4 c1 = (tex_opacity[0] > 0.0 ? premulColor(color1()) : vec4(0,0,0,0));
  vec4 c2 = (tex_opacity[1] > 0.0 ? premulColor(color2()) : vec4(0,0,0,0));
  gl_FragColor = normalColor(mixColors(c1, c2));
#elif (LABEL_TEX || COLOR_TEX)
  gl_FragColor = (tex_opacity[0] > 0.0 ? color1() : vec4(0,0,0,0));
#elif (SECOND_LABEL_TEX || SECOND_COLOR_TEX)
  gl_FragColor = (tex_opacity[1] > 0.0 ? color2() : vec4(0,0,0,0));
#else
  gl_FragColor = vec4(0,0,0,0);
#endif
}

