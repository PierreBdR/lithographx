/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "MeshProcessCellShapes.hpp"

#include "Dir.hpp"
#include "EigenDecomposition.hpp"
#include "Algorithms.hpp"
#include "Progress.hpp"
#include "CSVStream.hpp"

#include <QFileDialog>
#include <limits>

namespace lgx {
namespace process {

namespace
{
struct Triangle
{
  Point3f p1, p2, p3;
  double area;
};

Matrix2f triangleMoment2d(const Triangle& tr, const Point2f& center = Point2f())
{
  Point2f p1 = Point2f(tr.p1) - center;
  Point2f p2 = Point2f(tr.p2) - center;
  Point2f p3 = Point2f(tr.p3) - center;
  double xx = (p1.x()*p1.x() + p2.x()*p2.x() + p3.x()*p3.x() +
               p1.x()*p2.x() + p1.x()*p3.x() + p2.x()*p3.x())/6.;
  double yy = (p1.y()*p1.y() + p2.y()*p2.y() + p3.y()*p3.y() +
               p1.y()*p2.y() + p1.y()*p3.y() + p2.y()*p3.y())/6.;
  double xy = (2*p1.x()*p1.y() + 2*p2.x()*p2.y() + 2*p3.x()*p3.y() +
               p1.x()*p2.y() + p1.x()*p3.y() + p2.x()*p1.y() + p2.x()*p3.y() + p3.x()*p1.y() + p3.x()*p2.y()) / 12.;
  double m[4] = {xx, xy, xy, yy};
  return Matrix2f(m);
}

float signedArea(const Point2f& p1, const Point2f& p2, const Point2f& p3)
{
  return ((p2 - p1) ^ (p3 - p1))/2.f;
}

enum CellShapeData
{
  SD_X = 0,
  SD_Y,
  SD_Z,
  SD_POLAR_RADIUS,
  SD_POLAR_THETA,
  SD_MAJOR_LENGTH,
  SD_MINOR_LENGTH,
  SD_ECCENTRICITY,
  SD_MAJOR_AXIS_THETA,
  SD_AREA,
  SD_PERIMETER,
  SD_RADIUS_AVERAGE,
  SD_RADIUS_MAX,
  SD_RADIUS_MIN,
  SD_CELL_EXTENSION,
  SD_INCLINE_ANGLE,
  SD_NB_FIELDS
};

QString fieldName(CellShapeData field)
{
  switch(field)
  {
    case SD_X:
      return "X";
    case SD_Y:
      return "Y";
    case SD_Z:
      return "Z";
    case SD_POLAR_RADIUS:
      return "Polar Radius";
    case SD_POLAR_THETA:
      return "Polar Theta";
    case SD_MAJOR_LENGTH:
      return "Majoraxis";
    case SD_MINOR_LENGTH:
      return "Minoraxis";
    case SD_ECCENTRICITY:
      return "Eccentricity";
    case SD_MAJOR_AXIS_THETA:
      return "Majoraxis Theta";
    case SD_AREA:
      return "Area";
    case SD_PERIMETER:
      return "Perimeter";
    case SD_RADIUS_AVERAGE:
      return "Radius Average";
    case SD_RADIUS_MAX:
      return "Radius Max";
    case SD_RADIUS_MIN:
      return "Radius Min";
    case SD_CELL_EXTENSION:
      return "Cell Extension";
    case SD_INCLINE_ANGLE:
      return "Incline Angle";
    case SD_NB_FIELDS:
      return "NbFields";
  }
  return "";
}

QString fieldUnit(CellShapeData field)
{
  switch(field)
  {
    case SD_X:
    case SD_Y:
    case SD_Z:
    case SD_POLAR_RADIUS:
    case SD_MAJOR_LENGTH:
    case SD_MINOR_LENGTH:
    case SD_PERIMETER:
    case SD_RADIUS_AVERAGE:
    case SD_RADIUS_MAX:
    case SD_RADIUS_MIN:
    case SD_CELL_EXTENSION:
      return UM;
    case SD_POLAR_THETA:
    case SD_INCLINE_ANGLE:
    case SD_ECCENTRICITY:
    case SD_MAJOR_AXIS_THETA:
    case SD_NB_FIELDS:
      return "";
    case SD_AREA:
      return UM2;
  }
  return "";
}

CellShapeData parseCellShapeHeat(const QString& s)
{
  QString l = s.toLower();
  if(l == "none")
    return SD_NB_FIELDS;
  if(l == "radial position")
    return SD_POLAR_RADIUS;
  if(l == "angular position")
    return SD_POLAR_THETA;
  if(l == "major axis length")
    return SD_MAJOR_LENGTH;
  if(l == "minor axis length")
    return SD_MINOR_LENGTH;
  if(l == "eccentricity")
    return SD_ECCENTRICITY;
  if(l == "major axis orientation")
    return SD_MAJOR_AXIS_THETA;
  if(l == "area")
    return SD_AREA;
  if(l == "perimeter")
    return SD_PERIMETER;
  if(l == "average radius")
    return SD_RADIUS_AVERAGE;
  if(l == "maximum radius")
    return SD_RADIUS_MAX;
  if(l == "minimum radius")
    return SD_RADIUS_MIN;
  if(l == "cell extension")
    return SD_CELL_EXTENSION;
  if(l == "incline angle")
    return SD_INCLINE_ANGLE;
  Information::out << "Warning, unknown Cell Shape heat: '" << s << "'" << endl;
  return SD_NB_FIELDS;
}

} // namespace

bool CellShapesProcess::initialize(ParmList& parms, QWidget* parent)
{
  QString filename = parms[0].toString();
  if(filename.isEmpty())
    filename = util::currentPath();

  filename = QFileDialog::getSaveFileName(parent, "CSV file for cell shape features", filename,
                                          "CSV files (*.csv);;All files (*.*)");

  if(filename.isEmpty() and parms[1].toString().toLower() == "none")
    return false;
  parms[0] = filename;
  return true;
}

bool CellShapesProcess::operator()(Mesh* mesh, const QString& filename, const QString& heatStr)
{
  // First, compute cell axes
  auto heat = parseCellShapeHeat(heatStr);
  if(heat == SD_NB_FIELDS and filename.isEmpty())
    return setErrorMessage("You need to correctly specify a file to write the result to or a heat to display.");
  Compute2DCellShapeAxis cellAxisProc(*this);
  cellAxisProc(mesh, Point2f(2., 2.)); // Correct for an ellipsis

  // Compute data
  std::unordered_map<int,size_t> cells;
  std::vector<int> labels;

  const auto& cellAxis = mesh->cellAxis();
  for(const auto& p : cellAxis) {
    auto cid = labels.size();
    labels.push_back(p.first);
    cells[p.first] = cid;
  }

  auto nb_cells = labels.size();

  std::vector<std::array<float, SD_NB_FIELDS>> fields(nb_cells);
  const auto& S = mesh->graph();
  const auto* stk = mesh->stack();
  Matrix4d mm;

  for(const vertex& v : S) if(v->type == 'c' and v->label > 0) {
    int label = v->label;
    auto cid = cells[label];

    // Global position
    auto glob_pos = stk->worldToGlobal(v->pos);
    fields[cid][SD_X] = glob_pos.x();
    fields[cid][SD_Y] = glob_pos.y();
    fields[cid][SD_Z] = glob_pos.z();

    // Polar coordinates
    float radius = norm(Point2f(glob_pos));
    float theta = atan2(glob_pos.y(), glob_pos.x());
    fields[cid][SD_POLAR_RADIUS] = radius;
    fields[cid][SD_POLAR_THETA] = theta;

    // Ellipse fitting
    auto found = cellAxis.find(label);
    if(found == cellAxis.end())
      return setErrorMessage(QString("Error, couldn't find axis information for cell '%1'").arg(label));
    const auto& tensor = found->second;
    fields[cid][SD_MAJOR_LENGTH] = tensor.evals()[0];
    fields[cid][SD_MINOR_LENGTH] = tensor.evals()[1];
    fields[cid][SD_CELL_EXTENSION] = sqrt(tensor.evals()[0]*tensor.evals()[1]);
    float ratio = tensor.evals()[1] / tensor.evals()[0];
    fields[cid][SD_ECCENTRICITY] = sqrt(1-ratio*ratio);
    auto ev1 = stk->worldToGlobalVector(tensor.ev1());
    double major_theta = atan2(ev1.y(), ev1.x());
    if(major_theta > M_PI/2)
      major_theta -= M_PI;
    else if(major_theta < M_PI/2)
      major_theta += M_PI;
    fields[cid][SD_MAJOR_AXIS_THETA] = major_theta;
    auto incline_sin_vec = glob_pos ^ ev1;
    float incline_sin = norm(incline_sin_vec);
    if(incline_sin_vec * v->nrml < 0)
      incline_sin = -incline_sin;
    float incline_cos = glob_pos * ev1;
    auto incline_angle = atan2(incline_sin, incline_cos);
    if(incline_angle > M_PI/2) incline_angle -= M_PI;
    if(incline_angle <= -M_PI/2) incline_angle += M_PI;
    fields[cid][SD_INCLINE_ANGLE] = incline_angle;

    // Area and perimeter -- handle case of not quite flat, not convex, polygons
    float area = 0, perimeter = 0;
    float min_radius = std::numeric_limits<float>::max();
    float max_radius = 0;
    float mean_radius = 0;
    for(const vertex& n: S.neighbors(v)) {
      const vertex& m = S.nextTo(v, n);
      float dl = norm(m->pos - n->pos);
      float start_radius = norm(n->pos - v->pos);
      float end_radius = norm(m->pos - v->pos);
      mean_radius = (start_radius + end_radius)/2 * dl;
      min_radius = std::min(min_radius, start_radius);
      max_radius = std::max(max_radius, start_radius);
      perimeter += dl;
      Point3f normal = (n->pos - v->pos) ^ (m->pos - v->pos);
      float tr_area = norm(normal)/2;
      if(normal * v->nrml < 0)
        tr_area -= tr_area;
      area += tr_area;
    }
    mean_radius /= perimeter;
    fields[cid][SD_AREA] = area;
    fields[cid][SD_PERIMETER] = perimeter;
    fields[cid][SD_RADIUS_AVERAGE] = mean_radius;
    fields[cid][SD_RADIUS_MAX] = max_radius;
    fields[cid][SD_RADIUS_MIN] = min_radius;
  }

  if(not filename.isEmpty()) {
    QFile file(filename);
    if(not file.open(QIODevice::WriteOnly))
      return setErrorMessage(QString("Error, couldn't open file '%1' for writing").arg(filename));
    util::CSVStream ts(&file);
    QStringList header;
    header << "Label";
    for(int i = 0 ; i < SD_NB_FIELDS ; ++i) {
      QString field = fieldName((CellShapeData)i);
      auto unit = fieldUnit((CellShapeData)i);
      if(not unit.isEmpty())
        field += "(" + unit + ")";
      header << field;
    }
    ts << header;

    std::sort(labels.begin(), labels.end());
    for(int lab: labels) {
      auto cid = cells[lab];
      QStringList csv_fields;
      csv_fields << QString::number(lab);
      const auto& values = fields[cid];
      auto nb_digits = std::numeric_limits<float>::max_digits10;
      for(size_t i = 0 ; i < SD_NB_FIELDS ; ++i)
        csv_fields << QString::number(values[i], 'g', nb_digits);
      ts << csv_fields;
    }
  }

  if(heat != SD_NB_FIELDS) {
    auto& labelHeat = mesh->labelHeat();
    labelHeat.clear();
    float low = std::numeric_limits<float>::max();
    float high = std::numeric_limits<float>::lowest();
    for(int lab: labels) {
      auto cid = cells[lab];
      auto value = fields[cid][heat];
      labelHeat[lab] = value;
      if(value > high) high = value;
      if(value < low) low = value;
    }
    mesh->heatMapDesc() = Description(fieldName(heat));
    mesh->heatMapUnit() = fieldUnit(heat);
    mesh->heatMapBounds() = Point2f(low, high);
    mesh->updateTriangles();
  }

  return true;
}

REGISTER_MESH_PROCESS(CellShapesProcess);

bool Compute2DCellShapeAxis::operator()(Mesh* mesh, const Point2f& correction_factor)
{
  // First, collect the cells
  std::unordered_map<int,size_t> cells;
  std::vector<int> labels;
  std::vector<std::vector<Triangle>> triangles;
  std::vector<Point3f> normals, centers;
  std::vector<float> areas;
  const auto& S = mesh->graph();
  Progress progress("Compute Cell Shape Axis 2D", S.size()*2);
  int interval = S.size() / 100;
  int progress_cnt = 0;
  if(mesh->cells()) {
    for(const vertex& v: S) {
      if((progress_cnt++) % interval == 0 and not progress.advance(progress_cnt))
        userCancel();
      if(v->type == 'c' and v->label > 0) {
        if(cells.find(v->label) != cells.end())
          return setErrorMessage("Error, two cells have the same label!");
        size_t cid = labels.size();
        labels.push_back(v->label);
        cells[v->label] = cid;
        triangles.emplace_back();
        normals.push_back(normalized(v->nrml));
        centers.push_back(v->pos);
        for(const vertex& n: S.neighbors(v)) {
          const vertex& m = S.nextTo(v, n);
          triangles[cid].push_back({v->pos, n->pos, m->pos, 0});
        }
      }
    }
  } else {
    for(const vertex& v: S) {
      if((progress_cnt++) % interval == 0 and not progress.advance(progress_cnt))
        userCancel();
      for(const vertex& n: S.neighbors(v)) {
        const vertex& m = S.nextTo(v, n);
        if(mesh->uniqueTri(v, n, m)) {
          int lab = mesh->getLabel(v, n, m);
          if(lab > 0) {
            auto found = cells.find(lab);
            size_t cid;
            if(found == cells.end()) {
              cid = labels.size();
              labels.push_back(lab);
              triangles.emplace_back();
              cells[lab] = cid;
              normals.emplace_back();
              centers.emplace_back();
              areas.push_back(0);
            } else
              cid = found->second;
            double tr_area = norm((n->pos - v->pos) ^ (m->pos - v->pos))/2;
            normals[cid] += tr_area*(v->nrml + n->nrml + m->nrml);
            centers[cid] += tr_area*(v->pos + n->pos + m->pos)/3;
            areas[cid] += tr_area;

            triangles[cid].push_back({v->pos, n->pos, m->pos, 0});
          }
        }
      }
    }
    for(size_t cid = 0 ; cid < labels.size() ; ++cid) {
      centers[cid] /= areas[cid];
      normals[cid].normalize();
    }
  }

  size_t nb_cells = labels.size();

  progress.setMaximum(S.size() + nb_cells + 2);

  SETSTATUS(QString("Found %1 cells").arg(nb_cells));

  bool valid = true;
  std::vector<util::SymmetricTensor> tensors(nb_cells);
  auto processCell = [&valid, &triangles, &normals, &centers, &tensors, &labels, &correction_factor](size_t cid) -> bool {
    // First, find the best fit plane of the triangles, averaging the normals and finding the center
    auto& trs = triangles[cid];
    const Point3f& normal = normals[cid];
    const Point3f& center = centers[cid];
    double total_area = 0;
    Point3f u, v;
    if(normal.x() <= normal.y() and normal.x() <= normal.z()) {
      u = normalized(normal ^ Point3f(1,0,0));
      v = normal ^ u;
    } else if(normal.y() <= normal.x() and normal.y() <= normal.z()) {
      u = normalized(normal ^ Point3f(0,1,0));
      v = normal ^ u;
    } else {
      u = normalized(normal ^ Point3f(0,0,1));
      v = normal ^ u;
    }

    Matrix2f moment;
    total_area = 0;
    for(Triangle& tr: trs) {
      tr.p1 -= center;
      tr.p2 -= center;
      tr.p3 -= center;
      tr.p1 = Point3f(tr.p1*u, tr.p1*v, 0);
      tr.p2 = Point3f(tr.p2*u, tr.p2*v, 0);
      tr.p3 = Point3f(tr.p3*u, tr.p3*v, 0);
      tr.area = Point2f(tr.p2 - tr.p1) ^ Point2f(tr.p3 - tr.p1) / 2;
      moment += tr.area * triangleMoment2d(tr);
      total_area += tr.area;
    }

    moment /= total_area;

    const auto& decomp = util::eigenDecomposition(moment, util::EigenSort::Decreasing);
    if(not decomp.valid() or decomp.eval[0] < 0 or decomp.eval[1] < 0) {
      Information::err << "Failed Eigen-value decomposition for cell " << labels[cid] << endl;
      valid = false;
      return true;
    }

    float v1, v2;
    if(normsq(correction_factor) == 0) {
      v1 = v2 = 0;
      for(const Triangle& tr: trs) {
        float x1 = Point2f(tr.p1) * decomp.evec[0];
        float x2 = Point2f(tr.p2) * decomp.evec[0];
        float x3 = Point2f(tr.p3) * decomp.evec[0];
        float y1 = Point2f(tr.p1) * decomp.evec[1];
        float y2 = Point2f(tr.p2) * decomp.evec[1];
        float y3 = Point2f(tr.p3) * decomp.evec[1];

        float x = std::max(std::max(fabs(x1), fabs(x2)), fabs(x3));
        float y = std::max(std::max(fabs(y1), fabs(y2)), fabs(y3));

        v1 = std::max(x, v1);
        v2 = std::max(y, v2);
      }
    } else {
      v1 = sqrt(decomp.eval[0]) * correction_factor[0];
      v2 = sqrt(decomp.eval[1]) * correction_factor[1];
    }

    /*
     *Information::out << "Center for cell " << labels[cid] << " = " << center
     *                 << "   v1 = " << v1 << " - v2 = " << v2
     *                 << " - normal = " << normal << endl;
     */

    //auto t = util::SymmetricTensor(Point3f(decomp.evec[0]), Point3f(decomp.evec[1]),
    //                               Point3f(v1, v2, 0));
    auto t = util::SymmetricTensor(decomp.evec[0].x() * u + decomp.evec[0].y()*v,
                                   decomp.evec[1].x() * u + decomp.evec[1].y()*v,
                                   Point3f(v1, v2, 0));
    tensors[cid] = t;
    return true;
  };

  util::apply_kernel_it(processCell, util::Counter(0), util::Counter(nb_cells), progress, progress_cnt, 1);

  progress_cnt += nb_cells;

  if(not valid)
    setWarningMessage("Error, couldn't compute Eigen value decomposition for some cells at least.\n"
                      "See terminal output for more details on the errors.");

  // Now, update all tensors
  auto& cellAxis = mesh->cellAxis();
  cellAxis.clear();

  for(size_t cid = 0 ; cid < nb_cells ; ++cid) {
    auto lab = labels[cid];
    cellAxis[lab] = tensors[cid];
  }

  if(not progress.advance(progress_cnt+1))
    userCancel();

  if(mesh->cells()) {
    for(const vertex& v: S) {
      if(v->type == 'c' and v->label > 0) {
        auto cid = cells[v->label];
        v->pos = centers[cid];
        v->nrml = normals[cid];
      }
    }
  } else {
    mesh->updateCentersNormals();
    auto& labelCenter = mesh->labelCenter();
    auto& labelNormal = mesh->labelNormal();
    labelCenter.clear();
    labelNormal.clear();
    for(size_t cid = 0 ; cid < nb_cells ; ++cid) {
      auto lab = labels[cid];
      labelCenter[lab] = centers[cid];
      labelNormal[lab] = normals[cid];
    }
  }

  mesh->setCellAxis();
  Description axisType = Description("Cell Shape 2D")
    << std::make_pair("Correction factor", QString("%1 %2").arg(correction_factor[0]).arg(correction_factor[1]));
  mesh->cellAxisDesc() = axisType;
  mesh->cellAxisUnit() = UM;
  mesh->updateAll();

  return true;
}

bool Compute2DCellShapeAxis::showResult()
{
  ParmList parms;
  if(not getLastParms("Mesh", "Display Cell Shape Axis 2D", parms))
    return setErrorMessage("Couldn't find drawer process 'Mesh.Display_Cell_Shape_Axis_2D'");
  return runProcess("Mesh", "Display Cell Shape Axis 2D", parms);
}

REGISTER_MESH_PROCESS(Compute2DCellShapeAxis);

bool Save2DCellShapeAxis::operator()(Mesh* mesh, const QString& filename)
{
  QString k1Name = QString("Major Axis (%1)").arg(UM);
  QString k2Name = QString("Minor Axis (%1)").arg(UM);
  return saveFile(mesh, filename, k1Name, k2Name);
}

REGISTER_MESH_PROCESS(Save2DCellShapeAxis);

bool Load2DCellShapeAxis::operator()(Mesh* mesh, const QString& filename)
{
  auto k1Name = QRegularExpression(QString("Major Axis \\(%1\\)").arg(UM));
  auto k2Name = QRegularExpression(QString("Minor Axis \\(%1\\)").arg(UM));
  QRegularExpression k3Name;
  Information::out << "Loading from file '" << filename << "'" << endl;
  if(not readFile(mesh, "Cell Shape 2D", filename, k1Name, k2Name, k3Name))
    return false;
  Information::out << "   SUCCESS!" << endl;
  mesh->cellAxisUnit() = UM;
  return true;
}

REGISTER_MESH_PROCESS(Load2DCellShapeAxis);

namespace {
enum CellAxisDisplayType {
  CD_NONE,
  CD_MINOR_AXIS,
  CD_MAJOR_AXIS,
  CD_ECCENTRICITY
};

CellAxisDisplayType readCellAxisDisplayType(QString value, bool* ok = 0)
{
  value = value.toLower();
  if(ok)
    *ok = true;
  if(value == "none")
    return CD_NONE;
  if(value == "minor axis")
    return CD_MINOR_AXIS;
  if(value == "major axis")
    return CD_MAJOR_AXIS;
  if(value == "eccentricity")
    return CD_ECCENTRICITY;
  if(ok)
    *ok = false;
  return CD_NONE;
}

} // namespace

bool Display2DCellShapeAxis::operator()(Mesh* mesh, const QString& displayHeatMapStr,
                                        const QColor& qminorColor, const QColor& qmajorColor,
                                        float axisLineWidth, float axisOffset)
{
  const IntSymTensorMap& dirs = mesh->cellAxis();
  if(dirs.size() == 0 or mesh->cellAxisDesc().type() != "Cell Shape 2D")
    return setErrorMessage(QString("No 2D Cell Shape Axis stored in active mesh."));

  bool ok;
  auto displayHeatMap = readCellAxisDisplayType(displayHeatMapStr, &ok);
  if(not ok)
    return setErrorMessage(QString("Error, unknown heat map type: '%1'").arg(displayHeatMapStr));

  mesh->updateCentersNormals();

  mesh->cellAxisWidth() = axisLineWidth;
  mesh->cellAxisOffset() = axisOffset;

  Colorf minorColor = qminorColor;
  Colorf majorColor = qmajorColor;
  mesh->prepareAxisDrawing(1., majorColor, minorColor, minorColor, Point3b(true, true, false));

  if(displayHeatMap != CD_NONE) {
    auto& labelHeat = mesh->labelHeat();
    float min_val = std::numeric_limits<float>::max();
    float max_val = std::numeric_limits<float>::lowest();
    for(const auto& p: mesh->cellAxis()) {
      float val;
      switch(displayHeatMap) {
        case CD_MAJOR_AXIS:
          val = p.second.evals()[0];
          break;
        case CD_MINOR_AXIS:
          val = p.second.evals()[1];
          break;
        case CD_ECCENTRICITY:
          {
            float ratio = p.second.evals()[1] / p.second.evals()[0];
            val = sqrt(1-ratio*ratio);
          }
          break;
        case CD_NONE:
          continue;
      }
      labelHeat[p.first] = val;
      if(val < min_val) min_val = val;
      if(val > max_val) max_val = val;
    }

    mesh->heatMapBounds() = Point2f(min_val, max_val);

    switch(displayHeatMap) {
      case CD_MAJOR_AXIS:
        mesh->heatMapDesc() = Description("Major Axis");
        mesh->heatMapUnit() = UM;
        break;
      case CD_MINOR_AXIS:
        mesh->heatMapDesc() = Description("Minor Axis");
        mesh->heatMapUnit() = UM;
        break;
      case CD_ECCENTRICITY:
        mesh->heatMapDesc() = Description("Eccentricity");
        mesh->heatMapUnit() = "";
        break;
      case CD_NONE:
        mesh->heatMapDesc() = Description();
        mesh->heatMapUnit() = "";
        break;
    }

    mesh->updateTriangles();
  }

  return true;
}

REGISTER_MESH_PROCESS(Display2DCellShapeAxis);


} // namespace process
} // namespace lgx
