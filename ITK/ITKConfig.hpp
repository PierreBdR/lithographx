/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef ITKCONFIG_HPP
#define ITKCONFIG_HPP

#include <LGXConfig.hpp>

#if defined(WIN32) || defined(WIN64)

#  ifdef lgxITKutil_EXPORTS
#    define itkutil_EXPORT __declspec(dllexport)
#  else
#    define itkutil_EXPORT __declspec(dllimport)
#  endif

#  ifdef lgxITK_EXPORTS
#    define itk_EXPORT __declspec(dllexport)
#  else
#    define itk_EXPORT __declspec(dllimport)
#  endif

#else

#  define itkutil_EXPORT
#  define itk_EXPORT

#endif

#endif
