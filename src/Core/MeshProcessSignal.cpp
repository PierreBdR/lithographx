/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "MeshProcessSignal.hpp"
#include "Algorithms.hpp"

#include "Progress.hpp"

#include <unordered_set>

#include "ui_LoadHeatMap.h"
#ifdef _OPENMP
#  include <omp.h>
#endif

namespace lgx {
namespace process {

using namespace util;

bool ViewMeshProcess::operator()(const ParmList& parms)
{
  QStringList warning;

  Mesh* m = currentMesh();

  if(not m)
    return setErrorMessage("Error, you need an active mesh");

  if(not parms[0].toString().isEmpty()) {
    if(parmToBool(parms[0]))
      m->showSurface();
    else
      m->hideSurface();
  }
  QString type = parms[1].toString();
  if(not type.isEmpty()) {
    if(type == "Normal")
      m->showNormal();
    else if(type == "Heat")
      m->showHeat();
    else if(type == "Label")
      m->showLabel();
    else if(type == "Parents")
      m->showParents();
    else
      warning << QString("Invalid surface type '%1'").arg(type);
  }
  type = parms[2].toString();
  if(not type.isEmpty()) {
    if(type == "Signal")
      m->showSignal();
    else if(type == "Texture")
      m->showTexture();
    else if(type == "Image")
      m->showImage();
    else
      warning << QString("Invalid signal type '%1'").arg(type);
  }
  if(not parms[3].toString().isEmpty())
    m->setBlending(parmToBool(parms[3]));
  if(not parms[4].toString().isEmpty())
    m->setCulling(parmToBool(parms[4]));
  if(not parms[5].toString().isEmpty()) {
    if(parmToBool(parms[5]))
      m->showMesh();
    else
      m->hideMesh();
  }
  type = parms[6].toString();
  if(not type.isEmpty()) {
    if(type == "All")
      m->showAllMesh();
    else if(type == "Border")
      m->showBorderMesh();
    else if(type == "Cells")
      m->showCellMesh();
    else if(type == "Selected")
      m->showSelectedMesh();
    else
      warning << QString("Invalid mesh view '%1'").arg(type);
  }
  if(not parms[7].toString().isEmpty())
    m->setShowMeshLines(parmToBool(parms[7]));
  if(not parms[8].toString().isEmpty())
    m->setShowMeshPoints(parmToBool(parms[8]));
  if(not parms[9].toString().isEmpty())
    m->setShowMeshCellMap(parmToBool(parms[9]));
  if(not parms[10].toString().isEmpty())
    m->setScaled(parmToBool(parms[10]));
  if(not parms[11].toString().isEmpty())
    m->setTransformed(parmToBool(parms[11]));
  if(not parms[12].toString().isEmpty())
    m->setShowBBox(parmToBool(parms[12]));
  if(not parms[13].toString().isEmpty() and parms[13].toFloat() >= 0)
    m->setBrightness(parms[13].toFloat());
  if(not parms[14].toString().isEmpty() and parms[14].toFloat() >= 0)
    m->setOpacity(parms[14].toFloat());
  if(not warning.isEmpty())
    setWarningMessage(warning.join("\n"));
  m->updateTriangles();
  return true;
}

REGISTER_MESH_PROCESS(ViewMeshProcess);

namespace {
void projSignal(const Stack* stack, const HVecUS& data, vertex v, Point3f vnrml,
                float mindist, float maxdist, bool averageSignal)
{
  Point3f vpos = v->pos;
  if(stack->showScale() and norm(stack->scale()) != 1.0) {
    vpos = divide(vpos, stack->scale());
    vnrml = divide(vnrml, stack->scale());
  }
  Point3f pmin = (vpos - vnrml * mindist);
  Point3f pmax = (vpos - vnrml * maxdist);
  // Point3i vi = stack->worldToImagei(vpos);
  Point3i pimin = stack->worldToImagei(pmin);
  Point3i pimax = stack->worldToImagei(pmax);
  int steps = int((pimax - pimin).norm() * 2 + 3);

  float sum = 0, first = 0, last = 0;
  for(int i = 0; i <= steps; i++) {
    Point3i oi = stack->worldToImagei(pmax + vnrml * (maxdist - mindist) * float(i) / float(steps));
    float value = 0.f;
    if(stack->boundsOK(oi.x(), oi.y(), oi.z()))
      value = data[stack->offset(oi.x(), oi.y(), oi.z())];
    if(i == 0) first = value;
    else if(i == steps) last = value;
    sum += value;
  }
  // Trapezoidal integration
  float total = float(2*sum - first - last) / float(2*steps-2);
  if(not averageSignal)
    total *= fabs(maxdist - mindist);
  v->signal = total;
}
} // namespace

bool ProjectSignal::operator()(const Store* store, Mesh* mesh,
                               float minDist, float maxDist,
                               bool averageSignal, bool projectPerCell,
                               bool useMinSignal, bool useMaxSignal,
                               float absSignalMin, float absSignalMax)
{
  const std::vector<vertex>& vs = mesh->activeVertices();
  mesh->setNormals();
  const HVecUS& data = store->data();
  float avgSignal = 0, maxSignal = -FLT_MAX, minSignal = FLT_MAX;
  const Stack* stack = store->stack();

  if(projectPerCell)
    mesh->updateCentersNormals();

  const IntPoint3fMap& labelNormal = mesh->labelNormal();

  const vvgraph& G = mesh->graph();

#pragma omp parallel for reduction(min : minSignal) \
  reduction(max : maxSignal)                        \
  reduction(+ : avgSignal)
  for(size_t i = 0; i < vs.size(); ++i) {
    const vertex& v = vs[i];
    Point3f vnrml = v->nrml;
    if(projectPerCell) {
      if(v->label > 0) {
        IntPoint3fMap::const_iterator found = labelNormal.find(v->label);
        if(found != labelNormal.end()) {
          vnrml = found->second;
        }
      } else if(v->label < 0) {
        Point3f cnrml;
        size_t count = 0;
        for(const vertex& n: G.neighbors(v)) {
          if(n->label > 0) {
            auto found = labelNormal.find(n->label);
            if(found != labelNormal.end()) {
              cnrml += found->second;
              ++count;
            }
          }
        }
        if(count > 0) {
          vnrml = normalized(cnrml);
        }
      }
    }
    projSignal(stack, data, v, vnrml, minDist, maxDist, averageSignal);
    if(v->signal > maxSignal)
      maxSignal = v->signal;
    if(v->signal < minSignal)
      minSignal = v->signal;
    avgSignal += v->signal;
  }

  if(useMinSignal)
    minSignal = absSignalMin;
  if(useMaxSignal)
    maxSignal = absSignalMax;
  if(maxSignal == minSignal)
    maxSignal = minSignal + 1;     // to avoid division by zero

  SETSTATUS("Signal projection, Avg:" << avgSignal / float(vs.size()) << " min:" << minSignal
                                      << " max:" << maxSignal);
  mesh->signalUnit() = "";
  mesh->signalBounds() = Point2f(minSignal, maxSignal);
  mesh->updateTriangles();
  mesh->signalDesc() = Description("Signal") << std::make_pair("Min dist", QString::number(minDist) + " " + UM)
                                             << std::make_pair("Max dist", QString::number(maxDist) + " " + UM);
  return true;
}
REGISTER_MESH_PROCESS(ProjectSignal);

bool SmoothMeshSignal::operator()(Mesh* mesh, uint passes)
{
  vvgraph& S = mesh->graph();
  const std::vector<vertex>& vs = mesh->activeVertices();
  if(vs.empty())
    return true;
  for(uint i = 0; i < passes; i++) {
    forall(const vertex& v, vs) {
      float tarea = 0;
      v->tsignal = 0;
      forall(const vertex& n, S.neighbors(v)) {
        vertex m = S.nextTo(v, n);
        if(!S.edge(m, n))
          continue;
        float a = triangleArea(v->pos, n->pos, m->pos);
        tarea += a;
        v->tsignal += a * (v->signal + n->signal + m->signal) / 3.0;
      }
      if(tarea > 0)
        v->tsignal /= tarea;
      else
        v->tsignal = v->signal;
    }
    forall(const vertex& v, vs)
      v->signal = v->tsignal;
  }
  mesh->updateTriangles();
  return true;
}
REGISTER_MESH_PROCESS(SmoothMeshSignal);

namespace {
struct ProjectCurvatureKernel {
  ProjectCurvatureKernel(const vvgraph& SS, const std::vector<vertex>& vvs, CurvatureType t, float n2,
                         std::vector<Curvature>& cs)
    : S(SS)
    , vs(vvs)
    , type(t)
    , neighborhood_sq(n2)
    , curvs(cs)
  {
  }

  bool operator()(int vi)
  {
    vertex v = vs[vi];
    const Point3f& vpos = v->pos;
    std::unordered_set<vertex> nbs;
    std::vector<vertex> externs;
    nbs.insert(v);
    externs.push_back(v);
    while(not externs.empty()) {
      std::vector<vertex> new_externs;
      forall(const vertex& t, externs)
        forall(const vertex& n, S.neighbors(t)) {
          if(nbs.find(n) == nbs.end() and normsq(n->pos - vpos) < neighborhood_sq) {
            nbs.insert(n);
            new_externs.push_back(n);
          }
        }
      swap(externs, new_externs);
    }
    std::vector<Point3f> plist, nlist;
    plist.push_back(v->pos);
    nlist.push_back(v->nrml);
    forall(const vertex& n, nbs) {
      if(n == v)
        continue;
      plist.push_back(n->pos);
      nlist.push_back(n->nrml);
    }
    if(plist.size() < 3)
      return false;
    if(!curvs.empty())
      v->signal = computeCurv(curvs[vi], plist, nlist);
    else {
      Curvature curv(type);
      v->signal = computeCurv(curv, plist, nlist);
    }
    return true;
  }

  float computeCurv(Curvature& curv, const std::vector<Point3f>& points, const std::vector<Point3f>& normals)
  {
    curv.update(points, normals);
    return curv.get_curvature();
  }

  const vvgraph& S;
  const std::vector<vertex>& vs;
  CurvatureType type;
  float neighborhood_sq;
  std::vector<Curvature>& curvs;
};
} // namespace

bool ProjectCurvature::operator()(Mesh* mesh, QString output, QString typeStr, float neighborhood, bool auto_scale,
                                  float mincurv, float maxcurv, int percentile)
{
  vvgraph& S = mesh->graph();
  const std::vector<vertex>& vs = mesh->activeVertices();
  if(vs.empty())
    return true;
  bool ok;
  CurvatureType type = readCurvatureType(typeStr, &ok);
  if(not ok)
    return setErrorMessage(QString("Error, unknown curvature type: '%1'").arg(typeStr));

  if((type == CT_SUMSQUARE or type == CT_ANISOTROPY) and mincurv < 0)
    mincurv = 0;
  if(!auto_scale and mincurv >= maxcurv)
    return setErrorMessage("Error, min curvature >= max curvature");
  std::vector<Curvature> curvatures(output.isEmpty() ? 0 : vs.size());

  Progress progress(QString("project curvature on Mesh %1").arg(mesh->userId()), vs.size());

  float n2 = neighborhood * neighborhood;

  {
    ProjectCurvatureKernel kernel(S, vs, type, n2, curvatures);
    if(not apply_kernel_it(kernel, Counter(0), Counter(vs.size()), progress))
      return setErrorMessage(
        "Error while computing curvature. Maybe the neighborhood is too small, or you have isolated vertices?");
  }

  RescaleSignal rescale(*this);
  rescale(mesh, true, (auto_scale ? percentile : 0), mincurv, maxcurv);

  if(!output.isEmpty()) {
    QFile f(output);
    if(f.open(QIODevice::WriteOnly)) {
      QTextStream ts(&f);
      ts << QString("X,Y,Z,X1,Label,Y1,Z1,X2,Y2,Z2,k1 (1/%1),k2 (1/%1)\n").arg(UM);
      size_t i = 0;
      forall(const vertex& v, S) {
        const Point3f& pos = v->pos;
        Curvature& curv = curvatures[i];
        Point3f e1, e2;
        float c1, c2;
        curv.get_eigenVectors(e1, e2);
        curv.get_eigenValues(c1, c2);
        QStringList fields;
        fields << QString::number(pos.x(), 'g', 10) << QString::number(pos.y(), 'g', 10)
               << QString::number(pos.z(), 'g', 10) << QString::number(v->label)
               << QString::number(e1.x(), 'g', 10) << QString::number(e1.y(), 'g', 10)
               << QString::number(e1.z(), 'g', 10) << QString::number(e2.x(), 'g', 10)
               << QString::number(e2.y(), 'g', 10) << QString::number(e2.z(), 'g', 10)
               << QString::number(c1, 'g', 10) << QString::number(c2, 'g', 10);
        ts << fields.join(",") << "\n";
        ++i;
      }
    } else
      setWarningMessage(QString("Warning, could not open file '%1' for writing. Not output written").arg(output));
  }

  if(type == CT_GAUSSIAN or type == CT_SUMSQUARE)
    mesh->signalUnit() = QString::fromWCharArray(L"\xb5m\x207B\xb2");
  else if(type == CT_ANISOTROPY)
    mesh->signalUnit() = "";
  else
    mesh->signalUnit() = QString::fromWCharArray(L"\xb5m\x207B\xb9");

  mesh->showNormal();
  mesh->showSignal();
  mesh->updateTriangles();
  mesh->signalDesc() = Description("Curvature") << std::make_pair("Measure", toString(type))
                                                << std::make_pair("Radius", QString::number(neighborhood) + " " + UM);
  return true;
}
REGISTER_MESH_PROCESS(ProjectCurvature);

static const float sqrt_2_pi = sqrt(2 * M_PI);

MeshGaussianBlurOp::MeshGaussianBlurOp(const vvgraph& _S, float _radius)
  : S(_S)
  , radius(_radius)
{
}

bool MeshGaussianBlurOp::operator()(const vertex& v)
{
  const float radius_sq = radius * radius;
  float ar = 0, wt = 0;
  VtxSet vset = Mesh::getNeighborhood(S, v, 2 * radius);   // Neighbourhood processing

  // Gaussian blur over the cell
  forall(const vertex& r, vset) {
    if(norm(r->pos - v->pos) < radius) {
      float g = exp(-normsq(r->pos - v->pos) / (2 * radius_sq)) / (radius * sqrt_2_pi);
      ar += g * r->signal;
      wt += g;
    }
  }
  v->tsignal = ar / wt;
  return true;
}

bool MeshGaussianBlur::operator()(Mesh* mesh, float radius)
{
  vvgraph& S = mesh->graph();
  const std::vector<vertex>& vs = mesh->activeVertices();

  Progress progress(QString("Gaussian Blur on Mesh %1").arg(mesh->userId()), vs.size());
  MeshGaussianBlurOp op(S, radius);
  apply_kernel(op, vs, progress);

  if(!progress.advance(vs.size()))
    userCancel();

  mesh->finalizeTSignal();
  mesh->updateTriangles();
  return true;
}
REGISTER_MESH_PROCESS(MeshGaussianBlur);

MeshLocalMinimaOp::MeshLocalMinimaOp(const vvgraph& _S, const std::vector<vertex>& _vs, float _radius, int _startLabel)
  : S(_S)
  , vs(_vs)
  , radius(_radius)
  , label(_startLabel)
{
}

bool MeshLocalMinimaOp::operator()(int i)
{
  const vertex& v = vs[i];
  VtxSet vset = Mesh::getNeighborhood(S, v, radius);   // Neighbourhood processing

  // Finding the lowest v->signal in the set of vertices respective to a cell
  float lowest = HUGE_VAL;
  forall(const vertex& r, vset)
    if(r->signal < lowest)
      lowest = r->signal;

  // Labelling the cell at the local minima
  if(v->signal == lowest) {
    v->label = label + i;

    forall(const vertex& n, S.neighbors(v))
      n->label = v->label;
  }
  return true;
}

bool MeshLocalMinima::operator()(Mesh* mesh, float radius)
{
  vvgraph& S = mesh->graph();
  const std::vector<vertex>& vs = mesh->activeVertices();

  Progress progress(QString("Finding Local Minima for Mesh %1").arg(mesh->userId()), vs.size());

  int startLabel = mesh->viewLabel();
  int lastLabel = startLabel;
  MeshLocalMinimaOp op(S, vs, radius, startLabel);
  apply_kernel_it(op, Counter(0), Counter(vs.size()), progress);
  if(!progress.advance(vs.size()))
    userCancel();

  mesh->setLabel(lastLabel);

  mesh->showLabel();
  mesh->updateTriangles();
  return true;
}
REGISTER_MESH_PROCESS(MeshLocalMinima);

MeshNormalizeOp::MeshNormalizeOp(const vvgraph& _S, float _radius)
  : S(_S)
  , radius(_radius)
{
}

bool MeshNormalizeOp::operator()(const vertex& v)
{
  float smin = v->signal;
  float smax = smin;
  VtxSet vset = Mesh::getNeighborhood(S, v, radius);

  if(vset.size() < 2)
    Information::out << "Warning, vset.size() = " << vset.size() << endl;

  // Normalizing the signal of mesh
  forall(const vertex& n, vset) {
    if(n->signal < smin)
      smin = n->signal;
    if(n->signal > smax)
      smax = n->signal;
  }
  if(smin == smax)
    v->tsignal = 1;
  else
    v->tsignal = (v->signal - smin) / (smax - smin);
  return true;
}

bool MeshNormalize::operator()(Mesh* mesh, float radius)
{
  vvgraph& S = mesh->graph();
  const std::vector<vertex>& vs = mesh->activeVertices();
  Progress progress(QString("Normalizing Signal on Mesh %1").arg(mesh->userId()), vs.size());
  MeshNormalizeOp op(S, radius);
  apply_kernel(op, vs, progress);

  if(!progress.advance(vs.size()))
    userCancel();

  mesh->finalizeTSignal();

  mesh->signalBounds() = Point2f(0, 1);
  mesh->updateTriangles();
  return true;
}
REGISTER_MESH_PROCESS(MeshNormalize);

MeshDilationOp::MeshDilationOp(const vvgraph& _S, float _radius)
  : S(_S)
  , radius(_radius)
{
}

bool MeshDilationOp::operator()(const vertex& v)
{
  float smax = -FLT_MAX;
  VtxSet vset = Mesh::getNeighborhood(S, v, radius);

  // Dilation
  forall(const vertex& n, vset)
    if(smax < n->signal)
      smax = n->signal;

  v->tsignal = smax;
  return true;
};

bool MeshDilation::operator()(Mesh* mesh, float radius)
{
  vvgraph& S = mesh->graph();
  const std::vector<vertex>& vs = mesh->activeVertices();
  Progress progress(QString("Dilating Signal on Mesh %1").arg(mesh->userId()), vs.size());
  MeshDilationOp op(S, radius);

  apply_kernel(op, vs, progress);

  if(!progress.advance(vs.size()))
    userCancel();

  mesh->finalizeTSignal();

  mesh->updateTriangles();
  return true;
}
REGISTER_MESH_PROCESS(MeshDilation);

MeshErosionOp::MeshErosionOp(const vvgraph& _S, float _radius)
  : S(_S)
  , radius(_radius)
{
}

bool MeshErosionOp::operator()(const vertex& v)
{
  float smin = FLT_MAX;
  VtxSet vset = Mesh::getNeighborhood(S, v, radius);

  // Erosion
  forall(const vertex& n, vset)
    if(smin > n->signal)
      smin = n->signal;

  v->tsignal = smin;
  return true;
};

bool MeshErosion::operator()(Mesh* mesh, float radius)
{
  vvgraph& S = mesh->graph();
  const std::vector<vertex>& vs = mesh->activeVertices();
  Progress progress(QString("Eroding Signal on Mesh %1").arg(mesh->userId()), vs.size());

  MeshErosionOp op(S, radius);

  apply_kernel(op, vs, progress);

  if(!progress.advance(vs.size()))
    userCancel();

  mesh->finalizeTSignal();

  mesh->updateTriangles();
  return true;
}
REGISTER_MESH_PROCESS(MeshErosion);

bool MeshClosing::operator()(Mesh* mesh, float radius)
{
  vvgraph& S = mesh->graph();
  const std::vector<vertex>& vs = mesh->activeVertices();
  Progress progress(QString("Closing Signal on Mesh %1").arg(mesh->userId()), 2 * vs.size());
  {
    MeshDilationOp opD(S, radius);

    apply_kernel(opD, vs, progress);
  }

  if(!progress.advance(vs.size()))
    userCancel();

  mesh->finalizeTSignal();

  {
    MeshErosionOp opE(S, radius);

    apply_kernel(opE, vs, progress, vs.size());
  }

  if(!progress.advance(2 * vs.size()))
    userCancel();

  mesh->finalizeTSignal();

  mesh->updateTriangles();
  return true;
}
REGISTER_MESH_PROCESS(MeshClosing);

bool MeshOpening::operator()(Mesh* mesh, float radius)
{
  vvgraph& S = mesh->graph();
  const std::vector<vertex>& vs = mesh->activeVertices();
  Progress progress(QString("Opening Signal on Mesh %1").arg(mesh->userId()), 2 * vs.size());
  {
    MeshErosionOp opE(S, radius);

    apply_kernel(opE, vs, progress);
  }

  if(!progress.advance(vs.size()))
    userCancel();

  mesh->finalizeTSignal();

  {
    MeshDilationOp opD(S, radius);

    apply_kernel(opD, vs, progress, vs.size());
  }

  if(!progress.advance(2 * vs.size()))
    userCancel();

  mesh->finalizeTSignal();

  mesh->updateTriangles();
  return true;
}
REGISTER_MESH_PROCESS(MeshOpening);

bool RescaleSignal::operator()(Mesh* mesh, bool use_zero, float percentile, float minimum, float maximum)
{
  if(percentile > 0) {
    vvgraph& S = mesh->graph();
    std::vector<float> values(S.size());
    int i = 0;
    forall(const vertex& v, S)
      values[i++] = v->signal;
    std::sort(values.begin(), values.end());

    int rankmax = values.size() - 1;
    int rankmin = 0;
    if(percentile < 100) {
      rankmax = (values.size() * percentile) / 100;
      rankmin = values.size() - rankmax;
    }
    minimum = values[rankmin];
    maximum = values[rankmax];
    if(use_zero) {
      if(minimum * maximum < 0) {
        maximum = std::max(fabs(maximum), fabs(minimum));
        minimum = -maximum;
      } else if(maximum > 0)
        minimum = 0;
      else if(minimum < 0)
        maximum = 0;
      else {
        minimum = 0;
        maximum = 1;
      }
    }
  }
  mesh->signalBounds() = Point2f(minimum, maximum);
  return true;
}

REGISTER_MESH_PROCESS(RescaleSignal);

bool SetSignalProcess::operator()(Mesh* m, float value, bool rescale, float percentile, bool use_zero)
{
  if(rescale and (percentile <= 0 or percentile > 100))
    return setErrorMessage("Percentile must be greater than 0 and less or equal to 100.");
  const std::vector<vertex>& vs = m->activeVertices();
  forall(const vertex& v, vs)
    v->signal = value;
  if(rescale) {
    RescaleSignal rescale(*this);
    rescale(m, use_zero, percentile, 0, 0);
  }
  m->updateTriangles();
  return true;
}

REGISTER_MESH_PROCESS(SetSignalProcess);

bool ClearMeshSignal::operator()(Mesh* mesh, uint value)
{
    const auto& vs = mesh->activeVertices();
#pragma omp parallel for
    for(size_t i = 0 ; i < vs.size() ; ++i) {
        vs[i]->signal = value;
    }
    mesh->updateTriangles();
    return true;
}

REGISTER_MESH_PROCESS(ClearMeshSignal);

} // namespace process
} // namespace lgx
