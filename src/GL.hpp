/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef GL_HPP
#define GL_HPP

#include <LGXConfig.hpp>

#include <QOpenGLFunctions_3_2_Compatibility>
namespace lgx {
typedef LGX_EXPORT QOpenGLFunctions_3_2_Compatibility OpenGLFunctions;
extern LGX_EXPORT OpenGLFunctions* opengl;
}
#include <GL/glu.h>

#endif
