/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef VERSION_H
#define VERSION_H

#include <Version.hpp>

#define VERSION_MAJOR ${LithoGraphX_VERSION_MAJOR}
#define VERSION_MINOR ${LithoGraphX_VERSION_MINOR}
#define VERSION_PATCH ${LithoGraphX_VERSION_PATCH}

#define VERSION "${LithoGraphX_VERSION}"

#define PROCESS_VERSION ${LithoGraphX_PROCESS_VERSION_HEX}

#define LithoGraphX_OS "${LithoGraphX_OS}${LithoGraphX_OS_RELEASE}${LithoGraphX_CUDA}"

#ifdef major
#  undef major
#endif

#ifdef minor
#  undef minor
#endif

extern LGX_EXPORT lgx::util::Version lgxVersion;

#endif // VERSION_H


