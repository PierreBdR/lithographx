/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef STACKINFODLG_HPP
#define STACKINFODLG_HPP

#include <ui_StackInfoDlg.h>

class QAbstractButton;
class QShowEvent;

namespace lgx {
class ImgData;

namespace gui {

class StackInfoDlg : public QDialog {
  Q_OBJECT
public:
  StackInfoDlg(ImgData* data, QWidget* parent = 0, Qt::WindowFlags f = 0);
  virtual ~StackInfoDlg();

public slots:
  void updateData();
  void updateMesh();
  void updateStack();
  void updateOther();

protected:
  void showEvent(QShowEvent* event);

protected slots:
  void on_buttonBox_clicked(QAbstractButton* btn);

protected:
  Ui::StackInfoDlg ui;
  ImgData* data;
};
} // namespace gui
} // namespace lgx
#endif // STACKINFODLG_HPP
