# Try to find the Eigen 3 library
#
# Once run this will define:
#
# Eigen_FOUND
#
# Eigen_INCLUDE_DIR = where to find headers
#
# Eigen_VERSION = version of the eigen library

find_file(Eigen_SIGNATURE signature_of_eigen3_matrix_library PATH_SUFFIXES eigen3
  DOC "Location of the Eigen 3 signature file name 'signature_of_eigen3_matrix_library'")

macro(_FAILURE_MESSAGE _msg)
  if (Eigen_FIND_REQUIRED)
    message(FATAL_ERROR "${_msg}")
  else ()
    if (NOT Eigen_FIND_QUIETLY)
      message(STATUS "${_msg}")
    endif ()
  endif ()
endmacro()

if(Eigen_SIGNATURE)
  get_filename_component(Eigen_INCLUDE_DIR ${Eigen_SIGNATURE} DIRECTORY)
  set(TEST_EIGEN_FILE "#include <Eigen/Core>
#include <iostream>

int main()
{
    std::cout << EIGEN_WORLD_VERSION << \".\"
              << EIGEN_MAJOR_VERSION << \".\"
              << EIGEN_MINOR_VERSION;
    return 0;
}")
  file(WRITE ${CMAKE_CURRENT_BINARY_DIR}/test_eigen.cpp "${TEST_EIGEN_FILE}")
  try_run(Eigen_VERSION_RUN Eigen_VERSION_COMPILE
    ${CMAKE_CURRENT_BINARY_DIR}/test_eigen ${CMAKE_CURRENT_BINARY_DIR}/test_eigen.cpp
    COMPILE_OUTPUT_VARIABLE Eigen_COMPILE_OUTPUT
    CMAKE_FLAGS -DINCLUDE_DIRECTORIES=${Eigen_INCLUDE_DIR}
    RUN_OUTPUT_VARIABLE Eigen_VERSION)
  set(VERSION_OK TRUE)
  if(NOT Eigen_VERSION_COMPILE)
    _FAILURE_MESSAGE("FindEigen error: Couldn't compile eigen test:\n${Eigen_COMPILE_OUTPUT}")
  elseif(Eigen_VERSION_RUN EQUAL 0)
    if(Eigen_FIND_VERSION)
      if(Eigen_FIND_VERSION_EXACT)
        if(NOT ${Eigen_FIND_VERSION} VERSION_EQUAL ${Eigen_VERSION})
          set(VERSION_OK FALSE)
        endif()
      else()
        if(${Eigen_FIND_VERSION} VERSION_GREATER ${Eigen_VERSION})
          set(VERSION_OK FALSE)
        endif()
      endif()
    endif()
    set(Eigen_FOUND TRUE)
  else()
    _FAILURE_MESSAGE("FineEigen error: Couldn't run compiled eigen test: Exit code = ${Eigen_VERSION_RUN} -- Output:\n${Eigen_VERSION}")
  endif()
else()
    set(Eigen_FOUND FALSE)
endif()

if(NOT VERSION_OK)
  _FAILURE_MESSAGE("FindEigen error: version expected ${Eigen_FIND_VERSION}, version found ${Eigen_VERSION}")
elseif(NOT Eigen_FOUND)
  _FAILURE_MESSAGE("Couldn't find the Eigen library")
endif()

mark_as_advanced(Eigen_FOUND Eigen_INCLUDE_DIR)
