/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "PluginManager.hpp"

#include "Defer.hpp"
#include "Library.hpp"
#include "Mangling.hpp"
#include "Misc.hpp"
#include "PrivateProcessUtils.hpp"
#include "QAlgorithms.hpp"
#include "SystemDirs.hpp"
#include "SystemProcess.hpp"
#include "Utility.hpp"

#include <algorithm>
#include <unordered_set>

#include <QStringList>
#include <QSettings>

namespace lgx {
namespace process {

namespace {
DummyProcess dummyProcess;

PluginManager* singleton = nullptr;

struct HashQString {
  size_t operator()(const QString& p) const {
    return size_t(qHash(p));
  }
};

QString pluginFullName(const QString& type, const QString& name) {
  return type + ":" + name;
}

struct MacroLanguage {
  QString name;
  bool used;

  macro_loader_t loader;
  macro_unloader_t unloader;
  macro_finalizer_t finalizer;
};

struct ProcessAlias {
  Plugin* plugin;    // The plugin containing the process
  QString registrationName; // The registration class name of the process
};

} // namespace

struct PluginPrivate
{
  QString name;
  QString type;
  QString errorMessage;
  bool valid = true;
  bool used = true;

  std::unordered_map<QString, std::unique_ptr<stackProcessDefinition>> stackProcesses;
  std::unordered_map<QString, std::unique_ptr<meshProcessDefinition>> meshProcesses;
  std::unordered_map<QString, std::unique_ptr<globalProcessDefinition>> globalProcesses;
  std::unordered_map<QString, MacroLanguage> macroLanguages;

  template <typename P>
  std::unordered_map<QString, std::unique_ptr<ProcessDefinition<P>>>& processes();
  template <typename P>
  const std::unordered_map<QString, std::unique_ptr<ProcessDefinition<P>>>& processes() const;

  template <typename P>
  void configureProcesses();
  template <typename P>
  void configureProcess(std::unique_ptr<ProcessDefinition<P>>& registrationName);
};

template <>
std::unordered_map<QString, std::unique_ptr<ProcessDefinition<StackProcess>>>&
PluginPrivate::processes<StackProcess>()
{
  return this->stackProcesses;
}
template <>
const std::unordered_map<QString, std::unique_ptr<ProcessDefinition<StackProcess>>>&
PluginPrivate::processes<StackProcess>() const
{
  return this->stackProcesses;
}

template <>
std::unordered_map<QString, std::unique_ptr<ProcessDefinition<MeshProcess>>>&
PluginPrivate::processes<MeshProcess>()
{
  return this->meshProcesses;
}
template <>
const std::unordered_map<QString, std::unique_ptr<ProcessDefinition<MeshProcess>>>&
PluginPrivate::processes<MeshProcess>() const
{
  return this->meshProcesses;
}

template <>
std::unordered_map<QString, std::unique_ptr<ProcessDefinition<GlobalProcess>>>&
PluginPrivate::processes<GlobalProcess>()
{
  return this->globalProcesses;
}
template <>
const std::unordered_map<QString, std::unique_ptr<ProcessDefinition<GlobalProcess>>>&
PluginPrivate::processes<GlobalProcess>() const
{
  return this->globalProcesses;
}

template <typename P>
void PluginPrivate::configureProcesses()
{
  auto& procs = this->processes<P>();
  for(auto& p : procs) {
    this->configureProcess(p.second);
  }
}

template <typename P>
void PluginPrivate::configureProcess(std::unique_ptr<ProcessDefinition<P>>& def)
{
  auto proc = (*def->factory)(dummyProcess);
  def->name = proc->name();
  def->description = proc->description();
  def->parmNames = proc->parmNames();
  def->parmDescs = proc->parmDescs();
  def->parms = proc->parmDefaults();
  def->folder = proc->folder();
  def->icon = proc->icon();
  def->parmChoice = proc->parmChoice();
  DEBUG_OUTPUT("Configured [" << typeName<P>() << "]" << def->name << endl);
}
// Main plugin methods
Plugin::Plugin(QString type, QString name)
  : p(std::make_unique<PluginPrivate>())
{
  p->type = type;
  p->name = name;
}

Plugin::~Plugin() { }

QString Plugin::name() const {
  return p->name;
}

QString Plugin::type() const {
  return p->type;
}

QString Plugin::fullName() const {
  return pluginFullName(p->type, p->name);
}

bool Plugin::valid() const {
  return p->valid;
}

bool Plugin::used() const {
  return p->used;
}

bool Plugin::isSystem() const {
  return type() == "LGXSystem";
}

void Plugin::use(bool on) {
  if(not isSystem() and p->valid) {
    p->used = on;
  }
}

void Plugin::validate() {
  p->valid = true;
  p->errorMessage = QString();

  p->configureProcesses<StackProcess>();
  p->configureProcesses<MeshProcess>();
  p->configureProcesses<GlobalProcess>();
}

void Plugin::error(QString message) {
  p->valid = false;
  p->used = false;
  p->errorMessage = message;
}

QString Plugin::errorMessage() const {
  return p->errorMessage;
}

bool Plugin::loaded() const {
  return not (p->macroLanguages.empty() and
              p->processes<StackProcess>().empty() and
              p->processes<MeshProcess>().empty() and
              p->processes<GlobalProcess>().empty());
}


bool Plugin::loadMacro(QString name) {
  auto found = p->macroLanguages.find(name);
  if(found == p->macroLanguages.end())
    return false;
  auto& macro = found->second;
  if(not macro.loader)
    return false;
  return macro.loader();
}

bool Plugin::unloadMacro(QString name) {
  auto found = p->macroLanguages.find(name);
  if(found == p->macroLanguages.end())
    return false;
  auto& macro = found->second;
  if(not macro.unloader)
    return false;
  return macro.unloader();
}

QStringList Plugin::macroLanguages() const {
  QStringList names;
  for(const auto& ms: p->macroLanguages)
    names << ms.first;
  return names;
}

size_t Plugin::nbMacroLanguages() const {
  return p->macroLanguages.size();
}

template <typename P>
size_t Plugin::nbProcesses() const
{
  return p->processes<P>().size();
}

size_t Plugin::nbProcesses() const
{
  return nbProcesses<StackProcess>() + nbProcesses<MeshProcess>() + nbProcesses<GlobalProcess>();
}

template <typename P>
QStringList Plugin::processes() const
{
  QStringList names;
  for(const auto& pr: p->processes<P>()) {
    names << pr.first;
  }
  names.sort(Qt::CaseInsensitive);
  return names;
}

struct PluginManagerPrivate {
  Plugin* currentPlugin = nullptr;

  std::unordered_map<QString, std::shared_ptr<Plugin>> plugins;
  std::vector<Library> libraries;

  // Ordered list of plugins
  QStringList pluginsLoadingOrder;

  // List of system plugins
  QStringList systemPlugins;

  // in which plugin will we find ...
  std::unordered_map<QString, ProcessAlias> stackProcesses;
  std::unordered_map<QString, ProcessAlias> meshProcesses;
  std::unordered_map<QString, ProcessAlias> globalProcesses;
  std::unordered_map<QString, Plugin*> macroLanguages;

  // true when we are loading macros or extensions
  bool loading = false;

  // when true, the extensions have been loaded and cannot be loaded anymore
  bool extensionsLoaded = false;

  template <typename P>
  std::unordered_map<QString, ProcessAlias>& processes();
  template <typename P>
  const std::unordered_map<QString, ProcessAlias>& processes() const;
};

template <>
std::unordered_map<QString, ProcessAlias>& PluginManagerPrivate::processes<StackProcess>()
{
  return this->stackProcesses;
}
template <>
const std::unordered_map<QString, ProcessAlias>& PluginManagerPrivate::processes<StackProcess>() const
{
  return this->stackProcesses;
}

template <>
std::unordered_map<QString, ProcessAlias>& PluginManagerPrivate::processes<MeshProcess>()
{
  return this->meshProcesses;
}
template <>
const std::unordered_map<QString, ProcessAlias>& PluginManagerPrivate::processes<MeshProcess>() const
{
  return this->meshProcesses;
}

template <>
std::unordered_map<QString, ProcessAlias>& PluginManagerPrivate::processes<GlobalProcess>()
{
  return this->globalProcesses;
}
template <>
const std::unordered_map<QString, ProcessAlias>& PluginManagerPrivate::processes<GlobalProcess>() const
{
  return this->globalProcesses;
}

namespace {
template <typename P>
void cleanProcesses(Plugin* plugin, PluginManagerPrivate* p) {
  auto& processes = p->processes<P>();
  for(auto& process : plugin->processes<P>()) {
    auto found = processes.find(process);
    if(found != processes.end() and found->second.plugin == plugin)
      processes.erase(found);
  }
}

void cleanMacroLaguages(Plugin* plugin, PluginManagerPrivate* p) {
  for(auto& macro: plugin->macroLanguages()) {
    auto found = p->macroLanguages.find(macro);
    if(found != p->macroLanguages.end() and found->second == plugin)
      p->macroLanguages.erase(found);
  }
}

void cleanPlugin(Plugin* plugin, PluginManagerPrivate* p) {
  // un-register processes, but keep them
  cleanProcesses<StackProcess>(plugin, p);
  cleanProcesses<MeshProcess>(plugin, p);
  cleanProcesses<GlobalProcess>(plugin, p);

  // remove macro languages too
  cleanMacroLaguages(plugin, p);
}

template <typename P>
bool refreshModuleProcesses(PluginManagerPrivate* p, Plugin* plugin) {
  const auto& procs = plugin->processes<P>();
  auto& glob_procs = p->processes<P>();
  for(const auto& p : procs) {
    const auto& def = plugin->processDefinition<P>(p);
    if(glob_procs.find(def.name) == glob_procs.end()) {
      glob_procs[def.name] = { plugin, p };
      DEBUG_OUTPUT("Adding [" << typeName<P>() << "]" << def.name << " <-- " << p << endl);
    }
  }
  return true;
}

template <typename P>
bool hasProcessName(const QString& name, const std::unique_ptr<PluginManagerPrivate>& p)
{
  const auto& glob_proc = p->processes<P>();
  auto found_plugin = glob_proc.find(name);
  return found_plugin != glob_proc.end();
}

template <typename P>
std::unique_ptr<P> buildProcess(const QString& name, const P* proc, const PluginManagerPrivate* p)
{
  const auto& glob_proc = p->processes<P>();
  auto glob_found = glob_proc.find(name);
  if(glob_found == glob_proc.end())
    throw NoSuchProcess(proc->type(), name);
  auto registrationName = glob_found->second.registrationName;
  const auto& def = glob_found->second.plugin->template processDefinition<P>(registrationName);
  return (*def.factory)(*proc);
}

} // namespace

PluginManager* PluginManager::instance()
{
  /*
   *if(not singleton)
   *  singleton = new PluginManager();
   */
  return singleton;
}

PluginManager::PluginManager()
  : p(new PluginManagerPrivate())
{
  p->systemPlugins << pluginFullName("LGXSystem", "LithoGraphX");
}

bool PluginManager::allocate()
{
  if(singleton) return false;
  singleton = new PluginManager();
  return true;
}

bool PluginManager::deallocate()
{
  if(not singleton) return false;
  delete singleton;
  return true;
}

PluginManager::~PluginManager()
{
  // Check all plugins have been un-loaded
  if(not p->plugins.empty()) {
    Information::err << "Error, " << p->plugins.size() << " plugins left loaded.\n" << endl;
    for(const auto& pl: p->plugins) {
      Information::err << "Plugin: " << pl.second->name() << " has "
                       << pl.second->nbProcesses<StackProcess>()  << " stack processes, "
                       << pl.second->nbProcesses<MeshProcess>()   << " mesh processes and "
                       << pl.second->nbProcesses<GlobalProcess>() << " global processes still registered." << endl;
    }
  } else
    Information::err << "PluginManager: all plugins unloaded!" << endl;
}

bool PluginManager::initialize()
{
  loadSettings();
  Information::out << "Found " << p->plugins.size() << " plugins in QSettings" << endl;
  if(not loadExtensions())
    return false;
  if(not loadMacros())
    return false;
  cleanPluginList();
  return refreshProcesses();
}

bool PluginManager::finalize()
{
  saveSettings();
  if(not unloadMacros())
    return false;
  if(not unloadExtensions())
    return false;
  return true;
}

void PluginManager::loadSettings()
{
  QSettings settings;
  settings.beginGroup("PluginManager");

  QStringList plugins;
  int nbPlugins = settings.beginReadArray("Plugins");
  for(int i = 0 ; i < nbPlugins ; ++i) {
    settings.setArrayIndex(i);
    plugins << settings.value("FullName").toString();
  }
  settings.endArray();

  settings.beginGroup("Plugins");
  for(const QString& pn: plugins) {
    settings.beginGroup(util::toPercentEncoding(pn));
    auto pluginType = settings.value("Type").toString();
    auto pluginName = settings.value("Name").toString();
    auto isUsed = settings.value("Used").toString().toLower();
    bool used = (isUsed == "error" or isUsed == "yes");
    auto found = p->plugins.find(pn);
    if(found == p->plugins.end()) {
      auto fields = pn.split(":");
      found = p->plugins.insert({pn, std::make_unique<Plugin>(pluginType, pluginName)}).first;
    }
    auto& plug = found->second;
    plug->use(used);

    QStringList macros;
    int nbMacros = settings.beginReadArray("MacroLanguages");
    for(int i = 0 ; i < nbMacros ; ++i) {
      settings.setArrayIndex(i);
      macros << settings.value("Name").toString();
    }
    settings.endArray();
    settings.endGroup();
  }
  settings.endGroup();

  {
    int nbLoadingLibs = settings.beginReadArray("PluginsLoadingOrder");
    QStringList pluginsOrder = p->systemPlugins;
    pluginsOrder.reserve(nbLoadingLibs);
    for(int i = 0 ; i < nbLoadingLibs ; ++i) {
      settings.setArrayIndex(i);
      pluginsOrder << settings.value("Name").toString();
    }
    settings.endArray();
    pluginsOrder.removeDuplicates();
    p->pluginsLoadingOrder = std::move(pluginsOrder);
  }

  settings.endGroup();
}

void PluginManager::saveSettings()
{
  QSettings settings;
  settings.beginGroup("PluginManager");
  // Save the plugin configurations

  int nbPlugins = p->plugins.size();
  {
    settings.beginWriteArray("Plugins", nbPlugins);
    int i = 0;
    for(const auto& pl: p->plugins) {
      settings.setArrayIndex(i++);
      settings.setValue("FullName", pl.first);
    }
    settings.endArray();
  }

  // Create sections for plugins
  settings.beginGroup("Plugins");
  for(const auto& pl: p->plugins) {
    const auto& plug = pl.second;
    settings.beginGroup(util::toPercentEncoding(pl.first));
    if(plug->used())
      settings.setValue("Used", "yes");
    else if(plug->valid())
      settings.setValue("Used", "no");
    else
      settings.setValue("Used", "error");
    settings.setValue("Name", plug->name());
    settings.setValue("Type", plug->type());
    auto macros = plug->macroLanguages();
    settings.beginWriteArray("MacroLanguages", macros.size());
    for(size_t i = 0 ; i < macros.size() ; ++i) {
      settings.setArrayIndex(i);
      settings.setValue("Name", macros.at(i));
    }
    settings.endArray();
    settings.endGroup();
  }
  settings.endGroup();

  int nbLoadingLibs = p->pluginsLoadingOrder.size();
  settings.beginWriteArray("PluginsLoadingOrder", nbLoadingLibs);
  for(int i = 0 ; i < nbLoadingLibs ; ++i) {
    settings.setArrayIndex(i);
    settings.setValue("Name", p->pluginsLoadingOrder.at(i));
  }
  settings.endArray();
  settings.endGroup();
}

bool PluginManager::unloadExtension(const QString& dllPath)
{
  // Find the library in the list
  for(auto it = p->libraries.begin() ;
      it != p->libraries.end() ; ++it) {
    auto& lib = *it;
    if(lib.fileName() == dllPath) {
      bool success = true;
      if(lib.isLoaded()) {
        auto plugName = pluginFullName("Library", dllPath);
        auto plug_found = p->plugins.find(plugName);
        if(plug_found != p->plugins.end()) {
          auto& plugin = plug_found->second;
          // Deconnect all processes
          auto& stackProcs = p->processes<StackProcess>();
          for(const auto& proc: plugin->processes<StackProcess>()) {
            auto proc_found = stackProcs.find(proc);
            if(proc_found != stackProcs.end() and proc_found->second.plugin == plugin.get()) {
              stackProcs.erase(proc_found);
            }
          }
          auto& meshProcs = p->processes<MeshProcess>();
          for(const auto& proc: plugin->processes<MeshProcess>()) {
            auto proc_found = meshProcs.find(proc);
            if(proc_found != meshProcs.end() and proc_found->second.plugin == plugin.get()) {
              meshProcs.erase(proc_found);
            }
          }
          auto& globalProcs = p->processes<GlobalProcess>();
          for(const auto& proc: plugin->processes<GlobalProcess>()) {
            auto proc_found = globalProcs.find(proc);
            if(proc_found != globalProcs.end() and proc_found->second.plugin == plugin.get()) {
              globalProcs.erase(proc_found);
            }
          }
          // Remove macro languages
          for(const auto& macro: plugin->macroLanguages()) {
            plugin->finalizeMacroLanguage(macro);
          }
        }
        // At last, try to unload
        success = lib.unload();
      }
      if(success) {
        p->libraries.erase(it);
      } else {
        Information::err << "Error unloading DLL '" << lib.fileName() << "': " << lib.errorMessage() << endl;
      }
      return success;
    }
  }
  // if it wasn't loaded: instant success!
  return true;
}

bool PluginManager::loadExtensions()
{
  if(p->extensionsLoaded or p->loading)
    return false;

  {
    p->loading = true;
    auto stopLoading = util::defer([this]() { this->p->loading = false; });
    Information::out << "Register System Process" << endl;
    auto sys = LoadingPlugin("LGXSystem", "LithoGraphX");
    registerSystemProcesses();
    sys.validate();
  }

  if(not loadNewExtensions())
    return false;
  p->extensionsLoaded = true;
  return true;
}

bool PluginManager::loadNewExtensions()
{
  if(p->loading)
    return false;
  p->loading = true;
  auto stopLoading = util::defer([this]() { this->p->loading = false; });

  // Set of libraries already loaded
  std::unordered_set<QString> loaded_libs;
  for(const auto& lib: p->libraries)
    loaded_libs.insert(lib.fileName());

  QList<QDir> dirs = util::processesDirs();
  forall(const QDir& dir, dirs) {
    Information::out << "\nLoading libraries from '" << dir.absolutePath() << "'" << endl;
    QStringList files = dir.entryList(QDir::Files);
    files.sort();
    for(const QString& f: files) {
      QString lib_path = dir.absoluteFilePath(f);
      if(Library::isLibrary(lib_path) and loaded_libs.find(lib_path) == loaded_libs.end()) {
        loaded_libs.insert(lib_path);
        auto loader = LoadingPlugin("Library", lib_path);
        Library lib(lib_path);
        lib.load();
        auto& plug = p->plugins.at(pluginFullName("Library", lib_path));
        if(lib.isLoaded()) {
          loader.validate();
          Information::out << "Loaded library " << f << endl;
          Information::out << "Number of processes loaded (stack,mesh,global) = ("
            << plug->nbProcesses<StackProcess>()  << ","
            << plug->nbProcesses<MeshProcess>() << ","
            << plug->nbProcesses<GlobalProcess>() << ")" << endl;
          p->libraries.push_back(lib);
        } else {
          auto msg = util::qdemangle(QString("Failed to load library file %1:\n%2").arg(f).arg(lib.errorMessage()));
          SETSTATUS(msg);
          loader.fail(msg);
        }
      }
    }
  }

  p->loading = false;

  return true;
}

bool PluginManager::unloadExtensions()
{
  if(not p->extensionsLoaded or p->loading)
    return false;

  QStringList toDelete;

  QStringList dllFiles;
  for(Library& lib: p->libraries)
    dllFiles << lib.fileName();
  for(const auto& dll: dllFiles) {
    Information::err << "Unloading library '" << dll << "'" << endl;
    toDelete << dll;
    if(not unloadExtension(dll)) {
      Information::err << "Problem unloading library " << dll << endl;
    }
  }

  for(auto n: toDelete) {
    auto fullName = pluginFullName("Library", n);
    auto found = p->plugins.find(fullName);
    if(found != p->plugins.end()) {
      const auto& pl = found->second;
      if(pl->nbProcesses<StackProcess>() > 0 or
         pl->nbProcesses<MeshProcess>() > 0 or
         pl->nbProcesses<GlobalProcess>()) {
        Information::err << "Warning, processes left after trying to unload '" << fullName << "': ("
                         << pl->nbProcesses<StackProcess>() << ", " << pl->nbProcesses<MeshProcess>() << ", "
                         << pl->nbProcesses<GlobalProcess>()<< ")"  << endl;
      } else
        p->plugins.erase(found);
    } else
      Information::err << "Cannot find plugin " << fullName << endl;
  }

  {
    unregisterSystemProcesses();

    auto found = p->plugins.find("LGXSystem:LithoGraphX");
    if(found == p->plugins.end())
      Information::err << "Error, cannot find system plugins" << endl;
    else {
      auto& pl = found->second;
      if(pl->nbProcesses<StackProcess>() > 0 or
         pl->nbProcesses<MeshProcess>() > 0 or
         pl->nbProcesses<GlobalProcess>()) {
        Information::err << "Warning, processes left after unregistering system processes: ("
                         << pl->nbProcesses<StackProcess>() << ", " << pl->nbProcesses<MeshProcess>() << ", "
                         << pl->nbProcesses<GlobalProcess>()<< ")"  << endl;
      }
      cleanPlugin(pl.get(), p.get());
      p->plugins.erase(found);
    }
  }

  return true;
}

bool PluginManager::refreshAll()
{
  saveSettings();
  if(not unloadMacros())
    return false;
  loadSettings();
  if(not loadMacros())
    return false;
  return refreshProcesses();
}

bool PluginManager::loadMacros()
{
  if(p->loading or not p->extensionsLoaded) return false;
  p->loading = true;
  auto stopLoading = util::defer([this]() { this->p->loading = false; });

  if(not unloadMacros()) return false;

  // Establish the list of macro languages
  p->macroLanguages.clear();

  // Find macro languages
  for(auto& ps : p->plugins) {
    auto& plugin = ps.second;
    if(plugin->used()) {
      for(auto& ms : plugin->macroLanguages()) {
        if(p->macroLanguages.find(ms) == p->macroLanguages.end())
          p->macroLanguages[ms] = plugin.get();
      }
    }
  }

  bool ok = true;

  for(auto& ms : p->macroLanguages) {
    if(not ms.second->loadMacro(ms.first)) {
      ok = false;
      Information::err << "Error loading macro language " << ms.first << endl;
    }
  }

  return ok;
}

bool PluginManager::unloadMacros()
{
  bool ok = true;
  // Unload existing macros
  for(const auto& ms: p->macroLanguages) {
    auto plug = ms.second;
    if(not plug->unloadMacro(ms.first)) {
      ok = false;
      Information::err << "Error, couldn't unload macro '" << ms.first << "'" << endl;
    }
  }
  return ok;
}

bool PluginManager::refreshProcesses()
{
  if(p->loading or not p->extensionsLoaded) return false;

  p->stackProcesses.clear();
  p->meshProcesses.clear();
  p->globalProcesses.clear();

  /*
   *Information::out << "Plugins loading order:\n";
   *for(int i = 0 ; i < p->pluginsLoadingOrder.size() ; ++i)
   *  Information::out << "  " << (i+1) << " -> " << p->pluginsLoadingOrder.at(i) << "\n";
   */

  for(const QString& pluginName : p->pluginsLoadingOrder) {
    Plugin* plugin = p->plugins.at(pluginName).get();
    if(plugin->used()) {
      bool ok = true;

      ok &= refreshModuleProcesses<StackProcess>(p.get(), plugin);
      ok &= refreshModuleProcesses<MeshProcess>(p.get(), plugin);
      ok &= refreshModuleProcesses<GlobalProcess>(p.get(), plugin);

      if(not ok) {
        Information::err << "Error, cannot extract processes from plugin '" << pluginName << "'" << endl;
        return false;
      }
    }
  }

  emit processesChanged();
  return true;
}

QString PluginManager::registerMacroLanguage(QString name,
                                             macro_loader_t loader,
                                             macro_unloader_t unloader,
                                             macro_finalizer_t finalizer)
{
  if(not p->loading or not p->currentPlugin or name == "Library")
    return QString();
  if(p->currentPlugin->registerMacroLanguage(name, loader, unloader, finalizer))
    return p->currentPlugin->name();
  return QString();
}

bool Plugin::registerMacroLanguage(QString name,
                                   macro_loader_t loader,
                                   macro_unloader_t unloader,
                                   macro_finalizer_t finalizer)
{
  auto found = p->macroLanguages.find(name);
  if(p->macroLanguages.find(name) == p->macroLanguages.end()) {
    found = p->macroLanguages.insert({name, MacroLanguage()}).first;
  } else if(bool(found->second.loader) or bool(found->second.unloader)) {
    Information::err << "Cannot define two macros languages with the same name in the same plugin" << endl;
    return false;
  }
  auto& macro = found->second;
  macro.name = name;
  macro.loader = loader;
  macro.unloader = unloader;
  macro.finalizer = finalizer;
  return true;
}

bool Plugin::createMacroLanguage(QString name)
{
  if(p->macroLanguages.find(name) != p->macroLanguages.end())
    return false;
  auto macro = MacroLanguage{};
  macro.name = name;
  p->macroLanguages[name] = macro;
  return true;
}

bool PluginManager::unregisterMacroLanguage(QString plugin, QString name)
{
  auto found_plug = p->plugins.find("Library:" + plugin);
  if(found_plug == p->plugins.end()) {
    Information::err << "Error, cannot find plugin '" << plugin << "'" << endl;
    return false;
  }
  if(found_plug->second->unregisterMacroLanguage(name)) {
    auto glob_found = p->macroLanguages.find(name);
    if(glob_found != p->macroLanguages.end() and glob_found->second == found_plug->second.get())
      p->macroLanguages.erase(glob_found);
    return true;
  }
  return false;
}

bool Plugin::unregisterMacroLanguage(QString name)
{
  auto found = p->macroLanguages.find(name);
  if(found == p->macroLanguages.end()) return false;
  const MacroLanguage& macro = found->second;
  macro.unloader(); // unload all processes
  p->macroLanguages.erase(found);
  return true;
}

bool Plugin::finalizeMacroLanguage(QString name)
{
  auto found = p->macroLanguages.find(name);
  if(found == p->macroLanguages.end()) return false;
  const MacroLanguage& macro = found->second;
  macro.unloader();
  macro.finalizer();
  p->macroLanguages.erase(found);
  return true;
}

template <typename P>
QString PluginManager::registerProcess(QString registrationName, FactoryPointer<P> factory)
{
  if(not p->loading or not p->currentPlugin)
    return QString();
  if(p->currentPlugin->registerProcess(registrationName, factory))
    return p->currentPlugin->fullName();
  return QString();
}

template <typename P>
bool Plugin::registerProcess(QString registrationName, FactoryPointer<P> factory)
{
  auto& procs = p->processes<P>();
  if(procs.find(registrationName) != procs.end()) {
    Information::out << "Error, plugin '" << p->name << "' register class " << registrationName << " more than once" << endl;
    return false;
  }

  //auto proc = (*factory)(dummyProcess);
  auto def = std::make_unique<ProcessDefinition<P>>();
  def->plugin = fullName();
  def->factory = factory;
  procs[registrationName] = std::move(def);
  return true;
}


template <typename P>
QString PluginManager::unregisterProcess(QString plugin, QString registrationName, FactoryPointer<P> factory)
{
  auto found_plug = p->plugins.find(plugin);
  if(found_plug == p->plugins.end()) {
    Information::out << "Cannot unregister process from unknown plugin '" << plugin << "'" << endl;
    return QString();
  }
  QString name = found_plug->second->unregisterProcess(registrationName, factory);
  if(name.isEmpty())
    return name;
  auto& globProcs = p->processes<P>();
  auto found_glob = globProcs.find(name);
  if(found_glob != globProcs.end() and found_glob->second.plugin == p->currentPlugin)
    globProcs.erase(found_glob);
  return name;
}

template <typename P>
QString Plugin::unregisterProcess(QString registrationName, FactoryPointer<P> factory)
{
  auto& procs = p->processes<P>();
  auto found = procs.find(registrationName);
  if(found == procs.end()) {
    Information::out << "Cannot unregister unknown process '" << registrationName << "'" << endl;
    return QString();
  }
  if(found->second->factory == factory) {
    auto name = found->second->name;
    procs.erase(found);
    return name;
  }
  Information::out << "Error factory function do not correspond when unregistering process '" << registrationName << "'" << endl;
  return QString();
}

QString PluginManager::pluginType(QString pluginPath) const
{
  auto found = p->plugins.find(pluginPath);
  if(found != p->plugins.end())
    return found->second->type();
  return QString();
}

bool PluginManager::loadingPlugin(QString pluginType, QString pluginPath)
{
  if(p->loading and not p->currentPlugin) {
    QString fullName = pluginFullName(pluginType, pluginPath);
    auto found = p->plugins.find(fullName);
    if(found == p->plugins.end()) {
      found = p->plugins.insert({fullName, std::make_unique<Plugin>(pluginType, pluginPath)}).first;
    }
    auto& plugin = found->second;
    if(plugin->isSystem()) {
      if(not p->systemPlugins.contains(fullName))
        p->systemPlugins << fullName;
    }
    if(plugin->loaded()) {
      Information::err << "Error, trying to load again plugin '" << fullName << "'" << endl;
      return false;
    }
    p->currentPlugin = plugin.get();
    if(not p->pluginsLoadingOrder.contains(fullName))
      p->pluginsLoadingOrder << fullName;
    return true;
  }
  return false;
}

bool PluginManager::pluginLoaded(QString fullName)
{
  if(p->loading and p->currentPlugin and p->currentPlugin->fullName() == fullName) {
    if(not p->currentPlugin->valid())
      cleanPlugin(p->currentPlugin, p.get());
    p->currentPlugin = nullptr;
    return true;
  }
  return false;
}

template <typename P>
QStringList PluginManager::processes() const
{
  QStringList processes;
  for(const auto& pr: p->processes<P>())
    processes << pr.first;
  processes.sort(Qt::CaseInsensitive);
  return processes;
}

template <typename P>
size_t PluginManager::nbProcesses() const
{
  return p->processes<P>().size();
}

template <typename P>
size_t PluginManager::nbRegisteredProcesses() const
{
  size_t total = 0;
  for(const auto& pl: p->plugins)
    total += pl.second->nbProcesses<P>();
  return total;
}

LoadingPlugin::LoadingPlugin(QString pluginType, QString pluginName)
  : _fullName(pluginFullName(pluginType, pluginName))
{
  auto manager = pluginManager();
  _success = manager->loadingPlugin(pluginType, pluginName);
  if(_success)
    _plugin = manager->plugin(_fullName);
}

QString LoadingPlugin::fullName() const
{
  return _fullName;
}

LoadingPlugin::~LoadingPlugin()
{
  if(_success)
    pluginManager()->pluginLoaded(_fullName);
}

void LoadingPlugin::validate()
{
  if(_success)
    _plugin->validate();
}

void LoadingPlugin::fail(QString message)
{
  if(_success)
    _plugin->error(message);
}

QStringList PluginManager::pluginsName() const
{
  QStringList names;
  for(const auto& pl: p->plugins)
    names << pl.first;
  return names;
}

std::shared_ptr<const Plugin> PluginManager::plugin(QString fullName) const
{
  auto found = p->plugins.find(fullName);
  if(found == p->plugins.end())
    return {};
  return found->second;
}

std::shared_ptr<Plugin> PluginManager::plugin(QString fullName)
{
  auto found = p->plugins.find(fullName);
  if(found == p->plugins.end())
    return {};
  return found->second;
}

BaseProcessDefinition& PluginManager::baseProcessDefinition(const QString& type, const QString& name)
{
  if(type == "Stack")
    return processDefinition<StackProcess>(name);
  else if(type == "Mesh")
    return processDefinition<MeshProcess>(name);
  else if(type == "Global")
    return processDefinition<GlobalProcess>(name);
  throw NoSuchProcess(type, name);
}

bool PluginManager::hasProcess(const QString& type, const QString& name) const
{
  if(type == "Stack")
    return hasProcessName<StackProcess>(name, p);
  else if(type == "Mesh")
    return hasProcessName<MeshProcess>(name, p);
  else if(type == "Global")
    return hasProcessName<GlobalProcess>(name, p);
  return false;
}

template <typename P>
bool PluginManager::hasProcess(const QString& processName) const
{
  return hasProcessName<P>(processName, p);
}

std::unique_ptr<Process> PluginManager::makeProcess(const QString& processType, const QString& processName,
                                                    const Process* proc) const
{
  if(processType == "Stack") {
    const StackProcess* sp = dynamic_cast<const StackProcess*>(proc);
    if(sp)
      return buildProcess(processName, sp, p.get());
  } else if(processType == "Mesh") {
    const MeshProcess* sp = dynamic_cast<const MeshProcess*>(proc);
    if(sp)
      return buildProcess(processName, sp, p.get());
  } else if(processType == "Global") {
    const GlobalProcess* sp = dynamic_cast<const GlobalProcess*>(proc);
    if(sp)
      return buildProcess(processName, sp, p.get());
  }
  throw BadProcessType(proc->type(), processType);
}

QStringList PluginManager::pluginsLoadingOrder() const
{
  return p->pluginsLoadingOrder;
}

void PluginManager::setPluginsLoadingOrder(QStringList order)
{
  QStringList fullOrder = p->systemPlugins + order;
  p->pluginsLoadingOrder = util::sortLike(p->pluginsLoadingOrder, fullOrder);
}

bool PluginManager::movePluginLoadingOrder(QString fullName, int dst)
{
  int idx = p->pluginsLoadingOrder.indexOf(fullName);
  if(idx < 0)
    return false; // Couldn't find plugin
  if(dst == idx or dst == idx+1)
    return true;
  auto startPlugin = p->plugins.at(fullName);
  if(startPlugin->isSystem())
    return false;
  if(idx < dst)
    dst--;
  p->pluginsLoadingOrder.removeAt(idx);
  if(dst >= p->pluginsLoadingOrder.size())
    p->pluginsLoadingOrder << fullName;
  else {
    auto replacing = p->plugins.at(p->pluginsLoadingOrder.at(dst));
    if(replacing->isSystem())
      return false;
    p->pluginsLoadingOrder.insert(dst, fullName);
  }
  return true;
}

template <typename P>
const ProcessDefinition<P>& Plugin::processDefinition(const QString& registrationName) const
{
  auto& proc = p->processes<P>();
  auto found = proc.find(registrationName);
  if(found == proc.end())
    throw NoSuchProcessInPlugin(p->name, typeName<P>(), registrationName);
  return *found->second;
}

template <typename P>
const ProcessDefinition<P>& PluginManager::processDefinition(const QString& processName) const
{
  auto& glob_proc = p->processes<P>();
  auto found_plugin = glob_proc.find(processName);
  if(found_plugin == glob_proc.end())
    throw NoSuchProcess(typeName<P>(), processName);
  return found_plugin->second.plugin->template processDefinition<P>(found_plugin->second.registrationName);
}

template <typename P>
ProcessDefinition<P>& Plugin::processDefinition(const QString& name)
{
  auto& proc = p->processes<P>();
  auto found = proc.find(name);
  if(found == proc.end())
    throw NoSuchProcess(typeName<P>(), name);
  return *found->second;
}

template <typename P>
ProcessDefinition<P>& PluginManager::processDefinition(const QString& name)
{
  auto& glob_proc = p->processes<P>();
  auto found_plugin = glob_proc.find(name);
  if(found_plugin == glob_proc.end())
    throw NoSuchProcess(typeName<P>(), name);
  return found_plugin->second.plugin->template processDefinition<P>(found_plugin->second.registrationName);
}

void PluginManager::cleanPluginList()
{
  QStringList toRemove;
  QStringList newPluginOrder;

  for(const auto& s : p->pluginsLoadingOrder) {
    auto plug = p->plugins.at(s);
    if(plug->loaded() or not plug->errorMessage().isEmpty())
      newPluginOrder << s;
    else
      toRemove << s;
  }
  for(const auto& s : toRemove)
    p->plugins.erase(s);

  p->pluginsLoadingOrder = newPluginOrder;
  saveSettings();
}

template
ProcessDefinition<StackProcess>& Plugin::processDefinition<StackProcess>(const QString& processName);
template
ProcessDefinition<MeshProcess>& Plugin::processDefinition<MeshProcess>(const QString& processName);
template
ProcessDefinition<GlobalProcess>& Plugin::processDefinition<GlobalProcess>(const QString& processName);

template
const ProcessDefinition<StackProcess>& Plugin::processDefinition<StackProcess>(const QString& processName) const;
template
const ProcessDefinition<MeshProcess>& Plugin::processDefinition<MeshProcess>(const QString& processName) const;
template
const ProcessDefinition<GlobalProcess>& Plugin::processDefinition<GlobalProcess>(const QString& processName) const;

template
ProcessDefinition<StackProcess>& PluginManager::processDefinition<StackProcess>(const QString& processName);
template
ProcessDefinition<MeshProcess>& PluginManager::processDefinition<MeshProcess>(const QString& processName);
template
ProcessDefinition<GlobalProcess>& PluginManager::processDefinition<GlobalProcess>(const QString& processName);

template
const ProcessDefinition<StackProcess>& PluginManager::processDefinition<StackProcess>(const QString& processName) const;
template
const ProcessDefinition<MeshProcess>& PluginManager::processDefinition<MeshProcess>(const QString& processName) const;
template
const ProcessDefinition<GlobalProcess>& PluginManager::processDefinition<GlobalProcess>(const QString& processName) const;

template
QString PluginManager::registerProcess<StackProcess>(QString, FactoryPointer<StackProcess>);
template
QString PluginManager::registerProcess<MeshProcess>(QString, FactoryPointer<MeshProcess>);
template
QString PluginManager::registerProcess<GlobalProcess>(QString, FactoryPointer<GlobalProcess>);

template
bool Plugin::registerProcess<StackProcess>(QString, FactoryPointer<StackProcess>);
template
bool Plugin::registerProcess<MeshProcess>(QString, FactoryPointer<MeshProcess>);
template
bool Plugin::registerProcess<GlobalProcess>(QString, FactoryPointer<GlobalProcess>);

template
QString PluginManager::unregisterProcess<StackProcess>(QString, QString, FactoryPointer<StackProcess>);
template
QString PluginManager::unregisterProcess<MeshProcess>(QString, QString, FactoryPointer<MeshProcess>);
template
QString PluginManager::unregisterProcess<GlobalProcess>(QString, QString, FactoryPointer<GlobalProcess>);

template
QString Plugin::unregisterProcess<StackProcess>(QString, FactoryPointer<StackProcess>);
template
QString Plugin::unregisterProcess<MeshProcess>(QString, FactoryPointer<MeshProcess>);
template
QString Plugin::unregisterProcess<GlobalProcess>(QString, FactoryPointer<GlobalProcess>);

template
QStringList PluginManager::processes<StackProcess>() const;
template
QStringList PluginManager::processes<MeshProcess>() const;
template
QStringList PluginManager::processes<GlobalProcess>() const;

template
QStringList Plugin::processes<StackProcess>() const;
template
QStringList Plugin::processes<MeshProcess>() const;
template
QStringList Plugin::processes<GlobalProcess>() const;

template size_t PluginManager::nbProcesses<StackProcess>() const;
template size_t PluginManager::nbProcesses<MeshProcess>() const;
template size_t PluginManager::nbProcesses<GlobalProcess>() const;

template bool PluginManager::hasProcess<StackProcess>(const QString&) const;
template bool PluginManager::hasProcess<MeshProcess>(const QString&) const;
template bool PluginManager::hasProcess<GlobalProcess>(const QString&) const;

template size_t PluginManager::nbRegisteredProcesses<StackProcess>() const;
template size_t PluginManager::nbRegisteredProcesses<MeshProcess>() const;
template size_t PluginManager::nbRegisteredProcesses<GlobalProcess>() const;

template size_t Plugin::nbProcesses<StackProcess>() const;
template size_t Plugin::nbProcesses<MeshProcess>() const;
template size_t Plugin::nbProcesses<GlobalProcess>() const;

} // namespace process
} // namespace lgx
