/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef TIE_HPP
#define TIE_HPP
/**
 * \file Tie.hpp
 *
 * Defines the util::tie function
 */

#include <LGXConfig.hpp>

#include <utility>

namespace lgx {
namespace util {

// Code taken from the Boost library !
/**
 * \class refpair Tie.hpp <Tie.hpp>
 *
 * Class used to hold references for the util::tie() function.
 */
template <typename T, typename U> struct refpair {
  typedef T first_type;
  typedef U second_type;

  /// Construct a pair of references to \c x and \c y.
  refpair(T& x, U& y)
    : first(x)
    , second(y)
  {
  }
  /// Construct a copy.
  refpair(refpair const& rp)
    : first(rp.first)
    , second(rp.second)
  {
  }

  /// Assign the values of \c p to the references in this pair.
  refpair& operator=(std::pair<T, U> const& p)
  {
    first = p.first;
    second = p.second;
    return *this;
  }

  /// The first member of the pair.
  T& first;
  /// The second member of the pair.
  U& second;
};

/**
 * Tie two variables to the values of a pair.
 *
 * Example:
 * \code
 * std::pair<int,double> p(1,2.5);
 * int a;
 * double b;
 * util::tie(a,b) = p;
 * \endcode
 *
 * At the end, \c a is \c 1 and \c b is \c 2.5
 */
template <typename T, typename U> inline refpair<T, U> tie(T& x, U& y) {
  return refpair<T, U>(x, y);
}
} // namespace util
} // namespace lgx
#endif
