#ifndef VTKPROGRESS_HPP
#define VTKPROGRESS_HPP

#include <VTKConfig.hpp>
#include <Progress.hpp>

class vtkCallbackCommand;
class vtkAlgorithm;

namespace lgx {
namespace process {
class vtkutil_EXPORT VTKProgress {
public:
  VTKProgress(const QString& text);
  ~VTKProgress();

  void setFilter(vtkAlgorithm* filter);

  Progress progress;
  vtkCallbackCommand* progressHandler, *startHandler, *endHandler;
};
}
}

#endif // VTKPROGRESS_HPP
