#ifndef CellCurvature_HPP
#define CellCurvature_HPP

#include <Process.hpp>

namespace process
{
class CellCurvature : public MeshProcess
{
public:
    CellCurvature(const MeshProcess& process)
        : Process(process)
        , MeshProcess(process)
    { }
    
    bool operator()(const QStringList& strings, const QList<float>& values)
    {
        if(!checkState().mesh(MESH_CELLS | MESH_NON_EMPTY))
            return false;
        Mesh *m = currentMesh();
        QString filename = strings[0];
        bool use_center = stringToBool(strings[1]);
        bool use_contours = stringToBool(strings[2]);
        bool show_tensors = stringToBool(strings[3]);
        int neighborhood = int(values[0]);
        if(neighborhood < 1)
            throw QString("Error, neighborhood argument must be 1 or more.");
        return operator()(m, filename, use_center, use_contours, show_tensors, neighborhood,
                          values[1], QColor(strings[4]), QColor(strings[5]));
    }
    
    bool operator()(Mesh *mesh, const QString& filename, bool use_center, bool use_contours, bool show_tensors, int neighborhood,
                    float tensor_scale, const QColor& pos, const QColor& neg);
    
    QString folder() const { return "Cell Mesh"; }
    QString name() const { return "Cell Curvature"; }
    QString description() const { return "Compute the curvature of 2D cells"; }
    QStringList parameters() const { return QStringList() << "Output" << "Use centers" << "Use contours" << "Show tensors" << "Color positive" << "Color negative" << "Neighborhood" << "Tensor scale"; }
    QStringList default_strings() const { return QStringList() << "output.csv" << "Yes" << "Yes" << "Yes" << "white" << "red"; }
    StringChoiceMap string_choice() const {
        StringChoiceMap map;
        map[1] = boolean_choice();
        map[2] = boolean_choice();
        map[3] = boolean_choice();
        return map;
    }
    QList<float> default_values() const { return QList<float>() << 1 << 1; }
    QIcon icon() const
    {
        return QIcon(":/images/CellCurvature.png");
    }
    
};
}

#endif // CellCurvature_HPP


