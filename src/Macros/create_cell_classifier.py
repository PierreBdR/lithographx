# -*- coding:utf-8 -*-
from __future__ import print_function, division
from lgxPy import parmToBool
import threading
from collections import namedtuple
import sys
import codecs
from lgxUtils.path import Path
from distutils.version import LooseVersion

from PyQt5.QtWidgets import QFileDialog, QDialog, QListWidgetItem, QAbstractItemView, QDialogButtonBox
from PyQt5.uic import loadUi
from PyQt5.QtCore import pyqtSlot, pyqtSignal, Qt, QItemSelectionModel, QItemSelection, QTimer
from PyQt5.QtGui import QColor, QPalette
from cell_classifier.utils import processFilePattern, isValidFilePattern, printable, correctFormat
from cell_classifier.find_files_model import FindFilesModel

if sys.platform == 'win32':
    n_jobs = 1
else:
    n_jobs = -1

try:
    import pandas as pd
    import numpy as np
    import sklearn
    from sklearn import decomposition, pipeline, metrics, grid_search, preprocessing, externals
    from scipy import stats
except ImportError:
    raise RuntimeError("""The following modules need to be installed:
- scikit-learn 0.17 or later (available from http://scikit-learn.org)
- numpy (available from http://numpy.org)
- scipy (available from http://scipy.org)""")

processType = "Global"
processName = "Create Cell Classifier"
processFolder = "Cell Classifier"
processIcon = ":/images/CellFeatures.png"
parmsDescs = dict(Folder=u"Folder containing all the files.",
                  Main_Pattern=u"""Pattern used to find the mail files.
By default, the base name of the file will be the file name without its extension.
If a pair of brackets is used, this defines which part of the file name should be
used to generate other file names""",
                  Recursive=u"If true, files will be also searched in sub-folders",
                  Cell_Type_Range=u"Min and max values for the cell types",
                  Feature_Suffix="Suffix to add to the base name of the main file.",
                  Classifier_Filename=u"Name of the file in which the classifier will be saved.",
                  Methods="""Method(s) used for the classification.
This can be either a list of method names separated by comas, a single method name or 'All'.""",
                  Parameter_Search='''Method used to search parameters for each method:
- None: default parameters are used
- Grid: search parameters on a full grid with n points per dimension
- Randomized: Sample n points randomly''',
                  KFold='''How many folds to use for the parameter search''',
                  NbPoints='How many points to sample either in total or per dimension.',
                  Summary='If specified, a summary of the machine learning process will be printed in this file.',
                  Validation_Set='Percentage of the data set to keep to test the various methods and select the better one.',
                  Test_Set='Percentage of the data set to keep to assess the algorithm at the end.')
parmsChoice = dict(Recursive=["Yes", "No"],
                   Parameter_Search=['None', 'Grid', 'Randomized'],
                   Methods=['Support Vector Machine', 'Random Forest', 'All'])


def confusion_matrix(real_cls, predict_cls):
    classes = np.unique(np.concatenate([real_cls, predict_cls]))
    n = len(classes)
    result = np.zeros((n, n), dtype=int)
    for i in range(n):
        for j in range(n):
            result[i][j] = np.sum((real_cls == classes[i]) & (predict_cls == classes[j]))
    res = pd.DataFrame(result, index=classes, columns=classes)
    res.index.name = "Real"
    res.columns.name = "Predicted"
    res['Sum'] = res.sum(axis=1)
    res.loc['Sum'] = res.sum()
    return res

ClassifierMethod = namedtuple('ClassifierMethod', ('name', 'builder', 'parameters', 'parmsDist', 'parmsRange'))


def linearRange(low, high):
    def makeRange(nbPoints):
        return np.linspace(low, high, nbPoints, endpoint=True)
    return makeRange


def logRange(low, high):
    def makeRange(nbPoints):
        return np.logspace(low, high, nbPoints, endpoint=True)
    return makeRange


def SVMMethod():
    from sklearn import svm
    name = "Support Vector Machine"

    class_weigths = [None]
    if LooseVersion(sklearn.__version__) >= '0.17':
        class_weigths += ['balanced']
    else:
        class_weigths += ['auto']

    def builder():
        return svm.SVC()
    parms = ['class_weight', 'C', 'gamma']
    parms_dist = dict(class_weight=class_weigths,
                      C=stats.expon(scale=100),
                      gamma=stats.expon(scale=.2))
    parms_range = dict(class_weight=lambda x: class_weigths,
                       C=logRange(-1, np.log10(300)),
                       gamma=logRange(-3, 0))
    print("Creating SVMMethod")
    return ClassifierMethod(name, builder, parms, parms_dist, parms_range)


def RandomForestMethod():
    from sklearn import ensemble
    name = "Random Forest"

    class_weigths = [None]
    if LooseVersion(sklearn.__version__) >= '0.17':
        class_weigths += ['balanced', 'balanced_subsample']
    else:
        class_weigths += ['auto', 'subsample']

    def builder():
        return ensemble.RandomForestClassifier(max_features='auto')
    parms = ['n_estimators', 'class_weight', 'criterion']
    parms_dist = dict(class_weight=class_weigths,
                      n_estimators=stats.poisson(200),
                      criterion=['gini', 'entropy'])
    parms_range = dict(class_weight=lambda x: class_weigths,
                       n_estimators=linearRange(0, 400),
                       criterion=lambda x: ['gini', 'entropy'])
    print("Creating RandomForestMethod")
    return ClassifierMethod(name, builder, parms, parms_dist, parms_range)


methodBuilder = [SVMMethod, RandomForestMethod]


def methodsAvailable():
    methods = []
    for builder in methodBuilder:
        try:
            m = builder()
            methods.append(m)
        except Exception as ex:
            print("Warning, error creating builder: ", printable(ex))
    return methods


class CreateClassifierDlg(QDialog):

    preventValidation = pyqtSignal(bool)
    allowValidation = pyqtSignal(bool)

    def __init__(self, parent):
        QDialog.__init__(self, parent)
        self._initialized = False
        pth = Path(__file__).dirname() / 'CreateClassifier.ui'
        print(repr(pth))
        self.ui = loadUi(pth, self)
        bbox = self.ui.buttonBox
        okBtn = bbox.button(bbox.Ok)
        self.preventValidation.connect(okBtn.setDisabled)
        self.allowValidation.connect(okBtn.setEnabled)
        pal = self.ui.mainFilePattern.palette()
        self.goodBack = pal.color(self.ui.mainFilePattern.backgroundRole())
        self.badBack = QColor(Qt.red).lighter()
        self.warnBack = QColor(255, 128, 0).lighter()

        methods = methodsAvailable()
        lstMethods = self.ui.listMethods
        lstMethods.clear()
        lstMethods.setSelectionMode(QAbstractItemView.MultiSelection)
        for m in methods:
            item = QListWidgetItem(m.name, lstMethods)
            item.setFlags(Qt.ItemIsSelectable | Qt.ItemIsEnabled)
        lstMethods.sortItems()

        model = FindFilesModel(self.ui.baseFolder.text(), ('Main Files', self.ui.mainFilePattern.text()),
                               [('Features Files', self.ui.featureFileSuffix.text(), "mandatory")],
                               False, self)
        model.setObjectName("filesModel")
        model.modelReset.connect(self.on_filesModel_modelReset)
        btn = self.ui.buttonBox.addButton("Refresh files", QDialogButtonBox.ActionRole)
        btn.clicked.connect(self.refreshFiles)
        self._model = model
        self.ui.filesView.setModel(model)
        self.resetTimer = QTimer()
        self.resetTimer.setInterval(10)
        self.resetTimer.setSingleShot(True)
        self.resetTimer.timeout.connect(self.resizeColumns)

        self.resetTimer.start()

    @pyqtSlot()
    def on_filesModel_modelReset(self):
        self.resetTimer.start()

    @pyqtSlot()
    def resizeColumns(self):
        n = self._model.columnCount()
        for i in range(n):
            self.ui.filesView.resizeColumnToContents(i)
        h = self.ui.filesView.header()
        h.stretchLastSection()

    @pyqtSlot()
    def refreshFiles(self):
        self._model.findFiles()

    def exec_(self):
        self._initialized = True
        return QDialog.exec_(self)

    def checkValues(self):
        if self.checkMainPattern() and self.checkMethods():
            self.allowValidation.emit(True)
            return True
        else:
            self.preventValidation.emit(True)
            return False

    def checkMainPattern(self):
        """
        Check if the main pattern is valid
        """
        return isValidFilePattern(self.ui.mainFilePattern.text())

    def checkMethods(self):
        """
        Check if at least one method is selected
        """
        lstMethods = self.ui.listMethods
        for i in range(lstMethods.count()):
            item = lstMethods.item(i)
            if item.isSelected():
                return True
        return False

    @pyqtSlot(str)
    def on_baseFolder_textChanged(self, txt):
        p = Path(txt)
        if p.isdir():
            self._model.baseFolder = p

    @pyqtSlot(bool)
    def on_findRecursive_toggled(self, on):
        self._model.recursive = on

    def setRecursive(self, on):
        self._model.recursive = on
        self.ui.findRecursive.setChecked(on)

    @pyqtSlot(str)
    def on_mainFilePattern_textChanged(self, txt):
        if not isValidFilePattern(txt):
            self.setError(self.ui.mainFilePattern)
            del self._model.mainFilePattern
            self.preventValidation.emit(True)
        else:
            self.setError(self.ui.mainFilePattern, False)
            self._model.mainFilePattern = txt
            self.checkValues()

    def setError(self, item, err=True):
        pal = item.palette()
        pal.setColor(QPalette.Base, self.badBack if err else self.goodBack)
        item.setPalette(pal)

    def setWarning(self, item, err=True):
        pal = item.palette()
        pal.setColor(QPalette.Base, self.warnBack if err else self.goodBack)
        item.setPalette(pal)

    @pyqtSlot(str)
    def on_featureFileSuffix_textChanged(self, txt):
        self._model.setColumnFormat(1, txt)

    @pyqtSlot()
    def on_chooseFolder_clicked(self):
        newFolder = self.ui.baseFolder.text()
        newFolder = QFileDialog.getExistingDirectory(self, "Choose Base Folder", newFolder)
        if newFolder:
            self.ui.baseFolder.setText(newFolder)

    @pyqtSlot()
    def on_chooseClassifier_clicked(self):
        baseDir = Path(self.ui.baseFolder.text())
        newFile = self.ui.classifierFilename.text()
        newFile = QFileDialog.getSaveFileName(self, "Choose file to save the classifier.",
                                              newFile, "Pickle Files (*.pkl);;All Files (*.*)")
        newFile = newFile[0]
        if newFile:
            if not newFile.endswith('.pkl'):
                newFile += '.pkl'
            newFile = Path(newFile).relpath(baseDir)
            self.ui.classifierFilename.setText(newFile)

    @pyqtSlot(str)
    def on_classifierFilename_textChanged(self, txt):
        pth = self._model.baseFolder / txt
        if pth.isfile():
            self.setWarning(self.ui.classifierFilename)
            self.ui.classifierFilename.setToolTip("File exists.")
        else:
            self.setWarning(self.ui.classifierFilename, False)
            self.ui.classifierFilename.setToolTip("File doesn't exist!")

    @pyqtSlot()
    def on_chooseSummary_clicked(self):
        baseDir = Path(self.ui.baseFolder.text())
        newFile = self.ui.summaryOutput.text()
        newFile = QFileDialog.getSaveFileName(self, "Choose file to save the summary.", newFile,
                                              "Text Files (*.txt);;All Files (*.*)")
        newFile = newFile[0]
        if newFile:
            if not newFile.endswith('.txt'):
                newFile += '.txt'
            newFile = Path(newFile).relpath(baseDir)
            self.ui.summaryOutput.setText(newFile)

    @pyqtSlot(str)
    def on_summaryOutput_textChanged(self, txt):
        pth = self._model.baseFolder / txt
        if pth.isfile():
            self.setWarning(self.ui.summaryOutput)
            self.ui.summaryOutput.setToolTip("File exists.")
        else:
            self.setWarning(self.ui.summaryOutput, False)
            self.ui.summaryOutput.setToolTip("File doesn't exist!")

    @pyqtSlot(int)
    def on_cellTypeMin_valueChanged(self, val):
        self.ui.cellTypeMax.setMinimum(val+1)

    @pyqtSlot()
    def on_listMethods_itemSelectionChanged(self):
        if self.checkMethods():
            self.setError(self.ui.listMethods.viewport(), False)
            self.checkValues()
        else:
            self.setError(self.ui.listMethods.viewport())
            self.preventValidation.emit(True)

    def showEvent(self, event):
        self.checkValues()


def parseParms(Folder, Main_Pattern, Recursive, Cell_Type_Range, Feature_Suffix,
               Methods, Parameter_Search, NbPoints, KFold, Validation_Set, Test_Set,
               Summary, Classifier_Filename):
    """
    Special function for Python Macros to pre-process their arguments and create Python objects.
    """
    Folder = Path(Folder)
    Main_Pattern = str(Main_Pattern)
    Recursive = parmToBool(Recursive)
    Cell_Type_Range = str(Cell_Type_Range)
    Feature_Suffix = str(Feature_Suffix)
    Classifier_Filename = Path(Classifier_Filename)
    Methods = [m.strip() for m in str(Methods).split(",")]
    Parameter_Search = str(Parameter_Search)
    NbPoints = int(NbPoints)
    KFold = int(KFold)
    Summary = Path(Summary)
    Validation_Set = int(Validation_Set)
    Test_Set = int(Test_Set)

    fields = Cell_Type_Range.split(' ')
    if len(fields) != 2:
        raise ValueError("Error, parameter 'Cell_Type_Range' must contain to integers")
    try:
        Cell_Type_Range = (int(fields[0]), int(fields[1]))
    except TypeError:
        raise ValueError("Error, parameter 'Cell_Type_Range' must contain to integers")
    if Cell_Type_Range[0] >= Cell_Type_Range[1]:
        raise ValueError("Error, the first integer in 'Cell_Type_Range' must be strictly smaller than the second")

    if not Folder:
        Folder = Path('.')
    Folder = Folder.abspath()

    return (Folder, Main_Pattern, Recursive, Cell_Type_Range, Feature_Suffix,
            Methods, Parameter_Search, NbPoints, KFold, Validation_Set, Test_Set,
            Summary, Classifier_Filename)


def initialize(Folder, Main_Pattern, Recursive, Cell_Type_Range, Feature_Suffix,
               Methods, Parameter_Search, NbPoints, KFold, Validation_Set, Test_Set,
               Summary, Classifier_Filename,
               parent):
    """
    Provide a user interface to initialize the process
    """
    dlg = CreateClassifierDlg(parent)

    dlg.ui.baseFolder.setText(Folder)
    dlg.ui.mainFilePattern.setText(Main_Pattern)
    dlg.ui.cellTypeMin.setValue(Cell_Type_Range[0])
    dlg.ui.cellTypeMax.setValue(Cell_Type_Range[1])
    dlg.setRecursive(Recursive)

    lstMethods = dlg.ui.listMethods
    low_methods = [m.lower() for m in Methods]
    use_all = 'all' in low_methods
    methodSelection = lstMethods.selectionModel()
    methodModel = lstMethods.model()
    methodSels = None
    for i in range(lstMethods.count()):
        item = lstMethods.item(i)
        if use_all or item.text().lower() in low_methods:
            print("Selecting ", printable(item.text()))
            idx = methodModel.index(i, 0)
            if methodSels is None:
                methodSels = QItemSelection(idx, idx)
            else:
                methodSels.select(idx, idx)
    methodSelection.select(methodSels, QItemSelectionModel.SelectCurrent)

    if Parameter_Search.lower() == 'none':
        dlg.ui.useOptimization.setChecked(False)
    else:
        dlg.ui.useOptimization.setChecked(True)
        for i, m in enumerate(parmsChoice['Parameter_Search']):
            if m.lower() == Parameter_Search.lower():
                dlg.ui.samplingMethod.setCurrentIndex(i-1)
                break

    dlg.ui.validationSetSize.setValue(Validation_Set)
    dlg.ui.testSetSize.setValue(Test_Set)
    dlg.ui.nbPoints.setValue(NbPoints)
    dlg.ui.kFold.setValue(KFold)
    dlg.ui.summaryOutput.setText(Summary)

    dlg.ui.featureFileSuffix.setText(Feature_Suffix)
    dlg.ui.classifierFilename.setText(Classifier_Filename)

    if dlg.exec_() == QDialog.Accepted:
        Folder = dlg.ui.baseFolder.text()
        Main_Pattern = dlg.ui.mainFilePattern.text()
        Recursive = dlg.ui.findRecursive.isChecked()
        Cell_Type_Range = "{0} {1}".format(Cell_Type_Range[0], Cell_Type_Range[1])
        methods = []
        for i in range(dlg.ui.listMethods.count()):
            item = dlg.ui.listMethods.item(i)
            if item.isSelected():
                methods.append(item.text())
        Methods = ",".join(methods)
        if dlg.ui.useOptimization.isChecked():
            Parameter_Search = dlg.ui.samplingMethod.currentText()
        else:
            Parameter_Search = 'None'
        NbPoints = dlg.ui.nbPoints.value()
        KFold = dlg.ui.kFold.value()
        Summary = dlg.ui.summaryOutput.text()
        Feature_Suffix = dlg.ui.featureFileSuffix.text()
        Classifier_Filename = dlg.ui.classifierFilename.text()
        Test_Set = dlg.ui.testSetSize.value()
        Validation_Set = dlg.ui.validationSetSize.value()

        return (Folder, Main_Pattern, Recursive, Cell_Type_Range, Feature_Suffix,
                Methods, Parameter_Search, NbPoints, KFold, Validation_Set, Test_Set,
                Summary, Classifier_Filename)


def bestClassifier(pipelines, endog, exog, validEndog, validExog, parameterSearch):
    if validEndog is not None:
        scores = []
        for clf in pipelines:
            sc = clf.score(validExog, validEndog)
            clf.validation_score = sc
            scores.append(sc)
        sel = np.argmax(scores)
    elif parameterSearch:
        sel = np.argmax([clf.best_score_ for clf in pipelines])
    else:
        sel = np.argmax([clf.score(exog, endog) for clf in pipelines])
    return pipelines[sel]


def run(Folder="", Main_Pattern="{*}.ply", Recursive=False,
        Cell_Type_Range="1 50", Feature_Suffix="-features.csv",
        Methods="All", Parameter_Search="Randomized", NbPoints=50, KFold=5,
        Validation_Set=10, Test_Set=10,
        Summary="summary.txt", Classifier_Filename="classifier.pkl"):
    """
    Execute the process
    """
    # Make sure the system identifies the current thread as the main one
    th = threading.current_thread()
    th.name = 'MainThread'

    Feature_Suffix = correctFormat(Feature_Suffix)

    main_glob, main_basename = processFilePattern(Main_Pattern)

    if Recursive:
        main_files = Folder.walkfiles(main_glob)
    else:
        main_files = Folder.files(main_glob)

    print("#### Reading the data")
    pd_data = []
    for main in main_files:
        basename = main_basename(main.relpath(Folder))
        feat_file = Folder / Feature_Suffix.format(basename)
        if feat_file.isfile():
            pd_data.append(pd.read_csv(feat_file))
    pd_data = pd.concat(pd_data, ignore_index=True)
    pd_data = pd_data[(pd_data.CellType >= Cell_Type_Range[0]) & (pd_data.CellType <= Cell_Type_Range[1])]

    columns = list(pd_data.columns)
    columns.remove('CellType')
    columns.remove('Label')

    exog = pd_data[columns].as_matrix()
    endog = pd_data.CellType.as_matrix()

    all_methods = methodsAvailable()
    methods_low = [m.lower() for m in Methods]

    if 'all' in methods_low:
        Methods = [m for m in all_methods]
    else:
        Methods = [m for m in all_methods if m.name.lower() in methods_low]

    pipelines = []
    for m in Methods:
        clf = pipeline.Pipeline([('norm', preprocessing.StandardScaler()),
                                 ('pca', decomposition.PCA(n_components=0.99)),
                                 ('class', m.builder())])
        pipelines.append(clf)

    if len(Methods) < 2:
        Validation_Set = 0

    testEndog = None
    testExog = None
    validEndog = None
    validExog = None
    if Test_Set > 0 or Validation_Set > 0:
        n = len(endog)
        train = np.random.permutation(n)
        nb_tests = Test_Set * n // 100
        nb_valid = Validation_Set * n // 100
        valid = []
        tests = []
        if Validation_Set > 0:
            print("  Keeping ", nb_valid, " samples for validation")
            valid = train[:nb_valid]
            train = train[nb_valid:]
            validEndog = endog[valid]
            validExog = exog[valid]
        if Test_Set > 0:
            print("  Keeping ", nb_tests, " samples for testing")
            tests = train[:nb_tests]
            train = train[nb_tests:]
            testEndog = endog[tests]
            testExog = exog[tests]
        endog = endog[train]
        exog = exog[train]

    parameterSearch = False
    if Parameter_Search.lower() == 'randomized':
        parameterSearch = True
        print("Randomized parameter search")
        new_pipelines = []
        for m, clf in zip(Methods, pipelines):
            parmsDist = {"class__"+k: m.parmsDist[k] for k in m.parmsDist}
            new_pipelines.append(grid_search.RandomizedSearchCV(clf, parmsDist, cv=KFold,
                                                                n_iter=NbPoints, n_jobs=n_jobs))
        pipelines = new_pipelines
    elif Parameter_Search.lower() == 'grid':
        parameterSearch = True
        print("Grid parameter search")
        new_pipelines = []
        for m, clf in zip(Methods, pipelines):
            parmsRange = {"class__"+k: m.parmsRange[k](NbPoints) for k in m.parmsDist}
            new_pipelines.append(grid_search.GridSearchCV(clf, parmsRange, cv=KFold, n_jobs=n_jobs))
        pipelines = new_pipelines

    print("#### Fitting the data ...")
    for m, clf in zip(Methods, pipelines):
        print("Fitting method ", printable(m.name))
        clf.fit(exog, endog)

    needValidation = len(pipelines) > 1

    if needValidation:
        cell_clf = bestClassifier(pipelines, endog, exog, validEndog, validExog, parameterSearch)
        for i, p in enumerate(pipelines):
            if p is cell_clf:
                best_method = Methods[i]
                break
        else:
            raise ValueError("Cannot determine which method is the best one")
    else:
        cell_clf = pipelines[0]
        best_method = Methods[0]

    if parameterSearch:
        estimator = cell_clf.best_estimator_
    else:
        estimator = cell_clf

    estimator.fields_name = columns

    if Classifier_Filename:
        Classifier_Filename = Folder / Classifier_Filename
        externals.joblib.dump(estimator, Classifier_Filename, compress=3)

    if Summary:
        Summary = Folder / Summary
        with codecs.open(Summary, "w", encoding='utf8', errors='backslashreplace') as f:
            print("Machine Learning Summary", file=f)
            print("========================", file=f)
            print("Selected classifier: ", best_method.name, file=f)
            useEndog = testEndog
            useExog = testExog
            if useEndog is not None:
                s = "Testing using set with {0} elements".format(len(useEndog))
            elif validEndog is not None:
                useEndog = validEndog
                useExog = validExog
                s = "Testing re-using validation set with {0} elements".format(len(useEndog))
            else:
                useEndog = endog
                useExog = exog
                s = "Testing re-using training data set"
            print("", file=f)
            print(s, file=f)
            print('-'*len(s), file=f)
            predictedEndog = estimator.predict(useExog)
            result = confusion_matrix(useEndog, predictedEndog)
            pd.set_option('display.max_rows', len(result))
            pd.set_option('display.max_columns', len(result))
            print("\nConfusion matrix:", file=f)
            print(result, file=f)
            print("\nClassification report:", file=f)
            print(metrics.classification_report(useEndog, predictedEndog), file=f)
            print("\nScore: ", 100*np.mean(useEndog == predictedEndog), "%", file=f)

            print("\nAlgorithms Details", file=f)
            print("==================", file=f)
            for m, clf in zip(Methods, pipelines):
                print("", file=f)
                print(m.name, file=f)
                print("-"*len(m.name), file=f)
                if parameterSearch:
                    print("\nBest score with k-fold:", clf.best_score_, file=f)
                    est = clf.best_estimator_
                else:
                    est = clf
                print("Parameters:", file=f)
                all_params = est.get_params()
                print("all_params = ", all_params)
                for p in m.parameters:
                    print(p, ':', all_params['class__'+p], file=f)
                if needValidation:
                    useEndog = validEndog
                    useExog = validExog
                    if useEndog is not None:
                        s = "Evaluation using set with {0} elements".format(len(validEndog))
                    else:
                        useEndog = endog
                        useExog = exog
                        s = "Evaluation re-using training data set"
                    print("", file=f)
                    print(s, file=f)
                    print('"'*len(s), file=f)
                    predictedEndog = est.predict(useExog)
                    result = confusion_matrix(useEndog, predictedEndog)
                    pd.set_option('display.max_rows', len(result))
                    pd.set_option('display.max_columns', len(result))
                    print("\nConfusion matrix:", file=f)
                    print(result, file=f)
                    print("\nClassification report:", file=f)
                    print(metrics.classification_report(useEndog, predictedEndog), file=f)
                    print("\nScore: ", 100*np.mean(useEndog == predictedEndog), "%", file=f)
    return True
