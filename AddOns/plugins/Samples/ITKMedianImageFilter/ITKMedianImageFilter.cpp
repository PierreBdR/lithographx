/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#include "ITKMedianImageFilter.hpp"

#include <Information.hpp>

#include <ITKProgress.hpp>
#include <itkMedianImageFilter.h>

namespace lgx {
namespace process {

namespace {
template <size_t dim>
bool itkMedianImageFilter(const Store* input, Store* output, util::Vector<dim,uint> radius)
{
  auto converter = UImageConverter<dim>::New();

  converter->SetStore(input);

  typedef itk::MedianImageFilter<UImageType<dim>, UImageType<dim>> FilterType;
  typedef typename FilterType::InputSizeType RadiusType;
  auto filter = FilterType::New();
  filter->SetInput(converter->GetOutput());

  RadiusType r;
  for(size_t i = 0 ; i < dim ; ++i)
    r[i] = radius[i];

  filter->SetRadius(r);

  ITKProgress progress("Median Image Filter");
  progress.setFilter(filter);

  filter->Update();

  auto result = filter->GetOutput();

  if(!UImageConverter<dim>::TransferImage(output->stack(), output, result))
    throw QString("ITK Process tried to change the size of the stack");

  output->copyMetaData(input);
  output->changed();
  return true;
}

}

bool ITKMedianImageFilter::operator()(const Store* input, Store* output, Point3u radius)
{
  const auto* stk = input->stack();
  if(stk->is2D())
    return itkMedianImageFilter<2>(input, output, stk->to2D(radius));
  return itkMedianImageFilter<3>(input, output, radius);
}

QIcon ITKMedianImageFilter::icon() const {
  if(not QFile(":/images/ITKMedianImageFilter.png").exists())
    Information::err << "Error, cannot file resource icon" << endl;
  return QIcon(":/images/ITKMedianImageFilter.png");
}

REGISTER_STACK_PROCESS(ITKMedianImageFilter);
}
}
