/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef LITHOOGRAPHX_HPP
#define LITHOOGRAPHX_HPP
typedef unsigned short ushort;
typedef unsigned int uint;

#include <LGXConfig.hpp>

#include <ui_GUI.h>

#include <CutSurf.hpp>
#include <Geometry.hpp>
#include <Misc.hpp>
#include <Process.hpp>
#include <TaskEditDlg.hpp>

#include <memory>
#include <string.h>
#include <QMainWindow>
#include <QMutex>
#include <QPointer>
#include <QTime>

class LabelEditorDlg;
class LithoViewer;
class PathEditorDlg;
class ProcessDocDialog;
class ProcessParmsModel;
class QAction;
class QCloseEvent;
class QDragEnterEvent;
class QDropEvent;
class QFileSystemWatcher;
class QLabel;
class SettingsDlg;
class Ui_EditParmsDialog;

namespace lgx {
class ProcessThread;

namespace process {
class SetupProcess;
class Process;
} // namespace process
} // namespace lgx

class LithoGraphX : public QMainWindow
{
  Q_OBJECT

public:
  LithoGraphX(QString appdir);
  ~LithoGraphX();

  static QString AppDir;
  static QLabel* ActiveStack;

  typedef lgx::Point3u Point3u;
  typedef lgx::Point3f Point3f;

  // Confocal stack objects
  QPointer<lgx::ImgData> _stack1, _stack2;

  // Cutting surface
  lgx::CutSurf _cutSurf;

private:
  QPointer<QDialog> _editParmsDialog;
  std::unique_ptr<Ui_EditParmsDialog> ui_editParmsDialog;
  QPointer<ProcessDocDialog> _processDocDialog;
  Ui::MainWindow ui;

  bool _needSaving;

  QPointer<ProcessParmsModel> _parmsModel;
  QString _currentProcessName;
  QString _currentProcessType;
  bool _currentProcessInTasks;
  std::unique_ptr<lgx::process::Process> _currentProcess;
  std::unique_ptr<lgx::process::SetupProcess> _currentSetup;
  QPointer<lgx::ProcessThread> _processThread;
#ifdef WATCH_PROCESS_FOLDERS
  QFileSystemWatcher* _processWatcher;
  QTimer* _processReloadTimer;
#endif
  QPointer<QAction> _resetParametersAct;
  QPointer<QAction> _editTasksAct;
  bool _stack1NeedsUpdate;
  bool _stack2NeedsUpdate;
  QPointer<QTimer> _stackUpdateTimer;

  QHash<QString, lgx::process::BaseProcessDefinition> _savedStackProc;
  QHash<QString, lgx::process::BaseProcessDefinition> _savedMeshProc;
  QHash<QString, lgx::process::BaseProcessDefinition> _savedGlobalProc;

  TaskEditDlg::tasks_t _tasks;

  QPointer<PathEditorDlg> _editPathsDlg;
  QPointer<LabelEditorDlg> _labelEditorDlg;
  QPointer<SettingsDlg> _settingsDlg;

  QTime _currentProcessTime;
  QString _loadFilename;

  const QString& projectFile() const;
  void setProjectFile(const QString& f);

public:
  void setDebug(bool debug);

  // Launch a process
  bool launchProcess(const QString& processType, const QString& processName, lgx::process::ParmList& parms,
                     bool useGUI = true,
                     bool saveDefaults = true);

  int activeStack() const;
  int activeMesh() const;
  QString getStackName(bool main, int num);
  QString getMeshName(int num);
  void setLoadFile(const QString& f) {
    _loadFilename = f;
  }
  bool runningProcess() const {
    return (bool)_currentProcess;
  }
  const lgx::process::Process* globalProcess() const;
  lgx::process::Process* globalProcess();
  static void writeProcessParams(const lgx::process::BaseProcessDefinition& def, QTextStream& pout);

  bool canAutoOpen(const QString& filename) const;

  LithoViewer* viewer();

private:
  void loadControls();
  void connectControlsIn();
  void connectControlsOut();
  void readParms();
  void writeParms();
  void clearMeshSelect();
  void updateVBOs();
  void processCommandGetParmValues(const QString& name, int type);
  void processCommandSetParmValues(const QString& name, int type);
  void readProcessParms();
  void createSetupProcess();
  void updateCurrentTasks(const TaskEditDlg::tasks_t& saved_tasks);
  void updateFillWorkOptions();

public slots:
  void modified(bool state = true);
  void initProject();
  void loadProject(const QString& filename, bool choose_file = true);
  void saveProject(const QString& filename, bool choose_file = true);
  void updateStateFromProcess(lgx::process::Process* proc);
  void exportMesh(int stack);
  void resetMesh(int mesh);
  void loadMesh(int stack);
  void saveMesh(int stack);
  void importStack(int stack, bool main);
  void importStackSeries(int stack, bool main);
  void openStack(int stack, bool main);
  void saveStack(int stack, bool main);
  void exportStack(int stack, bool main);
  void updateCurrentStack();
  void processSystemCommand(lgx::process::Process* proc, lgx::process::SystemCommand command, const lgx::process::ParmList& parms);
  // Open any file, using extension to figure out which file type
  void autoOpen(const QString& filename = QString(), int stack = 0, bool main = true);
  void resetDefaultParameters();
  void editTasks();
  void reloadTasks();
  void setCutSurfControl();
  void resetClipControl(float sceneRadius);
  void setSpinningSensitivity(float spin);

  void saveProcessesParameters();
  void recallProcessesParameters();
  void reloadProcesses();
  void scanProcessLists();
  void processCommandFinished();

  void editParmsSaveAs();
  void updateDefaultTex16Bits();

protected slots:
  void on_Stack1MainShow_toggled(bool);
  void on_Stack1WorkShow_toggled(bool);
  void on_Stack1SurfShow_toggled(bool);
  void on_Stack1MeshShow_toggled(bool);
  void on_Stack1WorkLabels_toggled(bool);

  void on_Stack2MainShow_toggled(bool);
  void on_Stack2WorkShow_toggled(bool);
  void on_Stack2SurfShow_toggled(bool);
  void on_Stack2MeshShow_toggled(bool);
  void on_Stack2WorkLabels_toggled(bool);

  void on_StackTabs_currentChanged(int);
  void on_ProcessTabs_currentChanged(int);

  void on_stackUpdateTimer_timeout();

  void on_parmsModel_valuesChanged();

  void on_Clip1Enable_toggled(bool);
  void on_Clip2Enable_toggled(bool);
  void on_Clip3Enable_toggled(bool);

  void on_Clip1Grid_toggled(bool);
  void on_Clip2Grid_toggled(bool);
  void on_Clip3Grid_toggled(bool);

  void on_Clip1Width_valueChanged(int);
  void on_Clip2Width_valueChanged(int);
  void on_Clip3Width_valueChanged(int);

  void on_PixelEditRadius_valueChanged(int);
  void on_FillWorkData_toggled(bool);
  void on_FillWorkValue_valueChanged(int);
  void on_SeedStack_toggled(bool);

  void on_Stack1_needsUpdate();
  void on_Stack1_stackUnloaded();
  void on_Stack1_changeSize(const Point3u& size, const Point3f& step, const Point3f& origin);
  void on_Stack1_changedInterface() { modified(); }
  void on_Stack1_updateSliderScale();

  void on_Stack2_needsUpdate();
  void on_Stack2_stackUnloaded();
  void on_Stack2_changeSize(const Point3u& size, const Point3f& step, const Point3f& origin);
  void on_Stack2_changedInterface() { modified(); }
  void on_Stack2_updateSliderScale();

  void on_ManipulateStack1_toggled(bool);
  void on_ManipulateStack2_toggled(bool);
  void on_ManipulateClip1_toggled(bool);
  void on_ManipulateClip2_toggled(bool);
  void on_ManipulateClip3_toggled(bool);
  void on_ManipulateCutSurf_toggled(bool);

  void on_resetParametersAct_triggered() { resetDefaultParameters(); }
  void on_editTasksAct_triggered() { editTasks(); }

  void on_ProcessStackFilter_textChanged(const QString& text);
  void on_ProcessMeshFilter_textChanged(const QString& text);
  void on_ProcessGlobalFilter_textChanged(const QString& text);
  void on_ProcessStackCommand_currentItemChanged(QTreeWidgetItem* current, QTreeWidgetItem* prev);
  void on_ProcessMeshCommand_currentItemChanged(QTreeWidgetItem* current, QTreeWidgetItem* prev);
  void on_ProcessGlobalCommand_currentItemChanged(QTreeWidgetItem* current, QTreeWidgetItem* prev);
  void on_ProcessTasksCommand_currentItemChanged(QTreeWidgetItem* current, QTreeWidgetItem* prev);

  void on_menuFileSaveAll_triggered();
  void on_menuFileReloadAll_triggered();
  void on_menuFileEditPaths_triggered();
  void on_menuFileOpen_triggered();
  void on_menuFileSave_triggered();
  void on_menuFileSaveAs_triggered();
  void on_menuFileResetAll_triggered();
  void on_menuExit_triggered();

  void on_menuStack1Import_triggered();
  void on_menuStack1Load_triggered();
  void on_menuStack1Save_triggered();
  void on_menuStack1Open_triggered();
  void on_menuStack1Export_triggered();
  void on_menuStack1Reset_triggered();
  void on_menuStack2Import_triggered();
  void on_menuStack2Load_triggered();
  void on_menuStack2Save_triggered();
  void on_menuStack2Open_triggered();
  void on_menuStack2Export_triggered();
  void on_menuStack2Reset_triggered();

  void on_menuWorkStack1Import_triggered();
  void on_menuWorkStack1Load_triggered();
  void on_menuWorkStack1Save_triggered();
  void on_menuWorkStack1Open_triggered();
  void on_menuWorkStack1Export_triggered();
  void on_menuWorkStack2Import_triggered();
  void on_menuWorkStack2Load_triggered();
  void on_menuWorkStack2Save_triggered();
  void on_menuWorkStack2Open_triggered();
  void on_menuWorkStack2Export_triggered();

  void on_menuMesh1Export_triggered();
  void on_menuMesh1Load_triggered();
  void on_menuMesh1Save_triggered();
  void on_menuMesh1Reset_triggered();
  void on_menuMesh2Export_triggered();
  void on_menuMesh2Load_triggered();
  void on_menuMesh2Save_triggered();
  void on_menuMesh2Reset_triggered();

  void on_actionSelectAll_triggered();
  void on_actionUnselect_triggered();
  void on_actionInvertSelection_triggered();
  void on_actionAddCurrentLabel_triggered();
  void on_actionSelectUnlabeled_triggered();
  void on_actionRemoveCurrentLabel_triggered();
  void on_actionChangeSeed_triggered();
  void on_actionNew_Seed_triggered();

  void on_actionUserManual_triggered();
  void on_actionOnlineUserManual_triggered();
  void on_actionDoxygen_triggered();
  void on_actionQGLViewerHelp_triggered();
  void on_actionAbout_triggered();
  void on_actionLibrariesUsed_triggered();
  void on_actionProcessDocs_triggered();
  void on_actionSample_Plugins_triggered();

  void on_Viewer_deleteSelection();
  void on_Viewer_setLabelColor(QIcon& icon);
  void on_Viewer_changeSceneRadius(float);
  void on_Viewer_modified() { modified(); }
  void on_Viewer_selectLabelChanged(int lab);

  void on_actionAddNewSeed_triggered(bool on);
  void on_actionAddCurrentSeed_triggered(bool on);
  void on_actionPickLabel_triggered(bool on);
  void on_actionGrabSeed_triggered(bool on);
  void on_actionFillLabel_triggered(bool on);
  void on_actionMeshSelect_triggered(bool on);
  void on_actionLabelSelect_triggered(bool on);
  void on_actionConnectedSelect_triggered(bool on);
  void on_actionPixelEdit_triggered(bool on);
  void on_actionPickVolLabel_triggered(bool on);
  void on_actionDeleteVolumeLabel_triggered(bool on);
  void on_actionDeletePickedLabel_triggered(bool on);
  void on_actionFillVolumeLabel_triggered(bool on);

  void on_actionEraseSelection_triggered();
  void on_actionFillSelection_triggered();
  void on_actionDeleteSelection_triggered();
  void on_actionEditParms_triggered();
  void on_actionColorPalette_triggered();
  void on_actionEditLabels_triggered(bool on);
  void on_actionSwapSurfaces_triggered();

  void on_actionResetView_triggered();
  void on_ProcessCommandGo_clicked();
  void on_actionDebug_Dialog_triggered();
  void on_CutSurfReset_clicked();

  void on_actionSelectStack1_triggered();
  void on_actionSelectStack2_triggered();

  void on_actionUserProcessFolder_triggered();
  void on_actionSystemProcessFolder_triggered();

  void on_actionUserMacroFolder_triggered();
  void on_actionSystemMacroFolder_triggered();
  void on_actionReloadMacros_triggered();

  void on_actionSettings_triggered(bool on);
  void on_actionManagePlugin_triggered();
  void on_actionManagePackages_triggered();

  void showPackageManager();

#ifdef LithoGraphX_CHECK_UPDATES
  void on_actionCheckUpdatesLGX_triggered();
  void checkUpdateAtStart();
  void hasNewVersion();
#endif

signals:
  void processFinished();
  void processFailed();
  void installPackage(const QString& filename);

protected:
  bool event(QEvent* e) override;
  void keyPressEvent(QKeyEvent *event) override;
  void closeEvent(QCloseEvent* e) override;
  void showEvent(QShowEvent* e) override;
  void cleanProcess();
  void saveSettings();
};

#endif // LITHOOGRAPHX_HPP
