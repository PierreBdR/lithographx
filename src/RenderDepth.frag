uniform sampler2D tex2D;
uniform sampler2D depthValue;
smooth centroid in vec2 texPos;

void main()
{
  gl_FragColor = texture(tex2D, texPos);
  gl_FragDepth = texture(depthValue, texPos).r;
}

