/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef SYSTEMPROCESSSAVE_HPP
#define SYSTEMPROCESSSAVE_HPP

#include <Process.hpp>

#include <QObject>
#include <QRegExp>
#include <LGXVersion.hpp>
#include <Mesh.hpp>

/**
 * \file SystemProcessSave.hpp
 * File containing the processes to save data.
 */

class QDialog;
class Ui_SaveMeshDialog;
class Ui_ExportMeshDialog;
class Ui_SaveAllDlg;
class Ui_ExportStackDialog;

namespace lgx {
namespace process {

/**
 * \class StackSave SystemProcessSave.hpp <SystemProcessSave.hpp>
 *
 * Save the data into a MGXS or INRIA format.
 *
 * Description of the MGXS file format
 * ===================================
 *
 * MGXS is a binary file format to store 3D images with all the metadata needed for proper rendering in LithoGraphX.
 *
 * There are many versions, from 1.0 to 1.3. This version of LithoGraphX will always generate MGXS files version 1.3.
 *
 * Each MGXS file starts with the ASCII string `MGXS` followed by a space, the version number in ASCII and another
 * space. For instance, for the current version, the file starts with the ASCII string `"MGXS 1.3 "`.
 *
 * A binary header follows the version number. New versions simply added fields to the header. The header is made of:
 *
 *   Field | Size (bytes) | type  | Version | Description
 * --------|--------------|-------|---------|-------------------------------------------------------------
 * isLabel |     1        | bool  |  1.1    | True if the stack's values are labels and not intensities
 * sxum    |     4        | float |  1.3    | Position of the stack's origin on the X axis, in micrometers
 * syum    |     4        | float |  1.3    | Position of the stack's origin on the Y axis, in micrometers
 * szum    |     4        | float |  1.3    | Position of the stack's origin on the Z axis, in micrometers
 * xsz     |     4        | uint  |  1.0    | Number of voxels along the X axis
 * ysz     |     4        | uint  |  1.0    | Number of voxels along the Y axis
 * zsz     |     4        | uint  |  1.0    | Number of voxels along the Z axis
 * xum     |     4        | float |  1.0    | Resolution along the X axis, in micrometers
 * yum     |     4        | float |  1.0    | Resolution along the Y axis, in micrometers
 * zum     |     4        | float |  1.0    | Resolution along the Z axis, in micrometers
 * datasz  |     8        | uint  |  1.0    | Size in bytes of the data. It should be `2*xsz*ysz*zsz`
 * cl      |     1        | uint  |  1.0    | Compression level
 *
 * If the compression level is 0, the header is then followed by the raw, uncompressed, data. Voxels are ordered
 * C-style (e.g. vertices consecutives along the X axis are consecutive in the file, vertices consecutives along the
 * Y axis are separated by `xsz` voxels in the file, and vertices consecutive in the Z axis are separated by `xsz*ysz`
 * voxels in the files).
 *
 * If the compression level is greater than 0, the stack is cut into slice. Each slice is compressed using the gzip
 * algorithm (using the `qCompress` Qt function). In the file, the size of the compressed slice is written as
 * a 4 bytes unsigned integer, followed by the compressed data. A pseudo code to read the compressed data is:
 *
 *     current_size = 0
 *     while current_size < datasz
 *         slice_size = read 4 bytes
 *         compressed_data = read slice_size bytes
 *         data = uncompress compressed_data
 *         store data
 *         current_size += size of data
 *
 * **Note** before version 1.3, if the stack was labeled, the label numbers were store as big endian. Starting 1.3,
 * they are stores as little endian. Stack intensities are always stored as little endian.
 *
 * \ingroup StackProcess
 */
class LGX_EXPORT StackSave : public StackProcess {
public:
  StackSave(const StackProcess& proc)
    : Process(proc)
    , StackProcess(proc)
  {
  }

  bool initialize(ParmList& parms, QWidget* parent) override;

  bool operator()(const ParmList& parms) override;
  /**
   * Save the stack
   * \param stack Stack containing the data
   * \param store Store containing the data
   * \param filename File in which to save the data, the extension must be
   * .mgxs or .inr
   */
  bool operator()(Stack* stack, Store* store, const QString& filename, int compressionLevel = 0);
  QString folder() const override {
    return "System";
  }
  QString name() const override {
    return "Save";
  }
  QString description() const override {
    return "Save a stack into a known 3D image format";
  }
  QStringList parmNames() const override
  {
    return QStringList() << "Filename"
                         << "Store"
                         << "Stack number"
                         << "Compression Level (0-9)";
  }
  QStringList parmDescs() const override
  {
    return QStringList() << "Filename"
                         << "Store"
                         << "Stack number"
                         << "Compression Level (0-9)";
  }
  ParmList parmDefaults() const override
  {
    return ParmList() << ""
                      << "Main"
                      << 0
                      << 5;
  }
  ParmChoiceMap parmChoice() const override
  {
    ParmChoiceMap map;
    map[1] = storeChoice();
    return map;
  }
  QIcon icon() const override {
    return QIcon(":/images/save.png");
  }
};

/**
 * \class StackExport SystemProcessSave.hpp <SystemProcessSave.hpp>
 *
 * Export the stack data into a stack of images.
 *
 * \ingroup StackProcess
 */
class LGX_EXPORT StackExport : public QObject, public StackProcess {
  Q_OBJECT
public:
  StackExport(const StackProcess& proc)
    : Process(proc)
    , QObject()
    , StackProcess(proc)
  {
  }

  bool initialize(ParmList& parms, QWidget* parent) override;

  bool operator()(const ParmList& parms) override;
  /**
   * Export the stack
   * \param stack Stack containing the image
   * \param store Store containing the image
   * \param filename Prefix of the file to store the data in
   * \param type Type of the file. Type must be one of 'DML', 'TIF', 'CIMG' or 'CImg Auto'. If 'CImg Auto', the
   * actual type will be deduced by the file extension, and the CImg library will try to save it using its own data
   * formats, Image Magick or Graphics Magick.
   * \param nb_digit Number of digits to use to generate the file numbering. If 0, it will use an optimal number of
   * digits.
   * \param generate_voxel_spacing If true, the process also generate a voxelspacing.txt file for easy import into
   * VolViewer.
   */
  bool operator()(Stack* stack, Store* store, const QString& filename, const QString& type,
                  unsigned int nb_digit, bool generate_voxel_spacing);
  QString folder() const override {
    return "System";
  }
  QString name() const override {
    return "Export";
  }
  QString description() const override {
    return "Export a stack into an image sequence.";
  }
  QStringList parmNames() const override
  {
    return QStringList() << "Filename"
                         << "Store"
                         << "Format"
                         << "Generate Voxel Spacing"
                         << "Stack number"
                         << "NbDigits";
  }
  QStringList parmDescs() const override
  {
    return QStringList() << "Filename"
                         << "Store"
                         << "Format"
                         << "Generate Voxel Spacing"
                         << "Stack number"
                         << "NbDigits";
  }
  ParmList parmDefaults() const override
  {
    return ParmList() << ""
                      << "Main"
                      << "CImg Auto"
                      << "No"
                      << 0
                      << 0;
  }
  ParmChoiceMap parmChoice() const override
  {
    ParmChoiceMap map;
    map[1] = storeChoice();
    map[3] = booleanChoice();
    return map;
  }
  QIcon icon() const override {
    return QIcon(":/images/save.png");
  }

protected slots:
  void selectImageFile();
  void setImageType(const QString& type);

protected:
  Ui_ExportStackDialog* ui;
  QDialog* dlg;
};

/**
 * \class MeshExport SystemProcessSave.hpp <SystemProcessSave.hpp>
 *
 * Export a mesh on various formats: \ref PLY, \ref VTK, \ref text, \ref cells, \ref MeshEdit, \ref STL or \ref OBJ.
 *
 * We will here describe the file formats, or at least how LithoGraphX uses them.
 *
 * \section PLY PLY format
 *
 * The basic principles of the PLY format are described on the website of
 * the Paul Bourke: http://paulbourke.net/dataformats/ply/ or on wikipedia: https://en.wikipedia.org/wiki/PLY_(file_format)
 *
 * The files generated by LithoGraphX are fully compatible with the basic PLY format, but provide much more information.
 * See MeshSave for more information.
 *
 * \section VTK VTK format
 *
 * LithoGraphX save VTU format, which are XML files for unstructure grids.
 *
 * The file format is quite complex, and fully described in the VTK book and website.
 *
 * LithoGraphX stores the following properties in addition to the mandatory ones:
 *
 * For each vertex: 'Signal' (float32), 'Normals' (3 floats32), 'Color' (float32) and 'Label' (int32)
 *
 * For each "cell" (e.g. triangles or 2D cells): 'Label' (int32)
 *
 * LithoGraphX also adds a DataArray to store the heat per VTU "cell". The name is either "Cell Heat" or "Wall Heat".
 *
 * At last, an extra DataArray is created, store the heat per label instead of per cell.
 *
 * \section text "text" format
 *
 * The text format is a trivial ASCII file format. The file contains:
 *
 *     n
 *     vertex 0
 *     vertex 1
 *     ...
 *     vertex n-1
 *     neighbors 0
 *     neighbors 1
 *     ...
 *     neighbors n-1
 *
 * Each vertex is described as:
 *
 *     id x y z label
 *
 * Each neighborhood is described as:
 *
 *     id m id_1 id_2 ... id_m
 *
 * Where `id` is the id of the current vertex, `m` the number of neighbors, and `id_1`, `id_2` are vertex ids as
 * described before. The vertex must be ordered counter-clockwise.
 *
 * \section cells "cells" format
 *
 * This format is very similar to the "text" format. Only each vertex is described as:
 *
 *     id x z y label type
 *
 * Where `type` is `j` for junctions and `c` for cell centers.
 *
 * \section MeshEdit MeshEdit format
 *
 * This file format is the one used by the MeshEdit software. In LithoGraphX, the file has the following format
 *
 *     MeshVersionFormatted 1
 *     Dimension 3
 *     Vertices
 *     vertex 1
 *     vertex 2
 *     ...
 *     vertex n
 *     Triangles
 *     triangle 1
 *     triangle 2
 *     ...
 *     triangle m
 *     End
 *
 * Each vertex is described as:
 *
 *     x y z label
 *
 * And each triangle is described as:
 *
 *     v0 v1 v2 label
 *
 * Again, the vertices must be ordered counter-clockwise.
 *
 * \section STL STL format
 *
 * The STL file format is the one used in many CAD software, including Abaqus.
 *
 * In LithoGraphX, the mesh is stored as a single solid called "lgx". The file structure is:
 *
 *     solid lgx
 *     facet 1
 *     facet 2
 *     ...
 *     facet n
 *     endsolid lgx
 *
 * Where each facet is described as:
 *
 *     facet normal nx ny nz
 *       outer loop
 *         vertex vx1 vy1 vz1
 *         vertex vx2 vy2 vz2
 *         vertex vx3 vy3 vz3
 *       endloop
 *     endfacet
 *
 * \section OBJ OBJ format
 *
 * LithoGraphX can also export files in the WaveFront's OBJ file format.
 *
 * The file has the following structure:
 *
 *     # Triangular mesh created by LithoGraphX
 *     # Length unit: µm
 *     # Vertices
 *     vertex 1
 *     vertex 2
 *     ...
 *     vertex n
 *     # Triangles
 *     triangle 1
 *     triangle 2
 *     ...
 *     triangle m
 *
 * Each vertex is stored as:
 *
 *     v x y z
 *     vn nx ny nz
 *
 * And each triangle as:
 *
 *     f v1 v2 v3
 *
 * where `v1`, `v2` and `v3` are 1-based index in the vertex array. Also, the vertices are oriented counter-clockwise.
 *
 * \section MGXM MorphoGraphX Mesh File Format
 *
 * MGXM is a binary file format to store triangular meshes with all the data and meta-data used by LithoGraphX.
 *
 * There are a few versions, from 1.0 to 1.2. This version of LithoGraphX will always generate MGXM files version
 * 1.2.
 *
 * Each MGXM file starts with the ASCII string `MGXM` followed by a space, the version number in ASCII and another
 * space. For instance, for the current version, the file starts with the ASCII string `"MGXM 1.2 "`.
 *
 * A binary header follows the version number. The header is made of:
 *
 *   Field       | Size (bytes) | type  | Version | Description
 * --------------|--------------|-------|---------|-------------------------------------------------------------
 *  is_cells     |      1       | bool  |  1.2    | If true, the mesh represent 2D cells (see notes)
 *  scale        |      1       | bool  |  1.1    | If true, the mesh has been scaled when saved
 *  transform    |      1       | bool  |  1.1    | If true, the mesh has been transformed rigidly
 *  signalLow    |      4       | float |  1.1    | Lower value of the signal
 *  signalHigh   |      4       | float |  1.1    | Upper value of the signal
 *  signalUnit   |      *       | utf8  |  1.1    | Text of the signal unit (see notes)
 *  header_size  |      4       | uint  |  1.0    | Specified the size in bytes of extra data placed after the header
 *  vertex_cnt   |      4       | uint  |  1.0    | Number of vertices in the mesh
 *  vertex_size  |      4       | uint  |  1.0    | Size in bytes of the structure describing a vertex, minus 12.
 *  edge_size    |      4       | uint  |  1.0    | Size in bytes of the structure describing an edge.
 *
 * The description of the vertices follows the header. For each vertex, a structure of size `vertex_size+12` is
 * written. Only fields fitting in the given size are actually written, and any other data is ignored. Note that the
 * first four field are always there!
 *
 *   Field       | Size (bytes) | type  |  Description
 * --------------|--------------|-------|----------------------------------------------------------------------
 *   Id          |      4       | uint  | Id of the vertex, used to reference it in other places in the file
 *   X           |      4       | float | X position of the vertex
 *   Y           |      4       | float | Y position of the vertex
 *   Z           |      4       | float | Z position of the vertex
 *   label       |      4       | uint  | Label of the vertex
 *   color       |      4       | float | "Color" of the vertex (e.g. like signal, but scaled from 0 to 1)
 *   signal      |      4       | float | Signal of the vertex
 *   type        |      1       | char  | type of vertex `'c'` for center and `'j'` for junction.
 *   selected    |      1       | bool  | If true, the vertex is selected
 *
 * Currently, the edges have no defined fields. For each vertex, the `Id` of the vertex is written, followed by a 32
 * bits unsigned integer for the number of neighbors, and the list of `Id` for each neighbor. Neighbors of a vertex
 * are always written counter-clockwise.
 *
 * Notes on UTF-8 strings
 * ----------------------
 *
 * UTF-8 strings are written as: their size in bytes as a 32 bits unsigned integer, followed by the bytes themselves.
 *
 * Notes on vertex labels
 * ----------------------
 *
 * Vertex are labels to mark which cells they are part of. Boundary vertices are labeled with -1 or -2. -1 mark
 * vertices between cells, and -2 vertices at the border of the whole mesh.
 *
 * Notes on cell mesh
 * ------------------
 *
 * If the mesh is a cell mesh, then each cell has a unique labeled vertex on its center, surrounded by cell-boundary
 * vertices, labels -1 or -2. In this case, labeled vertices have type `'c'` and boundary vertices `'j'`.
 *
 *
 * \ingroup MeshProcess
 */
class LGX_EXPORT MeshExport : public QObject, public MeshProcess {
  Q_OBJECT
public:
  MeshExport(const MeshProcess& proc)
    : Process(proc)
    , QObject()
    , MeshProcess(proc)
  {
  }

  bool initialize(ParmList& parms, QWidget* parent) override;
  bool operator()(const ParmList& parms) override;
  /**
   * Export a mesh
   * \param mesh Mesh to be exported
   * \param filename File to save the mesh into
   * \param type Type (format) of the export.
   * \param transform Save the transformed positions
   */
  bool operator()(Mesh* mesh, const QString& filename, const QString& type, bool transform);
  QString folder() const override {
    return "System";
  }
  QString name() const override {
    return "Export";
  }
  QString description() const override {
    return "Export a mesh into a known mesh format";
  }
  QStringList parmNames() const override
  {
    return QStringList() << "Filename"
                         << "Kind"
                         << "Transform"
                         << "Mesh number";
  }
  QStringList parmDescs() const override
  {
    return QStringList() << "Filename"
                         << "Kind"
                         << "Transform"
                         << "Mesh number";
  }
  QIcon icon() const override {
    return QIcon(":/images/save.png");
  }
  ParmList parmDefaults() const override
  {
    return ParmList() << ""
                      << "Text"
                      << false
                      << 0;
  }
  ParmChoiceMap parmChoice() const override
  {
    ParmChoiceMap map;
    map[1] = QStringList() << "PLY Binary"
                           << "PLY Ascii"
                           << "MorphoGraphX Mesh"
                           << "VTK Mesh Binary"
                           << "VTK Mesh Ascii"
                           << "Text"
                           << "Cells"
                           << "MeshEdit"
                           << "STL"
                           << "OBJ";
    map[2] = map[3] = booleanChoice();
    return map;
  }

protected slots:
  void selectMeshFile();
  void selectMeshType(const QString& type);

protected:
  QString properFile(QString filename, const QString& type) const;
  void setMeshFile(const QString& filename);
  Point3f savedPos(Point3f pos, bool transform, const Stack* stack);
  Point3f savedNormal(Point3f normal, bool transform, const Stack* stack);

  bool saveText(Mesh* mesh, const QString& filename, bool transform);
  bool saveCells(Mesh* mesh, const QString& filename, bool transform);
  bool saveMeshEdit(Mesh* mesh, const QString& filename, bool transform);
  bool saveMeshSTL(Mesh* mesh, const QString& filename, bool transform);
  bool saveVTU(Mesh* mesh, const QString& filename, bool transform, bool binary = false);
  bool saveOBJ(Mesh* mesh, const QString& filename, bool transform);
  bool savePLY(Mesh* mesh, const QString& filename, bool transform, bool binary = false);
  bool saveMGXM(Mesh* mesh, const QString& filename, bool transform);

  QDialog* dlg;
  Ui_ExportMeshDialog* ui;
};

/**
 * \class MeshSave SystemProcessSave.hpp <SystemProcessSave.hpp>
 *
 * \ingroup MeshProcess
 */
class LGX_EXPORT MeshSave : public MeshExport {
  Q_OBJECT
public:
  MeshSave(const MeshProcess& proc)
    : Process(proc)
    , MeshExport(proc)
  {
  }

  bool initialize(ParmList& parms, QWidget* parent) override;
  bool operator()(const ParmList& parms) override;
  /**
   * Save a mesh
   * \param mesh Mesh to be saved
   * \param filename File to save the mesh in. Its extension must be either .inr or .mgmx
   * \[aram transform Save the transformed positions
   */
  bool operator()(Mesh* mesh, const QString& filename, bool transform);
  QString folder() const override {
    return "System";
  }
  QString name() const override {
    return "Save";
  }
  QString description() const override {
    return "Save a mesh into a known mesh format";
  }
  QStringList parmNames() const override
  {
    return QStringList() << "Filename"
                         << "Transform"
                         << "Mesh number";
  }
  QStringList parmDescs() const override
  {
    return QStringList() << "Filename"
                         << "Transform"
                         << "Mesh number";
  }
  QIcon icon() const override {
    return QIcon(":/images/save.png");
  }
  ParmList parmDefaults() const override
  {
    return ParmList() << ""
                      << false
                      << 0;
  }
  ParmChoiceMap parmChoice() const override
  {
    ParmChoiceMap map;
    map[1] = map[2] = booleanChoice();
    return map;
  }

protected slots:
  void selectMeshFile();

protected:
  QString properFile(QString filename) const;
  void setMeshFile(const QString& filename);
  Point3f savedPos(Point3f pos, bool transform, const Stack* stack);

  bool saveMesh(Mesh* mesh, const QString& filename, bool transform);

  QDialog* dlg;
  Ui_SaveMeshDialog* ui;
};


/**
 * \class SaveProjectFile SystemProcessSave.hpp <SystemProcessSave.hpp>
 *
 * Save the view file
 *
 * \ingroup GlobalProcess
 */
class LGX_EXPORT SaveProjectFile : public GlobalProcess {
public:
  SaveProjectFile(const GlobalProcess& proc)
    : Process(proc)
    , GlobalProcess(proc)
  {
  }

  bool initialize(ParmList& parms, QWidget* parent) override;

  bool operator()(const ParmList& parms) override;
  /**
   * Save the view file
   * \param filename File to save the parameters in
   */
  bool operator()(const QString& filename);

  QString folder() const override {
    return "System";
  }
  QString name() const override {
    return "Save Project";
  }
  QString description() const override {
    return "Save the project file for the current configuration.";
  }
  QStringList parmNames() const override {
    return QStringList() << "Filename"
                         << "Choose File";
  }
  QStringList parmDescs() const override {
    return QStringList() << "Filename"
                         << "If true or if no filename is provided, the initialization will show a dialog box to select a file";
  }
  ParmList parmDefaults() const override {
    return ParmList() << "" << true;
  }
  ParmChoiceMap parmChoice() const override {
    ParmChoiceMap map;
    map[1] = booleanChoice();
    return map;
  }
  QIcon icon() const override {
    return QIcon(":/images/save.png");
  }
};

/**
 * \class SaveAll SystemProcessSave.hpp <SystemProcessSave.hpp>
 *
 * Save all the data currently loaded.
 *
 * \ingroup GlobalProcess
 */
class LGX_EXPORT SaveAll : public QObject, public GlobalProcess {
  Q_OBJECT

public:
  SaveAll(const GlobalProcess& proc)
    : Process(proc)
    , QObject()
    , GlobalProcess(proc)
  {
  }

  /**
   * Recognised type of object
   */
  enum OBJECT_TYPE { MAIN_STACK, WORK_STACK, MESH, VIEW, ALL };

  /**
   * Structure describing an object to be saved
   */
  struct SaveStruct {
    OBJECT_TYPE type;     ///< Type of the object
    int id;               ///< Id of the object (i.e. id used in Process::stack(int) or Process::mesh(int)
    QString filename;     ///< File to save the object in
    bool transform;       ///< For mesh only, save transformed positions
  };

  bool initialize(ParmList& parms, QWidget* parent) override;
  bool operator()(const ParmList& parms) override;
  /**
   * Save the objects described in the list
   */
  bool operator()(const QList<SaveStruct>& tosave, int compressionLevel = 0);

  QString folder() const override {
    return "System";
  }
  QString name() const override {
    return "Save All";
  }
  QString description() const override {
    return "Save all or part of the data currently loaded in LithoGraphX";
  }
  QStringList parmNames() const override
  {
    return QStringList() << "Type"
                         << "Definition"
                         << "Compression Level (0-9)";
  }
  QStringList parmDescs() const override
  {
    return QStringList() << "'All' to save all the structures, keeping their current file names\n"
                            "'Defined' to specify in the 'Definition' argument what needs saving and how"
                         << "A comma-separated list of fields, where each field is of the form KEY=VALUE\n"
                            "The key must be one of:"
                            " - 'MainStack' or 'WorkStack' followed by their user-visible id (i.e. 'MainStack1'\n"
                            "   for the first main stack)\n"
                            " - 'Mesh' preceded optionally by 'Transformed', and followed by the mesh id \n"
                            "   (i.e. 'TransformedMesh1' for the second mesh, transformed)\n"
                            "   The mesh type is by default binary mesh but can be empty, 'Text', 'Cells' \n"
                            "   or 'MeshEdit'. Note that a mesh cannot be saved as a Keyence mesh.\n"
                            " - 'View' for the view file.\n"
                            "The value is the name of the file to save the designated item into."
                         << "Compression level used for stacks.";
  }
  ParmList parmDefaults() const override
  {
    return ParmList() << "All"
                      << ""
                      << 9;
  }
  ParmChoiceMap parmChoice() const override
  {
    ParmChoiceMap map;
    map[0] = QStringList() << "All"
                           << "Defined";
    return map;
  }
  QIcon icon() const override {
    return QIcon(":/images/save.png");
  }

public slots:
  void on_selectProjectFile_clicked();
  void on_projectFileName_editingFinished();
  void validateProjectFile();

protected:
  Ui_SaveAllDlg* ui;
  QDialog* dlg;
};

/**
 * \class ResetAll SystemProcessSave.hpp <SystemProcessSave.hpp>
 *
 * Erase all data and reset all settings to default.
 *
 * \ingroup GlobalProcess
 */
class LGX_EXPORT ResetAll : public GlobalProcess {
public:
  ResetAll(const GlobalProcess& proc)
    : Process(proc)
    , GlobalProcess(proc)
  {
  }

  bool operator()(const ParmList&) override {
    return (*this)();
  }
  bool operator()();

  QString folder() const override {
    return "System";
  }
  QString name() const override {
    return "Reset All";
  }
  QString description() const override {
    return "Reset all stacks and meshes";
  }
  QStringList parmNames() const override {
    return QStringList();
  }
  QStringList parmDescs() const override {
    return QStringList();
  }
  QIcon icon() const override {
    return QIcon(":/images/ClearStack.png");
  }
};

/**
 * \class TakeSnapshot SystemProcessSave.hpp <SystemProcessSave.hpp>
 *
 * This process allows scripts and other processes to take snapshots of the current result.
 *
 * Parameter       | Description
 * ----------------|------------
 * Filename        | Name of the output file. The extension will define will file format is used.
 * Expend Frustrum | If true and the widht/height ratio is not the same as the screen, the frustrum will be expended
 * Width           | Width of the image
 * Height          | Height of the image
 * Oversampling    | Oversampling factor (see Notes)
 * Quality         | For lossy file formats, quality of the image on a scale from 0 to 100.
 *
 * Note on oversampling
 * ====================
 *
 * If oversampling is not 1, then the scene is rendered on an image with width and height multiplied by the
 * oversampling. The result is then scaled back to the wanted size, using bilinear filtering.
 *
 * \ingroup GlobalProcess
 */
class LGX_EXPORT TakeSnapshot : public GlobalProcess {
public:
  TakeSnapshot(const GlobalProcess& proc)
    : Process(proc)
    , GlobalProcess(proc)
  {
  }

  bool operator()(const ParmList& parms) override
  {
    QString filename = parms[0].value<QString>();
    if(filename.isEmpty())
      return setErrorMessage("Error, no file name specified.");
    int width = parms[2].toInt();
    int height = parms[3].toInt();
    float overSampling = parms[4].toFloat();
    int quality = parms[5].toInt();
    bool expand_frustum = parms[1].value<bool>();
    return (*this)(filename, overSampling, width, height, quality, expand_frustum);
  }

  bool operator()(QString filename, float overSampling, int width, int height, int quality, bool expand_frustum);

  QString folder() const override {
    return "System";
  }
  QString name() const override {
    return "Snapshot";
  }
  QString description() const override {
    return "Take a snapshot of the current view";
  }
  QStringList parmNames() const override
  {
    return QStringList() << "Filename"
                         << "Expand Frustum"
                         << "Width"
                         << "Height"
                         << "Oversampling"
                         << "Quality";
  }
  QStringList parmDescs() const override
  {
    return QStringList() << "Filename"
                         << "Expand Frustum"
                         << "Width"
                         << "Height"
                         << "Oversampling"
                         << "Quality";
  }
  ParmList parmDefaults() const override
  {
    return ParmList() << ""
                      << false
                      << 0
                      << 0
                      << 1.0
                      << 95;
  }
  QIcon icon() const override {
    return QIcon(":/images/SaveScreenshot.png");
  }
};
} // namespace process
} // namespace lgx

#endif
