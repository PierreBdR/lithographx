/**************************************************************************/
/*  This file is part of LithoGraphX.                                     */
/*                                                                        */
/*  LithoGraphX is free software: you can redistribute it and/or modify   */
/*  it under the terms of the GNU General Public License as published by  */
/*  the Free Software Foundation, either version 3 of the License, or     */
/*  (at your option) any later version.                                   */
/*                                                                        */
/*  LithoGraphX is distributed in the hope that it will be useful,        */
/*  but WITHOUT ANY WARRANTY; without even the implied warranty of        */
/*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         */
/*  GNU General Public License for more details.                          */
/*                                                                        */
/*  You should have received a copy of the GNU General Public License     */
/*  along with LithoGraphX.  If not, see <http://www.gnu.org/licenses/>.  */
/**************************************************************************/

#ifndef COLORBAR_H
#define COLORBAR_H

#include <LGXConfig.hpp>
#include <GL.hpp>

#include <LGXViewer/qglviewer.h>
#include <Parms.hpp>

#include <iostream>
#include <QDataStream>
#include <QString>
#include <QTextStream>
#include <string>
#include <valarray>

namespace lgx {
class LGX_EXPORT ColorBar {
public:
  typedef std::valarray<double> array;

  enum Position { Top, Bottom, Left, Right, TopLeft, BottomLeft, TopRight, BottomRight };
  enum Orientation { Horizontal, Vertical };

  ColorBar(Position pos = Right);

  void draw(QGLViewer* viewer, GLuint colormapTexId, QPaintDevice* device = 0) const;

  Position position;
  Orientation orientation;
  QFont font;

  double scale_length;
  double width;
  double distance_to_border;
  double text_to_bar;
  double tick_size;
  double exp_size;
  double epsilon;
  double line_width;

  double vmin, vmax;

  double globalScaling;

  QString label;

  void readParms(util::Parms& parms, QString section);
  void writeParms(QTextStream& pout, QString section);

  void scaleDrawing(double scale);
  void restoreScale();

  void loadDefaults();

protected:
  void startScreenCoordinatesSystem(QPaintDevice* device) const;
  void stopScreenCoordinatesSystem() const;

  mutable double prev_width, prev_height;
  void _getValues(double start, double end, double delta, array& result) const;
  array _selectValues_direct(double length, bool is_vertical, const QFontMetricsF& metric, double min_dist) const;
  std::pair<double, double> _significant_digits(double start, double end) const;
  bool _canRenderTicks(const array& ticks, double length, double min_dist, bool is_vertical,
                       const QFontMetricsF& font_metric) const;
  QStringList _tick2str(const array& ticks, QString* extra = 0) const;
  array selectValues(double length, bool is_vertical, const QFontMetricsF* metric = 0) const;
};

std::ostream& operator<<(std::ostream& s, const ColorBar::Position& pos);
std::istream& operator>>(std::istream& s, ColorBar::Position& pos);

QTextStream& operator<<(QTextStream& s, const ColorBar::Position& pos);
QTextStream& operator>>(QTextStream& s, ColorBar::Position& pos);

QDataStream& operator<<(QDataStream& s, const ColorBar::Position& pos);
QDataStream& operator>>(QDataStream& s, ColorBar::Position& pos);
} // namespace lgx
#endif // COLORBAR_H
